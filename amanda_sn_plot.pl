#!/usr/bin/env perl
use strict;
use rob_misc;

# http://bulgepositrons.pbworks.com/w/file/fetch/100788601/grid.pdf


# mcgbf
# mcgbtf
# mcagbf
# hrdiag_HG

# mcx_eagbf
# mctmsf (in hrdiag_HG?)

# default core limits:

# minimum_mass_for_carbon_ignition (ONe core) => 1.6
# minimum_mass_for_neon_ignition (hence SNII) => 2.85

my $opts = {
#    0.28 => {
 #       minimum_mass_for_carbon_ignition => 1.6,
  #      minimum_mass_for_neon_ignition => 2.85
   # },
};

my @masslist = (4,5,5.5,6.0,6.25,6.5,6.75,7.0,7.5,7.75,8.0,9.7,10,11,12);
    

foreach my $Y (0.28, 0.35, 0.40)
#my $Y = 0.28;
{
    if($Y==0.28)
    {
        foreach my $M (@masslist)
        {
            printf "% 10.2f",$M;
        }
        print "\n";
    }
    printf "  % 5s\n ",'Y='.sprintf('%.2f',$Y);


 #   foreach my $dM (-1.0,-0.9,-0.8,-0.7,-0.6,-0.5,-0.4,-0.3,-0.2,-0.1,0.0,0.1,0.2)
    {
#        printf "dM=% 5.2f ",$dM;

        #for(my $M=5.0; $M<=12; $M+=$dM)
        foreach my $M (@masslist)
        {
            my $args;
            
            #$args = "--amanda_dMHe $dM ";
            $args .= "--init_abund 5 $Y --M_1 $M --log_filename /dev/stdout ";
            if(defined $opts->{$Y})
            {
                map
                {
                    $args .= '--'.$_.' '.$opts->{$Y}->{$_}.' ';
                }sort keys %{$opts->{$Y}};
            }

            #print "CALL $args\n";
            my $log = `tbse $args`;
            my @x = grep {/^SUPERNOVA/} split(/\n/,$log);
            
            my $type;
            my $string;
            if($x[0] && $x[0]=~/in a type (\d+) \((\S+)\)/)
            {
                # supernova
                ($type,$string) = ($1,$2);
            }
            else
            {
                # WD : find out which type
                if($log =~/ 13700.0000\s+\S+\s+\S+\s+(\d+)\s+0\s+\S+/)
                {
                    if($1 == 11)
                    {
                        ($type,$string) = (-1,'COWD');
                    }
                    elsif($1 == 12)
                    {
                        ($type,$string) = (-2,'ONeWD');
                    }
                    else
                    {
                        ($type,$string) = (0,'FAIL');
                    }
                }
                else
                {
                    ($type,$string) = (0,'SCAN');
                    #print "LOG SCAN FAILED : \n$log\n";
                }
            }
            
            printf "% 9s ",$string;
        }
        print "\n";
    }
}
