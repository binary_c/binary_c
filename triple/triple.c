#include "../src/API/binary_c_API.h"
#include <time.h>
#include <sys/timeb.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/*
 * triple : triple stars with binary_c
 */

#define NUMBER_OF_SYSTEMS 1
#define SHOW_LOGS TRUE
#define MAX_EVOLUTION_TIME 15000.0
#define TIMESTEP 10.0
#define INNER_BINARY 1
#define OUTER_BINARY 0
#define NUMBER_OF_BINARIES 2

static void couple_binaries(struct stardata_t * inner,
                            struct stardata_t * outer);

int main ( int argc, char * *  argv )
{
    printf("triple stars with binary_c : binary_c version %s\n",
           BINARY_C_VERSION);

    const double maxt = MAX_EVOLUTION_TIME; /* maximum time Myr */
    const double dt = TIMESTEP; /* timestep Myr */
    double t = 0.0; /* time */
    int i; /* counter */


    /* memory for N binary systems */
    struct libbinary_c_stardata_t * stardata[NUMBER_OF_BINARIES];
    struct libbinary_c_store_t * store = NULL;
    struct libbinary_c_persistent_data_t * persistent_data = NULL;

    /*
     * M contains the masses: inner0, inner1, outer
     * in Msun
     */
    double M[NUMBER_OF_BINARIES+1] = { 1.0, 2.0, 0.5 };

    /*
     * Separations in Rsun : inner, outer
     */
    double sep[NUMBER_OF_BINARIES] = { 100.0, 1000.0 };

    for(i=0;i<NUMBER_OF_BINARIES;i++)
    {
        /*
         * Set up the argument string to define the binary system
         */
        double m0,m1,orbital_separation;
        if(i == INNER_BINARY)
        {
            /*
             * Inner binary
             */
            m0 = M[0]; // Msun
            m1 = M[1]; // Msun
            orbital_separation = sep[INNER_BINARY]; // Rsun
        }
        else if(i == OUTER_BINARY)
        {
            /*
             * Outer binary:
             * Star 0 is the inner binary
             * Star 1 is the outer companion
             */
            m0 = M[0] + M[1]; // Msun
            m1 = M[2]; // Msun
            orbital_separation = sep[OUTER_BINARY]; // Rsun
        }
        else
        {
            Exit_binary_c_no_stardata(
                BINARY_C_ALGORITHM_OUT_OF_RANGE,
                "Unknown binary system %d",
                i);
        }

        char * argstring;
        int n_writ = asprintf(&argstring,
                              "binary_c M_1 %g M_2 %g eccentricity 0.0 metallicity 0.02 max_evolution_time 15000 idum -10 separation %g",
                              m0,
                              m1,
                              orbital_separation
            );
        if(n_writ<0)
        {
            Exit_binary_c_no_stardata(BINARY_C_WRITE_FAILED,
                                      "snprintf into argstring failed with error %d\n",
                                      n_writ);
        }

        stardata[i] = NULL;
        binary_c_new_system(stardata + i,
                            NULL,
                            NULL,
                            &store,
                            &persistent_data,
                            &argstring,
                            -1);
        Safe_free(argstring);
        fprintf(stderr,
                "stardata[%d] = %p; preferences %p\n",
                i,
                (void*)stardata[i],
                (void*)stardata[i]->preferences);


        /* log to /tmp/ */
        char * filename = NULL;
        if(asprintf(&filename,
                    "/tmp/triple-%s.dat",
                    (i == INNER_BINARY ? "inner" :
                     i == OUTER_BINARY ? "outer" :
                     "unknown")
               )<=0)
        {
            Exit_binary_c_no_stardata(BINARY_C_ALLOC_FAILED,
                                      "asprintf into filename failed\n");

        }
        strlcpy(stardata[i]->preferences->log_filename,
                filename,
                STRING_LENGTH-1);

        /* disable API log */
        strlcpy(stardata[i]->preferences->api_log_filename_prefix,
                "/dev/null",
                STRING_LENGTH-1);

        /*
         * Send binary_c's PRINTF output to its buffer which is
         * outpout below.
         */
        stardata[i]->preferences->internal_buffering = INTERNAL_BUFFERING_STORE;

        /*
         * Batchmode should think we're using the shared library to
         * run systems.
         */
        stardata[i]->preferences->batchmode = BATCHMODE_LIBRARY;
    }


    /* Stellar evolution time loop */
    while(t < maxt + TINY)
    {
        for(i=0;i<NUMBER_OF_BINARIES;i++)
        {
            /* evolve each binary for a time dt */
            binary_c_evolve_for_dt(stardata[i],dt);
        }

        /*
         * Couple the binaries into a triple
         */
        couple_binaries(stardata[INNER_BINARY],
                        stardata[OUTER_BINARY]);

        for(i=0;i<NUMBER_OF_BINARIES;i++)
        {
            /* brief status string */
            fprintf(stderr,
                    "STATUS  : System %d : t=%g M1=%g M2=%g a=%g\n",
                    i,
                    stardata[i]->model.time,
                    stardata[i]->star[0].mass,
                    stardata[i]->star[1].mass,
                    stardata[i]->common.orbit.separation);

            /* Show log if SHOW_LOGS is set */
            if(SHOW_LOGS == TRUE)
            {
                char * buffer = NULL;
                size_t nbytes = 0;
                binary_c_buffer_info(*(stardata+i),
                                     &buffer,
                                     &nbytes);
                if(nbytes > 0)
                {
                    printf("%s\n",buffer);
                }
            }
        }

        t += dt; // update the time
    }

    /* free memory */
    for(i=0;i<NUMBER_OF_BINARIES;i++)
    {
        binary_c_free_memory(stardata+i,
                             TRUE, // prefs
                             TRUE, // stardata
                             FALSE, // store
                             TRUE, // buffer
                             TRUE // persistent
            );
    }
    binary_c_free_store_contents(store);

    return 0;
}

/************************************************************/

static void couple_binaries(struct stardata_t * inner,
                            struct stardata_t * outer)
{
    /*
     * Write some code here to couple the inner and outer binary
     */

    /* TODO */
}
