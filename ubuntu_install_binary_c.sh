#!/bin/bash

############################################################
#
# script to install the latest stable binary_c and
# binary_c-python
#
# tested on Ubuntu 22.04 and 24.04, binary_c 2.2.4
#
# Note: this may ask you to choose a version of python
#       as your default python. Some of our software only
#       works on Python 3.10+.
#
# Environment variables you can use to change things:
#
# CC : the C compiler, we default to gcc-12+.
#
# BINARY_C : the location of the binary_c root directory.
#
# GITROOT : the location in which we download the source code
#           of libraries we require.
#
#
# Note:
#
# You can pass responses to the various questions as a
# list of command-line arguments. For example, the following
# command is used to build a local binary_c and run unit
# tests, but not download  various libraries, as required
# for gitlab CI.
#
# ./ubuntu_install_binary_c.sh yes no no no no local_accurate yes no no no
#
############################################################
set -e  # make sure we exit on failure

# save args list
args=("$@")

# Defaults for the environment variables that control
# installation locations, compiler etc.

source /etc/os-release

# set up compiler
_VERSION_OK=0
if [ "$VERSION_ID" = "22.04" ];then
    : "${CC:="gcc-12"}"
    echo "Detected Ubuntu 22.04"
    _VERSION_OK=1
fi
if [ "$VERSION_ID" = "24.04" ];then
    : "${CC:="gcc-14"}"
    echo "Detected Ubuntu 24.04"
    _VERSION_OK=1
fi

# we are only sure to work on 22.04 or 24.04
if [ "$_VERSION_OK" = "0" ];then
    echo "You are using an unsupported version ($VERSION_ID). I will stop now before anything is broken."
    exit
fi

# set up location of binary_c
: "${BINARY_C:="$HOME/binary_c"}"
# git download location
: "${GITROOT:="$HOME/git"}"
# pip command
: "${PIP:="pip3"}"
# pipupgrade command
: "${PIPUPGRADE:="pipupgrade"}"
# sudo command
: "${SUDO:="sudo"}"
# apt-get command
: "${APTGET:="apt-get"}"
# add-apt-repository command
: "${ADDAPTREPOSITORY:="add-apt-repository -y"}"

export CC
export BINARY_C
export GITROOT
export PIP
export SUDO
export APTGET
export PIPUPGRADE

GITLAB_ROBIZZARD="gitlab.com/rob.izzard"
GITLAB_BINARY_C="gitlab.com/binary_c"
MINT_DATA_URL="http://personal.ph.surrey.ac.uk/~ri0005/MINT"

function setup
{
    # we require gawk and wget

    echo "#########################################################"
    echo "Before we start, we need to install wget and gawk"
    echo "#########################################################"
    $SUDO $APTGET install wget gawk

    # Get binary_c stable version and binary_c-python version
    export VERSION_URL=https://gitlab.com/binary_c/binary_c/-/raw/master/src/binary_c_version.h
    export BINARY_C_STABLE_VERSION=$(wget -q -O /dev/stdout $VERSION_URL | grep define\ BINARY_C_STABLE_VERSION\ \" |gawk "{print \$3}"|sed s/\"//g)
    export BINARY_C_PYTHON_VERSION=$(wget -q -O /dev/stdout $VERSION_URL | grep define\ BINARY_C_PYTHON_VERSION\ \" |gawk "{print \$3}"|sed s/\"//g)

    # Install librinterpolate, libmemoize, libcdict
    mkdir -p $GITROOT
    cd $GITROOT
    export GITROOT

    ############################################################
    # commands
    export WGET="wget --no-check-certificate"
}

function message1
{
    # show a welcome message
    echo
    echo "#########################################################"
    echo "Binary_c installation script for Ubuntu/Kubuntu"
    echo "#########################################################"
    echo
    echo "Press CTRL-C to exit at any time"
    echo
    echo "#########################################################"
    echo
}

function message2
{
    # show setup
    echo "#########################################################"
    echo
    echo "$ID version $VERSION_ID ($VERSION_CODENAME)"
    echo
    echo "C compiler is set in CC"
    echo "CC = $CC"
    echo
    echo "BINARY_C is the directory where binary_c will be installed"
    echo "BINARY_C = $BINARY_C"
    echo
    echo "BINARY_C_STABLE_VERSION = $BINARY_C_STABLE_VERSION"
    echo "BINARY_C_PYTHON_VERSION = $BINARY_C_PYTHON_VERSION"
    echo
    echo "GITROOT is where source code for support libraries is downloaded"
    echo "GITROOT = $GITROOT"
    echo
    echo "#########################################################"
    echo
}


function confirm {
    # call with a prompt string or use a default

    # message for the user is the first argument
    message=$1

    # get first argument and reset args
    first="${args[0]}"
    args=( "${args[@]:1}" )

    if [ ! -z "$message" ];then
        echo -n "$message "
    fi

    if [ -z $first ]; then
        #echo "read response from stdin"
        read -r response
    else
        #echo "Using $first as response"
        response="$first"
        echo
        echo "Using argument-set \"$first\""
        echo
    fi
    case "$response" in
        [yY][eE][sS]|[yY])
            true
            ;;
        *)
            false
            ;;
    esac
}

function checksetup
{
    confirm "Does the above look correct?" && return
    exit  # on "No"
}

############################################################
versionsorter() {
    [  "$1" = "`echo -e "$1\n$2" | sort -V | head -n1`" ]
}

function setup_bash
{
    # set up bash and .bashrc
    BASHRC="$HOME/.bashrc"
    if ! grep -q 'binary_c paths do not alter' $BASHRC
    then
        # set up bash paths : do this only once!
        echo '# binary_c paths do not alter' >> $BASHRC
        echo "export BINARY_C=\"$BINARY_C\"" >> $BASHRC
        echo 'export LD_LIBRARY_PATH=$HOME/lib:$BINARY_C:$BINARY_C/src' >> $BASHRC # do this only once!
        echo 'export PATH=.:$HOME/bin:$HOME/.local/bin:$PATH' >> $BASHRC # do this only once!

        # psb, psbb, git functions etc. and completion of binary_c arguments
        printf '%s' '


# function to switch to progs/stars/binary_c
function psb
{
    cd $BINARY_C
}
function psbb
{
    cd $BINARY_C/builddir
}

# git function for review number
git_rev ()
{
    d=`date +%Y%m%d`
    c=`git rev-list --full-history --all --abbrev-commit | /usr/bin/wc -l | /bin/sed -e "s/^ *//"`
    h=`git rev-list --full-history --all --abbrev-commit | head -1`
    echo ${c}:${d}:${h}
}

alias new="ls -lrt | tail -10"

# git function to find the repository url
git_url ()
{
    git config --get remote.origin.url
}

# source autocompletion library
if [ -n "$BASH_VERSION" ]; then
    [ -f /etc/bash_completion ] && . /etc/bash_completion
fi

# completion of binary_c arguments
_binary_c()
{
    COMPREPLY=()
    cmd="${COMP_WORDS[0]}"
    cur="${COMP_WORDS[COMP_CWORD]}"
    prev="${COMP_WORDS[COMP_CWORD-1]}"
    binaryc_opts=$($BINARY_C/binary_c help |/bin/grep -A2 "where the arguments are a selection of :"|tail -1|tr " " "\n")
    # check if we match a binary_c argument
    subcmd=( $(compgen -W "$binaryc_opts" "$prev") )
    if [[ $subcmd ]] ; then
        # we do match an argument : try to get sub options
        subopts=$($BINARY_C/binary_c argopts $subcmd)
        COMPREPLY=( $(compgen -W "$subopts" -- ${cur}) )
    else
        # we do not, so list the arguments
        COMPREPLY=( $(compgen -W "$binaryc_opts" -- ${cur}) )
    fi
    return 0
}
complete -F _binary_c binary_c
complete -F _binary_c tbse
complete -F _binary_c tbse.mint
complete -F _binary_c tbse.IRAS

# for binarycpython
export GSL_DIR=`gsl-config --prefix`

' >> $BASHRC
    fi

    # some of the above we require now
    export LD_LIBRARY_PATH="$HOME/lib:$BINARY_C:$BINARY_C/src"
    export LIBRARY_PATH="$LD_LIBRARY_PATH"
    export PATH="$HOME/bin:$HOME/.local/bin:$PATH"
}

function install_from_gitlab
{
    PACKAGE=$1

    # download and install $PACKAGE
    mkdir -p $GITROOT
    cd $GITROOT
    $WGET https://$GITLAB_ROBIZZARD/$PACKAGE/-/archive/master/$PACKAGE-master.zip -O $PACKAGE-master.zip
    unzip -o $PACKAGE-master.zip
    cd $PACKAGE-master/
    if [ -d builddir ];
    then
        rm -r builddir
    fi
    meson setup --prefix=$HOME --buildtype=release builddir --wipe
    ninja -C builddir install
}

function install_binary_c_from_zip
{
    # download and install binary_c
    echo "Install from zip"
    if [ -d $BINARY_C ];
    then
        echo "Directory $BINARY_C already exists: please remove any old binary_c installation before installing a new one"
        exit
    fi
    cd /tmp # work from /tmp
    $WGET https://$GITLAB_BINARY_C/binary_c/-/archive/master/binary_c-master.zip -O binary_c-master.zip
    unzip -o binary_c-master.zip
    rm binary_c-master.zip
    mv binary_c-master $BINARY_C
    cd $BINARY_C
    meson setup -Dbuildtype=release -Dvalgrind=true builddir --wipe
    ninja -C builddir binary_c_install
    ./tbse
    cd $HOME
}

function install_binary_c_from_git_https
{
    # download and install binary_c
    echo "Install from git https to BINARY_C=$BINARY_C"
    git clone --branch releases/$BINARY_C_STABLE_VERSION --single-branch https://$GITLAB_BINARY_C/binary_c.git $BINARY_C
    rm -rf $BINARY_C/builddir 2>/dev/null
    cd $BINARY_C
    meson setup -Dbuildtype=release -Dvalgrind=true $BINARY_C/builddir --wipe
    ninja -C $BINARY_C/builddir binary_c_install
    $BINARY_C/binary_c-config version
}


function install_binary_c_from_git_ssh
{
    # download and install binary_c git@gitlab.com:binary_c/binary_c.git
    echo "Install from git ssh"
    cd $BINARY_C/..
    git clone git@gitlab.com:binary_c/binary_c.git
    cd binary_c
    git checkout releases/$BINARY_C_STABLE_VERSION
    meson setup -Dbuildtype=release -Dvalgrind=true builddir --wipe
    ninja -C builddir binary_c_install
    ./binary_c-config version
}

function install_binary_c_from_local_source
{
    # install from pre-existing local source
    echo "Installing from local source at $BINARY_C"
    cd $BINARY_C
    meson setup -Dbuildtype=release -Dvalgrind=true builddir --wipe
    ninja -C builddir binary_c_install
    ./binary_c-config version
}

function install_binary_c_from_local_source_accurate
{
    # install from pre-existing local source with
    # accurate and debug on e.g. for unit testing
    echo "Installing from local source at $BINARY_C"
    cd $BINARY_C
    meson setup -Daccurate=true -Dvalgrind=true -Ddebug=true --buildtype=debug builddir --wipe
    ninja -C builddir binary_c_install
    ./binary_c-config version
}

function run_test_population
{
    # run a test population : TODO with binary_c-python
    source $HOME/.bashrc
    cd $BINARY_C
    ./src/python/ensemble.py r=10 verbosity=1 M1spacing=logM1 ensemble_filter_SCALARS=1 outdir=/tmp
}



function install_binary_c_python
{
    # install the binary_c Python module
    export LIBRARY_PATH=$LD_LIBRARY_PATH

    # ubuntu 22.04 only
    if [ "$VERSION_ID" = "22.04" ];then
        # requirements for binary_c's ensemble scripts
    $PIP install -U gitpython
    fi

    # ubuntu 24.04 only
    if [ "$VERSION_ID" = "24.04" ];then
        # we have to wrap pip3 calls to prevent errors on Ubuntu 24
        printf "#!/bin/bash\\npip3 \"\$@\" --break-system-packages" > $HOME/.pipwrap.sh
        chmod +x $HOME/.pipwrap.sh
        export PIP="$HOME/.pipwrap.sh"
    fi

    # This installs directly with pip
    # choose a binary_c-python install method
    #$PIP install git+https://$GITLAB_BINARY_C/binary_c-python.git

    # This checks out the git archive, then installs that
    cd $GITROOT
    git clone https://$GITLAB_BINARY_C/binary_c-python.git
    cd binary_c-python
    git checkout releases/$BINARY_C_PYTHON_VERSION/$BINARY_C_STABLE_VERSION
    ./install.sh
}

function update_packages
{
    # update and install a KDE desktop :)
    #$SUDO $APTGET install synaptic kubuntu-desktop
    #balooctl disable

    # ubuntu 24.04 only
    if [ "$VERSION_ID" = "24.04" ];then
        # require the universe repository for gcc-14
        echo "Adding universe repository"
        $SUDO $APTGET install software-properties-common
        $SUDO $ADDAPTREPOSITORY universe
    fi

    # update system
    $SUDO $APTGET update
    $SUDO $APTGET upgrade

    # install required packages
    $SUDO $APTGET install binutils binutils-dev coreutils debianutils bash zip  gdb valgrind gawk python3 pipenv kcachegrind meson ninja-build emacs perl libgsl-dev libgslcblas0 global libbsd-dev libiberty-dev libjemalloc-dev zlib1g zlib1g-dev unzip wget curl git jp2a libcfitsio-dev sed gawk pkg-config libc6 libc6-dev patchelf uuid-dev pandoc python3-pip

    # ubuntu 22.04 only
    if [ "$VERSION_ID" = "22.04" ];then
        echo "Installing packages for Ubuntu $VERSION_ID"
        $SUDO $APTGET install libcurl4-nss-dev gcc-12 libgcc-12-dev
        $SUDO update-alternatives --install /usr/bin/python python /usr/bin/python3.10 1
        echo 'You should normally choose Python 3.x where x >= 10'
        $SUDO update-alternatives --config python # >>> choose python3.10+ <<<
    fi

    # ubuntu 24.04 only
    if [ "$VERSION_ID" = "24.04" ];then
        echo "Installing packages for Ubuntu $VERSION_ID"
        $SUDO $APTGET install libcurl4-openssl-dev gcc-14 libgcc-14-dev
    fi

    # clean apt package cache
    $SUDO $APTGET clean

    # update python packages required for the build
    update_python_packages
}

function update_python_packages
{
    # update python packages and system packages required for them
    echo "Updating system packages for Python modules"
    $SUDO $APTGET install libgpg-error-dev libgpgme-dev swig libcairo2-dev libgirepository1.0-dev autoconf python3-dev


    # ubuntu 22.04 only
    if [ "$VERSION_ID" = "22.04" ];then
        echo "Installing pipupgrade"
        $PIP install pipupgrade
        $PIPUPGRADE --self
        $PIPUPGRADE --verbose --yes
        echo "Installing Python modules locally with $PIP"
        $PIP install --upgrade setuptools testresources gitpython prettytable beautifulsoup4 html5lib strip-ansi meson ninja deepdiff colorama prettytable uuid pandas pathlib
    fi

    # ubuntu 24.04 only
    if [ "$VERSION_ID" = "24.04" ];then
        echo "Installing Python modules with $APTGET"
        $SUDO $APTGET install python3-setuptools python3-testresources python3-prettytable python3-html5lib meson ninja-build python3-deepdiff python3-colorama python3-prettytable python3-pandas python3-git

        # this will break system packages but these modules have no system packages
        $PIP install --upgrade strip-ansi uuid --break-system-packages
    fi

    echo "using Meson version "`meson --version`
    echo "using Ninja version "`ninja --version`
}

function download_MINT_data
{
    # download MINT data
    mkdir -p $HOME/data/MINT
    cd $HOME/data/MINT
    rm -f sync
    $WGET $MINT_DATA_URL/sync
    chmod +x sync
    ./sync
}

function run_unit_tests
{
    # run unit tests
    cd $BINARY_C
    export BINARY_C_EXECDIR=$BINARY_C
    ./src/python/do_unit_tests.py compare force
}

############################################################

message1
setup
message2
checksetup

# set up bash (always do this)
setup_bash

# install/check system packages
confirm 'Do you wish to check and, if necessary, install system packages? (requires sudo permissions)' && update_packages

# now we can set GSL_DIR because GSL has been installed
# (or at least should have been)
export GSL_DIR=`gsl-config --prefix`

# install librinterpolate
confirm 'Install librinterpolate?' && install_from_gitlab librinterpolate

# install libmemoize
confirm 'Install libmemoize?' && install_from_gitlab libmemoize

# install libcdict
confirm 'Install libcdict?' && install_from_gitlab libcdict

# install binary_c (perhaps)
s="${args[0]}" # use arg if given
args=( "${args[@]:1}" )

if [ -z "$s" ];then
    # no arg given, choose from stdin
    echo 'Install binary_c from ... ?'
    select s in  'git (https)' 'git (ssh, keys must be set up)' 'git (zip, no version control)' 'No';
    do case $s in
           'git (https)' ) s="https"; break;;
           'git (ssh, keys must be set up)' ) s="ssh"; break;;
           'git (zip, no version control)' ) s="zip"; break;;
           "local source at $BINARY_C" ) s="local"; break;;
           'No' ) break;;
       esac
    done
fi

# act on install instruction
case $s in
    'https' ) install_binary_c_from_git_https;;
    'ssh'   ) install_binary_c_from_git_ssh;;
    'zip'   ) install_binary_c_from_zip;;
    'local' ) install_binary_c_from_local_source;;
    'local_accurate' ) install_binary_c_from_local_source_accurate;;
    'No'  )   echo "No install" ;;
esac

# run binary_c unit tests?
confirm 'Run binary_c unit tests?' && run_unit_tests

confirm 'Install binary_c-python?' && install_binary_c_python

# install MINT data?
confirm 'Download MINT data?' && download_MINT_data

# run test population
confirm 'Run a test binary_grid stellar population to make sure everything works?' && run_test_population

echo "binary_c install script finished"
