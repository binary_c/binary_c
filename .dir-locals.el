;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((c-mode
  (eval setq flycheck-clang-args
        (list "-fbracket-depth=512")))
 ("."
  (projectile-project-name . binary_c))
 ("."
  (projectile-project-compilation-dir . "builddir"))

 )

; open .def files as .h
(add-to-list 'auto-mode-alist '("\\.def\\'" . c-mode))

;; projectile

((nil . ((projectile-project-name . ("binary_c")))))
((nil . ((projectile-project-compilation-dir . ("builddir")))))
((nil . ((projectile-project-compilation-cmd . ("ninja")))))
((nil . ((projectile-project-type . ("meson")))))
