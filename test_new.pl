#!/usr/bin/env perl
use rob_misc;
use strict;

my $altcode = '/home/rgi/svn/binary_c/releases/2.0pre27';

#
# Test two versions of binary_c
# to see if their output is the same.
#
# To make it work, compile binary_c to 
# two executables.
#
# The "reference" version should be called binary_c-ref
# The "new" version should be called binary_c
#


while(1)
{
    # run a random system with this code
    print "Running reference system\n";
    my $r = `env BIN_C=binary_c-ref tbse --random_systems 1 --log_filename /tmp/c_log.ref 2>\&1`;
    my $args = ($r=~/^(--M_1.*)/)[0];
    print "ARGS:\n\n$args\n\n";

    # run with alt code
    print "Running alternative system\n";
    `cd $altcode; tbse $args --log_filename /tmp/c_log.old 2>\&1`;
    
    print "Doing diff\n";
    # check for difference, if so wait
    my $d = `diff /tmp/c_log.ref /tmp/c_log.old`;
    chomp $d;
    if($d)
    {
        print "$d\n\n\nDiff detected, waiting for you...\n";
        <STDIN>;
    }
}
