#!/bin/bash

# run WTTS2 using docker


# we create a volume for WTTS : this
# is a file space that is stored even when WTTS stops
docker volume create wtts_volume

docker run --name=wtts2 \
       -it \
       --rm \
       -e DISPLAY \
       --ipc=host \
       --mount source=wtts_volume,target=/home/wtts/wtts_rundir \
       -v /tmp/.X11-unix:/tmp/.X11-unix \
       -v $HOME/.Xauthority:/home/wtts/.Xauthority \
       robizzard/wtts:wtts2 |& tee run.log

# to clean the WTTS volume run:
# drocker volume rm wtts_volume
