#!/usr/bin/env perl
use strict;
use rob_misc;
use binary_grid2;
use binary_grid::C;

# 
# script to test the new polytrope comenv lambda 
# code vs Dewi & Tauris 2000
#

my $outdir = '/tmp/dewi';
mkdirhier($outdir);
my $datafp;

my $population = binary_grid2->new(defaults());



$population->set(
    parse_bse_function_pointer=>\&parse_bse,
    );

sub parse_bse
{

    while(1)
    {

        my $la = $population->tbse_line();
        my $header = shift @$la;
        last if($header eq 'fin');
	if($header eq 'COMENVLOG')
	{
            #if($la->[1] == 5 || $la->[1] == 6)
            #if($la->[1] != 4)
            {
                print {$datafp} "@$la\n";
            }
        }
    }
}

# reconstruct their Fig.1 which is lamdba as a 
# function of R/Rsun for various masses
my $gpfile = {
    dewi=>"$outdir/Dewi_Tauris_Fig1.plt",
    vds=>"$outdir/VDSluys_Fig4.plt"
};
my $gp = {};

foreach my $k (keys %$gpfile)
{    
    open($gp->{$k},'>',$gpfile->{$k})||die;

    my $pdf = $gpfile->{$k};
    $pdf =~ s/plt$/pdf/;

    print {$gp->{$k}} "set terminal pdfcairo
set output \"$pdf\"
set ylabel \"Lambda\"
set xtics
set ytics nomirror
set pointsize 0.2
";
}

print {$gp->{dewi}} "
set xrange[0:500]
set yrange[0.1:100]
set xlabel \"Radius / Rsun\"
set y2label \"k^{2}\"
set y2tics nomirror
set logscale y
";
print {$gp->{vds}} "
set xrange[0.15:1.25]
set yrange[0:5]
set xlabel \"Core mass / Msun\"
";

# Dewi plots
foreach my $m (3..10)
#foreach my $m (4)
{
    print {$gp->{dewi}} "
set title \"M=$m Msun\"
";
    my $plotcomma='plot ';
    foreach my $lambda_both (0.0, 1.0)
    {
        my $lambda_ionisation = $lambda_both;
        my $lambda_enthalpy = $lambda_both;
        my $f = "$outdir/dewi-$m.$lambda_both.dat";

        open($datafp, '>', $f)||die;
        print "LAM $lambda_both\n";

        $population->set(
            lambda_ionisation=>$lambda_ionisation,
            lambda_enthalpy=>$lambda_enthalpy,
            );

        $population->tbse( 
            $population->make_evcode_arghash
            ({
                probability=>1,
                M_1 => $m,
                M_2 => 0.1,
                orbital_period => 1e9,
                metallicity => 0.02,
                eccentricity => 0.0,
                max_evolution_time => 15000,
             })
            );

        close $datafp;
        print {$gp->{dewi}} "$plotcomma \"$f\" u 4:5 axes x1y1 w lp notitle , \"$f\" u 4:6 axes x1y2 w lp dashtype \".-\" notitle ";
        $plotcomma=',';
    }
}

# VDS plots : masses are from Fig.3
foreach my $m (0.91, 1.01, 1.14, 1.30, 1.48, 1.63, 1.81, 2.00, 2.46, 2.79, 3.17, 3.70, 4.09, 4.65, 5.28, 6.00, 6.82)
{
     print {$gp->{vds}} "
set title \"M=$m Msun\"
";
    my $plotcomma='plot ';
    foreach my $lambda_both (0.0, 1.0)
    {
        my $lambda_ionisation = $lambda_both;
        my $lambda_enthalpy = $lambda_both;
        my $f = "$outdir/vds-$m.$lambda_both.dat";

        open($datafp, '>', $f)||die;
        print "LAM $lambda_both\n";

        $population->set(
            lambda_ionisation=>$lambda_ionisation,
            lambda_enthalpy=>$lambda_enthalpy,
            );

        $population->tbse( 
            $population->make_evcode_arghash
            ({
                probability=>1,
                M_1 => $m,
                M_2 => 0.1,
                orbital_period => 1e9,
                metallicity => 0.02,
                eccentricity => 0.0,
                max_evolution_time => 15000,
             })
            );

        close $datafp;
        print {$gp->{vds}} "$plotcomma \"$f\" u 7:5 axes x1y1 w lp notitle ";
        $plotcomma=',';
    }
}


foreach my $k (keys %$gpfile) 
{
    close $gp->{$k};
    `gnuplot $gpfile->{$k}`;
}



exit;

sub defaults
{
    return (
        # physics options
        'metallicity' => 0.004,
        'wd_sigma' => 0.0,
        'c13_eff' => 1.0,
        'acc2' => 1.5,
        'tidal_strength_factor' => 1.0,
        'alpha_ce' => 0.2,
        'lambda_ce' => -3, # -1 = automatically set
        'lambda_ionisation' => 0.0, # 0.0
        'delta_mcmin' => 0.0,
        'lambda_min' => 0.0,
        'minimum_envelope_mass_for_third_dredgeup' => 0.5,
        'max_evolution_time' => 13000,
        'RLOF_interpolation_method' => 0, # 0 = binary_c, 1 = BSE 
        'log_filename' => '/dev/null', # no log required

        # grid options
        'parse_bse_function_pointer' => \&parse_data,
        'threads_stack_size' => 32, # MBytes
        'nice' => 'nice -n +1', # nice command e.g. 'nice -n +10' or '' 
        'timeout' => 30000, # seconds until timeout
        'log_args' => 0,
        'save_args' => 0,
        'vb' => 3,

        internal_buffering => 0,
        internal_buffering_compression => 9,

        # This is the path to the binary_c directory
        'rootpath' => "$ENV{HOME}/progs/stars/binary_c",

        # The name of the binary_c executable
        'prog' => 'binary_c-CND7',

        'binary' => 1, # single stars -> 0, binary stars -> 1
        'CRAP_parameter' => 0, # 1e3 = Tout
        'WRLOF_method' => 0,  # 1 = q-dep, 2=quadratic
        'RLOF_method' => 3, # 3 = Claeys et al 2014

        # we want array refs, not lines, from tbse_line
        return_array_refs => 1,

        # comenv parameters
        lambda_ce => -3,
        lambda_ionisation => 1.0,
        lambda_enthalpy => 1.0,
        comenv_splitmass=>1.0,

        AGB_method => 0

        );
}
