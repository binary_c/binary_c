#!/usr/bin/env perl
use strict;
use rob_misc;
use robqueue;
use threads::shared;
use Time::HiRes qw/usleep/;
my @testsystems=(   
    ' --M_1     1.71123754844831523769e+01 --M_2     7.57075105970837380909e+00 --orbital_period     4.43651679970229179162e+03 --eccentricity     5.68848656114774087200e-01 --metallicity     7.53323367414482445575e-03 --random_seed 13557',# double event

    '--M_1     2.60014534060197028964e+01 --M_2     8.55879786501285089173e+00 --orbital_period     2.05935187684281011400e+01 --eccentricity     4.43624291537574322319e-02 --metallicity     1.91377268702671715639e-02 --random_seed 789 use_events 0 max_evolution_time 70.78',
    '--M_1     2.76237741053063157182e+00 --M_2     1.60196227829056336844e+00 --orbital_period     4.61086822142070662522e+00 --eccentricity     7.36838498708443978558e-01 --metallicity     8.47031112564214025418e-03 --random_seed 8588',
    '--M_1     5.88781464962271616770e+00 --M_2     9.13266668344961507486e-01 --orbital_period     9.58002249943150445688e+02 --eccentricity     1.96788014934050181637e-01 --metallicity     1.74191690067243623585e-02 --random_seed 14434',
        '--M_1     4.13411193791208475545e+01 --maximum_timestep 0.01 --M_2     8.98645633651902997485e+00 --orbital_period     4.42542180119656620718e+02 --eccentricity     3.29671436606560064320e-03 --metallicity     2.94659167975601585243e-02 --random_seed 51506 ',

    '--M_1     7.96291832777422481371e+00 --M_2     6.96651666843061079248e+00 --orbital_period     1.15734216234446648741e+03 --eccentricity     8.30096343855025509129e-01 --metallicity     4.80810154883143034410e-03 --random_seed 35053',
      

    );

my $cls = `clear`;
my $grep = " grep -v ID |grep -v WDKICK|grep -v orbital_per |grep .";
my $diff = "diff --suppress-blank-empty --ignore-trailing-space --ignore-space-change --ignore-all-space";
my $waitkdiff : shared;
$waitkdiff = 0;
my $q = robqueue->new(
    nthreads=>4,
    command_queue=>0,
    subr=>\&cfsystem,
    prepend_thread_number_to_subroutine => 1
    );

while(1)
{
    $q->q();
    $q->qwait(4);
}
exit;

############################################################

sub cfsystem
{
    my ($nthread) = (@_);
    
    my $events_off = `tbse separation 0 random_systems 1 use_events 0 log_filename /tmp/c_log2.$nthread.ref`; 
    my $events_off_args = ($events_off=~/(--M_1.*)/)[0];
    
    #my $events_off_args = pop(@testsystems);
    #my $events_off = `tbse separation 0 random_systems 0 use_events 0 log_filename /tmp/c_log2.$nthread.ref $events_off_args | $grep `; 

    my $events_on = `tbse random_systems 0 $events_off_args use_events 1 log_filename /tmp/c_log2.$nthread.dat | $grep `;
    
    `cat /tmp/c_log2.$nthread.ref | $grep > /tmp/c_log2.$nthread.ref2`;
    `cat /tmp/c_log2.$nthread.dat | $grep > /tmp/c_log2.$nthread.dat2`;
    
    my $d = `$diff --color=always /tmp/c_log2.$nthread.ref2 /tmp/c_log2.$nthread.dat2 |grep -v random_seed|grep -v WDKICK | $grep`;
    chomp $d;
    my $ds = remove_ANSI($d);
    if($ds ne '2d1')
    {

        print $d;
        chomp $d;
        if($d eq '')
        {
            print "$nthread : No diff\n";
        }
        else
        {
            while($waitkdiff)
            {
                usleep(250);
            }
            
            $waitkdiff++;
            `kdiff3 /tmp/c_log2.$nthread.ref /tmp/c_log2.$nthread.dat`;
            $waitkdiff--;
            
            #print "$d \nPress return...\n";
            #<STDIN>;
            #print $cls;
        }
    }
}
