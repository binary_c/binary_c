#!/bin/bash

INARGS="$@"
RANDOM_ARGS=`tbse random_systems 1  max_evolution_time 0|grep At|head -1|perl -lane 'print "@F[4..$#F]"'`
ARGS="$RANDOM_ARGS maximum_timestep 0.1 solver 0 $INARGS"

echo
echo "Args random : $RANDOM_ARGS"
echo
echo "Args first : $ARGS"
echo
echo "Args in : $INARGS"
echo

echo "Run 0"
tbse $INARGS $ARGS evolution_algorithm 0 log_filename /tmp/c_log2.0 use_events 0 > 0.out && echo "0 done" &

echo "Run 1"
tbse $INARGS $ARGS evolution_algorithm 1 log_filename /tmp/c_log2.1 use_events 1 > 1.out && echo "1 done" &

wait

echo "Done"
