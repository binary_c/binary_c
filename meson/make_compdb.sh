#!/bin/bash
cd ${MESON_SOURCE_ROOT}
#
# The binary_c stellar population nucleosynthesis framework.
#
# Copyright Robert Izzard 2000-2021
#
# Contact rob.izzard@gmail.com
#
# Code repository:
# https://gitlab.com/binary_c
#
# Documentation is in doc/
# Code is in src/
#
# To build, run
#
# ---
#
# meson builddir
# cd builddir
# ninja binary_c_install
#
# ---
#
# Please see the files README, LICENCE and CHANGES,
# and the doc/ directory for documentation which is mirrored
# at https://binary_c.gitlab.io
#
######################
#
# Purpose:
#
# Make compile_commands JSON file with combdb, if available.
# Note: this script is not currently used by binary_c's Meson build.
#
# See make_compile_commands_json.sh for an equivalent.
#
######################

if compdb --help >/dev/null ; then
    compdb -p builddir/ list > compile_commands.json
else
    echo "Have no compdb : "
fi
