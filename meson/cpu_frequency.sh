#!/bin/bash
#
# The binary_c stellar population nucleosynthesis framework.
#
# Copyright Robert Izzard 2000-2021
#
# Contact rob.izzard@gmail.com
#
# Code repository:
# https://gitlab.com/binary_c
#
# Documentation is in doc/
# Code is in src/
#
# To build, run
#
# ---
#
# meson builddir
# cd builddir
# ninja binary_c_install
#
# ---
#
# Please see the files README, LICENCE and CHANGES,
# and the doc/ directory for documentation which is mirrored
# at https://binary_c.gitlab.io
#
######################
#
# Purpose:
#
# script to show maximum CPU frequency in Mhz, or return 1000 (Mhz)
# if not found
#
######################

SYSFILE="/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq";

if [ -f "$SYSFILE" ]; then
    # standard on Linux
    gawk "{print int(\$1/1000.0)}" $SYSFILE || exit 1

elif [ "$(sysctl >/dev/null 2>/dev/null)" ]; then
    # this might work on a Mac
    sysctl -a |gawk "{print \$2}" || exit 1

elif [ -f /proc/cpuinfo ]; then

    # crude backup: burst the CPU and look at cpuinfo
    #echo "Using /proc/cpuinfo"
    seq 4 | xargs -P0 -n1 timeout 0.5 yes > /dev/null || \
    grep -i Mhz /proc/cpuinfo | gawk "{print int(\$4)}"|head -1 || exit 1

else
    # default to 1000 Mhz
    echo 1000
fi
