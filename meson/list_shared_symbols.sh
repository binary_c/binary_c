#!/bin/bash

#
# The binary_c stellar population nucleosynthesis framework.
#
# Copyright Robert Izzard 2000-2021
#
# Contact rob.izzard@gmail.com
#
# Code repository:
# https://gitlab.com/binary_c
#
# Documentation is in doc/
# Code is in src/
#
# To build, run
#
# ---
#
# meson builddir
# cd builddir
# ninja binary_c_install
#
# ---
#
# Please see the files README, LICENCE and CHANGES,
# and the doc/ directory for documentation which is mirrored
# at https://binary_c.gitlab.io
#
######################
#
# Purpose:
#
# list shared symbols in the shared library given in the first argument
#
# (we filter out other functions that are from sub-libraries)
#
######################


SOFILE=$1
export CLICOLOR=1 # required for OSX
GREEN="\e[32m"
RESET="\e[0m"

# red
gecho -e $GREEN 2>/dev/null || echo -e $GREEN

nm -C -f posix "$SOFILE" | awk '{if ($2 == "T"){print $1}}' | grep -v _fini|grep -v _init | tr "\n" " "

#reset
gecho -e $RESET 2>/dev/null || echo -e $RESET
