function disable_turbo() {
    if [ -f /sys/devices/system/cpu/intel_pstate/no_turbo ]; then
        # disable turbo
        if [ `cat /sys/devices/system/cpu/intel_pstate/no_turbo` -eq 0 ]; then
            echo "Disabling CPU turbo boost"
            echo 1 | sudo tee /sys/devices/system/cpu/intel_pstate/no_turbo >/dev/null

            if [ `cat /sys/devices/system/cpu/intel_pstate/no_turbo` -eq 0 ]; then
                echo "Failed to disable CPU turbo boost"
                exit
            else
                echo "Disabled CPU tubo boost"
            fi
        fi
    fi
}



function enable_turbo() {
    if [ -f /sys/devices/system/cpu/intel_pstate/no_turbo ]; then
        if [ `cat /sys/devices/system/cpu/intel_pstate/no_turbo` -eq 1 ]; then
            # enable turbo
            echo "Enabling CPU turbo boost (requires sudo)"
            echo 0 | sudo tee /sys/devices/system/cpu/intel_pstate/no_turbo >/dev/null

            if [ `cat /sys/devices/system/cpu/intel_pstate/no_turbo` -eq 1 ]; then
                echo "Failed to enable CPU turbo boost"
                exit
            else
                echo "Enabled CPU turbo boost"
            fi
        fi
    fi
}

function statusbar() {
    local KEEP_LOOPING=true
    printf "Thread  %5s/%8s   %12s\n" "Ndone" "Ntotal" "complete %"
    while $KEEP_LOOPING;
    do
        KEEP_LOOPING=false
        for i in $(seq 0 $NCORES1); do
            DONE=`grep --text . "$TMP/binary_c_pgo.$BUILD.$i" | wc -l | gawk "{print \\\$1}"`
            PCDONE=`echo "$DONE $REPEAT_PER_CORE" | gawk "{print 100.0*\\\$1/\\\$2}"`

            # output status: note that "\33[2K\r" clears the line first
            printf "\33[2K\r%3d  %8d/%8d %12.3f %% " $i $DONE $REPEAT_PER_CORE $PCDONE

            # show CPU info if we have it
            if [ $HAVE_CPUINFO -eq 1 ]; then
                # -A10 is a hack : is this enough for your system? not sure!
                local CPUMHZ=`cat /proc/cpuinfo |grep 'processor\s\+\:\s\+'\$i -A10|grep cpu\ MHz|head -1|gawk "{print \\\$4}"`
                printf "  %g MHz" $CPUMHZ
            fi
            if [ $DONE == 0 ]; then
                printf "(warming up, please wait)"
            fi
            printf "\n"

            if ! [ $DONE -ge $REPEAT_PER_CORE ]; then
                KEEP_LOOPING=true
            fi
        done

        if [ $KEEP_LOOPING = true ]; then
            sleep $STATUS_SLEEP
            echo -n -e '\033['$NCORES'A'
        fi
    done;
}

# function to kill child processes
function KillChildren() {
    local pid="${1}"
    local self="${2:-false}"
    if children="$(pgrep -P "$pid")"; then
        for child in $children; do
            KillChildren "$child" true
        done
    fi

    if [ "$self" == true ]; then
        #kill -s SIGTERM "$pid" || (sleep 10 && kill -9 "$pid" &)
        kill -9 "$pid"
    fi
}

function run_stellar_list() {
    for i in $(seq 0 $NCORES1); do
        echo "Launch tbse on core $i"
        if ! [ $DUMMY -eq 1 ]; then
            taskset -c "$i" "$TBSE" "system_list" "$TMP/pgo_list_$i" "$WARMUP" | grep At | awk '{print substr($0, index($0,$5))}' > "$TMP/binary_c_pgo.$BUILD.$i" &
        fi
        sleep "$SLEEP"
    done
    statusbar
    wait
}

function demo() {
    echo "repeat = $REPEAT_PER_CORE"
    exit
    }
