#!/usr/bin/env perl
use strict;
#
# The binary_c stellar population nucleosynthesis framework.
#
# Copyright Robert Izzard 2000-2021
#
# Contact rob.izzard@gmail.com
#
# Code repository:
# https://gitlab.com/binary_c
#
# Documentation is in doc/
# Code is in src/
#
# To build, run
#
# ---
#
# meson builddir
# cd builddir
# ninja binary_c_install
#
# ---
#
# Please see the files README, LICENCE and CHANGES,
# and the doc/ directory for documentation which is mirrored
# at https://binary_c.gitlab.io/binary_c.html
#
############################################################
#
# Purpose:
#
# Short perl script for Meson build: this checks subdirs and
# builds data objects that haven't been built
#
# If environment variable REBUILD_BINARY_C_DATA_OBJECTS
# is 1, rebuild them even if they already exist.
#
############################################################

my $vb=0;
my $cc=$ENV{CC}//'cc';
my $objdump = `objdump -f /bin/ls 2>\&1`;
my $binary_arch = ($objdump=~/architecture\:\s+([^,]+)/)[0];
my $output_target = ($objdump=~/file format (\S+)/)[0];
my $objcopy_opts = "-I binary -B $binary_arch -O $output_target";
make_dataobjects();
exit 0;

sub make_dataobjects
{
    # each src/* directory can contain a script
    # called make_data_objects.sh
    #
    # If found, run the script to generate the
    # appropriate data objects. This means you
    # don't have to put data into .h files anymore,
    # it can be stored in ascii files which are converted
    # with these scripts.
    #
    # Note that these script can also use sed/grep/tr etc.
    # to convert .h files into ascii which is then
    # converted into a data object. This is done for legacy code.
    #
    my @dataobjects;
    print "making dataobjects\n"if($vb);
    opendir(my $src,'./src') or die ("opendir() on ./src failed : $!\n");
    while(my $d = readdir($src))
    {
        next if ($d eq '.' || $d eq '..');
        if(defined $d)
        {
            my $f = './src/'.$d.'/make_data_objects.sh';
            if( -d './src/'.$d && -f -e -s $f)
            {
                print "Running script $f\n";
                my $cmd = "cd src/$d; CC=$cc OBJCOPY_OPTS=\"$objcopy_opts\" ./make_data_objects.sh 2>\&1";
                print "cmd = $cmd\n";
                my $r = `$cmd`;
                print $r,"\n\n\n\n";
                my @objects = grep {/^DATAOBJECT/} split(/\n/,$r);
                map
                {
                    s/^/$d\//;
                }@objects;
                push(@dataobjects,@objects);
            }
        }
    }
    closedir $src;
    chomp @dataobjects;
    @dataobjects = map {s/DATAOBJECT\s+// && $_} sort @dataobjects;
    print "Linking in the following data objects : @dataobjects\n";
}
