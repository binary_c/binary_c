#!/bin/bash
array=("$@")
sorted=($(printf "%s " "${array[@]}" | sort))
for element in "${sorted[@]}"
do
    echo -n "$element "
done
echo
