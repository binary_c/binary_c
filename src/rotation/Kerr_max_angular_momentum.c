#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Maximum angular momentum from the Kerr solution
 * to Einstein's equations.
 *
 * Input is mass in solar units.
 */

double Pure_function Kerr_max_angular_momentum(const double mass)
{
    /* return value is in code units */
    return Angular_momentum_cgs_to_code(
        Kerr_max_angular_momentum_cgs(mass)
        );
}

double Pure_function Kerr_max_angular_momentum_cgs(const double mass)
{
    /* return value is in cgs */
    return GRAVITATIONAL_CONSTANT * Pow2(mass * M_SUN) /
        SPEED_OF_LIGHT;
}
