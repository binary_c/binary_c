#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Calculate the moment of inertia of a star's core
 * given the core_type (if core_type < 0, use Outermost_core_type(star)
 * instead
 */
double Pure_function core_moment_of_inertia(const struct star_t * Restrict const star,
                                            const double core_radius,
                                            Core_type core_type)
{
    if(core_type < 0)
    {
        core_type = Outermost_core_type(star);
    }

    return
        HAS_BSE_CORE(star->stellar_type) ?
        (CORE_MOMENT_OF_INERTIA_FACTOR * star->core_mass[core_type] * Pow2(core_radius)) :
        0.0;
}
