#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Given a star's angular momentum and
 * calculated moment of inertia, calculate its
 * angular velocity (omega) in code units
 * (probably yr^-1)
 *
 * Note that if the moment of inertia is zero,
 * and the star is not a remnant, this is formally
 * a bug. In this case we just return 0.0.
 */
double Pure_function omegaspin(struct stardata_t * stardata,
                               const struct star_t * Restrict const star)
{
    if(star->stellar_type==MASSLESS_REMNANT ||
       Is_zero(star->effective_radius))
    {
        return 0.0;
    }
    else
    {
        const double I = moment_of_inertia(stardata,
                                           star,
                                           star->effective_radius);
        return Is_not_zero(I) ? (star->angular_momentum/I) : 0.0;
    }
}
