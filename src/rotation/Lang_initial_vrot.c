#include "../binary_c.h"
No_empty_translation_unit_warning;


Constant_function double Lang_initial_rotation(const double m)
{
    /*
     * (much) Later on... indeed this is true. Lang_initial_rotation is used to set
     * the ZAMS spin of a star according to some archaic data set
     * from Lang(1992).
     *
     * See Hurley, Phd Thesis, 2000 (University of Cambridge)
     * page 63 for details (his function was "Lang_initial_rotation").
     */
    return 330.0 * pow(m,3.3) / (15.0 + pow(m,3.45)); // km/s
}
