#pragma once
#ifndef ROTATION_H
#define ROTATION_H

#include "rotation_parameters.def"

#undef X
#define X(CODE) OVERSPIN_##CODE,
enum { OVERSPIN_PARAMETERS_LIST };
#undef X

#define X(CODE,NUM) VROT_##CODE = (NUM),
enum { VROT_PARAMETERS_LIST };
#undef X

#define X(CODE) ANGULAR_MOMENTUM_##CODE,
enum { ANGULAR_MOMENTUM_LABELS_LIST };
#undef X



#endif // ROTATION_H
