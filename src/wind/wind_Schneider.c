#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "wind.h"
#include "wind_Schneider.h"

/*
 * Wind mass-loss rate based on Fabian Schneider's routine
 */

double wind_Schneider(WIND_PROTOTYPE_ARGS)
{
    const Boolean debug = FALSE;
    double mdot_Schneider = 0.0;

    if(star->stellar_type < HeWD)
    {
        if(debug)
        {
            Printf("wind Schneider M=%g %s L=%g %s R=%g %s\n",
                   star->mass,M_SOLAR,
                   star->luminosity,L_SOLAR,
                   star->radius,R_SOLAR);
        }

        /*
         * Zbase is the original metallicity.
         * Use the "effective metallicity".
         */
        const double Zbase = stardata->common.effective_metallicity;

        /*
         * switch between hot, cool, WR and super-massive star winds
         * we blend the hot and cool winds at Teff=10^4K via an interpolation
         */
        const double T_high = 1.1e4;
        const double T_low  = 1.0e4;
        const double Tsurf = Teff_from_star_struct(star);

        if(debug)
        {
            Printf("Tsurf = %g cf T_low = %g, T_high = %g\n",
                   Tsurf,
                   T_low,
                   T_high);
        }

        if(Tsurf <= T_low)
        {
            mdot_Schneider = eval_lowT_Schneider(SCHNEIDER_CALL_ARGS);
        }
        else if(Tsurf >= T_high)
        {
            mdot_Schneider = eval_highT_Schneider(SCHNEIDER_CALL_ARGS);
        }
        else
        {
            const double w1 = eval_lowT_Schneider(SCHNEIDER_CALL_ARGS);
            const double w2 = eval_highT_Schneider(SCHNEIDER_CALL_ARGS);
            const double f = (Tsurf - T_low) / (T_high - T_low);
            mdot_Schneider = (1.0 - f) * w1 + f * w2;
        }

        /*
         * Apply LBV-type mass-loss if needed
         */
        const double mdot_lbv = eval_LBV(SCHNEIDER_CALL_ARGS);
        mdot_Schneider = Max(mdot_Schneider, mdot_lbv);

        /*
         * Apply some global limitations as is done in
         * binary_c's wind mass-loss routine
         */

        /* limit to the dynamical rate of the envelope */
        const double mdot_dyn =
            Max(0.1*star->mass, envelope_mass(star)) /
            Max(1e-20,dynamical_timescale_envelope(star));

        /*
         * limit to 10% wind mass loss change in any one timestep
         *
         * Normally, this is never required. But if there is a merger
         * between a massive helium star and a main sequence star, a
         * massive CHeB star is formed. The LBV mass loss rate can then
         * lead to very rapid mass loss which exceeds 100% per time step.
         * Even 10% is still fast, but at least manageable by the
         * evolution algorithm.
         */
        const double mdot_tenpc = star->mass * 0.1 / stardata->model.dt;

        /*
         * Never remove a neutron-star or black-hole core.
         */
        const double mdot_compact =
            (Core_exists(star,CORE_NEUTRON) ? (star->mass - star->core_mass[CORE_NEUTRON]) :
             Core_exists(star,CORE_BLACK_HOLE) ? (star->mass - star->core_mass[CORE_BLACK_HOLE]) :
             1e100) / stardata->model.dt;

        mdot_Schneider = Min4(mdot_Schneider,
                              mdot_dyn,
                              mdot_tenpc,
                              mdot_compact);
    }
    else
    {
        /*
         * no mass loss for remnants, except post-AGB stars
         */
        if(WHITE_DWARF(stellar_type))
        {
            mdot_Schneider = wind_WD(WIND_CALL_ARGS);
        }
        else
        {
            mdot_Schneider = 0.0;
        }
    }
    return mdot_Schneider;
}

/*
 * hot star wind: WR and OB stars
 */
static double eval_highT_Schneider(SCHNEIDER_PROTOTYPE_ARGS)
{
    const double Mdot_Vink = eval_Vink_wind(SCHNEIDER_CALL_ARGS);
    const double Mdot_WR = eval_Schneider_WR_wind(SCHNEIDER_CALL_ARGS);
    const double mdot = Max(Mdot_Vink, Mdot_WR);
    //printf("debug %g %g %i %g %g %g %g %g\n", stardata->model.time, star->mass, star->stellar_type, Tsurf, star->luminosity, Mdot_Vink, Mdot_WR, mdot);

    if(debug==TRUE)
    {
        printf("log10 Schneider_wind : high temp. = %g \n", log10(mdot));
    }

    return mdot;
}

static double eval_lowT_Schneider(SCHNEIDER_PROTOTYPE_ARGS)
{
    /*
     * mass-loss rates for cool stars
     */
    const double mdot_GB = wind_GB(WIND_CALL_ARGS);
    const double mdot_AGB = wind_AGB(WIND_CALL_ARGS);
    double mdot_NJ = 0.0;
    double mdot = 0.0;

    /*
     * apply Hurley-like luminosity cut for wind mass-loss
     * of massive stars across the "cool" part of HRD (i.e.
     * where Vink and WR rates do not apply)
     */
    if(star->luminosity > stardata->preferences->wind_Nieuwenhuijzen_luminosity_lower_limit)
    {
        /*
         * cool star wind
         */
        mdot_NJ = eval_Nieuwenhuijzen_wind(SCHNEIDER_CALL_ARGS);

        /*
         * apply metallicity scaling of cool star winds as found by Mauron & Josselin (2011)
         */
        mdot_NJ *= pow(Zbase/VINK_SOLAR_METALLICITY, 0.5);
    }

    mdot = Max3(mdot_GB,
                mdot_AGB,
                mdot_NJ);

    if(debug==TRUE)
    {
        printf("log10 Schneider_wind : low temp. = %g \n", log10(mdot));
    }

    return mdot;
}

static double eval_Schneider_WR_wind(SCHNEIDER_PROTOTYPE_ARGS)
{
    /*
     * WR star wind
     *
     * We use the WNE rates given by Yoon 2017, MNRAS
     */
    const double Zsun = 0.02;
    const double f_WR = 1.58;
    double mdot = 0.0;
    double Mdot_WNE;

    Mdot_WNE = f_WR * exp10( -11.32) * pow(star->luminosity, 1.18) * pow(Zbase/Zsun, 0.60);
    //Mdot_WNE = f_WR * 1e-13 * pow(star->luminosity, 1.5) * pow(Zbase/Zsun, 0.86); // Belczynski WR winds

    if(star->stellar_type>=HeMS)
    {
        mdot = Mdot_WNE;
        Dprint("He-star WR %g\n",mdot);
    }
    else
    {
        const double mu = WR_mu(star);
        if(mu < 1.0)
        {
            mdot = Mdot_WNE * (1.0 - mu);
        }
        else
        {
            mdot = 0.0;
        }
        Dprint("H-star WR %g (mu=%g)\n",mdot,mu);
    }

    //double Mdot_WC;
    //Mdot_WC = f_WR * exp10( -9.20) * pow(star->luminosity, 0.85) * pow(Y, 0.44) * pow(Zbase/Zsun, 0.25);
    //
    //mdot = 0.0;
    //if (Y >= 1.0-Zbase)
    //{
    //    mdot = Mdot_WNE;
    //}
    //else if (Y < 0.90)
    //{
    //    mdot = Mdot_WC;
    //}
    //else
    //{
    //    x = (1.0-Zbase-Y)/(1-Zbase-0.9);
    //    mdot = (1.0-x)*Mdot_WNE + x*Mdot_WC;
    //}

    if(debug==TRUE)
    {
        printf("Schneider_wind = WNE Yoon 2017 %g\n", log10(mdot));
    }

    return mdot;
}


/*
 * Vink et al. (2000, 2001) copied from winds.f90
 * Modification: take wind-enhancement because of continuum
 * driving into account when approaching the Eddington limit
 */
static double eval_Vink_wind(SCHNEIDER_PROTOTYPE_ARGS)
{
    const double zz = Zbase/VINK_SOLAR_METALLICITY;
    double alpha;
    /*
     * interpolating factor: alpha = 1 for hot side, = 0 for cool side
     */
    if(Tsurf > 27500.0)
    {
        alpha = 1.0;
    }
    else if(Tsurf < 22500.0)
    {
        alpha = 0.0;
    }
    else
    {
        /*
         * use Vink et al 2001, eqns 14 and 15 to set "jump" temperature
         */
        const double Teff_jump = 1e3*(61.2 + 2.59*(-13.636 + 0.889*log10(zz)));
        const double dT = 100.0;
        alpha =
            Tsurf > (Teff_jump + dT) ? 1.0 :
            Tsurf < (Teff_jump - dT) ? 0.0 :
            ((Tsurf - (Teff_jump - dT)) / (2.0*dT));
    }

    double w1,w2;
    if(alpha > 0.0)
    {
        /*
         * eval hot side wind (eqn 24)
         */
        if(stardata->preferences->wind_mass_loss == WIND_ALGORITHM_BINARY_C_2022)
        {
            /*
             * Use the updated Sabahit et al. (2022) fit
             */
            w1 = wind_Sabahit_2022(stardata, star);
        }
        else
        {
            /*
             * Use Vink's (2001) fit
             */
            w1 = wind_Vink2001(stardata,star);
        }
    }
    else
    {
        w1 = 0.0;
    }

    if(alpha < 1.0)
    {
        /*
         * eval cool side wind (eqn 25)
         */
        const double vinf_div_vesc =
            1.3 * /* this is the cool side Galactic value */
            pow(zz,0.13); /* corrected for Z */
        w2 = exp10(
                 - 6.688
                 + 2.210*log10(star->luminosity/1e5)
                 - 1.339*log10(star->mass/30)
                 - 1.601*log10(vinf_div_vesc/2.0)
                 + 1.07*log10(Tsurf/2e4)
                 + 0.85*log10(zz));
    }
    else
    {
        w2 = 0.0;
    }

    const double mdot = alpha * w1 + (1.0 - alpha) * w2;

    if(debug)
    {
        printf("vink wind %g %g %g\n", mdot, Zbase, VINK_SOLAR_METALLICITY);
    }
    return mdot;
}


/*
 * Nieuwenhuijzen, H., de Jager, C. 1990, A&A, 231, 134 (Eq. 2)
 */
static double eval_Nieuwenhuijzen_wind(SCHNEIDER_PROTOTYPE_ARGS)
{
    double mdot = exp10(
                      -14.02 +
                      1.24*log10(star->luminosity) +
                      0.16*log10(star->mass) +
                      0.81*log10(star->radius));

    if(debug)
    {
        Printf("Nieuwenhuijzen log10 wind %g\n",log10(mdot));
    }

    return mdot;
}

/*
 * LBV-like mass loss rate
 */
static double eval_LBV(SCHNEIDER_PROTOTYPE_ARGS)
{
    double mdot_LBV = 0.0;
    const double x = 1e-5 * star->radius * sqrt(star->luminosity) - 1.0;

    if(star->stellar_type >= HERTZSPRUNG_GAP &&
       luminosity > stardata->preferences->wind_LBV_luminosity_lower_limit &&
       x > 0.0)
    {
        mdot_LBV = 1.5e-4; // Belczynski+2010 model
    }
    else
    {
        mdot_LBV = 0.0;
    }

    mdot_LBV *= stardata->preferences->wind_type_multiplier[WIND_TYPE_LBV];

    Dprint("mdotLBV check stellar_type=%d radius=%g lum=%g hence x=(10^-5*R*sqrt(L)-1.0)=%g -> mdot_LBV=%g\n",
           stellar_type,radius,luminosity,x,mdot_LBV);

    return mdot_LBV;
}
