#pragma once
#ifndef WIND_PROTOTYPES_H
#define WIND_PROTOTYPES_H
#include "wind.h"

double wind_mass_loss_rate(WIND_PROTOTYPE_ARGS);
double wind_other(WIND_PROTOTYPE_ARGS) Constant_function;
double wind_GB(WIND_PROTOTYPE_ARGS) Pure_function;
double wind_AGB(WIND_PROTOTYPE_ARGS);
double wind_WD(WIND_PROTOTYPE_ARGS);
double wind_postAGB(WIND_PROTOTYPE_ARGS);

double Constant_function_if_no_debug wind_LBV(WIND_PROTOTYPE_ARGS);
double Pure_function wind_WR(WIND_PROTOTYPE_ARGS);
void wind_multiplicative_factors(double * const mdot,
                WIND_PROTOTYPE_ARGS);
double wind_enhancement_factors(double * const mdot,
                WIND_PROTOTYPE_ARGS);
double wind_Hurley2002(WIND_PROTOTYPE_ARGS);
double wind_Schneider(WIND_PROTOTYPE_ARGS);
double wind_momentum_rate(struct star_t * Restrict const star);
Boolean no_wind(struct stardata_t * const stardata Maybe_unused);
double Pure_function WR_mu(const struct star_t * Restrict const star);
double wind_Sander_and_Vink_2020(struct stardata_t * const stardata,
                                 struct star_t * const star,
                                 const double Z,
                                 int method);


double wind_binary_c_2020(WIND_PROTOTYPE_ARGS);
double wind_binary_c_2022(WIND_PROTOTYPE_ARGS);
double Pure_function wind_Reimers(struct star_t * const star,
                                  const double eta);
double Pure_function wind_mixing_factor(const struct stardata_t * const stardata,
                                        const Star_number k);
double wind_velocity(struct stardata_t * const stardata,
                     struct star_t * const star);
double Mira_wind_velocity(const double period);
double Constant_function_if_no_debug wind_Tout_Pringle_1992(WIND_PROTOTYPE_ARGS);
double wind_Vink2001(
    const struct stardata_t * const stardata,
    const struct star_t * const star
    );
double wind_Sabahit_2022(const struct stardata_t * const stardata,
                         const struct star_t * const star);
double wind_Krticka2020(WIND_PROTOTYPE_ARGS);


#endif // WIND_PROTOTYPES_H
