#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "wind.h"

Constant_function double wind_other(WIND_PROTOTYPE_ARGS)
{
    double mdot;

    /*
     * Mass loss rates not defined in any particular stellar phase
     */
    const double Lover = luminosity - stardata->preferences->wind_Nieuwenhuijzen_luminosity_lower_limit;
    Dprint("Lcheck %g\n",
           Lover);

    if(Is_not_zero(stardata->preferences->nieuwenhuijzen_windfac) &&
       Lover > 0.0)
    {
        /*
         * Apply mass loss of Nieuwenhuijzen & de Jager, A&A,
         * 1990, 231, 134, for massive stars over the entire HRD.
         */
        mdot = 9.6e-15*pow(radius,0.81)*pow(luminosity,1.24)*
            pow(mass,0.16)*sqrt(metallicity/0.02);
#ifdef NIEUWENHUIJZEN_FUDGE_FACTOR
        const double x = Min(1.0,Lover/500.0);
        mdot *= x;
#endif
        mdot *= stardata->preferences->nieuwenhuijzen_windfac;
        Dprint("N+dJ %g\n",mdot);
    }
    else
    {
        mdot = 0.0;
    }

    mdot *= stardata->preferences->wind_type_multiplier[WIND_TYPE_OTHER];

    return mdot;
}
