#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "wind.h"

/* wind loss rate for Wolf-Rayet stars */

#ifdef NUCSYN
#define CHEMICALLY_DEPENDENT_WINDS
#endif

#ifdef CHEMICALLY_DEPENDENT_WINDS
static double Dray_MM(WIND_PROTOTYPE_ARGS,double teff, int wr_type);
static double Dray_NL(WIND_PROTOTYPE_ARGS,double teff, int wr_type);
static double Eldridge(WIND_PROTOTYPE_ARGS,double teff);
static double dejager2(const double teff,const double lum,const double factor);
#endif

double Pure_function wind_WR(WIND_PROTOTYPE_ARGS)
{
    const double menv = envelope_mass(star);
    double mdot = 0.0; // default to zero

    if(stardata->preferences->wr_wind==WR_WIND_BSE)
    {
        /* Hurley et al 2002 mass loss */
       if(stellar_type>=HeMS)
        {
            mdot = 1.0e-13*Pow1p5(luminosity); // as below with mu=0
            Dprint("he-star WR %g\n",mdot);
        }
        else
        {
            const double mu = (menv/mass) *
                Min(5.0,Max(1.20,
                            pow((luminosity/LUM0),KAP)));
            if(mu<1.0)
            {
                mdot = 1.0e-13 * Pow1p5(luminosity) * (1.0 - mu);
                Dprint("H-star WR %g (L=%g, mu=%g)\n",mdot,luminosity,mu);
            }
            else
            {
                mdot = 0.0;
            }
        }
    }

#ifdef CHEMICALLY_DEPENDENT_WINDS
    else
    {

        double teff = 1000.0*pow((1130.0*luminosity/Pow2(radius)),0.25);

        if((stellar_type>MAIN_SEQUENCE)&&(stellar_type<HeWD)
           &&
           /*
            * extra checks to prevent WR type wind
            * loss being applied to CPNe stars
            */
           (stellar_type!=TPAGB)&&(star->effective_zams_mass>8))
        {
            Dprint("Getting WR type - stellar type =%d\n",stellar_type);

            int wr_type = nucsyn_WR_type(star);
            Dprint("WR type=%d\n",wr_type);

            /* Wolf-Rayet wind options */
            Dprint("wind %d M=%12.12e t=%12.12e\n",
                   stardata->preferences->wr_wind,
                   mass,
                   stardata->model.time);

            /* Lynnette's wind loss rates */
            if(stardata->preferences->wr_wind==WR_WIND_MAEDER_MEYNET)
            {
                mdot = Dray_MM(WIND_CALL_ARGS,teff,wr_type);
            }
            else if(stardata->preferences->wr_wind==WR_WIND_NUGIS_LAMERS)
            {
                mdot = Dray_NL(WIND_CALL_ARGS,teff,wr_type);
            }
            // John Eldridge's mass loss rates
            else if((stardata->preferences->wr_wind==WR_WIND_ELDRIDGE)
                    &&(luminosity>1)&&
                    (stellar_type!=TPAGB)&&
                    (stellar_type!=GIANT_BRANCH)&&
                    (stellar_type<10)
                )
            {
                mdot = Eldridge(WIND_CALL_ARGS,teff);
            }
        } // end special massive star wind types
    }
#endif // CHEMICALLY_DEPENDENT_WINDS

    mdot *= stardata->preferences->wind_type_multiplier[WIND_TYPE_WR];

    return mdot;
}


#ifdef CHEMICALLY_DEPENDENT_WINDS

static double Dray_MM(WIND_PROTOTYPE_ARGS,double teff,int wr_type)
{
    /* Maeder and Meynet version */
    double dmt;
    if(wr_type==WR_WNL)
    {
        /* twice the Conti(1998) value */
        dmt=8e-5;
        Dprint("wind MM (2xConti) %12.12e\n",dmt);
    }
    else if((wr_type==WR_WNE)||(wr_type==WR_WC)||(wr_type==WR_WO))
    {
        /* Langer 1989 (Lynnette says to multiply by a factor
         * 0.6 because Langer's values are too high)
         */
        dmt=0.6*1e-7*pow(mass,2.5);
        Dprint("wind MM (langer) %12.12e\n",dmt);
    }
    else
    {
        /* use mass loss rate of de Jager 1988 * 2.0 for pre-WR stage */
        dmt=dejager2(teff,luminosity,1.0);
        dmt=exp10(dmt);
        dmt*=2.0;
        // scale with Z
        dmt *= sqrt(stardata->common.metallicity/0.02);
        Dprint("wind de Jager 1998 (*2) %12.12e\n",dmt);
    }
    return(dmt);
}

static double Dray_NL(WIND_PROTOTYPE_ARGS,double teff,int wr_type)
{
    double dmt;
    /* Nugis and Lamers version */
    if((wr_type==WR_WNL)||(wr_type==WR_WNE))
    {
        /* this is actually the logged value */
        dmt=-13.6+1.63*log10(luminosity)+
            2.22*log10(Max(star->Xenv[XHe4],1e-14));
        dmt=exp10(dmt);
        Dprint("wind NL (WN) %12.12e\n",dmt);
    }
    else if(wr_type==WR_WC)
    {
        /* again the logged value */
        dmt=-8.3+0.84*log10(luminosity)+
            2.04*log10(Max(star->Xenv[XHe4],1e-14))+
            1.04*log10(1.0-star->Xenv[XH1]
                       -star->Xenv[XHe4]);
        //1.04*log10(stardata->common.metallicity);
        dmt=exp10(dmt);
        Dprint("wind NL (WC/O) %12.12e\n",dmt);
    }
    else if(wr_type==WR_WO)
    {
        dmt=1.9e-5;
    }
    else
    {
        /* use mass loss rate of de Jager 1988 * 1.0 for pre-WR stage*/
        dmt=dejager2(teff,luminosity,1.0);
        dmt=exp10(dmt);
        // scale with Z
        dmt *= sqrt(stardata->common.metallicity/0.02);
        Dprint("wind de Jager 1998 %12.12e\n",dmt);

    }
    return(dmt);
}

static double Eldridge(WIND_PROTOTYPE_ARGS,double teff)
{

    double rvin,rmva,rmvb,rmjj,rmwr,rmjnaccretor,COHe ;
    const double eldridge_power=0.5;

    /* The standard pre-WR rate is the De Jager rate */

    rmjj=dejager2(teff,luminosity,1.0);
    rmjnaccretor=pow(50.0*metallicity,eldridge_power)*exp10(rmjj);

    /* Apply correction due to Vink */

    /* I have cleaned up John's code so it is readable and used the
     * In_range macro where In_range(X,A,B) returns true if
     * X is in the range A to B (inclusive)
     */
    if(In_range(teff,1e4,6e4))
    {
        rvin=pow(metallicity*50.0,0.13);
        rmva=-6.697+2.194*log10(luminosity/1e5)-1.313*log10(mass/30.0)
            -1.226*log10(rvin*2.6/2.0)+0.933*log10(teff/4e4)
            -10.92*Pow2(log10(teff/4e4))
            +0.85*log10(50.0*metallicity);
        rmvb=-6.688+2.210*log10(luminosity/1e5)-1.339*log10(mass/30.0)
            -1.601*log10(rvin*1.3/2.0)+1.07*log10(teff/2e4)
            +0.85*log10(50.0*metallicity);

        if(In_range(teff,1e4,1.25e4))
        {
            rmjnaccretor=((teff-1e4)*exp10(rmvb)+(1.25e4-teff)*exp10(rmjj)
                   *pow(50.0*metallicity,eldridge_power))/(2.5e3);
        }
        else if(In_range(teff,1.25e4,2.25e4))
        {
            rmjnaccretor=exp10(rmvb);
        }
        else if(In_range(teff,2.25e4,2.75e4))
        {
            rmjnaccretor=((teff-2.25e4)*exp10(rmva)+(2.75e4-teff)*exp10(rmvb))/5e3;
        }
        else if(In_range(teff,2.75e4,5e4))
        {
            rmjnaccretor=exp10(rmva);
        }
        else //(In_range(teff,5e4,6e4))
        {
            rmjnaccretor=((6e4-teff)*exp10(rmva)+(teff-5e4)*exp10(rmjj)
                   *pow(50.0*metallicity,eldridge_power))/1e4;
        }
    }
    Dprint("JJE wind post-vink %12.12e\n",rmjnaccretor);

    /* Apply Nugis and Lamers correction */

    //     !!!!WOLF RAYET MASS-LOSS!!!!!
    rmwr=0.0;
    if(star->Xenv[XH1]<0.4)
    {
        rmjj=-13.6+1.63*log10(luminosity)+
            2.22*log10(Max(1e-14,star->Xenv[XHe4]));
        rmwr=(pow(metallicity*50.0,eldridge_power))*exp10(rmjj);
    }
    // WNE:
    if(star->Xenv[XH1]<1e-3)
    {
        // CO/He by number
        COHe=((star->Xenv[XC12]/12.0+
               star->Xenv[XC13]/13.0+
               star->Xenv[XO16]/16.0)/
              (1e-14+star->Xenv[XHe4]/4.0));

        if(Less_or_equal(COHe,3e-2))
        {
            // WNE stars
            rmjj=-13.6+1.63*log10(luminosity)+
                2.22*log10(Max(star->Xenv[XHe4],1e-14));
            rmwr=(pow(metallicity*50.0,eldridge_power))*exp10(rmjj);
        }
        // WC:
        else if(More_or_equal(COHe,3e-2)&&(COHe<1.0))
        {
            rmjj=-8.3+0.84*log10(luminosity)+
                2.04*log10(Max(1e-14,star->Xenv[XHe4]))
                +1.04*log10(Max(1.0-star->Xenv[XHe4]-star->Xenv[XH1],1e-14));
            rmwr=(pow(metallicity*50.0,eldridge_power))*exp10(rmjj);
        }
        //     WO:
        else // COHe > 1.0
        {
            rmwr=(pow(metallicity*50.0,eldridge_power))*1.9e-5;
        }
    }
    if(rmwr>0.0)
    {
        if(teff>1e4)
        {
            rmjnaccretor=rmwr;
        }
        else if(teff>5e3)
        {
            rmjnaccretor=((teff-5e3)*rmwr+(1e4-teff)*rmjnaccretor)*2e-4;
        }
    }
    Dprint("JJE wind final (post NL) %12.12e\n",rmjnaccretor);

    return(rmjnaccretor);
}




static double dejager2(const double teff,const double lum,const double factor)
{
    static const double aij[6][6]={{6.34916,-5.04240,-0.83426,-1.13925,-0.12202,0},
                                   {3.41678,0.15629,2.96244,0.33659,0.57576,0},
                                   {-1.08683,0.41952,-1.37272,-1.07493,0,0},
                                   {0.13095,-0.09825,0.13025,0,0,0},
                                   {0.22427,0.46591,0,0,0,0},
                                   {0.11968,0,0,0,0,0}};

    double zml1=(log10(teff)-4.050)/0.75;
    double zml2=(log10(lum)-4.60)/2.10;

    double rmjj;
    int n2,i2,naccretor;

    if(Abs_more_than_or_equal(zml1,1.0)||
       Abs_more_than_or_equal(zml2,1.0))
    {
        // outside validity region... use other rates
        rmjj=-20.0;
    }
    else
    {
        rmjj=0.0;
        for(n2=0;n2<6;n2++)
        {
            for(i2=0;i2<=n2;i2++)
            {
                naccretor=n2-i2;
                // NB switched i2 and naccretor (fortran<->C)
                rmjj -= aij[naccretor][i2]*
                    cos((double)naccretor*acos(zml1))*
                    cos((double)i2*acos(zml2));
            }
        }
    }

    return (factor*rmjj);
}

#endif
