#include "../binary_c.h"
No_empty_translation_unit_warning;

double Mira_wind_velocity(const double period)
{
    /*
     * calculate wind velocity at infinity for a Mira
     *
     * This formula is problematic if period < 13.5/0.056 = 241 days,
     * hence limit the period min to 300 days.
     */
    return Max(1.0e5 * (-13.5+0.056*Max(period,300.0)),0.0) ;
}
