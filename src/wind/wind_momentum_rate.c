#include "../binary_c.h"
No_empty_translation_unit_warning;


double Constant_function wind_momentum_rate(struct star_t * Restrict star)
{
    /*
     * Wind momentum rate :
     * consider loss only, ignore accretion
     *
     * return in CGS units
     */
    double pdot =
        star->vwind *
        star->derivative[DERIVATIVE_STELLAR_MASS_WIND_LOSS] *
        1e5 * M_SUN / YEAR_LENGTH_IN_SECONDS;
    
    return pdot;
}
