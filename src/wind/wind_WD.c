#include "../binary_c.h"
No_empty_translation_unit_warning;
#include "wind.h"

/*
 * Mass-loss rate for white dwarfs
 */

double wind_WD(WIND_PROTOTYPE_ARGS)
{
    return Max(
        stardata->preferences->postagbwind != POSTAGB_WIND_NONE ? 0.0 : wind_postAGB(WIND_CALL_ARGS),
        0.0
        );
}
