#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "wind.h"

/*
 * Wind mass-loss rate based on Fabian Schneider's routine,
 * with Sander and Wink (2020) for helium stars.
 */

double wind_binary_c_2020(WIND_PROTOTYPE_ARGS)
{
    double mdot;
    if(WHITE_DWARF(stellar_type))
    {
        mdot = wind_WD(WIND_CALL_ARGS);
    }
    else if(stardata->preferences->wind_mass_loss == WIND_ALGORITHM_BINARY_C_2020 &&
            star->stellar_type == HeMS && //NAKED_HELIUM_STAR(star->stellar_type) &&
            log10(star->luminosity) > 4.85)
    {
        /*
         * Sander and Vink 2020
         * https://arxiv.org/pdf/2009.01849.pdf
         * for Wolf-Rayet stars
         */
        mdot = wind_Sander_and_Vink_2020(stardata,
                                         star,
                                         stardata->common.effective_metallicity,
                                         WIND_SANDER_VINK_2020_GAMMA);
    }
    else
    {
        /*
         * Default to Fabian's 2018 routine
         */
        mdot = wind_Schneider(WIND_CALL_ARGS);
    }

    return mdot;
}
