#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * The mu factor which determines whether a star is a WR star or not
 * according to Hurley's prescription.
 */
double Pure_function WR_mu(const struct star_t * Restrict const star)
{
    double mu;
    if(star->stellar_type >= HERTZSPRUNG_GAP &&
       star->stellar_type <= HeGB)
    {
        if(star->stellar_type>=HeMS)
        {
            mu = 0.0;
        }
        else
        {
            mu = envelope_mass(star) / star->mass *
                Limit_range(pow(star->luminosity/LUM0,KAP),1.20,5.0);
        }
    }
    else
    {
        /* other stars: not "WR" */
        mu = 10.0;
    }

    return (mu);
}
