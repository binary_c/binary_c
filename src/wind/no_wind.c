#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Function to return TRUE if you want to disable stellar winds
 */

Boolean no_wind(struct stardata_t * const stardata Maybe_unused)
{

#ifdef DISCS
    if(stardata->preferences->cbdisc_no_wind_if_cbdisc == TRUE &&
       stardata->common.ndiscs > 0)
    {
        return TRUE;
    }
#endif // DISCS

    return FALSE;
}
