#pragma once
#ifndef WIND_MACROS_H
#define WIND_MACROS_H

/*
 * Wind algorithms
 */

#define Wind_algorithm_string(N) (wind_algorithm_strings[(N)])

/* constants */
#define NIEUWENHUIJZEN_FUDGE_FACTOR
#define VINK_SOLAR_METALLICITY 0.019

#define WIND_PROTOTYPE_ARGS                             \
    const double mass Maybe_unused,                     \
        const double luminosity Maybe_unused,           \
        const double radius Maybe_unused,               \
        const double roche_lobe_radius Maybe_unused,    \
        const double metallicity Maybe_unused,          \
        const double omega Maybe_unused,                \
        const Stellar_type stellar_type Maybe_unused,   \
        struct star_t * const star Maybe_unused,        \
        struct stardata_t * const stardata Maybe_unused

#define WIND_CALL_ARGS                          \
    mass,                                       \
        luminosity,                             \
        radius,                                 \
        roche_lobe_radius,                      \
        metallicity,                            \
        omega,                                  \
        stellar_type,                           \
        star,                                   \
        stardata


#endif // WIND_MACROS_H
