#include "../binary_c.h"
No_empty_translation_unit_warning;
#include "wind.h"

double wind_Krticka2020(WIND_PROTOTYPE_ARGS)
{
    /*
     * Krticka, Kubát and Krticková (2020, A&A 635, A173)
     *
     * Eq. 1. Note that I have introduced factors of 1e-9
     * and converted temperatures to K, and their index
     * runs from 1 to 4, while ours runs from 0 to 3,
     * both inclusive.
     *
     * As they say, this is valid for 10-117kK only, so I
     * limit application to Teff>10kK, but at the high
     * temperature end I limit the Teff used in the calculation
     * to 117kK.
     *
     * Also, white dwarfs require T>105kK to launch a wind
     * (see their section 7.3) so I also put in that limit.
     */
    double mdot;
    const double teff = Teff_from_star_struct(star);
    if(
        (
            stellar_type < HeWD &&
            teff > 10e3
            )
        ||
        (
            WHITE_DWARF(stellar_type) &&
            teff > 105e3
            )
        )
    {
        const double teff_use = Min(teff,117e3);
        const double alpha = 1.63;
        const double zeta = 0.53;
        const double Zsun = 0.0134;
        static const double mi[] = {2.51e-9, 0.72e-9, 0.48e-9, 0.33e-9};
        static const double Ti[] = {18.3e3, 46.4e3, 84.6e3, 116.7e3};
        static const double deltaTi[] = {3.63e3, 17.7e3, 20.4e3, 1.7e3};
        mdot = 0.0;
        for(int i=0; i<4; i++)
        {
            mdot += mi[i] * exp(-Pow2((teff_use - Ti[i])/deltaTi[i]));
        }
        mdot *=
            pow(star->luminosity*1e-3,alpha)
            *
            pow(stardata->common.effective_metallicity/Zsun, zeta);
    }
    else
    {
        mdot = 0.0;
    }
    return mdot;
}
