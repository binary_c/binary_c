#ifndef BATCHMODE_H
#define BATCHMODE_H

#include "batchmode.def"

#undef X
#define X(CODE,NUM) BATCHMODE_##CODE = NUM,
enum { BATCHMODES_LIST };
#undef X

#undef X
#define X(CODE) BATCH_SUBMODE_##CODE,
enum { BATCHMODES_SUBMODES_LIST };
#undef X


#endif // BATCHMODE_H
