#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef BATCHMODE
#include <string.h>
#include <termios.h>
#include <unistd.h>

void batchmode_loop(struct preferences_t * preferences,
                    struct stardata_t * stardata,
                    ticks * start_tick)
{
    /*
     * batchstring contains the argument string
     * batchstring2 is a copy of it (both are destroyed by strtok)
     * batchargs is an array of strings (corresponding to argv)
     */

#define batchstring (stardata->tmpstore->batchstring)
#define batchstring2 (stardata->tmpstore->batchstring2)
#define batchargs (stardata->tmpstore->batchargs)

//#define _FIXED_SIZES

#ifdef _FIXED_SIZES
    /* allocate memory for fixed-size strings : deprecated */
    batchstring = Malloc(sizeof(char)*BATCHMODE_STRING_LENGTH);
    batchstring2 = Malloc(sizeof(char)*BATCHMODE_STRING_LENGTH);
#else
    /* use realloced pointers */
    batchstring = NULL;
    batchstring2 = NULL;
    batchstring = Malloc(sizeof(char)*2*BATCHMODE_STRING_LENGTH);
    batchstring2 = Malloc(sizeof(char)*2*BATCHMODE_STRING_LENGTH);
    size_t nn = 2*BATCHMODE_STRING_LENGTH;
#undef BATCHMODE_STRING_LENGTH
#endif // _FIXED_SIZES

#ifdef TIMER_PER_SYSTEM
    Boolean first=TRUE;
#endif

    /*
     * Prevent tty limits
     */
    struct termios told;
    const Boolean is_tty = isatty(STDIN_FILENO);
    if(is_tty)
    {
        tcgetattr(STDIN_FILENO, &told);
        struct termios tnew = told;
        tnew.c_lflag &= ~ICANON;
        tcsetattr(STDIN_FILENO, TCSAFLUSH, &tnew);
    }

    /* Batch command mode */
    if(Batchmode_is_on(preferences->batchmode))
    {
        /* set flushing on input/output streams */
        force_flushing();

        while(Batchmode_is_on(preferences->batchmode))
        {
            // get input string from command line
#ifdef _FIXED_SIZES
            if(fgets(batchstring,BATCHMODE_STRING_LENGTH,stdin) != NULL)
#else
            const ssize_t ngetline = getline(&batchstring,&nn,stdin);
            if(ngetline)
#endif // _FIXED_SIZES
            {
                /* remove newline */
                const size_t b = strlen(batchstring)-1;
                if(b>1)
                {
                    /* chomp */
                    if(batchstring[b]=='\n') batchstring[b]='\0';

#ifdef _FIXED_SIZES
                    /* copy */
                    strlcpy(batchstring2,
                            batchstring,
                            BATCHMODE_STRING_LENGTH);
#else
                    batchstring2 =
                        Realloc(batchstring2,
                                sizeof(char)*(strlen(batchstring)+1));
                    memcpy(batchstring2,batchstring,strlen(batchstring)+1);
#endif // _FIXED_SIZES

                    /*
                     * Count number of args
                     */
                    size_t n = 0;
                    char * saveptr = NULL;
                    char * c = strtok_r(batchstring," ",&saveptr);
                    while(c)
                    {
                        n++;
                        c = strtok_r(NULL," ",&saveptr);
                    }

                    /*
                     * Act on arg values.
                     */
                    if(n==1 && Strings_equal(batchstring,"go"))
                    {
                        /*
                         * Go command : special case
                         */
                        int evolve_system_return_value;
                        if(!setjmp(stardata->batchmode_setjmp_buf))
                        {
                            /* (Re?)start evolution */
                            evolve_system_return_value = evolve_system(stardata);
                        }
                        else
                        {
                            /* batchmode caught error */
                            printf("BATCHMODE caught Exit_binary_c : back in batchmode function\n");
                            evolve_system_return_value = -1;
                        }

#if defined TIMER && defined TIMER_PER_SYSTEM
                        if(evolve_system_return_value == 0)
                            calc_ticks_timer(stardata,start_tick,&first);
#endif // TIMER && TIMER_PER_SYSTEM

                        /* fin(ished) message */
                        printf("fin\n");
                        fflush(stdout); // flush is REQUIRED here

                    }
                    else if(n>0)
                    {
                        /*
                         * Generic string of arguments
                         *
                         * Split batchstring with strtok
                         * into the batchargs array
                         */
                        batchargs = Malloc(sizeof(char *)*n);
                        size_t bn = 0;
                        char * saveptr2 = NULL;
                        c = strtok_r(batchstring2," ",&saveptr2);
                        while(c)
                        {
                            batchargs[bn++] = c;
                            c = strtok_r(NULL," ",&saveptr2);
                        }

                        /* call parse_arguments */
                        parse_arguments(0,
                                        n,
                                        batchargs,
                                        stardata);
                        fflush(stdout);

                        /* free memory */
                        Safe_free(batchargs);
                    }
                }
            }
        }
    }
    printf("batchmode BATCHMODE_OFF (=%d)\n",BATCHMODE_OFF);
    fflush(stdout);

    Safe_free(batchargs);
    Safe_free(batchstring);
    Safe_free(batchstring2);

    if(is_tty == TRUE)
    {
        tcsetattr(STDIN_FILENO, TCSAFLUSH, &told);
    }
}

#endif
