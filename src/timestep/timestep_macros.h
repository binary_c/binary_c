#pragma once
#ifndef TIMESTEP_MACROS_H
#define TIMESTEP_MACROS_H
/*
 * Macros specific to binary_c's timestep algorithm
 */
#include "timestep_function_macros.h"

/*
 * A long time in whatever units
 */
#define LONG_TIMESTEP (1e100)
#define LONG_TIME_REMAINING (1e50)
#define LONG_TIDAL_TIME (1e100)

/*
 * number of burn in timesteps
 */
#define BURN_IN_TIMESTEPS 10

/*
 * Zoom of the timestep
 */
enum {
    ZOOMFAC_DO_NOTHING,
    ZOOMFAC_DECREASE_TIMESTEP,
    ZOOMFAC_INCREASE_TIMESTEP,
    ZOOMFAC_RESET
};
#define Zoomfac_action_string(N)                        \
    (                                                   \
        (N) == ZOOMFAC_DECREASE_TIMESTEP ? "reduce" :     \
        (N) == ZOOMFAC_INCREASE_TIMESTEP ? "increase" : \
        (N) == ZOOMFAC_RESET ? "reset" :                \
        (N) == ZOOMFAC_DO_NOTHING ? "do nothing" :                \
        "unknown"                                       \
        )
#define DEFAULT_ZOOMFAC_DECREASE 0.5
#define DEFAULT_ZOOMFAC_INCREASE 1.2



#endif // TIMESTEP_MACROS_H
