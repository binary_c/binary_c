#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "timestep_debug.h"

void stellar_timestep(Timestep_prototype_args)
{
    /*
     * Calculate the stellar timestep.
     *
     * This is set in *dt and has units of Myr.
     *
     * Note that the stellar type can be MASSLESS_REMNANT.
     */
    double _age,_tm,_tn;
#ifdef BSE
    _age = age;
    _tm = tm;
    _tn = tn;
#else
    _age = 0.0;
    _tm = 0.0;
    _tn = 0.0;
#endif//BSE

    Dprint("stellar_timestep at %g in star %d stellar_type=%d=%d, age=%g, tm=%g, tn=%g time_remaining=%g\n",
           stardata->model.time,
           star->starnum,
           stellar_type,
           star->stellar_type,
           _age,
           _tm,
           _tn,
           *time_remaining
        );

    /*
     * tscls is a list of timescales equivalent to those
     * in BSE. If NULL, use the stellar evolution algorithm
     * to create them.
     */
#ifdef BSE
    struct BSE_data_t * bse;
    if(tscls == NULL)
    {
        bse = new_BSE_data();
        stellar_timescales(stardata,
                           star,
                           bse,
                           star->phase_start_mass,
                           star->mass,
                           star->stellar_type);
        tscls = bse->timescales;
    }
    else
    {
        bse = NULL;
    }
#endif//BSE

    /*
     * First, set no limit
     */
    *dt  = LONG_TIMESTEP;
    star->dtlimiter = DT_LIMIT_NONE;

#ifdef HRDIAG
    stardata->model.hrdiag_dt_guess=100.0;
#endif
    if(stardata->preferences->stellar_structure_algorithm !=
       STELLAR_STRUCTURE_ALGORITHM_NONE)
    {
        switch(stellar_type)
        {
        case LOW_MASS_MAIN_SEQUENCE:
        case MAIN_SEQUENCE:
            timestep_MS(Timestep_call_args);
            break;

        case HERTZSPRUNG_GAP:
            timestep_HG(Timestep_call_args);
            break;

        case FIRST_GIANT_BRANCH:
            timestep_FGB(Timestep_call_args);
            break;

        case CHeB:
            timestep_CHeB(Timestep_call_args);
            break;

        case EAGB:
            timestep_EAGB(Timestep_call_args);
            break;

        case TPAGB:
            timestep_TPAGB(Timestep_call_args);
            break;

        case HeMS:
            timestep_HeMS(Timestep_call_args);
            break;

        case HeHG:
        case HeGB:
            timestep_HeHG_HeGB(Timestep_call_args);
            break;

        case MASSLESS_REMNANT:
            timestep_massless_remnant(Timestep_call_args);
            break;

        default:
            timestep_other(Timestep_call_args);

        }
    }

    nonstellar_timestep(stardata,dt);
    timestep_limits(Timestep_call_args);
    timestep_logging(Timestep_call_args);
    timestep_modulation(Timestep_call_args);
    timestep_hard_limits(Timestep_call_args);

    star->stellar_timestep = *dt;

    if(stardata->preferences->timestep_logging == TRUE)
    {
        fprintf(stdout,
                "%sstellar_timestep at t=%23.15g model %d "
                "%s%sdt=%20.12g Myr%s "
                "set by %s%s%s (%d) "
                "reject_same %s reject_shorten %s "
                "star %d stellar_type %d [m=%20.12g mc=%20.12g L=%20.12g R=%20.12g RL=%20.12g (1-R/RL=%20.12g) minit=%20.12g menv=%20.12g SN=%d J*=%20.12g; a=%20.12g e=%20.12g Jorb=%20.12g, ω*=%20.12g ωorb=%20.12g tcirc=%20.12g y, age=%20.12g, tm=%20.12g, tn=%20.12g] "
                "time_remaining=%20.12g : "
                ": logtnow=%20.12g logtnext=%20.12g (Fequal? %s Times_are_equal? %s : diff %20.12g) triggerd? %d %s\n",

                stardata->store->colours[YELLOW],
                stardata->model.time,
                stardata->model.model_number,

                stardata->store->colours[UNDERLINE],
                stardata->store->colours[BRIGHT_RED],
                *dt,
                stardata->store->colours[YELLOW],

                stardata->store->colours[BRIGHT_MAGENTA],
                Dt_limit_string(abs(star->dtlimiter)),
                stardata->store->colours[YELLOW],
                star->dtlimiter,

                Truefalse(stardata->model.reject_same_timestep),
                Truefalse(stardata->model.reject_shorten_timestep),

                star->starnum,
                star->stellar_type,
                star->mass,
                Outermost_core_mass(star),
                star->luminosity,
                star->radius,
                star->roche_radius,
                Is_not_zero(star->roche_radius) ? (1.0 - star->radius/star->roche_radius) : 0.0,
                star->phase_start_mass,
                envelope_mass(star),
                star->SN_type,
                star->angular_momentum,
                stardata->common.orbit.separation,
                stardata->common.orbit.eccentricity,
                stardata->common.orbit.angular_momentum,
                star->omega,
                stardata->common.orbit.angular_frequency,
                stardata->common.orbit.tcirc,
#ifdef BSE
                age,
                tm,
                tn,
#else
                star->age,
                0.0,
                0.0,
#endif

                *time_remaining,

                log10(stardata->model.time),
                log10(stardata->model.time + *dt),
                Yesno(Fequal(log10(stardata->model.time),
                             log10(stardata->model.time + *dt))),
                Yesno(Times_are_equal(log10(stardata->model.time),
                                      log10(stardata->model.time + *dt))),
                log10(stardata->model.time) -
                log10(stardata->model.time + *dt),
                stardata->model.fixed_timestep_triggered,
                stardata->store->colours[COLOUR_RESET]
            );
        fflush(stdout);
    }

    Dprint("at t = %g model %d want dt = %g, next target %g, triggered? %d\n",
           stardata->model.time,
           stardata->model.model_number,
           *dt,
           stardata->model.time > 100 ? target_times_next(stardata) : -1,
           timestep_trigger_any_fixed_timestep(stardata)
        );
    const double min_dt = DBL_EPSILON * stardata->preferences->max_evolution_time;
    if(*dt != 0.0 &&
       *dt < min_dt &&
       timestep_trigger_any_fixed_timestep(stardata) == FALSE)
    {
        Exit_binary_c(BINARY_C_FLOATING_POINT_ERROR,
                      "Stellar timestep < %g which will cause problems. Please increase minimum_timestep, or fix the timestepping algorithm, to avoid this problem.",
                      min_dt);
    }

#ifdef BSE
    if(bse != NULL)
    {
        free_BSE_data(&bse);
    }
#endif//BSE
    return;
}
