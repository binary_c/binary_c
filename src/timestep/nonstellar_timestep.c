#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "timestep_debug.h"

void nonstellar_timestep(struct stardata_t * const stardata,
                         double * const dt)
{
    /*
     * Timesteps that don't depend on the properties
     * of the stars.
     */


    /*
     * Fixed timesteps: associate these with star 0
     * which will always exist
     */
    timestep_fixed_timesteps(stardata,
                             &stardata->star[0],
                             dt);


    /*
     * Hard wired minimum : do not apply
     * if the timestep is limited by a fixed timestep
     */
    if(stardata->model.fixed_timestep_triggered == FALSE &&
       stardata->star[0].dtlimiter != DT_LIMIT_FIXED_TIMESTEP)
    {
        *dt = Max(*dt,
                  MINIMUM_STELLAR_TIMESTEP);
    }
}
