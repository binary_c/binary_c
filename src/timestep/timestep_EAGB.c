#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "timestep_debug.h"

void timestep_EAGB(Timestep_prototype_args)
{
#ifdef BSE

    if(Use_timestep(DT_LIMIT_EAGB))
    {
        double dtt;
        if(star->TZO == TRUE)
        {
            /*
             * TZO timescale assuming 1e-6 Msun/year mass loss
             */
            dtt = 1e-6 * (star->phase_start_mass - Max(star->core_mass[CORE_NEUTRON],star->core_mass[CORE_BLACK_HOLE]))/1e-6;
        }
        else
        {
            const int itinf = age < tscls[T_EAGB_T] ? T_EAGB_TINF_1 : T_EAGB_TINF_2;
            dtt = tscls[itinf] - age;
        }
        Limit_timestep(*dt,stardata->preferences->timestep_multipliers[DT_LIMIT_EAGB]*dtt,star,DT_LIMIT_EAGB);
    }

#ifdef HRDIAG
    stardata->model.hrdiag_dt_guess = HRDIAG_FRAC* dtt;
#ifdef HRDIAG_DTLOG
    printf("HRDT (deltat) EAGB %g\n",stardata->model.hrdiag_dt_guess);
#endif // HRDIAG_DTLOG
    HRDIAG_DT_NEGATIVE_CHECK;
#endif // HRDIAG

    //*time_remaining = Min(tn,tscls[T_TPAGB_FIRST_PULSE]) - age;
    if(star->TZO)
    {
        *time_remaining = 1e-6 * (star->phase_start_mass - Max(star->core_mass[CORE_NEUTRON],star->core_mass[CORE_BLACK_HOLE]))/1e-6;
    }
    else
    {
        *time_remaining = tscls[T_TPAGB_FIRST_PULSE] - age;
    }

    /*
     * Slow down as we approach RLOF, we want to make this
     * as accurate as possible.
     */

#if defined SLOW_DOWN_PREROCHE_EAGB && !defined RLOF_REDUCE_TIMESTEP
    if(stardata->model.sgl == FALSE)
    {
        double rr=star->radius / star->roche_radius;
        rr=1.0-rr;
        rr=Min(1.0,rr);
        rr=Max(0.1,rr);
        double newdt = *dt * rr;
        Limit_timestep(*dt,newdt,star,DT_LIMIT_EAGB_PREROCHE);
    }
#endif // SLOW_DOWN_PREROCHE_EAGB

    double accretion_rate = Mdot_net(star);
    if(accretion_rate > 0.0)
    {
        /*
         * Use the accretion rate limit, not the
         * stellar evolution limit if an AGB star is accreting.
         */
        *time_remaining = LONG_TIME_REMAINING;
    }
#endif
}
