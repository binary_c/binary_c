#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "timestep_debug.h"

void timestep_MS(Timestep_prototype_args)
{

    if(stardata->preferences->stellar_structure_algorithm == STELLAR_STRUCTURE_ALGORITHM_MINT)
    {
#ifdef MINT
        /*
         * Estimate main sequence timestep based on rate of central hydrogen
         * exhaustion and main sequence lifetime
         */
        const double MS_lifetime =  1e6 * MINT_MS_lifetime(stardata,star);

#define Estimate_timescale(X,DXDT)                                  \
        (Is_zero(DXDT) ? LONG_TIMESTEP : (fabs((X)/(DXDT))))

        /*
         * When Xc is very small, we often get to the point
         * where it would take a very short time to burn the
         * remaining hydrogen by the standard estimate. So fudge
         * this by considering only X > 1e-4.
         */
        const double timescales[] = {
            Estimate_timescale(star->mint->XHc, star->derivative[DERIVATIVE_STELLAR_CENTRAL_HYDROGEN]),
            Estimate_timescale(star->mint->central_degeneracy, star->derivative[DERIVATIVE_STELLAR_CENTRAL_DEGENERACY]),
            MS_lifetime
        };

        for(size_t i=0; i<Array_size(timescales); i++)
        {
            const double _dt = 1e-6*stardata->preferences->timestep_multipliers[DT_LIMIT_MS]*fabs(timescales[i]);
            Limit_timestep(*dt,
                           _dt,
                           star,
                           DT_LIMIT_MS);
        }

        *time_remaining = star->mint->XHc < 1e-6 ? timescales[0] : (star->mint->XHc/0.7 * MS_lifetime * 1e-6);
#endif//MINT
    }
    else
    {
#ifdef BSE
        if(Use_timestep(DT_LIMIT_MS))
        {
            Limit_timestep(*dt,
                         stardata->preferences->timestep_multipliers[DT_LIMIT_MS]*tm,
                         star,
                         DT_LIMIT_MS);
        }
        *time_remaining = tm - age;
#ifdef HRDIAG
        /*
         * smoother MS timestep:
         *
         * at early times ~ 10% of the MS lifetime
         * at late times ~ 10% of the HG lifetime
         * This resolves the MS->HG transition.
         */
        {
            const double x=stardata->model.time / tm;
            const double dt_ms=HRDIAG_FRAC*tm;
            const double dt_hg=HRDIAG_FRAC*(tscls[T_BGB]-tm);
            const double f=1.0/(1.0+pow(1e-10,0.8-x));
            stardata->model.hrdiag_dt_guess = dt_hg + dt_ms*f;
        }
#ifdef HRDIAG_DTLOG
        Dprint("HRDT (deltat) MS %g\n",stardata->model.hrdiag_dt_guess);
#endif // HRDIAG_DTLOG
        HRDIAG_DT_NEGATIVE_CHECK;
#endif // HRDIAG

#ifdef PRE_MAIN_SEQUENCE
        /* resolve the pre-Main sequence */
        if(Use_timestep(DT_LIMIT_PREMS) &&
           stardata->preferences->pre_main_sequence == TRUE)
        {
            const double dtPMS = 1e-6 * preMS_lifetime(star->mass); // Myr
            if(stardata->model.time < 2.0*dtPMS)
            {
                Limit_timestep(*dt,stardata->preferences->timestep_multipliers[DT_LIMIT_PREMS]*dtPMS,star,DT_LIMIT_PREMS);
            }
        }
#endif // PRE_MAIN_SEQUENCE

#ifdef SLOW_DOWN_PREROCHE_MS
        if(Use_timestep(DT_LIMIT_PREROCHE_MS) &&
           stardata->model.sgl == FALSE &&
           Is_not_zero(star->roche_radius))
        {
            const double rr = star->radius / star->roche_radius;
            Dprint("prerocheMS RR = %g from R = %g, RL = %g (t = %g)\n",
                   rr,
                   star->radius,
                   star->roche_radius,
                   stardata->model.time);
            if(rr<1.0)
            {
                const double flim = star->stellar_type ==
                    LOW_MASS_MAIN_SEQUENCE ? 1e-3 : 1e-2;
                const double k = 2.0; // larger -> smaller timestep
                const double fdt =
                    rr<0.5 ? 1.0 :
                    rr<1.0 ? (Min(1.0, Max(flim,pow(1.0/rr-1.0,k))))
                    : flim;
                const double newdt = *dt * fdt;
                Limit_timestep(*dt,newdt,star,DT_LIMIT_PREROCHE_MS);
                Dprint("prerocheMS star=%d rr=%g fdt=%g\n",
                       star->starnum,
                       rr,
                       fdt);
            }
        }
#endif // SLOW_DOWN_PREROCHE_MS
#endif // BSE
    }
}
