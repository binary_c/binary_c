#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "timestep_debug.h"

void timestep_fixed_timesteps(struct stardata_t * const stardata,
                              struct star_t * const star,
                              double * const dt)
{
    /*
     * Limit the timestep according to the various
     * fixed timestep algorithms, as set in setup_fixed_timesteps.c
     */
    for(int i=0;i<FIXED_TIMESTEP_NUMBER;i++)
    {
        struct binary_c_fixed_timestep_t * const t = stardata->model.fixed_timesteps + i;

        if(t->enabled == TRUE)
        {
            const double T = t->logarithmic ? log10(stardata->model.time) : stardata->model.time;
            if(Less_or_equal(T,t->end))
            {
                double dtlim;
                if(timestep_fixed_trigger(stardata,i))
                {
                    /*
                     * Triggered on this timestep : limit next timestep
                     * to exactly this trigger's step.
                     */
                    dtlim = t->logarithmic ? (exp10(T + t->step) - stardata->model.time) : t->step;
                }
                else
                {
                    /*
                     * Not triggered on this timestep :
                     * the time to the next trigger limits the timestep.
                     */
                    dtlim = (t->logarithmic ? exp10(t->next) : t->next) - stardata->model.time;
                }

                Dprint("At t = %g : Set dtlim = %30.20g : next time will be %30.20g : Times_are_equal? %s\n",
                       isinf(fabs(T)) ? -1e10 : T,
                       dtlim,
                       t->logarithmic ? log10(stardata->model.time + dtlim) : (stardata->model.time + dtlim),
                       Yesno(Times_are_equal(t->next,
                                             log10(stardata->model.time + dtlim)))
                    );

                /*
                 * Limit the timestep, but do not impose
                 * the stardata->preferences->minimum_timestep
                 */
                Limit_timestep_no_minimum(*dt,
                                          dtlim,
                                          star,
                                          DT_LIMIT_FIXED_TIMESTEP);
            }
        }
    }
}
