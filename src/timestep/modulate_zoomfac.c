#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "timestep_debug.h"

Boolean modulate_zoomfac(struct stardata_t * stardata,
                         const unsigned int action)
{
    /*
     * modulate the timestep zoom factor using the given "action".
     *
     * - Make it smaller (action == ZOOMFAC_DECREASE_TIMESTEP),
     * - Make it larger (action == ZOOMFAC_DECREASE_TIMESTEP)
     * - Reset it to 1.0 (action == ZOOMFAC_RESET).
     *
     * Returns TRUE if we were able to do what we wanted to do,
     * FALSE otherwise.
     */
    Boolean retval = TRUE;
    Set_debug_dt;
    Dprint("ZOOM modulate_zoomfac %s : was %g ...",
           Zoomfac_action_string(action),
           stardata->model.dt_zoomfac);

    if(action == ZOOMFAC_DECREASE_TIMESTEP)
    {
        /*
         * Reduce timestep by zoomfac_multiplier_decrease (usually
         * 0.5, i.e. a bisection) if we can.
         */
        stardata->model.dt_zoomfac *= stardata->preferences->zoomfac_multiplier_decrease;

        if(unlikely(stardata->model.dtm * stardata->model.dt_zoomfac <
                    stardata->preferences->minimum_timestep))
        {
            stardata->model.dt_zoomfac =
                stardata->preferences->minimum_timestep /
                stardata->model.dtm;
            retval = FALSE;
        }
        else
        {
            retval = TRUE;
        }
    }
    else if(action == ZOOMFAC_INCREASE_TIMESTEP)
    {
        /*
         * Increase timestep by zoomfac_multiplier_increase
         * (usually 1.2) if we can.
         */
        stardata->model.dt_zoomfac *= stardata->preferences->zoomfac_multiplier_increase;

        if(unlikely(stardata->model.dt_zoomfac > 1.0))
        {
            retval = FALSE;
            stardata->model.dt_zoomfac = 1.0;
        }
        else
        {
            retval = TRUE;
        }
    }
    else if(action == ZOOMFAC_RESET)
    {
        /*
         * Reset timestep multiplier : this can always be done
         */
        stardata->model.dt_zoomfac = 1.0;
        retval = TRUE;
    }
    else
    {
        retval = FALSE;
        Exit_binary_c(BINARY_C_ALGORITHM_OUT_OF_RANGE,
                      "Unknown action = %u in modulate_zoomfac\n",
                      action);

    }

    Dprint("ZOOM now %g (new dtm will be %g [%g,%g]) : Return %d\n",
           stardata->model.dt_zoomfac,
           stardata->model.dtm * stardata->model.dt_zoomfac,
           stardata->preferences->minimum_timestep,
           stardata->preferences->maximum_timestep,
           retval);
    return retval;
}
