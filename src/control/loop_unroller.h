#pragma once
#ifndef LOOP_UNROLLER_H
#define LOOP_UNROLLER_H
#include "loop_unroller_recurse.h"

/*
 * Loop unroller, written in C macros for speed.
 */

/*
 * __Unrolled_loop_Concat3 allows you to construct unique variable names.
 * __Unrolled_loop_Array_size takes an array as input and returns its length.
 * __Unrolled_loop_Min(A,B) takes the minimum of A and B
 */
#undef __Unrolled_loop_Concat3
#define __Unrolled_loop_Concat3(A,B,C) A##B##C
#undef __Unrolled_loop_Array_size
#define __Unrolled_loop_Array_size(A) (sizeof(A)/sizeof((A)[0]))
#undef __Unrolled_loop_Min
#define __Unrolled_loop_Min(a,b) \
    ({typeof(a) _a = (a); typeof(b) _b = (b); _a < _b ? _a : _b; })

/*
 * Main entry macros, unroll to the implementation
 * for either and array or pointer.
 *
 * When passing a pointer, we also have to pass the
 * size of the array to which it points.
 */
#define Unrolled_loop_array(ARRAY,                                      \
                            STRIDE,                                     \
                            LOOPVAR,                                    \
                            LHS,                                        \
                            RHS,                                        \
                            FINAL_EXPR_LHS,                             \
                            FINAL_EXPR_RHS,                             \
                            ...)                                        \
                                                                        \
    __Unrolled_loop_implentation(__unrolled_loop__,                     \
                                 __COUNTER__,                           \
                                 ARRAY,                                 \
                                 (__Unrolled_loop_Array_size(ARRAY)),   \
                                 STRIDE,                                \
                                 LOOPVAR,                               \
                                 LHS,                                   \
                                 RHS,                                   \
                                 FINAL_EXPR_LHS,                        \
                                 FINAL_EXPR_RHS,                        \
                                 __VA_ARGS__)

#define Unrolled_loop_pointer(POINTER,              \
                              POINTER_N,            \
                              STRIDE,               \
                              LOOPVAR,              \
                              LHS,                  \
                              RHS,                  \
                              FINAL_EXPR_LHS,       \
                              FINAL_EXPR_RHS,       \
                              ...)                  \
                                                    \
    __Unrolled_loop_implentation(__unrolled_loop__, \
                                 __COUNTER__,       \
                                 POINTER,           \
                                 (POINTER_N),       \
                                 STRIDE,            \
                                 LOOPVAR,           \
                                 LHS,               \
                                 RHS,               \
                                 FINAL_EXPR_LHS,    \
                                 FINAL_EXPR_RHS,    \
                                 __VA_ARGS__)

/*
 * ... the implementation
 */
#ifndef GCC_IVDEP
#define GCC_IVDEP /* do nothing */
#endif
#define __Unrolled_loop_implentation(LABEL,                             \
                                     LINE,                              \
                                     INPUT_ARRAY,                       \
                                     INPUT_ARRAY_SIZE,                  \
                                     STRIDE,                            \
                                     LOOPVAR,                           \
                                     LHS,                               \
                                     RHS,                               \
                                     FINAL_EXPR_LHS,                    \
                                     FINAL_EXPR_RHS,                    \
                                     ...)                               \
    __extension__                                                       \
    ({                                                                  \
        /* array that's summed at the end */                            \
        typeof(INPUT_ARRAY[0]) __Unrolled_loop_Concat3(sum_array,LABEL,LINE)[(STRIDE)] \
            = { 0.0 };                                                  \
                                                                        \
        /* max counter of strided loop */                               \
        const size_t __Unrolled_loop_Concat3(_max,LABEL,LINE) =         \
            __Unrolled_loop_Min((INPUT_ARRAY_SIZE),                     \
                                ((INPUT_ARRAY_SIZE)/(STRIDE))*(STRIDE)); \
                                                                        \
        /* strided loop */                                              \
        GCC_IVDEP                                                       \
        for(size_t LOOPVAR = 0;                                         \
            LOOPVAR < __Unrolled_loop_Concat3(_max,LABEL,LINE);         \
            )                                                           \
        {                                                               \
            __Unrolled_loop_recurse(                                    \
                STRIDE,                                                 \
                __Unrolled_loop_Concat3(sum_array,LABEL,LINE),          \
                LHS,                                                    \
                RHS; ++LOOPVAR;                                         \
                );                                                      \
        }                                                               \
                                                                        \
        /* remaining items */                                           \
        for(size_t LOOPVAR = __Unrolled_loop_Concat3(_max,LABEL,LINE);  \
            LOOPVAR < (INPUT_ARRAY_SIZE);                               \
            ++LOOPVAR)                                                  \
        {                                                               \
            LHS __Unrolled_loop_Concat3(sum_array,LABEL,LINE)[0] RHS;   \
        }                                                               \
                                                                        \
        /* sum up the result */                                         \
        (                                                               \
            __Unrolled_loop_recurse_final(                              \
                STRIDE,                                                 \
                __Unrolled_loop_Concat3(sum_array,LABEL,LINE),          \
                FINAL_EXPR_LHS,                                         \
                FINAL_EXPR_RHS )                                        \
            );                                                          \
    })

#endif // LOOP_UNROLLER_H
