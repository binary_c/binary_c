#pragma once
#ifndef BINARY_PARAMETERS_H
#define BINARY_PARAMETERS_H
/*
 * The binary_c stellar population nucleosynthesis framework.
 *
 * Copyright Robert Izzard 2000-2025
 *
 * Contact rob.izzard@gmail.com
 *
 * Code repository:
 * https://gitlab.com/binary_c
 *
 * Documentation is in doc/
 * Code is in src/
 *
 * To build, run
 *
 * ---
 *
 * meson setup builddir
 * cd builddir
 * ninja binary_c_install
 *
 * ---
 *
 * Please see the files README, LICENCE and CHANGES,
 * and the doc/ directory for documentation which is mirrored
 * at https://binary_c.gitlab.io
 *
 **********************
 *
 * Purpose:
 *
 * This file contains the parameters which turn parts of the code
 * on and off, as well as some physical constants used in various
 * algorithms in binary_c.
 */





/*
 * binary_c_code_options.h contains the machine-specific and
 * build-specific coding options, rather than options which should
 * affect the behaviour of the code. It should be read first.
 */
#include "binary_c_code_options.h"


/*
 * Constants from BSE
 */
#include "binary_c_bse.h"

/*
 * Include experimental features?
 */
#include "binary_c_experimental.h"


/************************************************************
 * Stellar structure algorithms
 ************************************************************/

/*
 * Enable SSE/BSE stellar structure algorithm.
 *
 * You should probably always have this on, even with
 * MINT you need BSE to fill in the evolutionary gaps.
 */
#define BSE

/*
 * Experimental:
 *
 * Enable MINT stellar structure where we have it.
 *
 * If you use MINT with NUCSYN, you'll get shellular burning
 * on the main sequence.
 */
#define MINT


/************************************************************
 * General model parameters
 ************************************************************/

/*
 * NUMBER_OF_STARS is the number of stars used in
 * loops over binaries in the code, so should be 2.
 */
#define NUMBER_OF_STARS 2

/*
 * Allow for orbiting, non-evolving, objects
 */
#define ORBITING_OBJECTS

/*
 * Enable splitting of evolutionary sequences,
 * e.g. at supernovae.
 */
#define EVOLUTION_SPLITTING

/*
 * Maximum split depth:
 * The EVOLUTION_SPLITTING_HARD_MAX_DEPTH macro is the hard-wired
 * maximum split depth, although values smaller than this can be
 * set into preferences->evolution_splitting_maxdepth
 */
#define EVOLUTION_SPLITTING_HARD_MAX_DEPTH 10
#define EVOLUTION_SPLITTING_MAX_SPLITDEPTH_DEFAULT 2

/*
 * default resolution for the splits, e.g.
 * EVOLUTION_SPLITTING_SUPERNOVA_N is set to 10
 * meaning that at each supernova, the evolution is
 * split into 10.
 */
#define EVOLUTION_SPLITTING_SUPERNOVA_N 10


/*
 * give the star galactic co-ordinates, distance etc.
 * and activate related code
 */
//#define GALACTIC_MODEL

/************************************************************
 * Stellar evolution
 ************************************************************/

#define DEFAULT_CHANDRASEKHAR_MASS 1.38 /* chandrasekhar mass in solar units (1.38, was 1.44 in BSE) */

#define DEFAULT_MAX_NEUTRON_STAR_MASS 2.2 /* Maximum mass for a NS before it turns into a BH (2.2, 1.8 in BSE) */

/* minimum core mass for carbon ignition. Pols et al. (1998, used in BSE) suggest 1.6 Msun */
#define DEFAULT_MINIMUM_CORE_MASS_FOR_CARBON_IGNITION 1.6

#define DEFAULT_MAX_TPAGB_CORE_MASS 1.38 // could be 1.368?

#define DEFAULT_MAX_MASS_FOR_SECOND_DREDGEUP 2.25

/*
 * In theory we can change the mixing length on the TPAGB,
 * if you want to use this macro to do it. At present this affects only
 * the interpulse period function.
 */
#define TPAGB_MIXING_LENGTH_PARAMETER 1.75

/*
 * Mass break above which we use Hurley+ 2002
 * fits on the TPAGB
 */
#define TPAGB_MASS_BREAK 8.0


/*
 * This is the correction for the post-flash luminosity
 * dip, set to either:
 *
 * TPAGB_LUMINOSITY_AVERAGE: uses an average luminosity
 *
 * or
 *
 * TPAGB_LUMINOSITY_DIPS : uses the dips, but you MUST
 *
 * set dtfac to 0.01 or at least something small to resolve
 * the dips.
 */
#define TPAGB_LUMTYPE TPAGB_LUMINOSITY_AVERAGE

// Drop for this many pulses: set to a large number for all the pulses
#define PULSE_LUM_DROP_N_PULSES 10000.0


// Drop factor - luminosity drops to this factor times
// the peak luminosity function - do not set to 1.0! 0.5 is
// about right (from Amanda or Richard's models) but Arend Jan's
// models are around 0.1
#define THERMAL_PULSE_LUM_DROP_FACTOR 0.15

// 1.0/the timescale for recovery
// e.g. for a value 3.0 the luminosity recovers in ~ 1/3
// of the interpulse time. This is the default.
#define THERMAL_PULSE_LUM_DROP_TIMESCALE 3.0


/*
 * The minimum mass of envelope for third dredgeup to occur
 * I used to use 0.5 (after Straniero et al which agrees well
 * with Amanda's models) but perhaps 0.0 is
 * better, given the latest results of Richard Stancliffe... ?
 *
 * Note, this is now a command line option:
 * --minimum_envelope_mass_for_third_dredgeup <x>
 *
 * with default value set here (0.5).
 */
#define Minimum_envelope_mass_for_3dup \
    (stardata->preferences->minimum_envelope_mass_for_third_dredgeup)
#define MINIMUM_ENVELOPE_MASS_FOR_THIRD_DREDGEUP_DEFAULT 0.5

/*
 * Speed up the TPAGB by increasing the timestep for latter pulses
 */
#define TPAGB_SPEEDUP
#define TPAGB_SPEEDUP_AFTER 3000.0

/*
 * Max TPAGB evolutionary time in years, just in case something
 * goes badly wrong!
 */
#define MAX_TPAGB_TIME 1e8

/*
 * minimum CO core mass for carbon ignition (Pols+1998)
 * given mcbagb>1.6
 */
#define DEFAULT_MINIMUM_CO_CORE_MASS_FOR_CARBON_IGNITION 1.08

/*
 * maximum mcbagb for degenerate carbon ignition
 */
#define DEFAULT_MAXIMUM_MCBAGB_FOR_DEGENERATE_CARBON_IGNITION 1.6

/*
 * minimum mcbagb for non-degenerate carbon ignition
 */
#define DEFAULT_MINIMUM_MCBAGB_FOR_NONDEGENERATE_CARBON_IGNITION 2.25

/*
 * minimum core mass for neon ignition: Poelarends et al. 2008
 * suggest around 2.8-2.9 with STERN (Langer) code models.
 * However, it's very uncertain and probably depends on stellar
 * history as well.
 *
 * Nomoto ("Type II Supernoave from 8-10Msun progenitors")
 * suggests 1.43 Msun is a more appropriate limit. Hmm.
 *
 * Jones et al. 2013 and Woosley and Heger 2015 use 1.42Msun
 * for single stars.
 *
 * Poelarends et al. 2017 forcus on this (The Astrophysical
 * Journal, 850:197) in single and binary stars, suggesting
 * anything between 1.38 and 1.50Msun.
 */
#define DEFAULT_MINIMUM_CO_CORE_MASS_FOR_NEON_IGNITION 1.42


/** Maximum and minimum masses for convective envelopes **/
#define MAX_CONVECTIVE_MASS 1.25
#define MIN_CONVECTIVE_MASS 0.35

/*
 * set to a large value (e.g. 1e10) to disable
 * double detonation by accretion SNe Ia
 */
#define MASS_ACCRETION_FOR_DDet_DEFAULT 0.15


/*
 * If you define BINARY_STARS_ONLY then the code will stop iterating
 * a star when it is no longer in the binary phase
 */
//#define BINARY_STARS_ONLY

/*
 * Use rob's updated tpagb interpulse period fits?
 */
//#define KARAKAS2002_REFITTED_TPAGB_INTERPULSES

/*
 * The minimum interpulse period: it should never really get
 * this low but this stops problems which occur if it is zero
 */
#define MINIMUM_INTERPULSE_PERIOD_MYR 1e-4

/*
 * If the secondary has a mass <0.09 Msun, force the primary to
 * evolve as a single star and ignore the secondary (probably quite
 * dubious, but may be faster)
 */
//#define FORCE_SINGLE_STAR_EVOLUTION_WHEN_SECONDARY_IS_A_PLANET


/************************************************************
 * Updates to the BSE library
 ************************************************************/

/*
 * Enable this to use Axel's updated (and much better!) RGB radius function.
 */
#define AXEL_RGBF_FIX

/*
 * Fix the bug where an He star accreting hydrogen does not become CHeB
 */
//#define HE_STAR_ACCRETING_H_FIX

/*
 * The DTR_CHECKS are a set of fudges to fix this function
 * so time_remaining>0 at all times. Otherwise, things gets really slow
 */
#define DTR_CHECKS

/* choose one of these for WD cooling */
#define MESTEL_COOLING
//#define MODIFIED_MESTEL_COOLING



/*
 * When HeWDs accrete hydrogen, what do they do?
 * Either you have novae, but what if actually after
 * He_REIGNITION_LAYER_THICKNESS is accreted the helium reignites
 * and you get a helium star again? To enable this define NO_HeWD_NOVAE.
 *
 * I have no idea what the layer thickness should be. (Thin? 0.01 Msun?)
 */
#define NO_HeWD_NOVAE
#define He_REIGNITION_LAYER_THICKNESS 0.01

/*
 * Use new main sequence lifetimes table instead of old thookf fitting
 * function for M>20 (all Z)
 */
#define USE_2012_MAIN_SEQUENCE_LIFETIMES_TABLE

/* activate Hall and Tout (2014) radii */
#ifdef BSE
#define HALL_TOUT_2014_RADII
#endif

/* activate pre-main sequence radii (Railton et al 2014) */
#define PRE_MAIN_SEQUENCE

/*
 * Onno's prescription smooths the helium ignition luminosity
 * around the he-flash mass : enable this to prevent such smoothing
 */
//#define DISCONTINUOUS_HELIUM_IGNITION

/*
 * Algorithms to determine whether an exposed helium core ignites
 */
#define HELIUM_IGNITION_ALGORITHM_BSE 0

/*
 * Apply a refitted ZAMS convective envelope mass valid for
 * Z=1e-4 to 0.02 (convective_envelope_mass_and_radius used to be valid only for Z=0.02)
 * (also improves Jarrod's fit based on models from Evert)
 */
//#define ZAMS_MENV_METALLICITY_CORRECTION


/* activate this for fatal errors in giant_age */
//#define GIANT_AGE_FATAL_ERRORS

/*
 * activate this to correct for negative ages in
 * acquire_stellar_parameters_at_apparent_evolution_age
 */
#define ACQUIRE_FIX_NEGATIVE_AGES

/*
 * Define this to test for negative ages and perhaps exit
 * if they are really seriously negative.
 */
//#define ACQUIRE_TEST_FOR_NEGATIVE_AGES

// fudges to make BSE fit Eldridge's code
//#define JJE_FUDGES

/*
 * Do not allow the stellar type to change if the timestep is negative
 */
//#define RLOF_NO_STELLAR_TYPE_CHANGE_IF_DTM_NEGATIVE


/*
 * Experimental:
 * better treatment of the radius when mass is stripped?
 * Not very useful at high mass!
 */
//#define MAIN_SEQUENCE_STRIP


/*
 * Wrapper macro to enable Selma's code for better treatment of mergers
 * etc.
 */
#define SELMA_EXTRA_FEATURES
#ifdef SELMA_EXTRA_FEATURES

/*
 * Choose one of
 * SELMA_FIX_ACCRETED_MASS_IN_REJUVENATION
 * SELMA_BETTER_TREATMENT_OF_MS_ACCRETORS (recommended)
 */
//#define SELMA_FIX_ACCRETED_MASS_IN_REJUVENATION
#define SELMA_BETTER_TREATMENT_OF_MS_ACCRETORS

#endif // SELMA_EXTRA_FEATURES

/*
 * The Hurley+2002 paper introduced rejuvenation of HG stars.
 * It seems unlikely that this is required. The core mass set
 * up on the main sequence is the core mass on the HG, and this
 * depends on the pre-merger stellar mass so there should be
 * no rejuvenation.
 */
//#define REJUVENATE_HERZTSPRUNG_GAP_STARS

/*
 * minimum luminosity for TPAGB stars (fudge/check!)
 */
#define KARAKAS2002_TPAGB_MIN_LUMINOSITY 100.0


/************************************************************
 * Mass loss, general
 ************************************************************/


/*
 * Log when the star is rapidly (>95%) or mediumly (>50%)
 * rotating (this goes in the file log)
 */
//#define LOG_ROTATION_STATES

/************************************************************
 * Mass loss by stellar winds
 ************************************************************/

/* use 2011 mass loss library */
#define USE_2011_MASS_LOSS

/*
 * Default Reimers wind loss parameter
 */
#define REIMERS_ETA_DEFAULT 0.5

/*
 * Allow shift VW93 mira period relation
 */
#define VW93_MIRA_SHIFT

/*
 * Allow multiplied VW93 mass-loss rate
 */
#define VW93_MULTIPLIER

/* AGB rotational wind enhancement based on Dijkstra & Speck 2006 */
//#define AGB_ROTATIONAL_WIND_ENHANCEMENT_DS06

/* or based on Moe's fit */
//#define AGB_ROTATIONAL_WIND_ENHANCEMENT_MOE

/************************************************************
 * Supernovae and kicks
 ************************************************************/

/*
 * When kicking, solve the mean anomaly to this tolerance.
 * Also set the maximum number of iterations before failure.
 */
#define MEAN_ANOMALY_TOLERANCE 1e-8
#define MEAN_ANOMALY_MAX_ITERATIONS 1000

/*
 * Sigma is the velocity dispersion of the supernova kick (km s-1)
 */
#define SN_SIGMA_DEFAULT 190.0
/* WD kicks */
#define WD_SIGMA_DEFAULT 0.0

/* custom kick function parameters:
 * the max kick velocity (used for the table and to check the output)
 */
#define MAX_KICK_VELOCITY 2000.0
/* velocity resolution : if this is too small then
 * either the manual lookup will be slow of the KICK_CDF_TABLE
 * will be very large (km/s) : 0.1 is enough to resolve a 190km/s
 * maxwellian up to 800km/s (i.e. 1 part in 1e3)
 */
#define KICK_VELOCITY_RESOLUTION 1.0

/* if KICK_CDF_TABLE is defined, pre-cache the cumulative
 * distribution function to speed things up (a lot!):
 * NB this is of size MAX_KICK_VELOCITY/KICK_VELOCITY_RESOLUTION
 * NB in testing this is ~20x faster than manual calculation
 */
#define KICK_CDF_TABLE
#define KICK_CDF_TABLE_USELOG

/* allow white dwarfs to be kicked on formation */
#define WD_KICKS

/*
 * Enable this to allow HeIa supernovae from accretion (or merger) onto
 * a HeWD > 0.7Msun. Otherwise just reignite (e.g. as a HeMS: sdB/O)
 * Note that Jarrod's original code has HeWD supernovae implemented,
 * but who believes it?
 */
//#define ALLOW_HeWD_SUPERNOVAE


/*
 * Runaway star detection, e.g. Tauris & Takens prescription
 */
#define RUNAWAY_STARS

/************************************************************
 * Angular momentum : tides and systematic loss
 ************************************************************/

/*
 * Log when the stars are tidally locked to the orbit (>90%)
 */
#define LOG_TIDAL_LOCKING

/* a factor to change the tidal strength */
#define TIDAL_STRENGTH_FACTOR_DEFAULT 1.0

/* After testing, set these two to be the same (10.0, I expect) */
#define MIN_SEP_FOR_CLOSE_ANG_MOM_LOSS1 10.0 /* special_cases.c */
#define MIN_SEP_FOR_CLOSE_ANG_MOM_LOSS2 10.0 /* wind_loss.c */

/*
 * Post testing, replace the above macros with the one below
 */
#define MIN_SEP_FOR_CLOSE_ANG_MOM_LOSS 10.0

/************************************************************
 * Mass limits
 ************************************************************/

/* min and max stellar mass that are "trusted" by the evolution code */


#ifdef MINT
/* MINT */
#define BINARY_C_MINIMUM_STELLAR_MASS 0.08
#define BINARY_C_MAXIMUM_STELLAR_MASS 1000.0
#else
/* BSE */
#define BINARY_C_MINIMUM_STELLAR_MASS 0.1
#define BINARY_C_MAXIMUM_STELLAR_MASS 100.0
#endif//MINT

/* If mass > MAXIMUM_STELLAR_MASS, and STRIP_SUPERMASSIVE_STARS is true,
 * we limit the mass to MAXIMUM_STELLAR_MASS and continue the evolution
 */
#ifdef MINT
/* MINT */
#define MAXIMUM_STELLAR_MASS 1000.0
#else
/* BSE */
#define MAXIMUM_STELLAR_MASS 200.0
#define STRIP_SUPERMASSIVE_STARS
#endif//MINT

#define TIDES_DIAGNOSIS_OUTPUT
/*
 * Minimum mass to go into some of the fitting formulae (prevents
 * dodgy errors with very low mass objects). Comment out the #define
 * to not use this feature.
 */
//#define MINIMUM_STELLAR_MASS 0.01


/*
 * Maximum brown-dwarf mass : i.e. the boundary between
 * non-hydrogen burning and hydrogen burning objects.
 *
 * Typically 0.07 Msun
 */
#define MAXIMUM_BROWN_DWARF_MASS 0.07


/************************************************************
 * Timestepping
 ************************************************************/

// Minimum timestep in Myr (default value)
#define MINIMUM_TIMESTEP_DEFAULT 1e-6

// Maximum timestep in Myr (default value)
#define MAXIMUM_TIMESTEP_DEFAULT 1e20

// Minimum stellar evolution timestep (Myr)
#define MINIMUM_STELLAR_TIMESTEP 1e-7

/* maximum timestep factor between two timesteps, ignored if 0 */
#define MAXIMUM_TIMESTEP_FACTOR_DEFAULT 0.0

/* timestep multiplication factor */
#define DTFAC_DEFAULT 1.0

/*
 * Enable timestep modulation
 */
#define TIMESTEP_MODULATION


/*
 * Enable these to reduce the timestep when R approaches R_lobe (roche_radius)
 * during particular stellar types
 */
#define SLOW_DOWN_PREROCHE_MS
#define SLOW_DOWN_PREROCHE_HG
#define SLOW_DOWN_PREROCHE_EAGB
#define SLOW_DOWN_PREROCHE_TPAGB


/*
 * Perhaps reduce the timestep when the star is rotating
 * quickly in order to resolve the excess mass loss
 */
//#define MODULATE_TIMESTEP_TO_RESOLVE_ROTATION_MASS_LOSS

/* maximum blue straggler system timestep (MYr) */
#define BLUE_STRAGGLER_MAX_TIMESTEP 1.0


/*
 * Default mass transfer rate multiplier limits:
 * these should be 1.0 or, if to be ignored, -1.0.
 */

#define ACCRETION_LIMIT_EDDINGTON_MULTIPLIER_STEADY_DEFAULT 1.0
#define ACCRETION_LIMIT_EDDINGTON_MULTIPLIER_LMMS_DEFAULT 1.0
#define ACCRETION_LIMIT_EDDINGTON_MULTIPLIER_WD_TO_REMNANT_DEFAULT -1.0
#define ACCRETION_LIMIT_THERMAL_MULTIPLIER_DEFAULT 1.0
#define ACCRETION_LIMIT_DYNAMICAL_MULTIPLIER_DEFAULT 1.0
#define DONOR_LIMIT_THERMAL_MULTIPLIER_DEFAULT 1.0
#define DONOR_LIMIT_DYNAMICAL_MULTIPLIER_DEFAULT 1.0
#define DONOR_LIMIT_ENVELOPE_MULTIPLIER_DEFAULT 1.0

/*
 * Default compact object accretion rate limits
 */
#define ACCRETION_RATE_NOVAE_UPPER_LIMIT_HYDROGEN_DONOR_DEFAULT (DONOR_RATE_ALGORITHM_CLAEYS2014)
#define ACCRETION_RATE_NOVAE_UPPER_LIMIT_HELIUM_DONOR_DEFAULT (DONOR_RATE_ALGORITHM_CLAEYS2014)
#define ACCRETION_RATE_NOVAE_UPPER_LIMIT_OTHER_DONOR_DEFAULT (DONOR_RATE_ALGORITHM_CLAEYS2014)
#define ACCRETION_RATE_NEW_GIANT_ENVELOPE_LOWER_LIMIT_HYDROGEN_DONOR_DEFAULT (DONOR_RATE_ALGORITHM_CLAEYS2014)
#define ACCRETION_RATE_NEW_GIANT_ENVELOPE_LOWER_LIMIT_HELIUM_DONOR_DEFAULT (DONOR_RATE_ALGORITHM_CLAEYS2014)
#define ACCRETION_RATE_NEW_GIANT_ENVELOPE_LOWER_LIMIT_OTHER_DONOR_DEFAULT (DONOR_RATE_ALGORITHM_CLAEYS2014)

/*
 * Enable calculation of X-ray luminosity
 */
//#define XRAY_LUMINOSITY


/*
 * Enable Hachisu, Kato and Nomoto 1996 disk wind and associated
 * accretion rate limit. Define default multiplier and qcrit.
 */
#define HACHISU_DISK_WIND_QCRIT_DEFAULT 1.15


/*
 * Enable this line to prevent accretion of material onto a secondary
 * which would then spin beyond breakup (not enabled by default)
 *
 * This is the original Hurley et al algorithm
 *
 * WARNING: I am not convinced that this works, use
 * ROTATIONALLY_ENHANCED_MASS_LOSS to achieve a similar result.
 */
//#define STOP_ACCRETION_OF_MATERIAL_BEYOND_BREAKUP

/*
 * Mass that must be accreted to be a blue straggler: should be
 * >=1e-5
 */
#define BLUE_STRAGGLER_ACCRETED_MASS_THRESHOLD 1e-5


/*
 * Selma's fix to take the accreted mass in the rejuvenation process correctly into account:
 *  => using star->last_mass to be independent from star->dM_RLOF_accrete and so even if the mass transfer
 *     is not conservative the rejuvenation will work as intended
 *
 * Note: Selma's option 'SELMA_BETTER_TREATMENT_OF_MS_ACCRETORS' is another method to rejuvenate
 *       stars. You cannot use both option at the same time!
 */

/*
  Recommended: Enable this if you want to use Selma's updates for MS
  mergers and accreting of main sequence stars.  By default it is assumed
  that a small fraction of mass is lost during the merger and it
  treats the core sizes of massive stars better than the old Hurley
  prescription, leading to less extreme rejuvenation. If you want you
  can adjust the mixing parameters yourself but the values below seem
  reasonable given our limited understanding of mergers at the moment (6/2011).

  The option 'SELMA_BETTER_TREATMENT_OF_MS_ACCRETORS' automatically disables
  'SELMA_FIX_ACCRETED_MASS_IN_REJUVENATION', which is just a fix to the old (buggy)
  rejuvenation prescription!!!
*/
// improved rejuvenation process for the accretor


// improved handling of mixing and rejuvenation of MS merger

/* Regulates the amount of extra mixing in a merger resulting from two
   main sequence stars, as it affects the central abundances of
   apparent age.  Note that the old prescpription was almost analogue to
   complete mixing.*/
#define MIXINGPAR_MS_MERGERS_DEFAULT 0.1




/************************************************************
 * Novae
 ************************************************************/

/*
 * The fraction of matter retained in a nova explosion
 */
#define NOVA_RETENTION_FRACTION_DEFAULT 1e-3


/* deprecated: accretion from nova explosions */
//#define NOVA_BACK_ACCRETION

/* deprecated: instant mixing on the companion */
//#define NOVA_BACK_ACCRETION_INSTANT_MIXING

/*
 * after a nova explosion, we continue short timesteps
 * until NOVA_TIMEOUT_FACTOR * nova_recurrence_time
 */
#define NOVA_TIMEOUT_FACTOR 2.0

/************************************************************
 * Mass transfer by stellar winds
 ************************************************************/

/*
 * Default multipier for the Tout and Egglton "Companion
 * reinforced attrition process"
 */
#define CRAP_PARAMETER_DEFAULT 0.0

/*
 * Bondi-Hoyle wind accretion factor (3/2 in BSE/Hurley+2002)
 */
#define BONDI_HOYLE_ACCRETION_FACTOR_DEFAULT 1.50

/*
 * SPHERICAL_ANGMOM_ACCRETION_FACTOR
 * is used in binary_star_functions/stellar_wind_angmom.c
 * to multiply the angular momentum that accretes
 * with "spherically accreting" material from a
 * companion's wind.
 *
 * In general, this should be 1.0, equivalent
 * to Keplerian accretion (from a disc).
 *
 * This will be improved by work being done in Bonn
 * at present (Matrozis et al.)
 */
#define SPHERICAL_ANGMOM_ACCRETION_FACTOR 1.0

/************************************************************
 * Mass transfer by hybrid wind-Roche lobe overflow
 ************************************************************/

/*
 * CAB = Carlo Abate, not an upper-case taxi
 *
 * CAB: wind-RLOF mode of mass transfer.
 *      You can choose between QUADRATIC_WRLOF, that is mass
 *      accretion efficiency that does not depend on the mass ratio q
 *      or WRLOF_q_depent, where the mass accretion efficiency depends on q^2
 *      See Abate et al. (2013)
 */
#define WRLOF_MASS_TRANSFER

/************************************************************
 * Mass transfer by Roche-lobe overflow
 ************************************************************/

/*
 * Enable a command-line multiplier for the Roche-lobe mass
 * transfer rate
 */
#define RLOF_MDOT_MODULATION

/*
 * Use a fixed mass accretion efficiency (beta) for RLOF.
 * Generally you do not want to do this.
 */
//#define FIXED_RLOF_BETA 1.0

/*
 * gamma is the angular momentum factor for mass lost during Roche (-1.0).
 *
 * -2 = isotropic wind from the secondary
 * -1 = ditto from the donor
 * >=0 carries a fraction gamma of the system's specific angular momentum
 */
#define NONCONSERVATIVE_ANGMOM_GAMMA_DEFAULT -1.0

/*
 * Enable a check for pathological RLOFing, i.e. where going back in time
 * the radius does not shrink inside the Roche lobe.
 */
#define PATHOLOGICAL_RLOF_CHECK

/*
 * Allow correction of Eggleton's Roche lobe radius formula
 * by a factor which takes into account radiation force:
 * NEEDS TO BE UPDATED WITH THE LATEST FORMULA
 */
//#define RLOF_RADIATION_CORRECTION
#define RLOF_RADIATION_CORRECTION_F_DEFAULT 0.0

/*
 * Activate code to adapt the RLOF mass transfer rate based on
 * an experimental prescription which matches the radius to the
 * Roche radius by experimentally removing material from the star,
 * rather than the rate prescribed by Hurley et al 2002.
 */
#define ADAPTIVE_RLOF


/*
 * A better version of adaptive RLOF
 */
#define ADAPTIVE_RLOF2

/*
 * Parameters for zooming in on R=RL using the binary_c
 * positive timestepping algorithm
 */
#define RLOF_ZOOM_FAC 0.5
#define TIMESTEP_N_MAX_REJECTIONS 100

/*
 * Threshold for entry into RLOF.
 * For any given system, RLOF is detected when R>RL,
 * and possibly R>>RL, because of the finite time resolution.
 *
 * When this happens, we use some kind of interpolation to
 * return R to <RL*RLOF_ENTRY_THRESHOLD, because entering the
 * RLOF loop.
 */

#define RLOF_ENTRY_THRESHOLD 1.02

/*
 * When interpolating (BSE method) this is a stability parameter.
 * Was 1.001 in BSE.
 */
#define RLOF_OVERFILL 1.0

/* this was 1.0 in BSE, but 0.5 helps convergence */
#define RLOF_CONVERGENCE_FACTOR1 0.5

#define RLOF_LIMIT_TO_TWENTY_PERCENT_CHANGE

/*
 * For convective stars, simply eject the envelope without
 * going through the iterative process. This is faster and more numerically
 * stable (e.g. it does not have problems when the stellar type changes).
 */
//#define ADAPTIVE_RLOF_CONVECTIVE_CHECK

/* activate verbose logging (for the rlof.sh script) */
//#define ADAPTIVE_RLOF_LOG

/*
 * Three factors affect the stability of RLOF and the new
 * adaptive treatment: note that the defaults have been chosen
 * BY EXPERIMENT and may fail for you!
 *
 * Stars begin to RLOF when R > RLOF_OVERFLOW_THRESHOLD * R_L
 * In theory this should be 1.0 and indeed this seems ok. However,
 * the BSE code used 1.01 (or 1.02?) so you might want to use that too.
 * Large values (>1.01) lead to us flipping in and out of RLOF, so it
 * seems rather unstable ... choose 1.0!
 *
 * Even when R = R_L in the adaptive treatment, there are still other
 * effects (e.g. orbit widening) which lead to the radius moving inside
 * the Roche lobe, when in reality it would not. So, we allow RLOF
 * to continue if R > RLOF_STABILITY_FACTOR * R_L if, and only if, it
 * was already underway.
 *
 * By experiment, 0.99 works, but 0.999 may be too close to the Roche radius
 * and oscillation in and out of RLOF occurs (which is what we're trying
 * to avoid!) ... note that the results are almost identical when
 * this oscillation is allowed to happen, except that the log file is
 * filled with more details. Only make this smaller if you want to suppress
 * the log output. If you make it too small then the star will never escape
 * RLOF and of course this is wrong!
 *
 */

/*
 * defaults:
 * RLOF_OVERFLOW_THRESHOLD 1.0
 * RLOF_STABILITY_FACTOR 1.0
 */
#define RLOF_OVERFLOW_THRESHOLD 1.0
#define RLOF_STABILITY_FACTOR 1.0

/*
 * ADAPTIVE_RLOF_TARGET_FACTOR is a multiplier which is applied to R_L
 * when the iteration is taking place. e.g. if it is 1.05 then the iteration
 * will stop when the radius is actually 5% greater than the Roche lobe
 * radius. This may aid numerical stability, at the price of (slight) dodginess
 * in the physical results.
 * Default is 1.0.
 */

#define ADAPTIVE_RLOF_TARGET_FACTOR 1.0

/*
 * In some cases the explicit nature of the code is problematic.
 * Introduce some viscosity: do not allow the mass transfer rate
 * to exceed VISCOUS_RLOF_MAX_FAC* the previous timestep rate.
 * Ditto for VISCOUS_RLOF_MIN_FAC* by acts as a lower limit.
 * Defaults: Max 1.5 Min 0.5
 */
//#define VISCOUS_RLOF
#define VISCOUS_RLOF_MAX_FAC 1.1
#define VISCOUS_RLOF_MIN_FAC 0.999
//#define VISCOUS_RLOF_TRIGGER_SMALLER_TIMESTEP


/*
 * Maximum number of times we can have dr=0 before bailing out
 * of adpative RLOF and using BSE's formula.
 * This can happen when the envelope is thin and removal leads to a
 * (for example) main sequence star, the radius of which does not
 * depend on the current mass (but on the age).
 */
#define ADAPTIVE_RLOF_MAX_DRZERO_COUNT 10

/*
 * Minimum and maximum number of iterations in the adaptive scheme
 */
#define ADAPTIVE_RLOF_IMIN 4
#define ADAPTIVE_RLOF_IMAX 10000

/*
 * If the radius error < ADAPTIVE_RLOF_MINIMUM_RADIUS_ERROR
 * then stop even if we have not iterated the required
 * number (ADAPTIVE_RLOF_IMIN) of times. This is
 * because if iteration continues, the radius error tends
 * to zero, hence so does dm, and we get dr/dm = 0/0 which
 * clearly is bad. (default 1e-6)
 * Note that you might think that making this larger speeds
 * up the code: it does, but not much. (~10% speedup for 1e-6 to 1e-3)
 * (1e-6)
 */
#define ADAPTIVE_RLOF_MINIMUM_RADIUS_ERROR 1e-6

/*
 * Limit the change in the mass per iteration.
 * This can be a constant (e.g. 0.1Msun) or a fraction of the previous
 * mass (hr_mt) or the original mass (mass) or some more complicated
 * expression.
 *
 * A small value slows the code down, and may cause convergence failures
 * in ADAPTIVE_RLOF_IMAX iterations, a large value fails to converge,
 * especially if ADAPTIVE_RLOF_ALPHA is large
 *
 * This needs to be worked on!
 */
#define ADAPTIVE_RLOF_DM_MAX (Max(1e-2*mass,0.1))

/*
 * Limit the mass transfer rate to a fraction
 * ADAPTIVE_RLOF_MAX_ENV_FRAC of the envelope in
 * any one timestep.
 *
 * Ignored if zero or if menv > ADAPTIVE_RLOF_MIN_ENV.
 */
#define ADAPTIVE_RLOF_MAX_ENV_FRAC 0.5
#define ADAPTIVE_RLOF_MIN_ENV 0.05

/*
 * Apply ADAPTIVE_RLOF_ALPHA as a factor in the calculation of dm
 * based on dr/dm. Must be < 1, and a larger value leads to quicker
 * convergence, but if it is too large then overshoot occurs which is bad.
 *
 * NB I have found that 0.25 often leads to issues, so I have set alpha
 * to 0.05 to be safe...
 */
#define ADAPTIVE_RLOF_ALPHA (Max(0.01,Min(0.25,5.0/((double)i))))

/*
 * Define this to limit accretion on the secondary to the thermal
 * rate.
 */
#define LIMIT_RLOF_ACCRETION_TO_ACCRETORS_THERMAL_RATE

/*
 * Don't allow the speedup factor (orbits per timestep) to get too small
 * when the mass transfer rate is high.  Really, the timestep
 * should be allowed to get small on its own, but it collapses to zero in
 * the case of a large (RLOF) mass loss rate, which just isn't right. There's
 * some kind of feedback going on which should be looked at and fixed.
 * (This is just a fudge, but changing the value doesn't seem to alter the
 * evolution very much)
 *
 * Experiment shows that a value of 100 (or maybe even 1000) gives the
 * same result as 1. However, anything over 10000 gives a significantly
 * different mass loss rate (which is wrong, because the timestep is simply
 * too large to resolve the RLOF).
 *
 * Note that for some systems this messes *everything* up! So I have
 * reverted to 1 or none at all. (esp mass transfer from a CHeB star)
 */
//#define RLOF_MINIMUM_SPEEDUP_FACTOR 1

/*
 * Enable this to reduce the timestep such that RLOF is better resolved
 * (see stellar_timestep/timestep_RLOF.c) : this is useful but
 * slows down the code (a bit).
 * The AGGRESION is a measure of how aggressive you should be :
 * 1 is the default, but any number > 1 will cause the timestep to become
 * smaller (this is the power by which r/roche_radius is raised).
 * RLOF_REDUCE_TIMESTEP_MINIMUM_FACTOR is the minimum factor by which
 * the timestep is reduced. Note that if this is too small then the
 * osciallation in and out of RLOF will occur - this is bad! (0.01 is
 * the default).
 *
 * The RLOF_REDUCE_TIMESTEP_THRESHOLD is the minimum ratio of R/RL which
 * is required to activate the algorithm.
 */
//#define RLOF_REDUCE_TIMESTEP

#ifdef RLOF_REDUCE_TIMESTEP
#define RLOF_REDUCE_TIMESTEP_AGGRESSION (100.0)
#define RLOF_REDUCE_TIMESTEP_MINIMUM_FACTOR (1e-2)
#define RLOF_REDUCE_TIMESTEP_THRESHOLD (0.9)
#endif

/*
 * Set a minimum timestep to make sure we average out
 * mass loss from an RLOFing star (units = Myr) : this may not
 * be required any more (given RLOF_MINIMUM_SPEEDUP_FACTOR) (1e-6)
 */
#define RLOF_MINIMUM_TIMESTEP 1e-6

/*
 * In the original BSE code, the timestep depends on the current time.
 * This is bizarre, but if you do NOT include RLOF_MINIMUM_TIMESTEP,
 * this is required to prevent lockups.
 */
//#define TIMESTEP_DEPENDS_ON_THE_TIME

/*
 * Log RLOF types e.g. A (MS), B (shell-H burning), C (He burning)
 * (also x=compact object, .=none)
 */
//#define RLOF_ABC


/*
 * Limit the mass transferred in any one timestep to
 * RLOF_MAX_RATE_MASS_FRACTION times the mass or, should
 * the star have a compact (pre-WD) core, this fraction
 * times the envelope mass.
 */
#define RLOF_MAX_RATE_MASS_FRACTION 0.1

/*
 * Mass transfer efficiency on the first
 * timestep of RLOF using the Chen & Han formalism.
 * Please see RLOF_critical_q.c
 */
#define CHEN_HAN_FIRST_TIMESTEP_BETA 0.0



/************************************************************
 * Nucleosynthesis - define NUCSYN here to enable, then
 * see nucsyn/nucsyn.h for details of parts you can switch on
 * off.
 *
 * When using MINT, shellular burning and physics depending
 * on it (e.g. thermohaline mixing) require NUCSYN.
 ************************************************************/
#define NUCSYN

/***********************************************************
 * Common envelope evolution
 ***********************************************************
 *
 * Functions are in the common_envelope directory.
 */


/*
 * Maximum number of common envelope events per system.
 * This is used to set alpha, lambda etc. on a per-comenv
 * basis, and to bail out the code
 */
#define MAX_NUMBER_OF_COMENVS 100

/*
 * After this many common envelope ejections, switch to BSE.
 * Ignored if negative, but you probably want to set this to
 * MAX_NUMBER_OF_COMENVS - 3.
 */
#define COMENV_BSE_AFTER (MAX_NUMBER_OF_COMENVS - 3)

/*
 * Common envelopes must have at least this much mass
 */
#define DEFAULT_COMENV_MIN_MASS 0.1

/*
 * Lambda is the envelope binding energy parameter:
 * Ebind = -G M Menv / (R lambda)
 *
 * Note: much as we'd like to set this to one of the
 *       three LAMBDA_* macros (for the Dewi/Tauris,
 *       Wang or Polytrope) these are not reliable over
 *       the whole parameter space. Instead, use 0.5.
 */
#define DEFAULT_LAMBDA_CE 0.5

/*
 * default common envelope lambda_enthalpy
 */
#define DEFAULT_LAMBDA_ENTHALPY 0.0

/* use Hurley et al. (2002) energy balance prescription by default */
#define DEFAULT_COMENV_PRESCRIPTION COMENV_BSE

/*
 * Default value for alpha_CE in the Hurley et al. (2002) prescription
 */
#define DEFAULT_ALPHA_CE 1.0

/*
 * The ionisation/internal energy parameter (0.5)
 * default value. This is a multiplier on ionisation+internal energy
 * when using the Dewi+Tauris prescription.
 * When using polytropes it is only for ionisation.
 */
#define DEFAULT_LAMBDA_IONISATION 0.5

/*
 *
 * ceflag > 0 activates spin-energy correction in common-envelope (0).
 */
#define CEFLAG 0

/*
 * Experimental:
 * use polytropic structures to calculate lambda
 */
//#define COMENV_POLYTROPES
#define COMENV_POLYTROPE_LOGGING
#define POLYTROPE_MSRS_TABLE_LENGTH 100

/*
 * Experimental:
 * common envelope from Wang 2016
 */
#define COMENV_WANG2016

/*
 * Experimental:
 * Calculate envelope structure at common envelope
 */
#define COMENV_STRUCTURE

/* minimum value of lambda_ce */
#define LAMBDA_CE_MIN 1e-3

/*
 * The Nelemans and Tout angular-momentum prescription is
 * an alternative to    the Hurley et al. (2002) energy prescription.
 *
 * Nelemans' gamma default value (taken from his paper)
 */
#define DEFAULT_NELEMANS_GAMMA 1.75

/* limit the range of Q which can be used (Nelemans says >~0.2) */
#define NELEMANS_MIN_Q_DEFAULT 0.2

/*
 * Maximum fraction change in the angular momentum:
 * If this is 1.0 then systems are allowed to merge!
 */
#define NELEMANS_MAX_FRAC_J_CHANGE_DEFAULT 1.0

/*
 * Number of common envelopes for which we use Nelemans' prescription
 * They say 1, but maybe set to a large value (e.g. 4) to use Nelemans'
 * prescription for all common envelopes.
 */
#define NELEMANS_N_COMENVS_DEFAULT 1

/*
 * Post-common envelope compact objects can have envelopes,
 * the masses of which are defined here.
 *
 * These should not be too large because they should lead to a
 * contracting star as it evolves off the (A)GB.
 *
 * NB at the moment this applies only to GB and AGB stars.
 */
#define POST_CE_ENVELOPE_DM_GB 1e-2
#define POST_CE_ENVELOPE_DM_EAGB 1e-3
// RGI  was 2e-3 but 1e-2 gives a better transition from
// the AGB for post-CE systems
#define POST_CE_ENVELOPE_DM_TPAGB 2e-2

/*
 * Only stars with an envelope mass >  MIN_ENV_MASS_TO_HAVE_ANY_LEFT_OVER
 * can have one at the end of comenv : this is a fudge to prevent
 * repeated common-envelope evolution. Set to 0.0 to ignore. (0.01)
 */
#define MIN_ENV_MASS_TO_HAVE_ANY_LEFT_OVER 0.0

/*
 * Prevent CEE from happening if Menv < MIN_MENV_FOR_CEE Msun.
 *
 * This is really to prevent multiple RLOFs caused by post-comenv
 * evolution (e.g. orbital shrinkage by tides, winds, etc.). It's
 * not clear whether such circumstances are physically realistic.
 */
#define POST_CEE_WIND_ONLY
#define MIN_MENV_FOR_CEE Max3(POST_CE_ENVELOPE_DM_GB,           \
                              POST_CE_ENVELOPE_DM_EAGB,         \
                              POST_CE_ENVELOPE_DM_TPAGB)



/*
 * How much of the Roche lobe should be filled at the end
 * of common envelope evolution? (1.0 in theory, perhaps
 * less for numerical stability?)
 */
#define CE_ROCHE_FILL_FACTOR 0.9

/*
 * Enable this to allow accretion onto neutron stars during common
 * envelope evolution
 */
//#define COMENV_NS_ACCRETION

#ifdef COMENV_NS_ACCRETION
// mass accreted from the common envelope as a fraction of the envelope
#define COMENV_NS_ACCRETION_FRACTION_DEFAULT 0.0
// or a fixed mass
#define COMENV_NS_ACCRETION_MASS_DEFAULT 0.0
#endif //COMENV_NS_ACCRETION


/* enable this to enable accretion onto a MS star during comenv */
//#define COMENV_MS_ACCRETION

#ifdef COMENV_MS_ACCRETION
/* how much to accrete */
#define COMENV_MS_ACCRETION_MASS_DEFAULT 0.00
#endif //COMENV_MS_ACCRETION


/*
 * Macro flag to activate comenv accretion code
 */
#if defined COMENV_NS_ACCRETION ||              \
    (defined NUCSYN &&                          \
     defined COMENV_MS_ACCRETION)
#define COMENV_ACCRETION
#endif



/************************************************************
 * Stellar mergers
 ************************************************************/


/* Merged stars are given an angular momentum which is
 * taken from the pre-merger spins plus the orbital angular
 * momentum. */
#define MERGER_ANGULAR_MOMENTUM_FACTOR_DEFAULT 1.0



/*
 * Force CO-CO core mergers over MCh to explode as SNeIIa,
 * otherwise they form an ONeWD core which electron captures to a NS,
 * so the excess ONeMg material is lost together with the envelope.
 * If it is a SNIIa then the CO-CO systems explodes like a normal CO-CO merger.
 */
#define COMENV_CO_MERGER_GIVES_IIa

/*
 * Allow COWD-COWD mergers to ignite carbon quietly,
 * forming an ONeWD and ejecting the companion.
 */
//#define COWD_QUIET_MERGER

/*
 * If you enable this then the code will exit upon an immediate merger
 * of the stars at time zero
 */
//#define NO_IMMEDIATE_MERGERS

/*
 * Disable nucleosynthesis energy contribution
 * on merger of degenerate cores (e.g. in a common envelope)
 */
#define NO_MERGER_NUCLEOSYNTHESIS


/*
 * Disable HeWD - Red giant (or HG) ignition of the core and destruction
 * of the system
 */
#define HeWD_HeCORE_MERGER_IGNITION

/*
 * Mass at which HeWD accreting helium turns into a HeMS star
 */
#define HeWD_HeWD_IGNITION_MASS 0.3

/*************************************************************
 *
 * PN logging for Denise
 *
 *************************************************************/
#define DENISE_LOG

/************************************************************
 * 2020 treatment of circumstellar and circumbinary discs
 ************************************************************/
#define DISCS

#ifdef DISCS

/*
 * Max number of discs
 */
#define NDISCS 1

#define DISCS_ACCRETION_ONTO_RGB_FUDGE

/*
 * Turn on Memoize() calls : is usually slower
 */
//#define DISCS_MEMOIZE

/*
 * Material ejected from the common envelope should be put in
 * a circumbinary disc
 */
#define DISCS_CIRCUMBINARY_FROM_COMENV

/*
 * Material ejected from a wind should be put in a circumbinary disc
 */
#define DISCS_CIRCUMBINARY_FROM_WIND

/*
 * Append material to a pre-existing disc if there
 * is a second common envelope evolution
 */
#define DISCS_COMENV_APPEND

#define DISC_EVAPORATE_ON_FAILURE

#define CBDISC_ECCENTRICITY_PUMPING

/*
 * Population synthesis of disc logging
 */
#define DISC_LOG_POPSYN

#endif // DISCS


/*
 * Post-AGB star logging
 */
#define LOG_POSTAGB_STARS


/*
 * Circumbinary disk physics: deprecated. This remains
 * from the Dermine, Izzard et al paper and all its
 * features are in the 2017 circumbinary disc library.
 */
//#define CIRCUMBINARY_DISK_DERMINE

/************************************************************
 * Stellar colour library
 ************************************************************/

/*
 * Enable routines for calculating stellar colours ?
 */
#define STELLAR_COLOURS

/************************************************************
 * Internal stellar physics:
 *
 * equation of state
 * opacity
 * envelope integrations
 *
 * Note: envelope integrations requires OPACITY_ALGORITHMS
 * and one of the algorithms to be enabled, as well as
 * EQUATION_OF_STATE_ALGORITHMS and one of the algorithms
 * to be enabled.
 *
 ************************************************************/
/*
#define OPACITY_ALGORITHMS

#define OPACITY_ENABLE_ALGORITHM_PACZYNSKI
#define OPACITY_ENABLE_ALGORITHM_FERGUSON_OPAL
#define OPACITY_ENABLE_ALGORITHM_STARS

#define EQUATION_OF_STATE_ALGORITHMS

#define EQUATION_OF_STATE_ENABLE_ALGORITHM_PACZYNSKI

#define ENVELOPE_INTEGRATIONS
*/

/************************************************************
 * Logging
 ************************************************************/

/*
 * Stellar ensemble output:
 * this is REQUIRED
 */
#define STELLAR_POPULATIONS_ENSEMBLE
#define STELLAR_POPULATIONS_ENSEMBLE_SPARSE
#define STELLAR_POPULATIONS_ENSEMBLE_DEFAULT FALSE
#define STELLAR_POPULATIONS_ENSEMBLE_DEFER_DEFAULT FALSE
#define STELLAR_POPULATIONS_ENSEMBLE_LOGTIMES_DEFAULT FALSE
#define STELLAR_POPULATIONS_ENSEMBLE_DT_DEFAULT 1.0
#define STELLAR_POPULATIONS_ENSEMBLE_LOGDT_DEFAULT 0.1
#define STELLAR_POPULATIONS_ENSEMBLE_STARTLOGTIME_DEFAULT 0.1


/*
 * Enable the standard (BSE-style) file log.
 * The log can be disabled, even if FILE_LOG is defined,
 *  by setting the log_filename to /dev/null
 */
#define FILE_LOG

/*
 * call rewind instead of close/open, note that this does NOT
 * erase the log file for the previous star, it merely overwrites
 * it (should be faster!)
 */
#define FILE_LOG_REWIND

/* enable flushed output on the file log (might be slow) */
//#define FILE_LOG_FLUSH
/* enable file log output to be echoed to stdout (might be slow) */
//#define FILE_LOG_ECHO

/*
 * Colour the file log
 */
#define FILE_LOG_COLOUR

// compact and coloured log output : deprecated?
//#define DETAILED_COMPACT_LOG


/*
 * Define this to output at the end of the evolution for single stars
 * (required for time-adaptive mass grid (see binary_grid2)
 * so best to leave it defined)
 *
 * If you want streams to be flushed after this output, also define
 * SINGLE_STAR_LIFETIMES_FLUSH (not usually required)
 */
#define SINGLE_STAR_LIFETIMES
#define SINGLE_STAR_LIFETIMES_FLUSH

/*
 * Enable this to compare the results of this model to amanda's tpagb star
 * models. Activates some logging in the nucsyn library.
 */
#define CFAMANDA

/*
 * Make binary_c/nucsyn as similar to Amanda Karakas's models as possible
 */
//#define LIKE_AMANDA

/* Enable this to compare to Lynnette's models */
//#define CF_LYNNETTE


/*
 * Log post-common-envelope data
 */
//#define COMENV_LOG


// log NS->BH transitions for Enrico
//#define NS_BH_AIC_LOG
//#define NS_NS_BIRTH_LOG
#ifdef NS_BH_AIC_LOG
#ifndef SHORT_SUPERNOVA_LOG
#define SHORT_SUPERNOVA_LOG
#endif // !SHORT_SUPERNOVA_LOG

/*
  #ifndef RANDOM_SEED_LOG
  #define RANDOM_SEED_LOG
  #define RANDOM_SEED_LOG_STDERR
  #endif
*/
#endif // NS_BH_AIC_LOG

/*
 * Enable Fabian's changes for studying binary mass-transfer of massive MS stars
 * on shape of PDMF (IMF resp.)
 *
 * NB comment is outdated!
 *
 * - will enable certain kind of output in ./evolution/iterate_logging.c
 * - sets specific logging intervals in ./evolution/deltat.c
 * - sets specific exact logging times in ./evolution/deltat.c, ./binary_c_structures.h
 *   and ./setup/parse_arguments.c (via argument '--fabian_imf_log_time' which
 *   goes to stardata->model->next_fabian_imf_log_time and '--fabian_imf_log_timestep'
 *   which goes to stardata->model->fabian_imf_log_timestep)
 * - default values are set in ./iteration/init_model.c
 * - tries to calculate an "observed" mass by adding the luminosities of the
 *   binary stars and using an inversion of ZAMS L-M-Z relation from
 *   Tout et. al (1996) to gain mass from "observed" luminosity
 * - ./setup/version.c adjusted such that it states the default preferences
 */
//#define FABIAN_IMF_LOG

// Stuff for Rob Detmers
//#define DETMERS_LOG


/* RSTARS does nothing but activate other switches */
//#define RSTARS

#ifdef RSTARS
#define NO_MERGER_NUCLEOSYNTHESIS
#define HeWD_HeCORE_MERGER_IGNITION
#define LOG_COMENV_RSTARS
#define LOG_RSTARS

/* disallow He-He core merger explosions */
#define NO_MERGER_NUCLEOSYNTHESIS
#define HeWD_HeCORE_MERGER_IGNITION

#endif//RSTARS

// binned HR diagram output
//#define HRDIAG

#ifdef HRDIAG
//#define HRDIAG_DTLOG

// and the output bin accuracy
#define HRDIAG_BIN_SIZE_LOGTIME 0.05
#define HRDIAG_START_LOG 5.0
#define HRDIAG_BIN_SIZE_TEFF 100.0
#define HRDIAG_BIN_SIZE_LOGL 0.1
#define HRDIAG_BIN_SIZE_LOGG 0.5
#define HRDIAG_BIN_SIZE_GEFF 0.1

// enable this for more time resolution at the AGB->WD transition
// BEWARE creates a LOT of output
#define HRDIAG_EXTRA_RESOLUTION_AT_END_AGB

/* append extra nucleosynthesis information? */
#define HRDIAG_APPEND_NUCSYN

//#define HRDIAG_THICK_DISK_GIANTS_ONLY

// for best grid : require single star lifetimes
#undef SINGLE_STAR_LIFETIMES
#define SINGLE_STAR_LIFETIMES

//#undef FILE_LOG

#endif//HRDIAG


/* enable extra logging of parameters relevant to SNeIa */
//#define EXTRA_IA_LOGGING


/* define this to include the stardata_status function, which outputs
 * everything about each star */
//#define STARDATA_STATUS

/*
 * Enable logging to compare to herbert's stars: note that this also
 * turns off mass loss!
 */
//#define LOG_HERBERT

/*
 *  Final secondary mass log (for Selma)
 */
//#define FINAL_MASSES_LOG



#ifdef CFAMANDA
#undef REIMERS_ETA_DEFAULT
/* use NETA=0.4 to match amanda's tpagb models */
#define REIMERS_ETA_DEFAULT 0.4
#endif

/*
 * Joke's nova log
 */
//#define LOG_NOVAE


/*
 * testing log for individual_novae
 */
//#define LOG_INDIVIDUAL_NOVAE

/* Here you can set the angular momentum (just prior to SN?) checks */
//#define ANG_MOM_CHECKS

/* enable this to allow a short description of any supernovae */
//#define SHORT_SUPERNOVA_LOG

/* SN log in the log file (old, might not work)
 *
 * Note: LOG_SUPERNOVAE has been deprecated and will be removed!
 */
//#define LOG_SUPERNOVAE

/* log for Sung-Chul Yoon project (enable both!)
 * NB Doesn't work with API
 */
//#define SUPERNOVA_COMPANION_LOG
//#define SUPERNOVA_COMPANION_LOG2

/*
 * Enable LOG_SUPERNOVAE, SUPERNOVA_COMPANION_LOG and SUPERNOVA_COMPANION_LOG2
 * for Takashi's project
 */


/* resolutions */
#define SNCOMP_LOGTIME_RESOLUTION 0.1
#define SNCOMP_MASS_RESOLUTION 0.1
#define SNCOMP_LOGL_RESOLUTION 0.1
#define SNCOMP_LOGR_RESOLUTION 0.1
#define SNCOMP_LOGG_RESOLUTION 0.1
#define SNCOMP_LOGTEFF_RESOLUTION 0.1
#define SNCOMP_LOGSEP_RESOLUTION 0.1
#define SNCOMP_LOGPER_RESOLUTION 0.1
#define SNCOMP_ECC_RESOLUTION 0.05
/* only binaries? */
//#define SNCOMP_BINARIES_ONLY


/* check for sdB stars */
//#define SDB_CHECKS

/* the individual stars */
//#define BI_LYN



// check for X-ray binary?
//#define XRAY_BINARIES
#ifdef XRAY_BINARIES
#define XRAY_LUMINOSITY
#endif

// log stellar types
//#define STELLAR_TYPE_LOG

// Stuff for Eldridge
//#define LOG_JJE

/* Log for Lattanzio/Ryan work */
//#define NUCSYN_LOG_JL

/* selma's stuff for MS binary RLOF*/
//#define SELMA

//#define CARLO_OUTPUT

/* project with Lilia, Dayal and Tyl */
//#define CANBERRA_PROJECT

/* project with Evert Glebbeek and Nathan Leigh */
//#define BLUE_STRAGGLER_PROJECT

/* TPAGB tracks for masters proposal */
//#define TPAGBTRACKS

/* log for Window To The Stars 2 */
#define WTTS_LOG

/* anti TZ project */
//#define ANTI_TZ

/* project to look at wind energy from binaries */
//#define WIND_ENERGY_LOGGING

/* turn on Gaia logging */
#define GAIA

/* Masseron's logging */
//#define MASSERON


/*
 * Detect CSJ's sdO stars
 */
//#define CSJ_SDO_STARS

//#define CFDENISE





/************************************************************
 * Testing
 ************************************************************/

/* run stellar colour tests? */
//#define STELLAR_COLOUR_TESTS

// test Eldridge vs BSE
//#define LTEST

/*
 * Activate tests for the Nature submission
 */
//#define TEST_NATURE


/************************************************************
 * Dependencies
 ************************************************************/


/* changes to match Denise's branch */

#ifdef CFDENISE
#undef NUCSYN_SMOOTH_CORE_MASS_TRANSITION
#define NUCSYN_FIRST_DREDGE_UP_ACCRETION_CORRECTION_FROM_TAMS
#undef LITHIUM_TABLES
#undef TPAGB_SPEEDUP
#undef NUCSYN_SIGMAV_PRE_INTERPOLATE
#define KARAKAS2002_REFITTED_TPAGB_INTERPULSES
#define RLOF_NO_STELLAR_TYPE_CHANGE_IF_DTM_NEGATIVE
#undef RLOF_ENTRY_THRESHOLD
#define RLOF_ENTRY_THRESHOLD 1.0
#undef RLOF_OVERFILL
#define RLOF_OVERFILL 1.001
#undef RLOF_CONVERGENCE_FACTOR1
#define RLOF_CONVERGENCE_FACTOR1 1.0
#undef RLOF_LIMIT_TO_TWENTY_PERCENT_CHANGE
#endif//CFDENISE

#ifdef HRDIAG__XXXXX
#undef ADAPTIVE_RLOF
#undef NANCHECKS
#ifndef HRDIAG_APPEND_NUCSYN
#undef NUCSYN
#endif//HRDIAG_APPEND_NUCSYN
#endif//HRDIAG_XXXXX

#ifdef NUCSYN
#include "nucsyn/nucsyn_parameters.h"
#endif

#ifdef FABIAN_IMF_LOG
#define NEXT_FABIAN_IMF_LOG_TIME_DEFAULT 9.999999 // exact logging begins at this time (in Myr)
#define FABIAN_IMF_LOG_TIMESTEP_DEFAULT 100 // timestep for logging (Myr)
#endif


// for Sung Chul's project we want to fix the He star accretion bug
#if (defined(SUPERNOVA_COMPANION_LOG2) && (!defined(HE_STAR_ACCRETING_H_FIX)))
#define HE_STAR_ACCRETING_H_FIX
#endif


#ifdef BINARY_C_API
/* some features do not work with the API */
#include "API/binary_c_API_excluded_features.h"
/* some features are required by the API */
#include "API/binary_c_API_required_features.h"
#endif //BINARY_C_API

#if (defined(TEST_NATURE) && (!defined(RLOF_ABC)))
#define RLOF_ABC
#endif //TEST_NATURE

#ifdef LIKE_AMANDA
#undef AXEL
#undef AXEL_RGBF_FIX
#undef ACQUIRE_FIX_NEGATIVE_AGES
#undef ACQUIRE_TEST_FOR_NEGATIVE_AGES
#undef GIANT_AGE_FATAL_ERRORS
#undef NO_MERGER_NUCLEOSYNTHESIS
#undef USE_2011_MASS_LOSS

#endif // LIKE_AMANDA

#ifdef ADAPTIVE_RLOF

#define MODULATE_TIMESTEP_TO_RESOLVE_ROTATION_MASS_LOSS
#endif

/* default dependencies for envelope integrations */
#ifdef ENVELOPE_INTEGRATIONS
#if !defined OPACITY_ENABLE_ALGORITHM_PACZYNSKI &&      \
    !defined OPACITY_ENABLE_ALGORITHM_FERGUSON_OPAL
#define OPACITY_ENABLE_ALGORITHM_FERGUSON_OPAL
#endif
#if !defined OPACITY_ALGORITHMS
#define OPACITY_ALGORITHMS
#endif
#if !defined EQUATION_OF_STATE_ALGORITHMS
#define EQUATION_OF_STATE_ALGORITHMS
#endif
#if !defined EQUATION_OF_STATE_ENABLE_ALGORITHM_PACZYNSKI
#define  EQUATION_OF_STATE_ENABLE_ALGORITHM_PACZYNSKI
#endif
#endif // ENVELOPE_INTEGRATIONS



/*
 * Enable updates for triple code
 */
//#define TRIPLE

/*
 * Energy ranges to define FUV, EUV and X-ray,
 * in eV.
 *
 * Following Richling and Yorke
 *
 * FUV photons have 6 < E / eV < 13.6
 * EUV photons have 13.6 < E/eV < 100.0
 * X-ray photons have 100.0 < E/eV < 10^5
 *
 */
#define FUV_eV_MIN 6.0
#define FUV_eV_MAX 13.6
#define EUV_eV_MIN 13.6
#define EUV_eV_MAX 1e2
#define XRAY_eV_MIN 1e2
#define XRAY_eV_MAX 1e5


/*
 * Fabian's cosmology project
 */
//#define FABIAN_COSMOLOGY

/*
 * Allow time to reverse
 */
//#define REVERSE_TIME


/*
 * Exponential tides? This is always more stable,
 * so you should use it. It may not quite be correct when
 * there are multiple sources of spin up and spin down
 * acting together, because they will not be added quite
 * correctly. However, usually it's fine because either tides
 * dominate or are minimal (they're stiff!).
 */
#define EXPONENTIAL_TIDES

/*
 * Allow the core to have a different spin to the envelope.
 * EXPERIMENTAL
 */
//#define OMEGA_CORE

/*
 * Minimum mass of helium on top of a COWD for
 * the star to be labelled "hybrid"
 */
#define MIN_MASS_He_FOR_HYBRID_COWD (1e-3)

/*
 * Hydrogen-envelope masses below which we
 * apply the accretion rate limit to helium-core
 * growth.
 *
 * We apply this to GB and CHeB stars and
 * have different limits for each type.
 *
 * Note: these are rather arbitrary!
 */
#define ACCRETION_LIMIT_H_ENV_GB 0.05
#define ACCRETION_LIMIT_H_ENV_CHeB 0.05
#define ACCRETION_LIMIT_H_ENV_EAGB 0.05


/*
 * Log helium stars that become CO white dwarfs
 * with some helium left on top
 */
//#define LOG_HYBRID_WHITE_DWARFS


/*
 * Print repeat number in the log file
 */
#define LOG_REPEAT_NUMBER

/*
 * System logging
 */
//#define SYSTEM_LOGGING

/*
 * Send data about timestep rejections to the log file.
 * Warning: may lead to a lot of output.
 */
//#define LOG_REJECTIONS


/*
 * Zhengwei's SNIa systems
 */
//#define ZW2019_SNIA_LOG

/*
 * Logging for Gaia-like HRD
 */
//#define GAIAHRD

#ifdef GAIAHRD
#ifndef RESOLVE_POSTAGB
#define RESOLVE_POSTAGB
#endif
#endif//GAIAHRD


/*
 * Introduce timestep limits to resolve the post-AGB
 * phase (around 10kK)
 */
#define RESOLVE_POSTAGB


/*
 * Set stellar type changes to be logged
 * as events
 */
//#define STELLAR_TYPE_CHANGE_EVENTS

/*
 * Write timestep rejections to the main log
 */
#define LOG_REJECTIONS

/*
 * Include user's header file binary_c_user.h here.
 *
 * This MUST be included here after parameters are defined
 * so these parameters can be altered before, say,
 * structures and prototypes are defined.
 *
 * This feature requires __has_include : we
 * also test if __has_include exists so it can work.
 * It should work on a modern gcc or clang compiler.
 *
 * Reading at
 * https://gcc.gnu.org/onlinedocs/cpp/_005f_005fhas_005finclude.html
 */
#if defined __has_include
#if  !defined BINARY_C_USER_H
#if    __has_include("binary_c_user.h")
#      define BINARY_C_USER_H
#      include "binary_c_user.h"
#      endif
#   endif
#endif


/*
 * Transition from HeMS to HeWD on a thermal timescale
 */
#define HeMS_HeWD_THERMAL_TRANSITION

#define ETA_RUITER2013 0.75

/*
 * Save mass-loss history to a cdict?
 */
//#define SAVE_MASS_HISTORY

/*
 * Enable YBC bolometric correction tables
 */
#define YBC

/*
 * Enable event-based logging functionality.
 * For a selected set of events (like RLOF, SN),
 * we provide a standardised output which is written at
 * the end of the evolution and correctly handles the
 * splitting of systems properly (i.e. split systems obtain
 * a copy of their previous history)
 */
#define EVENT_BASED_LOGGING

/*
 * Treat overspinning stellar mass/angular momentum loss
 * as an event.
 */
#define OVERSPIN_EVENTS


/* Switches to activate the Pair instability supernovae
 * and Pulsational Pair Instability Supernovae routines
 * Activating PPISN will activate both.
 */
#define PPISN

/*
 * Turn on Mike Lau's 2024 prescription for thermal
 * expansion of secondaries on accretion.
 * See arXiv 2401.09570
 */
#define LAU2024_THERMAL_EXPANSION

/*
 * Faster argument parsing using hashes rather than
 * linear searches.
 */
#define HASH_ARGUMENTS

#endif //BINARY_PARAMETERS_H
