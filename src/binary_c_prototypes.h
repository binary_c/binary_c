#pragma once
#ifndef BINARY_PROTOTYPES_H
#define BINARY_PROTOTYPES_H

/*
 * The binary_c stellar population nucleosynthesis framework.
 *
 * Copyright Robert Izzard 2000-2025
 *
 * Contact rob.izzard@gmail.com
 *
 * Code repository:
 * https://gitlab.com/binary_c
 *
 * Documentation is in doc/
 * Code is in src/
 *
 * To build, run
 *
 * ---
 *
 * meson setup builddir
 * cd builddir
 * ninja binary_c_install
 *
 * ---
 *
 * Please see the files README, LICENCE and CHANGES,
 * and the doc/ directory for documentation which is mirrored
 * at https://binary_c.gitlab.io
 *
 **********************
 *
 * Purpose:
 *
 * This header file loads in all the binary_c function
 * prototypes.
 */


#include <stdio.h>
#include <stdlib.h>
#include "binary_c_code_options.h"
#include "binary_c_parameters.h"
#include "binary_c_macros.h"
#include "binary_c_structures.h"

#include "./binary_c_main_prototypes.h"
#include "./debug/debug_prototypes.h"
#include "./interface/interface_prototypes.h"
#include "./file/file_prototypes.h"
#include "./stack/stack_prototypes.h"
#include "./batchmode/batchmode_prototypes.h"
#include "./memory/memory_prototypes.h"
#include "./tables/tables_prototypes.h"
#include "./timers/timers_prototypes.h"
#include "./string/string_prototypes.h"
#include "./control/control_prototypes.h"
#include "./signals/signals_prototypes.h"
#include "./evolution/evolution_prototypes.h"
#include "./events/events_prototypes.h"
#include "./ensemble/ensemble_prototypes.h"
#include "./zfuncs/zfuncs_prototypes.h"
#include "./single_star_functions/single_star_functions_prototypes.h"
#include "./stellar_timescales/stellar_timescales_prototypes.h"
#include "./binary_star_functions/binary_star_functions_prototypes.h"
#include "./tides/tides_prototypes.h"
#include "./novae/novae_prototypes.h"
#include "./setup/setup_prototypes.h"
#include "./stellar_structure/stellar_structure_prototypes.h"
#include "./logging/logging_prototypes.h"
#include "./RLOF/RLOF_prototypes.h"
#include "./maths/maths_prototypes.h"
#ifdef __HAVE_LIBRINTERPOLATE__
#  include <rinterpolate_prototypes.h>
#else
#  include "librinterpolate/rinterpolate_prototypes.h"
#endif//__HAVE_LIBRINTERPOLATE__
#include "./misc/misc_prototypes.h"
#include "./wind/wind_prototypes.h"
#include "./stellar_colours/stellar_colours_prototypes.h"
#include "./buffering/buffering_prototypes.h"
#include "./disc/disc_prototypes.h"
#include "./dust/dust_prototypes.h"
#include "./massive/massive_prototypes.h"
#include "./rotation/rotation_prototypes.h"
#include "./timestep/timestep_prototypes.h"
#include "./spectra/spectra_prototypes.h"
#include "./common_envelope/common_envelope_prototypes.h"
#include "./supernovae/supernovae_prototypes.h"
#include "./opacity/opacity_prototypes.h"
#include "./equation_of_state/equation_of_state_prototypes.h"
#include "./envelope_integration/envelope_integration_prototypes.h"
#include "./breakpoints/breakpoints_prototypes.h"
#include "./pulsation/pulsation_prototypes.h"
#include "./units/units_prototypes.h"
#include "./circumstellar_disc/circumstellar_disc_prototypes.h"
#ifdef ORBITING_OBJECTS
#include "./orbiting_objects/orbiting_objects_prototypes.h"
#endif // ORBITING_OBJECTS
#ifdef __HAVE_LIBMEMOIZE__
#  include <memoize_prototypes.h>
#else
#  include "./libmemoize/memoize_prototypes.h"
#endif
#include "binary_c_exit_prototypes.h"
#include "orbit/orbit_prototypes.h"
#include "MINT/MINT_prototypes.h"
#include "data/data_prototypes.h"
#include "transients/transients_prototypes.h"
#ifdef YBC
#include "stellar_colours/ybc.h"
#endif//YBC
#include "event_based_logging/event_based_logging_prototypes.h"

void reset_binary_c_timeout(void);
void disable_binary_c_timeout(void);
void hoover_and_sms(struct stardata_t * const stardata,
                    const unsigned int s) No_return;


/*
 * Force _exit() to be used ONLY for temporary code.
 * Issue a stern warning otherwise.
 */
void _exit(int status) Deprecated("_exit should only be used for temporary code!");


#endif /* BINARY_PROTOTYPES_H */
