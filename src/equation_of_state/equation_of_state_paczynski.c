#include "../binary_c.h"

No_empty_translation_unit_warning;

#if defined EQUATION_OF_STATE_ALGORITHMS &&     \
    defined EQUATION_OF_STATE_ENABLE_ALGORITHM_PACZYNSKI
#include "equation_of_state.h"

const int vb_eos = 1;

struct xion_t {
    double xh1;
    double xhe1;
    double xhe2;
    double helim2;
    double rhc1;
    double rhc2;
    double rhc3;
    double zav;
};

struct nion_t {
    double  nH2; // hydrogen molecules
    double  nHI; // neutral hydrogen
    double  nHII; // ionized hydrogen
    double  nHeI; // neutral helium
    double  nHeII; // ionized helium
    double  nHeIII; // doubly-ionized helium
    double  ne; // free electrons
    double  nt; // total number of particles
};

struct grad_t {
    double vad;
    double grad;
    double gradad;
    double gradra;
    double vt;
    //double delt; // unused?
};

static void termo(
    const double ro,
    const double t,
    const double x,
    const double y,
    const double z,
    const double typ,
    struct xion_t * Restrict xion,
    struct nion_t * Restrict nion,
    struct grad_t * Restrict grad,
    double * Restrict q,
    double * Restrict Cp,
    double * Restrict p,
    double * Restrict pgas,
    double * Restrict prad,
    double * Restrict dpro,
    double * Restrict dpt,
    double * Restrict u,
    double * Restrict Cv,
    double * Restrict error
    );

static void energ(const double ro,
                  const double t,
                  const double x,
                  const double y,
                  const double typ,
                  double * Restrict p,
                  double * Restrict pgas,
                  double * Restrict prad,
                  double * Restrict u,
                  struct xion_t * Restrict xion,
                  struct nion_t * Restrict nion,
                  double * Restrict error
    );

static void fzz(double * Restrict zz,
                double * Restrict f1,
                double * Restrict f2,
                double * Restrict f3,
                const double f4,
                const double f5);

static double Pure_function funzz(double zz,
                                  double f1,
                                  double f2,
                                  double f3,
                                  double f4,
                                  double f5);


void equation_of_state_paczynski(Equation_of_state_args_in)
{
    /*
     * Paczynski's equation of state, based on the
     * GOB code of the 60s/70s
     */
    const double typ = 1.0;

    struct xion_t xion = {
        .xh1 = eq->xh1,
        .xhe1 = eq->xhe1,
        .xhe2 = eq->xhe2
    };
    struct nion_t nion;
    struct grad_t grad;

    double error = 0.0;

    termo(
        eq->density,
        eq->temperature,
        eq->X,
        eq->Y,
        eq->Z,
        typ,
        &xion,
        &nion,
        &grad,
        &(eq->dlnrho_dlnT_P), // q
        &(eq->Cp), // Cp
        &(eq->P), // p
        &(eq->Pgas),// p gas
        &(eq->Prad),// p rad
        &(eq->dP_drho_T), // dpro
        &(eq->dP_dT_rho), // dpt
        &(eq->U), // u
        &(eq->Cv), // cv
        &error
        );

    /* set and convert values where required */
    eq->dlnrho_dlnT_P *= -1.0;
    eq->xh1 = xion.xh1;
    eq->xhe1 = xion.xhe1;
    eq->xhe2 = xion.xhe2;
    eq->gradad = grad.gradad;
    eq->cs_ad = grad.vad;


    eq->nH2 = nion.nH2;
    eq->nHI = nion.nHI;
    eq->nHII = nion.nHII;
    eq->nHeI = nion.nHeI;
    eq->nHeII = nion.nHeII;
    eq->nHeIII = nion.nHeIII;
    eq->ne = nion.ne;
    eq->nt = nion.nt;

    eq->error = error;
}

static void termo(
    const double ro,
    const double t,
    const double x,
    const double y,
    const double z,
    const double typ,
    struct xion_t * Restrict xion,
    struct nion_t * Restrict nion,
    struct grad_t * Restrict grad,
    double * Restrict q,
    double * Restrict Cp,
    double * Restrict p,
    double * Restrict pgas,
    double * Restrict prad,
    double * Restrict dpro,
    double * Restrict dpt,
    double * Restrict u,
    double * Restrict Cv,
    double * Restrict error
    )
{
/*
 *   input:
 *       ro      density(c.g.s.)
 *       t       temp(k)
 *       x       hydrogen mass fraction
 *       y       helium mass fraction
 *       typ     control variable:
 *                       > 0 include radiation in ionization region
 *                       <=0 neglect radiation in ionization region
 *   output:
 *       q       -(d ln rho /d ln t)p                           |
 *       Cp      (du/dt)p   specific heat cap. at const p       |
 *       gradad  (d ln t/d ln p)s   adiabatic gradient          |
 *       p       pressure (c.g.s.)                              |
 *       dpro    (dp/drho)t                                     |--> c.g.s.k.
 *       dpt     (dp/dt)rho                                     |
 *       u       specific internal energy                       |
 *       Cv=Cv     (du/dt)rho  specific heat cap. at const vol.   |
 *       vad     adiabatic sound speed                          |
 *
 *       an adjustment is made to take into account weak electron degen.,
 *       here for full ionization, in energ for partial ionization
 */

    double duro;
    double rot = ro * t;

    if(vb_eos)
        printf("termo in%12.3E%12.3E%12.3E%12.3E%12.3E\n",
               ro,
               t,
               x,
               y,
               typ);

    /*
     * Common blocks dens and dathe2 were global variables: instead
     * just set them here.
     *
     * set the values of critical densities for pressure ionization:
     * "rhc1", "rhc2", "rhc3"
     * the value of critical second helium ionization: "helim2"
     * and the average charge of "metals": "zav" .
     */
    xion->helim2 = 0.99;
    xion->zav    = 10.0;
    xion->rhc1   = 0.1; // exp10( -1.0);
    xion->rhc2   = 0.3162277660168379332; // exp10( -0.5);
    xion->rhc3   = 1.0; // exp10( 0.0);

    if(vb_eos)printf("termo branch%12.3E%12.3E\n",xion->xhe2,xion->helim2);

    if(xion->xhe2 >= xion->helim2)
    {
        /*
         * full ionization
         */
        xion->xh1 = 1.0;
        xion->xhe1 = 0.0;
        xion->xhe2 = 1.0;

        /*
         *  p1 is for particles, p2 is for photons
         */
        double nelect = x + 0.5  * y + 0.5 * z;
        double nnucl  = x + 0.25 * y + 0.5 * z / xion->zav;
        double mu    = 0.5 * ( 1.0 + x );
        double tt     = t * 1e-6;
        double nedegen  = nelect * (
            1.0
            +
            2.19e-2 * ro / ( mu * Pow1p5(tt) )
            );

        double pg = 0.825075e8 * ( nedegen + nnucl ); // was p1
        double pr = 2.523e-15 * Pow3(t) / ro; // was p2

        *prad = pr * rot;
        *pgas = pg * rot;

        *p = *prad + *pgas;

        *u = ( 1.5 * pg + 3.0 * pr ) * t + 1.516e13 * x + 1.890e13 * y;

        *dpro = pg * t;
        *dpt  = (pg + 4.0 * pr) * ro;
         duro = -3.0 * pr * t / ro;
        *Cv  = 1.5 * pg + 12.0 * pr;

        if(vb_eos)printf("full ion%12.3E%12.3E%12.3E%12.3E%12.3E%12.3E\n",
               *p,*u,*dpro,*dpt,duro,*Cv);


        /* no neutral species */
        nion->nH2 = 0.0;
        nion->nHI = 0.0;
        nion->nHeI = 0.0;
        nion->nHeII = 0.0;

        /* all H, He are in ionized states */
        const double h = 1.6734e-24;
        nion->nHII = x * ro / h;
        nion->nHeIII = 0.25 * y * ro / h;
        nion->ne = nelect;
        double nmet = 0.5 / xion->zav * ( 1.0 - x - y ) * ro / h;
        nion->nt = nion->nHII + nion->nHeIII + nedegen + nmet;
    }
    else
    {
        /*
         * partial ionization of hydrogen and helium
         */
        double pp[4],uu[4],pg[4],pr[4];
        struct nion_t nions[4];

#define Energ_args(N) x,                        \
            y,                                  \
            typ,                                \
            &pp[(N)],                           \
            &pg[(N)],                           \
            &pr[(N)],                           \
            &uu[(N)],                           \
            xion,                               \
            &(nions[(N)]),                      \
        error

        const double d = 1e-3;

        energ(ro,(1.0-d)*t,Energ_args(0));

        if(vb_eos)printf("energ1%12.3E%12.3E\n",pp[0],uu[0]);

        energ(ro,(1.0+d)*t,Energ_args(1));
        if(vb_eos)printf("energ2%12.3E%12.3E\n",pp[1],uu[1]);

        energ((1.0-d)*ro,t,Energ_args(2));
        if(vb_eos)printf("energ3%12.3E%12.3E\n",pp[2],uu[2]);

        energ((1.0+d)*ro,t,Energ_args(3));
        if(vb_eos)printf("energ4%12.3E%12.3E\n",pp[3],uu[3]);

#define MEAN4(X) ( ( X[0] + X[1] + X[2] + X[3] ) * 0.25 )
#define MEANNION4(X) ( ( nions[0].X + nions[1].X + nions[2].X + nions[3].X ) * 0.25 )

        *p = MEAN4(pp);
        *pgas = MEAN4(pg);
        *prad = MEAN4(pr);
        *u = MEAN4(uu);

        nion->nH2 = MEANNION4(nH2);
        nion->nHI = MEANNION4(nHI);
        nion->nHII = MEANNION4(nHII);
        nion->nHeI = MEANNION4(nHeI);
        nion->nHeII = MEANNION4(nHeII);
        nion->nHeIII = MEANNION4(nHeIII);
        nion->ne = MEANNION4(ne);
        nion->nt = MEANNION4(nt);

        *dpro = ( pp[3] - pp[2] ) / (ro * 2.0 * d);
        *dpt  = ( pp[1] - pp[0] ) / (t * 2.0 * d);
         duro = ( uu[3] - uu[2] ) / (ro * 2.0 * d);
        *Cv  = ( uu[1] - uu[0] ) / (t * 2.0 * d);

        if(vb_eos)printf("part ion%12.3E%12.3E%12.3E%12.3E%12.3E%12.3E\n",
               *p,*u,*dpro,*dpt,duro,*Cv);
    }

    /*
     * evaluation of more complex thermodynamic functions and the error
     */
    *q  =  t * *dpt / ( *dpro * ro );
    *Cp = *Cv + *q / ro * *dpt;
    grad->gradad = *p * *q / (*Cp * rot);
    grad->vad = sqrt( Max(0.0,*dpro * *Cp / *Cv) );
    printf("VAD %g * %g / %g\n",*dpro, *Cp, *Cv);
    if(vb_eos)printf("termo out%12.3E%12.3E%12.3E%12.3E\n",
           *q,*Cp,grad->gradad,grad->vad);

    double er1 = fabs(1.0 - t / *p * *dpt - duro * Pow2(ro) / *p) + 1e-10;
    *error = log10(er1);

    if(vb_eos)printf("termo error%12.3E%12.3E\n",er1,*error);

}



static void energ(
    const double ro,
    const double t,
    const double x,
    const double y,
    const double typ,
    double * Restrict p,
    double * Restrict pgas,
    double * Restrict prad,
    double * Restrict u,
    struct xion_t * Restrict xion,
    struct nion_t * Restrict nion,
    double * Restrict error
    )
{
    /*
     *   input:
     *       ro      density(c.g.s.)
     *       t       temp(k)
     *       x       hydrogen mass fraction
     *       y       helium mass fraction
     *       typ     control variable:
     *                       > 0 to include radiation
     *                       <=0 to neglect radiation
     *   output:
     *       p       pressure(c.g.s.)
     *       u       specific internal energy
     *
     *               the gas is assumed non-relativistic.  a first-order
     *               electron-degeneracy correction is made.
     *               the metals are assumed to be fully ionized (poor assumption)
     *
     * rhc1, rhc2, rhc3  are critical densities for "pressure ionization"
     */
    double nt,nh2,nhi,nhii,nhei,nheii,nheiii,ne,pg,pr,
        uh2,teta,logt,fac,nh,nhe,nmet,mu,nmetel,nedgen;
    const double dm = 2.302585;
    const double h = 1.6734e-24;

    if(vb_eos)printf("energ in%12.3E%12.3E%12.3E%12.3E%12.3E\n",
           ro,t,x,y,typ);

    double tm = t / ( 5040.0 * dm );
    teta = 5040.0 / t;
    logt = log10( t );
    mu = 0.5 * ( 1.0 + x );

    nh = x * ro / h;
    nhe = 0.25 * y * ro / h;
    nmet = 0.5 / xion->zav * ( 1.0 - x - y ) * ro / h;

    if(vb_eos)printf("energA%12.3E%12.3E%12.3E%12.3E%12.3E%12.3E%12.3E%12.3E%12.3E\n",
           tm,mu,nh,nhe,nmet,xion->zav,h,x,y);

    /*
     * assume metals fully ionized
     */
    nmetel = nmet * xion->zav;
    ne = 0.0;
    nhi = nh;
    nhii = 0.0;
    nhei = nhe;
    nheii = 0.0;
    nheiii = 0.0;

    xion->xh1 = 0.0;
    xion->xhe1 = 0.0;
    xion->xhe2 = 0.0;

    double fac1 = 0.0;
    double fac2 = 0.0;
    double fac3 = 0.0;

    /*
     * hydrogen ionization
     */
    double hi1 = 13.595;
    double hi = hi1*(1.0 - ro / xion->rhc1 * (1.0 + tm / hi1 ) );
    double fhl = log10(nh);
    double b10 = 15.3828 + 1.5 * logt - hi * teta - fhl;

    if(vb_eos)printf("energB%12.3E%12.3E\n",
           hi,b10);

    if(b10 >= -10.0)
    {
        if(b10 > 10.0) b10 = 10.0;

        double b = exp10( b10);
        double c = b;
        fac1 = c * nh;
        double bc = 0.5 * b / c;
        double xx = 1.0 / ( sqrt( Pow2(bc) + 1.0 / c) + bc);

        /*
         * xx is the positive root of equation: " xx**2 + b*xx - c = 0 "
         */
        double xx1 = 1.0 - xx;
        if(xx1 < 1e-10) xx1 = 1e-10;
        nhii = nh * xx;
        ne = nhii;
        nhi = nh * xx1;
        xion->xh1 = xx;

        if(vb_eos) printf("energC%12.3E%12.3E%12.3E\n",
                      xion->xh1,nhi,nhii);
        /*
         * first helium ionization
         */
        double hi2 = 24.580;
        hi = hi2 * ( 1.0 - ro / xion->rhc2 * ( 1.0 + tm / hi2 ) );
        double fhel = log10(nhe);
        b10 = 15.9849 + 1.5 * logt - hi * teta - fhel;

        if(vb_eos)printf("energD%12.3E%12.3E\n",
               hi,b10);

        if(b10 >= -10.0)
        {
            if(b10 > 10.0) b10 = 10.0;

            c = exp10( b10);
            b = c + ne / nhe;
            fac2 = c * nhe;
            bc = 0.5 * b / c;
            xx = 1.0 / ( sqrt( Pow2(bc) + 1.0 / c ) + bc);
            xx1 = 1.0 - xx;
            if(xx1 < 1e-10) xx1 = 1e-10;
            nheii = nhe * xx;
            ne += nheii;
            nhei = nhe * xx1;
            xion->xhe1 = xx;

            if(vb_eos)printf("energE%12.3E\n",xion->xhe1);

            /*
             * second helium ionization
             */
            const double hi3 = 54.403;
            hi = hi3 * ( 1.0 - ro / xion->rhc2 * ( 1.0 + tm / hi3 ) );
            fhel = log10(nheii);
            b10 = 15.3828 + 1.5 * logt - hi * teta - fhel;

            if(b10 >= -10.0)
            {
                if(b10 > 10.0) b10 = 10.0;

                c = exp10( b10);
                b = c + ne / nheii;
                fac3 = c * nheii;
                bc = 0.5 * b / c;
                xx = 1.0 / ( sqrt( Pow2(bc) + 1.0 / c ) + bc);
                xx1 = 1.0 - xx;
                if(xx1 < 1e-10) xx1 = 1e-10;
                nheiii = nheii * xx;
                ne += nheiii;
                nheii = nheii * xx1;
                xion->xhe2 = xx;

                if(vb_eos) printf("energF%12.3E\n",xion->xhe2);

            }
            /*
             * correct the ionization of hydrogen and helium
             */
            double f1 = fac1 / ne;
            double f2 = fac2 / ne;
            double f3 = fac3 / ne;
            const double f4 = nh / ne;
            const double f5 = 0.25 * y/x;
            double zz = 1.0;

            fzz(&zz,&f1,&f2,&f3,f4,f5);

            if(vb_eos)printf("energG%12.3E%12.3E%12.3E%12.3E%12.3E%12.3E\n",
                   zz,f1,f2,f3,f4,f5);

            ne *= zz;

            xion->xh1 = f1 / ( 1.0 + f1);
            xion->xhe1 = f2 / ( 1.0 + f2 * ( 1.0 + f3 ) );
            xion->xhe2 = xion->xhe1 * f3;

            nhi = nh * ( 1.0 - xion->xh1 );
            nhii = nh * xion->xh1;
            nhei = nhe * (1.0 - xion->xhe1 - xion->xhe2);
            nheii = nhe * xion->xhe1;
            nheiii = nhe * xion->xhe2;

            if(vb_eos)printf("energH%12.3E%12.3E%12.3E%12.3E%12.3E\n",
                   nhi,nhii,nhei,nheii,nheiii);

        }
    } // F : 21

    /*
     * molecular hydrogen
     */
    nh2 = 0.0;
    if(vb_eos)printf("energHa%12.3E%12.3E\n",nhi,t);

    if(nhi >= 0.001*nh && t <= 20000.0)
    {

        fac = 28.0925 - teta * ( 4.92516 - teta * ( 0.056191 + teta * 0.0032688 ) ) - logt;
        if(t > 12000.0) fac += ( t - 12000.0) / 1000.0;

        fac = exp(dm*fac);
        if(vb_eos)printf("energHb%12.3E%12.3E\n",fac,1e-20*nhi);

        if(fac < 1e-20 *nhi )
        {
            nh2 = 0.5 * nhi;
            nhi = 0.0;
            if(vb_eos)printf("energJ\n");
        }
        else
        {
            double b = fac / nhi;
            double c = b;
            double bc = 0.5 * b / c;
            double xx = 1.0 / (sqrt( Pow2(bc) + 1.0 / c ) + bc);
            nh2 = 0.5 * nhi * ( 1.0 - xx );
            nhi *= xx;
            if(vb_eos)printf("energI%12.3E\n",nhi);
        }
    }

    /*
     * calculation of energy and pressure
     * corr. for slight electron degeneracy
     */
    double tt = 1e-6 * t;
    nedgen  =  ( nmetel + ne ) *
        (1.0 + 2.19e-2 * ro / ( mu * Pow1p5(tt) ) );
    if(vb_eos)printf("energK%12.3E\n",nedgen);
    nt = nh - nh2 + nhe + nedgen + nmet;
    pg = 1.3805e-16 * nt * t;
    pr = 0.0;
    if(typ > 0.0) pr = 2.523e-15 * Pow4(t);

    /* save the pressures */
    *p = pg + pr;
    *pgas = pg;
    *prad = pr;

    uh2 = t * ( 2.1 + t * 2.5e-4);
    if(t > 3000.0) uh2 = -1890.0 + t * ( 3.36 + t * 0.4e-4 );
    *u = ( 1.5 * pg + 3.0 * pr + 1.3805e-16 * nh2 * uh2 + 3.585e-12 * nhi + 25.36e-12 * nhii +
          39.37e-12 * nheii + 126.52e-12 * nheiii ) / ro;

    nion->nH2 = nh2;
    nion->nHI = nhi;
    nion->nHII = nhii;
    nion->nHeI = nhei;
    nion->nHeII = nheii;
    nion->nHeIII = nheiii;
    nion->ne = ne;
    nion->nt = nt;



    if(vb_eos)printf("energZ%12.3E%12.3E\n",*p,*u);

}

static void fzz(double * Restrict zz,
                double * Restrict f1,
                double * Restrict f2,
                double * Restrict f3,
                const double f4,
                const double f5)
{
    /*
     * input:
     *    zz = a guess of correcting factor to the electron density (=1.0)
     *    f1,f2,f3 = ionization factors divided by electron density
     *    f4 = number density of hydrogen ions and atoms / electron number density
     *    f5 = ratio of helium to hydrogen nuclei
     *
     * output:
     *    zz = the iterated value of the correcting factor
     *    f1,f2,f3 = ionization factors divided by the corrected electron density
     */
    const double del = 1e-3;
    const double acc = 1e-5;
    const int itmax = 30;
    int iter = 0;
    int converged = 0;
    while(iter++ < itmax)
    {
        double fz = funzz(*zz,*f1,*f2,*f3,f4,f5);
        if(fabs(fz) < acc)
        {
            converged = 1;
            break;
        }
        else
        {
            double zz1 = *zz + del;
            double fz1 = funzz(zz1,*f1,*f2,*f3,f4,f5);
            double dz = del * fz / ( fz - fz1 );
            *zz += dz;
        }
    }

    if(converged)
    {
        *f1 /= *zz;
        *f2 /= *zz;
        *f3 /= *zz;
        return;
    }
    else
    {
        if(vb_eos)printf("fzz iterations do not converge\n");
        Exit_binary_c_no_stardata(BINARY_C_EQUATION_OF_STATE_FAILED,
                                  "fzz iterations do not converge in paczynski equation of state\n");
    }
}



static double Pure_function funzz(double zz,
                                  double f1,
                                  double f2,
                                  double f3,
                                  double f4,
                                  double f5)
{
    return f1/(f1+zz)+f5*f2*(zz+2.0*f3)/(Pow2(zz)+f2*(zz+f3))-zz/f4;
}







#endif // EQUATION_OF_STATE_ALGORITHMS
