#include "../binary_c.h"

No_empty_translation_unit_warning;

#ifdef EQUATION_OF_STATE_ALGORITHMS
#include "equation_of_state.h"

void equation_of_state(Equation_of_state_args_in)
{

#ifdef EQUATION_OF_STATE_ENABLE_ALGORITHM_PACZYNSKI
    if(algorithm == EQUATION_OF_STATE_PACZYNSKI)
    {
        equation_of_state_paczynski(Equation_of_state_args);
    }
    else
#endif
    {
        Exit_binary_c(BINARY_C_ALGORITHM_BRANCH_FAILURE,
                      "Equation of state algorithm %d unknown\n",
                      algorithm);
    }
}

#endif // EQUATION_OF_STATE_ALGORITHMS
