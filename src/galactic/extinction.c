#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Amores and Lepine 2004/ 2005
 * Galactic extinction calculator
 * http://www.galextin.org/modextin.html
 *
 * NOTE: There are numerous errors in this code!
 */

void extinction(double * Av,
                double * EBV)
{
/*

      program main
      dimension dis(1000),x(1000),y(1000),yy(1000),r(1000),
     c     z(1000),zco(1000),zh(1000)
      dimension ah1(1000),aco(1000),zmet(1000),agas(1000),
     c     agastot(1000)
*/


    // RGI : why are the arrays this size?
    double dis[1000],x[1000],y[1000],yy[1000],r[1000],z[1000],zco[1000],zh[1000];
    double ah1[1000],aco[1000],zmet[1000],agas[1000],agastot[1000]={0.0};

    /*

C program computes extinction between Sun and a point with given galactic coordinates
C and distance
    1  r0=7.5
C  adopted distance of the Galactic center
      conv=3.14159/180.
      step  = 0.05
      dist = 10.0
C distance of the point to which we will calculate the extinction

    */

    double r0 = 7.5; // kpc?
    double pi180 = PI/180.0;
    double step = 0.05;

    int i,k;

/*
      print*, 'Interstellar Extinction in the Galaxy',
     c     '(Amores & Lépine - 2004)'
      print*,'Give the galactic longitude and latitude (Degrees)'
      read*, glong,glat
      print*,  'Give distance in kpc'
      read*, dist
*/

    double glong = 10.0;
    double glat = 45.0;
    double dist = 5.0;

    int nstep=(int)(dist/step)+1;
// C we added +1 to get the same results given by IDL fix


//C computes  trigonometric functions only once
    double yproj=cos(glong*pi180);
    double xproj=sin(glong*pi180);
    double bproj=sin(glat*pi180);
    double dproj=cos(glat*pi180);
    *Av = 0.0;

//C       H1 scale-height (Guilbert 1978)
    double zc = 0.02 ;


/*
C   for the integration of the colunar density
C declaring and putting values in the variables. The arrays will contain the
C  quantities like galactic radius or gas density for each step along the line-of sight
C if you work with other language you should probably define these quantities in a loop
*/

    //do i=1,nstep
    for(i=1;i<nstep;i++)
    {
        // int ipas=i; // RGI : never used?
        dis[i] = i*step;
        x[i]=dis[i]*xproj*dproj;
        y[i]=dis[i]*yproj*dproj;
        yy[i]=r0-y[i];
        r[i]=sqrt(x[i]*x[i]+yy[i]*yy[i]);
        z[i]=dis[i]*bproj;
        zco[i]=0.036*exp(0.08*r[i]);

//c       H2 scale-height
        zh[i] = zco[i]*1.8;


//C       shift takes in to account that the sun is not precisely in the galactic plane

        ah1[i]=0.7*exp(-r[i]/7.0-(Pow2(1.9/r[i])));
//C function that calculates the HI density

        aco[i] = 58.0*exp(-r[i]/1.20-(Pow2(3.50/r[i]))) +
            240.0*exp(-(Pow2(r[i])/0.095));
//C  H2 density; last term is for galactic center
    }

    //do i=1,nstep
    for(i=1;i<nstep;i++)
    {
        zmet[i]=1.0;
        if(r[i]<=1.2)  zmet[i] = 9.6;
        if((r[i]> 1.2)&&(r[i]<=9.0)) zmet[i]=sqrt(r0/r[i]);
        if(r[i]> 9.0)   zmet[i] = pow(r0/r[i],0.1);
    }
    double gam1=1.0,gam2=2.0;

//C	See the final tuning (section 4.1) correction factor for interval l=120-200

    double tune = 1.0;
    if((glong>=120.0)&&(glong<= 200.0))  tune=2.0;
    agas[1]=gam1*(ah1[1]*zmet[1]*exp(-0.5*Pow2((z[1]-zc)/zh[1]))) +
        gam2*aco[1]*exp(-0.5*Pow2((z[1]-zc)/zco[1]));
    agastot[1]=agas[1];


    for(k=2;k<nstep;k++)
    {
        agas[k]=gam1*(ah1[k]*zmet[k]*exp(-0.5*Pow2((z[k]-zc)/zh[k]))) +
            gam2*aco[k]*exp(-0.5*Pow2((z[k]-zc)/zco[k]));
        agastot[k]=agastot[k-1] + agas[k];

    }

    // RGI : but agastot[nstep] hasn't been set yet!
    *Av=agastot[nstep]*step*3.086*.57*tune;
/*
  c   agastot is the integral along line-of-sight of the gas density
  c   step is in units of kpc= 3.08*10^21 cm and
  c   the conversion factor gamma= .57 10^-21 mag cm2
*/
    double rs = 3.05;
//C	ratio between total to selective extinction
    *EBV = *Av/rs;


    //print*,'E(B-V)= ',ebv,' mag', 'Av= ', av, 'mag'
    //end
//*/
}
