#ifndef GALACTIC_PROTOTYPES_H
#define GALACTIC_PROTOTYPES_H
#ifdef GALACTIC_MODEL

double apparent_luminosity(struct star_t * star,
                           struct stardata_t * stardata);

double bolometric_magnitude(struct star_t * star);
void monte_carlo_coordinates(struct stardata_t * stardata);

#endif // GALACTIC_MODEL
#endif // GALACTIC_PROTOTYPES_H
