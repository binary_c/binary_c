#include "../binary_c.h"

No_empty_translation_unit_warning;

#ifdef GALACTIC_MODEL

static double sigma_gas(double r,
                        double t);
static double radialdensity(double *r);
static double zdensity(double *z);
double SFR(double r,double t);


/*
 * All distances are in parsecs
 */
#define SOL_DISTANCE 8500
#define DISC_SCALE_HEIGHT 300
#define GALACTIC_RADIUS 19000

void monte_carlo_coordinates(struct stardata_t * stardata)
{
    /*
     * Generate Galactic co-ordinates according to the model of choice
     */
    if(stardata->preferences->galactic_model==GALACTIC_MODEL_NELEMANS2004)
    {
        /*
         * Assign Galactic co-ordinates
         */
        double x=SOL_DISTANCE/sqrt(2.0);
        double y=x;
        double z=0;
        double r=sqrt(x*x+y*y);
        double t;
        for(t=0.0; t<15.0; t+=1.0)
        {
            double SFR_sol = SFR(SOL_DISTANCE,t);
            printf("t=%g r=%g sigma_gas = %g, SFR/SFRsol = %g (sol %g)\n",
                   t,
                   r,
                   sigma_gas(r,t),
                   SFR(r,t)/SFR_sol,
                   SFR_sol
                );
        }
    }
}

static double radialdensity(double *r)
{
    return 0.0;
}


static double zdensity(double *z)
{
    return Pow2(1.0/cosh(z[0]/DISC_SCALE_HEIGHT))    ;
}

static double sigma_gas(double r,
                        double t)
{
    /*
     * Gas density as a function of radius and time.
     * r in pc, t in Gyr
     */

    r *= 1e-3; // convert r to kpc
    r = Max(2.0,r); // only valid outside 2kpc

    /*
     * Fits to Boissier and Prantzos, 1999
     * Fig 1a
     */
    // 1   Gyr (-1.32600e-01)*r + (-1.20150e-01)*log(r) + (2.20880e+00)
    // 5   Gyr (-1.36470e-01)*r + (3.33900e-01)*log(r) + (1.62860e+00)
    // 13.5Gyr (-1.19870e-01)*r + (5.34240e-01)*log(r) + (8.73410e-01)

    /*
     * Interpolate the co-efficients
     */
    const static double table[12] = {
        1,   (-1.32600e-01), + (-1.20150e-01),  (2.20880e+00),
        5,   (-1.36470e-01), + (3.33900e-01),  (1.62860e+00),
        13.5, (-1.19870e-01), + (5.34240e-01),  (8.73410e-01)
    };
    double x[1];
    x[0]=t;
    double result[3];
    rinterpolate(table,
                 stardata->tmpstore->rinterpolate_data,
                 1,
                 3,
                 3,
                 x,
                 result,
                 FALSE);

    /*
     * Hence fit the gas density,
     * NB this is RGI's fitting function
     */
    double sigma = exp10(result[0] * r + result[1] * log(r) + result[2]);

    sigma = Min(70.0,sigma);

    return sigma;
}



double SFR(double r,double t)
{
    // Boissier & Prantzos 1999 : Eq. 2
    // r in parsec, t in Gyr
    r=Max(2e3,r); // only valid outside 2kpc

    // Nelemans bulge factor
    double f = r<3e3 ? 2.0 : 1.0;

    return 0.1 * f * pow(sigma_gas(r,t),1.5) * (SOL_DISTANCE/r);
}

#endif // GALACTIC_MODEL
