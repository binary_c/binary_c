#include "../binary_c.h"
No_empty_translation_unit_warning;


int binary_c_fclose(struct binary_c_file_t ** const fp)
{
    /*
     * Given a pointer fp to a binary_c_file_t structure pointer, close
     * the file and free the memory, and set the file structrure pointer
     * to NULL.
     *
     * Return what is returned by fclose, or:
     * -2 if the pointer to the file structure pointer is NULL
     * -1 if file or file->fp is NULL
     */
    int ret;


    if(fp!=NULL)
    {
        struct binary_c_file_t * file = *fp;
        if(file->stream)
        {
            ret = close_stream(&file->stream);
        }
        else
        {
            if(likely(file != NULL))
            {
                if(likely(file->fp != NULL))
                {
#ifdef MEMOIZE
                    if(file->memo!=NULL)
                    {
                        memoize_free(&file->memo);
                    }
#endif // MEMOIZE
                    ret = fclose(file->fp);
                }
                else
                {
                    ret = -1;
                }

                /*
                 * Free and explicitly NULL
                 */
                Safe_free(file);
                *fp = NULL;
            }
            else
            {
                ret = -1;
            }
        }
    }
    else
    {
        ret = -2;
    }

    return ret;
}
