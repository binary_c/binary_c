#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Construct a binary_c data filename
 * returning a string of the full path.
 *
 * The returned string must be freed by the caller.
 *
 * Returns NULL if nothing found.
 */

char * data_filename(struct stardata_t * const stardata Maybe_unused,
                     const char * const data_directory,
                     const char * const filename)
{
    char * pathname = NULL;
    if(data_directory == NULL)
    {
        /*
         * No directory given, use BINARY_C environment variable
         */
        char * binary_c = getenv("BINARY_C");
        if(binary_c != NULL &&
           asprintf(&pathname,
                    "%s/%s",
                    binary_c,
                    filename) >= 0
           &&
           check_file_exists(pathname) != NULL
            )
        {
            return pathname;
        }

        /*
         * No environment variable, or file not found,
         * use binary_c src directory.
         */
#ifdef BINARY_C_SRC
        if(asprintf(&pathname,
                    "%s/%s",
                    Stringize(BINARY_C_SRC),
                    filename) >= 0
           &&
           check_file_exists(pathname) != NULL
            )
        {
            return pathname;
        }
#endif // BINARY_C_SRC

    }
    else
    {
        /*
         * Look in the given directory
         */
        if(asprintf(&pathname,
                    "%s/%s",
                    data_directory,
                    filename) >= 0
           &&
           check_file_exists(pathname) != NULL
            )
        {
            return pathname;
        }
    }
    return NULL;
}
