#include "../binary_c.h"
No_empty_translation_unit_warning;


char * file_splitline(struct stardata_t * const stardata,
                      FILE * const Restrict fp,
                      char ** line,
                      struct string_array_t * const string_array,
                      const char comment_char,
                      char delimiter,
                      const size_t prealloc,
                      const size_t max)
{
    /*
     * Read the first line of fp, ignoring any leading comment_char
     * if it is non-zero.
     *
     * Returns a single string containing the line data, and
     * sets a string_array_t containing (copies of) the split strings.
     *
     * array points to an array of new character substrings,
     * with narray elements.
     *
     * You should set *array to be NULL or be pre-allocated with
     * prealloc members (each of size sizeof(char**)) prior to calling
     * this function.
     *
     * *array will need freeing when you are done with the memory.
     *
     * On error, *narray is set to -1 and **array is undefined.
     *
     * prealloc is passed to string_split: it tells string_split
     * to pre-allocate to *array. We also do not tell string_split
     * to shrink the final *array.
     *
     * max is the maximum number of strings to extract
     *
     * Note:
     */

    /*
     * get data line
     */
    char * nextline = NULL;
    size_t n = 0;
    const ssize_t x = getline(&nextline,
                             &n,
                             fp);
    if(x > 0)
    {
        /*
         * Remove trailing newline
         */
        chomp(nextline);

        /*
         * Check for comment character
         */
        Boolean found_comment;
        if(comment_char != 0 &&
           nextline[0] == comment_char)
        {
            found_comment = TRUE;
            nextline++;
        }
        else
        {
            found_comment = FALSE;
        }

        /*
         * Remove the strings in the string array,
         * which sets string_array->n=0, but do not
         * free its list of pointers
         */
        free_string_array_contents(string_array);

        /*
         * Split as required, and do not shrink the RAM
         * (pre)allocated
         */
        string_split_preserve(stardata,
                              nextline,
                              string_array,
                              delimiter,
                              Max((size_t)string_array->n,
                                  prealloc),
                              max,
                              FALSE);

        if(nextline != NULL &&
           found_comment)
        {
            nextline--;
        }
    }
    else
    {
        string_array->n = -1;
    }

    *line = string_array->n == -1 ? NULL : nextline;
    return nextline;
}
