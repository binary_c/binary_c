#include "../binary_c.h"
No_empty_translation_unit_warning;


char * binary_c_getline(struct binary_c_file_t * const fp)
{
    char * lineptr = NULL;
    size_t n = 0;
    if(getline(&lineptr,
               &n,
               fp->fp) == -1)
    {
        return NULL;
    }
    else
    {
        return lineptr;
    }
}
