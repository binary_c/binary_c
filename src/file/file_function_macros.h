#pragma once
#ifndef BINARY_C_FILE_FUNCTION_MACROS_H
#define BINARY_C_FILE_FUNCTION_MACROS_H

/*
 * Wrapper around load_and_remap_table so it can be called
 * with only four arguments
 *
 * WANTED_COLS should be a list of integers corresponding
 * to the columns required from the original table, or All_cols
 * for all of them.
 *
 * REMAP_RES should be a list of integers corresponding to the
 * new resolution of each parameter.
 *
 * If SETUP_RINTERPOLATE is TRUE then load_table automatically
 * sets up the table in rinterpolate_data.
 */
#define Load_and_remap_table(TABLE,                 \
                             FILENAME,              \
                             WANTED_COLS,           \
                             REMAP_RES,             \
                             SKIPLINES,             \
                             SETUP_RINTERPOLATE)    \
    load_and_remap_table(                           \
        stardata,                                   \
        &(TABLE),                                   \
        FILENAME,                                   \
        WANTED_COLS,                                \
        REMAP_RES,                                  \
        Array_size(WANTED_COLS),                    \
        Array_size(REMAP_RES),                      \
        (SKIPLINES),                                \
        (SETUP_RINTERPOLATE)                        \
        );

#define Load_table(TABLE,                       \
                   FILENAME,                    \
                   WANTED_COLS,                 \
                   NPARAMS,                     \
                   SKIPLINES,                   \
                   SETUP_RINTERPOLATE           \
    )                                           \
    load_table(                                 \
        stardata,                               \
        &(TABLE),                               \
        FILENAME,                               \
        WANTED_COLS,                            \
        Array_size(WANTED_COLS),                \
        (NPARAMS),                              \
        (SKIPLINES),                            \
        (SETUP_RINTERPOLATE)                    \
        );

#endif // BINARY_C_FILE_FUNCTION_MACROS_H
