#include "../binary_c.h"
No_empty_translation_unit_warning;



int Gnu_format_args(2,3) binary_c_fprintf(struct binary_c_file_t * const Restrict file,
                                          const char * const Restrict format,
                                          ...)
{
    /*
     * For a variable arg list, call vprintf
     * to do the work
     */
    va_list args;
    va_start(args,format);
    const int written = binary_c_vprintf(file,format,args);
    va_end(args);
    return written;
}
