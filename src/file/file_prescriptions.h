#pragma once
#ifndef FILE_PRESCRIPTIONS_H
#define FILE_PRESCRIPTIONS_H

#include "file_prescriptions.def"

#undef X
#define X(CODE) FILE_FILTER_##CODE,
enum { FILE_FILTERS_LIST };
#undef X



#endif // FILE_PRESCRIPTIONS_H
