int poo;
#include "file/file_macros.h"


#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef __HAVE_LIBZ__
#include <zlib.h>
#endif//__HAVE_LIBZ__

struct binary_c_file_t * binary_c_fopen(struct stardata_t * const stardata,
                                        const char * Restrict const path,
                                        const char * Restrict const mode,
                                        const size_t maxsize)
{

    /*
     * open the file at "path" with mode "mode" which
     * is "r" or "w".
     *
     * Set its maxsize, which is ignored if zero,
     * and current size (zero).
     *
     * Returns a pointer to the newly allocated file struct.
     *
     * The return value from fopen is set as file->fp
     *
     * On failure, returns NULL and frees all
     * associated memory.
     */
    struct binary_c_file_t * file =
        Malloc(sizeof(struct binary_c_file_t));
    if(file != NULL)
    {
        Dprint("stream mode %s\n",mode);
        if(mode[0] == 'r')
        {
            Dprint("fopen stream\n");
            file->stream = open_stream(stardata,
                                       path);
            file->fp = file->stream->fp;
            file->format = file->stream->format;
        }
        else
        {
            Dprint("fopen native\n");
            file->stream = NULL;
            file->fp = fopen(path,
                             mode);
            file->format = FILE_FORMAT_NATIVE;
        }


#ifdef MEMOIZE
        file->memo = NULL;
#endif // MEMOIZE
        if(file->fp)
        {
            /*
             * Opened ok, set values.
             */
            file->size = 0;
            file->maxsize = maxsize;
            strlcpy(file->path,
                    path,
                    sizeof(char) * (STRING_LENGTH - 1));
        }
        else
        {
            /*
             * Failure: free to NULL
             */
            Safe_free(file);
        }
    }
    return file;
}
