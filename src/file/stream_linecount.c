#include "../binary_c.h"
No_empty_translation_unit_warning;
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>

#undef BUFFER_SIZE
#define BUFFER_SIZE 1024*100

/*
 * File line counter.
 * Lines starting with comment_char are ignored.
 * The first skiplines are skipped and not counted.
 */
static ssize_t safe_read(const int fd,
                         void * const buf,
                         size_t count);

enum { SYS_BUFSIZE_MAX = INT_MAX >> 20 << 20 };
long int stream_linecount(struct binary_c_stream_t * const stream,
                          const char comment_char,
                          const size_t skiplines)
{
    long int nlines = 0; /* returned */
    size_t nfilelines = 0;
    const int fd = fileno(stream->fp);
#ifdef __HAVE_POSIX_FADVISE
    posix_fadvise(fd, 0, 0, POSIX_FADV_SEQUENTIAL);
#endif
    ssize_t bytes_read = 0;
    char buf[BUFFER_SIZE+1];
    Boolean checkfirst = TRUE;
    Boolean last_is_newline = FALSE;
    while((bytes_read = safe_read(fd, buf, BUFFER_SIZE)) > 0)
    {
        char * p = buf;
        const char * const end = p + bytes_read;
        const char * last = end - 1;
        last_is_newline = *last == '\n';

        if(bytes_read > 0)
        {
            if(checkfirst==TRUE &&
               *p == comment_char)
            {
                /* check first char for comment */
                nlines--;
                bytes_read--;
                p++;
            }

            if(bytes_read > 1)
            {
                /* look for newlines from *p to *end */

                /* if last char is newline, set checkfirst to true */
                checkfirst = *last == '\n' ? TRUE : FALSE;

                while((p = memchr(p, '\n', end - p)) &&
                      (p++ < end))
                {
                    if(nfilelines < skiplines)
                    {
                        /* skip this line */
                    }
                    else
                    {
                        nlines++;
                        if(p<last && *p == comment_char)
                        {
                            nlines--;
                        }
                    }
                    nfilelines++;
                }
            }
        }
    }
    if(!last_is_newline)
    {
        /*
         * Final line has no newline, but we still want
         * it to contribute to the number of lines count
         */
        nlines++;
    }
    return nlines > 0 ? nlines : 0;
}


/*
 * safe_read is based on code from GNU's coreutils
 * https://github.com/coreutils/gnulib/blob/master/lib/safe-read.c
 * and modified
 */

/* An interface to read and write that retries after interrupts.
   Copyright (C) 1993-1994, 1998, 2002-2006, 2009-2021 Free Software
   Foundation, Inc.
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#ifdef EINTR
#define IS_EINTR(x) ((x) == EINTR)
#else
#define IS_EINTR(x) 0
#endif

ssize_t safe_read(const int fd,
                  void * const buf,
                  size_t count)
{
    Loop_forever
    {
        const ssize_t result = read(fd, buf, count);
        if(result >= 0)
        {
            return result;
        }
        else if(IS_EINTR(errno))
        {
            continue;
        }
        else if(errno == EINVAL &&
                SYS_BUFSIZE_MAX < count)
        {
            count = SYS_BUFSIZE_MAX;
        }
        else
        {
            /* some other error */
            return result;
        }
    }
}
