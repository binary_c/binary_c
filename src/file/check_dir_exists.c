#include "../binary_c.h"
No_empty_translation_unit_warning;

#include <unistd.h>
#include <sys/stat.h>

Boolean check_dir_exists(char * const Restrict path)
{
    /*
     * Check that a directory exists at given path.
     *
     * Returns TRUE if it does, FALSE otherwise.
     */
    struct stat stats;

    if(stat(path, &stats) == 0 &&
       S_ISDIR(stats.st_mode))
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}
