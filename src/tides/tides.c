#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "tides.h"

/*
 * This function calculates the derivatives of
 * stellar and orbital angular momentum, as well
 * as orbital eccentricity, because of tidal interactions
 * caused by "companion" and disspated in "star".
 *
 * Note: radius (passed in) can be smaller than the formal
 * stellar radius because it's the radius of the star when there
 * is no mass transfer, or the Roche lobe when there is.
 *
 * Normally, these two should be very similar, i.e. R~RL.
 */

void tides(struct stardata_t * const stardata,
           const Boolean RLOF_boolean,
           const double radius,
           struct star_t * const star,
           struct star_t * const companion)
{
    double f;
    if(Fequal(stardata->preferences->tidal_strength_factor,
              TIDAL_STRENGTH_VARIABLE))
    {
        if(stardata->tmpstore->tidal_multipliers == NULL)
        {
            tides_load_variables(stardata);
        }
        const double x[2] = {
            log10(Teff_from_star_struct(star)),
            log10(star->luminosity)
        };
        double r[1];
        Interpolate(stardata->tmpstore->tidal_multipliers,
                    x,
                    r,
                    0);
        f = exp10(r[0]);
        Dprint("Interpolate tidal strength multiplier %g %g -> %g\n",x[0],x[1],f);
    }
    else
    {
        f = stardata->preferences->tidal_strength_factor;
    }

    tides_generic(stardata,
                  star,
                  &stardata->common.orbit,
                  companion->mass,
                  radius,
                  f,
                  RLOF_boolean,
                  &(star->derivative[DERIVATIVE_STELLAR_ANGULAR_VELOCITY_TIDES]),
                  &(star->derivative[DERIVATIVE_STELLAR_ANGMOM_TIDES]),
                  &(stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM_TIDES]),
                  &(star->derivative[DERIVATIVE_STELLAR_ECCENTRICITY]),
                  &(stardata->model.derivative[DERIVATIVE_ORBIT_ECCENTRICITY_TIDES]),
                  FALSE);

    const double tspin =
        Is_zero(star->derivative[DERIVATIVE_STELLAR_ANGULAR_VELOCITY_TIDES]) ?
        1e100 :
        Min(1e100,
            fabs(star->omega/star->derivative[DERIVATIVE_STELLAR_ANGULAR_VELOCITY_TIDES]));

    star->tide_spin_lock =
        Boolean_(tspin < stardata->preferences->minimum_timestep);
}
