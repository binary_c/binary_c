#ifndef TIDES_H
#define TIDES_H

/*
 * all stars can have tides, but black holes and
 * massless remnants have no size, so cannot
 */

enum {
    TIDAL_STRENGTH_VARIABLE = -1,
};

#ifdef PRE_MAIN_SEQUENCE

/* Boolean to define a convective pre main sequence star */
#define CONVECTIVE_PMS                              \
    (stardata->preferences->pre_main_sequence &&    \
     1e6 * star->age < preMS_lifetime(star->mass))

/* stars with M>1.25 are 'radiative' if they are not PreMS stars */
#define RADIATIVE_MAIN_SEQUENCE                 \
    (star->stellar_type==MAIN_SEQUENCE          \
     && !CONVECTIVE_PMS                         \
     && More_or_equal(star->mass,1.25))         \

#else

/* standard definition */
#define RADIATIVE_MAIN_SEQUENCE                 \
    (star->stellar_type==MAIN_SEQUENCE          \
     && More_or_equal(star->mass,1.25))         \

#endif // PRE_MAIN_SEQUENCE


#define USE_RADIATIVE_DAMPING                   \
    (RADIATIVE_MAIN_SEQUENCE ||                 \
     (star->stellar_type==CHeB)||               \
     (star->stellar_type==HeMS))

#define USE_CONVECTIVE_DAMPING (star->stellar_type<HeWD)

#endif // TIDES_H
