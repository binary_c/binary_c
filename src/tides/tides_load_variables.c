#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "tides.h"

void tides_load_variables(struct stardata_t * const stardata)
{
    /*
     * Load the data table for tidal multipliers
     */
    if(stardata->preferences->tides_variable_filename[0] == '\0')
    {
        Exit_binary_c(BINARY_C_FILE_OPEN_ERROR,
                      "tides_variable_filename is NULL and must be set if you want to load variable tide data.");
    }
    size_t lines;
    size_t length;
    size_t maxwidth;
    stardata->tmpstore->tidal_multipliers =
        Malloc(sizeof(struct data_table_t));
    stardata->tmpstore->tidal_multipliers->data =
        load_data_as_doubles(stardata,
                  stardata->preferences->tides_variable_filename,
                  &lines,
                  &length,
                  &maxwidth,
                  ' ',
                  0);

    if(stardata->tmpstore->tidal_multipliers->data == NULL)
    {
        Exit_binary_c(BINARY_C_FILE_OPEN_ERROR,
                      "Failed to load data from \"%s\" please check the file exists and contains a tides data table.",
                      stardata->preferences->tides_variable_filename);
    }

    stardata->tmpstore->tidal_multipliers->nlines = lines;
    stardata->tmpstore->tidal_multipliers->nparam = 2;
    stardata->tmpstore->tidal_multipliers->ndata = 1;

    if(maxwidth != 3)
    {
        Exit_binary_c(BINARY_C_ALGORITHM_OUT_OF_RANGE,
                      "Maxwidth of the tides table should be 3 but it is %zu. Please fix this!\n",
                      (size_t)maxwidth);
    }
}
