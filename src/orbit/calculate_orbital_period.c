#include "../binary_c.h"
No_empty_translation_unit_warning;


double calculate_orbital_period (struct stardata_t * Restrict const stardata)
{
    /*
     * Calculate the orbital period using Kepler's law
     * return its length in days
     */
    const double per = sqrt(Pow3(stardata->common.orbit.separation/
             (double)AU_IN_SOLAR_RADII)/(Total_mass))
        *YEAR_LENGTH_IN_DAYS;

    Dprint("Calculating orbital period: %g (M1+M2=%g a=%g)\n",
           per,
           Total_mass,
           stardata->common.orbit.separation);

    return per;
}
