#pragma once
#ifndef ORBIT_PROTOTYPES_H
#define ORBIT_PROTOTYPES_H

void update_orbital_period_and_angular_frequency(struct orbit_t * Restrict const orbit,
                                                 struct star_t * Restrict const star1,
                                                 struct star_t * Restrict const star2);

void update_orbital_variables(struct stardata_t * Restrict const stardata,
                              struct orbit_t * Restrict const orbit,
                              struct star_t * Restrict const star1,
                              struct star_t * Restrict const star2);

double orbital_angular_momentum(struct stardata_t * const stardata);
void set_separation(struct stardata_t * const stardata,
                    const double separation);

double calculate_orbital_separation(struct stardata_t * Restrict const stardata);
double calculate_orbital_period(struct stardata_t * Restrict const stardata);
double orbital_energy(struct stardata_t * Restrict const stardata,
                      struct orbit_t * Restrict const orbit);
double orbital_angular_momentum_from_orbit(const struct orbit_t * const orbit,
                                           const double m1,
                                           const double m2);
void sudden_orbital_change(struct stardata_t * const stardata,
                           struct orbit_t * orbit_in,
                           struct orbit_t * const orbit_out,
                           const double dm1,
                           const double dm2,
                           const double phase_angle,
                           const double mean_anomaly,
                           const double eccentric_anomaly);

double eccentric_anomaly_from_mean_anomaly(struct stardata_t * const stardata,
                                           const double eccentricity,
                                           const double mean_anomaly,
                                           const double f,
                                           const double tol,
                                           const int max_iterations);

double Antonov_tidal_limit(struct stardata_t * const stardata);

double Pure_function orbital_velocity_squared(struct stardata_t * Restrict const stardata);
double Pure_function orbital_velocity_squared_cgs(struct stardata_t * Restrict const stardata);
double Pure_function orbital_velocity(struct stardata_t * Restrict const stardata);
double Pure_function orbital_velocity_cgs(struct stardata_t * Restrict const stardata);
double minimum_stable_orbit_Mardling_Aarseth_2001(const double eccentricity,
                                                  const double inclination,
                                                  const double q);
#endif//ORBIT_PROTOTYPES_H
