#include "../binary_c.h"
No_empty_translation_unit_warning;


double orbital_energy(struct stardata_t * Restrict const stardata,
                      struct orbit_t * Restrict const orbit_in)
{

    /*
     * return the orbital energy in cgs
     *
     * NB if orbit is NULL, use stardata's orbit
     */
    return
        - GRAVITATIONAL_CONSTANT * M_SUN * M_SUN *
        stardata->star[0].mass * stardata->star[1].mass
        /
        (2.0 * Max3(stardata->star[0].radius,
                    stardata->star[1].radius,
                    orbit_in != NULL ? orbit_in->separation : stardata->common.orbit.separation) * R_SUN);

}
