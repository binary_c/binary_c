#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * The binary_c stellar population nucleosynthesis framework.
 *
 * Copyright Robert Izzard 2000-2025
 *
 * Contact rob.izzard@gmail.com
 *
 * Code repository:
 * https://gitlab.com/binary_c
 *
 * Documentation is in doc/
 * Code is in src/
 *
 * To build, run
 *
 * ---
 *
 * meson setup builddir
 * cd builddir
 * ninja binary_c_install
 *
 * ---
 *
 * Please see the files README, LICENCE and CHANGES,
 * and the doc/ directory for documentation which is mirrored
 * at https://binary_c.gitlab.io
 *
 **********************
 *
 * Subroutine:
 *     tidal_limit
 *
 * Purpose:
 *     Calculates the Antonov (1972) tidal limit for the current system.
 *
 * Arguments:
 *     stardata - the binary system
 *
 * Returns:
 *     the semi-major axis of system at which Galactic tidal perturbations
 *     exceed the mutual gravitational attraction of the stars,
 *     in Solar radii.
 *
 **********************
 */

double Antonov_tidal_limit(struct stardata_t * const stardata)
{
    return 2.07e5 * ASTRONOMICAL_UNIT * cbrt(Sum_of_stars(mass));
}
