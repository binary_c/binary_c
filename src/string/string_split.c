#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Function to split a string on a given *single
 * character* delimiter and set the array with
 * (pointers to these) substrings.
 *
 * Return the number of strings in array.
 *
 * Note that if no strings are found, *array is NULL.
 *
 * prealloc is the size we first allocated to *array, if you
 * know nothing, set this to 0. If you think you know how long
 * the array will be, set the number in prealloc.
 *
 * You can even use prealloc to maintain the size of *array passed
 * in, if it's alloc'd once, and the alloc size never changes,
 * you never have to make it again.
 *
 * Because we use strtok_r, the string passed in is modified.
 * If you do not want to modify the string, use string_split_preserve()
 * instead.
 */


#define STRING_SPLIT_MULTIPLIER 2

/*
 * You can use any function that returns the appropriate char*
 * at each delimiter, e.g.:
 *
 * strtok_r((char*const Restrict)STRING,delimiter_string,&saveptr)
 *     [ from the (usually GNU) C library ]
 *
 * my_strtok_r((char*const Restrict)STRING,delimiter,&saveptr)
 *     [ my version, which is faster for a single, const delimter char ]
 *
 * my_strtok_r_longword((char*const Restrict)STRING,delimiter_string,&saveptr)
 *     [ based on https://code.woboq.org/userspace/glibc/string/memchr.c.html ]
 *
 * strsep(STRING,delimiter_string)
 *     [ strsep from the (usually GNU) C library ]
 *
 * The fastest seems to be my_strtok_r:
 *
 * Runtimes on 5000000 long string:
 *
 * strtok_r 32.76s
 * my_strtok_r 9.80s
 * my_strtok_r with reused memory 5.19s
 * my_strtok_r with strchr 17.12s
 * my_strtok_r_longword 13.00s
 * strsep 21.06s
 *
 * If you use strtok_r or strsep, you'll need to undefine
 * the delimiter_string definition
 */
#define String_split_strtok1 my_strtok_r((char*)string,delimiter,&saveptr)
#define String_split_strtok2 my_strtok_r(NULL,delimiter,&saveptr)
//#define String_split_strtok1 strtok_r(string,delimiter_string,saveptr)
//#define String_split_strtok2 strtok_r(NULL,delimiter_string,saveptr)
//#define String_split_strtok1 my_strtok_r_longword(string,delimiter,saveptr)
//#define String_split_strtok2 my_strtok_r_longword(NULL,delimiter,saveptr)
//#define String_split_strtok1 strsep((char**)&string,delimiter_string)
//#define String_split_strtok2 strsep((char**)&string,delimiter_string)


Nonnull_some_arguments(3)
static inline char * my_strtok_r(char * string,
                                 const char delimiter,
                                 char ** const saveptr);
Nonnull_some_arguments(3)
static char * my_strtok_r_longword(char * string,
                                   const char delimiter,
                                   char ** const saveptr);

void string_split(struct stardata_t * const stardata,
                  const char * const Restrict string,
                  struct string_array_t * string_array,
                  const char delimiter,
                  const size_t prealloc,
                  const size_t max,
                  const Boolean shrink)
{
    /*
     * We realloc by doubling the allocation when required,
     * then trim the array at the end. This is generally
     * faster than calling realloc for every string.
     *
     * prealloc is the memory allocated in the first call
     * to realloc: you can use this to allocate extra memory
     * thus saving on realloc calls.
     *
     * If shrink is TRUE, we then shrink the allocated
     * array.
     *
     * max is the maximum number of strings to be split.
     * Ignored if 0.
     *
     * Returns the number of items that are split,
     * or 0 on error (e.g. realloc failure).
     */

    /*
     * First, clean out any existing strings
     */
    free_string_array_contents(string_array);

    /*
     * Increase size of list of strings if required.
     * We can just use Recalloc calls here because there's
     * currently nothing in the strings and lengths arrays.
     */
    if(prealloc > string_array->nalloc)
    {
        string_array->strings = Recalloc(string_array->strings,
                                         sizeof(char**) * prealloc);
        string_array->lengths = Recalloc(string_array->lengths,
                                         sizeof(size_t) * prealloc);
        if(unlikely(string_array->strings == NULL) ||
           unlikely(string_array->lengths == NULL))
        {
            string_array->n = -1;
            string_array->nalloc = 0;
            return;
        }
        else
        {
            string_array->nalloc = prealloc;
        }
    }

    size_t s = prealloc;  /* currently allocated size */
    char * saveptr = NULL;
    size_t count = 0;


    for(char * c = String_split_strtok1;
        c != NULL && (max==0 || count < max);
        c = String_split_strtok2)
    {
        if(count + 1 > s)
        {
            s = s==0 ? 1 : (s * STRING_SPLIT_MULTIPLIER);
            string_array->strings = Realloc(string_array->strings,
                                            sizeof(char**) * s);
            string_array->lengths = Realloc(string_array->lengths,
                                            sizeof(size_t) * s);

            if(unlikely(string_array->strings == NULL) ||
               unlikely(string_array->lengths == NULL))
            {
                /* error : realloc failed */
                string_array->n = -1;
                string_array->nalloc = 0;
                return;
            }
            else
            {
                string_array->nalloc = s;
            }
        }
        string_array->strings[count] = strduplen(c,
                                                 &string_array->lengths[count]);
       count++;
    }
    string_array->n = (ssize_t)count;

    if(0 && shrink == TRUE)
    {
        /* shrink array to only used size */
        if(string_array->nalloc > (size_t)string_array->n)
        {
            string_array->strings = Realloc(string_array->strings,
                                            sizeof(char**) * string_array->n);
            string_array->lengths = Realloc(string_array->lengths,
                                            sizeof(size_t) * string_array->n);
            if(unlikely(string_array->strings == NULL) ||
               unlikely(string_array->lengths == NULL))
            {
                string_array->n = -1;
                string_array->nalloc = 0;
                return;
            }
        }
        string_array->nalloc = string_array->n;
    }
}


/*
 * A version of strtok_r which uses a constant single
 * character delimiter, so should be faster. We hope!
 */

Nonnull_some_arguments(3)
static inline char * my_strtok_r(char * string,
                                 const char delimiter,
                                 char ** const saveptr)
{
#ifdef _TESTING
    // use normal strtok_r ?
    char * d;
    int x = asprintf(&d,"%c",delimiter);
    x*=2;
    char * const p = strtok_r(string,d,saveptr);
    Safe_free(d);
    return p;
#endif //_TESTING

    if(string == NULL)
    {
        char * const p = *saveptr;
        if(p == NULL ||
           (char)*p == '\0')
        {
            return NULL;
        }
        string = p;
    }

    /*
     * Skip leading matches, return NULL if we
     * find only leading matches.
     */
    while(unlikely(*string == delimiter)
          && *(string++) != '\0')
    {
        if(*string == '\0')
        {
            *saveptr = NULL;
            return NULL;
        }
    }

    /*
     * save the last matched position, to be returned
     */
    char * const lastmatch = string;

    /*
     * Scan for next match
     */
    Loop_forever
    {
        /*
         * Get the next character. We save this because
         * it's very likely to be compared twice, assuming
         * most characeters are NOT delimiters.
         *
         * We do the delimiter check first, as there are
         * likely >1 delimiters, but only ever one NULL
         * character at the end of the string.
         */

        if(unlikely(*string == delimiter))
        {
            /*
             * set NULL in the original string and increment
             */
            *string = '\0';

            /* set the saveptr so we know where to start next time */
            *saveptr = string + 1;

            /* return the last match location */
            return lastmatch;
        }
        else if(unlikely(*string == '\0'))
        {
            *saveptr = NULL;
            return lastmatch;
        }
        string++;
    }
}


Maybe_unused Nonnull_some_arguments(3)
static char * my_strtok_r_longword(char * string,
                                   const char delimiter,
                                   char ** const saveptr)

{
    if(string == NULL)
    {
        char * const p = *saveptr;
        if(p == NULL ||
           (char)*p == '\0')
        {
            return NULL;
        }
        string = p;
    }

    /*
     * Skip leading matches, return NULL if we
     * find only leading matches.
     */
    while(*string == delimiter &&
          *(string++) != '\0')
    {
        if(*string == '\0')
        {
            *saveptr = NULL;
            return NULL;
        }
    }

    /*
     * save the last matched position, to be returned
     */
    char * const lastmatch = string;

    typedef unsigned long int longword;
    longword repeated_one;
    longword repeated_c;
    unsigned char c = (unsigned char) delimiter;
    unsigned char *char_ptr;
    longword *longword_ptr;

    size_t n = 100;

    /* Handle the first few bytes by reading one byte at a time.
       Do this until CHAR_PTR is aligned on a longword boundary.  */
    for (char_ptr = (unsigned char *) string;
         n > 0 && (size_t) char_ptr % sizeof (longword) != 0;
         --n, ++char_ptr)
    {
        if(*char_ptr == c)
        {
            *char_ptr = '\0';
            *saveptr = (char *) (char_ptr + 1);
            return lastmatch;
        }
    }

    longword_ptr = (longword *) char_ptr;
    repeated_one = 0x01010101;
    repeated_c = c | (c << 8);
    repeated_c |= repeated_c << 16;
    if(0xffffffffU < (longword) -1)
    {
        repeated_one |= repeated_one << 31 << 1;
        repeated_c |= repeated_c << 31 << 1;
        if(8 < sizeof (longword))
        {
            size_t i;
            for(i = 64; i < sizeof (longword) * 8; i *= 2)
            {
                repeated_one |= repeated_one << i;
                repeated_c |= repeated_c << i;
            }
        }
    }

    while (n >= sizeof (longword))
    {
        longword longword1 = *longword_ptr ^ repeated_c;
        if((((longword1 - repeated_one) & ~longword1)
             & (repeated_one << 7)) != 0)
        {
            break;
        }
        longword_ptr++;
        n -= sizeof (longword);
    }

    char_ptr = (unsigned char *) longword_ptr;
    for (; n > 0; --n, ++char_ptr)
    {
        if(*char_ptr == c)
        {
            *char_ptr = '\0';
            *saveptr = (char *) (char_ptr+1);
            return lastmatch;
        }
    }

    *saveptr = NULL;
    return NULL;


    /*
     * Scan for next match
     */
    Loop_forever
    {
        /*
         * Get the next character. We save this because
         * it's very likely to be compared twice, assuming
         * most characeters are NOT delimiters.
         *
         * We do the delimiter check first, as there are
         * likely >1 delimiters, but only ever one NULL
         * character at the end of the string.
         */
        const char this = *(string++);

        if(unlikely(this == delimiter))
        {
            /*
             * set NULL in the original string and increment
             */
            *((char*)(string-1)) = '\0';

            /* set the saveptr so we know where to start next time */
            *saveptr = string;

            /* return the last match location */
            return lastmatch;
        }
        else if(unlikely(this == '\0'))
        {
            *saveptr = NULL;
            return lastmatch;
        }
    }
}
