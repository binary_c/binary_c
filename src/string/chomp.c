#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Function to remove a newline at the end of a string
 * like Perl's "chomp".
 *
 * Note: this could be \n or \r, we find the first of
 * these and just terminate the string there.
 */
void chomp(const char * const s)
{
    char *p;
    static const char remove[] = "\n\r";
    while (NULL != s &&
           NULL != (p = strpbrk(s, remove)))
    {
        *p = '\0';
    }
}
