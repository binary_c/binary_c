#include <errno.h>
#include "../binary_c.h"
No_empty_translation_unit_warning;

char * escape_string(struct stardata_t * const stardata,
                     const char * const s)
{
    /*
     * Escape the string passed in so, e.g., it can be passed
     * as a shell command.
     *
     * Returns the string, which you should free.
     */
    const size_t len = strlen(s)+1; // including NULL
    const char * const end = s + len;
    char * escaped_string = Malloc(sizeof(char)*len*2);
    char * original = (char *) s;
    {
        char * p = escaped_string;
        while(original < end)
        {
            const char this_char = *(original++);
            switch (this_char)
            {
            case '\"':
            case '\'':
            case '\\':
                *(p++) = '\\';
                *(p++) = this_char;
                break;
            case '\a':
                *(p++) = '\\';
                *(p++) = 'a';
                break;
            case '\b':
                *(p++) = '\\';
                *(p++) = 'b';
                break;
            case '\f':
                *(p++) = '\\';
                *(p++) = 'f';
                break;
            case '\n':
                *(p++) = '\\';
                *(p++) = 'n';
                break;
            case '\r':
                *(p++) = '\\';
                *(p++) = 'r';
                break;
            case '\t':
                *(p++) = '\\';
                *(p++) = 't';
                break;
            case '\v':
                *(p++) = '\\';
                *(p++) = 'v';
                break;
            default:
                *(p++) = this_char;
            }
        }

        /* end the string */
        *(p) = '\0';
    }

    escaped_string = Realloc(escaped_string,
                             (1+strlen(escaped_string)) * sizeof(char));

    return escaped_string;
}
