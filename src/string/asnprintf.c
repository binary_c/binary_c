#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Like asprintf for a limited number, size-1, of characters.
 */

int Gnu_format_args(3,4) asnprintf(
    char ** Restrict const string_pointer,
    const size_t size,
    const char * Restrict const format,
    ...)
{
    va_list args;
    va_start(args,format);
    int ret;
    ret = vasnprintf(string_pointer,
                     size,
                     format,
                     args);
    va_end(args);
    return ret;
}
