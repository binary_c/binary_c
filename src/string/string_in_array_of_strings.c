#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Check if string s is in the array
 * of strings a of length n.
 * If so, return TRUE, if not, FALSE.
 */

Boolean string_in_array_of_strings(
    const char * const Restrict s,
    const char ** a,
    const size_t n)
{
    for(size_t i=0; i<n; i++)
    {
        if(Strings_equal(s,a[i]))
        {
            return TRUE;
        }
    }
    return FALSE;
}
