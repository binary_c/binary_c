#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Function to strip a trailing character (c) from a *string.
 *
 * Does not change the amount of memory located at *string
 *
 * c should not be \0 (NULL char)
 */
void strip_trailing(char * const string,
                    const char c)
{
    if(string != NULL)
    {
        size_t len;
        while ((len = strlen(string)) &&
               string[strlen(string)-1] == c)
        {
            string[strlen(string)-1] = '\0';
        }
    }
}
