#include "../binary_c.h"
No_empty_translation_unit_warning;

Boolean string_in_string_array(
    const char * const Restrict s,
    const struct string_array_t * const a)
{
    /*
     * Wrapper for string_in_array_of_strings
     * to act on a binary_c string_array
     */
    return string_in_array_of_strings(
        s,
        (const char ** const)a->strings,
        (const size_t)a->n
        );
}
