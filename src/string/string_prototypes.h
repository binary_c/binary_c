#pragma once
#ifndef STRING_PROTOTYPES_H
#define STRING_PROTOTYPES_H
#include "../binary_c_code_options.h"

void chomp(const char * const s);


struct double_array_t * string_array_to_double_array(struct string_array_t * Restrict string_array,
                                                     double fallback);
struct int_array_t * string_array_to_int_array(struct string_array_t * Restrict string_array);
struct long_int_array_t * string_array_to_long_int_array(struct string_array_t * Restrict string_array);
struct unsigned_int_array_t * string_array_to_unsigned_int_array(struct string_array_t * Restrict string_array);

#ifdef __STRLCAT_IS_BSD_COPY__
size_t __local_bsd_strlcat(char *dst,
                           const char *src,
                           size_t dsize);
#endif
Boolean string_in_array_of_strings(
    const char * const Restrict s,
    const char ** const a,
    const size_t n);
Boolean string_in_string_array(
    const char * const Restrict s,
    const struct string_array_t * const a);

Boolean string_arrays_equal_n(struct string_array_t * const a,
                              struct string_array_t * const b,
                              const size_t n);
Boolean string_arrays_equal(struct string_array_t * const a,
                            struct string_array_t * const b);
void string_split(struct stardata_t * const stardata,
                  const char * const Restrict string,
                  struct string_array_t * string_array,
                  const char delimiter,
                  const size_t prealloc,
                  const size_t max,
                  const Boolean shrink);
void string_split_preserve(struct stardata_t * const stardata,
                           const char * const Restrict string,
                           struct string_array_t * string_array,
                           const char delimiter,
                           const size_t prealloc,
                           const size_t max,
                           const Boolean shrink);

int strtoi(const char * const Restrict s, char **endptr, const int base);
short int strtoshort(const char * const Restrict s, char **endptr, const int base);
signed char strtoschar(const char * const Restrict s, char **endptr, const int base);
int16_t strtoint16(const char * const Restrict s, char **endptr, const int base);

void int_range_from_string(struct stardata_t * const stardata,
                           const char * const Restrict string,
                           int * const low,
                           int * const high);

void free_in_string_array(char ** const array,
                          const size_t n);
void free_string_array(struct string_array_t ** const string_array);
void free_string_array_contents(struct string_array_t * const string_array);

void free_array_of_string_arrays(struct string_array_t *** const array_of_string_arrays,
                                 const size_t n);

Boolean string_replace(char ** string,
                       char * match,
                       char * replacement);

char * string_toupper(const char * const s);
char * string_tolower(const char * const s);

char * fast_double_parser(const char *p,
                          double * const outDouble);
char * fast_double_parser_with_len(const char * p,
                                   const size_t len,
                                   double * const outDouble);
double fast_strtod(const char * nptr,
                   char ** endptr);
void strip_trailing(char * const string,
                    const char c);

struct string_array_t * new_string_array(const ssize_t n);

Boolean char_pointer_arrays_equal(char * Restrict const * Restrict const a,
                                  char * Restrict const * Restrict const b,
                                  const size_t n);
char * escape_string(struct stardata_t * const stardata,
                     const char * const s);
Nonnull_all_arguments
char * strduplen(const char * const s,
                 size_t * const len);

int binary_c_asprintf(char ** Restrict const string_pointer,
                      const char * Restrict const format,
                      ...) Gnu_format_args(2,3);

int vasnprintf(
    char ** Restrict const string_pointer,
    const size_t size,
    const char * Restrict const format,
    va_list args);

int Gnu_format_args(2,3) reasprintf(
    char ** Restrict const string_pointer,
    const char * Restrict const format,
    ...);

int Gnu_format_args(3,4) reasnprintf(
    char ** Restrict const string_pointer,
    const size_t size,
    const char * Restrict const format,
    ...);

int Gnu_format_args(3,4) asnprintf(
    char ** Restrict const string_pointer,
    const size_t size,
    const char * Restrict const format,
    ...);

/*
 * Character is integer tests
 */
Pure_function Boolean is_integer(const unsigned char c);
Pure_function Boolean is_integer2(const unsigned char c);
Pure_function Boolean is_integer3(const unsigned char c);
Pure_function Boolean is_integer_or(const unsigned char c);
Pure_function Boolean is_integer_table(const unsigned char c);

int fprint_string_array(FILE * const fp,
                        struct string_array_t * Restrict const string_array,
                        const char * const delimiter);
int print_string_array(struct string_array_t * Restrict const string_array);

#endif // STRING_PROTOTYPES_H
