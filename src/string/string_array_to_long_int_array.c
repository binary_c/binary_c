#include "../binary_c.h"
No_empty_translation_unit_warning;

struct long_int_array_t * string_array_to_long_int_array(struct string_array_t * Restrict string_array)
{
    /*
     * Convert string_array to long_int_array
     * using strtol.
     */
    struct long_int_array_t * const long_int_array =
        new_long_int_array(string_array->n);

    for(ssize_t i=0; i<string_array->n; i++)
    {
        long_int_array->long_ints[i] =
            strtol(string_array->strings[i],
                   NULL,
                   10);
    }
    long_int_array->n = string_array->n;
    return long_int_array;
}
