#include "../binary_c.h"
No_empty_translation_unit_warning;

struct double_array_t * string_array_to_double_array(struct string_array_t * Restrict string_array,
                                                     const double fallback)
{
    /*
     * Convert string_array to double_array,
     * and where conversion is impossible set the value
     * to fallback (usually you'd set this to NAN).
     */
    struct double_array_t * const double_array = new_double_array(string_array->n);

    for(ssize_t i=0; i<string_array->n; i++)
    {
        /*
         * Using only the strtod function from C's stdlib
         */
        //double_array->doubles[i] = strtod(string_array->strings[i],NULL);

        /*
         * Or our fast double parser, which is generally quicker.
         * Note that we have to set the return value to avoid
         * compiler warnings: this will be optimized out anyway
         * so don't worry too much.
         *
         * You could also use fast_strtod if you want to set the
         * value directly, but that's another layer of function
         * calls to slow things down.
2         */
        const char * _ Maybe_unused = fast_double_parser_with_len(
            string_array->strings[i],
            string_array->lengths[i],
            double_array->doubles + i
            );
        if(_ == NULL)
        {
            double_array->doubles[i] = fallback;
        }
    }
    double_array->n = string_array->n;
    return double_array;
}
