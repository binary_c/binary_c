#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Like asnprintf for an existing string_pointer:
 * frees memory appropriately to prevent
 * leaks.
 */

int Gnu_format_args(3,4) reasnprintf(
    char ** Restrict const string_pointer,
    const size_t size,
    const char * Restrict const format,
    ...)
{
    va_list args;
    va_start(args,format);
    int ret;
    if(string_pointer != NULL)
    {
        /*
         * Save the old pointer so we can free it
         */
        char * newstring = NULL;

        /*
         * Allocate new space for the string and set it
         * with vasprintf
         */
        ret = vasnprintf(&newstring,
                         size,
                         format,
                         args);

        /*
         * Free the old string
         */
        if(newstring != *string_pointer)
        {
            Safe_free(*string_pointer);
        }

        /*
         * Set the newstring in its place
         */
        *string_pointer = newstring;
    }
    else
    {
        ret = -1;
    }
    va_end(args);
    return ret;
}
