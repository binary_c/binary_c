#include "../binary_c.h"
No_empty_translation_unit_warning;

struct int_array_t * string_array_to_int_array(struct string_array_t * Restrict string_array)
{
    /*
     * Convert string_array to int_array
     * using strtoi.
     */
    struct int_array_t * const int_array =
        new_int_array(string_array->n);

    for(ssize_t i=0; i<string_array->n; i++)
    {
        int_array->ints[i] =
            strtoi(string_array->strings[i],
                   NULL,
                   10);
    }
    int_array->n = string_array->n;
    return int_array;
}
