#include "../binary_c.h"
No_empty_translation_unit_warning;

struct string_array_t * new_string_array(const ssize_t n)
{
    /*
     * Make and return a new string array
     * containing n strings. Also clears the
     * array so all the pointers are NULL.
     */
    struct string_array_t * const s = Malloc(sizeof(struct string_array_t));
    s->n = n;
    if(n<=0)
    {
        s->strings = NULL;
        s->lengths = NULL;
        s->nalloc = 0;
    }
    else
    {
        s->strings = Calloc(n, sizeof(char*));
        s->lengths = Calloc(n, sizeof(size_t));
        s->nalloc = n;
    }
    return s;
}
