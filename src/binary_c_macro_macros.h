#pragma once
#ifndef BINARY_C_MACRO_MACROS_H
#define BINARY_C_MACRO_MACROS_H
/*
 * The binary_c stellar population nucleosynthesis framework.
 *
 * Copyright Robert Izzard 2000-2025
 *
 * Contact rob.izzard@gmail.com
 *
 * Code repository:
 * https://gitlab.com/binary_c
 *
 * Documentation is in doc/
 * Code is in src/
 *
 * To build, run
 *
 * ---
 *
 * meson setup builddir
 * cd builddir
 * ninja binary_c_install
 *
 * ---
 *
 * Please see the files README, LICENCE and CHANGES,
 * and the doc/ directory for documentation which is mirrored
 * at https://binary_c.gitlab.io
 *
 **********************
 *
 * Purpose:

 * Macro that handle other macros, and version checks.
 * These are very useful, e.g. in version.c
 */

/*
 * Macro to convert a macro to a string
 */
#define Stringify(item) "" #item

/*
 * Macro to expand a macro and then convert to a string
 */
#define Stringify_macro(item) (Stringify(item))


/*
 * Alternative stringifying macros
 */
#define Stringify_literal( x ) # x
#define Stringify_expanded( x ) Stringify_literal( x )
#define Stringify_with_quotes( x ) Stringify_expanded( Stringify_expanded( x ) )


/*
 * Macro that returns 1 if a macro (passed in as arg) is defined.
 *
 * Note that you should not embed this macro in another macro,
 * it will always return 1 in that case.
 */
#define Is_defined(MACRO)                                   \
    __extension__                                           \
    ({                                                      \
        (__builtin_strcmp("" # MACRO, Stringify(MACRO)));   \
    })

/*
 * Macros to count and add numbers of arguments in
 * variadic lists.
 *
 * Number_of_arguments works but gives warnings on strings.
 *
 * Instead, use Get_argument_count but this only works up
 * to N=100 - this should be sufficient.
 */
#define Number_of_arguments(...)                \
    (sizeof((int[]){                            \
            0,                                  \
            __VA_ARGS__ __VA_OPT__(,)})/        \
        sizeof(int)-1)

#define Sum_of_arguments(...)                   \
    sum(Number_of_arguments(__VA_ARGS__)        \
        __VA_OPT__(,) __VA_ARGS__)

#define Get_argument_count(...)                     \
    __Internal_get_argument_count_private(          \
        0,  __VA_ARGS__ __VA_OPT__(,) \
        100, 99, 98, 97, 96, 95, 94, 93, 92, 91,    \
        90, 89, 88, 87, 86, 85, 84, 83, 82, 81,     \
        80, 79, 78, 77, 76, 75, 74, 73, 72, 71,     \
        70, 69, 68, 67, 66, 65, 64, 63, 62, 61,     \
        60, 59, 58, 57, 56, 55, 54, 53, 52, 51,     \
        50, 49, 48, 47, 46, 45, 44, 43, 42, 41,     \
        40, 39, 38, 37, 36, 35, 34, 33, 32, 31,     \
        30, 29, 28, 27, 26, 25, 24, 23, 22, 21,     \
        20, 19, 18, 17, 16, 15, 14, 13, 12, 11,     \
        10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0)

#define __Internal_get_argument_count_private(                      \
    _0_, _1_, _2_, _3_, _4_, _5_, _6_, _7_, _8_, _9_, _10_,         \
    _11_, _12_, _13_, _14_, _15_, _16_, _17_, _18_, _19_, _20_,     \
    _21_, _22_, _23_, _24_, _25_, _26_, _27_, _28_, _29_, _30_,     \
    _31_, _32_, _33_, _34_, _35_, _36_, _37_, _38_, _39_, _40_,     \
    _41_, _42_, _43_, _44_, _45_, _46_, _47_, _48_, _49_, _50_,     \
    _51_, _52_, _53_, _54_, _55_, _56_, _57_, _58_, _59_, _60_,     \
    _61_, _62_, _63_, _64_, _65_, _66_, _67_, _68_, _69_, _70_,     \
    _71_, _72_, _73_, _74_, _75_, _76_, _77_, _78_, _79_, _80_,     \
    _81_, _82_, _83_, _84_, _85_, _86_, _87_, _88_, _89_, _90_,     \
    _91_, _92_, _93_, _94_, _95_, _96_, _97_, _98_, _99_, _100_,    \
    count, ...) count

#define _Shelvis(NAME) _Shelvis_implementation(NAME,        \
                                               Shelvis,     \
                                               __COUNTER__)
#define _Shelvis_implementation(NAME,LABEL,LINE)                \
    {                                                           \
        char * Concat3(__e,LABEL,LINE) =                        \
            getenv("BINARY_C_MACRO_HEADER");                    \
        if(Concat3(__e,LABEL,LINE))                             \
        {                                                       \
            Printf("%s %s : ",                                  \
                   Concat3(__e,LABEL,LINE),NAME);               \
        }                                                       \
        Printf("%s",                                            \
               Elvis(getenv("BINARY_C_" NAME "_MACRO_HEADER"),  \
                     "")                                        \
            );                                                  \
    }

/*
 * Test macro to see if the macro passed in is defined, and print
 * a description message (whether is it defined or not)
 */
#define Macrotest(macro)                                            \
    _Shelvis("MACRO");                                              \
    Printf("%s is %s ",                                             \
           "" #macro,                                               \
           (__builtin_strcmp("" #macro,                             \
                             Stringify_macro(macro)) ? "on" : "off" ));   \
    Printf("\n");
#define Macrotest2(macro,...)                                       \
    _Shelvis("MACRO");                                              \
    Printf("%s is %s ",                                             \
           "" #macro,                                               \
           (__builtin_strcmp("" #macro,                             \
                             Stringify_macro(macro)) ? "on" : "off" ));   \
    Printf(__VA_ARGS__);                                            \
    Printf("\n");
/*
 * As above, but only print the message string if the macro is defined
 * and true
 */
#define Macrotestif(macro,...)                          \
    _Shelvis("MACRO");                                  \
    Printf("%s is %s ",                                 \
           "" #macro,                                   \
           (strcmp("" #macro,                           \
                   Stringify(macro)) ? "on" : "off" )); \
    if(!__builtin_strcmp("" #macro,                     \
                         Stringify(macro)))             \
    {                                                   \
        Printf(__VA_ARGS__);                            \
    }                                                   \
    Printf("\n");
#define Macrotestif2(macro,...)                         \
    _Shelvis("MACRO");                                  \
    Printf("%s is %s ",                                 \
           "" #macro,                                   \
           (strcmp("" #macro,                           \
                   Stringify(macro)) ? "on" : "off" )); \
    Printf("\n");

/*
 * ... and if defined and false.
 */
#define Macrotestifnot(macro,...)                       \
    _Shelvis("MACRO");                                  \
    Printf("%s is %s ",                                 \
           "" #macro,                                   \
           (strcmp("" #macro,                           \
                   Stringify(macro)) ? "on" : "off" )); \
    Printf("\n");
#define Macrotestifnot2(macro,...)                      \
    _Shelvis("MACRO");                                  \
    Printf("%s is %s ",                                 \
           "" #macro,                                   \
           (strcmp("" #macro,                           \
                   Stringify(macro)) ? "on" : "off" )); \
    if(!__builtin_strcmp("" #macro,                     \
                         Stringify(macro)))             \
    {                                                   \
        Printf(__VA_ARGS__);                            \
    }                                                   \
    Printf("\n");

/*
 * Macros to show other macros
 */

#define Show_float_macro(macro)                 \
    _Shelvis("FLOAT");                          \
    if(__builtin_strcmp("" #macro,              \
                        Stringify(macro)))      \
    {                                           \
        Printf_deslash("%s is %50.30Le ",       \
                       "" #macro,               \
                       (long double)(macro));   \
    }                                           \
    else                                        \
    {                                           \
        Printf_deslash("%s is undefined ",      \
                       "" #macro);              \
    }                                           \
    Printf("\n");

#define Show_float_macro_as_string(macro)       \
    _Shelvis("FLOAT");                          \
    if(__builtin_strcmp("" #macro,              \
                        Stringify(macro)))      \
    {                                           \
        Printf_deslash("%s is \"%50.30Le\" ",   \
                       "" #macro,               \
                       (long double)(macro));   \
    }                                           \
    else                                        \
    {                                           \
        Printf_deslash("%s is undefined ",      \
                       "" #macro);              \
    }                                           \
    Printf("\n");



#define Show_float_macro2(macro,...)            \
    _Shelvis("FLOAT");                          \
    if(__builtin_strcmp("" #macro,              \
                        Stringify(macro)))      \
    {                                           \
        Printf_deslash("%s is %50.30Le ",       \
                       "" #macro,               \
                       (long double)(macro));   \
    }                                           \
    else                                        \
    {                                           \
        Printf_deslash("%s is undefined ",      \
                       "" #macro);              \
    }                                           \
    Printf(__VA_ARGS__);                        \
    Printf("\n");

#define Show_string_macro(macro)                            \
    _Shelvis("STRING");                                     \
    Printf_deslash("%s is %s ",                             \
                   "" #macro,                               \
                   (__builtin_strcmp("" #macro,             \
                                     Stringify_macro(macro))) ? \
                   (Stringify(macro)) :                     \
                   "undefined");                            \
    Printf("\n");
#define Show_string_macro2(macro,...)                       \
    _Shelvis("STRING");                                     \
    Printf_deslash("%s is %s ",                             \
                   "" #macro,                               \
                   (__builtin_strcmp("" #macro,             \
                                     Stringify_macro(macro))) ? \
                   (Stringify(macro)) :                     \
                   "undefined");                            \
    Printf(__VA_ARGS__);                                    \
    Printf("\n");
#define Show_string_macro_takes_varargs(macro,...)          \
    _Shelvis("STRING");                                     \
    Printf_deslash("%s is %s ",                             \
                   "" #macro,                               \
                   (__builtin_strcmp("" #macro,             \
                                     Stringify_macro(macro))) ? \
                   (Stringify(macro)) :                     \
                   "undefined");                            \
    Printf("\n");

/*
 * Show physical constants that are in cgs and SI form
 */
#define Show_physical_constants(macro)              \
    _Shelvis("FLOAT");                              \
    if(__builtin_strcmp("" #macro,                  \
                        Stringify(macro)))          \
    {                                               \
        Printf_deslash("%s is %50.30Le\n",          \
                       "" #macro,                   \
                       (long double)(macro));       \
        Printf_deslash("%s_SI is %50.30Le ",        \
                       "" #macro,                   \
                       (long double)(macro##_SI));  \
    }                                               \
    else                                            \
    {                                               \
        Printf_deslash("%s is undefined ",          \
                       "" #macro);                  \
    }                                               \
    Printf("\n");

/*
 * Macros to match X of TYPE to itself, when
 * X is not a macro (it's probably an enum).
 *
 * There's no real way to do this, so we use
 * a very unlikely integer to compare, and also
 * check whether it is < 0 or > 0 to avoid
 * overflow.
 *
 * We rely on the range of enum being int (it is)
 * which means we can compare long int.
 *
 * The unsigned version is identical just does
 * not handle negative numbers.
 */

#define Show_int_macro(macro)                       \
    _Shelvis("INT");                                \
    if(__builtin_strcmp("" #macro,                  \
                        Stringify(macro)))          \
    {                                               \
        Printf_deslash("%s is %lld  ",              \
                       "" #macro,                   \
                       (long long int)(macro));     \
    }                                               \
    else                                            \
    {                                               \
        Printf_deslash("%s is undefined ",      \
                       "" #macro);              \
    }                                           \
    Printf("\n");


#define Show_int_macro_as_string(macro)         \
    _Shelvis("INT");                            \
    if(__builtin_strcmp("" #macro,              \
                        Stringify(macro)))      \
    {                                           \
        Printf_deslash("%s is \"%lld\"  ",      \
                       "" #macro,               \
                       (long long int)(macro)); \
    }                                           \
    else                                        \
    {                                           \
        Printf_deslash("\"%s\" is undefined ",  \
                       "" #macro);              \
    }                                           \
    Printf("\n");


#define Show_int_macro2(macro,...)                  \
        _Shelvis("INT");                            \
        if(__builtin_strcmp("" #macro,              \
                            Stringify(macro)))      \
        {                                           \
            Printf_deslash("%s is %lld ",           \
                           "" #macro,               \
                           (long long int)(macro)); \
        }                                           \
        else                                        \
        {                                           \
            Printf_deslash("%s is undefined ",      \
                           "" #macro);              \
        }                                           \
        Printf(__VA_ARGS__);                        \
        Printf("\n");

#define Show_unsigned_int_macro(macro)                          \
        _Shelvis("UINT");                                       \
        if(__builtin_strcmp("" #macro,                          \
                            Stringify(macro)))                  \
        {                                                       \
            Printf_deslash("%s is %llu ",                       \
                           "" #macro,                           \
                           (unsigned long long int)(macro));    \
        }                                                       \
        else                                                    \
        {                                                       \
            Printf_deslash("%s is undefined ",                  \
                           "" #macro);                          \
        }                                                       \
        Printf("\n");

#define Show_unsigned_int_macro_as_string(macro)                \
        _Shelvis("UINT");                                       \
        if(__builtin_strcmp("" #macro,                          \
                            Stringify(macro)))                  \
        {                                                       \
            Printf_deslash("%s is \"%llu\" ",                   \
                           "" #macro,                           \
                           (unsigned long long int)(macro));    \
        }                                                       \
        else                                                    \
        {                                                       \
            Printf_deslash("%s is undefined ",                  \
                           "" #macro);                          \
        }                                                       \
        Printf("\n");


#define Show_unsigned_int_macro2(macro,...)                     \
        _Shelvis("UINT");                                       \
        if(__builtin_strcmp("" #macro,                          \
                            Stringify(macro)))                  \
        {                                                       \
            Printf_deslash("%s is %llu ",                       \
                           "" #macro,                           \
                           (unsigned long long int)(macro));    \
        }                                                       \
        else                                                    \
        {                                                       \
            Printf_deslash("%s is undefined ",                  \
                           "" #macro);                          \
        }                                                       \
        Printf(__VA_ARGS__);                                    \
        Printf("\n");



#define Show_long_int_macro(macro)              \
        _Shelvis("LONG_INT");                   \
        if(__builtin_strcmp("" #macro,          \
                            Stringify(macro)))  \
        {                                       \
            Printf_deslash("%s is %ld ",        \
                           "" #macro,           \
                           (long int)(macro));  \
        }                                       \
        else                                    \
        {                                       \
            Printf_deslash("%s is undefined ",  \
                           "" #macro);          \
        }                                       \
        Printf("\n");

#define Show_long_int_macro2(macro,...)         \
        _Shelvis("LONG_INT");                   \
        if(__builtin_strcmp("" #macro,          \
                            Stringify(macro)))  \
        {                                       \
            Printf_deslash("%s is %ld ",        \
                           "" #macro,           \
                           (long int)(macro));  \
        }                                       \
        else                                    \
        {                                       \
            Printf_deslash("%s is undefined ",  \
                           "" #macro);          \
        }                                       \
        Printf(__VA_ARGS__);                    \
        Printf("\n");

#define Show_enum(X)                            \
    Printf("%s is %d\n",#X,(int)X);

/*
 * Map a va_arg without having to specify its TYPE twice
 */
#define Map_varg(TYPE,NAME,ARGLIST)             \
    TYPE NAME = va_arg(ARGLIST,TYPE);



/*
 * Macro to prefix a list of macros
 *
 * Call with:
 *
 * 1st arg: the prefix
 * arg list: the macros to be prefixed
 */
#define _paste(A,B) _paste2(A,B)
#define _paste2(A,B) A##B

#define _prefix_macros0(...)
#define _prefix_macros1(PREFIX,X,...) PREFIX ## X, _prefix_macros0(PREFIX,__VA_ARGS__)
#define _prefix_macros2(PREFIX,X,...) PREFIX ## X, _prefix_macros1(PREFIX,__VA_ARGS__)
#define _prefix_macros3(PREFIX,X,...) PREFIX ## X, _prefix_macros2(PREFIX,__VA_ARGS__)
#define _prefix_macros4(PREFIX,X,...) PREFIX ## X, _prefix_macros3(PREFIX,__VA_ARGS__)
#define _prefix_macros5(PREFIX,X,...) PREFIX ## X, _prefix_macros4(PREFIX,__VA_ARGS__)
#define _prefix_macros6(PREFIX,X,...) PREFIX ## X, _prefix_macros5(PREFIX,__VA_ARGS__)
#define _prefix_macros7(PREFIX,X,...) PREFIX ## X, _prefix_macros6(PREFIX,__VA_ARGS__)
#define _prefix_macros8(PREFIX,X,...) PREFIX ## X, _prefix_macros7(PREFIX,__VA_ARGS__)
#define _prefix_macros9(PREFIX,X,...) PREFIX ## X, _prefix_macros8(PREFIX,__VA_ARGS__)
#define _prefix_macros10(PREFIX,X,...) PREFIX ## X, _prefix_macros9(PREFIX,__VA_ARGS__)
#define _prefix_macros11(PREFIX,X,...) PREFIX ## X, _prefix_macros10(PREFIX,__VA_ARGS__)
#define _prefix_macros12(PREFIX,X,...) PREFIX ## X, _prefix_macros11(PREFIX,__VA_ARGS__)
#define _prefix_macros13(PREFIX,X,...) PREFIX ## X, _prefix_macros12(PREFIX,__VA_ARGS__)
#define _prefix_macros14(PREFIX,X,...) PREFIX ## X, _prefix_macros13(PREFIX,__VA_ARGS__)
#define _prefix_macros15(PREFIX,X,...) PREFIX ## X, _prefix_macros14(PREFIX,__VA_ARGS__)
#define _prefix_macros16(PREFIX,X,...) PREFIX ## X, _prefix_macros15(PREFIX,__VA_ARGS__)
#define _prefix_macros17(PREFIX,X,...) PREFIX ## X, _prefix_macros16(PREFIX,__VA_ARGS__)
#define _prefix_macros18(PREFIX,X,...) PREFIX ## X, _prefix_macros17(PREFIX,__VA_ARGS__)
#define _prefix_macros19(PREFIX,X,...) PREFIX ## X, _prefix_macros18(PREFIX,__VA_ARGS__)
#define _prefix_macros20(PREFIX,X,...) PREFIX ## X, _prefix_macros19(PREFIX,__VA_ARGS__)
#define _prefix_macros21(PREFIX,X,...) PREFIX ## X, _prefix_macros20(PREFIX,__VA_ARGS__)
#define _prefix_macros22(PREFIX,X,...) PREFIX ## X, _prefix_macros21(PREFIX,__VA_ARGS__)
#define _prefix_macros23(PREFIX,X,...) PREFIX ## X, _prefix_macros22(PREFIX,__VA_ARGS__)
#define _prefix_macros24(PREFIX,X,...) PREFIX ## X, _prefix_macros23(PREFIX,__VA_ARGS__)
#define _prefix_macros25(PREFIX,X,...) PREFIX ## X, _prefix_macros24(PREFIX,__VA_ARGS__)
#define _prefix_macros26(PREFIX,X,...) PREFIX ## X, _prefix_macros25(PREFIX,__VA_ARGS__)
#define _prefix_macros27(PREFIX,X,...) PREFIX ## X, _prefix_macros26(PREFIX,__VA_ARGS__)
#define _prefix_macros28(PREFIX,X,...) PREFIX ## X, _prefix_macros27(PREFIX,__VA_ARGS__)
#define _prefix_macros29(PREFIX,X,...) PREFIX ## X, _prefix_macros28(PREFIX,__VA_ARGS__)
#define _prefix_macros30(PREFIX,X,...) PREFIX ## X, _prefix_macros29(PREFIX,__VA_ARGS__)
#define _prefix_macros31(PREFIX,X,...) PREFIX ## X, _prefix_macros30(PREFIX,__VA_ARGS__)
#define _prefix_macros32(PREFIX,X,...) PREFIX ## X, _prefix_macros31(PREFIX,__VA_ARGS__)
#define _prefix_macros33(PREFIX,X,...) PREFIX ## X, _prefix_macros32(PREFIX,__VA_ARGS__)
#define _prefix_macros34(PREFIX,X,...) PREFIX ## X, _prefix_macros33(PREFIX,__VA_ARGS__)
#define _prefix_macros35(PREFIX,X,...) PREFIX ## X, _prefix_macros34(PREFIX,__VA_ARGS__)
#define _prefix_macros36(PREFIX,X,...) PREFIX ## X, _prefix_macros35(PREFIX,__VA_ARGS__)
#define _prefix_macros37(PREFIX,X,...) PREFIX ## X, _prefix_macros36(PREFIX,__VA_ARGS__)
#define _prefix_macros38(PREFIX,X,...) PREFIX ## X, _prefix_macros37(PREFIX,__VA_ARGS__)
#define _prefix_macros39(PREFIX,X,...) PREFIX ## X, _prefix_macros38(PREFIX,__VA_ARGS__)
#define _prefix_macros40(PREFIX,X,...) PREFIX ## X, _prefix_macros39(PREFIX,__VA_ARGS__)
#define _prefix_macros41(PREFIX,X,...) PREFIX ## X, _prefix_macros40(PREFIX,__VA_ARGS__)
#define _prefix_macros42(PREFIX,X,...) PREFIX ## X, _prefix_macros41(PREFIX,__VA_ARGS__)
#define _prefix_macros43(PREFIX,X,...) PREFIX ## X, _prefix_macros42(PREFIX,__VA_ARGS__)
#define _prefix_macros44(PREFIX,X,...) PREFIX ## X, _prefix_macros43(PREFIX,__VA_ARGS__)
#define _prefix_macros45(PREFIX,X,...) PREFIX ## X, _prefix_macros44(PREFIX,__VA_ARGS__)
#define _prefix_macros46(PREFIX,X,...) PREFIX ## X, _prefix_macros45(PREFIX,__VA_ARGS__)
#define _prefix_macros47(PREFIX,X,...) PREFIX ## X, _prefix_macros46(PREFIX,__VA_ARGS__)
#define _prefix_macros48(PREFIX,X,...) PREFIX ## X, _prefix_macros47(PREFIX,__VA_ARGS__)
#define _prefix_macros49(PREFIX,X,...) PREFIX ## X, _prefix_macros48(PREFIX,__VA_ARGS__)
#define _prefix_macros50(PREFIX,X,...) PREFIX ## X, _prefix_macros49(PREFIX,__VA_ARGS__)
#define _prefix_macros51(PREFIX,X,...) PREFIX ## X, _prefix_macros50(PREFIX,__VA_ARGS__)
#define _prefix_macros52(PREFIX,X,...) PREFIX ## X, _prefix_macros51(PREFIX,__VA_ARGS__)
#define _prefix_macros53(PREFIX,X,...) PREFIX ## X, _prefix_macros52(PREFIX,__VA_ARGS__)
#define _prefix_macros54(PREFIX,X,...) PREFIX ## X, _prefix_macros53(PREFIX,__VA_ARGS__)
#define _prefix_macros55(PREFIX,X,...) PREFIX ## X, _prefix_macros54(PREFIX,__VA_ARGS__)
#define _prefix_macros56(PREFIX,X,...) PREFIX ## X, _prefix_macros55(PREFIX,__VA_ARGS__)
#define _prefix_macros57(PREFIX,X,...) PREFIX ## X, _prefix_macros56(PREFIX,__VA_ARGS__)
#define _prefix_macros58(PREFIX,X,...) PREFIX ## X, _prefix_macros57(PREFIX,__VA_ARGS__)
#define _prefix_macros59(PREFIX,X,...) PREFIX ## X, _prefix_macros58(PREFIX,__VA_ARGS__)
#define _prefix_macros60(PREFIX,X,...) PREFIX ## X, _prefix_macros59(PREFIX,__VA_ARGS__)
#define _prefix_macros61(PREFIX,X,...) PREFIX ## X, _prefix_macros60(PREFIX,__VA_ARGS__)
#define _prefix_macros62(PREFIX,X,...) PREFIX ## X, _prefix_macros61(PREFIX,__VA_ARGS__)
#define _prefix_macros63(PREFIX,X,...) PREFIX ## X, _prefix_macros62(PREFIX,__VA_ARGS__)
#define _prefix_macros64(PREFIX,X,...) PREFIX ## X, _prefix_macros63(PREFIX,__VA_ARGS__)
#define _prefix_macros65(PREFIX,X,...) PREFIX ## X, _prefix_macros64(PREFIX,__VA_ARGS__)
#define _prefix_macros66(PREFIX,X,...) PREFIX ## X, _prefix_macros65(PREFIX,__VA_ARGS__)
#define _prefix_macros67(PREFIX,X,...) PREFIX ## X, _prefix_macros66(PREFIX,__VA_ARGS__)
#define _prefix_macros68(PREFIX,X,...) PREFIX ## X, _prefix_macros67(PREFIX,__VA_ARGS__)
#define _prefix_macros69(PREFIX,X,...) PREFIX ## X, _prefix_macros68(PREFIX,__VA_ARGS__)
#define _prefix_macros70(PREFIX,X,...) PREFIX ## X, _prefix_macros69(PREFIX,__VA_ARGS__)
#define _prefix_macros71(PREFIX,X,...) PREFIX ## X, _prefix_macros70(PREFIX,__VA_ARGS__)
#define _prefix_macros72(PREFIX,X,...) PREFIX ## X, _prefix_macros71(PREFIX,__VA_ARGS__)
#define _prefix_macros73(PREFIX,X,...) PREFIX ## X, _prefix_macros72(PREFIX,__VA_ARGS__)
#define _prefix_macros74(PREFIX,X,...) PREFIX ## X, _prefix_macros73(PREFIX,__VA_ARGS__)
#define _prefix_macros75(PREFIX,X,...) PREFIX ## X, _prefix_macros74(PREFIX,__VA_ARGS__)
#define _prefix_macros76(PREFIX,X,...) PREFIX ## X, _prefix_macros75(PREFIX,__VA_ARGS__)
#define _prefix_macros77(PREFIX,X,...) PREFIX ## X, _prefix_macros76(PREFIX,__VA_ARGS__)
#define _prefix_macros78(PREFIX,X,...) PREFIX ## X, _prefix_macros77(PREFIX,__VA_ARGS__)
#define _prefix_macros79(PREFIX,X,...) PREFIX ## X, _prefix_macros78(PREFIX,__VA_ARGS__)
#define _prefix_macros80(PREFIX,X,...) PREFIX ## X, _prefix_macros79(PREFIX,__VA_ARGS__)
#define _prefix_macros81(PREFIX,X,...) PREFIX ## X, _prefix_macros80(PREFIX,__VA_ARGS__)
#define _prefix_macros82(PREFIX,X,...) PREFIX ## X, _prefix_macros81(PREFIX,__VA_ARGS__)
#define _prefix_macros83(PREFIX,X,...) PREFIX ## X, _prefix_macros82(PREFIX,__VA_ARGS__)
#define _prefix_macros84(PREFIX,X,...) PREFIX ## X, _prefix_macros83(PREFIX,__VA_ARGS__)
#define _prefix_macros85(PREFIX,X,...) PREFIX ## X, _prefix_macros84(PREFIX,__VA_ARGS__)
#define _prefix_macros86(PREFIX,X,...) PREFIX ## X, _prefix_macros85(PREFIX,__VA_ARGS__)
#define _prefix_macros87(PREFIX,X,...) PREFIX ## X, _prefix_macros86(PREFIX,__VA_ARGS__)
#define _prefix_macros88(PREFIX,X,...) PREFIX ## X, _prefix_macros87(PREFIX,__VA_ARGS__)
#define _prefix_macros89(PREFIX,X,...) PREFIX ## X, _prefix_macros88(PREFIX,__VA_ARGS__)
#define _prefix_macros90(PREFIX,X,...) PREFIX ## X, _prefix_macros89(PREFIX,__VA_ARGS__)
#define _prefix_macros91(PREFIX,X,...) PREFIX ## X, _prefix_macros90(PREFIX,__VA_ARGS__)
#define _prefix_macros92(PREFIX,X,...) PREFIX ## X, _prefix_macros91(PREFIX,__VA_ARGS__)
#define _prefix_macros93(PREFIX,X,...) PREFIX ## X, _prefix_macros92(PREFIX,__VA_ARGS__)
#define _prefix_macros94(PREFIX,X,...) PREFIX ## X, _prefix_macros93(PREFIX,__VA_ARGS__)
#define _prefix_macros95(PREFIX,X,...) PREFIX ## X, _prefix_macros94(PREFIX,__VA_ARGS__)
#define _prefix_macros96(PREFIX,X,...) PREFIX ## X, _prefix_macros95(PREFIX,__VA_ARGS__)
#define _prefix_macros97(PREFIX,X,...) PREFIX ## X, _prefix_macros96(PREFIX,__VA_ARGS__)
#define _prefix_macros98(PREFIX,X,...) PREFIX ## X, _prefix_macros97(PREFIX,__VA_ARGS__)
#define _prefix_macros99(PREFIX,X,...) PREFIX ## X, _prefix_macros98(PREFIX,__VA_ARGS__)
#define _prefix_macros100(PREFIX,X,...) PREFIX ## X, _prefix_macros99(PREFIX,__VA_ARGS__)
#define _prefix_macros101(PREFIX,X,...) PREFIX ## X, _prefix_macros100(PREFIX,__VA_ARGS__)
#define _prefix_macros102(PREFIX,X,...) PREFIX ## X, _prefix_macros101(PREFIX,__VA_ARGS__)
#define _prefix_macros103(PREFIX,X,...) PREFIX ## X, _prefix_macros102(PREFIX,__VA_ARGS__)
#define _prefix_macros104(PREFIX,X,...) PREFIX ## X, _prefix_macros103(PREFIX,__VA_ARGS__)
#define _prefix_macros105(PREFIX,X,...) PREFIX ## X, _prefix_macros104(PREFIX,__VA_ARGS__)
#define _prefix_macros106(PREFIX,X,...) PREFIX ## X, _prefix_macros105(PREFIX,__VA_ARGS__)
#define _prefix_macros107(PREFIX,X,...) PREFIX ## X, _prefix_macros106(PREFIX,__VA_ARGS__)
#define _prefix_macros108(PREFIX,X,...) PREFIX ## X, _prefix_macros107(PREFIX,__VA_ARGS__)
#define _prefix_macros109(PREFIX,X,...) PREFIX ## X, _prefix_macros108(PREFIX,__VA_ARGS__)
#define _prefix_macros110(PREFIX,X,...) PREFIX ## X, _prefix_macros109(PREFIX,__VA_ARGS__)
#define _prefix_macros111(PREFIX,X,...) PREFIX ## X, _prefix_macros110(PREFIX,__VA_ARGS__)
#define _prefix_macros112(PREFIX,X,...) PREFIX ## X, _prefix_macros111(PREFIX,__VA_ARGS__)
#define _prefix_macros113(PREFIX,X,...) PREFIX ## X, _prefix_macros112(PREFIX,__VA_ARGS__)
#define _prefix_macros114(PREFIX,X,...) PREFIX ## X, _prefix_macros113(PREFIX,__VA_ARGS__)
#define _prefix_macros115(PREFIX,X,...) PREFIX ## X, _prefix_macros114(PREFIX,__VA_ARGS__)
#define _prefix_macros116(PREFIX,X,...) PREFIX ## X, _prefix_macros115(PREFIX,__VA_ARGS__)
#define _prefix_macros117(PREFIX,X,...) PREFIX ## X, _prefix_macros116(PREFIX,__VA_ARGS__)
#define _prefix_macros118(PREFIX,X,...) PREFIX ## X, _prefix_macros117(PREFIX,__VA_ARGS__)
#define _prefix_macros119(PREFIX,X,...) PREFIX ## X, _prefix_macros118(PREFIX,__VA_ARGS__)
#define _prefix_macros120(PREFIX,X,...) PREFIX ## X, _prefix_macros119(PREFIX,__VA_ARGS__)
#define _prefix_macros121(PREFIX,X,...) PREFIX ## X, _prefix_macros120(PREFIX,__VA_ARGS__)
#define _prefix_macros122(PREFIX,X,...) PREFIX ## X, _prefix_macros121(PREFIX,__VA_ARGS__)
#define _prefix_macros123(PREFIX,X,...) PREFIX ## X, _prefix_macros122(PREFIX,__VA_ARGS__)
#define _prefix_macros124(PREFIX,X,...) PREFIX ## X, _prefix_macros123(PREFIX,__VA_ARGS__)
#define _prefix_macros125(PREFIX,X,...) PREFIX ## X, _prefix_macros124(PREFIX,__VA_ARGS__)
#define _prefix_macros126(PREFIX,X,...) PREFIX ## X, _prefix_macros125(PREFIX,__VA_ARGS__)
#define _prefix_macros127(PREFIX,X,...) PREFIX ## X, _prefix_macros126(PREFIX,__VA_ARGS__)
#define _prefix_macros128(PREFIX,X,...) PREFIX ## X, _prefix_macros127(PREFIX,__VA_ARGS__)
#define _prefix_macros129(PREFIX,X,...) PREFIX ## X, _prefix_macros128(PREFIX,__VA_ARGS__)
#define _prefix_macros130(PREFIX,X,...) PREFIX ## X, _prefix_macros129(PREFIX,__VA_ARGS__)
#define _prefix_macros131(PREFIX,X,...) PREFIX ## X, _prefix_macros130(PREFIX,__VA_ARGS__)
#define _prefix_macros132(PREFIX,X,...) PREFIX ## X, _prefix_macros131(PREFIX,__VA_ARGS__)
#define _prefix_macros133(PREFIX,X,...) PREFIX ## X, _prefix_macros132(PREFIX,__VA_ARGS__)
#define _prefix_macros134(PREFIX,X,...) PREFIX ## X, _prefix_macros133(PREFIX,__VA_ARGS__)
#define _prefix_macros135(PREFIX,X,...) PREFIX ## X, _prefix_macros134(PREFIX,__VA_ARGS__)
#define _prefix_macros136(PREFIX,X,...) PREFIX ## X, _prefix_macros135(PREFIX,__VA_ARGS__)
#define _prefix_macros137(PREFIX,X,...) PREFIX ## X, _prefix_macros136(PREFIX,__VA_ARGS__)
#define _prefix_macros138(PREFIX,X,...) PREFIX ## X, _prefix_macros137(PREFIX,__VA_ARGS__)
#define _prefix_macros139(PREFIX,X,...) PREFIX ## X, _prefix_macros138(PREFIX,__VA_ARGS__)
#define _prefix_macros140(PREFIX,X,...) PREFIX ## X, _prefix_macros139(PREFIX,__VA_ARGS__)
#define _prefix_macros141(PREFIX,X,...) PREFIX ## X, _prefix_macros140(PREFIX,__VA_ARGS__)
#define _prefix_macros142(PREFIX,X,...) PREFIX ## X, _prefix_macros141(PREFIX,__VA_ARGS__)
#define _prefix_macros143(PREFIX,X,...) PREFIX ## X, _prefix_macros142(PREFIX,__VA_ARGS__)
#define _prefix_macros144(PREFIX,X,...) PREFIX ## X, _prefix_macros143(PREFIX,__VA_ARGS__)
#define _prefix_macros145(PREFIX,X,...) PREFIX ## X, _prefix_macros144(PREFIX,__VA_ARGS__)
#define _prefix_macros146(PREFIX,X,...) PREFIX ## X, _prefix_macros145(PREFIX,__VA_ARGS__)
#define _prefix_macros147(PREFIX,X,...) PREFIX ## X, _prefix_macros146(PREFIX,__VA_ARGS__)
#define _prefix_macros148(PREFIX,X,...) PREFIX ## X, _prefix_macros147(PREFIX,__VA_ARGS__)
#define _prefix_macros149(PREFIX,X,...) PREFIX ## X, _prefix_macros148(PREFIX,__VA_ARGS__)
#define _prefix_macros150(PREFIX,X,...) PREFIX ## X, _prefix_macros149(PREFIX,__VA_ARGS__)
#define _prefix_macros151(PREFIX,X,...) PREFIX ## X, _prefix_macros150(PREFIX,__VA_ARGS__)
#define _prefix_macros152(PREFIX,X,...) PREFIX ## X, _prefix_macros151(PREFIX,__VA_ARGS__)
#define _prefix_macros153(PREFIX,X,...) PREFIX ## X, _prefix_macros152(PREFIX,__VA_ARGS__)
#define _prefix_macros154(PREFIX,X,...) PREFIX ## X, _prefix_macros153(PREFIX,__VA_ARGS__)
#define _prefix_macros155(PREFIX,X,...) PREFIX ## X, _prefix_macros154(PREFIX,__VA_ARGS__)
#define _prefix_macros156(PREFIX,X,...) PREFIX ## X, _prefix_macros155(PREFIX,__VA_ARGS__)
#define _prefix_macros157(PREFIX,X,...) PREFIX ## X, _prefix_macros156(PREFIX,__VA_ARGS__)
#define _prefix_macros158(PREFIX,X,...) PREFIX ## X, _prefix_macros157(PREFIX,__VA_ARGS__)
#define _prefix_macros159(PREFIX,X,...) PREFIX ## X, _prefix_macros158(PREFIX,__VA_ARGS__)
#define _prefix_macros160(PREFIX,X,...) PREFIX ## X, _prefix_macros159(PREFIX,__VA_ARGS__)
#define _prefix_macros161(PREFIX,X,...) PREFIX ## X, _prefix_macros160(PREFIX,__VA_ARGS__)
#define _prefix_macros162(PREFIX,X,...) PREFIX ## X, _prefix_macros161(PREFIX,__VA_ARGS__)
#define _prefix_macros163(PREFIX,X,...) PREFIX ## X, _prefix_macros162(PREFIX,__VA_ARGS__)
#define _prefix_macros164(PREFIX,X,...) PREFIX ## X, _prefix_macros163(PREFIX,__VA_ARGS__)
#define _prefix_macros165(PREFIX,X,...) PREFIX ## X, _prefix_macros164(PREFIX,__VA_ARGS__)
#define _prefix_macros166(PREFIX,X,...) PREFIX ## X, _prefix_macros165(PREFIX,__VA_ARGS__)
#define _prefix_macros167(PREFIX,X,...) PREFIX ## X, _prefix_macros166(PREFIX,__VA_ARGS__)
#define _prefix_macros168(PREFIX,X,...) PREFIX ## X, _prefix_macros167(PREFIX,__VA_ARGS__)
#define _prefix_macros169(PREFIX,X,...) PREFIX ## X, _prefix_macros168(PREFIX,__VA_ARGS__)
#define _prefix_macros170(PREFIX,X,...) PREFIX ## X, _prefix_macros169(PREFIX,__VA_ARGS__)
#define _prefix_macros171(PREFIX,X,...) PREFIX ## X, _prefix_macros170(PREFIX,__VA_ARGS__)
#define _prefix_macros172(PREFIX,X,...) PREFIX ## X, _prefix_macros171(PREFIX,__VA_ARGS__)
#define _prefix_macros173(PREFIX,X,...) PREFIX ## X, _prefix_macros172(PREFIX,__VA_ARGS__)
#define _prefix_macros174(PREFIX,X,...) PREFIX ## X, _prefix_macros173(PREFIX,__VA_ARGS__)
#define _prefix_macros175(PREFIX,X,...) PREFIX ## X, _prefix_macros174(PREFIX,__VA_ARGS__)
#define _prefix_macros176(PREFIX,X,...) PREFIX ## X, _prefix_macros175(PREFIX,__VA_ARGS__)
#define _prefix_macros177(PREFIX,X,...) PREFIX ## X, _prefix_macros176(PREFIX,__VA_ARGS__)
#define _prefix_macros178(PREFIX,X,...) PREFIX ## X, _prefix_macros177(PREFIX,__VA_ARGS__)
#define _prefix_macros179(PREFIX,X,...) PREFIX ## X, _prefix_macros178(PREFIX,__VA_ARGS__)
#define _prefix_macros180(PREFIX,X,...) PREFIX ## X, _prefix_macros179(PREFIX,__VA_ARGS__)
#define _prefix_macros181(PREFIX,X,...) PREFIX ## X, _prefix_macros180(PREFIX,__VA_ARGS__)
#define _prefix_macros182(PREFIX,X,...) PREFIX ## X, _prefix_macros181(PREFIX,__VA_ARGS__)
#define _prefix_macros183(PREFIX,X,...) PREFIX ## X, _prefix_macros182(PREFIX,__VA_ARGS__)
#define _prefix_macros184(PREFIX,X,...) PREFIX ## X, _prefix_macros183(PREFIX,__VA_ARGS__)
#define _prefix_macros185(PREFIX,X,...) PREFIX ## X, _prefix_macros184(PREFIX,__VA_ARGS__)
#define _prefix_macros186(PREFIX,X,...) PREFIX ## X, _prefix_macros185(PREFIX,__VA_ARGS__)
#define _prefix_macros187(PREFIX,X,...) PREFIX ## X, _prefix_macros186(PREFIX,__VA_ARGS__)
#define _prefix_macros188(PREFIX,X,...) PREFIX ## X, _prefix_macros187(PREFIX,__VA_ARGS__)
#define _prefix_macros189(PREFIX,X,...) PREFIX ## X, _prefix_macros188(PREFIX,__VA_ARGS__)
#define _prefix_macros190(PREFIX,X,...) PREFIX ## X, _prefix_macros189(PREFIX,__VA_ARGS__)
#define _prefix_macros191(PREFIX,X,...) PREFIX ## X, _prefix_macros190(PREFIX,__VA_ARGS__)
#define _prefix_macros192(PREFIX,X,...) PREFIX ## X, _prefix_macros191(PREFIX,__VA_ARGS__)
#define _prefix_macros193(PREFIX,X,...) PREFIX ## X, _prefix_macros192(PREFIX,__VA_ARGS__)
#define _prefix_macros194(PREFIX,X,...) PREFIX ## X, _prefix_macros193(PREFIX,__VA_ARGS__)
#define _prefix_macros195(PREFIX,X,...) PREFIX ## X, _prefix_macros194(PREFIX,__VA_ARGS__)
#define _prefix_macros196(PREFIX,X,...) PREFIX ## X, _prefix_macros195(PREFIX,__VA_ARGS__)
#define _prefix_macros197(PREFIX,X,...) PREFIX ## X, _prefix_macros196(PREFIX,__VA_ARGS__)
#define _prefix_macros198(PREFIX,X,...) PREFIX ## X, _prefix_macros197(PREFIX,__VA_ARGS__)
#define _prefix_macros199(PREFIX,X,...) PREFIX ## X, _prefix_macros198(PREFIX,__VA_ARGS__)
#define _prefix_macros200(PREFIX,X,...) PREFIX ## X, _prefix_macros199(PREFIX,__VA_ARGS__)
#define _prefix_macros201(PREFIX,X,...) PREFIX ## X, _prefix_macros200(PREFIX,__VA_ARGS__)
#define _prefix_macros202(PREFIX,X,...) PREFIX ## X, _prefix_macros201(PREFIX,__VA_ARGS__)
#define _prefix_macros203(PREFIX,X,...) PREFIX ## X, _prefix_macros202(PREFIX,__VA_ARGS__)
#define _prefix_macros204(PREFIX,X,...) PREFIX ## X, _prefix_macros203(PREFIX,__VA_ARGS__)
#define _prefix_macros205(PREFIX,X,...) PREFIX ## X, _prefix_macros204(PREFIX,__VA_ARGS__)
#define _prefix_macros206(PREFIX,X,...) PREFIX ## X, _prefix_macros205(PREFIX,__VA_ARGS__)
#define _prefix_macros207(PREFIX,X,...) PREFIX ## X, _prefix_macros206(PREFIX,__VA_ARGS__)
#define _prefix_macros208(PREFIX,X,...) PREFIX ## X, _prefix_macros207(PREFIX,__VA_ARGS__)
#define _prefix_macros209(PREFIX,X,...) PREFIX ## X, _prefix_macros208(PREFIX,__VA_ARGS__)
#define _prefix_macros210(PREFIX,X,...) PREFIX ## X, _prefix_macros209(PREFIX,__VA_ARGS__)
#define _prefix_macros211(PREFIX,X,...) PREFIX ## X, _prefix_macros210(PREFIX,__VA_ARGS__)
#define _prefix_macros212(PREFIX,X,...) PREFIX ## X, _prefix_macros211(PREFIX,__VA_ARGS__)
#define _prefix_macros213(PREFIX,X,...) PREFIX ## X, _prefix_macros212(PREFIX,__VA_ARGS__)
#define _prefix_macros214(PREFIX,X,...) PREFIX ## X, _prefix_macros213(PREFIX,__VA_ARGS__)
#define _prefix_macros215(PREFIX,X,...) PREFIX ## X, _prefix_macros214(PREFIX,__VA_ARGS__)
#define _prefix_macros216(PREFIX,X,...) PREFIX ## X, _prefix_macros215(PREFIX,__VA_ARGS__)
#define _prefix_macros217(PREFIX,X,...) PREFIX ## X, _prefix_macros216(PREFIX,__VA_ARGS__)
#define _prefix_macros218(PREFIX,X,...) PREFIX ## X, _prefix_macros217(PREFIX,__VA_ARGS__)
#define _prefix_macros219(PREFIX,X,...) PREFIX ## X, _prefix_macros218(PREFIX,__VA_ARGS__)
#define _prefix_macros220(PREFIX,X,...) PREFIX ## X, _prefix_macros219(PREFIX,__VA_ARGS__)
#define _prefix_macros221(PREFIX,X,...) PREFIX ## X, _prefix_macros220(PREFIX,__VA_ARGS__)
#define _prefix_macros222(PREFIX,X,...) PREFIX ## X, _prefix_macros221(PREFIX,__VA_ARGS__)
#define _prefix_macros223(PREFIX,X,...) PREFIX ## X, _prefix_macros222(PREFIX,__VA_ARGS__)
#define _prefix_macros224(PREFIX,X,...) PREFIX ## X, _prefix_macros223(PREFIX,__VA_ARGS__)
#define _prefix_macros225(PREFIX,X,...) PREFIX ## X, _prefix_macros224(PREFIX,__VA_ARGS__)
#define _prefix_macros226(PREFIX,X,...) PREFIX ## X, _prefix_macros225(PREFIX,__VA_ARGS__)
#define _prefix_macros227(PREFIX,X,...) PREFIX ## X, _prefix_macros226(PREFIX,__VA_ARGS__)
#define _prefix_macros228(PREFIX,X,...) PREFIX ## X, _prefix_macros227(PREFIX,__VA_ARGS__)
#define _prefix_macros229(PREFIX,X,...) PREFIX ## X, _prefix_macros228(PREFIX,__VA_ARGS__)
#define _prefix_macros230(PREFIX,X,...) PREFIX ## X, _prefix_macros229(PREFIX,__VA_ARGS__)
#define _prefix_macros231(PREFIX,X,...) PREFIX ## X, _prefix_macros230(PREFIX,__VA_ARGS__)
#define _prefix_macros232(PREFIX,X,...) PREFIX ## X, _prefix_macros231(PREFIX,__VA_ARGS__)
#define _prefix_macros233(PREFIX,X,...) PREFIX ## X, _prefix_macros232(PREFIX,__VA_ARGS__)
#define _prefix_macros234(PREFIX,X,...) PREFIX ## X, _prefix_macros233(PREFIX,__VA_ARGS__)
#define _prefix_macros235(PREFIX,X,...) PREFIX ## X, _prefix_macros234(PREFIX,__VA_ARGS__)
#define _prefix_macros236(PREFIX,X,...) PREFIX ## X, _prefix_macros235(PREFIX,__VA_ARGS__)
#define _prefix_macros237(PREFIX,X,...) PREFIX ## X, _prefix_macros236(PREFIX,__VA_ARGS__)
#define _prefix_macros238(PREFIX,X,...) PREFIX ## X, _prefix_macros237(PREFIX,__VA_ARGS__)
#define _prefix_macros239(PREFIX,X,...) PREFIX ## X, _prefix_macros238(PREFIX,__VA_ARGS__)
#define _prefix_macros240(PREFIX,X,...) PREFIX ## X, _prefix_macros239(PREFIX,__VA_ARGS__)
#define _prefix_macros241(PREFIX,X,...) PREFIX ## X, _prefix_macros240(PREFIX,__VA_ARGS__)
#define _prefix_macros242(PREFIX,X,...) PREFIX ## X, _prefix_macros241(PREFIX,__VA_ARGS__)
#define _prefix_macros243(PREFIX,X,...) PREFIX ## X, _prefix_macros242(PREFIX,__VA_ARGS__)
#define _prefix_macros244(PREFIX,X,...) PREFIX ## X, _prefix_macros243(PREFIX,__VA_ARGS__)
#define _prefix_macros245(PREFIX,X,...) PREFIX ## X, _prefix_macros244(PREFIX,__VA_ARGS__)
#define _prefix_macros246(PREFIX,X,...) PREFIX ## X, _prefix_macros245(PREFIX,__VA_ARGS__)
#define _prefix_macros247(PREFIX,X,...) PREFIX ## X, _prefix_macros246(PREFIX,__VA_ARGS__)
#define _prefix_macros248(PREFIX,X,...) PREFIX ## X, _prefix_macros247(PREFIX,__VA_ARGS__)
#define _prefix_macros249(PREFIX,X,...) PREFIX ## X, _prefix_macros248(PREFIX,__VA_ARGS__)
#define _prefix_macros250(PREFIX,X,...) PREFIX ## X, _prefix_macros249(PREFIX,__VA_ARGS__)
#define _prefix_macros251(PREFIX,X,...) PREFIX ## X, _prefix_macros250(PREFIX,__VA_ARGS__)
#define _prefix_macros252(PREFIX,X,...) PREFIX ## X, _prefix_macros251(PREFIX,__VA_ARGS__)
#define _prefix_macros253(PREFIX,X,...) PREFIX ## X, _prefix_macros252(PREFIX,__VA_ARGS__)
#define _prefix_macros254(PREFIX,X,...) PREFIX ## X, _prefix_macros253(PREFIX,__VA_ARGS__)
#define _prefix_macros255(PREFIX,X,...) PREFIX ## X, _prefix_macros254(PREFIX,__VA_ARGS__)
#define _prefix_macros256(PREFIX,X,...) PREFIX ## X, _prefix_macros255(PREFIX,__VA_ARGS__)
#define _prefix_macros257(PREFIX,X,...) PREFIX ## X, _prefix_macros256(PREFIX,__VA_ARGS__)
#define _prefix_macros258(PREFIX,X,...) PREFIX ## X, _prefix_macros257(PREFIX,__VA_ARGS__)
#define _prefix_macros259(PREFIX,X,...) PREFIX ## X, _prefix_macros258(PREFIX,__VA_ARGS__)
#define _prefix_macros260(PREFIX,X,...) PREFIX ## X, _prefix_macros259(PREFIX,__VA_ARGS__)
#define _prefix_macros261(PREFIX,X,...) PREFIX ## X, _prefix_macros260(PREFIX,__VA_ARGS__)
#define _prefix_macros262(PREFIX,X,...) PREFIX ## X, _prefix_macros261(PREFIX,__VA_ARGS__)
#define _prefix_macros263(PREFIX,X,...) PREFIX ## X, _prefix_macros262(PREFIX,__VA_ARGS__)
#define _prefix_macros264(PREFIX,X,...) PREFIX ## X, _prefix_macros263(PREFIX,__VA_ARGS__)
#define _prefix_macros265(PREFIX,X,...) PREFIX ## X, _prefix_macros264(PREFIX,__VA_ARGS__)
#define _prefix_macros266(PREFIX,X,...) PREFIX ## X, _prefix_macros265(PREFIX,__VA_ARGS__)
#define _prefix_macros267(PREFIX,X,...) PREFIX ## X, _prefix_macros266(PREFIX,__VA_ARGS__)
#define _prefix_macros268(PREFIX,X,...) PREFIX ## X, _prefix_macros267(PREFIX,__VA_ARGS__)
#define _prefix_macros269(PREFIX,X,...) PREFIX ## X, _prefix_macros268(PREFIX,__VA_ARGS__)
#define _prefix_macros270(PREFIX,X,...) PREFIX ## X, _prefix_macros269(PREFIX,__VA_ARGS__)
#define _prefix_macros271(PREFIX,X,...) PREFIX ## X, _prefix_macros270(PREFIX,__VA_ARGS__)
#define _prefix_macros272(PREFIX,X,...) PREFIX ## X, _prefix_macros271(PREFIX,__VA_ARGS__)
#define _prefix_macros273(PREFIX,X,...) PREFIX ## X, _prefix_macros272(PREFIX,__VA_ARGS__)
#define _prefix_macros274(PREFIX,X,...) PREFIX ## X, _prefix_macros273(PREFIX,__VA_ARGS__)
#define _prefix_macros275(PREFIX,X,...) PREFIX ## X, _prefix_macros274(PREFIX,__VA_ARGS__)
#define _prefix_macros276(PREFIX,X,...) PREFIX ## X, _prefix_macros275(PREFIX,__VA_ARGS__)
#define _prefix_macros277(PREFIX,X,...) PREFIX ## X, _prefix_macros276(PREFIX,__VA_ARGS__)
#define _prefix_macros278(PREFIX,X,...) PREFIX ## X, _prefix_macros277(PREFIX,__VA_ARGS__)
#define _prefix_macros279(PREFIX,X,...) PREFIX ## X, _prefix_macros278(PREFIX,__VA_ARGS__)
#define _prefix_macros280(PREFIX,X,...) PREFIX ## X, _prefix_macros279(PREFIX,__VA_ARGS__)
#define _prefix_macros281(PREFIX,X,...) PREFIX ## X, _prefix_macros280(PREFIX,__VA_ARGS__)
#define _prefix_macros282(PREFIX,X,...) PREFIX ## X, _prefix_macros281(PREFIX,__VA_ARGS__)
#define _prefix_macros283(PREFIX,X,...) PREFIX ## X, _prefix_macros282(PREFIX,__VA_ARGS__)
#define _prefix_macros284(PREFIX,X,...) PREFIX ## X, _prefix_macros283(PREFIX,__VA_ARGS__)
#define _prefix_macros285(PREFIX,X,...) PREFIX ## X, _prefix_macros284(PREFIX,__VA_ARGS__)
#define _prefix_macros286(PREFIX,X,...) PREFIX ## X, _prefix_macros285(PREFIX,__VA_ARGS__)
#define _prefix_macros287(PREFIX,X,...) PREFIX ## X, _prefix_macros286(PREFIX,__VA_ARGS__)
#define _prefix_macros288(PREFIX,X,...) PREFIX ## X, _prefix_macros287(PREFIX,__VA_ARGS__)
#define _prefix_macros289(PREFIX,X,...) PREFIX ## X, _prefix_macros288(PREFIX,__VA_ARGS__)
#define _prefix_macros290(PREFIX,X,...) PREFIX ## X, _prefix_macros289(PREFIX,__VA_ARGS__)
#define _prefix_macros291(PREFIX,X,...) PREFIX ## X, _prefix_macros290(PREFIX,__VA_ARGS__)
#define _prefix_macros292(PREFIX,X,...) PREFIX ## X, _prefix_macros291(PREFIX,__VA_ARGS__)
#define _prefix_macros293(PREFIX,X,...) PREFIX ## X, _prefix_macros292(PREFIX,__VA_ARGS__)
#define _prefix_macros294(PREFIX,X,...) PREFIX ## X, _prefix_macros293(PREFIX,__VA_ARGS__)
#define _prefix_macros295(PREFIX,X,...) PREFIX ## X, _prefix_macros294(PREFIX,__VA_ARGS__)
#define _prefix_macros296(PREFIX,X,...) PREFIX ## X, _prefix_macros295(PREFIX,__VA_ARGS__)
#define _prefix_macros297(PREFIX,X,...) PREFIX ## X, _prefix_macros296(PREFIX,__VA_ARGS__)
#define _prefix_macros298(PREFIX,X,...) PREFIX ## X, _prefix_macros297(PREFIX,__VA_ARGS__)
#define _prefix_macros299(PREFIX,X,...) PREFIX ## X, _prefix_macros298(PREFIX,__VA_ARGS__)
#define _prefix_macros300(PREFIX,X,...) PREFIX ## X, _prefix_macros299(PREFIX,__VA_ARGS__)
#define _prefix_macros301(PREFIX,X,...) PREFIX ## X, _prefix_macros300(PREFIX,__VA_ARGS__)
#define _prefix_macros302(PREFIX,X,...) PREFIX ## X, _prefix_macros301(PREFIX,__VA_ARGS__)
#define _prefix_macros303(PREFIX,X,...) PREFIX ## X, _prefix_macros302(PREFIX,__VA_ARGS__)
#define _prefix_macros304(PREFIX,X,...) PREFIX ## X, _prefix_macros303(PREFIX,__VA_ARGS__)
#define _prefix_macros305(PREFIX,X,...) PREFIX ## X, _prefix_macros304(PREFIX,__VA_ARGS__)
#define _prefix_macros306(PREFIX,X,...) PREFIX ## X, _prefix_macros305(PREFIX,__VA_ARGS__)
#define _prefix_macros307(PREFIX,X,...) PREFIX ## X, _prefix_macros306(PREFIX,__VA_ARGS__)
#define _prefix_macros308(PREFIX,X,...) PREFIX ## X, _prefix_macros307(PREFIX,__VA_ARGS__)
#define _prefix_macros309(PREFIX,X,...) PREFIX ## X, _prefix_macros308(PREFIX,__VA_ARGS__)
#define _prefix_macros310(PREFIX,X,...) PREFIX ## X, _prefix_macros309(PREFIX,__VA_ARGS__)
#define _prefix_macros311(PREFIX,X,...) PREFIX ## X, _prefix_macros310(PREFIX,__VA_ARGS__)
#define _prefix_macros312(PREFIX,X,...) PREFIX ## X, _prefix_macros311(PREFIX,__VA_ARGS__)
#define _prefix_macros313(PREFIX,X,...) PREFIX ## X, _prefix_macros312(PREFIX,__VA_ARGS__)
#define _prefix_macros314(PREFIX,X,...) PREFIX ## X, _prefix_macros313(PREFIX,__VA_ARGS__)
#define _prefix_macros315(PREFIX,X,...) PREFIX ## X, _prefix_macros314(PREFIX,__VA_ARGS__)
#define _prefix_macros316(PREFIX,X,...) PREFIX ## X, _prefix_macros315(PREFIX,__VA_ARGS__)
#define _prefix_macros317(PREFIX,X,...) PREFIX ## X, _prefix_macros316(PREFIX,__VA_ARGS__)
#define _prefix_macros318(PREFIX,X,...) PREFIX ## X, _prefix_macros317(PREFIX,__VA_ARGS__)
#define _prefix_macros319(PREFIX,X,...) PREFIX ## X, _prefix_macros318(PREFIX,__VA_ARGS__)
#define _prefix_macros320(PREFIX,X,...) PREFIX ## X, _prefix_macros319(PREFIX,__VA_ARGS__)
#define _prefix_macros321(PREFIX,X,...) PREFIX ## X, _prefix_macros320(PREFIX,__VA_ARGS__)
#define _prefix_macros322(PREFIX,X,...) PREFIX ## X, _prefix_macros321(PREFIX,__VA_ARGS__)
#define _prefix_macros323(PREFIX,X,...) PREFIX ## X, _prefix_macros322(PREFIX,__VA_ARGS__)
#define _prefix_macros324(PREFIX,X,...) PREFIX ## X, _prefix_macros323(PREFIX,__VA_ARGS__)
#define _prefix_macros325(PREFIX,X,...) PREFIX ## X, _prefix_macros324(PREFIX,__VA_ARGS__)
#define _prefix_macros326(PREFIX,X,...) PREFIX ## X, _prefix_macros325(PREFIX,__VA_ARGS__)
#define _prefix_macros327(PREFIX,X,...) PREFIX ## X, _prefix_macros326(PREFIX,__VA_ARGS__)
#define _prefix_macros328(PREFIX,X,...) PREFIX ## X, _prefix_macros327(PREFIX,__VA_ARGS__)
#define _prefix_macros329(PREFIX,X,...) PREFIX ## X, _prefix_macros328(PREFIX,__VA_ARGS__)
#define _prefix_macros330(PREFIX,X,...) PREFIX ## X, _prefix_macros329(PREFIX,__VA_ARGS__)
#define _prefix_macros331(PREFIX,X,...) PREFIX ## X, _prefix_macros330(PREFIX,__VA_ARGS__)
#define _prefix_macros332(PREFIX,X,...) PREFIX ## X, _prefix_macros331(PREFIX,__VA_ARGS__)
#define _prefix_macros333(PREFIX,X,...) PREFIX ## X, _prefix_macros332(PREFIX,__VA_ARGS__)
#define _prefix_macros334(PREFIX,X,...) PREFIX ## X, _prefix_macros333(PREFIX,__VA_ARGS__)
#define _prefix_macros335(PREFIX,X,...) PREFIX ## X, _prefix_macros334(PREFIX,__VA_ARGS__)
#define _prefix_macros336(PREFIX,X,...) PREFIX ## X, _prefix_macros335(PREFIX,__VA_ARGS__)
#define _prefix_macros337(PREFIX,X,...) PREFIX ## X, _prefix_macros336(PREFIX,__VA_ARGS__)
#define _prefix_macros338(PREFIX,X,...) PREFIX ## X, _prefix_macros337(PREFIX,__VA_ARGS__)
#define _prefix_macros339(PREFIX,X,...) PREFIX ## X, _prefix_macros338(PREFIX,__VA_ARGS__)
#define _prefix_macros340(PREFIX,X,...) PREFIX ## X, _prefix_macros339(PREFIX,__VA_ARGS__)
#define _prefix_macros341(PREFIX,X,...) PREFIX ## X, _prefix_macros340(PREFIX,__VA_ARGS__)
#define _prefix_macros342(PREFIX,X,...) PREFIX ## X, _prefix_macros341(PREFIX,__VA_ARGS__)
#define _prefix_macros343(PREFIX,X,...) PREFIX ## X, _prefix_macros342(PREFIX,__VA_ARGS__)
#define _prefix_macros344(PREFIX,X,...) PREFIX ## X, _prefix_macros343(PREFIX,__VA_ARGS__)
#define _prefix_macros345(PREFIX,X,...) PREFIX ## X, _prefix_macros344(PREFIX,__VA_ARGS__)
#define _prefix_macros346(PREFIX,X,...) PREFIX ## X, _prefix_macros345(PREFIX,__VA_ARGS__)
#define _prefix_macros347(PREFIX,X,...) PREFIX ## X, _prefix_macros346(PREFIX,__VA_ARGS__)
#define _prefix_macros348(PREFIX,X,...) PREFIX ## X, _prefix_macros347(PREFIX,__VA_ARGS__)
#define _prefix_macros349(PREFIX,X,...) PREFIX ## X, _prefix_macros348(PREFIX,__VA_ARGS__)
#define _prefix_macros350(PREFIX,X,...) PREFIX ## X, _prefix_macros349(PREFIX,__VA_ARGS__)
#define _prefix_macros351(PREFIX,X,...) PREFIX ## X, _prefix_macros350(PREFIX,__VA_ARGS__)
#define _prefix_macros352(PREFIX,X,...) PREFIX ## X, _prefix_macros351(PREFIX,__VA_ARGS__)
#define _prefix_macros353(PREFIX,X,...) PREFIX ## X, _prefix_macros352(PREFIX,__VA_ARGS__)
#define _prefix_macros354(PREFIX,X,...) PREFIX ## X, _prefix_macros353(PREFIX,__VA_ARGS__)
#define _prefix_macros355(PREFIX,X,...) PREFIX ## X, _prefix_macros354(PREFIX,__VA_ARGS__)
#define _prefix_macros356(PREFIX,X,...) PREFIX ## X, _prefix_macros355(PREFIX,__VA_ARGS__)
#define _prefix_macros357(PREFIX,X,...) PREFIX ## X, _prefix_macros356(PREFIX,__VA_ARGS__)
#define _prefix_macros358(PREFIX,X,...) PREFIX ## X, _prefix_macros357(PREFIX,__VA_ARGS__)
#define _prefix_macros359(PREFIX,X,...) PREFIX ## X, _prefix_macros358(PREFIX,__VA_ARGS__)
#define _prefix_macros360(PREFIX,X,...) PREFIX ## X, _prefix_macros359(PREFIX,__VA_ARGS__)
#define _prefix_macros361(PREFIX,X,...) PREFIX ## X, _prefix_macros360(PREFIX,__VA_ARGS__)
#define _prefix_macros362(PREFIX,X,...) PREFIX ## X, _prefix_macros361(PREFIX,__VA_ARGS__)
#define _prefix_macros363(PREFIX,X,...) PREFIX ## X, _prefix_macros362(PREFIX,__VA_ARGS__)
#define _prefix_macros364(PREFIX,X,...) PREFIX ## X, _prefix_macros363(PREFIX,__VA_ARGS__)
#define _prefix_macros365(PREFIX,X,...) PREFIX ## X, _prefix_macros364(PREFIX,__VA_ARGS__)
#define _prefix_macros366(PREFIX,X,...) PREFIX ## X, _prefix_macros365(PREFIX,__VA_ARGS__)
#define _prefix_macros367(PREFIX,X,...) PREFIX ## X, _prefix_macros366(PREFIX,__VA_ARGS__)
#define _prefix_macros368(PREFIX,X,...) PREFIX ## X, _prefix_macros367(PREFIX,__VA_ARGS__)
#define _prefix_macros369(PREFIX,X,...) PREFIX ## X, _prefix_macros368(PREFIX,__VA_ARGS__)
#define _prefix_macros370(PREFIX,X,...) PREFIX ## X, _prefix_macros369(PREFIX,__VA_ARGS__)
#define _prefix_macros371(PREFIX,X,...) PREFIX ## X, _prefix_macros370(PREFIX,__VA_ARGS__)
#define _prefix_macros372(PREFIX,X,...) PREFIX ## X, _prefix_macros371(PREFIX,__VA_ARGS__)
#define _prefix_macros373(PREFIX,X,...) PREFIX ## X, _prefix_macros372(PREFIX,__VA_ARGS__)
#define _prefix_macros374(PREFIX,X,...) PREFIX ## X, _prefix_macros373(PREFIX,__VA_ARGS__)
#define _prefix_macros375(PREFIX,X,...) PREFIX ## X, _prefix_macros374(PREFIX,__VA_ARGS__)
#define _prefix_macros376(PREFIX,X,...) PREFIX ## X, _prefix_macros375(PREFIX,__VA_ARGS__)
#define _prefix_macros377(PREFIX,X,...) PREFIX ## X, _prefix_macros376(PREFIX,__VA_ARGS__)
#define _prefix_macros378(PREFIX,X,...) PREFIX ## X, _prefix_macros377(PREFIX,__VA_ARGS__)
#define _prefix_macros379(PREFIX,X,...) PREFIX ## X, _prefix_macros378(PREFIX,__VA_ARGS__)
#define _prefix_macros380(PREFIX,X,...) PREFIX ## X, _prefix_macros379(PREFIX,__VA_ARGS__)
#define _prefix_macros381(PREFIX,X,...) PREFIX ## X, _prefix_macros380(PREFIX,__VA_ARGS__)
#define _prefix_macros382(PREFIX,X,...) PREFIX ## X, _prefix_macros381(PREFIX,__VA_ARGS__)
#define _prefix_macros383(PREFIX,X,...) PREFIX ## X, _prefix_macros382(PREFIX,__VA_ARGS__)
#define _prefix_macros384(PREFIX,X,...) PREFIX ## X, _prefix_macros383(PREFIX,__VA_ARGS__)
#define _prefix_macros385(PREFIX,X,...) PREFIX ## X, _prefix_macros384(PREFIX,__VA_ARGS__)
#define _prefix_macros386(PREFIX,X,...) PREFIX ## X, _prefix_macros385(PREFIX,__VA_ARGS__)
#define _prefix_macros387(PREFIX,X,...) PREFIX ## X, _prefix_macros386(PREFIX,__VA_ARGS__)
#define _prefix_macros388(PREFIX,X,...) PREFIX ## X, _prefix_macros387(PREFIX,__VA_ARGS__)
#define _prefix_macros389(PREFIX,X,...) PREFIX ## X, _prefix_macros388(PREFIX,__VA_ARGS__)
#define _prefix_macros390(PREFIX,X,...) PREFIX ## X, _prefix_macros389(PREFIX,__VA_ARGS__)
#define _prefix_macros391(PREFIX,X,...) PREFIX ## X, _prefix_macros390(PREFIX,__VA_ARGS__)
#define _prefix_macros392(PREFIX,X,...) PREFIX ## X, _prefix_macros391(PREFIX,__VA_ARGS__)
#define _prefix_macros393(PREFIX,X,...) PREFIX ## X, _prefix_macros392(PREFIX,__VA_ARGS__)
#define _prefix_macros394(PREFIX,X,...) PREFIX ## X, _prefix_macros393(PREFIX,__VA_ARGS__)
#define _prefix_macros395(PREFIX,X,...) PREFIX ## X, _prefix_macros394(PREFIX,__VA_ARGS__)
#define _prefix_macros396(PREFIX,X,...) PREFIX ## X, _prefix_macros395(PREFIX,__VA_ARGS__)
#define _prefix_macros397(PREFIX,X,...) PREFIX ## X, _prefix_macros396(PREFIX,__VA_ARGS__)
#define _prefix_macros398(PREFIX,X,...) PREFIX ## X, _prefix_macros397(PREFIX,__VA_ARGS__)
#define _prefix_macros399(PREFIX,X,...) PREFIX ## X, _prefix_macros398(PREFIX,__VA_ARGS__)
#define _prefix_macros400(PREFIX,X,...) PREFIX ## X, _prefix_macros399(PREFIX,__VA_ARGS__)
#define _prefix_macros401(PREFIX,X,...) PREFIX ## X, _prefix_macros400(PREFIX,__VA_ARGS__)
#define _prefix_macros402(PREFIX,X,...) PREFIX ## X, _prefix_macros401(PREFIX,__VA_ARGS__)
#define _prefix_macros403(PREFIX,X,...) PREFIX ## X, _prefix_macros402(PREFIX,__VA_ARGS__)
#define _prefix_macros404(PREFIX,X,...) PREFIX ## X, _prefix_macros403(PREFIX,__VA_ARGS__)
#define _prefix_macros405(PREFIX,X,...) PREFIX ## X, _prefix_macros404(PREFIX,__VA_ARGS__)
#define _prefix_macros406(PREFIX,X,...) PREFIX ## X, _prefix_macros405(PREFIX,__VA_ARGS__)
#define _prefix_macros407(PREFIX,X,...) PREFIX ## X, _prefix_macros406(PREFIX,__VA_ARGS__)
#define _prefix_macros408(PREFIX,X,...) PREFIX ## X, _prefix_macros407(PREFIX,__VA_ARGS__)
#define _prefix_macros409(PREFIX,X,...) PREFIX ## X, _prefix_macros408(PREFIX,__VA_ARGS__)
#define _prefix_macros410(PREFIX,X,...) PREFIX ## X, _prefix_macros409(PREFIX,__VA_ARGS__)
#define _prefix_macros411(PREFIX,X,...) PREFIX ## X, _prefix_macros410(PREFIX,__VA_ARGS__)
#define _prefix_macros412(PREFIX,X,...) PREFIX ## X, _prefix_macros411(PREFIX,__VA_ARGS__)
#define _prefix_macros413(PREFIX,X,...) PREFIX ## X, _prefix_macros412(PREFIX,__VA_ARGS__)
#define _prefix_macros414(PREFIX,X,...) PREFIX ## X, _prefix_macros413(PREFIX,__VA_ARGS__)
#define _prefix_macros415(PREFIX,X,...) PREFIX ## X, _prefix_macros414(PREFIX,__VA_ARGS__)
#define _prefix_macros416(PREFIX,X,...) PREFIX ## X, _prefix_macros415(PREFIX,__VA_ARGS__)
#define _prefix_macros417(PREFIX,X,...) PREFIX ## X, _prefix_macros416(PREFIX,__VA_ARGS__)
#define _prefix_macros418(PREFIX,X,...) PREFIX ## X, _prefix_macros417(PREFIX,__VA_ARGS__)
#define _prefix_macros419(PREFIX,X,...) PREFIX ## X, _prefix_macros418(PREFIX,__VA_ARGS__)
#define _prefix_macros420(PREFIX,X,...) PREFIX ## X, _prefix_macros419(PREFIX,__VA_ARGS__)
#define _prefix_macros421(PREFIX,X,...) PREFIX ## X, _prefix_macros420(PREFIX,__VA_ARGS__)
#define _prefix_macros422(PREFIX,X,...) PREFIX ## X, _prefix_macros421(PREFIX,__VA_ARGS__)
#define _prefix_macros423(PREFIX,X,...) PREFIX ## X, _prefix_macros422(PREFIX,__VA_ARGS__)
#define _prefix_macros424(PREFIX,X,...) PREFIX ## X, _prefix_macros423(PREFIX,__VA_ARGS__)
#define _prefix_macros425(PREFIX,X,...) PREFIX ## X, _prefix_macros424(PREFIX,__VA_ARGS__)
#define _prefix_macros426(PREFIX,X,...) PREFIX ## X, _prefix_macros425(PREFIX,__VA_ARGS__)
#define _prefix_macros427(PREFIX,X,...) PREFIX ## X, _prefix_macros426(PREFIX,__VA_ARGS__)
#define _prefix_macros428(PREFIX,X,...) PREFIX ## X, _prefix_macros427(PREFIX,__VA_ARGS__)
#define _prefix_macros429(PREFIX,X,...) PREFIX ## X, _prefix_macros428(PREFIX,__VA_ARGS__)
#define _prefix_macros430(PREFIX,X,...) PREFIX ## X, _prefix_macros429(PREFIX,__VA_ARGS__)
#define _prefix_macros431(PREFIX,X,...) PREFIX ## X, _prefix_macros430(PREFIX,__VA_ARGS__)
#define _prefix_macros432(PREFIX,X,...) PREFIX ## X, _prefix_macros431(PREFIX,__VA_ARGS__)
#define _prefix_macros433(PREFIX,X,...) PREFIX ## X, _prefix_macros432(PREFIX,__VA_ARGS__)
#define _prefix_macros434(PREFIX,X,...) PREFIX ## X, _prefix_macros433(PREFIX,__VA_ARGS__)
#define _prefix_macros435(PREFIX,X,...) PREFIX ## X, _prefix_macros434(PREFIX,__VA_ARGS__)
#define _prefix_macros436(PREFIX,X,...) PREFIX ## X, _prefix_macros435(PREFIX,__VA_ARGS__)
#define _prefix_macros437(PREFIX,X,...) PREFIX ## X, _prefix_macros436(PREFIX,__VA_ARGS__)
#define _prefix_macros438(PREFIX,X,...) PREFIX ## X, _prefix_macros437(PREFIX,__VA_ARGS__)
#define _prefix_macros439(PREFIX,X,...) PREFIX ## X, _prefix_macros438(PREFIX,__VA_ARGS__)
#define _prefix_macros440(PREFIX,X,...) PREFIX ## X, _prefix_macros439(PREFIX,__VA_ARGS__)
#define _prefix_macros441(PREFIX,X,...) PREFIX ## X, _prefix_macros440(PREFIX,__VA_ARGS__)
#define _prefix_macros442(PREFIX,X,...) PREFIX ## X, _prefix_macros441(PREFIX,__VA_ARGS__)
#define _prefix_macros443(PREFIX,X,...) PREFIX ## X, _prefix_macros442(PREFIX,__VA_ARGS__)
#define _prefix_macros444(PREFIX,X,...) PREFIX ## X, _prefix_macros443(PREFIX,__VA_ARGS__)
#define _prefix_macros445(PREFIX,X,...) PREFIX ## X, _prefix_macros444(PREFIX,__VA_ARGS__)
#define _prefix_macros446(PREFIX,X,...) PREFIX ## X, _prefix_macros445(PREFIX,__VA_ARGS__)
#define _prefix_macros447(PREFIX,X,...) PREFIX ## X, _prefix_macros446(PREFIX,__VA_ARGS__)
#define _prefix_macros448(PREFIX,X,...) PREFIX ## X, _prefix_macros447(PREFIX,__VA_ARGS__)
#define _prefix_macros449(PREFIX,X,...) PREFIX ## X, _prefix_macros448(PREFIX,__VA_ARGS__)
#define _prefix_macros450(PREFIX,X,...) PREFIX ## X, _prefix_macros449(PREFIX,__VA_ARGS__)
#define _prefix_macros451(PREFIX,X,...) PREFIX ## X, _prefix_macros450(PREFIX,__VA_ARGS__)
#define _prefix_macros452(PREFIX,X,...) PREFIX ## X, _prefix_macros451(PREFIX,__VA_ARGS__)
#define _prefix_macros453(PREFIX,X,...) PREFIX ## X, _prefix_macros452(PREFIX,__VA_ARGS__)
#define _prefix_macros454(PREFIX,X,...) PREFIX ## X, _prefix_macros453(PREFIX,__VA_ARGS__)
#define _prefix_macros455(PREFIX,X,...) PREFIX ## X, _prefix_macros454(PREFIX,__VA_ARGS__)
#define _prefix_macros456(PREFIX,X,...) PREFIX ## X, _prefix_macros455(PREFIX,__VA_ARGS__)
#define _prefix_macros457(PREFIX,X,...) PREFIX ## X, _prefix_macros456(PREFIX,__VA_ARGS__)
#define _prefix_macros458(PREFIX,X,...) PREFIX ## X, _prefix_macros457(PREFIX,__VA_ARGS__)
#define _prefix_macros459(PREFIX,X,...) PREFIX ## X, _prefix_macros458(PREFIX,__VA_ARGS__)
#define _prefix_macros460(PREFIX,X,...) PREFIX ## X, _prefix_macros459(PREFIX,__VA_ARGS__)
#define _prefix_macros461(PREFIX,X,...) PREFIX ## X, _prefix_macros460(PREFIX,__VA_ARGS__)
#define _prefix_macros462(PREFIX,X,...) PREFIX ## X, _prefix_macros461(PREFIX,__VA_ARGS__)
#define _prefix_macros463(PREFIX,X,...) PREFIX ## X, _prefix_macros462(PREFIX,__VA_ARGS__)
#define _prefix_macros464(PREFIX,X,...) PREFIX ## X, _prefix_macros463(PREFIX,__VA_ARGS__)
#define _prefix_macros465(PREFIX,X,...) PREFIX ## X, _prefix_macros464(PREFIX,__VA_ARGS__)
#define _prefix_macros466(PREFIX,X,...) PREFIX ## X, _prefix_macros465(PREFIX,__VA_ARGS__)
#define _prefix_macros467(PREFIX,X,...) PREFIX ## X, _prefix_macros466(PREFIX,__VA_ARGS__)
#define _prefix_macros468(PREFIX,X,...) PREFIX ## X, _prefix_macros467(PREFIX,__VA_ARGS__)
#define _prefix_macros469(PREFIX,X,...) PREFIX ## X, _prefix_macros468(PREFIX,__VA_ARGS__)
#define _prefix_macros470(PREFIX,X,...) PREFIX ## X, _prefix_macros469(PREFIX,__VA_ARGS__)
#define _prefix_macros471(PREFIX,X,...) PREFIX ## X, _prefix_macros470(PREFIX,__VA_ARGS__)
#define _prefix_macros472(PREFIX,X,...) PREFIX ## X, _prefix_macros471(PREFIX,__VA_ARGS__)
#define _prefix_macros473(PREFIX,X,...) PREFIX ## X, _prefix_macros472(PREFIX,__VA_ARGS__)
#define _prefix_macros474(PREFIX,X,...) PREFIX ## X, _prefix_macros473(PREFIX,__VA_ARGS__)
#define _prefix_macros475(PREFIX,X,...) PREFIX ## X, _prefix_macros474(PREFIX,__VA_ARGS__)
#define _prefix_macros476(PREFIX,X,...) PREFIX ## X, _prefix_macros475(PREFIX,__VA_ARGS__)
#define _prefix_macros477(PREFIX,X,...) PREFIX ## X, _prefix_macros476(PREFIX,__VA_ARGS__)
#define _prefix_macros478(PREFIX,X,...) PREFIX ## X, _prefix_macros477(PREFIX,__VA_ARGS__)
#define _prefix_macros479(PREFIX,X,...) PREFIX ## X, _prefix_macros478(PREFIX,__VA_ARGS__)
#define _prefix_macros480(PREFIX,X,...) PREFIX ## X, _prefix_macros479(PREFIX,__VA_ARGS__)
#define _prefix_macros481(PREFIX,X,...) PREFIX ## X, _prefix_macros480(PREFIX,__VA_ARGS__)
#define _prefix_macros482(PREFIX,X,...) PREFIX ## X, _prefix_macros481(PREFIX,__VA_ARGS__)
#define _prefix_macros483(PREFIX,X,...) PREFIX ## X, _prefix_macros482(PREFIX,__VA_ARGS__)
#define _prefix_macros484(PREFIX,X,...) PREFIX ## X, _prefix_macros483(PREFIX,__VA_ARGS__)
#define _prefix_macros485(PREFIX,X,...) PREFIX ## X, _prefix_macros484(PREFIX,__VA_ARGS__)
#define _prefix_macros486(PREFIX,X,...) PREFIX ## X, _prefix_macros485(PREFIX,__VA_ARGS__)
#define _prefix_macros487(PREFIX,X,...) PREFIX ## X, _prefix_macros486(PREFIX,__VA_ARGS__)
#define _prefix_macros488(PREFIX,X,...) PREFIX ## X, _prefix_macros487(PREFIX,__VA_ARGS__)
#define _prefix_macros489(PREFIX,X,...) PREFIX ## X, _prefix_macros488(PREFIX,__VA_ARGS__)
#define _prefix_macros490(PREFIX,X,...) PREFIX ## X, _prefix_macros489(PREFIX,__VA_ARGS__)
#define _prefix_macros491(PREFIX,X,...) PREFIX ## X, _prefix_macros490(PREFIX,__VA_ARGS__)
#define _prefix_macros492(PREFIX,X,...) PREFIX ## X, _prefix_macros491(PREFIX,__VA_ARGS__)
#define _prefix_macros493(PREFIX,X,...) PREFIX ## X, _prefix_macros492(PREFIX,__VA_ARGS__)
#define _prefix_macros494(PREFIX,X,...) PREFIX ## X, _prefix_macros493(PREFIX,__VA_ARGS__)
#define _prefix_macros495(PREFIX,X,...) PREFIX ## X, _prefix_macros494(PREFIX,__VA_ARGS__)
#define _prefix_macros496(PREFIX,X,...) PREFIX ## X, _prefix_macros495(PREFIX,__VA_ARGS__)
#define _prefix_macros497(PREFIX,X,...) PREFIX ## X, _prefix_macros496(PREFIX,__VA_ARGS__)
#define _prefix_macros498(PREFIX,X,...) PREFIX ## X, _prefix_macros497(PREFIX,__VA_ARGS__)
#define _prefix_macros499(PREFIX,X,...) PREFIX ## X, _prefix_macros498(PREFIX,__VA_ARGS__)
#define _prefix_macros500(PREFIX,X,...) PREFIX ## X, _prefix_macros499(PREFIX,__VA_ARGS__)
#define _prefix_macros501(PREFIX,X,...) PREFIX ## X, _prefix_macros500(PREFIX,__VA_ARGS__)
#define _prefix_macros502(PREFIX,X,...) PREFIX ## X, _prefix_macros501(PREFIX,__VA_ARGS__)
#define _prefix_macros503(PREFIX,X,...) PREFIX ## X, _prefix_macros502(PREFIX,__VA_ARGS__)
#define _prefix_macros504(PREFIX,X,...) PREFIX ## X, _prefix_macros503(PREFIX,__VA_ARGS__)
#define _prefix_macros505(PREFIX,X,...) PREFIX ## X, _prefix_macros504(PREFIX,__VA_ARGS__)
#define _prefix_macros506(PREFIX,X,...) PREFIX ## X, _prefix_macros505(PREFIX,__VA_ARGS__)
#define _prefix_macros507(PREFIX,X,...) PREFIX ## X, _prefix_macros506(PREFIX,__VA_ARGS__)
#define _prefix_macros508(PREFIX,X,...) PREFIX ## X, _prefix_macros507(PREFIX,__VA_ARGS__)
#define _prefix_macros509(PREFIX,X,...) PREFIX ## X, _prefix_macros508(PREFIX,__VA_ARGS__)
#define _prefix_macros510(PREFIX,X,...) PREFIX ## X, _prefix_macros509(PREFIX,__VA_ARGS__)
#define _prefix_macros511(PREFIX,X,...) PREFIX ## X, _prefix_macros510(PREFIX,__VA_ARGS__)
#define _prefix_macros512(PREFIX,X,...) PREFIX ## X, _prefix_macros511(PREFIX,__VA_ARGS__)

/* is 512 enough? why a power of two?! ... */
#define NUMBER_OF_PREFIX_MACROS_ARGUMENTS 512

#define Prefix_macros(PREFIX,...) _paste(_prefix_macros,NARGS(__VA_ARGS__)) (PREFIX,__VA_ARGS__)

#endif // BINARY_C_MACRO_MACROS_H
