#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "memory_alignment_checks.h"

/*
 * Deep copy a stardata struct from src to dest
 *
 * Returns NULL on failure.
 *
 * Note that dest should be made by calling new_stardata()
 * so that it has its sub-structures filled.
 */

#undef Cprint
#define Cprint(...)
//#define Cprint(...) printf("DEEPCOPY: ");printf(__VA_ARGS__);fflush(NULL);

void * deep_copy_stardata(struct stardata_t * const src,
                          struct stardata_t * const dest)
{
    Cprint("\ndeep copy %p to %p\n",
           (void*)src,
           (void*)dest);
    if(src==NULL || dest==NULL) return NULL;

    struct stardata_t * stardata Maybe_unused = dest;
    Star_number k Maybe_unused;

    /*
     * Save pointers and other information that needs
     * to be restored
     */
    struct star_pointers_t * star_pointers[NUMBER_OF_STARS];
    Number_of_stars_Starloop(k)
    {
        star_pointers[k] = save_star_pointers(&stardata->star[k]);
    }
#ifdef ORBITING_OBJECTS
    /*
     * Free existing orbiting objects
     */
    for(size_t i=0; i<dest->common.n_orbiting_objects; i++)
    {
        Safe_free(dest->common.orbiting_object[i].name);
    }
    Safe_free(dest->common.orbiting_object);
#endif // ORBITING_OBJECTS

    /*
     * Copy the contents
     */
    memcpy(dest,src,sizeof(struct stardata_t));

    /*
     * Don't keep pre_events_stardata
     */
    dest->pre_events_stardata = NULL;

#ifdef MINT
    Cprint("post-memcpy old mint %p, new mint %p (should be same)\n",
           (void*)src->star[0].mint,
           (void*)dest->star[0].mint);
#endif

    /*
     * Duplicate file pointers?
     */
#ifdef __DUPLICATE_FILE_POINTERS
    dest->model.log_fp = Duplicate_writable_file_pointer(src->model.log_fp);
    dest->model.api_log_fp = Duplicate_writable_file_pointer(src->model.api_log_fp);
#ifdef LOG_SUPERNOVAE
    dest->common.snfp = Duplicate_writable_file_pointer(src->common.snfp);
#endif // LOG_SUPERNOVAE
#ifdef DETAILED_LOG
    dest->common.detailed_log_fp = Duplicate_writable_file_pointer(src->common.detailed_log_fp);
#endif // DETAILED_LOG
#endif // __DUPLICATE_FILE_POINTERS

    /*
     * Restore the dest pointers prior to copying into them
     */
    stardata = dest;

    /*
     * restore star pointers
     */
    Number_of_stars_Starloop(k)
    {
        restore_star_pointers(&stardata->star[k],
                              (struct star_pointers_t * const)star_pointers[k]);
        Safe_free(star_pointers[k]);
    }

    void * const ret = copy_stars(src,
                                  dest);

    if(ret == NULL)
    {
        Exit_binary_c(BINARY_C_MALLOC_FAILED,
                      "Pointers are equal after copy star: failure");
    }


#ifdef ORBITING_OBJECTS
    /*
     * Copy orbiting-objects data, taking care with
     * their names
     */
    if(src->common.n_orbiting_objects > 0)
    {
        const size_t orbiting_objects_size =
            sizeof(struct orbiting_object_t) * src->common.n_orbiting_objects;

        dest->common.orbiting_object =
            Malloc(orbiting_objects_size);

        memcpy(dest->common.orbiting_object,
               src->common.orbiting_object,
               orbiting_objects_size);

        for(size_t i=0; i<src->common.n_orbiting_objects; i++)
        {
            const size_t namesize =
                sizeof(char) * (1+strlen(src->common.orbiting_object[i].name));
            dest->common.orbiting_object[i].name = Malloc(namesize);
            memcpy(dest->common.orbiting_object[i].name,
                   src->common.orbiting_object[i].name,
                   namesize);
        }
    }
    if(src->common.zero_age.n_orbiting_objects > 0)
    {
        const size_t stacksize = src->common.zero_age.n_orbiting_objects *
            sizeof(struct orbiting_object_t *);
        if(dest->common.zero_age.orbiting_object == NULL)
        {
            dest->common.zero_age.orbiting_object =
                Malloc(stacksize);
        }
        memcpy(dest->common.zero_age.orbiting_object,
               src->common.zero_age.orbiting_object,
               stacksize);
    }
#endif//ORBITING_OBJECTS

#ifdef DEPRECATED
    /*
     * Deep copy the pointers' contents
     */
#ifdef MINT
    Number_of_stars_Starloop(k)
    {
        struct star_t * const s = &src->star[k];
        struct star_t * const d = &dest->star[k];

        if(s->mint != NULL &&
           d->mint != NULL)
        {
            /* main MINT struct */
            memcpy(d->mint,
                   s->mint,
                   sizeof(struct mint_t));

            /* shell contents */
            if(!MINT_has_shells(s))
            {
                Cprint("MINT has no shells (s %p %d)\n",
                       (void*)s->mint->shells,
                       s->mint->nshells);
                Safe_free(d->mint->shells);
            }
            else if(d->mint->nshells != s->mint->nshells)
            {
                /*
                 * Size has changed: requires Realloc
                 */
                const size_t shellssize =
                    sizeof(struct mint_shell_t)*s->mint->nshells;
                Cprint("Realloc d = %p -> shells to size %zu (n was %d, will be %d)\n",
                       (void*)d,
                       shellssize,
                       d->mint->nshells,
                       s->mint->nshells);
                d->mint->shells =
                    Realloc(d->mint->shells,shellssize);
                Cprint("realloc gave %p\n",(void*)d->mint->shells);fflush(NULL);
                memcpy(d->mint->shells,
                       s->mint->shells,
                       shellssize);
            }
        }
        Cprint("post-copy_star %d old mint %p (%p), new mint %p (%p) (should be different)\n",
               k,
               (void*)s->mint,
               (void*)s->mint->shells,
               (void*)d->mint,
               (void*)d->mint->shells
            );
    }
#endif
#endif//DEPRECATED
    return ret;
}
