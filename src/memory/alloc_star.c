#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "memory_alignment_checks.h"

/*
 * Allocate a star struct and any memory it
 * requries
 */
Malloc_like
struct star_t * alloc_star(struct stardata_t * const Restrict stardata Maybe_unused)
{
    struct star_t * const star = Calloc(1,sizeof(struct star_t));

    alloc_star_contents(stardata->preferences,
                        star);

    return star;
}
