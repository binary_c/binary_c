#include "binary_c.h"

void free_stardata_stack(struct stardata_t * Restrict const stardata)
{
    /*
     * Free the stardata stack
     */
    for(unsigned int i=0;i<stardata->n_stardata_stack;i++)
    {
        free_stardata(&stardata->stardata_stack[i]);
    }
    Safe_free(stardata->stardata_stack);
}
