#include "../binary_c.h"
No_empty_translation_unit_warning;

#define Argstore_free(P) Safe_free(store->argstore->P);

void free_argstore(struct store_t * const store)
{
    /*
     * Free command line arguments table
     */
    if(store->argstore->cmd_line_args_on_parse != NULL)
    {
        for(int parse_number=0; parse_number<ARG_NUM_PARSES; parse_number++)
        {
            Argstore_free(cmd_line_args_on_parse[parse_number]);
        }
        Argstore_free(cmd_line_args_on_parse);
    }
    Argstore_free(n_cmd_line_args_on_parse);

    {
        for(size_t i=0; i<store->argstore->arg_count; i++)
        {
            if(store->argstore->cmd_line_args[i].nargpairs > 0)
            {
                for(size_t j=0; j<store->argstore->cmd_line_args[i].nargpairs; j++)
                {
                    Argstore_free(cmd_line_args[i].argpairs[j].string);
                }
            }
            Argstore_free(cmd_line_args[i].argpairs);
            Argstore_free(cmd_line_args[i].pairs);
        }
        Argstore_free(cmd_line_args);
        Safe_free(store->argstore);
    }
}
