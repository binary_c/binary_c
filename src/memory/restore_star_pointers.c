#include "../binary_c.h"
No_empty_translation_unit_warning;

void restore_star_pointers(struct star_t * const star,
                           struct star_pointers_t * const p)
{
    /*
     * Restore star pointers in star
     */
#ifdef BSE
    star->bse = p->bse;
#endif // BSE

#ifdef MINT
    star->mint = p->mint;
    if(p->mint)
    {
        star->mint->shells = p->mint_shells;
        star->mint->nshells = p->mint_nshells;
    }
#endif // MINT

#if defined NUCSYN && \
    defined NUCSYN_ID_SOURCES
    memcpy(star->oldsource, p->oldsource, sizeof(Abundance*)*SOURCE_NUMBER);
    memcpy(star->Xsource, p->Xsource, sizeof(Abundance*)*SOURCE_NUMBER);
#endif

}
