#include "../binary_c.h"
No_empty_translation_unit_warning;


#define Tmp_clean(POINTER,SIZE)        \
    memset(tmpstore->POINTER,0,(SIZE))

void clean_tmpstore(struct tmpstore_t * Restrict const tmpstore)
{
    /*
     * Clean the contents of the temporary data store (tmpstore)
     */
    if(tmpstore)
    {
        free_difflogstack(tmpstore->logstack);
#ifdef KICK_CDF_TABLE
        Delete_data_table(tmpstore->cdf_table);
#endif
        Tmp_clean(error_buffer,
                  sizeof(char)*BUFFERED_PRINTF_ERROR_BUFFER_SIZE);

        Tmp_clean(raw_buffer,
                  tmpstore->raw_buffer_size);
        tmpstore->raw_buffer_size = 0;

        Tmp_clean(stellar_structure_newstar,sizeof(struct star_t));
        Tmp_clean(stellar_evolution_newstar,sizeof(struct star_t));
#ifdef NUCSYN
        Tmp_clean(Xwind_gain,ISOTOPE_MEMSIZE);
#ifdef NUCSYN_NOVAE
        Tmp_clean(Xnovae,ISOTOPE_MEMSIZE);
#endif
        {
            Star_number k;
            Number_of_stars_Starloop(k)
            {
                Tmp_clean(nXacc[k],ISOTOPE_MEMSIZE);
                Tmp_clean(nXenv[k],ISOTOPE_MEMSIZE);
            }
        }

#endif//NUCSYN
#ifdef WTTS_LOG
        {
            unsigned int i;
            for(i=0;i<=1;i++)
            {
                Safe_fclose(tmpstore->fp_star[i]);
            }
            Safe_fclose(tmpstore->fp_sys);
        }
#endif // WTTS_LOG

    }
}
