#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef STELLAR_POPULATIONS_ENSEMBLE

void make_ensemble_cdict(struct stardata_t * const Restrict stardata)
{
    /*
     * Make a new ensemble cdict
     */
    if(stardata->model.ensemble_cdict == NULL)
    {
        if(stardata->preferences->ensemble_defer &&
           stardata->persistent_data->ensemble_cdict != NULL)
        {
            /*
             * We have a cdict in the persistent store,
             * use it.
             */
            stardata->model.ensemble_cdict =
                stardata->persistent_data->ensemble_cdict;
        }
        else
        {
            CDict_new_from(stardata->model.ensemble_cdict);

            Dprint("made new ensemble cdict at %p\n",
                   (void*)stardata->model.ensemble_cdict);

            /*
             * activate memory monitoring?
             * beware: it is slow!
             */
            //CDict_activate_memory_monitoring(stardata->model.ensemble_cdict);

            /*
             * Do not force maps because we want to save RAM
             * at the expense of (slightly) slower code.
             */
            stardata->model.ensemble_cdict->force_maps = FALSE;

            if(stardata->model.ensemble_cdict == NULL)
            {
                Exit_binary_c(BINARY_C_CDICT_ERROR,
                              "libcdict failed to make a new cdict\n");
            }

            if(stardata->preferences->ensemble_defer == TRUE)
            {
                /*
                 * Save a pointer to the cdict in the persistent store
                 */
                stardata->persistent_data->ensemble_cdict =
                    stardata->model.ensemble_cdict;
            }

            /*
             * Perhaps fill with dummy data (e.g. to
             * test memory use)
             */
            if(stardata->preferences->ensemble_dummy_n > 0)
            {
                for(int i=0;i<stardata->preferences->ensemble_dummy_n;i++)
                {
                    CDict_nest(stardata->model.ensemble_cdict,
                               random_number(stardata,NULL),1,
                               "ensemble","dummy data");
                }
            }
        }
        stardata->model.ensemble_cdict->vb = 0;

        /*
        CDict_stats(stardata->model.ensemble_cdict);
        struct cdict_stats_t * stats = Calloc(1,sizeof(struct cdict_stats_t));
        stardata->model.ensemble_cdict->stats = stats;
        */
        stardata->model.ensemble_cdict->max_size = 0; /* ignored if 0 */
        stardata->model.ensemble_cdict->error_handler = &ensemble_cdict_error_handler;
        stardata->model.ensemble_cdict->error_data = stardata;

    }
}
#endif // STELLAR_POPULATIONS_ENSEMBLE
