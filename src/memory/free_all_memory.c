#include "binary_c.h"
No_empty_translation_unit_warning;

/*
 * Wrapper function to free all binary_c's memory
 */
void free_all_memory(struct stardata_t ** Restrict const sp)
{
    free_memory(sp,TRUE,TRUE,TRUE,TRUE,TRUE);
}
