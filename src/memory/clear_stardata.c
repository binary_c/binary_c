#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Clear a stardata struct, but preserve pointers e.g.
 * to the MINT structs, and clear them too.
 */
void clear_stardata(struct stardata_t * const Restrict stardata)
{
    Star_number k;
#ifdef MINT
    struct mint_t * mint_pointer[NUMBER_OF_STARS];
    Starloop(k)
    {
        mint_pointer[k] = stardata->star[k].mint;
    }
    MINT_clear(stardata);
#endif //MINT
#ifdef BSE
    struct BSE_data_t * bse_pointer[NUMBER_OF_STARS];
    Starloop(k)
    {
        bse_pointer[k] = stardata->star[k].bse;
    }
#endif//BSE

    memset(stardata,0,sizeof(struct stardata_t));

#ifdef MINT
    Starloop(k)
    {
        stardata->star[k].mint = mint_pointer[k];
    }
#endif //MINT
#ifdef BSE
    Starloop(k)
    {
        stardata->star[k].bse = bse_pointer[k];
    }
#endif
}
