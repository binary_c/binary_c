#include "../binary_c.h"
No_empty_translation_unit_warning;


float Pure_function diff_struct_pc(void * const a,
                                   void * const b,
                                   const size_t len)
{
    /*
     * Compare memory regions a and b, of length len, and return a
     * % difference.
     */
    size_t i, same Maybe_unused = 0, different = 0;
    for(i=0;i<len;i++)
    {
        if(*(((char*)a)+i) == *(((char*)b)+i))
        {
            same++;
        }
        else
        {
            different++;
        }
    }
    return 100.0*((float)different)/((float)(len));
}
