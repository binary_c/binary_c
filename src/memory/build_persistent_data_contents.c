#include "../binary_c.h"
No_empty_translation_unit_warning;

static int _rinterpolate_errfunc(void * data,
                                 const int errnum,
                                 const char * const format,
                                 va_list args);
static int _rinterpolate_errfunc(void * data,
                                 const int errnum,
                                 const char * const format,
                                 va_list args)
{
    /*
     * Error handler for librinterpolate
     */
    struct stardata_t * const stardata = (struct stardata_t * const) data;
    vExit_binary_c(BINARY_C_INTERPOLATION_ERROR,
                   format,
                   args,
                   "librinterpolate error %d",
                   errnum
        );
}

void build_persistent_data_contents(struct stardata_t * const stardata)
{
    if(stardata->persistent_data->rinterpolate_data == NULL)
    {
        rinterpolate_alloc_dataspace(&stardata->persistent_data->rinterpolate_data);
        stardata->persistent_data->rinterpolate_data->errdata = stardata;
        stardata->persistent_data->rinterpolate_data->error_handler =
            _rinterpolate_errfunc;
    }
#ifdef MINT
    stardata->persistent_data->MINT_MS_result = NULL;
    stardata->persistent_data->MINT_MS_result_chebyshev = NULL;
    stardata->persistent_data->MINT_MS_result_conv = NULL;
#endif//MINT

    Peters_grav_wave_merger_time(stardata,1.0,1.0,1.0,0.0);
#ifdef COMENV_WANG2016
    common_envelope_wang2016(1.0,100.0,TPAGB,stardata);
#endif // COMENV_WANG2016
#ifdef NUCSYN
    nucsyn_sn_limongi_chieffi_2018(stardata,NULL,0.0,0.0,0.0,NULL);
#endif // NUCSYN
}
