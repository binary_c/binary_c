
#pragma once
#ifndef DISC_H
#define DISC_H
#ifdef DISCS

/*
 * Header file for the binary_c disc library
 */


#include "disc_parameters.h"
#include "disc_macros.h"
#include "disc_function_macros.h"
#include "disc_power_laws.h"
#include "disc_prototypes.h"
#include "disc_thermal_zones.h"

#endif // DISCS
#endif // DISC_H
