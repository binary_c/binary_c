#include "../binary_c.h"

No_empty_translation_unit_warning;

#ifdef DISCS

int disc_hottest_zone_at_radius(struct disc_thermal_zone_t * const zones,
                                const double radius,
                                const Boolean all,
                                double * Thottest_p)
{
    /*
     * Return the counter of the hottest zone at
     * given radius, and set *Thottest_p to this temperature
     * if Thottest_p != NULL.
     *
     * Returns -1 on failure (note that the return type
     * is int, not Disc_zone_counter which is an
     * unsigned int).
     *
     * Invalid zones are skipped, unless all == TRUE.
     */
    double Thottest = 0.0;
    Disc_zone_counter i;
    int ihottest = -1;

    for(i=0;i<DISCS_MAX_N_ZONES;i++)
    {
        if(all == TRUE ||
           zones[i].valid == TRUE)
        {
            const double T = power_law(&zones[i].Tlaw,radius);
            //printf("T[%u](%g) = %g\n",i,radius,T);
            if(T > Thottest)
            {
                ihottest = i;
                Thottest = T;
            }
        }
        /*
        else
        {
            printf("Zkip zone %u\n",i);
        }
        */
    }
    if(ihottest!=-1 && Thottest_p != NULL)
    {
        *Thottest_p = Thottest;
    }
    return ihottest;
}

#endif //DISCS
