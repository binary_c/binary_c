#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS
double disc_inner_edge_accretion_f(struct stardata_t * stardata)
{
    double f;

    /*
     * Of material accreting from the inner edge of a
     * circumbinary disc, return f, the fraction that
     * accretes onto star 0.
     *
     * If star 1 is more massive than star 0, i.e. q>1,
     * we flip q (so then q<1) and return 1.0 - f
     */

    Boolean flip = stardata->star[1].mass > stardata->star[0].mass ?
        TRUE : FALSE;

    if(stardata->preferences->cbdisc_mass_loss_inner_viscous_accretion_method ==
       CBDISC_MASS_LOSS_INNER_VISCOUS_ACCRETION_METHOD_YOUNG_CLARKE_2015)
    {
        /*
         * Young and Clarke (2015, MNRAS 452, 3085)
         * give a prescription for f=f(q) (Eqs. 8 and 9)
         * with q = M2/M1 <= 1.0, for cold gas.
         *
         * Note: if q > 1, we should flip M1 and M2.
         */
        double q = stardata->star[1].mass / stardata->star[0].mass;
        if(flip == TRUE) q = 1.0 / q;
        f = 0.5 * q; // Young & Clarke Eq.8
        if(flip == TRUE) f = 1.0 - f;

        /* Clamp just in case */
        Clamp(f,0.0,1.0);
    }
    else if(stardata->preferences->cbdisc_mass_loss_inner_viscous_accretion_method ==
            CBDISC_MASS_LOSS_INNER_VISCOUS_ACCRETION_METHOD_GEROSA_2015)
    {
        /*
         * Gerosa et al. (2015, MNRAS 451, 3941)
         * Eq. 23
         */
        double q = stardata->star[1].mass / stardata->star[0].mass;
        if(flip == TRUE) q = 1.0 / q;
        f = q / (1 + q);
        if(flip == TRUE) f = 1.0 - f;

        /* Clamp just in case */
        Clamp(f,0.0,1.0);
    }
    else if(stardata->preferences->cbdisc_mass_loss_inner_viscous_accretion_method ==
            CBDISC_MASS_LOSS_INNER_VISCOUS_ACCRETION_METHOD_EQUAL)
    {
        /*
         * No mass dependence: 50:50 accretion
         */
        f = 0.5;
    }
    else if(stardata->preferences->cbdisc_mass_loss_inner_viscous_accretion_method ==
            CBDISC_MASS_LOSS_INNER_VISCOUS_ACCRETION_METHOD_NONE)
    {
        /*
         * No accretion
         */
        f = -1.0;
    }
    else
    {
        f = 0.0;
        Exit_binary_c(BINARY_C_ALGORITHM_BRANCH_FAILURE,
                      "Unknown disc accretion method");
    }

    return f;
}
#endif
