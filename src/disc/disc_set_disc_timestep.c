#include "../binary_c.h"

No_empty_translation_unit_warning;


#ifdef DISCS

void disc_set_disc_timestep(struct stardata_t * const stardata,
                            struct disc_t * const disc,
                            const struct binary_system_t * const binary,
                            const double dt)
{
    /*
     * We have a converged structure from the previous
     * step. This means we can estimate timescales for changes
     * to disc properties.
     */
    unsigned int why;
    const double dtwas = disc->dt;
    const double dt_natural = disc_calc_natural_timescale(stardata,
                                                          disc,
                                                          binary,
                                                          dt,
                                                          &why);
    /*
     * Hence the disc timestep
     */
    disc->dt = dt_natural * stardata->preferences->disc_timestep_factor;

    if(disc->dt < DISC_MIN_TIMESTEP)
    {
        disc->dt = DISC_MIN_TIMESTEP;
        why = DISC_TIMESTEP_LIMIT_MIN;
    }
    else if(disc->dt > DISC_MAX_TIMESTEP)
    {
        disc->dt = DISC_MAX_TIMESTEP;
        why = DISC_TIMESTEP_LIMIT_MAX;
    }

    /*
     * Don't let the timestep increase too fast
     */
    if(disc->dt > dtwas * DISC_TIMESTEP_HIGH_FACTOR)
    {
        disc->dt = dtwas * DISC_TIMESTEP_HIGH_FACTOR;
    }

    Discdebug(1,
              "Set disc dt = %g y because %s (was %g, (fac=%g)*(natural=%g y), min %g, max %g) (natural timestep %g y, disc t = %g, M = %g, binary sep = %g Rsun, RLOF? %d : binary_c dt = %g y)\n",
              disc->dt/YEAR_LENGTH_IN_SECONDS,
              Disc_timestep_limit_string(why),
              dtwas/YEAR_LENGTH_IN_SECONDS,
              stardata->preferences->disc_timestep_factor,
              dt_natural/YEAR_LENGTH_IN_SECONDS,
              DISC_MIN_TIMESTEP/YEAR_LENGTH_IN_SECONDS,
              DISC_MAX_TIMESTEP/YEAR_LENGTH_IN_SECONDS,
              dt_natural/YEAR_LENGTH_IN_SECONDS,
              disc->lifetime / YEAR_LENGTH_IN_SECONDS,
              disc->M / M_SUN,
              binary->separation/R_SUN,
              binary->RLOF,
              stardata->model.dtm * 1e6);

}


#endif//DISCS
