#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef DISCS
void disc_init_binary_structure(const struct stardata_t * const stardata,
                                struct binary_system_t * const binary,
                                struct disc_t * const cbdisc)
{
    /*
     * Initialize the binary star information
     * structure. This also precomputes some variables,
     * such as mtot and q.
     *
     * Also sets the disc minimum inner radius to the L2 radius
     */
    Discdebug(2,"initbinary ");
    binary->L = L_SUN*(stardata->star[0].luminosity +
                       stardata->star[1].luminosity);
    binary->m1 = Min(stardata->star[0].mass, stardata->star[1].mass) * M_SUN;
    binary->m2 = Max(stardata->star[0].mass, stardata->star[1].mass) * M_SUN;

    /*
     * For the separation take the maximum of the stellar
     * radii and the actual separation. During RLOF the separation
     * can become very small (<1e5cm), which throws off the disc
     * calculations. (Such small separations imply contact, which
     * happens imminently.)
     */
    binary->separation = R_SUN * Max3(stardata->common.orbit.separation,
                                      stardata->star[0].radius,
                                      stardata->star[1].radius);
    binary->torqueF = stardata->preferences->cbdisc_torquef;

    /*
     * Find the L2 and L3 points
     */
    {
        struct coordinate_t L[NUMBER_OF_LAGRANGE_POINTS];
        lagrange_points(binary->m1,
                        binary->m2,
                        binary->separation,
                        L,
                        &binary->a1,
                        &binary->a2);
        binary->aL1 = Pythag3_coord(L[0]); // L1 radius
        binary->aL2 = Pythag3_coord(L[1]); // L2 radius
        binary->aL3 = Pythag3_coord(L[2]); // L3 radius
        binary->a1 = fabs(binary->a1);
        binary->a2 = fabs(binary->a2);
        cbdisc->Rin_min = 0.0;
    }

    binary->eccentricity = Limit_range(stardata->common.orbit.eccentricity,0.0,1.0-1e-8);
    binary->Jorb = stardata->common.orbit.angular_momentum * ANGULAR_MOMENTUM_CGS;
    binary->mtot = binary->m1 + binary->m2;
    binary->reduced_mass = stardata->star[0].mass * stardata->star[1].mass * M_SUN * M_SUN / binary->mtot;
    binary->q = binary->m2/Max(1e-5,binary->m1); // NB q>1 by definition
    binary->omega = sqrt(GRAVITATIONAL_CONSTANT * binary->mtot/
                         Pow3(binary->separation)); // radian s^-1
    binary->orbital_period = 2.0 * PI / binary->omega; // period in s

    /*
     * Use the flux from the brighter star
     */
    //Star_number brighter = stardata->star[0].luminosity > stardata->star[1].luminosity ? 0 : 1;
    //binary->Rstar = stardata->star[brighter].radius * R_SUN;

#define Stellar_flux(N) (L_SUN * stardata->star[(N)].luminosity /       \
                         (4.0 * PI * Pow2(R_SUN * stardata->star[(N)].radius)))

#define FR2(N) (Stellar_flux(N)*Pow2(R_SUN*stardata->star[(N)].radius))
#define FR3(N) (Stellar_flux(N)*Pow3(R_SUN*stardata->star[(N)].radius))

    /*
     * Flux-weighted radius
     */
    binary->Rstar = (FR3(0) + FR3(1))/(FR2(0) + FR2(1));
    binary->flux = binary->L/(4.0 * PI * Pow2(binary->Rstar));

    /*
     * RLOFing check
     */
    binary->RLOF = (stardata->star[0].radius > stardata->star[0].roche_radius ||
                    stardata->star[1].radius > stardata->star[1].roche_radius) ? TRUE : FALSE;

    int i;

    /*
     * Luminosities in EUV, FUV and X-ray (cgs)
     */
    binary->LEUV = 0.0;
    binary->LFUV = 0.0;
    binary->LX   = 0.0;

    Starloop(i)
    {
        binary->Teff[i] = Teff(i);
        binary->luminosity[i] = stardata->star[i].luminosity;

        /*
         * Following Richling and Yorke
         *
         * FUV photons have 6 < E / eV < 13.6
         * EUV photons have 13.6 < E/eV < 100.0
         * X-ray photons have 100.0 < E/eV < 10^5
         */

        double frac_FUV = Blackbody_flux_fraction(binary->Teff[i],
                                                  FUV_eV_MIN*ELECTRON_VOLT,
                                                  FUV_eV_MAX*ELECTRON_VOLT);
        double frac_EUV = Blackbody_flux_fraction(binary->Teff[i],
                                                  EUV_eV_MIN*ELECTRON_VOLT,
                                                  EUV_eV_MAX*ELECTRON_VOLT);
        double frac_X = Blackbody_flux_fraction(binary->Teff[i],
                                                XRAY_eV_MIN*ELECTRON_VOLT,
                                                XRAY_eV_MAX*ELECTRON_VOLT);
        double Lcgs = stardata->star[i].luminosity * L_SUN;

        binary->LEUV += frac_EUV * Lcgs;
        binary->LFUV += frac_FUV * Lcgs;
        binary->LX += frac_X * Lcgs;

        Discdebug(2,
                  "LFRACS %d Teff=%g L=%g Lsun, %g erg/s : FUV %g EUV %g X %g : LX = %g erg/s = %g Lsun\n",
                  i,
                  binary->Teff[i],
                  stardata->star[i].luminosity,
                  stardata->star[i].luminosity * L_SUN,
                  frac_FUV,
                  frac_EUV,
                  frac_X,
                  binary->LX,
                  binary->LX/L_SUN
            );

//#define TESTBB
#ifdef TESTBB

        /*
         * Test black body fractions
         * e.g. then compare to Jain (Phys Educ. 31, 014)
         * NB Jain's data goes from T2 to some high cut off.
         * Here I also include X-ray above 100eV
         */

        double T;
        double T1 = 18932.0; // IR/VIS boundary
        double T2 = 37864.0; // VIS/UV boundary

        for(T=1e3; T<1e6; T+=1e3)
        {
            printf("BB %g %g %g %g %g\n",
                   T,
                   Blackbody_flux_fraction(T,
                                           1.0 * BOLTZMANN_CONSTANT,
                                           T1 * BOLTZMANN_CONSTANT),
                   Blackbody_flux_fraction(T,
                                           T1 * BOLTZMANN_CONSTANT,
                                           T2 * BOLTZMANN_CONSTANT),
                   Blackbody_flux_fraction(T,
                                           T2 * BOLTZMANN_CONSTANT,
                                           1e2 * ELECTRON_VOLT),
                   Blackbody_flux_fraction(T,
                                           1e2 * ELECTRON_VOLT,
                                           1e5 * ELECTRON_VOLT)
                );

        }
        Exit_binary_c(0,"Exit after testing black body functions");
#endif // TESTBB

    }

    const double menv = envelope_mass(&stardata->star[0]);

    /* debugging */
    Discdebug(2,
              "Binary structure L=%g Lsun (LFUV = %g), M1=%g (Menv1=%g Msun, st=%d) M2=%g Msun, Teff1=%g, sep = %g Rsun, e = %g, Rstar = %g Rsun, q=%g\n",
              binary->L/L_SUN,
              binary->LFUV/L_SUN,
              binary->m1/M_SUN,
              menv,
              stardata->star[0].stellar_type,
              binary->m2/M_SUN,
              Teff(0),
              binary->separation/R_SUN,
              binary->eccentricity,
              binary->Rstar/R_SUN,
              binary->q);

    Discdebug(2,
              "HRD %g %g %g %g\n",
                         stardata->model.time,
                         log10(Teff(0)),
                         log10(stardata->star[0].luminosity),
                         menv > 0.0 ? log10(menv) : -30
        );

    cbdisc->LX = binary->LX;

}

#endif
