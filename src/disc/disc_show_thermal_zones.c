#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS
/*
 * show disc thermal zone structure
 */
void disc_show_thermal_zones(struct stardata_t * const stardata,
                             struct disc_t * const disc,
                             const struct disc_thermal_zone_t * zones,
                             const int n)
{
    if(zones == NULL)
    {
        zones = disc->thermal_zones;
    }

    const int max = n==-1 ? DISCS_MAX_N_ZONES : n;
    int i;
    const Boolean show_invalid = FALSE;
    Boolean show = FALSE;
    for(i=0;i<max;i++)
    {
        if((zones+i)->valid==TRUE)
        {
            show = TRUE;
            break;
        }
    }

#define NORMAL_COLOUR(z) (z->valid==TRUE ? stardata->store->colours[BRIGHT_GREEN] : stardata->store->colours[RED])
#define ERROR_COLOUR (stardata->store->colours[YELLOW])
#define ASSERT_COLOUR(A) ((A)==TRUE ? (ERROR_COLOUR) : "")

    for(i=0;i<max;i++)
    {
        if(show_invalid == TRUE || (zones+i)->valid == TRUE)
        {

            const struct disc_thermal_zone_t * z = zones + i;
            printf("Zone %d at %p :",i,(void*)z);
            fflush(stdout);
            printf("type %s : %s %s: ",
                   Disc_zone_type_string(z),
                   Disc_zone_valid_string(z),
                   NORMAL_COLOUR(z)
                );

            fflush(stdout);
            if(show == TRUE)
            {
                if(0){
                    Boolean xxx=(i!=max-1 && (zones+i+1)->valid==TRUE);
                    if(xxx==TRUE)
                    {
                        printf("R transition=%5.3e ",i!=max-1 ? (disc_Rcross(z+i,z+i+1)/R_SUN) : 0);
                        fflush(stdout);
                    }
                    else
                    {
                        printf("                       ");
                    }
                }
                printf(" %sstart=%g%s%s, %send=%g%s%s, %swidth=%g%s%s ",
                       ASSERT_COLOUR(!(More_or_equal(z->rstart/disc->Rin,1.0) &&
                                       Less_or_equal(z->rstart/disc->Rout,1.0))),
                       z->rstart/R_SUN,
                       R_SOLAR,
                       NORMAL_COLOUR(z),
                       ASSERT_COLOUR(!(More_or_equal(z->rend/disc->Rin,1.0) &&
                                       Less_or_equal(z->rend/disc->Rout,1.0))),
                       z->rend/R_SUN,
                       R_SOLAR,
                       NORMAL_COLOUR(z),
                       ASSERT_COLOUR(z->rstart >= z->rend),
                       (z->rend - z->rstart)/R_SUN,
                       R_SOLAR,
                       NORMAL_COLOUR(z)
                    );
                fflush(stdout);

            }

            double dmzone = disc_partial_mass(disc,
                                              z->rstart,
                                              z->rend);
            printf(": prefactor=%g exponent=%g%s Tstart=%g (from A0=%g R0=%g %s exponent=%g) T(rstart)=%g Sigma(rstart)=%g [zone M=%20.10g Msun] %s\n",
                   z->Tlaw.A0,
                   z->Tlaw.exponent,
                   stardata->store->colours[CYAN],
                   (
                       Is_really_zero(z->Tlaw.A1) ?
                       // power law not yet complete
                       (z->Tlaw.A0*pow(z->rstart,z->Tlaw.exponent)) :
                       // power law is set up properly
                       power_law(&(z->Tlaw),z->rstart)),
                   z->Tlaw.A0,
                   z->rstart/R_SUN,
                   R_SOLAR,
                   z->Tlaw.exponent,
                   power_law(&z->Tlaw,z->rstart),
                   power_law(&z->power_laws[POWER_LAW_SIGMA],z->rstart),
                   dmzone/M_SUN,
                   stardata->store->colours[COLOUR_RESET]
                );
            fflush(stdout);
        }
    }
    printf("\n");
}



#endif//DISCS
