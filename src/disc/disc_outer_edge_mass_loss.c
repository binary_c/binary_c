#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS

double disc_outer_edge_mass_loss(struct stardata_t * const stardata,
                                 struct disc_t * const newdisc,
                                 struct disc_t * const olddisc,
                                 const struct binary_system_t * const binary)
{
    /*
     * Strip the outer part of the disc in to newdisc->Revap_out,
     * if newdisc->Revap_out is non-zero.
     *
     * olddisc is the previously calculated disc structure
     * which is used to calculate the changes.
     *
     * We apply the new Revap_out to newdisc.
     *
     * Returns the amount of mass lost (positive, grams).
     */
    double dm;

    if(Disc_is_disc(newdisc) &&
       Disc_is_disc(olddisc) &&
       Is_not_zero(newdisc->Revap_out))
    {
        /*
         * Timescale over which the mass is lost.
         */
        const double timescale = disc_outer_edge_loss_timescale(stardata,
                                                                olddisc,
                                                                binary);

        const double f = Is_really_zero(timescale) ? 1.0 :
            (Limit_range(olddisc->dt / timescale, 0.0, 1.0));

        const double dmwant = disc_partial_mass(olddisc,
                                                newdisc->Revap_out,
                                                olddisc->Rout);
        const double djwant = disc_partial_angular_momentum(olddisc,
                                                            binary,
                                                            newdisc->Revap_out,
                                                            olddisc->Rout);
        dm = Min(olddisc->M * DISC_STRIP_FRAC_M_OUTER, f*dmwant);
        double dj = Min(olddisc->J * DISC_STRIP_FRAC_J_OUTER, f*djwant);

        Discdebug(1,
                  "Disc outer edge evaporation in  : Rin = %g, Rout = %g, Revap_out = %g, M = %g Msun : timescale = %g y hence f = %g : dmwant = %g Msun -> dm = %g Msun, djwant = %g -> dj = %g\n",
                  olddisc->Rin/R_SUN,
                  olddisc->Rout/R_SUN,
                  newdisc->Revap_out/R_SUN,
                  olddisc->M/M_SUN,
                  timescale / YEAR_LENGTH_IN_SECONDS,
                  f,
                  dmwant / M_SUN,
                  dm / M_SUN,
                  djwant,
                  dj
            );

        /*
         * Beware:
         *         The disc may no longer be converged.
         *         This means the bisectors will BREAK.
         *         What to do? We should instead bisect on the
         *         old structure, not the new structure, and
         *         update based on that.
         */
        if(!Fequal(dmwant,dm) || !Fequal(djwant,dj))
        {
            /*
             * We tried to remove more material than limiters allow,
             * so adjust Revap_out to compensate.
             */
            newdisc->Revap_out = Max(
                newdisc->Rin,
                disc_mass_radius(stardata,olddisc,binary,olddisc->M - dm)
                //,disc_angular_momentum_radius(olddisc,binary,olddisc->J - dj)
                );

            Discdebug(1,
                      "dmwant = %g != dm = %g : adapt Revap_out to %g Rsun (cf. Rout = %g Rsun)\n",
                      dmwant,
                      dm,
                      newdisc->Revap_out / R_SUN,
                      newdisc->Rout / R_SUN
                );


            /*
             * Hence the actual mass and angular momentum removed
             */
            dm = disc_partial_mass(olddisc,
                                   newdisc->Revap_out,
                                   olddisc->Rout);
            dj = disc_partial_angular_momentum(olddisc,
                                               binary,
                                               newdisc->Revap_out,
                                               olddisc->Rout);
        }

        double mdot_out,jdot_out;
        if(Is_really_zero(dm))
        {
            /*
             * Do nothing.
             */
            mdot_out = 0.0;
            jdot_out = 0.0;
        }
        else
        {
            /*
             * Hence the mass and angular momentum loss rates (negative)
             */
             mdot_out = - dm / olddisc->dt;
             jdot_out = - dj / olddisc->dt;

            /*
             * Adjust mass and angular momenta of the new disc
             */
            Clamp(newdisc->M, 0.0, newdisc->M - dm);
            Clamp(newdisc->J, 0.0, newdisc->J - dj);

            /*
             * Update disc Rout and zones
             */
            Clamp(newdisc->Rout, newdisc->Rin, newdisc->Revap_out);

            /*
             * Stripped material is ejected from the binary system
             */
            newdisc->dM_ejected += dm;
            newdisc->dJ_ejected += dj;

            /*
             * Enforce M,J=0 when Rin==Rout:
             * prevents small numerical errors
             */
            if(Fequal(newdisc->Rin/newdisc->Rout,1.0))
            {
                Evaporate_disc(newdisc,
                               "Rin/Rout are equal in outer edge mass loss");
            }
            newdisc->loss[DISC_LOSS_OUTER_EDGE_STRIP].mdot = mdot_out;
            newdisc->loss[DISC_LOSS_OUTER_EDGE_STRIP].jdot = jdot_out;

            disc_rezone(newdisc,binary);
        }

        if(0)printf("STRIP OUTER from %g (%g) to %g : dm = %g Msun, dj = %g, olddisc->J = %g, integral %g [%g rest + %g stripped] new Rout = %g\n ",
               newdisc->Revap_out,
               olddisc->Revap_out,
               newdisc->Rout,
               dm/M_SUN,
               dj,
               olddisc->J,
               disc_total_angular_momentum(olddisc,binary),
               disc_partial_angular_momentum(olddisc,binary,olddisc->Rin,newdisc->Revap_out),
               disc_partial_angular_momentum(olddisc,binary,newdisc->Revap_out,olddisc->Rout),
               newdisc->Rout
            );


        Discdebug(1,
                  "Disc outer edge evaporation out : Rin = %g, Rout = %g, Revap_out = %g, M = %g Msun (mdot = %g, jdot = %g)\n",
                  newdisc->Rin/R_SUN,
                  newdisc->Rout/R_SUN,
                  newdisc->Revap_out/R_SUN,
                  newdisc->M/M_SUN,
                  mdot_out,
                  jdot_out
            );

    }
    else
    {
        dm = 0.0;
    }

    return dm;

}


#endif//DISCS
