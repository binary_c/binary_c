#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS

double disc_Rcross(const struct disc_thermal_zone_t * const z1,
                   const struct disc_thermal_zone_t * const z2)
{
    /*
     * Temperature power law crossing radius between zones z1 and z2.
     */
    return power_law_crossing_radius(&z1->Tlaw,&z2->Tlaw);
}


#endif
