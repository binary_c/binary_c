#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS

/*
 * Fill a disc_info_t struct and return it, or return
 * NULL on error.
 *
 * For example, this is used to interact with discs for
 * logging/ensemble purposes inside binary_grid scripts.
 */
binary_c_API_function
struct disc_info_t *
binary_c_disc_info(struct stardata_t * Restrict const stardata)
{
    struct disc_info_t * info;

    if(stardata->common.ndiscs > 0 )
    {
        struct disc_t * const disc = & stardata->common.discs[0];

        if(disc->converged == TRUE &&
           disc->n_thermal_zones > 0)
        {
            info = Malloc(sizeof(struct disc_info_t));

            struct binary_system_t * b;
            b = Calloc(1,sizeof(struct binary_system_t));
            disc_init_binary_structure(stardata,b,disc);

            info->M = disc->M/M_SUN;
            info->J = disc->J;
            info->L = disc_total_luminosity(disc)/L_SUN;
            info->Tin = disc_inner_edge_temperature(disc);
            info->Tout = disc_outer_edge_temperature(disc);
            info->Rin = disc->Rin/R_SUN;
            info->Rout = disc->Rout/R_SUN;
            info->Rin_a = disc->Rin/R_SUN/stardata->common.orbit.separation;
            info->Rout_a = disc->Rout/R_SUN/stardata->common.orbit.separation;
            info->Hin_Rin = Disc_scale_height_ratio(in);
            info->Hout_Rout = Disc_scale_height_ratio(out);
            info->lifetime = disc->lifetime;
        info->Mdot_visc = -disc->loss[DISC_LOSS_INNER_VISCOUS].mdot / M_SUN * YEAR_LENGTH_IN_SECONDS;

            Safe_free(b);
        }
        else
        {
            info = NULL;
        }
    }
    else
    {
        info = NULL;
    }
    return info;
}

#endif // DISCS
