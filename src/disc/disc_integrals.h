#pragma once

#ifdef DISC_ADAM_INTEGRALS_H
#define DISC_ADAM_INTEGRALS_H

static double disc_integral(const struct disc_t * const disc,
                            const int nlaw);

static double disc_partial_integral(const struct disc_t * const disc,
                                    const int nlaw,
                                    const double r0,
                                    const double r1);

#endif // DISC_ADAM_INTEGRALS_H
