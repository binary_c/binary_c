#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS

#undef DEBUG_ANGMOM_FLUX_BISECTOR
#define DEBUG_ANGMOM_FLUX_BISECTOR 1


double disc_F_bisector(const double Rin,
                       void * p)
{
    /*
     * Bisect the angular momentum flux equation by varying
     * Rin at fixed Rout and mass (i.e. Tvisc0)
     */
    Map_GSL_params(p,args);
    Map_varg(struct stardata_t *,stardata,args);
    Map_varg(struct disc_t *,disc,args);
    Map_varg(struct binary_system_t *,binary,args);
    Map_varg(double,Routfac,args);
    va_end(args);
    double ret;


    if(DEBUG_ANGMOM_FLUX_BISECTOR)
    {
        printf("Bisect for F with Rin = %20.12e, Rout = %g\n",disc->Rin,disc->Rout);
    }

    /*
     * We're given Rin, the inner radius of the disc,
     * as the variable parameter.
     *
     * Tvisc0 and Rout are fixed, although you can
     * set Routfac to force Rout = Routfac * Rin
     * at each step.
     */
    disc->Rin = Rin;
    if(Is_not_zero(Routfac))
    {
        disc->Rout = Rin * Routfac;
        if(DEBUG_ANGMOM_FLUX_BISECTOR)
        {
            printf("Force Rout = %g from Rin = %g * %g\n",
                   disc->Rout,
                   Rin,
                   Routfac);
        }
    }

    double F = -666.0;
    int status;

    if(Rin > disc->Rout)
    {
        if(DEBUG_ANGMOM_FLUX_BISECTOR)
        {
            printf("Error : Rin = %g > Rout = %g : return -1\n",Rin,disc->Rout);
        }
        status = DISC_ZONES_RIN_EXCEEDS_ROUT;
        ret = -1.0;
    }
    else
    {
        status = disc_build_disc_zones(stardata,disc,binary);

        if(status == DISC_ZONES_OK)
        {
            F = disc_total_angular_momentum_flux(disc,binary);

            if(DEBUG_ANGMOM_FLUX_BISECTOR)
            {
                double M = disc_total_mass(disc);
                double J = disc_total_angular_momentum(disc,binary);
                printf("Bisect for F: Given Rin = %g, Rout = %g, Tvisc0 = %g : M=%g (want %g) J=%g (want %g) F=%g (want %g) eps=%g\n",
                       disc->Rin,disc->Rout,disc->Tvisc0,
                       M,disc->M,
                       J,disc->J,
                       F,disc->F,
                       disc->F/F - 1.0
                    );
            }
            ret = Bisection_result_inverse(disc->F,F);
            if(0)printf("Bisect for F : ok want=%g / have=%g = %g : ret = %g : Rin = %g\n",
                        disc->F,
                        F,
                        disc->F/F,
                        ret,
                        disc->Rin);
        }
        else
        {
            if(1)printf("Bisect for F : disc_build_disc_zones failed with status %d \"%s\"\n",
                   status,
                   Disc_zone_status(status)
                );
            ret = -1.0;
        }
    }
    //printf("F bisector return %g\n",ret);
    if(0)printf("F bisector (Rin=%g Rout=%g) -> F=%g, ret=%g, status %d %s\n",
                Rin,disc->Rout,F,ret,status,Disc_zone_status(status));
    return ret;
}



#endif // DISCS
