#include "../binary_c.h"

No_empty_translation_unit_warning;

#ifdef DISCS

double disc_inner_edge_mass_loss(struct stardata_t * const stardata,
                                 struct disc_t * const newdisc,
                                 struct disc_t * const olddisc,
                                 const struct binary_system_t * const binary)
{

    /*
     * Strip the inner edge of the disc out to
     * newdisc->Revap_in, if newdisc->Revap_in is non-zero.
     *
     * olddisc is the previously calculated disc structure
     * which is used to calculate the changes.
     *
     * We apply the new Revap_in to newdisc.
     */
    double mdot_in=0.0;
    double dm;

    if(Disc_is_disc(newdisc) &&
       Disc_is_disc(olddisc) &&
       Is_not_zero(newdisc->Revap_in) &&
       More_or_equal(newdisc->Revap_in, olddisc->Rin))
    {
        Discdebug(1,
                  "Disc inner edge evaporation : pre : Rin = %g %s, Revap_in was = %g %s\n",
                  Solar(olddisc->Rin,R),
                  Solar(newdisc->Revap_in,R));

        Discdebug(1,
                  "Disc->J = %g, integral gives %g\n",
                  olddisc->J,
                  disc_total_angular_momentum(olddisc,binary));

        /*
         * Mass loss timescale ~ the orbital period (seconds)
         */
        const double timescale = disc_inner_edge_loss_timescale(stardata,
                                                          olddisc,
                                                          binary);
        Discdebug(1,"Disc mass loss timescale %g y\n",
                  timescale/YEAR_LENGTH_IN_SECONDS);

        const double f = Is_really_zero(timescale) ? 1.0 :
            (Limit_range(olddisc->dt / timescale, 0.0, 1.0));

        /*
         * Mass we would like to strip
         */
        double dmwant = disc_partial_mass(olddisc,
                                          olddisc->Rin,
                                          newdisc->Revap_in);
        double djwant = disc_partial_angular_momentum(olddisc,
                                                      binary,
                                                      olddisc->Rin,
                                                      newdisc->Revap_in);

        Discdebug(1,
                  "dmwant = %30.20g djwant = %g \n",
                  dmwant/M_SUN,
                  djwant);

        /*
         * Mass we'll try to strip
         */
        dm = Min(olddisc->M * DISC_STRIP_FRAC_M_INNER, f * dmwant);
        double dj = Min(olddisc->J * DISC_STRIP_FRAC_J_INNER, f * djwant);

        Discdebug(1,
                  "dm = %30.20g \n",
                  dm/M_SUN);

        if(!Fequal(dmwant,dm) || !Fequal(djwant,dj))
        {
            /*
             * We tried to remove more material than limiters allow,
             * so adjust Revap_in to compensate.
             *
             * e.g. if too much mass is sucked off, and if there were no limit,
             * Revap_in would match dmwant, but we want it to match dm.
             *
             * ditto for dj, so choose the minimum of the two to prevent
             * as much mass loss as possible
             */
            Discdebug(1,
                      "Tried to remove more than limits allow : adjust\n");

            const double R_M = disc_mass_radius(stardata,olddisc,binary,dm);
            Discdebug(1,"R_M = %g\n",R_M);

            const double R_J = disc_angular_momentum_radius(stardata,olddisc,binary,dj);
            Discdebug(1,"R_J = %g\n",R_J);

            newdisc->Revap_in = Min(R_M,R_J);

            /*
             * Hence the actual mass and angular momentum removed
             */
            dm = disc_partial_mass(olddisc,
                                   olddisc->Rin,
                                   newdisc->Revap_in);
            dj = disc_partial_angular_momentum(olddisc,
                                               binary,
                                               olddisc->Rin,
                                               newdisc->Revap_in);
            Discdebug(1,
                      "dm(Revap_in=%g Rsun) = %30.20g \n",
                      newdisc->Revap_in / R_SUN,
                      dm/M_SUN);

        }

        Discdebug(1,
                  "Disc inner edge evaporation : remove dm = %g\n",
                  dm/M_SUN);

        if(Is_not_zero(dm))
        {

            double pm = disc_partial_mass(newdisc,
                                          newdisc->Rin,
                                          newdisc->Revap_in)/M_SUN;
            Discdebug(1,
                      "= %g %s between %g and %g Rsun\n",
                      pm,
                      M_SOLAR,
                      newdisc->Rin/R_SUN,
                      newdisc->Revap_in/R_SUN
                );
        }

        /*
         * Hence the mass loss rate
         */
        mdot_in = - dm / olddisc->dt;

        /*
         * Update the mass and angular momentum
         */
        Clamp(newdisc->M, 0.0, newdisc->M - dm);
        Clamp(newdisc->J, 0.0, newdisc->J - dj);

        /*
         * Update disc zones
         */
        double Rinwas = newdisc->Rin;
        newdisc->Rin = Max(newdisc->Rin, newdisc->Revap_in);
        Discdebug(2,
                  "Set new Rin = %g from old Rin %g, new Revap_in = %g\n",
                  newdisc->Rin/R_SUN,
                  Rinwas/R_SUN,
                  newdisc->Revap_in/R_SUN);

        newdisc->Rout = Max(newdisc->Rin, newdisc->Rout);

        /*
         * Update zones
         */
        disc_rezone(newdisc,binary);
        Discdebug(2,
                  "Post disc_rezone new Rin %g\n",
                  newdisc->Rin/R_SUN);

        /*
         * Stripped material is ejected from the binary system
         */
        newdisc->dM_ejected += dm;
        newdisc->dJ_ejected += dj;

        /*
         * Enforce M,J=0 when Rin==Rout:
         * prevents small numerical errors
         */
        if(Fequal(newdisc->Rin/newdisc->Rout,1.0))
        {
            Evaporate_disc(newdisc,
                           "Rin and Rout are equal in inner edge mass loss");
        }

        Discdebug(1,
                  "Disc inner edge remove: dm=%g dj=%g : M now %g (integral %g), J now %g (integral %g), Rin = %g (was %g), Revap_in = %g, Rout = %g, t = %g\n",
                  dm/M_SUN,
                  dj,
                  newdisc->M/M_SUN,
                  disc_total_mass(newdisc)/M_SUN,
                  newdisc->J,
                  disc_total_angular_momentum(newdisc,binary),
                  newdisc->Rin/R_SUN,
                  Rinwas/R_SUN,
                  newdisc->Revap_in/R_SUN,
                  newdisc->Rout/R_SUN,
                  newdisc->lifetime);



        /*
         * If inner edge evaporation rate exceeds
         * the viscous inflow rate, suppress all the
         * viscous inflow.
         * NB both numbers are negative, hence the <
         */
        if(mdot_in < newdisc->loss[DISC_LOSS_INNER_VISCOUS].mdot)
        {
            newdisc->suppress_viscous_inflow = TRUE;
        }
        else
        {
            newdisc->suppress_viscous_inflow = FALSE;
        }

        newdisc->loss[DISC_LOSS_INNER_EDGE_STRIP].mdot = mdot_in;
        newdisc->loss[DISC_LOSS_INNER_EDGE_STRIP].jdot = - dj / newdisc->dt;
    }
    else
    {
        Discdebug(1,
                  "Disc inner edge evaporation : none : is disc? %d Revap_in %g cf Rin %g\n",
                  Disc_is_disc(newdisc),
                  newdisc->Revap_in,
                  newdisc->Rin
            );
        dm = 0.0;
        newdisc->suppress_viscous_inflow = FALSE;
    }

    return dm;
}
#endif//DISCS
