#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS
//#undef DEBUG_ZONE_RADII
//#define DEBUG_ZONE_RADII 1
#define _OLD 0
#define _NEW 1


/*
 * Determine disc zone radii
 */

Boolean disc_determine_zone_radii(struct stardata_t * const stardata,
                                  struct disc_t * const disc)
{
    int method = _NEW;

    if(method == _NEW)
    {
        /*
         * Use new method, more flexible than previously
         */
        disc_new_zone_radii(disc);
    }
    else if(method == _OLD)
    {

        /*
         * W : list of all possible zones
         */
        struct disc_thermal_zone_t * W = disc->thermal_zones;

        /*
         * P : list of thermal zones to be constructed
         */
        struct disc_thermal_zone_t P[DISCS_MAX_N_ZONES+1];

        Disc_zone_counter i, nzones = DISCS_MAX_N_ZONES;

        /*
         * Set up list of all three zones in order
         */
        for(i=0;i<nzones;i++)
        {
            Copy_zone(W+i,P+i);
            P[i].valid=TRUE;
        }

        /*
         * Set radii based on crossing radii
         * and implausible inner and outer disc radii
         */
        P[0].rstart = 1.0;
        P[0].rend = disc_Rcross(P+0,P+1); // VR
        P[1].rstart = P[0].rend;
        P[1].rend = disc_Rcross(P+1,P+2); // IO
        P[2].rstart = P[1].rend;
        P[2].rend = 1e100;

        if(DEBUG_ZONE_RADII)
        {
            printf("Initial 3-zone setup (disc->Rin=%g disc->Rout=%g)\n",
                   disc->Rin,
                   disc->Rout);
            disc_show_thermal_zones(stardata,
                                    disc,
                                    P,
                                    nzones);
        }

        /*
         * Perhaps eliminate zones based on crossing radii locations
         */
        if(P[0].rend >= P[1].rend)
        {
            // VR>IO, so there is no inner radiative zone
            Copy_zone(P+2,P+1);
            P[0].rend = disc_Rcross(P+0,P+1); // VO is the new zone boundary
            P[1].rstart = P[0].rend;
            P[2].valid=FALSE;
            nzones--;
        }
        /*
          else
          {
          // we have all three zones in this case (VR<IO)
          }
        */

        if(DEBUG_ZONE_RADII)
        {
            printf("Perhaps removed inner radiative zone?\n");
            disc_show_thermal_zones(stardata,disc,P,nzones);
        }

        /* make sure all radii are in the disc */
        for(i=0;i<nzones;i++)
        {
            Clamp(P[i].rstart, disc->Rin, disc->Rout);
            Clamp(P[i].rend,   disc->Rin, disc->Rout);
        }

        if(DEBUG_ZONE_RADII)
        {
            printf("Make sure all radii are in the disc\n");
            disc_show_thermal_zones(stardata,disc,P,nzones);
        }

        /* eliminate zones with zero or negative width */
        for(i=0;i<nzones;i++)
        {
            double width = P[i].rend - P[i].rstart;
            if(width < DISC_MINIMUM_THERMAL_ZONE_WIDTH)
            {
                Disc_zone_counter j;
                for(j=i;j<nzones-1;j++)
                {
                    Copy_zone(P+j+1,P+j);
                }
                i--;
                nzones--;
            }
        }


        if(DEBUG_ZONE_RADII)
        {
            printf("Eliminated zones with <=0 width\n");
            disc_show_thermal_zones(stardata,disc,P,nzones);
        }



        /*
         * Default to no valid zones...
         */
        disc->n_thermal_zones = 0;
        for(i=0;i<DISCS_MAX_N_ZONES;i++)
        {
            disc->thermal_zones[i].valid = FALSE;
        }

        /*
         * ...then copy valid zones to the disc
         */
        for(i=0;i<nzones;i++)
        {
            if(P[i].valid == TRUE)
            {
                Copy_zone(P+i,&(disc->thermal_zones[disc->n_thermal_zones]));
                disc->n_thermal_zones++;
            }
        }

        /*
         * Set up the temperature power laws
         */
        disc_set_temperature_power_laws(disc);

        if(DEBUG_ZONE_RADII)
        {
            printf("Final disc setup (%u zones)\n",disc->n_thermal_zones);
            disc_show_thermal_zones(stardata,
                                    disc,
                                    disc->thermal_zones,
                                    disc->n_thermal_zones);
        }
    }

    /*
     * Return TRUE if the above procedure succeeded,
     * otherwise FALSE.
     */
    return disc->n_thermal_zones==0 ? FALSE : TRUE;
}

#endif//DISCS
