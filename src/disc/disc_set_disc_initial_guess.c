#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS

#if __GNUC__ >= 11
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wvla-parameter"
#endif
#include "disc_constraints.h"

void disc_set_disc_initial_guess(struct stardata_t * const stardata,
                                 struct disc_t * const disc,
                                 const int iguess,
                                 int n,/* VLA initial_guesses REQUIRES this to be type int, not const int */
                                 const disc_parameter * const parameter_types,
                                 double initial_guesses[DISC_SOLVER_NINITIAL_GUESSES+1][n])
{
    /*
     * Set intitial guess number iguess into the disc
     */
    double unknown;
    Discdebug(1,
              "Set initial guess : solver = %s :: ",
              Solver_string(disc->solver));
    for(int i=0;i<n;i++)
    {
        Disc_parameter(disc,parameter_types[i]) = initial_guesses[iguess][i];
        Discdebug_plain(1,
                        " %s = %g %s",
                        Disc_parameter_string(parameter_types[i]),
                        Disc_parameter(disc,parameter_types[i]),
                        ((i==n-1) ? "" : ","));
    }
    Discdebug_plain(1,"\n");
}
#if __GNUC__ >= 10
#pragma GCC diagnostic pop
#endif
#endif // DISCS
