#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS

void No_return disc_testing(struct stardata_t * const stardata)
{
    printf("DISC FUNCTION TESTING\n");

#ifdef BASIC_TESTS

    struct star_t * star = &(stardata->star[0]);

    printf("Make new disc at the inner edge with mass 1\n");
    struct disc_t * disc1 = new_disc(DISC_CIRCUMSTELLAR,star,DISC_INNER_EDGE);
    disc1->M = 1.0;

    disc_status(stardata);

    printf("Add a disc at the inner edge with mass 2\n");
    struct disc_t * disc2 = new_disc(DISC_CIRCUMSTELLAR,star,DISC_INNER_EDGE);
    disc2->M = 2.0;

    disc_status(stardata);

    printf("Add a disc at the outer edge with mass 3\n");
    struct disc_t * disc3 = new_disc(DISC_CIRCUMSTELLAR,star,DISC_OUTER_EDGE);
    disc3->M = 3.0;

    disc_status(stardata);

    printf("Add a disc at the outer edge with mass 4\n");
    struct disc_t * disc4 = new_disc(DISC_CIRCUMSTELLAR,star,DISC_OUTER_EDGE);
    disc4->M = 4.0;

    disc_status(stardata);

    printf("Add a disc at position 2 with mass 5\n");
    struct disc_t * disc5 = new_disc(DISC_CIRCUMSTELLAR,star,2);
    disc5->M = 5.0;

    disc_status(stardata);

    printf("The disc masses should be in the order : 2 1 5 3 4\n");

    printf("Remove disc at position 3\n");
    remove_disc(DISC_CIRCUMSTELLAR,star,3);

    printf("The disc masses should be in the order : 2 1 5 4\n");
    disc_status(stardata);

    printf("Remove inner edge disc\n");
    remove_disc(DISC_CIRCUMSTELLAR,star,DISC_INNER_EDGE);

    printf("The disc masses should be in the order : 1 5 4\n");
    disc_status(stardata);

    printf("Remove outer edge disc\n");
    remove_disc(DISC_CIRCUMSTELLAR,star,DISC_OUTER_EDGE);

    printf("The disc masses should be in the order : 1 5\n");
    disc_status(stardata);

    printf("Add circumbinary disc of mass 666\n");
    struct disc_t * cbdisc1 = new_disc(DISC_CIRCUMBINARY,stardata,DISC_OUTER_EDGE);
    cbdisc1->M = 666.0;

    disc_status(stardata);

    printf("Add circumbinary disc of mass 1000\n");
    struct disc_t * cbdisc2 = new_disc(DISC_CIRCUMBINARY,stardata,DISC_OUTER_EDGE);
    cbdisc2->M = 1000;

    disc_status(stardata);

    printf("Remove circumstellar discs\n");
    remove_discs(DISC_CIRCUMSTELLAR,star);

    disc_status(stardata);

    printf("Remove circumbinary discs\n");
    remove_discs(DISC_CIRCUMBINARY,&(stardata->common));

    printf("There should now be no discs\n");
    disc_status(stardata);

    printf("Pointers are : disc1=%p disc2=%p disc3=%p disc4=%p disc5=%p cbdisc1=%p cbdisc2=%p\n",
           disc1,disc2,disc3,disc4,disc5,cbdisc1,cbdisc2);

    printf("Make new disc at the inner edge with mass 1\n");
    disc1 = new_disc(DISC_CIRCUMSTELLAR,star,DISC_INNER_EDGE);
    disc1->M = 1.0;

    disc_status(stardata);


#endif // BASIC_TESTS

    struct disc_t * cbdisc = new_disc(stardata,
                                      DISC_CIRCUMBINARY,
                                      stardata,
                                      DISC_OUTER_EDGE);

    cbdisc->M = 1e-3 * M_SUN; // 1e-3
    cbdisc->Rin = 1e10; // 1e10
    cbdisc->Rout = 1e18; // 1e18
    cbdisc->alpha = 0.01; // 0.01
    cbdisc->gamma = 5.0/3.0; // 5/3
    cbdisc->torqueF = 1.0; // 1.0
    cbdisc->kappa = 1e-2; // 1e-2


    stardata->star[0].mass = 2.0; // 2
    stardata->star[1].mass = 1.0; // 1
    stardata->star[0].luminosity = 1e2; // Lsun
    stardata->star[1].luminosity = 0.1; // Lsun
    stardata->common.orbit.separation = 100.0; // Rsun
    stardata->star[0].radius=2.0;
    stardata->star[1].radius=1.0;


    /*
     * post-CE system
     */
    cbdisc->M = 0.606 * M_SUN;
    stardata->star[0].mass = 0.705;
    stardata->star[1].mass = 0.55;
    cbdisc->Rout = 3e14;
    stardata->common.orbit.separation = 26.6;
    cbdisc->Rin = R_SUN * stardata->common.orbit.separation * 3.0;

    stardata->star[0].luminosity = 10982.1;
    stardata->star[1].luminosity = 0.5;


    disc_status(stardata);

    printf("INITIALIZE DISC THERMAL STRUCTURE\n");fflush(stdout);
    disc_initialize_disc(stardata,cbdisc,NULL);

    printf("EVOLVE DISC THERMAL STRUCTURE\n");fflush(stdout);
    int i=0;
    double t=0.0;

    /* time for which we should evolve the disc */
    double discdt = 1e4 * YEAR_LENGTH_IN_SECONDS;
    while(t < 3e5 * YEAR_LENGTH_IN_SECONDS &&
          i++<100000)
    {
        t += disc_evolve_disc_structure(stardata,cbdisc,discdt);
        printf("time %g\n",t/YEAR_LENGTH_IN_SECONDS);
        disc_status(stardata);
        printf("DISCDATA %g %g %g %g\n",
               t/YEAR_LENGTH_IN_SECONDS,
               cbdisc->M/M_SUN,
               cbdisc->Rin/R_SUN,
               cbdisc->Rout/R_SUN);
    }
    Exit_binary_c(BINARY_C_NORMAL_EXIT,"Done disc testing\n");
}

#endif // DISCS
