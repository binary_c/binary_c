#pragma once
#ifndef DISC_CONSTRAINTS_H
#define DISC_CONSTRAINTS_H

/*
 * Scheme to set up constraints, parameters
 * and calculate residuals
 * for the disc's GSL solver.
 */


/************************************************************
 * Constraint labels
 *
 * Remember to update DISC_NUMBER_OF_CONSTRAINTS when adding
 * a constraint
 *
 ************************************************************/
#define DISC_CONSTRAINT_NONE 0
#define DISC_CONSTRAINT_M_TO_DISC_M 1
#define DISC_CONSTRAINT_J_TO_DISC_J 2
#define DISC_CONSTRAINT_F_TO_DISC_F 3
#define DISC_CONSTRAINT_RIN_MIN_TO_DISC_RIN 4
#define DISC_CONSTRAINT_ROUT_MAX_TO_DISC_ROUT 5
#define DISC_NUMBER_OF_CONSTRAINTS 6

/*
 * A result that cannot be true and must indicate
 * failure.
 */
#define DISC_CONSTRAINT_FAILED -1.0

/*
 * Convert a constraint to a human-readable string
 */
#define Disc_constraint_string(TYPE) (                                  \
        (TYPE) == DISC_CONSTRAINT_NONE ? "None" :                       \
        (TYPE) == DISC_CONSTRAINT_M_TO_DISC_M ? "M/disc->M" :           \
        (TYPE) == DISC_CONSTRAINT_J_TO_DISC_J ? "J/disc->J" :           \
        (TYPE) == DISC_CONSTRAINT_F_TO_DISC_F ? "F/disc->F" :           \
        (TYPE) == DISC_CONSTRAINT_RIN_MIN_TO_DISC_RIN ? "Rin_min/disc->Rin" : \
        (TYPE) == DISC_CONSTRAINT_ROUT_MAX_TO_DISC_ROUT ? "Rout_max/disc->Rout" : \
        "unknown")

/*
 * Define disc constraint variables.
 * D = disc, B = binary
 */
#define Disc_constraint_constant(D,B,TYPE) (                            \
        (TYPE) == DISC_CONSTRAINT_NONE ? 0.0 :                          \
        (TYPE) == DISC_CONSTRAINT_M_TO_DISC_M ? ((D)->M) :              \
        (TYPE) == DISC_CONSTRAINT_J_TO_DISC_J ? ((D)->J) :              \
        (TYPE) == DISC_CONSTRAINT_F_TO_DISC_F ? ((D)->F) :              \
        (TYPE) == DISC_CONSTRAINT_RIN_MIN_TO_DISC_RIN ? ((D)->Rin) :    \
        (TYPE) == DISC_CONSTRAINT_ROUT_MAX_TO_DISC_ROUT ? ((D)->Rout) : \
        0.0)

/*
 * Define disc constraint values, usually
 * integrals of some kind.
 */
#define Disc_constraint_value(D,B,TYPE)                                 \
    (                                                                   \
        (TYPE) == DISC_CONSTRAINT_NONE ? 0.0 :                          \
        (TYPE) == DISC_CONSTRAINT_M_TO_DISC_M ? (disc_total_mass((D))) : \
        (TYPE) == DISC_CONSTRAINT_J_TO_DISC_J ? (disc_total_angular_momentum((D),(B))) : \
        (TYPE) == DISC_CONSTRAINT_F_TO_DISC_F ? (disc_total_angular_momentum_flux((D),(B))) : \
        (TYPE) == DISC_CONSTRAINT_RIN_MIN_TO_DISC_RIN ? (Max(1e-100,(D)->Rin_min)) : \
        (TYPE) == DISC_CONSTRAINT_ROUT_MAX_TO_DISC_ROUT ? (Max(1e-100,(D)->Rout_max)) : \
        0.0                                                             \
        )

/*
 * Unit value for constraint value
 */
#define Disc_constraint_unit(TYPE)                                      \
    (                                                                   \
        (TYPE) == DISC_CONSTRAINT_M_TO_DISC_M ? M_SUN :                 \
        ((TYPE) == DISC_CONSTRAINT_RIN_MIN_TO_DISC_RIN ||               \
         (TYPE) == DISC_CONSTRAINT_ROUT_MAX_TO_DISC_ROUT) ? R_SUN :     \
        1.0                                                             \
        )

/*
 * Macro to construct a residual and
 * return its value, which is approximately
 * zero when converged.
 *
 * Please access the residual through Disc_residual(...).
 *
 * We usually assume the required values are positive.
 *
 * You can use the Residualfunc macro to define a residual.
 * This takes the variable name as VAR (from disc->VAR),
 * D is the disc, (D) in Disc_residual, and FUNC is the function
 * to compare to (D)->VAR. The remaining arguments are
 * passed to the function.
 *
 * Note that the Residualfunc MUST be monotonically increasing.
 *
 * For functions that are montonically decreasing, we provide
 * Residualfunc_inverse.
 *
 * Note that these map to the more generic Bisection_result
 * and Bisection_result_inverse macros (see binary_c_function_macros.h).
 */

#define Residualfunc(D,B,TYPE)                          \
    (                                                   \
        Bisection_result(                               \
            (Disc_constraint_constant((D),(B),(TYPE))), \
            (Disc_constraint_value((D),(B),(TYPE)))     \
            )                                           \
        )

#define Residualfunc_inverse(D,B,TYPE)                  \
    (                                                   \
        Bisection_result_inverse(                       \
            (Disc_constraint_constant((D),(B),(TYPE))), \
            (Disc_constraint_value((D),(B),(TYPE)))     \
            )                                           \
        )

#define Disc_residual(D,B,TYPE)                                 \
    (                                                           \
        /* standard */                                          \
        (                                                       \
            (TYPE)==DISC_CONSTRAINT_M_TO_DISC_M ||              \
            (TYPE)==DISC_CONSTRAINT_J_TO_DISC_J                 \
            ) ?                                                 \
                                                                \
        /* inverse */                                           \
        (Residualfunc((D),(B),(TYPE)))                          \
        :                                                       \
                                                                \
        (                                                       \
            (TYPE)==DISC_CONSTRAINT_F_TO_DISC_F ||              \
            (TYPE)==DISC_CONSTRAINT_ROUT_MAX_TO_DISC_ROUT ||    \
            (TYPE)==DISC_CONSTRAINT_ROUT_MAX_TO_DISC_ROUT       \
            ) ?                                                 \
                                                                \
        (Residualfunc_inverse((D),(B),(TYPE)))                  \
        :                                                       \
                                                                \
        DISC_CONSTRAINT_FAILED                                  \
        )

#define Disc_abs_residual(D,B,TYPE) (fabs(Disc_residual((D),(B),(TYPE))))

/*
 * Macro to return the tolerance of a certain constraint
 */
#define Disc_tolerance(D,B,TYPE)                        \
    (                                                   \
        (TYPE)==DISC_CONSTRAINT_M_TO_DISC_M ?           \
        (DISC_MASS_TOLERANCE) :                         \
                                                        \
        (TYPE)==DISC_CONSTRAINT_J_TO_DISC_J ?           \
        (DISC_ANGMOM_TOLERANCE) :                       \
                                                        \
        (TYPE)==DISC_CONSTRAINT_F_TO_DISC_F ?           \
        (DISC_ANGMOM_FLUX_TOLERANCE) :                  \
                                                        \
        (TYPE)==DISC_CONSTRAINT_RIN_MIN_TO_DISC_RIN ?   \
        (DISC_TOLERANCE) :                              \
                                                        \
        (TYPE)==DISC_CONSTRAINT_ROUT_MAX_TO_DISC_ROUT ? \
        (DISC_TOLERANCE) :                              \
                                                        \
        DISC_CONSTRAINT_FAILED                          \
        )



/*
 * Macro to return TRUE if constraint TYPE in disc D is converged,
 * FALSE otherwise
 */
#define Disc_constraint_converged(D,B,TYPE)     \
    (                                           \
        (Disc_abs_residual((D),(B),(TYPE)) <    \
         Disc_tolerance((D),(B),(TYPE))) ?      \
        TRUE :                                  \
        FALSE)

/************************************************************
 * Parameter labels
 ************************************************************/
#define DISC_PARAMETER_NONE 0
#define DISC_PARAMETER_TVISC0 1
#define DISC_PARAMETER_RIN 2
#define DISC_PARAMETER_ROUT 3
#define DISC_PARAMETER_TORQUEF 4

/*
 * Convert a parameter to a human-readable string
 */
#define Disc_parameter_string(TYPE) (                   \
        (TYPE) == DISC_PARAMETER_NONE   ? "None" :      \
        (TYPE) == DISC_PARAMETER_TVISC0 ? "Tvisc0" :    \
        (TYPE) == DISC_PARAMETER_RIN    ? "Rin" :       \
        (TYPE) == DISC_PARAMETER_ROUT   ? "Rout" :      \
        (TYPE) == DISC_PARAMETER_TORQUEF   ? "Torquef" :   \
        "Unknown")


/*
 * Macro to map a DISC_PARAMETER type (TYPE) to a
 * variable in the disc structure (D).
 *
 * If I is unknown, then the variable "unknown" is
 * pointed to. This should be declared as "double Maybe_unused".
 */
#define Disc_parameter(D,TYPE)                                  \
    (*(                                                         \
        (TYPE)==DISC_PARAMETER_TVISC0 ? (&((D)->Tvisc0)) :      \
        (TYPE)==DISC_PARAMETER_RIN ? (&((D)->Rin)) :            \
        (TYPE)==DISC_PARAMETER_ROUT ? (&((D)->Rout)) :          \
        (TYPE)==DISC_PARAMETER_TORQUEF ? (&((D)->torqueF)) :    \
        &unknown                                                \
        ))


/*
 * Macro to test if parameter of type (TYPE) value (VAL) is physically valid
 * in disc (D).
 *
 * Return TRUE if ok, FALSE if bad.
 *
 * Defaults to TRUE for an unknown parameter
 */
#define Disc_parameter_check(D,TYPE,VAL)                                \
    (                                                                   \
        (TYPE) == DISC_PARAMETER_TVISC0 ? ((VAL) > TINY ? TRUE : TRUE) : \
        (TYPE) == DISC_PARAMETER_RIN ? ((VAL) < ((D)->Rout + TINY) ? TRUE : TRUE) : \
        (TYPE) == DISC_PARAMETER_ROUT ? ((VAL) > ((D)->Rin  - TINY) ? TRUE : TRUE) : \
        TRUE)

/*
 * Macro to map the minima and maxima of the various parameters
 *
 * Defaults to 1e-100 or 1e+100 for an unknown parameter.
 *
 * Rin can be from DISC_RIN_MIN to almost (D)->Rout
 *
 * Rout can be from just more than (D)->Rin to DISC_ROUT_MAX
 *
 */

#define _EPS 1e-6
#define _JUST_MORE (1.0 + _EPS)
#define _JUST_LESS (1.0 - _EPS)
/*
#define Disc_parameter_min(D,TYPE)                                      \
    (                                                                   \
        (TYPE) == DISC_PARAMETER_TVISC0 ? DISC_TVISC0_MIN :             \
        (TYPE) == DISC_PARAMETER_RIN ? DISC_RIN_MIN :                   \
        (TYPE) == DISC_PARAMETER_ROUT ? (Max(DISC_RIN_MIN,              \
                                             (D)->Rin*_JUST_MORE)):     \
        1e-100                                                          \
        )
*/
#define Disc_parameter_min(D,TYPE)                                      \
    (                                                                   \
        (TYPE) == DISC_PARAMETER_TVISC0 ? DISC_TVISC0_MIN :             \
        (TYPE) == DISC_PARAMETER_RIN ? DISC_RIN_MIN :                   \
        (TYPE) == DISC_PARAMETER_ROUT ? DISC_RIN_MIN :                  \
        1e-100                                                          \
        )


/*
 * We have to be careful with the max of the parameters.
 *
 * Simply allowing them to be a "large number" messes
 * up the various root finding algorithms (which fail to
 * deal well with numbers like 1e300)
 */
#define Disc_parameter_max(D,TYPE)                                      \
    (                                                                   \
        (TYPE) == DISC_PARAMETER_TVISC0 ? DISC_TVISC0_MAX :             \
        (TYPE) == DISC_PARAMETER_RIN ? Min(((D)->Rout*_JUST_LESS),      \
                                           DISC_LARGE_RADIUS):          \
        (TYPE) == DISC_PARAMETER_ROUT ? Min(DISC_LARGE_RADIUS,          \
                                            DISC_ROUT_MAX) :            \
        1e+100                                                          \
        )


#endif // DISC_CONSTRAINTS_H
