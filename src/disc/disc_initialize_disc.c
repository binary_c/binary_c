#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS

void disc_initialize_disc(struct stardata_t * const stardata,
                          struct disc_t * const disc,
                          struct binary_system_t * const binary_in)
{
    /*
     * Initialize the structure of the disc.
     *
     * In particular, set up the internal binary
     * star information structure,
     * and any other physical parameters.
     *
     * The binary structure is allocated if required,
     * in which case binary_in should be NULL,
     * otherwise binary_in is used.
     */

    /*
     * Assume the molecular weight of the
     * mixture, if we know it, assuming it is
     * atomic (not ionised).
     *
     * If we don't know, assume it is hydrogen,
     * i.e. mu = m_proton.
     */
    disc->mu =
#ifdef NUCSYN
        nucsyn_molecular_weight(
            disc->X,
            stardata,
            GAS_ATOMIC
            ) *
#endif // NUCSYN
        M_PROTON;

#if 0
    /*
     * Debug molecular weight calculators
     */
    Exit_binary_c(2,
                  "Disc mu %g : molecular atomic %g plasma %g effective plasma %g approx %g\n",
                  disc->mu,
                  nucsyn_molecular_weight(
                      disc->X,
                      stardata,
                      GAS_ATOMIC
                      ) ,
                  nucsyn_molecular_weight(
                      disc->X,
                      stardata,
                      GAS_FULLY_IONISED_PLASMA
                      ) ,
                  nucsyn_effective_molecular_weight(
                      disc->X,
                      MAIN_SEQUENCE,
                      stardata->store->molweight
                      ),
                  4.0/(6.0*disc->X[XH1]+disc->X[XHe4]+2.0)
        );
#endif

    disc->solver = DISC_SOLVER_NONE;
    disc->iteration = 0;
#ifdef DISC_USE_PREVZONE
    disc->prevzone = DISC_NO_PREVZONE_YET;
#endif
#ifdef MEMOIZE
    if(disc->memo==NULL)
    {
        memoize_initialize(&disc->memo);
    }
#endif

    /*
     * If we're passed in a binary structure,
     * or need some debug output, set up the binary structure.
     */
    if(DISC_DEBUG || binary_in!=NULL)
    {
        struct binary_system_t * binary =
            binary_in != NULL ? binary_in :
            Malloc(sizeof(struct binary_system_t));

        disc_init_binary_structure(stardata,binary,disc);

        if(DISC_DEBUG)
        {
            disc_show_disc(stardata,
                           disc,
                           binary);
        }

        if(binary != binary_in)
        {
            Safe_free(binary);
        }
    }

    /*
     * Converge once
     */
    evolve_disc(stardata,disc,0);

    return;
}



#endif//DISCS
