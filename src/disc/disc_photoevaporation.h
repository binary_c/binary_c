#pragma once
#ifndef DISC_PHOTOEVAPORATION_H
#define DISC_PHOTOEVAPORATION_H

/*
 * Radii over which the Owen 2012 expressions
 * are to be normalized
 */
#define OWEN2012_INNER_RADIUS 0.0
#define OWEN2012_OUTER_RADIUS ASTRONOMICAL_UNIT*1.65*57.0


#define PHOTOEVAPORATION_GSL_INTEGRAL_TOLERANCE 1e-4

#define tmdot(R)                                                        \
    (disc_Owen_2012_mass_loss_timescale(disc,binary,(R)) /              \
     Max(1e-100,stardata->preferences->cbdisc_mass_loss_Xray_multiplier))

#define tvisc(R)                                \
    (disc_viscous_timescale((R),disc,binary))

static double Owen2012_angmom_f(double x,
                                void *params);

static double disc_Owen_2012_clearing_radius_bisector(const double r,
                                                      void * p);
static double Owen2012_sigmadot_f(double x,
                           void *params);


#endif // DISC_PHOTOEVAPORATION_H
