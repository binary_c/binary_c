#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS

void evolve_disc(struct stardata_t * const stardata,
                 struct disc_t * const disc,
                 const double dtsecs)
{
    /*
     * Evolve a disc for the current binary_c timestep
     */
    Discdebug(3,"Conv beg evolve Tvisc0 = %g\n",disc->Tvisc0);
    Discdebug(2,
              "evolve disc for dtsecs = %g = %g years\n",
              dtsecs,
              dtsecs/YEAR_LENGTH_IN_SECONDS);
    Discdebug(2,
              "t=%g disc time=%g R=%g RL=%g a=%g\n",
              stardata->model.time,
              (dtsecs+disc->lifetime) / YEAR_LENGTH_IN_SECONDS,
              stardata->star[0].radius,
              stardata->star[0].roche_radius,
              stardata->common.orbit.separation
        );
    Discdebug(1,"EV t=%g dt=%g\n",
              disc->lifetime/YEAR_LENGTH_IN_SECONDS,
              dtsecs/YEAR_LENGTH_IN_SECONDS);

    if(stardata->preferences->solver != SOLVER_FORWARD_EULER)
    {
        Exit_binary_c(BINARY_C_ALGORITHM_OUT_OF_RANGE,
                      "The discs algorithm has not been tested with solvers other than Forward-Euler. Please only use this solver.");
    }

    disc->first = disc->first==TRUE ? TRUE : Boolean_(Is_zero(disc->lifetime));

    disc->dT = 0.0;
    Clear_disc_feedback(disc);

    if(Disc_is_disc(disc))
    {
        /*
         * Evolve the disc
         */
        Discdebug(2,
                  "No RLOF : evolve disc for %g years\n",
                  dtsecs/YEAR_LENGTH_IN_SECONDS);

        Discdebug(3,"Conv precall converged? %s Tvisc0 = %g M = %g Msun\n",
                  Yesno(disc->converged),
                  disc->Tvisc0,
                  disc->M/M_SUN);
        const double ret = disc_evolve_disc_structure(stardata,
                                                      disc,
                                                      dtsecs);
        Discdebug(3,"Conv postcall converged? %s Tvisc0 = %g M = %g\n",
                  Yesno(disc->converged),
                  disc->Tvisc0,
                  disc->M/M_SUN);
        // binary structure
        struct binary_system_t binary;
        disc_init_binary_structure(stardata,&binary,disc);

        if(Is_not_zero(ret))
        {
            /*
             * Check and output the disc properties to make
             * sure the code is working
             */

            disc_convergence_status(stardata,
                                    "post-init-binary-structure",
                                    disc,
                                    &binary);

            /* choose the most evolved star */
            const Star_number nstar = stardata->star[0].stellar_type > stardata->star[1].stellar_type ? 0 : 1;

            Discdebug(2,
                      "disc %d, tdisc=%g dtdisc=%g years : nstar=%d type=%d T*=%g K L*=%g Lsun M* = %g Msun (Menv=%g Mc=%g) : Tin=%g K, Rin = %g Rsun = %g AU, Rout = %g Rsun = %g AU,  R* = %g : Hin/Rin = %g\n",
                      disc->ndisc,
                      disc->lifetime/YEAR_LENGTH_IN_SECONDS,
                      dtsecs/YEAR_LENGTH_IN_SECONDS,
                      nstar,
                      stardata->star[0].stellar_type,
                      Teff(nstar),
                      binary.L/L_SUN,
                      stardata->star[0].mass,
                      envelope_mass(&stardata->star[0]),
                      Outermost_core_mass(&stardata->star[0]),
                      disc_inner_edge_temperature(disc),
                      disc->Rin/R_SUN,
                      disc->Rin/ASTRONOMICAL_UNIT,
                      disc->Rout/R_SUN,
                      disc->Rout/ASTRONOMICAL_UNIT,
                      binary.Rstar/R_SUN,
                      disc_scale_height(disc->Rin,disc,&binary)/disc->Rin);
        }

        /*
         * If the disc has evaporated on the first timestep, it never had
         * a stable structure. Log this.
         */
        if(Is_zero(disc->lifetime) &&
           !Disc_is_disc(disc))
        {
            Evaporate_disc(disc,"Disc failed to converge on first timestep");
        }
        else
        {
            /*
             * Check for failure
             */
            char * failure_reason = disc_failure_mode(stardata,
                                                      disc,
                                                      &binary,
                                                      0);
            if(failure_reason)
            {
                Append_logstring(LOG_DISC,
                                 "Evaporate disc (M==%g) age %g y",
                                 disc->M,
                                 disc->lifetime/YEAR_LENGTH_IN_SECONDS);
                Evaporate_disc(disc,failure_reason);
            }
            Safe_free(failure_reason);
        }
    }
    else
    {
        /*
         * If disc is no longer evolving, set its
         * timestep to be very long, and remove its
         * mass and angular momentum.
         */
        Discdebug(2,
                  "disc lifetime %g exceeds DISC_MAX_LIFETIME = %g : set very long timestep\n",
                  disc->lifetime,
                  DISC_MAX_LIFETIME);
        disc->dt = LONG_TIMESTEP;
        Evaporate_disc(disc,"Disc lifetime exceeds limit");
    }
    Discdebug(3,"Conv end evolve Tvisc0 = %g\n",disc->Tvisc0);
}

#endif // DISCS
