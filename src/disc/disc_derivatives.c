#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS
/*
 * Calculate the derivatives of disc properties,
 * usually gain or loss of mass, angular momentum and
 * eccentricity, according to various physical processes.
 *
 * If edges is TRUE, then also calculate edge stripping
 * radii.
 *
 * Also set these values into stardata where appropriate
 * to apply changes to the binary system.
 *
 * Note that the disc structure passed may not be converged.
 *
 * We require 0 < multiplier < 1
 */

void disc_derivatives(struct disc_t * const disc,
                      const struct binary_system_t * const binary,
                      struct stardata_t * const stardata,
                      const double multiplier,
                      const Boolean edges)
{
    /*
     * Update derived variables, just in case
     */
    disc_calculate_derived_variables(stardata,disc,binary);

    /*
     * Reset evaporation boundaries : they'll
     * be recalculated below
     */
    if(edges == TRUE)
    {
        Discdebug(2,"Reset Revaps\n");
        if(stardata->preferences->cbdisc_outer_edge_stripping == FALSE)
        {
            disc->Revap_out = 0.0;
        }
        if(stardata->preferences->cbdisc_inner_edge_stripping == FALSE)
        {
            disc->Revap_in = 0.0;
        }
    }

    if(Disc_is_disc(disc))
    {
        /*
         * Radius the disc would have, were it a ring
         */
        const double Rring Maybe_unused = disc_ring_radius(disc,binary);

        /*
         * Specific angular momentum at the disc's inner edge
         */
        const double h_inner_edge = Pow2(disc->Rin) *
            disc_orbital_frequency(disc->Rin,binary);

        Discdebug(2,"L2 cross check\n");

        if(Is_really_not_zero(stardata->preferences->cbdisc_mass_loss_inner_L2_cross_multiplier))
        {
            /*
             * Inner edge inside L2 loss (NB this is negative).
             */

            /*
             * Apply mass loss if the disc is inside L2
             */
            Discdebug(1,
                      "Rin = %g %s cf aL2 = %g %s\n",
                      Solar(disc->Rin,R),
                      binary->aL2/R_SUN,
                      R_SOLAR);

            if(disc->Rin < binary->aL2)
            {
                if(stardata->preferences->cbdisc_viscous_L2_coupling == FALSE)
                {
                    /*
                     * If we aren't coupling viscously to the inner edge,
                     * we must estimate the mass and angular momentum loss here.
                     *
                     * This expression is very ad-hoc!
                     */
                    const double mdot = -
                        stardata->preferences->cbdisc_mass_loss_inner_L2_cross_multiplier *
                        Max(0.0,log10(binary->aL2 / disc->Rin))
                        * multiplier * M_SUN / YEAR_LENGTH_IN_SECONDS;

                    disc->loss[DISC_LOSS_INNER_L2_CROSSING].mdot = mdot;
                    disc->loss[DISC_LOSS_INNER_L2_CROSSING].jdot = mdot * h_inner_edge;
                    disc->loss[DISC_LOSS_INNER_L2_CROSSING].edot = 0.0;
                }
                else
                {
                    /*
                     * (On the first timestep?) enforce mass
                     * of material inside Revap_in if
                     * binary->aL2 is inside Rin.
                     */
                    disc->suppress_viscous_inflow = FALSE;

                    if(edges == TRUE)
                    {
                        double Revap = binary->aL2 * multiplier +
                            (1.0 - multiplier) * disc->Rin;

                        /*
                         * Scale if necessary
                         */
                        if(!Fequal(stardata->preferences->cbdisc_mass_loss_inner_L2_cross_multiplier,
                                   1.0))
                        {
                            const double dm = disc_partial_mass(disc,
                                                                disc->Rin,
                                                                Revap)
                                * stardata->preferences->cbdisc_mass_loss_inner_L2_cross_multiplier;
                            Revap = disc_mass_radius(stardata,
                                                     disc,
                                                     binary,
                                                     dm);
                            Revap = multiplier * Revap +
                                (1.0 - multiplier) * disc->Rin;
                        }

                        disc->Revap_in = Max(disc->Revap_in,
                                             Revap);

                        Discdebug(1,
                                  "\ncoupled L2 at Revap_in = %g Rsun (L2 = %g Rsun, Rin = %g Rsun, Rout = %g Rsun)\n",
                                  disc->Revap_in / R_SUN,
                                  binary->aL2 / R_SUN,
                                  disc->Rin / R_SUN,
                                  disc->Rout / R_SUN);
                    }
                    disc->loss[DISC_LOSS_INNER_L2_CROSSING].mdot = 0.0;
                    disc->loss[DISC_LOSS_INNER_L2_CROSSING].jdot = 0.0;
                    disc->loss[DISC_LOSS_INNER_L2_CROSSING].edot = 0.0;
                }
            }

            Discdebug(1,
                      "L2 : sep = %g, aL2 = %g, Rin = %g, Rout = %g, Rring = %g, Revap_in = %g (Rsun) : mdot = %g, jdot = %g (Mdisc %g, Jdisc %g)\n",
                      binary->separation / R_SUN,
                      binary->aL2 / R_SUN,
                      disc->Rin / R_SUN,
                      disc->Rout / R_SUN,
                      Rring / R_SUN,
                      disc->Revap_in / R_SUN,
                      disc->loss[DISC_LOSS_INNER_L2_CROSSING].mdot / (M_SUN / YEAR_LENGTH_IN_SECONDS),
                      disc->loss[DISC_LOSS_INNER_L2_CROSSING].jdot,
                      disc->M / M_SUN,
                      disc->J
                );
        }
        else
        {
            disc->loss[DISC_LOSS_INNER_L2_CROSSING].mdot = 0.0;
            disc->loss[DISC_LOSS_INNER_L2_CROSSING].jdot = 0.0;
            disc->loss[DISC_LOSS_INNER_L2_CROSSING].edot = 0.0;
        }

        /*
         * FUV and X-ray winds
         */
        disc->loss[DISC_LOSS_FUV].mdot = 0.0;
        disc->loss[DISC_LOSS_FUV].jdot = 0.0;
        disc->loss[DISC_LOSS_FUV].edot = 0.0;
        disc->loss[DISC_LOSS_XRAY].mdot = 0.0;
        disc->loss[DISC_LOSS_XRAY].jdot = 0.0;
        disc->loss[DISC_LOSS_XRAY].edot = 0.0;

        Discdebug(2,
                  "Disc is disc? dt = %g, %d, Xray mult %g, coupling? %d, edges? %d\n",
                  disc->dt/YEAR_LENGTH_IN_SECONDS,
                  Disc_is_disc(disc),
                  stardata->preferences->cbdisc_mass_loss_Xray_multiplier,
                  stardata->preferences->cbdisc_viscous_photoevaporative_coupling,
                  edges);

        if(Disc_is_disc(disc) &&
           Is_really_not_zero(stardata->preferences->cbdisc_mass_loss_Xray_multiplier))
        {
            /*
             * Set up the Owen+ (2012) disc evaporation function
             * for the current disc and binary system.
             */
            disc_Owen_2012_normalize(stardata,disc,binary);

            if(Is_really_not_zero(disc->Owen2012_norm))
            {

                /*
                 * If we are coupling the viscous evolution of the disc
                 * to the wind, find the radius inside which the disc
                 * should be evaporated.
                 */
                if(stardata->preferences->cbdisc_viscous_photoevaporative_coupling !=
                   CBDISC_VISCOUS_PHOTOEVAPORATIVE_COUPLING_NONE)
                {
                    if(edges == TRUE)
                    {
                        /*
                         * Enforce X-ray wind loss through coupling with the
                         * inner edge
                         */
                        Discdebug(2,
                                  "Find clearing radius \n");

#define METHOD_SINGLE_RADIUS 0
#define METHOD_DUAL_RADII 1

                        const int method = METHOD_DUAL_RADII;

                        Discdebug(2,
                                  "Find clearing %s \n",
                                  method == METHOD_SINGLE_RADIUS ? "radius" : "radii"
                            );

                        if(method==0)
                        {

                            const double a_clear =
                                disc_Owen_2012_clearing_radius(stardata,
                                                               disc,
                                                               binary);

                            Discdebug(2,
                                      "Clearing radius is %g %s\n",
                                      Solar(a_clear,R));

                            if(a_clear > disc->Rin + (1.0 + TINY))
                            {
                                const double Revap = a_clear * multiplier +
                                    disc->Rin * (1.0 - multiplier);
                                disc->Revap_in = Max(disc->Revap_in,
                                                     Revap);
                            }
                        }
                        else
                        {
                            double a_clear_in,a_clear_out;
                            disc_Owen_2012_clearing_radii(stardata,
                                                          disc,
                                                          binary,
                                                          &a_clear_in,
                                                          &a_clear_out);


                            Discdebug(2,
                                      "disc lifetime=%30.20e dt=%g -> Clearing radii are in : %g %s (Rin %g %s), out : %g %s M=%g\n",
                                      disc->lifetime / YEAR_LENGTH_IN_SECONDS,
                                      disc->dt / YEAR_LENGTH_IN_SECONDS,
                                      Solar(a_clear_in,R),
                                      Solar(disc->Rin,R),
                                      Solar(a_clear_out,R),
                                      disc->M);

                            if(!Fequal(a_clear_in, disc->Rin))
                            {
                                const double Revap = a_clear_in * multiplier +
                                    disc->Rin * (1.0 - multiplier);

                                if(stardata->preferences->cbdisc_viscous_photoevaporative_coupling == CBDISC_VISCOUS_PHOTOEVAPORATIVE_COUPLING_INSTANT)
                                {
                                    /*
                                     * Instantly remove material out to disc->Revap_in
                                     */
                                    disc->Revap_in = Max(disc->Revap_in,
                                                         Revap);
                                    disc->loss[DISC_LOSS_INNER_EDGE_SLOW].mdot = 0.0;
                                    disc->loss[DISC_LOSS_INNER_EDGE_SLOW].jdot = 0.0;
                                }
                                else if(stardata->preferences->cbdisc_viscous_photoevaporative_coupling == CBDISC_VISCOUS_PHOTOEVAPORATIVE_COUPLING_VISCOUS)
                                {
                                    /*
                                     * Viscous timescale "slow" loss
                                     */
                                    disc->Revap_in = 0.0;
                                    if(Revap > disc->Rin)
                                    {
                                        disc->loss[DISC_LOSS_INNER_EDGE_SLOW].mdot = - 2.0 * PI * Pow2(Revap) *
                                            disc_column_density(Revap,disc) / disc_viscous_timescale(Revap,disc,binary);
                                        disc->loss[DISC_LOSS_INNER_EDGE_SLOW].jdot =
                                            0* disc->loss[DISC_LOSS_INNER_EDGE_SLOW].mdot *
                                            disc_specific_angular_momentum(Revap,binary);
                                        disc->Revap_in = disc->Rin;
                                    }
                                    else
                                    {
                                        disc->loss[DISC_LOSS_INNER_EDGE_SLOW].mdot = 0.0;
                                        disc->loss[DISC_LOSS_INNER_EDGE_SLOW].jdot = 0.0;
                                    }
                                }
                                else
                                {
                                    Exit_binary_c(BINARY_C_ALGORITHM_OUT_OF_RANGE,
                                                  "Unknown cbdisc viscous photoevaporative coupling %d\n",
                                                  stardata->preferences->cbdisc_viscous_photoevaporative_coupling);
                                }
                            }

                            if(!Fequal(a_clear_out, disc->Rout))
                            {
                                const double Revap = a_clear_out * multiplier +
                                    disc->Rout * (1.0 - multiplier);
                                disc->Revap_out = Max(disc->Revap_out,
                                                      Revap);
                            }
                        }

                        Discdebug(2,
                                  "OWEN VISC EVAP Revap_in = %g %s (Rin %g %s Rout %g %s)\n",
                                  Solar(disc->Revap_in,R),
                                  Solar(disc->Rin,R),
                                  Solar(disc->Rout,R));
                    }
                }
            }
            else
            {
                disc->suppress_viscous_inflow = FALSE;
            }
        }
        else
        {
            disc->suppress_viscous_inflow = FALSE;
        }

        /*
         * Owen et al. (2012) give d(Sigma)/dt. Use this to calculate
         * the mass loss rate from the disc between Rin and Rout,
         * and take the angular momentum from the whole disc.
         */
        if(Is_really_not_zero(stardata->preferences->cbdisc_mass_loss_Xray_multiplier) &&
           Is_really_not_zero(disc->Owen2012_norm))
        {
            double mdot = disc_Owen_2012_sigmadot_integral(disc,
                                                           binary,
                                                           (Is_zero(disc->Revap_in) ? disc->Rin : Min(disc->Revap_in,disc->Rin)),
                                                           disc->Rout);
            Discdebug(2,
                      "OWEN wants %g Msun/y (Revap_in = %g, Rin = %g, Rout = %g) (Rsun)\n",
                      mdot / M_SUN * YEAR_LENGTH_IN_SECONDS,
                      disc->Revap_in / R_SUN,
                      disc->Rin / R_SUN,
                      disc->Rout / R_SUN);

            double jdot = disc_Owen_2012_angmom_integral(
                stardata,
                disc,
                binary,
                (Is_zero(disc->Revap_in) ? disc->Rin : Min(disc->Revap_in,disc->Rin)),
                disc->Rout);

            mdot *= stardata->preferences->cbdisc_mass_loss_Xray_multiplier;
            jdot *= stardata->preferences->cbdisc_mass_loss_Xray_multiplier;

            if(DISC_DEBUG>=2)
            {
                const double inf = disc_Owen_2012_sigmadot_integral(disc,
                                                                    binary,
                                                                    0.0,
                                                                    disc->Rout);
                Discdebug(2,
                          "OWEN mdot = %g Msun/y (over infinite disc %g)\n",
                          mdot/M_SUN*YEAR_LENGTH_IN_SECONDS,
                          inf/M_SUN*YEAR_LENGTH_IN_SECONDS);
                Discdebug(2,
                          "OWEN jdot = %g : timescale %g / %g = %g y\n",
                          jdot,
                          disc->J,
                          jdot,
                          -disc->J/jdot/YEAR_LENGTH_IN_SECONDS);
            }

            disc->loss[DISC_LOSS_XRAY].mdot = multiplier * mdot;
            disc->loss[DISC_LOSS_XRAY].jdot = multiplier * jdot;

            Discdebug(2,
                      "mdot Xray outside Revap_in = %g, jdot = %g\n",mdot,jdot);
        }
        else
        {
            disc->loss[DISC_LOSS_XRAY].mdot = 0.0;
            disc->loss[DISC_LOSS_XRAY].jdot = 0.0;
        }

        Discdebug(2,
                  "Saving some disc properties Rin=%g %s Rout=%g %s Revap_in=%g %s Revap_out=%g %s J=%g\n",
                  Solar(disc->Rin,R),
                  Solar(disc->Rout,R),
                  Solar(disc->Revap_in,R),
                  Solar(disc->Revap_out,R),
                  disc->J);


        /*
         * Edge-stripping derivatives are set and applied
         * in disc_edge_stripping, but need to be initialized
         * here.
         */
        disc->loss[DISC_LOSS_INNER_EDGE_STRIP].mdot = 0.0;
        disc->loss[DISC_LOSS_INNER_EDGE_STRIP].jdot = 0.0;
        disc->loss[DISC_LOSS_INNER_EDGE_STRIP].edot = 0.0;
        disc->loss[DISC_LOSS_OUTER_EDGE_STRIP].mdot = 0.0;
        disc->loss[DISC_LOSS_OUTER_EDGE_STRIP].jdot = 0.0;
        disc->loss[DISC_LOSS_OUTER_EDGE_STRIP].edot = 0.0;

        Discdebug(2,
                  "Is disc wind dominated? %s\n",
                  Yes_or_no(disc->suppress_viscous_inflow));


        Discdebug(2,
                  "Global derivatives (disc: M=%g J=%g)\n",
                  disc->M/M_SUN,
                  disc->J);

        /*
         * Derivatives which change global disc structure
         */
        {
            /*
             * Global disc mass loss and associated angular momentum loss.
             * NB This is negative and is given as Msun/year
             * in stardata->preferences.
             */
            double mdot = - M_SUN / YEAR_LENGTH_IN_SECONDS *
                stardata->preferences->cbdisc_mass_loss_constant_rate;
            disc->loss[DISC_LOSS_GLOBAL].mdot = multiplier * mdot;
            disc->loss[DISC_LOSS_GLOBAL].jdot = multiplier * disc->J / disc->M * mdot;
            disc->loss[DISC_LOSS_GLOBAL].edot = 0.0;
        }

        Discdebug(2,"Torque\n");
        {
            /*
             * Torque because of the inner binary
             *
             * NB This is positive angular momentum transfer from the binary
             * to the disc.
             *
             * NB We should use the preferences->cbdisc_torquef parameter
             * here, not the disc's torquef (which may be used to set Rin=Revap_in).
             */
            disc->loss[DISC_LOSS_BINARY_TORQUE].mdot = 0.0;
            disc->loss[DISC_LOSS_BINARY_TORQUE].jdot = 1.0 * stardata->preferences->cbdisc_torquef *
                disc_binary_angular_momentum_flux(disc,binary);
            disc->loss[DISC_LOSS_BINARY_TORQUE].edot = 0.0;

            /*
             * Perhaps resonance eccentricity pumping
             */
#ifdef CBDISC_ECCENTRICITY_PUMPING
            disc->loss[DISC_LOSS_RESONANCES].mdot = 0.0;
            cbdisc_eccentricity_pumping_rate(stardata,
                                             disc,
                                             binary,
                                             &disc->loss[DISC_LOSS_RESONANCES].edot,
                                             &disc->loss[DISC_LOSS_RESONANCES].jdot);
#else
            disc->loss[DISC_LOSS_RESONANCES].mdot = 0.0;
            disc->loss[DISC_LOSS_RESONANCES].jdot = 0.0;
            disc->loss[DISC_LOSS_RESONANCES].edot = 0.0;
#endif
        }


        Discdebug(2,"Viscous\n");

        if(Is_not_zero(1+stardata->preferences->cbdisc_mass_loss_inner_viscous_multiplier)
           &&
           disc->suppress_viscous_inflow == FALSE)
        {
            /*
             * Inner edge mass viscous loss which falls onto the binary.
             * NB this is negative.
             */
            double mdot = -disc_mass_inflow_rate(disc->Rin,disc,binary) *
                stardata->preferences->cbdisc_mass_loss_inner_viscous_multiplier;
            disc->loss[DISC_LOSS_INNER_VISCOUS].mdot = mdot * multiplier;
            disc->loss[DISC_LOSS_INNER_VISCOUS].jdot = mdot * multiplier * h_inner_edge *
                stardata->preferences->cbdisc_mass_loss_inner_viscous_angular_momentum_multiplier;
            disc->loss[DISC_LOSS_INNER_VISCOUS].edot = 0.0;
        }
        else
        {
            disc->loss[DISC_LOSS_INNER_VISCOUS].mdot = 0.0;
            disc->loss[DISC_LOSS_INNER_VISCOUS].jdot = 0.0;
            disc->loss[DISC_LOSS_INNER_VISCOUS].edot = 0.0;
        }

        Discdebug(2,"Ram strip\n");

        if(Is_not_zero(stardata->preferences->cbdisc_mass_loss_ISM_ram_pressure_multiplier))
        {
            /*
             * ISM disc truncation by ram stripping.
             */
            double P_ISM = stardata->preferences->cbdisc_mass_loss_ISM_pressure*BOLTZMANN_CONSTANT;

            if(stardata->preferences->cbdisc_outer_edge_stripping == FALSE)
            {
                /*
                 * Apply RAM-stripping rate as a smoothed, global rate.
                 *
                 * Calculate ISM pressure and outer edge gravitational pressure.
                 *
                 * x is the ratio of the ISM pressure to gravitational pressure
                 * at the outer edge of the disc.
                 */

                double P_grav = disc_gravitational_pressure(disc->Rout,disc,binary);
                double x = P_ISM / P_grav;
                Discdebug(2,
                          "PISM = %g (=%g k/K) Pgrav = %g ",
                          P_ISM,
                          P_ISM / BOLTZMANN_CONSTANT,
                          P_grav);

                /*
                 * Set smoothing to TRUE usually, otherwise there may be
                 * a spiky ISM mass loss rate because of numerical instability.
                 */
                const Boolean smoothing = TRUE;

                /*
                 * Smoothing: apply if ISM pressure is within 1e-3 of the gravitational pressure
                 *
                 * No-smoothing: apply if ISM pressure exceeds gravitational pressure
                 */
                if((smoothing==TRUE && x>1e-3)
                   ||
                   (smoothing==FALSE && More_or_equal(x,1.0)))
                {
                    /* smoothing factor : prevents oscillation */
                    double fsmooth = smoothing==TRUE ?
                        1.0/(1.0 + pow(1e-15, x - 1.0)) : 1.0;
                    /*
                     * Find the radius at which we should lose mass, and then
                     * the mass and angular momentum outside that radius which should
                     * be stripped.
                     */



                    /*
                     * If we apply smoothing, increase the pressure target so that
                     * its radius is inside the disc. fsmooth<=1 so P_ISM/fsmooth is
                     * larger (or equal to) than P_ISM, always.
                     */
                    double Ptarget = smoothing==TRUE ?
                        (P_ISM/Max(1e-3,Min(fsmooth,1.0))) : P_ISM;

                    double P = disc_pressure(disc->Rout,disc,binary);
                    double R = disc_pressure_radius(stardata,disc,binary,Ptarget);
                    double M = disc_partial_mass(disc,R,disc->Rout);
                    double J = disc_partial_angular_momentum(disc,binary,R,disc->Rout);
                    double f = - stardata->preferences->cbdisc_mass_loss_ISM_ram_pressure_multiplier;

                    disc->PISM = Ptarget;
                    disc->RISM = R;

                    /* stability factor */
                    f *= fsmooth * pow(P_ISM/P,0.5);

                    disc->loss[DISC_LOSS_ISM].mdot = multiplier * f * M / disc->dt;
                    disc->loss[DISC_LOSS_ISM].jdot = multiplier * f * J / disc->dt;
                    disc->loss[DISC_LOSS_ISM].edot = 0.0;

                    /* lose at most 10% of the disc in one timestep */
                    double f0 =
                        Min(0.1,
                            - disc->loss[DISC_LOSS_ISM].mdot * disc->dt /
                            disc->M);

                    disc->loss[DISC_LOSS_ISM].mdot *= f0;
                    disc->loss[DISC_LOSS_ISM].jdot *= f0;

                    Discdebug(2,
                              "ram stripping via global : mdot = %g * %g (=%g Msun) / %g = %g (Msun/year) (f=%g, Pout=%g, Ptarget=%g, Rtarget=%g [disc Rin = %g, Rout = %g] Moutside=%g Msun, Joutside=%g) mass lost = %g cf disc mass %g, f0 = %g\n",
                              f,
                              M,
                              M/M_SUN,
                              disc->dt,
                              disc->loss[DISC_LOSS_ISM].mdot/(M_SUN/YEAR_LENGTH_IN_SECONDS),
                              f,
                              P,
                              Ptarget,
                              R,
                              disc->Rin,
                              disc->Rout,
                              M/M_SUN,
                              J,
                              disc->loss[DISC_LOSS_ISM].mdot * disc->dt
                              / M_SUN,
                              disc->M / M_SUN,
                              f0
                        );
                }
            }
            else
            {
                if(edges == TRUE)
                {
                    /*
                     * Apply RAM stripping by directly specifying Revap_out.
                     *
                     * Interpolate according to the multiplier.
                     */
                    double R =
                        disc_pressure_radius(stardata,
                                             disc,binary,P_ISM) * multiplier
                        +
                        disc->Rout * (1.0 - multiplier);

                    // test function
                    //R = Max(1e5,1e6 - disc->lifetime/YEAR_LENGTH_IN_SECONDS*100.0) * R_SUN;
                    disc->Revap_out =
                        Is_really_zero(disc->Revap_out) ? R : Min(disc->Revap_out,R);

                    disc->RISM = R;
                    disc->loss[DISC_LOSS_ISM].mdot = 0.0;
                    disc->loss[DISC_LOSS_ISM].jdot = 0.0;
                    disc->loss[DISC_LOSS_ISM].edot = 0.0;
                    Discdebug(2,
                              "\nSTRIP t=%g Ram stripping via Revap : PISM %g == RISM %g cf Rout %g (m out there %g = %5.2f %%, disc M %g) Revap_out = %g\n",
                              disc->lifetime,
                              P_ISM,R,disc->Rout,
                              disc_partial_mass(disc,R,disc->Rout)/M_SUN,
                              100.0*disc_partial_mass(disc,R,disc->Rout)/disc->M,
                              disc->M/M_SUN,
                              disc->Revap_out
                        );
                }
            }
        }
        else
        {
            disc->loss[DISC_LOSS_ISM].mdot = 0.0;
            disc->loss[DISC_LOSS_ISM].jdot = 0.0;
            disc->loss[DISC_LOSS_ISM].edot = 0.0;
        }

        /*
         * Do not allow any one loss mechanism to take
         * more than 10% of the mass.
         */
        int l;
        if(0)
        {
            double mdot_limit = 0.1 * disc->M / disc->dt;
            for(l=0;l<DISC_LOSS_N;l++)
            {
                Discdebug(2,
                          "%d : CF Mdot = %g vs limit %g\n",
                          l,
                          - disc->loss[l].mdot,
                          mdot_limit);

                if(- disc->loss[l].mdot > mdot_limit)
                {
                    double f = mdot_limit / -disc->loss[l].mdot;
                    disc->loss[l].mdot *= f;
                    disc->loss[l].jdot *= f;
                    disc->loss[l].edot *= f;
                }
            }
            printf("Mdot max %g Msun/y\n",mdot_limit/M_SUN*YEAR_LENGTH_IN_SECONDS);
        }


#ifdef __DERIVDEBUG
        Discdebug(2,"done derivs");
        for(l=0;l<DISC_LOSS_N;l++)
        {
            /*
              disc->loss[l].mdot = 0.0;
              disc->loss[l].jdot = 0.0;
              disc->loss[l].edot = 0.0;
            */
            Show_disc_derivative(l);
        }
        printf("Evaporation from edges: Revap_in = %g, Revap_out = %g, Jdot evap %g (cf Flux %g)\n",
               disc->Revap_in/R_SUN,
               disc->Revap_out/R_SUN,
               disc->F_stripping_correction,
               disc->F);
#endif
    }

    /*
     * Check edge stripping:
     *
     * If it is off, set the appropriate radius to 0.0 so it is ignored.
     */
    if(stardata->preferences->cbdisc_outer_edge_stripping == FALSE)
    {
        disc->Revap_out = 0.0;
    }

    if(stardata->preferences->cbdisc_inner_edge_stripping == FALSE)
    {
        disc->Revap_in = 0.0;
    }

    /*
     * Appendix B3: Angular momentum flux correction because
     * of mass loss
     */
    disc_edge_loss_angular_momentum_flux(disc,
                                         binary);
}

#endif // DISCS
