#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS

void disc_status(const struct stardata_t * const stardata)
{
    /*
     * Show information about discs in the system
     */
    Boolean shown_header = FALSE;
    Foreach_star(star)
    {
        printf("Discs around %16s % 3d : % 3d\n","star",
               star->starnum,
               ndiscs(DISC_CIRCUMSTELLAR,
                      star));
        if(star->ndiscs>0)
        {
            int i;

            if(shown_header==FALSE)
            {
                shown_header=TRUE;
                printf("\n%3s %20s   %12s %12s %12s\n",
                       "n","pointer","Mdisc","Rin","Rout");
            }

            Discloop(star,i)
            {
                struct disc_t * const disc = (struct disc_t * const) star->discs[i];
                printf("% 3d %20p : % 12g % 12g % 12g\n",
                       i,
                       (void*)disc,
                       disc->M,
                       disc->Rin,
                       disc->Rout);
            }
            printf("[ inner = %20p, outer = %20p, mass in circumstellar discs = %g ]\n",
                   (void*)inner_disc(DISC_CIRCUMSTELLAR,star),
                   (void*)outer_disc(DISC_CIRCUMSTELLAR,star),
                   mass_in_discs(DISC_CIRCUMSTELLAR,star)
                );
        }
    }

    struct common_t * const common = (struct common_t * const) &(stardata->common);
    printf("Discs around %20s : % 3d\n","the binary",ndiscs(DISC_CIRCUMBINARY,stardata));
    if(common->ndiscs>0)
    {
        int i;

        if(shown_header==FALSE)
        {
            shown_header=TRUE;
            printf("%3s %20s    %12s %12s %12s\n",
                   "n","pointer","Mdisc","Rin","Rout");
        }

        Discloop(&(stardata->common),i)
        {
            struct disc_t * const disc = (struct disc_t * const) &(common->discs[i]);
            printf("% 3d %20p : % 12g % 12g % 12g\n",
                   i,
                   (void*)disc,
                   disc->M,
                   disc->Rin,
                   disc->Rout);
        }

        printf("[ inner = %20p, outer = %20p, mass in circumbinary discs = %g ]\n",
               (void*)inner_disc(DISC_CIRCUMBINARY,&(stardata->common)),
               (void*)outer_disc(DISC_CIRCUMBINARY,&(stardata->common)),
               mass_in_discs(DISC_CIRCUMBINARY,&(stardata->common))
            );
    }

    if(shown_header==FALSE)
    {
        printf("System has no discs\n");
    }

}


#endif //DISCS
