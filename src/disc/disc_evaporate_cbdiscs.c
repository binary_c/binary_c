#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS
void disc_evaporate_cbdiscs(struct stardata_t * const stardata)
{
    /*
     * The binary has disrupted, or merged, so evaporate
     * any circumbinary discs
     */
    int i;
    for(i=0; i<stardata->common.ndiscs; i++)
    {
        if(stardata->common.discs[i].M > 0.0)
        {
            struct disc_t * disc = &stardata->common.discs[i];
            Append_logstring(LOG_DISC,
                             "Evaporate disc (system becomes single) age %g y, M %g Msun, L = %g Lsun, Rin = %g Rsun, Rout = %g Rsun",
                             disc->lifetime/YEAR_LENGTH_IN_SECONDS,
                             disc->M/M_SUN,
                             disc_total_luminosity(disc)/L_SUN,
                             disc->Rin/R_SUN,
                             disc->Rout/R_SUN);

            disc->M = 0.0;
            disc->J = 0.0;
            disc->F = 0.0;
            disc->end_count = disc->end_count==0 ? 1 : disc->end_count;
        }
    }
}
#endif//DISCS
