#include "../binary_c.h"

No_empty_translation_unit_warning;

#ifdef DISCS

void disc_convergence_status(struct stardata_t * const stardata,
                             const char * const s,
                             const struct disc_t * const disc,
                             const struct binary_system_t * const binary)
{
    /*
     * Report on the convergence status of the disc
     */
    if(Disc_do_debug(2))
    {
        /*
         * Current disc's M, J and F from the disc integrals
         */
        double _M = disc_total_mass(disc);
        double _J = disc_total_angular_momentum(disc,binary);
        double _F = disc_total_angular_momentum_flux(disc,binary);

        /*
         * Compare to the desired M, J and F
         */
        double _epsM = fabs(disc->M/Max(_M,1e-100)-1.0);
        double _epsJ = fabs(disc->J/Max(_J,1e-100)-1.0);
        double _epsF = fabs(disc->F/Max(_F,1e-100)-1.0);
        Discdebug(2,
                  "Conv status %s t=%g disc->MJF = %g %g %g : MJF = %g %g %g : EPS %g %g %g : sigma0 = %g Tvisc0 = %g\n",
                  s,
                  disc->lifetime/YEAR_LENGTH_IN_SECONDS,
                  disc->M,
                  disc->J,
                  disc->F,
                  _M,
                  _J,
                  _F,
                  _epsM,
                  _epsJ,
                  _epsF,
                  disc->sigma0,
                  disc->Tvisc0);
    }
}
#endif//DISCS
