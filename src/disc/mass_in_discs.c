#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS

/*
 * Return the mass in all discs of a given type
 */

double mass_in_discs(const Disc_type type,
                     const void * const object)
{
    double mass = 0.0;
    if(type == DISC_CIRCUMSTELLAR)
    {
        struct star_t * star = (struct star_t *) object;
        int i;
        Discloop(star,i)
        {
            mass += star->discs[i]->M;
        }

    }
    else if(type == DISC_CIRCUMBINARY)
    {
        struct stardata_t * stardata = (struct stardata_t *) object;
        int i;
        Discloop(&(stardata->common),i)
        {
            mass += stardata->common.discs[i].M;
        }
    }
    else
    {
        Exit_binary_c_no_stardata(
            BINARY_C_UNKNOWN_DISC_TYPE,
            "Disc type %d unknown",
            type);
    }
    return mass;
}

#endif//DISCS
