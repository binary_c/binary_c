#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef DISCS

/*
 * Function to set up the temperature power laws.
 *
 * This is called by disc_determine_zone_radii, when making a new
 * disc structure, and by disc_rezone when the disc is stripped at its
 * outer or inner edge which may change the zone boundaries.
 */

void disc_set_temperature_power_laws(struct disc_t * const disc)
{

    /*
     * Enter appropriate radii into the temperature power law
     */
    Disc_zone_counter i;
    Disczoneloop(disc,i)
    {
        struct disc_thermal_zone_t * z = &disc->thermal_zones[i];
        struct power_law_t * pl = &z->Tlaw;
        pl->R0 = z->rstart;
        pl->R1 = z->rend;
    }

    /*
     * Set the inner temperature power law temperature at
     * R0, T0, and hence T1 at R1
     */
    {
        struct power_law_t * pl = &disc->thermal_zones[0].Tlaw;
        double Tin = pl->A0 * pow(pl->R0,pl->exponent);
        update_power_law(pl,Tin,0);
    }

    /*
     * And match the temperatures at zone boundaries such that
     * the temperature power law is continuous
     */
    if(disc->n_thermal_zones>1)
    {
        for(i=1;i<disc->n_thermal_zones;i++)
        {
            match_power_laws(&disc->thermal_zones[i-1].Tlaw,
                             &disc->thermal_zones[i].Tlaw);
        }
    }
}

#endif // DISCS
