#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS

double disc_M_bisector(const double Tvisc0,
                       void * p)
{
    Map_GSL_params(p,args);
    Map_varg(struct stardata_t *,stardata,args);
    Map_varg(struct disc_t *,disc,args);
    Map_varg(struct binary_system_t *,binary,args);
    va_end(args);

    if(DEBUG_MASS_BISECTOR)
    {
        printf("================================================================================\n\n");
        printf("================================================================================\n\n");
        printf("================================================================================\n\n");
        printf("Calculate the mass of the disc for Tvisc0 = %g\n",Tvisc0);
        printf("================================================================================\n\n");
    }

    /*
     * Get first guess of Tvisc0.
     * Rin and Rout are fixed.
     */
    disc->Tvisc0 = Tvisc0;

    double ret;
    if(disc->Rin > disc->Rout)
    {
        ret = -1.0;
    }
    else
    {
        /*
         * Make a new zone list with temperature power laws,
         * also sets sigma0. Return -1.0 on failure.
         */

        int status = disc_build_disc_zones(stardata,disc,binary);

        if(status == DISC_ZONES_OK)
        {
            double M = disc_total_mass(disc);
            ret = Bisection_result(disc->M,M);
            if(DEBUG_MASS_BISECTOR)
            {
                printf("M: Given Rin = %g, Rout = %g, Tvisc0 = %g : M=%g (want %g, diff %g) ret %g\n",
                       disc->Rin,
                       disc->Rout,
                       disc->Tvisc0,
                       M,
                       disc->M,
                       Abs_diff(disc->M,M),
                       ret
                    );
            }

        }
        else
        {
            if(DEBUG_MASS_BISECTOR)
            {
                printf("Failed to build disc zones %d %s\n",
                       status,
                       Disc_zone_status(status));
            }
            ret = -1.0;
        }
    }
    return ret;
}

#endif
