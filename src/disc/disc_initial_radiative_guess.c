#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef DISCS

void disc_initial_radiative_guesses(const struct binary_system_t * const binary,
                                    struct disc_t * const disc)
{
    /*
     * Assuming the disc is a single "outer" radiative zone,
     * we can calculate its structure exactly. Use this
     * as an initial guess for the disc structure.
     */
    double chi = disc->mu/
        (3.0 * PI * disc->alpha * disc->gamma
         * BOLTZMANN_CONSTANT * Prefactor_radiative_out);

    disc->Rin = pow(7.0/32.0 * PI * disc->torqueF * Pow4(binary->separation) * Pow2(binary->q) * GRAVITATIONAL_CONSTANT * binary->mtot * chi,
                    7.0/32.0);
    disc->Rout = pow(13.0/6.0 * disc->J/(disc->M * sqrt(GRAVITATIONAL_CONSTANT * binary->mtot)),2.0);

    /* force Rout > Rin * 2 */
    disc->Rout = Max(2.0 * disc->Rin,disc->Rout);

    disc->F = 3.0 * disc->M * pow(disc->Rout,-3.0/7.0)/ (14.0*PI*chi);

    disc->sigma0 = disc->F * disc->mu / (3.0 * PI * disc->alpha * disc->gamma * BOLTZMANN_CONSTANT);
    disc->Tvisc0 = Prefactor_viscous * pow(disc->sigma0,2.0/5.0);

#if defined DEBUG_FIRST_GUESSES && DEBUG_FIRST_GUESSES==1
    printf("First guess Rin = %g, Rout = %g, F = %g, sigma0 = %g, Tvisc0 = %g\n",
           disc->Rin,
           disc->Rout,
           disc->F,
           disc->sigma0,
           disc->Tvisc0
        );
#endif

}
#endif
