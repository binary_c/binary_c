#pragma once
#ifndef STELLAR_STRUCTURE_H
#define STELLAR_STRUCTURE_H

#include "stellar_structure_prescriptions.def"

#undef X
#define X(CODE) INTERNAL_STELLAR_STRUCTURE_##CODE,
enum { INTERNAL_STELLAR_STRUCTURE_STATES_LIST };
#undef X

#define X(CODE) STELLAR_STRUCTURE_ALGORITHM_##CODE,
enum {
    STELLAR_STRUCTURE_ALGORITHMS_LIST
    STELLAR_STRUCTURE_ALGORITHM_NUMBER
};
#undef X
#define X(CODE) #CODE,
static const char * const stellar_structure_algorithm_strings[] Maybe_unused = {STELLAR_STRUCTURE_ALGORITHMS_LIST };
#undef X

#define X(CODE,STRING) STELLAR_STRUCTURE_CALLER_##CODE,
enum { STELLAR_STRUCTURE_CALLER_LIST };
#undef X
#define X(CODE,STRING) STRING,
static const char * const stellar_structure_caller_strings[] Maybe_unused = { STELLAR_STRUCTURE_CALLER_LIST };
#undef X

#define Stellar_structure_caller_string(N) ((char*)stellar_structure_caller_strings[(N)])

#endif // STELLAR_STRUCTURE_H
