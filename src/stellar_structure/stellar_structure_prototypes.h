#ifndef STELLAR_STRUCTURE_PROTOTYPES_H
#define STELLAR_STRUCTURE_PROTOTYPES_H

#include "stellar_structure_macros.h"

int stellar_structure_BSE(struct stardata_t * const stardata,
                          const Caller_id caller_id,
                          struct star_t * const star,
                          struct BSE_data_t * bse);

int stellar_structure_BSE_given_timescales(struct star_t * newstar,
                                           struct star_t * oldstar,
                                           struct stardata_t * stardata,
                                           const Caller_id caller_id);

int stellar_structure_BSE_with_newstar(struct stardata_t * const stardata,
                                       const Caller_id caller_id,
                                       struct star_t * const oldstar,
                                       struct star_t * const newstar,
                                       struct BSE_data_t * bse);

Stellar_type stellar_structure_MS_BSE(struct star_t * Restrict const newstar,
                                      struct stardata_t * Restrict const stardata);

Stellar_type stellar_structure_HG(struct star_t * Restrict const newstar,
                                  struct stardata_t * Restrict stardata,
                                  const Caller_id caller_id Maybe_unused);

Stellar_type stellar_structure_RG(struct star_t * const newstar,
                                  double * const rg,
                                  struct stardata_t * Restrict const stardata,
                                  const Caller_id caller_id Maybe_unused);

Stellar_type stellar_structure_CHeB(struct star_t * const oldstar,
                                    struct star_t * Restrict const newstar,
                                    double * Restrict const rg,
                                    struct stardata_t * Restrict const stardata,
                                    const Caller_id caller_id);
Stellar_type stellar_structure_AGB(struct star_t * const oldstar,
                                   struct star_t * Restrict const newstar,
                                   double * Restrict const rg,
                                   const double tbagb,
                                   struct stardata_t * Restrict const stardata,
                                   const Caller_id caller_id);

Boolean stellar_structure_EAGB(struct star_t * const oldstar,
                               struct star_t * Restrict const newstar,
                               const double mcbagb,
                               struct stardata_t * Restrict const stardata,
                               const Caller_id caller_id Maybe_unused,
                               const Boolean force_to_explosion);

void stellar_structure_TPAGB(struct star_t * Restrict const newstar,
                             double * Restrict const mcmax,
                             const double mcbagb,
                             struct stardata_t * Restrict const stardata,
                             const Caller_id caller_id);


double lambda_3dup(struct stardata_t * Restrict const stardata,
                   struct star_t * Restrict const newstar,
                   Boolean * const above_mcmin);

double Pure_function lambda_3dup_Hurley(const double m);

double lambda_3dup_Karakas(struct star_t * Restrict const newstar,
                           struct stardata_t * Restrict const stardata,
                           Boolean * const above_mcmin);

double lambda_3dup_stancliffe(struct star_t * const newstar,
                              const double n,
                              struct stardata_t * const stardata,
                              Boolean * const above_mcmin
                              );

void stellar_structure_HeStar(struct star_t * const newstar,
                              double * const mtc,
                              double * const rg,
                              struct stardata_t * const stardata,
                              const Caller_id caller_id);

void stellar_structure_WD(struct star_t * const newstar,
                          struct stardata_t * const stardata,
                          const Caller_id caller_id Maybe_unused);

void stellar_structure_NS(struct star_t * const newstar,
                          struct stardata_t * const stardata);

void stellar_structure_BH(struct stardata_t * const stardata,
                          struct star_t * const newstar);

void stellar_structure_remnant_and_perturbations(struct star_t * const newstar,
                                                 double remnant_luminosity,
                                                 double remnant_radius,
                                                 double tbagb,
                                                 double mtc,
                                                 double mcx,
                                                 struct stardata_t * const stardata);
void stellar_structure_small_envelope_perturbations(struct stardata_t * const staradata,
                                                    struct star_t * const newstar,
                                                    const double mtc,
                                                    const double remnant_luminosity,
                                                    const double remnant_radius);
void stellar_structure_nucsyn(Stellar_type stellar_type,
                              Stellar_type stellar_typein,
                              struct star_t * const star,
                              struct stardata_t * const stardata);
double Pure_function remnant_radius(struct stardata_t * const stardata,
                                    struct star_t * const star,
                                    double * const metallicity_parameters,
                                    const Stellar_type stellar_type,
                                    const double mass,
                                    const double mc,
                                    const double r);

void convective_envelope_mass_and_radius(struct stardata_t * const stardata,
                                         struct star_t * const newstar,
                                         const double rg,
                                         double z Maybe_unused);


void stellar_structure_make_massless_remnant(struct stardata_t * const stardata,
                                             struct star_t * const newstar);
void set_stellar_structure_struct_from_star(struct star_t * const newstar,
                                            struct star_t * const oldstar);

void set_star_struct_from_stellar_structure(struct star_t * const newstar,
                                            struct star_t * const oldstar);
double TPAGB_luminosity(const Stellar_type stellar_type,
                        const double m,
                        const double mc,
                        const double mc1tp,
                        const double mcnodup,
                        const double age,
                        const double Z,
                        const double phase_start_mass,
                        const double num_thermal_pulses,
                        const double interpulse_period,
                        const double time_first_pulse,
                        const double time_prev_pulse,
                        const double time_next_pulse,
                        struct stardata_t * Restrict const stardata,
                        double * Restrict const GB
    );



void stellar_structure_small_envelope_miller_bertolami(struct stardata_t * const stardata,
                                                       struct star_t * const star,
                                                       const double remnant_radius Maybe_unused,
                                                       const double remnant_luminosity Maybe_unused
    );


void BSE_init(struct stardata_t * const stardata);
void stellar_structure_BSE_alloc_arrays(struct star_t * const star,
                                        struct BSE_data_t * const bse,
                                        Boolean alloc[3]);
void stellar_structure_BSE_free_arrays(struct star_t * const star,
                                       const Boolean alloc[3]);

#ifdef SAVE_MASS_HISTORY
void stellar_structure_adjust_from_mass_history(struct stardata_t * const stardata,
                                                struct star_t * const star);
#endif // SAVE_MASS_HISTORY
Stellar_type stellar_structure_TZO(struct star_t * Restrict const newstar,
                                   struct stardata_t * Restrict const stardata,
                                   const Caller_id caller_id);
void stellar_structure_sanity_checks(struct stardata_t * const stardata,
                                     struct star_t * const newstar);

#endif /*STELLAR_STRUCTURE_PROTOTYPES_H*/
