#!/bin/bash

# make data objects for common envelope evolution with binary_c

echo "make data objects for common envelope evolution with binary_c"

source ../make_data_objects_setup.sh

# Miller Bertolami's post-AGB tables
HFILE=miller_bertolami_postagb.h
TMPFILE=miller_bertolami_postagb.dat
OBJFILE=miller_bertolami_postagb.o
echo "HFILE=$HFILE TMPFILE=$TMPFILE OBJFILE=$OBJFILE"
$CC ../../double2bin.c -o ./double2bin

if [ $HFILE -nt $OBJFILE ] || [ !-z $BINARY_C_REBUILD_DATA_OBJECTS]  ; then
    cat $HFILE | tr , ' ' | sed s/\\\\// | grep -v define |grep -v \*    | ./double2bin > $TMPFILE && \
    OPTS="--rename-section .data=.rodata,alloc,load,readonly,data,contents " && \
    objcopy $OBJCOPY_OPTS $OPTS $TMPFILE $OBJFILE && \
    ls -l $TMPFILE $OBJFILE double2bin && \
    rm $TMPFILE &
fi

echo "DATAOBJECT $OBJFILE"

HFILE=miller_bertolami_postagb_coeffs_L.h
TMPFILE=miller_bertolami_postagb_coeffs_L.dat
OBJFILE=miller_bertolami_postagb_coeffs_L.o
echo "HFILE=$HFILE TMPFILE=$TMPFILE OBJFILE=$OBJFILE"

if [ $HFILE -nt $OBJFILE ]  || [ !-z $BINARY_C_REBUILD_DATA_OBJECTS] ; then
    cat $HFILE | tr , ' ' | sed s/\\\\// | grep -v define |grep -v \*    | ./double2bin > $TMPFILE && \
    OPTS="--rename-section .data=.rodata,alloc,load,readonly,data,contents " && \
    objcopy $OBJCOPY_OPTS $OPTS $TMPFILE $OBJFILE && \
    ls -l $TMPFILE $OBJFILE double2bin && \
    rm $TMPFILE &
fi

echo "DATAOBJECT $OBJFILE"

HFILE=miller_bertolami_postagb_coeffs_R.h
TMPFILE=miller_bertolami_postagb_coeffs_R.dat
OBJFILE=miller_bertolami_postagb_coeffs_R.o
echo "HFILE=$HFILE TMPFILE=$TMPFILE OBJFILE=$OBJFILE"

if [ $HFILE -nt $OBJFILE ]  || [ !-z $BINARY_C_REBUILD_DATA_OBJECTS] ; then
    cat $HFILE | tr , ' ' | sed s/\\\\// | grep -v define |grep -v \*    | ./double2bin > $TMPFILE && \
    OPTS="--rename-section .data=.rodata,alloc,load,readonly,data,contents " && \
    objcopy $OBJCOPY_OPTS $OPTS $TMPFILE $OBJFILE && \
    ls -l $TMPFILE $OBJFILE double2bin && \
    rm $TMPFILE &
fi
echo "DATAOBJECT $OBJFILE"

wait
rm ./double2bin
