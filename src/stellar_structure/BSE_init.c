#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BSE
void BSE_init(struct stardata_t * const stardata)
{
    set_metallicities(stardata);
}
#endif//BSE
