#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BSE

#include "stellar_structure_debug.h"

#define p (newstar->bse->GB[GB_p])
#define q (newstar->bse->GB[GB_q])
#define B (newstar->bse->GB[GB_B])
#define D (newstar->bse->GB[GB_D])
#define A (newstar->bse->GB[GB_A_HE])


Boolean stellar_structure_EAGB(struct star_t * const oldstar Maybe_unused,
                               struct star_t * const newstar,
                               const double mcbagb,
                               struct stardata_t * const stardata,
                               const Caller_id caller_id Maybe_unused,
                               const Boolean force_to_explosion)
{
    Boolean convert_to_helium_star=FALSE;
    Dprint("stellar_structure_EAGB newstar->age=%12.12e LESS than T_TPAGB_FIRST_PULSE=%12.12e\n",
           newstar->age,newstar->bse->timescales[T_TPAGB_FIRST_PULSE]);

    /*
     * estimate core mass at the first thermal pulse
     */
#if defined NUCSYN &&                           \
    defined NUCSYN_FIRST_DREDGE_UP_PHASE_IN
    newstar->mc_gb_was=0.0;
#endif // NUCSYN && NUCSYN_FIRST_DREDGE_UP_PHASE_IN

    if(force_to_explosion == FALSE)
    {
        /*
         * Set the CO core mass, but don't let it be
         * smaller than it already is.
         */
        newstar->core_mass[CORE_CO] =
            Max(newstar->core_mass[CORE_CO],
                mcgbtf(newstar->age,
                       newstar->bse->GB[GB_A_HE],
                       newstar->bse->GB,
                       newstar->bse->timescales[T_EAGB_TINF_1],
                       newstar->bse->timescales[T_EAGB_TINF_2],
                       newstar->bse->timescales[T_EAGB_T]));
        newstar->core_mass[CORE_CO] = inner_core_mass(stardata,
                                                      newstar,
                                                      CORE_CO);

        Dprint("mc_CO = mcx = %g from age = %g tscls %g %g %g GB_A_HE =%g\n",
               newstar->core_mass[CORE_CO],
               newstar->age,
               newstar->bse->timescales[T_EAGB_TINF_1],
               newstar->bse->timescales[T_EAGB_TINF_2],
               newstar->bse->timescales[T_EAGB_T],
               newstar->bse->GB[GB_A_HE]
            );
    }
    else
    {
        Dprint("keep mc_CO = mcx = %g because force_to_explosion is TRUE\n",
               newstar->core_mass[CORE_CO]);
    }

    /*
     * Set the initial CO core mass and its
     * rate of growth
     */
    double CO_dMc_dt Maybe_unused = 0.0;

    if(Is_zero(newstar->core_mass[CORE_CO]))
    {
        /*
         * New CO core
         */
        newstar->core_mass[CORE_CO] = Min(newstar->core_mass[CORE_He],
                                          mcgbtf(newstar->age,
                                                 newstar->bse->GB[GB_A_HE],
                                                 newstar->bse->GB,
                                                 newstar->bse->timescales[T_EAGB_TINF_1],
                                                 newstar->bse->timescales[T_EAGB_TINF_2],
                                                 newstar->bse->timescales[T_EAGB_T]));
        newstar->core_mass[CORE_CO] = inner_core_mass(stardata,
                                                      newstar,
                                                      CORE_CO);
        CO_dMc_dt = 0.0;
    }
    else
    {
        enum { LOW, HIGH };
        const int use = newstar->core_mass[CORE_CO] < newstar->bse->GB[GB_Mx] ? LOW : HIGH;
        CO_dMc_dt =
            A * (use == LOW ? D : B) *
            pow(newstar->core_mass[CORE_CO], use == LOW ? p : q);
    }

    /*
     * The helium core mass is always the helium core mass at the base of the AGB:
     * this does not change until the CO core catches up (or there is second
     * dredge up) when the star ascends the TPAGB
     */
    newstar->core_mass[CORE_He] = Min(newstar->mass,
                                      mcbagb);
    newstar->core_mass[CORE_CO] = inner_core_mass(stardata,
                                                  newstar,
                                                  CORE_CO);
    newstar->derivative[DERIVATIVE_STELLAR_GB_CORE_MASS] = 0.0;
    newstar->derivative[DERIVATIVE_STELLAR_He_CORE_MASS] = 0.0;
    newstar->derivative[DERIVATIVE_STELLAR_CO_CORE_MASS] = 0.0; // set above

    Dprint("Set core from Max(mcbagb=%12.12e,mcx=%12.12e) -> %g\n",
           mcbagb,newstar->mcx_EAGB,newstar->core_mass[CORE_He]);


    /*
     * check that the CO core mass is < the He core mass:
     * should not be required, but cannot hurt.
     */
    newstar->core_mass[CORE_CO] = Min3(newstar->mass,
                                       newstar->core_mass[CORE_He],
                                       newstar->core_mass[CORE_CO]);

    newstar->core_mass[CORE_CO] = inner_core_mass(stardata,
                                                  newstar,
                                                  CORE_CO);

    if(0)
    {
        const double accretion_dm = stardata->model.dt * Mdot_net(newstar);
        if(accretion_dm > 0.0)
        {
            int n = newstar->starnum;
            struct star_t * pp = &stardata->previous_stardata->star[n];
            if(accretion_dm > 0.0)
            {
                newstar->core_mass[CORE_CO] += accretion_dm;
            }
            Dprint("PREV in EAGB st=%d m=%g mcHe=%g mcCO=%g : now st=%d m=%g mc(BSE) He=%g CO=%g\n",
                   pp->stellar_type,
                   pp->mass,
                   pp->core_mass[CORE_He],
                   pp->core_mass[CORE_CO],
                   newstar->stellar_type,
                   newstar->mass,
                   newstar->core_mass[CORE_He],
                   newstar->core_mass[CORE_CO]);
        }
    }

    Dprint("STELLAR_STRUCTURE type=%d mc=mcbagb=%12.12e mcx=%12.12e\n",
           newstar->stellar_type,
           newstar->core_mass[CORE_He],
           newstar->core_mass[CORE_CO]);

    /*
     * Require core radius
     */
#ifdef HALL_TOUT_2014_RADII
    newstar->core_radius = Hall_Tout_2014_low_mass_HG_RGB_radius(stardata,
                                                                 newstar,
                                                                 newstar->core_mass[CORE_He]);
#else
    newstar->core_radius = 5.0 * rwd(stardata,
                                     newstar,
                                     newstar->core_mass[CORE_He]);
#endif // HALL_TOUT_2014_RADII
    newstar->core_radius = Max(newstar->core_radius,
                               rns(newstar->core_mass[CORE_He]));

    /*
     * Hence the luminosity as a function of CO core mass
     */
    newstar->luminosity = lmcgbf(newstar->core_mass[CORE_CO],
                                 newstar->bse->GB);

    newstar->mcx_EAGB = newstar->core_mass[CORE_CO];

    Dprint("Set newstar->mcx_EAGB = %g\n",newstar->mcx_EAGB);

    Dprint("mt=%12.12e core=%12.12e (if mt<core > TPAGB)\n",
           newstar->mass,newstar->core_mass[CORE_He]);

    if(Less_or_equal(newstar->mass,
                     newstar->core_mass[CORE_He]))
    {
        /*
         * Evolved naked helium star or white dwarf
         * as the envelope is lost but the
         * star has not completed its interior burning.

         * The star becomes a post-HeMS star if its core is
         * sufficiently massive, otherwise a white dwarf.
         */
        Dprint("EAGB to HeGB : EAGB cores He %g CO %g\n",
               newstar->core_mass[CORE_He],
               newstar->core_mass[CORE_CO]);

        newstar->stellar_type = HeGB;
        newstar->core_stellar_type = COWD;
        newstar->phase_start_mass = newstar->mass;

        /*
         * The helium star has no helium core, but preserves
         * the existing CO core
         */
        newstar->core_mass[CORE_He] = 0.0;

        /*
         * No other cores inside the CO core
         */
        set_no_core_below(newstar,CORE_CO);

        Dprint("New core He %g CO %g\n",
               newstar->core_mass[CORE_He],
               newstar->core_mass[CORE_CO]);

        /*
         * Update timescales
         */
        call_stellar_timescales2(newstar->stellar_type,
                                 newstar->phase_start_mass,
                                 newstar->mass);

        Dprint("Set new tm=%g mc He %g CO %g, GB_Mx=%g\n",
               newstar->tm,
               newstar->core_mass[CORE_He],
               newstar->core_mass[CORE_CO],
               newstar->bse->GB[GB_Mx]);


        /*
         * Now compute the age: we could call giant_age
         * but this is quicker.
         */
        if(Less_or_equal(newstar->core_mass[CORE_He],
                         newstar->bse->GB[GB_Mx]))
        {
            newstar->age = newstar->bse->timescales[T_GIANT_TINF_1] -
                (1.0/((newstar->bse->GB[GB_p]-1.0)*newstar->bse->GB[GB_A_HE]*newstar->bse->GB[GB_D]))*
                pow(newstar->core_mass[CORE_He],1.0-newstar->bse->GB[GB_p]);
        }
        else
        {
            newstar->age = newstar->bse->timescales[T_GIANT_TINF_2] -
                (1.0/((newstar->bse->GB[GB_q]-1.0)*newstar->bse->GB[GB_A_HE]*newstar->bse->GB[GB_B]))*
                pow(newstar->core_mass[CORE_He],1.0-newstar->bse->GB[GB_q]);
        }
        newstar->age = Max(newstar->age,
                           newstar->tm);

        Dprint("Set age = %g\n",newstar->age);

        convert_to_helium_star = TRUE;
    }
    else
    {
        /*
         * if convert_to_helium_star==TRUE (i.e. star is an EAGB star),
         * we can never get here
         */

        Dprint("Star is EAGB\n");
        Dprint("EAGB core mass %12.12e (CO core %12.12e)\n",
               newstar->core_mass[CORE_He],
               newstar->core_mass[CORE_CO]);

        newstar->max_EAGB_He_core_mass = Max(newstar->max_EAGB_He_core_mass,
                                             newstar->core_mass[CORE_He]);

#ifdef NUCSYN
        if(newstar->stellar_type!=TPAGB)
        {
            /* because mc1tp is different from mcbagb
             * sometimes we get confused and try to de-evolve
             * back to an EAGB star from the TPAGB. Don't
             * let this happen!!!
             */
#endif
            newstar->stellar_type = EAGB;
#ifdef NUCSYN
        }
#endif

        /*
         * Set the radius
         */

        if(stardata->preferences->require_drdm == FALSE)
        {
            newstar->radius = ragbf(newstar->mass,
                                    newstar->luminosity,
                                    stardata->common.metallicity_parameters[ZPAR_MASS_HE_FLASH],
                                    stardata->common.giant_branch_parameters
#ifdef AXEL_RGBF_FIX
                                    ,stardata->common.metallicity
#endif // AXEL_RGBF_FIX
                );
        }
        else
        {
            const double dm = newstar->mass * 1e-7;
            update_mass(stardata,newstar,dm);
            const double rm = ragbf(newstar->mass,
                                    newstar->luminosity,
                                    stardata->common.metallicity_parameters[ZPAR_MASS_HE_FLASH],
                                    stardata->common.giant_branch_parameters
#ifdef AXEL_RGBF_FIX
                                    ,stardata->common.metallicity
#endif // AXEL_RGBF_FIX
                );
            update_mass(stardata,newstar,dm);
            newstar->radius = ragbf(newstar->mass,
                                    newstar->luminosity,
                                    stardata->common.metallicity_parameters[ZPAR_MASS_HE_FLASH],
                                    stardata->common.giant_branch_parameters
#ifdef AXEL_RGBF_FIX
                                    ,stardata->common.metallicity
#endif // AXEL_RGBF_FIX
                );
            newstar->drdm = (newstar->radius - rm) / dm;
        }
        newstar->core_mass[CORE_He] = Min(newstar->mass,
                                          newstar->core_mass[CORE_He]);
        newstar->core_mass[CORE_CO] = Min(newstar->core_mass[CORE_CO],
                                          newstar->core_mass[CORE_He]);
    }
    Dprint("AJ set to %12.12e stellar_type is now %d with M=%g Menv=%g mc He %g CO %g\n",
           newstar->age,
           newstar->stellar_type,
           newstar->mass,
           newstar->mass - newstar->core_mass[CORE_He],
           newstar->core_mass[CORE_He],
           newstar->core_mass[CORE_CO]
        );

    return convert_to_helium_star;
}
#endif//BSE
