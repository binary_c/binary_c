#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BSE
#include "stellar_structure_debug.h"

Stellar_type stellar_structure_TZO(struct star_t * Restrict const newstar,
                                   struct stardata_t * Restrict const stardata,
                                   const Caller_id caller_id)
{
    /*
     * TZO stellar structure
     *
     * TZOs are set up as specially-flagged EAGB or HeHG stars.
     *
     * Check for envelope loss to form a helium giant,
     * or compact object, hence stellar type change.
     */
    newstar->deny_SN++;

    /*
     * First, check if star is stripped to
     * its NS or BH "core", in which case
     * update its stellar type.
     */
    const double menv = envelope_mass(newstar);
    if(Is_zero(menv))
    {
        /*
         * Stripped
         */
        if(Core_exists(newstar,CORE_NEUTRON) &&
           !Core_exists(newstar,CORE_BLACK_HOLE))
        {
            newstar->stellar_type = NEUTRON_STAR;
        }
        else
        {
            newstar->stellar_type = BLACK_HOLE;
        }
        newstar->age = 0.0;
    }
    else if(newstar->stellar_type == EAGB &&
            Less_or_equal(newstar->mass,
                          newstar->core_mass[CORE_He]))
    {
        /*
         * Hydrogen -> Helium giant transition
         */
        newstar->stellar_type = NAKED_HELIUM_STAR_GIANT_BRANCH;
        trim_cores(newstar,
                   newstar->mass);
        newstar->phase_start_mass = newstar->mass;
        newstar->age = 0.0;
        newstar->core_mass[CORE_CO] = Max(newstar->core_mass[CORE_CO],
                                          newstar->core_mass[CORE_He]);
        newstar->core_mass[CORE_He] = 0.0;

        /*
         * recompute timescales with the new
         * phase start mass and helium-star type
         */
        stellar_timescales(stardata,
                           newstar,
                           newstar->bse,
                           newstar->phase_start_mass,
                           newstar->mass,
                           newstar->stellar_type);
        /*
         * We need to guess the phase start mass
         * and age.
         */
        giant_age(&newstar->core_mass[CORE_CO],
                  newstar->mass,
                  &newstar->stellar_type,
                  &newstar->phase_start_mass,
                  &newstar->age,
                  stardata,
                  newstar);

        /*
         * recompute timescales with the new
         * phase start mass and helium-star type
         */
        stellar_timescales(stardata,
                           newstar,
                           newstar->bse,
                           newstar->phase_start_mass,
                           newstar->mass,
                           newstar->stellar_type);
        Dprint("New HeGB with M = %g, MCO = %g, age = %g, M0 = %g, lum = %g, st = %d\n",
               newstar->mass,
               newstar->core_mass[CORE_CO],
               newstar->age,
               newstar->phase_start_mass,
               newstar->luminosity,
               newstar->stellar_type);
    }

    /*
     * Check for NS -> BH transition
     */
    if(newstar->core_mass[CORE_NEUTRON] > stardata->preferences->max_neutron_star_mass &&
       Is_zero(newstar->core_mass[CORE_BLACK_HOLE]))
    {
        newstar->core_stellar_type = BLACK_HOLE;
        newstar->core_mass[CORE_BLACK_HOLE] = newstar->core_mass[CORE_NEUTRON];
        newstar->core_mass[CORE_NEUTRON] = 0.0;
        newstar->core_radius = rbh(newstar->core_mass[CORE_BLACK_HOLE]);
        if(newstar->stellar_type == NEUTRON_STAR)
        {
            newstar->stellar_type = BLACK_HOLE;
        }
    }
    else
    {
        newstar->core_stellar_type = NEUTRON_STAR;
        newstar->core_radius = rns(newstar->core_mass[CORE_NEUTRON]);
    }

    /*
     * Stellar structure continues to use the "helium+CO+NS+BH"
     * core mass
     */
    if(newstar->stellar_type == GIANT_BRANCH)
    {
        const double Mc = Outermost_core_mass(newstar);
        newstar->luminosity = lmcgbf(Mc,
                                     newstar->bse->GB);

        newstar->radius = ragbf(newstar->mass,
                                newstar->luminosity,
                                stardata->common.metallicity_parameters[ZPAR_MASS_HE_FLASH],
                                stardata->common.giant_branch_parameters
#ifdef AXEL_RGBF_FIX
                                ,stardata->common.metallicity
#endif // AXEL_RGBF_FIX
            );
        newstar->menv = newstar->mass - Mc;
        newstar->renv = newstar->radius;
        {
            char * corestring = core_string(newstar,FALSE);
            Dprint("CEE stellar structure TZO (GB) M = %g, cores %s, L = %g, R = %g\n",
                   newstar->mass,
                   corestring,
                   newstar->luminosity,
                   newstar->radius);
            Safe_free(corestring);
        }
    }
    else if(newstar->stellar_type == EAGB)
    {
        const double Mc = Outermost_core_mass(newstar);
        newstar->luminosity = lmcgbf(Mc,
                                     newstar->bse->GB);

        newstar->radius = ragbf(newstar->mass,
                                newstar->luminosity,
                                stardata->common.metallicity_parameters[ZPAR_MASS_HE_FLASH],
                                stardata->common.giant_branch_parameters
#ifdef AXEL_RGBF_FIX
                                ,stardata->common.metallicity
#endif // AXEL_RGBF_FIX
            );
        newstar->menv = newstar->mass - Mc;
        newstar->renv = newstar->radius;
        {
            char * corestring = core_string(newstar,FALSE);
            Dprint("CEE stellar structure TZO (EAGB) M = %g, cores %s, L = %g, R = %g\n",
                   newstar->mass,
                   corestring,
                   newstar->luminosity,
                   newstar->radius);
            Safe_free(corestring);
        }
    }
    else if(newstar->stellar_type == NAKED_HELIUM_STAR_HERTZSPRUNG_GAP ||
            newstar->stellar_type == NAKED_HELIUM_STAR_GIANT_BRANCH)
    {
        double mtc,rg;
        stellar_structure_HeStar(newstar,
                                 &mtc,
                                 &rg,
                                 stardata,
                                 -caller_id);
        Dprint("CEE stellar structure TZO (HeGB) M = %g, Mc(CO) = %g, Mc(n) = %g, L = %g, R = %g, age = %g\n",
               newstar->mass,
               newstar->core_mass[CORE_CO],
               newstar->core_mass[CORE_NEUTRON],
               newstar->luminosity,
               newstar->radius,
               newstar->age);
    }
    else if(newstar->stellar_type == NEUTRON_STAR)
    {
        /*
         * Stripped to neutron star
         */
        stellar_structure_NS(newstar,stardata);
        {
            char * corestring = core_string(newstar,FALSE);
            Dprint("CEE stellar structure TZO (NS) M = %g, cores %s, L = %g, R = %g\n",
                   newstar->mass,
                   corestring,
                   newstar->luminosity,
                   newstar->radius);
            Safe_free(corestring);
        }
    }
    else if(newstar->stellar_type == BLACK_HOLE)
    {
        /*
         * Stripped to black hole
         */
        stellar_structure_BH(stardata,newstar);
        {
            char * corestring = core_string(newstar,FALSE);
            Dprint("CEE stellar structure TZO (BH) M = %g, cores %s, L = %g, R = %g\n",
                   newstar->mass,
                   corestring,
                   newstar->luminosity,
                   newstar->radius);
            Safe_free(corestring);
        }
    }

    newstar->deny_SN--;
    return newstar->stellar_type;
}

#endif // BSE
