#pragma once
#ifndef MILLER_BERTOLAMI_POSTAGB_N_H
#define MILLER_BERTOLAMI_POSTAGB_N_H

#define TABLE_MILLER_BERTOLAMI_N_PARAMS 3
#define TABLE_MILLER_BERTOLAMI_N_DATA 4
#define TABLE_MILLER_BERTOLAMI_LINES 10800
#define TABLE_MILLER_BERTOLAMI_N_ALL ((TABLE_MILLER_BERTOLAMI_N_PARAMS+TABLE_MILLER_BERTOLAMI_N_DATA)*TABLE_MILLER_BERTOLAMI_LINES)

#endif// MILLER_BERTOLAMI_POSTAGB_N_H
