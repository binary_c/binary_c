#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "stellar_structure_debug.h"

void stellar_structure_NS(struct star_t * const newstar,
                          struct stardata_t * const stardata)
{
    set_no_core(newstar);
    newstar->core_mass[CORE_NEUTRON] = newstar->mass;
    Dprint("stellar_structure_NS : cores He %g CO %g NS %g\n",
           newstar->core_mass[CORE_He],
           newstar->core_mass[CORE_CO],
           newstar->core_mass[CORE_NEUTRON]);

    newstar->luminosity = lns(newstar->mass,newstar->age);
    if(stardata->preferences->require_drdm == TRUE)
    {
        const double dm =newstar->mass * 1e-7;
        update_mass(stardata,newstar,dm);
        const double rm = rns(newstar->mass);
        update_mass(stardata,newstar,-dm);
        newstar->radius = rns(newstar->mass);
        newstar->drdm = (rm - newstar->radius) / dm;
    }
    else
    {
        newstar->radius = rns(newstar->mass);
    }
    newstar->core_stellar_type = MASSLESS_REMNANT;

    if(newstar->core_mass[CORE_NEUTRON] > stardata->preferences->max_neutron_star_mass &&
       !Fequal(newstar->core_mass[CORE_NEUTRON],
               newstar->core_mass[CORE_BLACK_HOLE]))
    {
        /*
         * Accretion-induced collapse of a neutron
         * star to a black hole.
         */
        Dprint("Accretion induced Black Hole?\n");

        if(newstar->deny_SN == 0 &&
           newstar->SN_type == SN_NONE)
        {
            newstar->SN_type = SN_AIC_BH;
            struct star_t * news =
                new_supernova(stardata,
                              newstar,
                              Other_star_struct(newstar),
                              newstar);
            if(news)
            {
                set_no_core(news);
                news->stellar_type = BLACK_HOLE;
                news->core_stellar_type = MASSLESS_REMNANT;
                news->age = 0.0;
                news->core_mass[CORE_BLACK_HOLE] = news->mass;
                news->mass = newstar->mass;
            }
        }
    }
    newstar->menv = 0.0;
    newstar->renv = 0.0;
    newstar->TZO = TZO_NONE;
}
