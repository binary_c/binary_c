#include "../binary_c.h"
No_empty_translation_unit_warning;



static Constant_function double Karakas_lamaxf(double m,
                                               const Abundance z);


static double Karakas_mcmin_for_dredgeup(const double m,
                                         const double phase_start_mass,
                                         struct stardata_t * Restrict const stardata);

double lambda_3dup_Karakas(struct star_t * Restrict const newstar,
                           struct stardata_t * Restrict const stardata,
                           Boolean * const above_mcmin)
{
    /*
     * Table of NCAL fits to Karakas' new (2007) data tables
     */
    double lambda;
    double mcmin = Karakas_mcmin_for_dredgeup(newstar->phase_start_mass,
                                              newstar->phase_start_mass,
                                              stardata);
    mcmin += stardata->preferences->delta_mcmin;
    mcmin = Max(0.0, mcmin);

    if(newstar->core_mass[CORE_He] < mcmin)
    {
        lambda = 0.0;
        * above_mcmin = FALSE;
    }
    else
    {
        double x[2]={
            stardata->common.metallicity,
            Limit_range(newstar->phase_start_mass,1.0,6.5)
        };
        double rr[1]; // stores the resulting NCAL
        Interpolate(stardata->store->Karakas_ncal,
                    x,rr,2);
        double lammax = Min(1.0,
                            Karakas_lamaxf(newstar->phase_start_mass,
                                           stardata->common.metallicity));

        Dprint("lammax = %g\n",lammax);
        Clamp(lammax,stardata->preferences->lambda_min,1.0);
        lambda = (1.0-exp(-newstar->num_thermal_pulses_since_mcmin/rr[0]))
            *lammax;
        * above_mcmin = TRUE;
    }

    Dprint("Lambda 3dup Karakas lambda = %g, above mcmin? %d (NTP since mcmin %g)\n",
           lambda,
           *above_mcmin,
           newstar->num_thermal_pulses_since_mcmin
        );

    return lambda;
}


static Constant_function double Karakas_lamaxf(double m,
                                               const Abundance z)
{
    /*
     * Asymptotic value of dredge-up parameter lambda, according to the fitting
     * formula by Amanda Karakas (for non-overshooting models!).
     */
    double l=0.0;
    const double m3 = Pow3(m);

    /*
     * Maximum dredge-up parameter (asymptotic value) as a function
     * of total mass M and metallicity Z.
     *  Fitting formula from Karakas, Lattanzio & Pols 2002.
     */
    Clamp(m,1.0,6.5);

#define ASIZE 4

    /*
     * Asymptotic value of dredge-up parameter lambda, according to the fitting
     * formula by Amanda Karakas (for non-overshooting models!). NB: the
     * Z-dependence has not yet been included, m is the mass at the beginning of
     * the AGB (NOT the current mass!)
     */
    const double zz1 = 0.02;
    const double zz2 = 0.008;
    const double zz3 = 0.004;
    const double zz4 = 0.0001;
    double h1,h2;
    const double a1[ASIZE] = {-1.17696,0.76262,0.026028,0.041019};
    const double a2[ASIZE] = {-0.609465,0.55430,0.056878,0.069227};
    const double a3[ASIZE] = {-0.764199,0.70859,0.058833,0.075921};
    //const double a4[ASIZE] = {-0.82367052137017,0.855045797056254,0.075969726978165,0.094591645793495};

    /* better fit from Rob */
    const double a4[ASIZE] = {-8.90480e-01,8.84020e-01,1.12030e-01,1.41860e-01};

    /* new fit based on Amanda's new "radius tables":
     *  This is a smoother transition from amanda's functions to the newer fit
     * and neglects her pulses which have lambda>1.1 (the 'degenerate' pulses - I guess!)
     *
     * Note: this breaks at 4Msun Z=0.0001 - oh dear!
     * Best avoided then, use the "better fit" above.
     */
    //const double a4[ASIZE]={-5.62410e-01,7.32720e-01,1.63250e-02,3.45080e-02};

    double a[ASIZE];
    double zz = z; //log10(z/0.02);
    unsigned int i;

    zz=z;
    if(zz>zz2)
    {
        h1 = (zz-zz2)/(zz1-zz2);
        h2 = 1.0 - h1;
        for(i=0;i<ASIZE;i++)
        {
            a[i] = a1[i]*h1 + a2[i]*h2;
        }
    }
    else if(zz>zz3)
    {
        h1 = (zz-zz3)/(zz2-zz3);
        h2 = 1.0 - h1;
        for(i=0;i<ASIZE;i++)
        {
            a[i] = a2[i]*h1 + a3[i]*h2;
        }
    }
    else
    {
        h1 = (zz-zz4)/(zz3-zz4);
        h2 = 1.0 - h1;
        for(i=0;i<ASIZE;i++)
        {
            a[i] = a3[i]*h1 + a4[i]*h2;
        }
    }




    /* hence the fit */
    l = (a[0] + a[1]*m + a[2]*m3)/(1.0 + a[3]*m3);
    Clamp(l,0.0,1.0);
    return (l);

#undef ASIZE

}

static double Karakas_mcmin_for_dredgeup(const double m,
                                         const double phase_start_mass,
                                         struct stardata_t * Restrict const stardata)
{

    /* Updated version from Onno (thanks Onno!) */
#define ASIZE 4
    const Abundance zz1 = 0.02;
    const Abundance zz2 = 0.008;
    const Abundance zz3 = 0.004;
    const Abundance zz4 = 0.0001;
    const double a1[ASIZE] = {0.732759,-0.0202898,-0.0385818,0.0115593};
    const double a2[ASIZE] = {0.672660,0.0657372,-0.1080931,0.02754823};
    const double a3[ASIZE] = {0.516045,0.2411016,-0.1938891,0.0446382};
    //double a4[ASIZE] = {0.516045,0.2411016,-0.1938891,0.0446382};
    /* updated values from amanda */
    //const double a4[ASIZE]={0.78887270271463,-0.43719446893414,0.24178487467924,-0.031176683714852};
    /* updated fit from rob */
    const double a4[ASIZE]={2.18500e-01,6.21750e-01,-3.85190e-01 ,8.84560e-02};

    double h1,h2;
    double y;
    Abundance zz = stardata->common.metallicity;
    Clamp(zz,0.0001,0.02);


    /*
     * Choose to interpolcate the coefficients or the result.
     * Really, you should interpolate the result, such that the
     * expressions are calculated at metallicities at which they
     * are known to be correct.
     */
    //#define INTERPOLATE_COEFFS
#define INTERPOLATE_MCMIN

    const double *al , *ar;
#ifdef INTERPOLATE_COEFFS
    double a[ASIZE];
    unsigned int i;

    if(zz>zz2)
    {
        h1 = (zz-zz2)/(zz1-zz2);
        al = a1;
        ar = a2;
    }
    else if(zz>zz3)
    {
        h1 = (zz-zz3)/(zz2-zz3);
        al = a2;
        ar = a3;
    }
    else
    {
        h1 = (zz-zz4)/(zz3-zz4);
        al = a3;
        ar = a4;
    }
    h2 = 1.0 - h1;
    for(i=0;i<ASIZE;i++)
    {
        a[i] = al[i]*h1 + ar[i]*h2;
    }
    y = Cubic_array(m,a);
#endif

#ifdef INTERPOLATE_MCMIN
    double y1,y2;
    if(zz>zz2)
    {
        h1 = (zz-zz2)/(zz1-zz2);
        al = a1;
        ar = a2;
    }
    else if(zz>zz3)
    {
        h1 = (zz-zz3)/(zz2-zz3);
        al = a2;
        ar = a3;
    }
    else
    {
        h1 = (zz-zz4)/(zz3-zz4);
        al = a3;
        ar = a4;
    }
    y1 = Cubic_array(m,al);
    y2 = Cubic_array(m,ar);
    h2 = 1.0 - h1;
    y = h1*y1 + h2*y2;
#endif

    /* Now to fix for M>Msdu-0.5 */
    y = Max3(0.0,
             Karakas2002_mc1tp(m,phase_start_mass,stardata),
             Min(0.7,y));

    return(y);
}
