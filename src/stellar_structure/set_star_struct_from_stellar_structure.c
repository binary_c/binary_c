#include "../binary_c.h"
No_empty_translation_unit_warning;


void set_star_struct_from_stellar_structure(struct star_t * const newstar,
                                            struct star_t * const oldstar)
{
    /*
     * Given a new_stellar_structure in s, set the appropriate
     * variables in a star_t struct.
     */
    oldstar->age = newstar->age;
    oldstar->stellar_type = newstar->stellar_type;
    oldstar->radius = newstar->radius;
    oldstar->effective_radius =
        (oldstar->roche_radius>TINY && oldstar->radius>oldstar->roche_radius) ?
        Max(oldstar->roche_radius,oldstar->core_radius) : oldstar->radius;
    oldstar->mass = newstar->mass;
    oldstar->baryonic_mass = newstar->baryonic_mass;
    oldstar->core_mass[CORE_He] = newstar->core_mass[CORE_He];
    oldstar->core_radius = newstar->core_radius;
    oldstar->luminosity = newstar->luminosity;
    oldstar->tm = newstar->tm;
    oldstar->tn = newstar->tn;
    oldstar->renv = newstar->renv;
    oldstar->stellar_type = newstar->stellar_type;
    oldstar->moment_of_inertia_factor = newstar->moment_of_inertia_factor;
    oldstar->SN_type = newstar->SN_type;
    oldstar->phase_start_mass = newstar->phase_start_mass;
    oldstar->phase_start_core_mass = newstar->phase_start_core_mass;
}
