#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BSE
#include "stellar_structure_debug.h"

static double Pure_function core_growth_q(const double Z);
static double TPAGB_radius(struct stardata_t * const stardata,
                           struct star_t * const newstar);

/************************************************************/

void stellar_structure_TPAGB(struct star_t * Restrict const newstar,
                             double * Restrict const mcmax,
                             const double mcbagb Maybe_unused,
                             struct stardata_t * Restrict const stardata,
                             const Caller_id caller_id)
{
    if(DEBUG)
    {
        char * cores = core_string(newstar,FALSE);
        Dprint("stellar_structure_TPAGB (caller_id=%d, num_thermal_pulses=%g, %s)\n",
               caller_id,
               newstar->num_thermal_pulses,
               cores)
            Safe_free(cores);
    }

    /*
     * TPAGB star stellar structure
     */
    newstar->stellar_type = TPAGB;
    newstar->derivative[DERIVATIVE_STELLAR_GB_CORE_MASS] = 0.0;
    newstar->derivative[DERIVATIVE_STELLAR_He_CORE_MASS] = 0.0;
    newstar->derivative[DERIVATIVE_STELLAR_CO_CORE_MASS] = 0.0;
    newstar->derivative[DERIVATIVE_STELLAR_He_CORE_MASS_NO_TDUP] = 0.0;

    /*
     * Choose the AGB structure method
     *
     * This is only relevant if we're running
     * the 'default' method : then use Hurley without
     * NUCSYN, and Karakas with NUCSYN.
     *
     * This is really a backwards compatibility option,
     * it's how binary_c worked for a decade or more.
     */
    const Core_algorithm core_algorithm = AGB_Core_Algorithm;

#if defined NUCSYN && defined NUCSYN_SECOND_DREDGE_UP
    nucsyn_check_for_second_dredge_up(stardata,newstar,mcbagb);
#endif

    /*
     * compare to the previous stardata to determine
     * whether we were a TPAGB star in the past. Because previous_stardata
     * does not change between multi-step solver calls, this
     * always works.
     *
     * If we have no previous_stardata, use num_thermal_pulses.
     */
    const Boolean first_pulse =
        stardata->previous_stardata == NULL ?
        newstar->num_thermal_pulses >= 0 :
        stardata->previous_stardata->star[newstar->starnum].stellar_type != TPAGB;

    const double menv = Max(0.0,
                            newstar->mass - newstar->core_mass[CORE_He]);
    Dprint("first pulse? %d\n",first_pulse);

    if(unlikely(first_pulse))
    {
        /*
         * Many things must be initialised at the
         * first thermal pulse
         */
        newstar->num_thermal_pulses = 0.0;
        newstar->time_first_pulse = newstar->age;
        newstar->time_prev_pulse = newstar->age;
        newstar->prev_tagb = newstar->age;
        Dprint("time first pulse set to %g\n",newstar->time_first_pulse);
        const double mc1tp = guess_mc1tp(newstar->phase_start_mass,
                                         newstar->phase_start_mass,
                                         newstar,
                                         stardata);

        /*
         * Adjust core for second dredge up.
         * Assume we can never dredge into the CO core.
         */
        const double mcnew = Max(newstar->core_mass[CORE_CO],
                                 Min(mc1tp,
                                     newstar->core_mass[CORE_He]));

        Dprint("MC new %g 1tp %g\n",
               mcnew,
               mc1tp);

        newstar->core_mass[CORE_He] = mcnew;
        newstar->core_mass_no_3dup = mcnew;
        newstar->mc_1tp = mcnew;
        newstar->menv_1tp = newstar->mass - mcnew;
        newstar->Mc_prev_pulse = mcnew;
        newstar->interpulse_period =
            Karakas2002_interpulse_period(stardata,
                                          newstar->mc_1tp,
                                          newstar->mass,
                                          newstar->mc_1tp,
                                          stardata->common.metallicity,
                                          0.0,
                                          newstar);

        /*
         * newstar->time_next_pulse is relative to the start
         * of the TPAGB
         */
        newstar->time_next_pulse = newstar->time_prev_pulse + newstar->interpulse_period;
    }
    else
    {
        /* if star accretes, increase menv_1tp */
        newstar->menv_1tp = Max(menv, newstar->menv_1tp);
    }

    /*
     * age on the TPAGB (Myr)
     */
    const double tagb = newstar->age - newstar->time_first_pulse;
    Dprint(
            "TPAGBX tagb = %g from age = %g, first pulse at %g\n",
            tagb,
            newstar->age,
            newstar->time_first_pulse
        );

    /*
     * Lower limit to the interpulse period is 0.01 years
     * (this is arbitrary but prevents numerical issues).
     */
    newstar->interpulse_period = Max(1e-8,
                                     newstar->interpulse_period);


    /*
     * Calculate the third dredge up efficiency, lambda,
     * also sets above_mcmin : a Boolean which tells us whether
     * the core mass exceeds the minimum core mass for third
     * dredge up
     */
    Boolean above_mcmin;
    newstar->lambda_3dup = lambda_3dup(stardata,
                                       newstar,
                                       &above_mcmin);

    /*
     * cannot grow through the surface
     */
    const double dmcdt_max = (newstar->mass +
                              newstar->core_mass[CORE_He]) / stardata->model.dt;

    if(core_algorithm == AGB_CORE_ALGORITHM_KARAKAS)
    {
        /*
         * Grow the core because of hydrogen shell burning
         */

        /*
         * Requires the luminosity. This is prior to
         * dredge up, so is only approximately correct.
         */
        newstar->luminosity = TPAGB_luminosity(newstar->stellar_type,
                                               newstar->mass,
                                               newstar->core_mass[CORE_He],
                                               newstar->mc_1tp,
                                               newstar->core_mass_no_3dup,
                                               newstar->age,
                                               stardata->common.metallicity,
                                               newstar->phase_start_mass,
                                               newstar->num_thermal_pulses,
                                               newstar->interpulse_period,
                                               newstar->time_first_pulse,
                                               newstar->time_prev_pulse,
                                               newstar->time_next_pulse,
                                               stardata,
                                               newstar->bse->GB);

        const double qh6 = core_growth_q(stardata->common.metallicity);
        const double lmax = newstar->mass < 3.0 ? 3e4 : 4e4;
        double dmcdt = Min(dmcdt_max,
                           Min(newstar->luminosity, lmax) * qh6) ; // per year

        /*
         * Hence the core growth rates
         */
        newstar->derivative[DERIVATIVE_STELLAR_GB_CORE_MASS] = 0.0;
        newstar->derivative[DERIVATIVE_STELLAR_He_CORE_MASS] += dmcdt * (1.0 - newstar->lambda_3dup);
        newstar->derivative[DERIVATIVE_STELLAR_He_CORE_MASS_NO_TDUP] += dmcdt;
        newstar->derivative[DERIVATIVE_STELLAR_CO_CORE_MASS] += dmcdt * (1.0 - newstar->lambda_3dup);

        /*
         * Note: dt, and hence dmc, could be negative.
         */
        Dprint("dmc = %g from dmc/dt = %g, qh6 = %g, L = %g, dt = %g, newstar->dm_3dup = %g \n",
               dmcdt * stardata->model.dt,
               dmcdt,
               qh6,
               newstar->luminosity,
               stardata->model.dt,
               newstar->dm_3dup
            );



        Dprint("H-shell burn grown core to %g\n",newstar->core_mass[CORE_He]);
        Dprint("Check for next pulse at tagb=%g Myr : dntp = %g, Next_pulse = %d, Interpulse period %g Myr\n",
               tagb,
               newstar->dntp,
               Next_pulse(newstar),
               newstar->interpulse_period);

    }
    else if(core_algorithm == AGB_CORE_ALGORITHM_HURLEY)
    {
        /*
         * BSE core-mass growth algorithm
         *
         *
         * Do not let lambda_3dup drop :
         * this causes problems when the envelope
         * mass is small because lambda -> 0 (formally)
         * because Menv < Menv_min_for_third_dup.
         * This causes the core to grow wildly,
         * which is wrong and gives a SN. Leaves lambda
         * as is is equivalent to the Hurley algorithm.
         */
        newstar->lambda_3dup = Max(newstar->lambda_3dup,
                                   lambda_3dup(stardata,
                                               newstar,
                                               &above_mcmin));

        /*
         * The original BSE algorithm calculates
         * core mass changes by integrating the
         * Mc(t) function manually. This requires
         * an exact solution for the time at the first
         * thermal pulses.
         *
         * Instead of this, we calculate dMc/dt
         * and integrate this (with respect to time)
         * numerically. This is more stable against
         * non-BSE radii, luminosities, etc. which
         * give a different time at the first pulse.
         * Now, just the time *relative* to the first
         * thermal pulse time is important.
         */

        /*
         * calculate raw dMc/dt because of shell burning
         */
        const double dmcdt = Min(dmcdt_max,
                                 dmcgbtf_dt(
                                     newstar->age,
                                     newstar->bse->GB[GB_A_H_HE],
                                     newstar->bse->GB,
                                     newstar->bse->timescales[T_TPAGB_TINF_1],
                                     newstar->bse->timescales[T_TPAGB_TINF_2],
                                     newstar->bse->timescales[T_TPAGB_TX]
                                     ));
        newstar->derivative[DERIVATIVE_STELLAR_GB_CORE_MASS] = 0.0;
        newstar->derivative[DERIVATIVE_STELLAR_He_CORE_MASS] = dmcdt * 1e-6;
        newstar->derivative[DERIVATIVE_STELLAR_CO_CORE_MASS] = dmcdt * 1e-6;

        /*
         * Increase core mass, (eventually) taking into account
         * any third dredge up
         */
        newstar->derivative[DERIVATIVE_STELLAR_GB_CORE_MASS] = 0.0;
        newstar->derivative[DERIVATIVE_STELLAR_He_CORE_MASS] += dmcdt * 1e-6 * (1.0 - newstar->lambda_3dup);
        newstar->derivative[DERIVATIVE_STELLAR_He_CORE_MASS_NO_TDUP] += dmcdt * 1e-6;
        newstar->derivative[DERIVATIVE_STELLAR_CO_CORE_MASS] += dmcdt * 1e-6 * (1.0 - newstar->lambda_3dup);

#ifdef ORIGINAL_BSE_TPAGB
        /*
         * Original BSE algorithm.
         * Note, will not always work well especially
         * when combined with other formalisms!
         */
        newstar->core_mass[CORE_He] = mcgbtf(
            newstar->age,
            newstar->bse->GB[GB_A_H_HE],
            newstar->bse->GB,
            newstar->bse->timescales[T_TPAGB_TINF_1],
            newstar->bse->timescales[T_TPAGB_TINF_2],
            newstar->bse->timescales[T_TPAGB_TX]
            );

        /*
         * Approximate 3rd Dredge-up on AGB by limiting (CO) Core_Mass.
         */
        newstar->core_mass[CORE_CO] = mcgbtf(
            newstar->bse->timescales[T_TPAGB_FIRST_PULSE],
            newstar->bse->GB[GB_A_H_HE],
            newstar->bse->GB,
            newstar->bse->timescales[T_TPAGB_TINF_1],
            newstar->bse->timescales[T_TPAGB_TINF_2],
            newstar->bse->timescales[T_TPAGB_TX]
            );
        newstar->core_mass[CORE_He] -= newstar->lambda_3dup *
            (newstar->core_mass[CORE_He] - newstar->core_mass[CORE_CO]);
#endif // ORIGINAL_BSE_TPAGB
    }
    else
    {
        Exit_binary_c(BINARY_C_ALGORITHM_BRANCH_FAILURE,
                      "AGB core algorithm %d is unknown",
                      core_algorithm);
    }



    /*
     * Calculate luminosity
     */
    newstar->luminosity = TPAGB_luminosity(newstar->stellar_type,
                                           newstar->mass,
                                           newstar->core_mass[CORE_He],
                                           newstar->mc_1tp,
                                           newstar->core_mass_no_3dup,
                                           newstar->age,
                                           stardata->common.metallicity,
                                           newstar->phase_start_mass,
                                           newstar->num_thermal_pulses,
                                           newstar->interpulse_period,
                                           newstar->time_first_pulse,
                                           newstar->time_prev_pulse,
                                           newstar->time_next_pulse,
                                           stardata,
                                           newstar->bse->GB);

    if(TPAGB_LUMTYPE == TPAGB_LUMINOSITY_DIPS &&
       tagb < MAX_TPAGB_TIME &&
       newstar->num_thermal_pulses < PULSE_LUM_DROP_N_PULSES)
    {
        /*
         * crude attempt to parameterise the post-pulse luminosity drop
         */
        newstar->spiky_luminosity = newstar->luminosity
            * (1.0-Min(THERMAL_PULSE_LUM_DROP_FACTOR,
                       THERMAL_PULSE_LUM_DROP_FACTOR*
                       exp(-THERMAL_PULSE_LUM_DROP_TIMESCALE*
                           (tagb - newstar->time_prev_pulse)/newstar->interpulse_period)
                   ));
    }


    if(stardata->preferences->require_drdm == TRUE)
    {
        const double dm = newstar->mass * 1e-7;
        update_mass(stardata,newstar,dm);
        const double rm = TPAGB_radius(stardata,newstar);
        update_mass(stardata,newstar,-dm);
        newstar->radius = TPAGB_radius(stardata,newstar);
        newstar->drdm = (rm - newstar->radius) / dm;
    }
    else
    {
        newstar->radius = TPAGB_radius(stardata,
                                       newstar);
    }
    Dprint("TPAGB L = %g\n",newstar->luminosity);
    Dprint("TPAGB R = %g\n",newstar->radius);

    /*
     * CO core mass = He core mass - intershell mass
     */
    newstar->core_mass[CORE_CO] = Max3(0.0,
                                       0.0,
                                       newstar->core_mass[CORE_He] -
                                       dm_intershell(newstar->core_mass[CORE_He]));

    /*
     * Maximum core mass cannot exceed the total mass
     */
    *mcmax = Min(newstar->mass,*mcmax);

#ifdef WD_KICKS
    if(
        /* WD kick at a given pulse number */
        (stardata->preferences->wd_kick_when == WD_KICK_AT_GIVEN_PULSE &&
         newstar->already_kicked_WD==FALSE &&
         newstar->num_thermal_pulses+1 > stardata->preferences->wd_kick_pulse_number)
        ||
        /* WD kick every pulse */
        (stardata->preferences->wd_kick_when == WD_KICK_AT_EVERY_PULSE &&
         ( (int)(newstar->num_thermal_pulses+0.01) - newstar->prev_kick_pulse_number > 0))
        )
    {
        newstar->kick_WD=TRUE;
    }
#endif // WD_KICKS

    /*
     * Require core radius
     */
#ifdef HALL_TOUT_2014_RADII
    newstar->core_radius = Hall_Tout_2014_low_mass_HG_RGB_radius(stardata,
                                                                 newstar,
                                                                 newstar->core_mass[CORE_He]);
#else
    newstar->core_radius = 5.0 * rwd(stardata,
                                     newstar,
                                     newstar->core_mass[CORE_He]);
#endif // HALL_TOUT_2014_RADII
    newstar->core_radius = Max(newstar->core_radius,
                               rns(newstar->core_mass[CORE_He]));
    Dprint("core radius %g\n",
           newstar->core_radius);

#ifdef NUCSYN
    /*
     * Apply 3DUP/HBB changes to surface abundances only
     * on the final step of the solver.
     */
    if(stardata->model.intermediate_step == FALSE)
    {
        nucsyn_tpagb(&newstar->age,
                     &newstar->core_mass[CORE_He],
                     &newstar->mass,
                     &newstar->luminosity,
                     &newstar->radius,
                     newstar,
                     stardata);
    }
#endif // NUCSYN

    if(DEBUG)
    {
        char * cores = core_string(newstar,FALSE);
        Dprint("Return from TPAGB %s\n",cores);
        Safe_free(cores);
    }

}


static double Pure_function core_growth_q(const double Z)
{
    /*
     * Core growth factor where
     * dMc/dt = Q L
     *
     * This is based on Karakas' models, see
     * Izzard et al. (2004) for details.
     *
     * The factor 0.5 is to fit the detailed models
     * better with the new binary_c evolution algorithm.
     */
    return
        0.5 *
        1e-11 * (2.72530 - 1.8629 * Z)
        *
        Max(Min(1.0,7.94870e-01 + 5.12820e+01 * Z),0.8);
}


double TPAGB_luminosity(const Stellar_type stellar_type,
                        const double m,
                        const double mc,
                        const double mc1tp,
                        const double mcnodup,
                        const double age,
                        const double Z,
                        const double phase_start_mass,
                        const double num_thermal_pulses,
                        const double interpulse_period,
                        const double time_first_pulse,
                        const double time_prev_pulse,
                        const double time_next_pulse,
                        struct stardata_t * Restrict const stardata,
                        double * Restrict const GB
    )
{
    /*
     * Generic wrapper for the current TPAGB luminosity.
     */

    double luminosity;
    int luminosity_algorithm = AGB_Luminosity_Algorithm;

    if(luminosity_algorithm == AGB_LUMINOSITY_ALGORITHM_KARAKAS)
    {
        luminosity = Karakas2002_lumfunc(stellar_type,
                                         m,
                                         mc1tp,
                                         m - mc,
                                         mcnodup,
                                         phase_start_mass,
                                         num_thermal_pulses,
                                         interpulse_period,
                                         time_first_pulse,
                                         time_prev_pulse,
                                         time_next_pulse,
                                         age,
                                         Z,
                                         stardata,
                                         GB);
    }
    else if(luminosity_algorithm == AGB_LUMINOSITY_ALGORITHM_HURLEY)
    {
        luminosity = lmcgbf(mc,GB);
    }
    else
    {
        luminosity = 0.0;
        Exit_binary_c(BINARY_C_ALGORITHM_BRANCH_FAILURE,
                      "AGB luminosity algorithm %d is unknown",
                      luminosity_algorithm);
    }

    return luminosity;
}

static double TPAGB_radius(struct stardata_t * const stardata,
                           struct star_t * const newstar)
{
    /*
     * Hence radius
     */
    double r;
    double r_Hurley =
        ragbf(newstar->mass,
              newstar->luminosity,
              stardata->common.metallicity_parameters[ZPAR_MASS_HE_FLASH],
              stardata->common.giant_branch_parameters
#ifdef AXEL_RGBF_FIX
              ,stardata->common.metallicity
#endif
            );
    const Radius_algorithm radius_algorithm = AGB_Radius_Algorithm;

    if(radius_algorithm == AGB_RADIUS_ALGORITHM_KARAKAS)
    {
        r = Karakas2002_ragb_teff(newstar->mass,
                                  stardata->common.metallicity,
                                  newstar->luminosity,
                                  newstar->phase_start_mass,
                                  newstar->mc_1tp,
                                  newstar->mass - newstar->core_mass[CORE_He],
                                  newstar->core_mass_no_3dup,
                                  newstar,
                                  stardata);
        /*
         * If M > TPAGB_MASS_BREAK, interpolate into the
         * Hurley+ (2002) radius fit.
         */
        if(newstar->mass > TPAGB_MASS_BREAK)
        {
            const double f8 = 1.0 / (1.0 + pow(0.01, newstar->mass - 8.0));
            r = f8*r_Hurley + (1.0-f8)*r;
        }
    }
    else if(radius_algorithm == AGB_RADIUS_ALGORITHM_HURLEY)
    {
        r = r_Hurley;
    }
    else
    {
        Exit_binary_c(BINARY_C_ALGORITHM_BRANCH_FAILURE,
                      "AGB radius algorithm %d is unknown",
                      radius_algorithm);
    }
    return r;
}
#endif//BSE
