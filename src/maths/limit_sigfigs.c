#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Limit the double f to the digits
 * number of significant figures.
 */

Pure_function double limit_sigfigs(const double f,
                                   const int n)
{
    if(f == 0.0)
    {
        return 0.0;
    }
    else
    {
        const double scale = exp10(n);
        return round(f * scale) / scale;
    }
}
