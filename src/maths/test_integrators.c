#include "../binary_c.h"
No_empty_translation_unit_warning;


#define Apply                                                           \
    star->derivative[DERIVATIVE_STELLAR_MASS] = pow(t,10.0) + 1.023232; \
    apply_derivative(stardata,                                          \
                     &star->mass,                                       \
                     &wanted,                                           \
                     star->derivative,                                  \
                     dt,                                                \
                     DERIVATIVE_STELLAR_MASS,                           \
                     DERIVATIVES_GROUP_STELLAR,                         \
                     NULL,                                              \
                     NULL);


void No_return test_integrators(struct stardata_t * stardata)
{
    /*
     * Test binary_c's integration schemes
     */

    double t = 0.0;
    double dt = 0.0001;
    const double maxt = 2.0;
    struct star_t * star = &stardata->star[0];
    struct star_t * prev = &stardata->previous_stardata->star[0];
    double wanted;

    star->mass = 1.0;
    prev->mass = 1.0;

    printf("t = %g : M = %g \n",
           t,
           star->mass
        );

    while(t + dt*0.01 < maxt)
    {


        if(stardata->preferences->solver == SOLVER_FORWARD_EULER)
        {
            stardata->model.solver_step = 0;
            stardata->model.intermediate_step = FALSE;
            Apply;
            t+=dt;
        }
        else if(stardata->preferences->solver == SOLVER_RK2)
        {
            dt *= 0.5;
            stardata->model.solver_step = 0;
            stardata->model.intermediate_step = TRUE;
            Apply;
            t+=dt;
            stardata->model.solver_step = 1;
            stardata->model.intermediate_step = FALSE;
            Apply;
            t+=dt;
            dt *= 2.0;
        }
        else if(stardata->preferences->solver == SOLVER_RK4)
        {
            dt *= 0.5;

            stardata->model.solver_step = 0;
            stardata->model.intermediate_step = TRUE;
            Apply;
            t+=dt;

            // k2(t+0.5)
            stardata->model.solver_step = 1;
            Apply;

            // k3(t+0.5)
            stardata->model.solver_step = 2;
            Apply;
            t += dt;

            stardata->model.solver_step = 3;
            stardata->model.intermediate_step = FALSE;
            Apply;
            dt *= 2.0;
        }


        printf("t = %g : M = %g \n",
               t,
               star->mass
            );
        /* save to previous */
        prev->mass = star->mass;
    }

    Exit_binary_c(0,"Exit test integrators");
}
