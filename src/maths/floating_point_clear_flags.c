#include "../binary_c.h"
No_empty_translation_unit_warning;
#include <fenv.h>

/*
 * Clear floating-point exception flags
 */

void floating_point_clear_flags(struct stardata_t * const stardata Maybe_unused)
{
    feclearexcept(FE_ALL_EXCEPT);
}
