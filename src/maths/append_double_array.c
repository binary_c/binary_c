#include "../binary_c.h"
No_empty_translation_unit_warning;

void append_double_array(struct stardata_t * const stardata,
                         struct double_array_t * const a,
                         struct double_array_t * const b)
{
    /*
     * Make and return a new double array
     *
     * In this function, we append b to a.
     */
    a->doubles = Realloc(a->doubles,
                         sizeof(double) * (a->n+b->n));
    memcpy(a->doubles + a->n,
           b->doubles,
           sizeof(double) * b->n);
    a->n += b->n;
}
