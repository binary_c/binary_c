#include "../binary_c.h"
No_empty_translation_unit_warning;


void SolveCubic2(const double b,
                 const double c,
                 const double d,
                 unsigned int * nsolutions, /* number of REAL solutions */
                 double * Restrict x,
                 double * Restrict y)
{

    /*
     * Code taken from http://www.snippets.org/snippets/portable/Cubic+C.php3
     * but this is very similar to the numerical recipies version
     */
    /* This is a modified version to give the real part of each root */
    // NB assume a=1
    //double    a1 = b/a, a2 = c/a, a3 = d/a;
    const double a1 = b, a2 = c, a3 = d;
    const double Q = (a1*a1 - 3.0*a2)/9.0;
    const double Q3 = Pow3(Q);
    const double R = (a1*(2.0*a1*a1 - 9.0*a2) + 27.0*a3)/54.0;
    const double R2_Q3 = R*R - Q3;
    double theta, A, B, C;
    y[0]=0.0; // first root is always real!

    if(R2_Q3 <= 0)
    {
        *nsolutions = 3;
        theta = acos(R/sqrt(Q3));
        /* real parts */
        A = a1/3.0;
        B = -2.0*sqrt(Q);
        C = theta/3.0;
        x[0] = B*cos(C) - A;
        x[1] = B*cos(C+(2.0*PI)/3.0) - A;
        x[2] = B*cos(C+(4.0*PI)/3.0) - A;

        y[1]=0.0;
        y[2]=0.0;
    }
    else
    {
        *nsolutions = 1;
        /* assuming real Q,R (likely!) */
        B = fabs(R);
        A = -(R/B)*cbrt(B+sqrt(R2_Q3));
        //if(fabs(A)==0) // what does this mean?!
        if(Is_zero(A))
        {
            B = 0;
        }
        else
        {
            B = Q/A;
        }
        C = a1/3.0;
        x[0] = (A+B)-C;
        /* the next two are real parts only */
        x[2] = x[1] = -0.5*(A+B)-C;

        /*
         * The complex part is i * (sqrt(3)/2.0)*(A-B), add this to x[1] and
         * subtract from x[2]
         */
        y[1]=sqrt(3.0)/2.0*(A-B);
        y[2]=-y[1];
    }
}
