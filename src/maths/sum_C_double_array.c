#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Return the sum of an array of doubles
 */
double sum_C_double_array(const double * const x,
                          const size_t size)
{
    double sum = 0.0;
    const double * p = x;
    const double * const max = x + size;
    PRAGMA_GCC_IVDEP
    while(p<max)
    {
        sum += *p;
        p++;
    }
    return sum;
}
