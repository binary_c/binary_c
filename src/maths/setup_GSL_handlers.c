#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Handler for GSL errors
 */
void setup_GSL_handlers(struct stardata_t * Restrict const stardata Maybe_unused)
{
    gsl_set_error_handler_off();
}
