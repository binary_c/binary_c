#include "../binary_c.h"
No_empty_translation_unit_warning;

struct Boolean_array_t * new_Boolean_array(const ssize_t n)
{
    /*
     * Make and return a new Boolean array
     */
    struct Boolean_array_t * s = Malloc(sizeof(struct Boolean_array_t));
    s->n = n;
    if(n<=0)
    {
        s->Booleans = NULL;
    }
    else
    {
        s->Booleans = Malloc(sizeof(Boolean) * n);
    }
    return s;
}
