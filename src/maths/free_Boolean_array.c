#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Free a Boolean array
 */
void free_Boolean_array(struct Boolean_array_t ** Restrict Boolean_array)
{
    if(Boolean_array != NULL &&
       *Boolean_array != NULL)
    {
        Safe_free((*Boolean_array)->Booleans);
        Safe_free_nocheck(*Boolean_array);
    }
}
