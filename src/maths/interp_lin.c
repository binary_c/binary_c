#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef KEMP_NOVAE

double interp_lin(const double x,
                  const double xl,
                  const double xh,
                  const double yl,
                  const double yh)
{
    /*
     * Returns linearly interpolated or extrapolated value.
     *
     * This function will do whatever it takes, so use with caution.
     *
     * x: x where you want to interpolate to.
     * xl: x lower
     * xh: x higher
     * yl: y lower
     * yh: y higher
     * returns interpvalue, ie. y
     */
    return yl + (x - xl) * (yh - yl) / (xh - xl);
}
#endif // KEMP_NOVAE
