#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Check array for inf content.
 * Return TRUE if inf found, FALSE otherwise.
 */
Boolean check_double_array_for_inf(struct double_array_t * const Restrict d)
{
    /*
     * Check array, of size n, for infs
     */
    const double * const max = d->doubles + d->n;
    for(double * p = d->doubles; p<max; p++)
    {
        if(isinf(*p))
        {
            return TRUE;
        }
    }
    return FALSE;
}
