#pragma once
#ifndef GENERIC_BISECT_H
#define GENERIC_BISECT_H

/*
 * Headers for the generic_bisect.c function
 */


/*
 * Clean up and return
 */
#define Cleanup_bisect                          \
    va_end(args_main);                        \
    Safe_free(GSL_args);

#define Clean_return(X)                                 \
    Cleanup_bisect;                                     \
    return(*error == BINARY_C_BISECT_ERROR_NONE ?       \
           (uselog ? exp10((X)) : (X)) :             \
           BISECT_RETURN_FAIL)

/*
 * Define the function call.
 * Note: if the return value is within tolerance,
 * immediately return, this is good enough!
 *
 * Should it be args or &args?
 */
#define CALLFUNC(X)                             \
    GSL_args->error = 0;                        \
    va_copy(GSL_args->args,args_main);        \
    va_start(GSL_args->args,alpha);             \
                                                \
    ret = (*func)(                              \
        (uselog ? exp10((X)) : (X)),         \
        (void*)(GSL_args));                     \
                                                \
    va_end(GSL_args->args);                     \
                                                \
    CALLFUNCNANCHECK(ret);                      \
    if(fabs(ret) < tol)                         \
    {                                           \
        Clean_return(X);                        \
    }                                           \
    POSTFUNC;



//#define BISECT_SHOW_RANGE
/*
 * Bisection stats and checks.
 * NB Stats are not thread safe, and are
 *    just for testing and/or optimization.
 */
//#define BISECT_STATS
#define BISECT_BRACKET_CHECKS
#ifdef NANCHECKS
#define BISECT_NANS
#endif

#define BISECT_INCREASE 2.0
#define BISECT_DECREASE 0.5

/* allow verbosity */
#define VBPRINT(...)                            \
    if(vb == TRUE)                              \
    {                                           \
        generic_bisect_vbprint(n,__VA_ARGS__);  \
    }

static void generic_bisect_vbprint(int n,
                                   char * Restrict format,
                                   ...) Gnu_format_args(2,3);

static double generic_bisect_funcwrapper(const double x,
                                         void * f);

double brent_GSL_valist(brent_function wrapper,
                        brent_function func,
                        const int itmax,
                        const double xmin,
                        const double xmax,
                        const double tol,
                        const Boolean uselog,
                        va_list args);



#endif // GENERIC_BISECT_H
