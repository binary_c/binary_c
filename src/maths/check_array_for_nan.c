#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Check array for nan content.
 * Return TRUE if nan found, FALSE otherwise.
 */
Boolean check_array_for_nan(const double * const array,
                            const size_t n)
{
    /*
     * Check array, of size n, for nans
     */
    for(size_t i=0; i<n; i++)
    {
        if(isnan(array[i]))
        {
            return TRUE;
        }
    }
    return FALSE;
}
