#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Free an long_int array
 */
void free_long_int_array(struct long_int_array_t ** Restrict long_int_array)
{
    if(long_int_array != NULL &&
       *long_int_array != NULL)
    {
        Safe_free((*long_int_array)->long_ints);
        Safe_free_nocheck(*long_int_array);
    }
}
