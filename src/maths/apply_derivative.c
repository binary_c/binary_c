#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Generic derivative application for various solvers.
 *
 *     Call this with:
 *
 * stardata : as always, we need stardata
 *
 * *value : a pointer to the (double precision) value that needs to
 *          have the derivative applied to it
 *
 * *d : an array of derivatives from which we choose derivative
 *      (this is either model->derivative or star->derivative)
 *
 * nd : the derivative number (an index)
 *
 *
 * On the final step, the derivative is updated to the equivalent derivative
 * for a forward-Euler scheme, i.e. a suitable average over the timestep.
 * (This is why the input pointers are not read-only.)
 *
 * Note:
 *
 * We use pointers, to both the derivatives and the value, so you
 * can look up the derivative and value in previous_stardatas if you
 * like, perhaps to give you a better estimate of the true derivative.
 *
 * e.g. the RK2 scheme has two steps, each for time dt/2 :
 *
 * 1) update value(t) to value(t+dt/2) : this overwrites value(t) using the derivatives
 *                                       at t (in *d)
 *
 * 2) update value(t) to value(t+dt) : this uses the derivatives at
 *                                     t+dt/2 (in *d) and the value at the previous step,
 *                                     which is the original value(t).
 *
 * Note:
 *    Even if dt = 0 we MUST use the algorithm to set the various
 *    intermediate solutions and the final forward-Euler equivalent derivative.
 *
 *
 * Return value:
 *
 * Once a derivative is calculated, the (optional) checkfunc is called
 * to check the value.
 *
 * If this returns FALSE, then we restore the
 * value passed in and return BINARY_C_APPLY_DERIVATIVE_FAILED.
 *
 * Otherwise we return BINARY_C_APPLY_DERIVATIVE_SUCCESS.
 */



/*
 * Aprint is the "apply derivative" debug statement.
 *
 * You should set Aprint_array to point to ont of the drivative
 * arrays (e.g. stardata->star[0].derivative or stardata->model.derivative)
 * and Aprint_n should be one of the derivatives, e.g.
 * DERIVATIVE_ORBIT_ANGMOM
 */

#define Aprint_array stardata->star[0].derivative
#define Aprint_n DERIVATIVE_STELLAR_CENTRAL_HYDROGEN

#define Aprint(...) {                                                   \
        if(nd==(Aprint_n) &&                                            \
           d==(Aprint_array))                                           \
        {                                                               \
            printf("%s",stardata->store->colours[RED]);                                           \
            printf(__VA_ARGS__);                                        \
            printf("%s",stardata->store->colours[COLOUR_RESET]);                                  \
            fflush(NULL);                                               \
        }                                                               \
    }

/* uncomment the next two lines to turn off debugging */
#undef Aprint
#define Aprint(...) /* do nothing */

/* shortcuts to the RK terms */
#define xn (*(double*)((char*)stardata->previous_stardatas[0]+offset))
#define x1p (*(double*)((char*)stardata->stardata_stack[0]+offset))
#define x2p (*(double*)((char*)stardata->stardata_stack[1]+offset))
#define x3p (*(double*)((char*)stardata->stardata_stack[2]+offset))





int apply_derivative(struct stardata_t * const stardata,
                     double * const value, /* pointer to the value, in stardata, to which we add the derivative */
                     double * const wanted, /* value wanted: only set when checkfunc fails, ignored if NULL */
                     double * const d, /* array of derivatives in stardata */
                     const double dt, /* timestep */
                     const Derivative nd, /* derivative number */
                     const Derivative_group derivative_group,
                     void * data, /* to be passed to the checkfunc, can be NULL */
                     derivative_check_function checkfunc
    )
{
    /*
     * Apply derivative d[nd] for timestep dt to *value.
     */
    int ret; // return value
    const size_t offset = (char*)value - (char*)stardata;
    Aprint("Apply derivative %u derivative_group %u=%s step %d, t = %g, var %p = %g, dx/dt = %g, dt = %g years in stardata = %p -> offset %zu : *value=%g, in stardata = %g, in previous_stardatas[0] = %g : ",
           nd,
           derivative_group,
           Derivative_group_string(derivative_group),
           stardata->model.solver_step,
           stardata->model.time,
           (void*)value,
           *value,
           d[nd],
           dt,
           (void*)stardata,
           offset,
           *value,
           *(double*)((char*)stardata + offset),
           xn
        );
    const double value_in = * value;

    if(stardata->preferences->solver == SOLVER_FORWARD_EULER)
    {
        /*
         * Forward Euler method
         *
         * Use an exponential solver if we can, otherwise
         * the (faster) linear solution.
         */
        const double was Maybe_unused = * value;
        Boolean is_exp;
        if(Is_not_zero(*value) &&
           Exponential_derivative(stardata,d,nd))
        {
            const double y = d[nd] * dt / *value;
            if(Is_not_zero(y) && fabs(y)<1.0)
            {
                *value *= exp(y);
                is_exp = TRUE;
            }
            else
            {
                *value += d[nd] * dt;
                is_exp = FALSE;
            }
        }
        else
        {
            *value += d[nd] * dt;
            is_exp = FALSE;
        }
        Dprint("APPLY d[%u=%s in %s] = %g (exp %s) for dt = %g -> Δ= %g : was %g now %g\n",
               nd,
               Derivative_string(derivative_group,nd),
               Derivative_group_string(derivative_group),
               d[nd],
               Yesno(is_exp),
               dt,
               dt*d[nd],
               was,
               *value);
    }
    else if(stardata->preferences->solver == SOLVER_RK2)
    {
        /*
         * Runge Kutta 2 method
         */
        if(stardata->model.solver_step == 0)
        {
            /*
             * First RK2 step : apply the previous timestep
             *                  derivative * 1/2
             */
            Aprint("< RK2(1) pre value=%g previous=%g ",
                    *value,
                    xn);
            *value += d[nd] * dt;
            Aprint("RK2(2) post %g >",*value);
        }
        else if(stardata->model.solver_step == 1)
        {
            /*
             * Second RK2 step : the derivatives are now
             *                   correct at t+dt/2, apply
             *                   them in full to the previous stardata.
             */
            Aprint("< RK2(2) pre value=%g previous=%g ",
                    *value,
                    xn);
            *value = xn + d[nd] * 2.0 * dt;

            /* reconstruct the forward-Euler equivalent derivative */
            d[nd] = Is_zero(dt) ? 0.0 : ((*value - xn)/(2.0*dt));

            Aprint("RK2(2) post %g >",*value);
        }
        else
        {
            Exit_binary_c(BINARY_C_ALGORITHM_OUT_OF_RANGE,
                          "Exceeded RK2 step number limit (2)\n");
        }
    }
    else if(stardata->preferences->solver == SOLVER_RK4)
    {
        /*
         * Runge Kutta 4 method
         */
        Aprint("\nApply RK4 step %d : stack=%p n_stack=%u\n",
               stardata->model.solver_step,
               (void*)stardata->stardata_stack,
               stardata->n_stardata_stack
            );
        if(stardata->stardata_stack==NULL ||
           stardata->n_stardata_stack < 3)
        {
            /*
             * Set up memory for k1,k2,k3
             */
            stardata->stardata_stack = Malloc(sizeof(struct stardata_t*)*3);
            for(int i=0;i<3;i++)
            {
                stardata->stardata_stack[i] = new_stardata(stardata->preferences); // k_i
            }
            stardata->n_stardata_stack = 3;
        }

        if(stardata->model.solver_step == 0)
        {
            /*
             * k1 calculation at t : leaves structure x1' at t+dt/2
             */
            Aprint("< RK4(k1) pre value=%g previous=%g ",
                    *value,
                    xn
                );
            const double k1p = d[nd] * dt;

            /*
             * Next calculations are at (x1',t+dt/2) = (xn + k1',t+dt/2)
             */
            x1p = *value = xn + k1p;

            Aprint("RK4(k1) post x1p = %g = %g>\n",x1p,*value);
            Aprint("KK1 %g\n",k1p*2.0);
        }
        else if(stardata->model.solver_step == 1)
        {
            /*
             * k2 calculation at t+dt/2, leaves structure at t+dt/2
             */
            Aprint("< RK4(k2) pre value=%g previous=%g ",
                    *value,
                    xn);
            const double k2p = d[nd] * dt;

            /*
             * Next calculations are at (x2',t+dt/2) = (xn + k2',t+dt/2)
             */
            x2p = *value = xn + k2p;

            Aprint("RK4(k2) post x2p = %g = %g>\n",x2p,*value);
            Aprint("KK2 %g\n",k2p*2.0);
        }
        else if(stardata->model.solver_step == 2)
        {
            /*
             * k3 calculation at t+dt/2 leaves structure at t+dt
             */
            Aprint("< RK4(k3) pre value=%g previous=%g ",
                    *value,
                    xn);
            const double k3p = d[nd] * dt;

            /*
             * Next calculations are at (x3', t+dt) i.e. at (xn + 2*k3p,t+dt)
             */
            x3p = *value = xn + 2.0 * k3p;

            Aprint("RK4(k3) post x3p = %g = %g >\n",x3p,*value);
            Aprint("KK3 %g\n",k3p*2.0);
        }
        else if(stardata->model.solver_step == 3)
        {
            /*
             * k4, hence final, calculation at t+dt
             */
            Aprint("< RK4(k4) pre value=%g previous=%g ",
                    *value,
                   *(double*)((char*)stardata->previous_stardatas[0]+offset));

            const double k4p = dt * d[nd];

            Aprint("RK4 from xn = %g\n",xn);
            Aprint("RK4 from x1p = %g\n",x1p);
            Aprint("RK4 from x2p = %g\n",x2p);
            Aprint("RK4 from x3p = %g\n",x3p);
            Aprint("RK4 from k4p = %g\n",k4p);

            *value =
                -1.0 / 3.0 * xn +
                1.0 / 3.0 * (
                    x1p +
                    2.0 * x2p +
                    1.0 * x3p +
                    k4p
                    );

            /* reconstruct the forward-Euler equivalent derivative */
            d[nd] = Is_zero(dt) ? 0.0 : ((*value - xn)/(2.0*dt));

            Aprint("RK4(k4) post %g >\n",*value);
            Aprint("KK4 %g\n",k4p*2.0);
        }
    }
    else if(stardata->preferences->solver == SOLVER_PREDICTOR_CORRECTOR)
    {
        /*
         * Predictor-corrector method
         */
        if(stardata->stardata_stack==NULL||
           stardata->n_stardata_stack<1)
        {
            /*
             * Set up memory to save previous
             */
            stardata->stardata_stack=Malloc(sizeof(struct stardata_t*)*1);
            stardata->stardata_stack[0] = new_stardata(stardata->preferences);
            stardata->n_stardata_stack = 1;
        }

        if(stardata->model.solver_step == 0)
        {
            /*
             * Initial (forward Euler) step at time t based
             * on derivatives at time t, stored in x1p.
             */
            x1p = d[nd] * dt;
            *value = xn + x1p;
        }
        else
        {
            /*
             * Subsequent steps at time t+dt, these
             * are the CE steps.
             */
            *value = xn +
                0.5 * (
                    x1p +
                    d[nd] * dt
                    );
        }

    }
    else
    {
        Exit_binary_c(BINARY_C_ALGORITHM_OUT_OF_RANGE,
                      "Unknown solver scheme %d\n",
                      stardata->preferences->solver);
    }
    Aprint("at t+dt = %30.15e\n",*value);

    /*
     * If a check function is defined and the derivative
     * is non-zero, call the check function.
     */
    if(
        checkfunc != NULL
        &&
        Is_really_not_zero(d[nd])
        )
    {
        if(checkfunc(stardata,
                     data,
                     value_in,
                     *value,
                     d[nd],
                     dt,
                     nd,
                     derivative_group) == FALSE)
        {
            /*
             * check failed : restore value and return
             * BINARY_C_APPLY_DERIVATIVE_FAILED
             *
             * Note: not restoring the value is
             * pointless. The value passed in is
             * at least physical: the incorrect value
             * that caused the failure is likely unphysical.
             */
            if(wanted)
            {
                *wanted = *value;
            }
            *value = value_in;
            ret = BINARY_C_APPLY_DERIVATIVE_FAILED;
        }
        else
        {
            /*
             * Check went well :
             * return BINARY_C_APPLY_DERIVATIVE_SUCCESS
             */
            ret = BINARY_C_APPLY_DERIVATIVE_SUCCESS;
        }
    }
    else
    {
        /*
         * Derivative is zero: must always succeed
         */
        ret = BINARY_C_APPLY_DERIVATIVE_SUCCESS;
    }

    return ret;
}
