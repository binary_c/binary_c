#include "binary_c.h"

void check_nans_are_signalled(void)
{
#ifdef CHECK_ISNAN_WORKS
    /*
     * Some algorithms in binary_c require NaN to be 
     * correctly identified. So, check that isnan works.
     *
     * GCC 8 requires -fsignaling-nans if -ffast-math is used.
     *
     * Clang 5 does not require (in fact, does not support) -fsignaling-nans
     * with -ffast-math to detect nans.
     *
     * On failure, exits with an error message to stdout and stderr.
     */

    double nan;
#ifdef NAN
    /*
     * gcc provides NAN which is always not-a-number
     */ 
    nan = NAN;
#else
    nan = 0.0 / 0.0;
#endif
    
    if(isnan(nan)!=0)
    {
        /*
         * NaN is detected correctly
         */
    }
    else
    {
        /*
         * NaN is not detected correctly.
         */
#define _ERRSTRING "Not-a-number (NaN or nan) is not detected as NaN. This means some algorithms will fail. Please build with -fsignaling-nans enabled. (Note that \"-ffast-math\" disables \"-fsignaling-nans\" by default, and this is not what you want, try making sure that \"-fsignaling-nans\" appears after \"-ffast-math\" in the Makefile. You should probably also enable -fno-finite-math-only.)\n"
        
        fprintf(stderr,_ERRSTRING);
        fprintf(stdout,_ERRSTRING);
        Backtrace;
        Exit_binary_c_no_stardata(BINARY_C_ISNAN_FAILED,
                                  _ERRSTRING);
    }
#endif // CHECK_ISNAN_WORKS
}
