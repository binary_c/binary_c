#include "../binary_c.h"
No_empty_translation_unit_warning;


double max_from_list(int n,...)
{
    /*
     * given a list of doubles return the maximum value
     */
    va_list args;
    va_start(args,n);
    double val,maxval = va_arg(args,double);
    int i;
    for(i=1;i<n;i++)
    {
        val = va_arg(args,double);
        maxval=Max(val,maxval);
    }
    va_end(args);
    return maxval;
}
