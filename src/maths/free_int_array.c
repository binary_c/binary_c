#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Free an int array
 */
void free_int_array(struct int_array_t ** Restrict int_array)
{
    if(int_array != NULL &&
       *int_array != NULL)
    {
        Safe_free((*int_array)->ints);
        Safe_free_nocheck(*int_array);
    }
}
