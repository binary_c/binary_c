#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Make floating point exception bitstring,
 * useful for debugging
 */
char * floating_point_exception_string(void)
{
    unsigned int n = 0;
#ifdef FE_INEXACT
    n++;
#endif
#ifdef FE_DIVBYZERO
    n++;
#endif
#ifdef FE_UNDERFLOW
    n++;
#endif
#ifdef FE_OVERFLOW
    n++;
#endif
#ifdef FE_INVALID
    n++;
#endif
    char * const s = Malloc(sizeof(char)*(n+1));
    n=0;
#undef _TEST
#define _TEST(X) s[n++] = (fetestexcept(X) ? '!' : '.')
#ifdef FE_INEXACT
    _TEST(FE_INEXACT);
#endif
#ifdef FE_DIVBYZERO
    _TEST(FE_DIVBYZERO);
#endif
#ifdef FE_UNDERFLOW
    _TEST(FE_UNDERFLOW);
#endif
#ifdef FE_OVERFLOW
    _TEST(FE_OVERFLOW);
#endif
#ifdef FE_INVALID
    _TEST(FE_INVALID);
#endif
    s[n] = '\0';
    return s;
}
