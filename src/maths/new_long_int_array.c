#include "../binary_c.h"
No_empty_translation_unit_warning;

struct long_int_array_t * new_long_int_array(const ssize_t n)
{
    /*
     * Make and return a new long_int array
     */
    struct long_int_array_t * s = Malloc(sizeof(struct long_int_array_t));
    s->n = n;
    if(n<=0)
    {
        s->long_ints = NULL;
    }
    else
    {
        s->long_ints = Malloc(sizeof(long int) * n);
    }
    return s;
}
