#include "../binary_c.h"
No_empty_translation_unit_warning;

#include <gsl/gsl_linalg.h>

double kaps_rentrop_GSL(struct stardata_t * Restrict const stardata,
                        const double h, // timestep
                        double * Restrict const y, // number densities
                        double * Restrict const y2, // dummy array
                        const unsigned int n, // number of isotopes
                        double * const * const Restrict J, // Jacobian (pre-allocated, modified)
                        const double * const Restrict sigmav, // reaction rates
                        const double * const N, // initial number densities of all isotopes
                        void (*jacobian)(double *const* const J,
                                         const double * const N,
                                         const double * const Restrict sigmav),
                        void (*derivatives)(const double * const y,
                                            double *dydt,
                                            const double * const Restrict sigmav,
                                            const double * const N),
                        double * const Restrict f1,
                        double * const Restrict f2,
                        double * const Restrict f3,
                        double * const Restrict f4,
                        const double error_threshold,
                        const Boolean solution_must_be_positive,
                        int * const Restrict indx Maybe_unused)
{
    /*
     * Kaps Rentrop integration method for nuclear burning:
     *
     * Variables in/out:
     *
     * h : constant timestep
     * y : number densities of isotopes which burn, n of them (size n+1)
     * y2 : dummy array, pre-allocated, size n+1
     * n : number of isotopes being burned
     * J : Jacobian, n*n matrix, pre-allocated (set (once) in this function)
     * sigmav : Reaction rate array (pre-allocated, pre-defined, constant)
     * N : all isotopic abundances (see nucsyn_isotopes.h)
     * jacobian : function pointer to the Jacobian calculator
     * derivatives : function pointer to the derivatives calculator
     * f1,f2,f3,f4 : pre-defined arrays of size n+1, used in integration steps
     * indx : pre-defined array of size n+1, used by numerical recipes routines
     *
     * NOTE : All the arrays passed into this function are pre-defined, or defined
     * in the tmpstore, which
     * means it avoids (slow) calls to malloc/calloc.
     *
     * However, this means that in the Jacobian function all
     * the zero elements must be explicitly
     * declared. Usually this is not a problem, and perhaps if it is a problem
     * for you you can use a memset(J,...) to clear J before you use it.
     *
     * It's not clear which is the fastest way to clear J... (depends
     * on n?)
     */

    /* parameters from Shampine (1982, ACM transactions on mathematical
     * software, vol. 8, pp 93-113) as chosen by Press et al. 1996
     */
#define A21 (2.0)
#define A31 (48.0/25.0)
#define A32 (6.0/25.0)
#define C21 (-8.0)
#define C31 (372.0/25.0)
#define C32 (12.0/5.0)
#define C41 (-112.0/125.0)
#define C42 (-54.0/125.0)
#define C43 (-2.0/5.0)
#define B1  (19.0/9.0)
#define B2  (1.0/2.0)
#define B3  (25.0/108.0)
#define B4  (125.0/108.0)
#define E1  (17.0/54.0)
#define E2  (7.0/36.0)
#define E3  (0.0)
#define E4  (125.0/108.0)

#define KAPS_RENTROP_GAMMA 0.5

#define Copy_array(A,B) memcpy((B),(A),nnd);

    /* A[i] += B[i] * C */
#define Add_multiple_of_array(A,B,C)            \
    {                                           \
        const double cc=(C);                    \
        for(unsigned int i=1;i<nn;i++)          \
        {                                       \
            ((A)[i]) += (((B)[i])*cc);          \
        }                                       \
    }

    /* A[i] += B[i] * C + D[i] * E */
#define Add_multiple_of_array2(A,B,C,D,E)           \
    {                                               \
        const double cc=(C),ee=(E);                 \
        for(unsigned int i=1;i<nn;i++)              \
        {                                           \
            ((A)[i]) += (((B)[i])*cc+((D)[i])*ee);  \
        }                                           \
    }

    /* A[i] += B[i] * C + D[i] * E + F[i] * G */
#define Add_multiple_of_array3(A,B,C,D,E,F,G)                   \
    {                                                           \
        const double cc=(C),ee=(E),gg=(G);                      \
        for(unsigned int i=1; i<nn; i++)                        \
        {                                                       \
            ((A)[i]) += (((B)[i])*cc+((D)[i])*ee+((F)[i])*gg);  \
        }                                                       \
    }


    double d; // dummy variable
    const double ih = 1.0 / h; // saves multiple calculations of 1/h

    // used in matrix calculations
    const unsigned int nn = n + 1; // size of arrays
    const size_t nnd = nn * sizeof(double);

#define m stardata->tmpstore->kaps_rentrop_GSL_m
#define p stardata->tmpstore->kaps_rentrop_GSL_p

    if(n > stardata->tmpstore->kaps_rentrop_GSL_n)
    {
        /*
         * Need more space
         */
        if(m != NULL) gsl_matrix_free(m);
        if(p != NULL) gsl_permutation_free(p);
        m = gsl_matrix_alloc(n,n);
        p = gsl_permutation_alloc(n);
        stardata->tmpstore->kaps_rentrop_GSL_n = n;
    }
    else
    {
        /*
         * Use only space n, even if more is allocated
         */
        m->size1 = n;
        m->size2 = n;
        p->size = n;
    }

    /*
     * Space for LU decompositions. This is used in
     * kaps_rentrop_LU_backsub.c
     */
#define gsl_b stardata->tmpstore->kaps_rentrop_LU_backsub_b
#define gsl_x stardata->tmpstore->kaps_rentrop_LU_backsub_x
#define gsl_n stardata->tmpstore->kaps_rentrop_LU_backsub_n

    if(n > stardata->tmpstore->kaps_rentrop_LU_backsub_n)
    {
        /*
         * Increase the size of the vectors
         */
        Safe_free_GSL_vector(gsl_b);
        Safe_free_GSL_vector(gsl_x);
        gsl_b = New_GSL_vector(n);
        gsl_x = New_GSL_vector(n);
        gsl_n = n;
    }
    else
    {
        /*
         * Set the size of the vectors: the ->size
         * variables are used internally by the GSL
         * LU routines.
         */
        gsl_b->size = n;
        gsl_x->size = n;
    }

#ifdef ALLOC_CHECKS
    if(m==NULL || p==NULL)
    {
        Exit_binary_c_no_stardata(
            BINARY_C_ALLOC_FAILED,
            "Failed to allocate memory for GSL Kaps Rentrop\n");
    }
#endif

    /* calculate Jacobian, J */
    jacobian(J,N,sigmav);

    /* calculate I/(gamma*h)-J */
    d = ih / KAPS_RENTROP_GAMMA;

    for(unsigned int i=1;i<nn;i++)
    {
        for(unsigned int j=1;j<nn;j++)
        {
            J[i][j] = -J[i][j];
        }
        J[i][i] += d;
    }

#ifdef NANCHECKS
    /* nan checks */
    for(unsigned int k=1;k<nn;k++)
    {
        for(unsigned int j=1;j<nn;j++)
        {
            if(isnan(J[k][j]))
            {
                _printf("pre LU (pure J) J[%u][%u]=%g\n",k,j,J[k][j]);
                Exit_binary_c_no_stardata(BINARY_C_EXIT_NAN,
                                          "Kaps-Rentrop exit, Jacobian(%u,%u) = %g",
                                          k,
                                          j,
                                          J[k][j]);
            }
        }
    }
#endif // NANCHECKS

    /*
     * Make LU decomposition of Jacobian
     * (modifies J)
     */
    kaps_rentrop_LU_decomp(J,nn,m,p);

    /* Setup of the first term of the LHS is now complete */

    /* alias f1..4 as delta1..4 to make the code clearer */
#define delta1 f1
#define delta2 f2
#define delta3 f3
#define delta4 f4

    /*
     * Note that the way this is done, the f1,f2,f3,f4 will eventually
     * store the vectors delta1,delta2,delta3,delta4, but I have used
     * some macros to make the code much easier to read
     */

    /* DELTA1 */
    derivatives(y,f1,sigmav,N); // calculate derivatives at y
    kaps_rentrop_LU_backsub(stardata,J,f1,nn,m,p); // solve for delta1, given f1

    /************************************************************/
    /* DELTA2 */
    Copy_array(y,y2); // make copy of y in y2

    // calculate derivatives at y2=y+A21*delta1, put them in f2
    Add_multiple_of_array(y2,delta1,A21);
    derivatives(y2,f2,sigmav,N);

    // add (C21/h)*delta1
    Add_multiple_of_array(f2,delta1,ih*C21);
    kaps_rentrop_LU_backsub(stardata,J,f2,nn,m,p);// solve for delta2

    /************************************************************/
    /* DELTA3 */
    Copy_array(y,y2); // reset y2 from original y

    // calculate derivatives at y2=y+A31*delta1+A32*delta2, put them in f3
    Add_multiple_of_array2(y2,delta1,A31,delta2,A32);
    derivatives(y2,f3,sigmav,N);
    Copy_array(f3,f4); // save for delta4 calculation

    // add (C31/h)*delta1 + (C32/h)*delta2
    Add_multiple_of_array2(f3,delta1,C31*ih,delta2,C32*ih);
    kaps_rentrop_LU_backsub(stardata,J,f3,nn,m,p);// solve for delta3

    /************************************************************/
    /* DELTA4 */

    // derivatives were stored (f4) in delta2 calculation
    // add (C41/h)*delta1 + (C42/h)*delta2 + (C43/h)*delta3
    Add_multiple_of_array3(f4,delta1,C41*ih,delta2,C42*ih,delta3,C43*ih);
    kaps_rentrop_LU_backsub(stardata,J,f4,nn,m,p);// solve for delta4

    /************************************************************/

    /*
     * find the maximum value of the abundances,
     * use this to scale the errors
     */
    double ymax = y[1];
    if(nn>1)
    {
        for(unsigned int i=2; i<nn; i++)
        {
            ymax = Max(y[i],ymax);
        }
    }

    /*
     * estimate contribution to the error if y[i]>ymin
     */
    double errmax = 0.0;
    const double ymin_for_error = ymax * 1e-3;
    for(unsigned int i=1; i<nn; i++)
    {
        const Boolean use = y[i] > ymin_for_error;
        const double err = use
            ? fabs( ( E1*f1[i] + E2*f2[i] + E3*f3[i] + E4*f4[i] ) / y[i] )
            : 0.0;
        errmax = Max(errmax,err);
        //printf("ERR %u : y = %g y/ymax = %g use? %s -> err %g -> errmax %g (threshold %g)\n",
        //i,y[i],y[i]/ymax,Yesno(use),err,errmax,error_threshold);
    }

    /************************************************************/
    /* if the error is < threshold, construct the solution */
    if(likely(errmax < error_threshold))
    {
        for(unsigned int i=1;i<nn;i++)
        {
            y[i]+=f1[i]*B1+f2[i]*B2+f3[i]*B3+f4[i]*B4;
        }

        /*
         * Force the solution to be positive if we're
         * told to (probably we are)
         */
        if(likely(solution_must_be_positive == SOLUTION_MUST_BE_POSITIVE))
        {
            for(unsigned int i=1;i<nn;i++)
            {
                y[i] = Max(0.0,y[i]);
            }
        }
    }


    /*
     * return the error relative to the threshold, for
     * automatic timestep manipulation
     */
    return errmax / error_threshold;
}
#undef A21
#undef A31
#undef A32
#undef C21
#undef C31
#undef C32
#undef C41
#undef C42
#undef C43
#undef B1
#undef B2
#undef B3
#undef B4
#undef E1
#undef E2
#undef E3
#undef E4
#undef Copy_array
#undef Add_multiple_of_array
#undef Add_multiple_of_array2
#undef Add_multiple_of_array3
