#include "../binary_c.h"
No_empty_translation_unit_warning;

double Pure_function bin_data(const double x,
                              const double bin_width)
{
    /*
     * bin data : copysign returns the sign of x
     * (standard math library func)
     *
     * See also bin_data_sigfigs
     */
    const double sign = copysign(1.0,x);

    /*
     * Bin to the nearest bin_width, note
     * the extra 0.5*sign puts the binned value in
     * the middle of the bin (deliberately!)
     */
    return (
        (
            (int)(x/bin_width)
            +
            0.5*sign
            )*bin_width
        );
}
