 /*
 * Generic bisector : combines the (Brent)
 * interpolator with an initial bracketing
 * procedure and some function checking.
 *
 *
 * n is a number defining the bisection: this
 * is defined in bisect_parameters.def (see the section
 * "Debugging numbers for generic_bisect) and is usually
 * used for debugging.
 *
 * monochecks, if set to BISECT_USE_MONOCHECKS or
 * BISECT_USE_MONOCHECKS_FATAL, and if BISECT_DO_MONOCHECKS
 * is defined (see bisect_parameters.def), activates checks on the
 * range and required positive monotonous behaviour of the
 * passed in function. To turn this off, set this parameter
 * to BISECT_NO_MONOCHECKS.
 *
 * If monochecks is the BISECT_USE_MONOCHECKS_FATAL
 * option, then we exit binary_c on failure.
 *
 * The function 'func' should be monotonously increasing.
 * If it is not, and monochecks is FALSE, you never know
 * what will go wrong. If in doubt, check your function!
 *
 * guess is the initial guess
 * min and max are the range of guesses
 * tol is the tolerance (return if this is matched)
 * alpha is the convergence parameter :
 * if alpha=1 : return the converged parameter,
 * if alpha=0 : return the initial guess,
 * 1<alpha<0  : interpolate in between the initial guess
 *              and converged parameter. This will
 *              require multiple calls to get the answer.
 *
 * itmax is the maximum number of iterations
 *
 * If uselog is TRUE then use log(x) instead of (x) when
 * bisecting. This may, or may not, be quicker.
 *
 * Returns the converged value with *error == BINARY_C_BISECT_ERROR_NONE
 * on success.
 *
 * On failure, sets *error and returns BISECT_RETURN_FAIL,
 * although the return value is not useful in this case.
 *
 * If BISECT_FAIL_EXIT is defined, exits on error.
 * This is not ideal when running a population of stars: you
 * want to check *error and act accordingly.
 *
 * Note that this routine will fail on functions that
 * return very large (or small, presumably) numbers.
 * To avoid this, instead of returning, say, x - X,
 * make your function return 1.0 - x/X which is nearer to 1.0.
 */

#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "generic_bisect.h"

double generic_bisect(int * Restrict error,
                      const int monochecks,
                      int n,
                      brent_function func,
                      double guess,
                      double min,
                      double max,
                      const double tol,
                      const int itmax,
                      const Boolean uselog,
                      const double alpha,
                      ...
    )
{
    /*
     * If uselog, then adjust ranges.
     *
     * Allow min to be zero by setting it to REALLY_TINY.
     */
    if(uselog == TRUE)
    {
        if(min<-TINY || max<-TINY)
        {
            Exit_binary_c_no_stardata(
                BINARY_C_OUT_OF_RANGE,
                "if using logarithms in generic_bisect, the minimum cannot be <=0\n");
        }
        min = log10(Max(REALLY_TINY,min));
        max = log10(Max(REALLY_TINY,max));
        guess = log10(guess);
    }
    Boolean vb = FALSE;
    *error = BINARY_C_BISECT_ERROR_NONE;
    va_list args_main,args;
    double top,bottom;
    double ret = -1.0;
    double x = guess;
    const double guessin = guess;

    /* set up GSL args */
    struct GSL_args_t * GSL_args = Malloc(sizeof(struct GSL_args_t));

    if(unlikely(n<0))
    {
        /*
         * Called with a negative identifier: this means
         * we should use the equivalent positive identifier
         * and turn verbosity on.
         */
        vb = TRUE;
        n = -n;
    }

    //if(n==BISECTOR_DISC_BISECTION_ROOTER) vb = TRUE;

    /*
     * call va_start to get args_main
     *
     * then we always copy this to access the args passed in
     */
    va_start(args_main,alpha);

    VBPRINT("generic_bisect: guess=%g min=%g max=%g tol=%g itmax=%d func=%p alpha=%g\n",
            guess,min,max,tol,itmax,
            Cast_function_pointer(func),
            alpha);

    /*
     * If min == max, there's no point in doing anything.
     * We should just return min (or max).
     */
    if(unlikely(Is_really_zero(min-max)))
    {
        VBPRINT("generic_bisect: min==max=%g, just returning this\n",min);
        Clean_return(min);
    }


#ifdef BISECT_STATS
    static THREAD_LOCAL int count[2] = {0,0};
#endif

#ifdef BISECT_STATS
#define POSTFUNC count[n]++;
#else
#define POSTFUNC
#endif

#ifdef BISECT_NANS
#define CALLFUNCNANCHECK(X)                     \
    if(unlikely(isnan(X)))                      \
    {                                           \
        Backtrace;                              \
        Exit_binary_c_no_stardata(              \
            BINARY_C_EXIT_NAN,                           \
            "NAN failure (bisector %d %s)",     \
            n,                                  \
            Bisector_string(n)                  \
            );                                  \
    }
#else
#define CALLFUNCNANCHECK(X)
#endif



#ifdef BISECT_DO_MONOCHECKS
    if(unlikely(monochecks != BISECT_NO_MONOCHECKS))
    {
        /*
         * Check function is monotonic, i.e. slope is positive.
         *
         * This is done at min + fmin and fmax * the range,
         * which is usually at 0% and 100%.
         *
         * We calculate 'diff', the difference between the function
         * evalulated at fmin and fmax, and 'sign' which is the
         * sign of the function.
         *
         * If diff is zero, or sign is not +1, return an error.
         */
        const double range = max - min;
        const double eps = 1e-6;
        const double fmin = eps;
        const double fmax = 1.0 - eps;
        const double xmin = min;
        const double xmax = max;
        int GSL_error_min, GSL_error_max;

        /*
         * monotonous checks
         *
         * xmin should be < xmax, but
         * there may be no solution right
         * on the edge of the range, so try
         * slightly inside as well.
         */
        CALLFUNC(xmin);
        if(unlikely(GSL_args->error))
        {
            VBPRINT("xmin gave error : trying just above\n");
            CALLFUNC(xmin + range*eps);
            if(GSL_args->error)
            {
                GSL_error_min = 1;
                VBPRINT("This failed too\n");
            }
            else
            {
                VBPRINT("This worked\n");
                GSL_error_min = 0;
            }
        }
        else
        {
            GSL_error_min = 0;
        }
        const double retmin = ret;

        CALLFUNC(xmax);
        if(unlikely(GSL_args->error))
        {
            VBPRINT("xmax gave error : trying just below\n");
            CALLFUNC(xmax - range*eps);
            if(GSL_args->error)
            {
                GSL_error_max = 1;
                VBPRINT("This failed too\n");
            }
            else
            {
                VBPRINT("This worked\n");
                GSL_error_max = 0;
            }
        }
        else
        {
            GSL_error_max = 0;
        }

        const double retmax = ret;
        const double diff = retmax - retmin;
        double sign = Sign_is_really(diff);
        VBPRINT("Monocheck: GSL_error? min %d max %d : f(xmin = %g) = %g, f(xmax = %g) = high %g : diff = %g : sign %g\n",
                GSL_error_min,
                GSL_error_max,
                xmin,
                retmin,
                xmax,
                retmax,
                diff,
                sign);

        /*
         * Set error if required
         */
        *error =
            Is_really_zero(diff) ? BINARY_C_BISECT_ERROR_ALL_ZERO :
            (sign < 0.0) ? BINARY_C_BISECT_ERROR_NON_MONOTONICALLY_INCREASING :
            GSL_error_min ? BINARY_C_BISECT_ERROR_FUNCTION_FAILED :
            GSL_error_max ? BINARY_C_BISECT_ERROR_FUNCTION_FAILED :
            *error;

        if(unlikely(*error))
        {
            //int show = 0;
            if(*error == BINARY_C_BISECT_ERROR_ALL_ZERO)
            {
                //show = 1;
                VBPRINT("WARNING : evalulations at the minimum and maximum of the bisector function (%d \"%s\") range are both zero. This function cannot be bisected.\n",
                        n,
                        Bisector_string(n));
            }
            else if(*error == BINARY_C_BISECT_ERROR_NON_MONOTONICALLY_INCREASING)
            {
                //show = 1;
                VBPRINT("WARNING : evalulations at the minimum (%g) and maximum (%g) of the bisector function (%d \"%s\") give the wrong sign (sign = %g). Please fix the function so it is montonically increasing with its parameter.\n",
                        min,
                        max,
                        n,
                        Bisector_string(n),
                        sign);
                Backtrace;
                Exit_binary_c_no_stardata(
                    BINARY_C_BISECT_ERROR,
                    "generic_bisect: evalulations at the minimum (%g) and maximum (%g) of the bisector function (%d \"%s\") give the wrong sign (sign = %g). Please fix the function so it is montonically increasing with its parameter.\n",
                    min,
                    max,
                    n,
                    Bisector_string(n),
                    sign);
            }

            //if(0 && show)
            if(0)
            {
                /*
                 * Try at the floating point range
                 */
                double lx,prevsign=-2,prevchange=DBL_MIN;
                int nn = (int)(log10(DBL_MAX));
                for(lx=log10(DBL_MIN);
                    lx<log10(DBL_MAX);
                    lx += (log10(DBL_MAX) - log10(DBL_MIN))/(1.0*(double)nn))
                {
                    double xx = exp10(lx);
                    CALLFUNC(xx);

                    sign = Sign_is_really(ret);
                    if(unlikely(Fequal(prevsign,-2.0))) prevsign=sign;
                    if((!Fequal(sign,prevsign) &&
                        !Fequal(prevchange,xx)))
                    {
                        printf(
                            "Range %g to %g has sign %+g\n",
                            prevchange,
                            xx,
                            prevsign
                            );
                        prevsign = sign;
                        prevchange = xx;
                    }
                }
                printf(
                    "Range %g to %g has sign %+g\n",
                    prevchange,
                    DBL_MAX,
                    prevsign
                    );

                CALLFUNC(xmax);
                printf("at xmax = %g -> ret = %g (error %d)\n",xmax,ret,GSL_args->error);
                CALLFUNC(xmin);
                printf("at xmin = %g -> ret = %g (error %d)\n",xmin,ret,GSL_args->error);

                Exit_binary_c_no_stardata(
                    0,
                    "Exit after testing bisection range");
            }

            if(unlikely(monochecks == BISECT_USE_MONOCHECKS_FATAL))
            {
                Cleanup_bisect;
                Exit_binary_c_no_stardata(BINARY_C_INTERPOLATION_ERROR,"Function %d \"%s\" into generic_bisect is poorly behaved: the difference between its values at range locations %g and %g (x values %g and %g) is %g (this must be non-zero), sign (which should be == +1) is %g (0.0 means diff the difference is zero)\n",
                                          n,
                                          Bisector_string(n),
                                          fmin,
                                          fmax,
                                          fmin * range,
                                          fmax * range,
                                          diff,
                                          sign);
            }
            else
            {
                Clean_return(BISECT_RETURN_FAIL);
            }
        }
    }
#endif // BISECT_DO_MONOCHECKS

    /*
     * The top bracket must have ret > 0
     * and the bottom must not.
     *
     * x is currently our root guess,
     * and if it is > 0 then it's a good top
     * bracket and we must find the bottom bracket.
     *
     * Otherwise, we must find a top bracket and
     * use 0.5 * x as the bottom bracket.
     */

    VBPRINT("first guess %g\n",guess);
    CALLFUNC(x);
    VBPRINT("first x=%g ret = %g\n",x,ret);

    if(ret < 0.0)
    {
        VBPRINT("hence guess = x = %g (which gave ret = %g) is the bottom bracket\n",
                x,
                ret);

        /*
         * loop to find top bracket
         */
        VBPRINT("topfind ret starts at x=%g ret=%g\n",x,ret);
        while(ret < 0.0 && *error == BINARY_C_BISECT_ERROR_NONE)
        {
            x *= (BISECT_INCREASE);

            /* panic if zero! (could happen) */
            if(Is_really_zero(x)) x+=TINY;

            /* limit to given range */
            x = Min(max,x);

            VBPRINT("topfind x = %g vs max = %g\n",x,max);

            if(Fequal(x,max))
            {
                CALLFUNC(x);
                if(ret > 0.0)
                {
                    VBPRINT("fixed on last call : ret = %g\n",ret);
                    break;
                }
                else
                {
                    VBPRINT("failed on last call\n");
                }
            }

            if(More_or_equal(x, max))
            {
                /*
                 * Bisection maxed out : issue a warning and perhaps exit
                 */
                VBPRINT("topfind maxed out\n");
#ifdef BISECT_FAIL_EXIT
                CALLFUNC(min);
                double fmin = ret;
                CALLFUNC(max);
                double fmax = ret;
                CALLFUNC(guess);
                double fguess=ret;
                CALLFUNC(0.99*guess);
                double fguess_low=ret;
                CALLFUNC(1.01*guess);
                double fguess_high = ret;
#endif

#if defined BISECT_SHOW_RANGE
                if(vb)
                {
                    double y,dy=(log10(max)-log10(min))/1000.0;
                    int iy=0;
                    for(y=log10(min);y<=log10(max);y+=dy)
                    {
                        double Y = exp10(y);
                        CALLFUNC(Y);
                        printf("%3d : At %g func = %g\n",
                               iy,Y,ret);
                    }
                    Binary_c_exit_no_stardata(0,
                                              "Exit after showing bisect range");
                }
#endif

                *error  = BINARY_C_BISECT_ERROR_MAXED_OUT;

#ifdef BISECT_FAIL_EXIT

                {
                    double y,dy=(log10(max)-log10(min))/100.0;
                    int iy=0;
                    for(y=log10(min);y<=log10(max);y+=dy)
                    {
                        double Y = exp10(y);
                        CALLFUNC(Y);
                        printf("%3d : At %g func = %g\n",
                               iy,Y,ret);
                    }
                }

                Exit_binary_c_no_stardata(
                    2,
                    "Bisection (caller %s, number %d) maxed out : x=%g min=%g f(min)=%g; max=%g f(max)=%g; f(guess= %g<%g<%g)=%g\n",
                    Bisector_string(n),
                    n,
                    x,
                    min,
                    fmin,
                    max,
                    fmax,
                    guess,
                    fguess_low,
                    fguess,
                    fguess_high
                    );
#endif
                /* if we haven't exited, return -1 */
                //x = max;
                Clean_return(BISECT_RETURN_FAIL);
            }
            else
            {
                CALLFUNC(x);
                VBPRINT("topfind f(%g) = %g (<0 ? %d [want >0])\n",x,ret,(int)(ret<0));
            }
        }

        VBPRINT("Exit topfind with x=%g (ret=%g should be >0)\n",x,ret);
        top = Min(max,x);
        bottom = Max(min,BISECT_DECREASE * x);
    }
    else
    {
        /*
         * guess is a good top bracket. We require
         * a bottom bracket.
         */
        VBPRINT("hence guess = x = %g (which gave ret %g) is the top bracket\n",
                x,
                ret);

        /* look to find bottom bracket */
        x = guess;

        VBPRINT("bottomfind 0 : ret starts at x=%g ret=%g\n",x,ret);
        Boolean ok = TRUE;
        while(ok == TRUE && ret > 0.0)
        {
            /*
             * Save prev x so we can check for dx == 0 (below)
             *
             * then decrease to find new x
             */
            double xprev = x;
            x = min + (BISECT_DECREASE)*(x-min);
            double dx = fabs(x - xprev);
            VBPRINT("bottomfind 1 : reduce x from %g to %g (min=%g, cf %g to %g : %d)\n",
                    xprev,
                    x,
                    min,
                    dx,
                    REALLY_TINY,
                    Is_really_zero(dx)
                );
            /*
             * If dx == 0, break the loop and fail
             */
            if(Is_really_zero(dx))
            {
                VBPRINT("Warning: x has not decreased within floating point uncertainty\n");
                ok = FALSE;
            }
            VBPRINT("bottomfind 1a : x now %g\n",x);
            CALLFUNC(x);
            VBPRINT("bottomfind 2 : f(%g) = ret = %g, ok = %d : loop? %d\n",x,ret,ok,
                    (ok == TRUE && ret > 0.0) ? 1 : 0
                );
        }

        if(ok == FALSE)
        {
            *error  = BINARY_C_BISECT_ERROR_MAXED_OUT;
            Clean_return(BISECT_RETURN_FAIL);
        }

        VBPRINT("bottomfind : done with x=%g (ret=%g should be <0)\n",x,ret);
        bottom = Max(min,x);
        top = guess;
    }

    if(unlikely(isnan(top) || isnan(bottom)))
    {
        fflush(stdout);
        Exit_binary_c_no_stardata(BINARY_C_EXIT_NAN,
                                  "NaN in top=%g bottom=%g\n",
                                  top,
                                  bottom);
    }
#ifdef BISECT_BRACKET_CHECKS
    double check_bottom,check_top;

    VBPRINT("bracket check: bottom x=%g\n",bottom);
    CALLFUNC(bottom);
    check_bottom = ret;
    VBPRINT("bracket check: bottom x=%g -> %g\n",bottom,ret);

    VBPRINT("bracket check: top x=%g\n",top);
    CALLFUNC(top);
    check_top = ret;
    VBPRINT("bracket check: top x=%g -> %g\n",top,ret);
    VBPRINT("Check brackets : f(bottom=%g)=%g(require <0) f(top=%g)=%g(require >0)",
            bottom,
            check_bottom,
            top,
            check_top
        );
    if(vb)
    {
        printf(" %s\n",
               ((check_bottom < 0.0 && check_top > 0.0) ? "ok" : "failed"));
    }

    if(unlikely(check_bottom > 0.0 || check_top < 0.0))
    {
#ifdef BISECT_FAIL_EXIT
        fflush(stdout);
        Exit_binary_c_no_stardata(BINARY_C_OUT_OF_RANGE,
                                  "bracket error (search %d, %s) bottom %g top %g\n",
                                  check_bottom,check_top);
#endif
        *error = BINARY_C_BISECT_ERROR_BRACKET_FAILED;
        Clean_return(BISECT_RETURN_FAIL);
    }


    VBPRINT("brackets %g %g\n",bottom,top);
#endif

    va_copy(args,args_main);
    va_start(args,alpha);
    VBPRINT("Call brent_GSL_valist itmax = %d, range [%g, %g], tol = %g\n",
            itmax,
            bottom,
            top,
            tol);

    ret = brent_GSL_valist(
        generic_bisect_funcwrapper,
        func,
        itmax,
        bottom,
        top,
        tol,
        uselog,
        args);

    va_end(args);

    VBPRINT("Brent bisect gave ret = %g \n",ret);

#ifdef BISECT_STATS
    _printf("Count %d = %d : in %g, out %g (min=%g max=%g bottom=%g top=%g)\n",count[n],guess,ret,min,max,bottom,top);
#endif

    if(0)printf("RET %g + %g = %g\n",alpha*ret, (1-alpha)*guessin, alpha * ret + (1.0 - alpha) * guessin);
    Clean_return(alpha * ret + (1.0 - alpha) * guessin);
}

static void Gnu_format_args(2,3) generic_bisect_vbprint(int n,
                                                        char * Restrict format,
                                                        ...)
{
    _printf("GBISECT: %d (%s) ",n,Bisector_string(n));
    va_list args;
    va_start(args,format);
    vprintf(format,args);
    va_end(args);
    fflush(stdout);
}


static double generic_bisect_funcwrapper(const double x,
                                         void * p)
{
    /*
     * Wrapper for generic bisect. Should return the
     * residual value as a double.
     *
     * p is a va_list of which the first two elements are
     *
     * Boolean uselog
     * *func (double,void*)
     *
     * and the rest should be send to func in void*
     */
    struct GSL_args_t * pcast = (struct GSL_args_t *)p;
    const double xx = pcast->uselog == TRUE ? exp10(x) : x;
    double y = pcast->func(xx,
                           pcast);
    return y;
}
