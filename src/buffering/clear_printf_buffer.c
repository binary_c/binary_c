#include "../binary_c.h"
No_empty_translation_unit_warning;


void clear_printf_buffer(struct stardata_t * Restrict const stardata)
{
    if(stardata->tmpstore != NULL)
    {
        if(stardata->tmpstore->raw_buffer_size>0)
        {
            fwrite(
                (char *)stardata->tmpstore->raw_buffer,
                (int)stardata->tmpstore->raw_buffer_size,
                1,
                stdout);
        }        
        Safe_free(stardata->tmpstore->raw_buffer);
        stardata->tmpstore->raw_buffer_size = 0;
        stardata->tmpstore->raw_buffer_alloced = 0;    
    }
}
