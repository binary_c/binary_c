#include "../binary_c.h"
No_empty_translation_unit_warning;


size_t deslash(char * const string)
{
    /*
     * convert _slash_ to / in string
     */
    char * match = strstr(string,"_slash_");
    const size_t s = strlen(string);
    while(match != NULL)
    {
        size_t i;
        match[0] = '/';
        for(i=1;i<s;i++)
        {
            match[i] = match[i+6];
            if(match[i] == '\0')
            {
                break;
            }
        }
        match = strstr(string,"_slash_");
    }
    return strlen(string);
}
