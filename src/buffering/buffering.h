#pragma once
#ifndef BUFFERING_H
#define BUFFERING_H

#include "buffering.def"

#undef X
#define X(CODE) INTERNAL_BUFFERING_##CODE,
enum { INTERNAL_BUFFERING_STATES_LIST };
#undef X



#endif // BUFFERING_H
