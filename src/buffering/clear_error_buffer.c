#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Dump the error buffer to stderr
 */
void clear_error_buffer(struct stardata_t * Restrict const stardata)
{
    if(stardata->tmpstore != NULL)
    {
        if(stardata->tmpstore->error_buffer != NULL) 
        {
            fprintf(stderr,
                    "%s\n",
                    (char *)stardata->tmpstore->error_buffer);
            Safe_free(stardata->tmpstore->error_buffer);
        }        
    }
}
