#pragma once
#ifndef BUFFERING_MACROS_H
#define BUFFERING_MACROS_H

/*
 * binary_c_buffered_printf error codes
 */
#define BUFFERED_PRINTF_NO_ERROR 0
#define BUFFERED_PRINTF_ASPRINTF_ERROR -1
#define BUFFERED_PRINTF_ALLOC_ERROR -2
#define BUFFERED_PRINTF_FULL_ERROR -3
#define BUFFERED_PRINTF_STARDATA_ERROR -4
#define BUFFERED_PRINTF_ERROR_BUFFER_ALLOC_ERROR -5
#define BUFFERED_PRINTF_GENERIC_ERROR -6

/*
 * Error string
 */
#define Buffered_printf_error_string(N)                                 \
    (                                                                   \
        (N) == BUFFERED_PRINTF_NO_ERROR ? "No error" :                  \
        (N) == BUFFERED_PRINTF_GENERIC_ERROR ? "generic problem" :      \
        (N) == BUFFERED_PRINTF_ASPRINTF_ERROR ? "asprintf failed" :     \
        (N) == BUFFERED_PRINTF_ALLOC_ERROR ? "alloc failed" :           \
        (N) == BUFFERED_PRINTF_FULL_ERROR ? "buffer is full" :          \
        (N) == BUFFERED_PRINTF_STARDATA_ERROR ? "stardata or stardata->tmpstore is NULL" : \
        "unknown"                                                       \
        )

/*
 * Macro to return true on error
 */
#define Buffered_printf_error(N) ((N)<0)


/*
 * Size of the next buffer increase
 */
#define Buffer_next ((size_t)(BUFFERED_PRINTF_INCREASE_RATIO * t->raw_buffer_alloced))

#endif//BUFFERING_MACROS_H
