#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef NUCSYN_LOG_JL
void log_jl(struct stardata_t * Restrict const stardata)
{

    const double dt = 1e6*(stardata->model.time-tprev);
    unsigned Stellar_type stellar_type = stardata->star[0].stellar_type;

    if(stardata->model.time<1e-10)
    {
        //first timestep: reset timer
        stardata->model.log_jl_tprev=0.0;
        // set stellar type to -1 to output initial abunds
        stellar_type=-1;
    }

    stardata->model.log_jl_tprev=stardata->model.time;

    printf("JL__ %d %g %g %g %g %g %g %g %g %g %g %g %g\n",
           stellar_type,
           stardata->model.time,
           dt,
           stardata->star[0].Xenv[XC12],
           stardata->star[0].Xenv[XO16],
           stardata->star[0].Xenv[XMg24],
           stardata->star[0].Xenv[XMg25],
           stardata->star[0].Xenv[XMg26],
           nucsyn_elemental_abundance("Ba",stardata->star[0].Xenv,stardata,stardata->store),
           nucsyn_elemental_abundance("Y",stardata->star[0].Xenv,stardata,stardata->store),
           nucsyn_elemental_abundance("Pb",stardata->star[0].Xenv,stardata,stardata->store),
           nucsyn_elemental_abundance("Nd",stardata->star[0].Xenv,stardata,stardata->store),
           nucsyn_elemental_abundance("Sn",stardata->star[0].Xenv,stardata,stardata->store)
        );
}
#endif // NUCSYN_LOG_JL
