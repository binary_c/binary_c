#pragma once

#ifndef ANSI_COLOURS_H
#define ANSI_COLOURS_H

/*
 * ANSI Colours
 *
 * Note that \x1B is the same as the escape
 * character, also written (in non-ISO, but works
 * in gcc and clang) \e
 */
#define ANSI_COLOURS_LIST            \
    X(          BLACK, "\x1B[0;30m") \
    X(            RED, "\x1B[0;31m") \
    X(          GREEN, "\x1B[0;32m") \
    X(         YELLOW, "\x1B[0;33m") \
    X(         ORANGE, "\x1B[0;33m") \
    X(           BLUE, "\x1B[0;34m") \
    X(        MAGENTA, "\x1B[0;35m") \
    X(           CYAN, "\x1B[0;36m") \
    X(          WHITE, "\x1B[0;37m") \
    X(           BOLD,  "\x1B[0;1m") \
    X(     BRIGHT_RED, "\x1B[1;31m") \
    X(   BRIGHT_GREEN, "\x1B[1;32m") \
    X(  BRIGHT_YELLOW, "\x1B[1;33m") \
    X(    BRIGHT_BLUE, "\x1B[1;34m") \
    X( BRIGHT_MAGENTA, "\x1B[1;35m") \
    X(    BRIGHT_CYAN, "\x1B[1;36m") \
    X(   BRIGHT_WHITE, "\x1B[1;37m") \
    X(    BRIGHT_BOLD,  "\x1B[1;1m") \
    X(      UNDERLINE,  "\x1B[0;4m") \
    X(   COLOUR_RESET,  "\x1B[0;0m")

#define DEBUG_COLOURS
#define FILE_LOG_COLOURS

#undef X
#define X(COLOUR,STRING) COLOUR,
enum { ANSI_COLOURS_LIST NUM_ANSI_COLOURS };
#undef X
#define X(COLOUR,STRING) #COLOUR,
static const char * const colour_strings[] = { ANSI_COLOURS_LIST };
#undef X

#endif // ANSI_COLOURS_H
