#include "../binary_c.h"
No_empty_translation_unit_warning;


void free_stellar_derivatives_array(double ** array)
{
    /*
     * Free 2D stellar derivatives array
     */
    Star_number n;
    Number_of_stars_Starloop(n)
    {
        Safe_free(array[n]);
    }
    Safe_free(array);
}
