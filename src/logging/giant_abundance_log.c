#include "../binary_c.h"
No_empty_translation_unit_warning;

#define ISOTOPES_TO_LOG XH1,XHe4,XC12,XC13,XN14,XN15,XO16,XO17,XNe20,XNe21,XNe22,XNa23,XMg24,XMg25,XMg26,XAl26,XAl27,XSi28,XSi29,XSi30
#define NUM_ISOTOPES_TO_LOG 20

// dt=1, initial m1,m2,sep=2-4, 5-8 L M R P
// H1=9 C12=11 N14=13 O16=15 Ne20=17 Ne22=19 Mg24=21 Mg25=22 Mg26=23

#ifdef NUCSYN_GIANT_ABUNDS_LOG
void giant_abundance_log(struct stardata_t * Restrict const stardata)
{
    double ratio_mg25_24=stardata->preferences->zero_age.XZAMS[XMg25]/stardata->preferences->zero_age.XZAMS[XMg24];
    double ratio_mg26_24=stardata->preferences->zero_age.XZAMS[XMg26]/stardata->preferences->zero_age.XZAMS[XMg24];
    Star_number i;
    Isotope isotopes[NUM_ISOTOPES_TO_LOG]={ISOTOPES_TO_LOG};
    Isotope k;

    if(stardata->model.time<0.001)
    {
        /* first call */
        stardata->model.giant_prevt=0.0;
    }
    stardata->model.giant_dt=stardata->model.time-stardata->model.giant_prevt;

    Starloop(i)
    {
        double r1=stardata->star[i].Xenv[XMg25]/stardata->star[i].Xenv[XMg24];
        double r2=stardata->star[i].Xenv[XMg26]/stardata->star[i].Xenv[XMg24];
        double f1=fabs(r1-ratio_mg25_24)/ratio_mg25_24;
        double f2=fabs(r2-ratio_mg26_24)/ratio_mg26_24;

        //printf("Ratios r1=%g vs %g -> f1=%g ; r2=%g vs %g -> f2=%g\n",r1,ratio_mg25_24,f1,r2,ratio_mg26_24,f2);
        if(
            (ON_MAIN_SEQUENCE(stardata->star[i].stellar_type))&&
            // either Mg25/Mg24 or Mg26/Mg24 must be 10% different to ZAMS
            ((f1 > 0.1)||(f2>0.1))
            )
        {
            /*
             * If the other star is a WD, NS or BH, this is an
             * extrinsic star
             */
            if(MASSIVE_REMNANT(stardata->star[Other_star(i)].stellar_type))
            {
                /*printf("EXTRINSIC%d f1=%g f2=%g mg24=%g vs %g, mg25=%g vs %g, mg26=%g vs %g",i,f1,f2,

                  stardata->star[i].Xenv[XMg24],stardata->preferences->zero_age.XZAMS[XMg24],
                  stardata->star[i].Xenv[XMg25],stardata->preferences->zero_age.XZAMS[XMg25],
                  stardata->star[i].Xenv[XMg26],stardata->preferences->zero_age.XZAMS[XMg26]
                  );
                */
                printf("EXTRINSIC ");
            }
            else
                /* Intrinsic */
            {
                printf("INTRINSIC ");
            }

            /* same output for all */
            printf("%g %g %g %g %g %g %g %g ",
                   stardata->model.giant_dt, // 1
                   stardata->common.zero_age.mass[i],
                   stardata->common.zero_age.mass[Other_star(i)],
                   stardata->common.zero_age.separation[0],
                   stardata->star[i].luminosity, // 5
                   stardata->star[i].mass,
                   stardata->star[i].radius,
                   stardata->common.orbit.period // 8
                );
            for(k=0;k<NUM_ISOTOPES_TO_LOG;k++)
            {
                printf("%g ",stardata->star[i].Xenv[isotopes[k]]);
            }
            printf("\n");
        }
    }
    stardata->model.giant_prevt=stardata->model.time;
}
#endif
