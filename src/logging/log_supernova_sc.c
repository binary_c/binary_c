#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef SUPERNOVA_COMPANION_LOG

void log_supernova_sc(struct stardata_t * Restrict stardata)
{
    /*
     * Second attempt to log supernovae for SCY
     */
    struct stardata_t *prev = stardata->previous_stardata;

    // check for post-SN disruption (or merger)
    Boolean disrupt=(((stardata->model.sgl==TRUE)&&
                      (prev->model.sgl==FALSE))||
                     (stardata->common.orbit.separation<TINY)||
                     (stardata->common.orbit.eccentricity<-TINY));

    if(0)
    {
        printf("Periods: pre=%g post=%g\n",
               prev->common.orbit.period*YEAR_LENGTH_IN_DAYS,
               stardata->common.orbit.period*YEAR_LENGTH_IN_DAYS);

        printf("Seps: pre=%g post=%g\n",
               prev->common.orbit.separation,
               stardata->common.orbit.separation);

        printf("Ecc: pre=%g post=%g\n",
               prev->common.orbit.eccentricity,
               stardata->common.orbit.eccentricity);

        printf("SGL: pre=%d post=%d\n",
               prev->model.sgl,
               stardata->model.sgl);
        printf("disrupt? %d\n",disrupt);
    }

    // first time: header (prefix l=log10)
    if(stardata->common.sn_sc_header==FALSE)
    {
        printf("#SNCOMP {prob sntype t bin} {pre: P a e} {post: P a e} {ZAMS: M1 M2 a P} {exploder: st M L Teff Mdot} {companion: st M L Teff Mdot}\n");
        stardata->common.sn_sc_header = TRUE;
    }

    int sntype=stardata->star[1].went_sn_last_time;

    printf("PROB %g\n",stardata->model.probability);

    printf("SNCOMP {%g %d %g %d} ",
           stardata->model.probability,
           sntype,
           prev->model.time,
           1-prev->model.sgl //binarity before explosion
        );

    /* orbit: pre explosion */
    if(prev->model.sgl==TRUE)
    {
        printf("{* * *} ");
    }
    else
    {
        printf("{%g %g %g} ",
               prev->common.orbit.period*YEAR_LENGTH_IN_DAYS,
               prev->common.orbit.separation,
               prev->common.orbit.eccentricity);
    }

    /* orbit: post explosion */
    if(disrupt==FALSE)
    {
        printf("{%g %g %g} ",
               stardata->common.orbit.period*YEAR_LENGTH_IN_DAYS,
               stardata->common.orbit.separation,
               stardata->common.orbit.eccentricity);
    }
    else
    {
        printf("{* * *} ");
    }

    /* ZAMS */
    printf("{%g %g %g %g} ",
           stardata->common.zero_age.mass[0],
           stardata->common.zero_age.mass[1],
           stardata->common.zero_age.separation[0],
           stardata->common.zero_age.orbital_period[0] /* days */);

    int k=stardata->star[0].went_sn_last_time;

    /* exploder */
    printf("{%d %g %g %g %g %g} ",
           prev->star[k].stellar_type,
           prev->star[k].mass,
           Outermost_core_mass(prev->star[k]),
           prev->star[k].luminosity,
           (1000.0*pow(1130.0*prev->star[k].luminosity/(Pow2(prev->star[k].radius)),0.25)),
           prev->star[k].mdot);

    /* companion */
    k=Other_star(k);
    printf("{%d %g %g %g %g %g} ",
           prev->star[k].stellar_type,
           prev->star[k].mass,
           Outermost_core_mass(prev->star[k]),
           prev->star[k].luminosity,
           (1000.0*pow(1130.0*prev->star[k].luminosity/(Pow2(prev->star[k].radius)),0.25)),
           prev->star[k].mdot);

    printf("\n");

    /* reset went_sn_last_time */
    stardata->star[0].went_sn_last_time=FALSE;
    stardata->star[1].went_sn_last_time=FALSE;
}
#endif// SUPERNOVA_COMPANION_LOG2
