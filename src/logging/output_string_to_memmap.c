#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MEMMAP
void output_string_to_memmap(struct stardata_t *stardata,
                             const char *c)
{

    /* function to output a string to the mmap'd region "biglog" */
    stardata->biglog_pos++;

    if(stardata->biglog_pos>BIGLOG_LINES-3)
    {
        Exit_binary_c(BINARY_C_EXCEED_BIGLOG_LINES,
                      "Just about to exceed BIGLOG_LINES (=%d), consider increasing the size of the biglog (currently defined by BIGLOG_SIZE (=%d), or actually %d)\n",
                      BIGLOG_LINES,
                      BIGLOG_SIZE,
                      sizeof(stardata->biglog)
            );
    }


    Dprint("add to memmap at %ld (max %d) \"%s\"\n",
           stardata->biglog_pos,
           BIGLOG_LINES,
           c);
#if (DEBUG==1)
    /* Use snprintf to avoid overflow */
    snprintf(stardata->biglog+stardata->biglog_pos*MEMMAP_STRING_LENGTH,
             MEMMAP_STRING_LENGTH,
             "%s @ %ld",
             c,
             stardata->biglog_pos);
#else
    /* Similarly use strlcpy to avoid overflow */
    strlcpy(stardata->biglog+stardata->biglog_pos*MEMMAP_STRING_LENGTH,
            c,
            MEMMAP_STRING_LENGTH);
#endif



}
#endif/* MEMMAP */
