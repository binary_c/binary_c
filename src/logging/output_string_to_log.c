#include "../binary_c.h"
No_empty_translation_unit_warning;


/* A function to output a string to the log file */

void output_string_to_log(struct stardata_t * Restrict const stardata,
                          const char * Restrict const c)
{
#ifdef FILE_LOG
    if(stardata->model.log_fp!=NULL)
    {
        fputs(c,stardata->model.log_fp);
#ifdef FILE_LOG_FLUSH
        fflush(stardata->model.log_fp);
#endif
    }
#endif /* FILE_LOG */

}
