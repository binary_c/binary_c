#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef UNIT_TESTS
#include "../binary_c_unit_tests.h"

/*
 * Unit test output for binary_c
 */

void unit_tests(struct stardata_t * const stardata)
{
    if(UNIT_TEST_FIRST_TIMESTEP_TEST)
    {
        Printf("%s ",UNIT_TEST_PREFIX);
        Printf(UNIT_TEST_HEADER_FORMAT,
               UNIT_TEST_HEADER);
        Printf("\n");
    }
    Printf("%s ",UNIT_TEST_PREFIX);
    Printf(UNIT_TEST_FORMAT,
           UNIT_TEST_DATA);
    Printf("\n");
}

#endif // UNIT_TESTS
