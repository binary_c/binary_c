#include "../binary_c.h"
No_empty_translation_unit_warning;

#include <stdarg.h>

Gnu_format_args(3,4)
void append_logstring(const Log_index N,
                      struct stardata_t * const stardata,
                      char * const format,
                      ...)
{
    /*
     * Append a string to the log
     */
    va_list args;
    va_start(args, format);
    char * const s = stardata->model.logflagstrings[N];
    const Boolean wasset = stardata->model.logflags[N];
    stardata->model.prevlogn = N;

    if(s==NULL)
    {
        set_logstring(N,stardata,format,args);
    }
    else
    {
        char cstring[STRING_LENGTH];
        size_t istring = strnlen(s,
                                 STRING_LENGTH-1);
        istring = Max((size_t)0,istring);

        /* insert separator prior to append, if we should/can */
        if(
            istring > 0 && //should
            istring < STRING_LENGTH-4 //can
            )
        {
            if(s[istring-1] == ' ')
            {
                /*
                 * Last char is a space : overwrite this also
                 */
                istring--;
            }

            /*
             * Insert separator at position istring
             */
            s[istring++] = ' ';
            s[istring++] = '-';
            s[istring++] = ' ';

            /* new NULL char at the end */
            s[istring] = '\0';
        }

        if(vsnprintf(cstring,
                     Max((size_t)0,STRING_LENGTH-1-istring),
                     format,
                     args) > 0)
        {
            strlcat(s,
                    cstring,
                    STRING_LENGTH-1);
            stardata->model.logflags[N] = TRUE;
        }
    }

    /* set next free stack item to N */
    if(wasset == FALSE)
    {
        unsigned int ilog = 0;
        while(ilog<LOG_NFLAGS)
        {
            if(stardata->model.logstack[ilog]==LOG_NONE)
            {
                if((ilog>0 &&
                    stardata->model.logstack[ilog-1] != (unsigned int)N) ||
                   ilog==0)
                {
                    stardata->model.logstack[ilog] = (unsigned int)N;
                    break;
                }
            }
            ilog++;
        }
    }

    va_end(args);
}
