#include "../binary_c.h"
No_empty_translation_unit_warning;


double ** make_stellar_derivatives_array(struct stardata_t * Restrict const stardata)
{
    /*
     * make 2D array with copies of the stellar deriviatives in it
     * (which requires freeing)
     */
    double ** const stellar_derivatives = Malloc(sizeof(double*) * NUMBER_OF_STARS);
    Star_number n;
    Number_of_stars_Starloop(n)
    {
        struct star_t * const star = & stardata->star[n];
        stellar_derivatives[n] = Calloc(DERIVATIVE_STELLAR_NUMBER,sizeof(double));
        memcpy(stellar_derivatives[n],
               star->derivative,
               sizeof(double) * DERIVATIVE_STELLAR_NUMBER);
        stellar_derivatives[n][DERIVATIVE_STELLAR_MASS] = Mdot_net(star);
        stellar_derivatives[n][DERIVATIVE_STELLAR_ANGMOM] = Jdot_net(star);
    }
    return stellar_derivatives;
}
