#include "../binary_c.h"
No_empty_translation_unit_warning;

double cs_truncation_fraction(struct stardata_t * const stardata Maybe_unused,
                              struct star_t * const donor)
{
    /*
     * Estimate truncation radius fraction of
     * the decretion disc around the donor.
     *
     * Note: 1/donor->q = accretor->q
     *
     * Zhang et al. (2004) ApJ 603, 663
     * Rimulo et al. (2018) MNRAS 476, 3555
     *
     *
     * Boyuan Liu's notes, after Eq. 1
     * https://www.overleaf.com/project/632c916bd7878c40e2dc9191
     */
    const double Ntrunc = 3.0;
    const double ftrunc =
        pow(Ntrunc, -2.0/3.0) *
        pow(1.0 + 1.0/donor->q, -1.0/3.0);
    return ftrunc;
}
