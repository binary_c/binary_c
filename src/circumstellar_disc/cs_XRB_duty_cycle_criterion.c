#include "../binary_c.h"
No_empty_translation_unit_warning;

Boolean cs_XRB_duty_cycle_criterion(struct stardata_t * const stardata,
                                    struct star_t * const donor,
                                    struct star_t * const accretor)
{
    /*
     * Test if the duty cycle is appropriate for an XRB.
     *
     * Boyuan Liu's notes section "Duty Cycle"
     * https://www.overleaf.com/project/632c916bd7878c40e2dc9191
     */

    const double Mdotacc = cs_peak_Be_accretion_rate(stardata, accretor);
    if(Is_really_zero(Mdotacc))
    {
        return FALSE;
    }
    else
    {
        const double Sigma0 = cs_Sigma0(stardata, accretor);
        const double Mdotej = 2.5e-11 * Sigma0 / 0.015;
        const double fde = 0.1;
        const int XRB_type = cs_Be_XRB_type(stardata,donor,accretor);
        const double fduty =
            XRB_type == Be_XRAY_TYPE_I ? 0.2 :
            XRB_type == Be_XRAY_TYPE_II ? 0.01 :
            1e100; // huge : cannot work if type is undefined

        Dprint("fduty %g vs %g * %g / %g = %g\n",
               fduty,
               fde,
               Mdotej,
               Mdotacc,
               fde * Mdotej / Mdotacc
            );

        return fduty < fde * Mdotej / Mdotacc;
    }
}
