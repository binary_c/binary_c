#include "../binary_c.h"
No_empty_translation_unit_warning;


void stellar_timescales_GB_parameters(
    struct BSE_data_t * Restrict const bse,
    const double mass, /* phase start mass */
    const double mp6 /* f(Z) = metallicity_parameters[6] */
    )
{
    /*
     * Set the GB parameters
     */
    bse->GB[GB_EFFECTIVE_AH] = exp10(
        Max(-4.8,
            Min(-5.7+0.8*mass,
                -4.1+0.14*mass)));
    bse->GB[GB_A_H_HE] = 1.27e-05;
    bse->GB[GB_A_HE] = 8.0e-05;
    bse->GB[GB_B] = Max(3.0e+04,
                   500.0 + 1.75e+04*pow(mass,0.6));

    if(mass < 2.5)
    {
        if(mass<2.0)
        {
            bse->GB[GB_D] = mp6;
            bse->GB[GB_p] = 6.0;
            bse->GB[GB_q] = 3.0;
        }
        else
        {
            double x = mass-2.0;
            x+=x;
            bse->GB[GB_p] = 6.0 - x;
            bse->GB[GB_q] = 3.0 - x;
            x *= mp6 - (0.975*mp6 - 0.45);
            bse->GB[GB_D] = mp6 - x;
        }
    }
    else
    {
        bse->GB[GB_D] = Max(-1.0,
                       0.5*mp6 - 0.06*mass);
        bse->GB[GB_D] = Max(bse->GB[GB_D],
                       0.975*mp6 - 0.18*mass);
        bse->GB[GB_p] = 5.0;
        bse->GB[GB_q] = 2.0;
    }

    bse->GB[GB_D] = exp10(bse->GB[GB_D]);

}
