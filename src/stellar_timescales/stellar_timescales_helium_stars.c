#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BSE
void stellar_timescales_helium_stars(struct stardata_t * const stardata,
                                     struct star_t * const star,
                                     struct BSE_data_t * const bse,
                                     const double mass,
                                     const double mt)
{
    /*
     * Helium stars
     */

    /*
     * Calculate Helium star Main Sequence lifetime.
     */
    bse->tm = themsf(mass);
    Dprint("Set Hestar MS lifetime %g from mass=%g\n",bse->tm,mass);

    /*
     * Zero- and terminal-age Helium-star main-sequence luminosity
     */
    bse->luminosities[L_ZAMS] = lzhef(mass);
    bse->luminosities[L_END_MS] = bse->luminosities[L_ZAMS]*(1.0+0.45+Max(0.0,(0.85-0.08*mass)));
    Dprint("Set He star ZAMS L from %g to %g\n",
           bse->luminosities[L_ZAMS],
           bse->luminosities[L_END_MS]);

    /*
     * Set the Helium star GB parameters
     */
    bse->GB[GB_A_HE] = 8.0e-05;
    bse->GB[GB_B] = 4.1e+04;
    bse->GB[GB_D] = 5.5e+04/(1.0+0.4*Pow4(mass));
    bse->GB[GB_p] = 5.0;
    bse->GB[GB_q] = 3.0;
    const double mp = 1.0-bse->GB[GB_p];
    const double mq = 1.0-bse->GB[GB_q];
    const double ip = -1.0/mp;
    const double ipq = (1.0/(bse->GB[GB_p]-bse->GB[GB_q]));
    const double invD = 1.0/bse->GB[GB_D];
    bse->GB[GB_Mx] = pow(bse->GB[GB_B]*invD,ipq);

    /*
     * Change in slope of giant L-Mc relation.
     */
    bse->luminosities[L_LMX] = bse->GB[GB_D]*pow(bse->GB[GB_Mx],bse->GB[GB_p]);

    /*
     * Set Helium star GB timescales
     */
    double mc1 = mcgbf(bse->luminosities[L_END_MS],
                       bse->GB,
                       bse->luminosities[L_LMX]);
    const double i_he = 1.0/bse->GB[GB_A_HE];
    const double y = ip*i_he*invD;
    bse->timescales[T_GIANT_TINF_1] = bse->tm + y*pow(mc1,mp);
    bse->timescales[T_GIANT_TX] = bse->timescales[T_GIANT_TINF_1] - (bse->timescales[T_GIANT_TINF_1] - bse->tm)*pow((bse->GB[GB_Mx]/mc1),mp);
    bse->timescales[T_GIANT_TINF_2] = bse->timescales[T_GIANT_TX] + (1.0/((bse->GB[GB_q]-1.0)*bse->GB[GB_A_HE]*bse->GB[GB_B]))*pow(bse->GB[GB_Mx],(1.0-bse->GB[GB_q]));

    /*
     * Get an idea of when Mc = Min(Mt,Mc,C,max) on the GB
     */
    const double mcmax = Min(
        Min(mt,
            1.45*mt-0.31),
        Max(chandrasekhar_mass(stardata,star),
            0.773*mass-0.35)
        );
    Dprint("mcmax %g\n",mcmax);
    if(Less_or_equal(mcmax,bse->GB[GB_Mx]))
    {
        bse->timescales[T_TMCMAX] = bse->timescales[T_GIANT_TINF_1] - y*pow(mcmax,mp);
    }
    else
    {
        const double y2 = -i_he/(mq*bse->GB[GB_B]);
        bse->timescales[T_TMCMAX] = bse->timescales[T_GIANT_TINF_2] - y2*pow(mcmax,mq);
    }

    bse->timescales[T_TMCMAX] = Max(bse->timescales[T_TMCMAX],bse->tm);
    bse->tn = bse->timescales[T_TMCMAX];

    Dprint("HE timescales age GB_A_HE %g, %g %g %g, ZAMS L %g %g\n",
           bse->GB[GB_A_HE],
           bse->timescales[T_GIANT_TINF_1],
           bse->timescales[T_GIANT_TINF_2],
           bse->timescales[T_GIANT_TX],
           bse->luminosities[L_ZAMS],
           bse->luminosities[L_END_MS]);
}

#endif//BSE
