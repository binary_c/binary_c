#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BSE
int stellar_timescales_post_HG(struct stardata_t * const stardata,
                               struct star_t * const star,
                               struct BSE_data_t * const bse,
                               const double * const giant_branch_parameters,
                               const double * const metallicity_parameters,
                               const double tbagb,
                               const double ip,
                               const double iq,
                               const double GBp,
                               const double GBq,
                               const double mp,
                               const double mq,
                               const double mcbagb,
                               const double phase_start_mass,
                               const double mass,
                               const Stellar_type stellar_type)
{
    double mc1,mc2,mcmax,lambda;
    Dprint("phase start mass %g\n",phase_start_mass);

    /*
     * Set the core mass at the BGB.
     */
    Dprint("Set BGB core mass\n");
#ifndef USB_BSE_TIMESCALES_H
    if(Less_or_equal(phase_start_mass,metallicity_parameters[ZPAR_MASS_HE_FLASH]))
    {
        bse->GB[GB_MC_BGB] = mcgbf(bse->luminosities[L_BGB],
                                   bse->GB,
                                   bse->luminosities[L_LMX]);

    }
    else if(Less_or_equal(phase_start_mass,metallicity_parameters[ZPAR_MASS_FGB]))
    {
        bse->GB[GB_MC_BGB] = mcheif(phase_start_mass,
                                    metallicity_parameters[ZPAR_MASS_HE_FLASH],
                                    metallicity_parameters[9],
                                    giant_branch_parameters);
    }
    else
    {
        bse->GB[GB_MC_BGB] = mcheif(phase_start_mass,
                                    metallicity_parameters[ZPAR_MASS_HE_FLASH],
                                    metallicity_parameters[10],
                                    giant_branch_parameters);
    }

    /*
     * FAGB time parameters
     */
    Dprint("Set FAGB parameters %g\n",phase_start_mass);

    bse->timescales[T_EAGB_TINF_1] = tbagb + (ip/(bse->GB[GB_A_HE]*bse->GB[GB_D]))*
        pow(bse->GB[GB_D]/bse->luminosities[L_BAGB],GBp);

    bse->timescales[T_EAGB_T] = bse->timescales[T_EAGB_TINF_1] - (bse->timescales[T_EAGB_TINF_1] - tbagb)*pow(bse->luminosities[L_BAGB]/bse->luminosities[L_LMX],GBp);

    bse->timescales[T_EAGB_TINF_2] = bse->timescales[T_EAGB_T] + (iq/(bse->GB[GB_A_HE]*bse->GB[GB_B]))*pow(bse->GB[GB_B]/bse->luminosities[L_LMX],GBq);

#endif
    Dprint("Find LTP etc %g\n",phase_start_mass);

    /*
     * Now to find Ltp and ttp using Mc,He,tp
     *
     * The star undergoes dredge-up at the first thermal pulse
     * decreasing Mc,He. Guess Mc at 1tp.
     */
    mc1 = guess_mc1tp(phase_start_mass,phase_start_mass,star,stardata);

    /*
     * NB we MUST call TPAGB_luminosity with the mass
     * at the first thermal pulse, and a constant mass
     * (i.e. phase_start_mass)
     */
    bse->luminosities[L_TPAGB_FIRST_PULSE] = TPAGB_luminosity(stellar_type,
                                                              star->phase_start_mass,
                                                              mc1,
                                                              mc1,
                                                              mc1,
                                                              star->age,
                                                              stardata->common.metallicity,
                                                              star->phase_start_mass,
                                                              star->num_thermal_pulses,
                                                              star->interpulse_period,
                                                              star->time_first_pulse,
                                                              star->time_prev_pulse,
                                                              star->time_next_pulse,
                                                              stardata,
                                                              bse->GB);

    if(Less_or_equal(mc1,bse->GB[GB_Mx]))
    {
        bse->timescales[T_TPAGB_FIRST_PULSE] = bse->timescales[T_EAGB_TINF_1] -
            (ip/(bse->GB[GB_A_HE]*bse->GB[GB_D]))*pow(mc1,mp);
    }
    else
    {
        bse->timescales[T_TPAGB_FIRST_PULSE] = bse->timescales[T_EAGB_TINF_2] -
            (iq/(bse->GB[GB_A_HE]*bse->GB[GB_B]))*pow(mc1,mq);
    }

    Dprint("First pulse at %g (mc = %g, mc1 = %g, L1TP = %g cf L = %g)\n",
           bse->timescales[T_TPAGB_FIRST_PULSE],
           star->core_mass[CORE_He],
           mc1,
           bse->luminosities[L_TPAGB_FIRST_PULSE],
           star->luminosity
        );

#ifdef NUCSYN
    // low WR mass loss rate leads to a wrong
    // estimate of this timescale at M~50, Z=1e-4
    // so force it to be long
    //if(mass>20.0)  bse->timescales[T_TPAGB_FIRST_PULSE]*=10.0;
#endif

    /*
     * SAGB time parameters
     */
    {
        double y=iq/(bse->GB[GB_A_H_HE]*bse->GB[GB_B]);
        if(Less_or_equal(mc1,bse->GB[GB_Mx]))
        {
            bse->timescales[T_TPAGB_TINF_1] = bse->timescales[T_TPAGB_FIRST_PULSE] +
                ip/(bse->GB[GB_A_H_HE]*bse->GB[GB_D]) *
                pow(bse->GB[GB_D]/bse->luminosities[L_TPAGB_FIRST_PULSE],GBp);
            bse->timescales[T_TPAGB_TX] = bse->timescales[T_TPAGB_TINF_1] -
                (bse->timescales[T_TPAGB_TINF_1] - bse->timescales[T_TPAGB_FIRST_PULSE])*
                pow(bse->luminosities[L_TPAGB_FIRST_PULSE]/bse->luminosities[L_LMX],GBp);
            bse->timescales[T_TPAGB_TINF_2] = bse->timescales[T_TPAGB_TX] +
                y*pow(bse->GB[GB_B]/bse->luminosities[L_LMX],GBq);
        }
        else
        {
            bse->timescales[T_TPAGB_TINF_1] = bse->timescales[T_EAGB_TINF_1];
            bse->timescales[T_TPAGB_TX] = bse->timescales[T_EAGB_T];
            bse->timescales[T_TPAGB_TINF_2] = bse->timescales[T_TPAGB_FIRST_PULSE] +
                y*pow(bse->GB[GB_B]/bse->luminosities[L_TPAGB_FIRST_PULSE],GBq);
        }
    }

    /*
     * Get an idea of when Mc,C = Mc,C,max on the AGB
     */
    mc2 = mcgbtf(bse->timescales[T_HE_IGNITION] + bse->timescales[T_HE_BURNING],
                 bse->GB[GB_A_HE],
                 bse->GB,
                 bse->timescales[T_EAGB_TINF_1],
                 bse->timescales[T_EAGB_TINF_2],
                 bse->timescales[T_EAGB_T]
        );

    mcmax = Max3(chandrasekhar_mass(stardata,star),
                 0.773*mcbagb - 0.35,
                 1.05*mc2);

    if(Less_or_equal(mcmax,mc1))
    {
        if(Less_or_equal(mcmax,bse->GB[GB_Mx]))
        {
            bse->timescales[T_TMCMAX] = bse->timescales[T_EAGB_TINF_1] -
                (ip/(bse->GB[GB_A_HE]*bse->GB[GB_D]))*pow(mcmax,mp);
        }
        else
        {
            bse->timescales[T_TMCMAX] = bse->timescales[T_EAGB_TINF_2] -
                (iq/(bse->GB[GB_A_HE]*bse->GB[GB_B]))*pow(mcmax,mq);
        }
    }
    else
    {
        /*
         * Star is on SAGB and we need to increase mcmax if any 3rd
         * dredge-up has occurred.
         */
        Boolean above_mcmin = FALSE;
        lambda = lambda_3dup(stardata,star,&above_mcmin);
        //lambda = Min(0.9,(0.3+0.001*Pow5(phase_start_mass)));
        mcmax = More_or_equal(lambda,1.0) ? 1e10 : (mcmax - lambda*mc1)/(1.0 - lambda);

        if(Less_or_equal(mcmax,bse->GB[GB_Mx]))
        {
            bse->timescales[T_TMCMAX] = bse->timescales[T_TPAGB_TINF_1] -
                (ip/(bse->GB[GB_A_H_HE]*bse->GB[GB_D]))*pow(mcmax,mp);
        }
        else
        {
            bse->timescales[T_TMCMAX] = bse->timescales[T_TPAGB_TINF_2] -
                (iq/(bse->GB[GB_A_H_HE]*bse->GB[GB_B]))*pow(mcmax,mq);
        }

    }
    bse->timescales[T_TMCMAX] = Max(tbagb,bse->timescales[T_TMCMAX]);


    /*
     * Calculate the nuclear timescale - the time of exhausting
     * nuclear fuel without further mass loss.
     * This means we want to find when Mc = Mass which defines Tn and will
     * be used in determining the timestep required. Note that after some
     * stars reach Mc = Mass there will be a Naked Helium Star lifetime
     * which is also a nuclear burning period but is not included in Tn.
     */
    if(Fequal(mass,mcbagb) && stellar_type<EAGB)
    {
        Dprint("Set tn=tbagb=%g\n",bse->tn);
        bse->tn = tbagb;
    }
    else
    {
        /* Note that the only occurence of Mc being double-valued is for stars
         * that have a dredge-up. If Mass = Mc where Mc could be the value taken
         * from CHeB or from the AGB we need to check the current stellar type.
         */
        if(mass>mcbagb ||
           (More_or_equal(mass,mc1) && stellar_type>CHeB))
        {
            stellar_timescales_massive_AGB(star,
                                           bse,
                                           phase_start_mass,
                                           mass,
                                           mcbagb,
                                           ip,
                                           iq,
                                           mp,
                                           mq,
                                           stellar_type);
        }
        else
        {
            stellar_timescales_low_mass_AGB(stardata,
                                            star,
                                            bse,
                                            giant_branch_parameters,
                                            metallicity_parameters,
                                            phase_start_mass,
                                            mass,
                                            mcbagb,
                                            ip,
                                            iq,
                                            mp,
                                            mq);
        }
    }

    bse->tn = Min(bse->tn,bse->timescales[T_TMCMAX]);

    return (0);
}
#endif
