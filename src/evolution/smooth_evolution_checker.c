#include "../binary_c.h"
No_empty_translation_unit_warning;


Boolean smooth_evolution_checker(
    struct stardata_t * const stardata,
    const Boolean check_stellar_type_change,
    const Boolean check_duplicity_change,
    const Boolean check_events_pending
    )
{
    /*
     * Define "smooth" evolution to be when the stellar
     * types do not change, the system remains single or binary,
     * and there are no events
     */
    return Boolean_(
        (
            /* always check we have a previous stardata */
            stardata->previous_stardata != NULL
            )
        &&
        (
            /*
             * check for stellar type change
             */
            check_stellar_type_change == FALSE || (
                stardata->star[0].stellar_type == stardata->previous_stardata->star[0].stellar_type &&
                stardata->star[1].stellar_type == stardata->previous_stardata->star[1].stellar_type
                )
            )
        &&
        (
            /*
             * check for duplicity change
             */
            check_duplicity_change == FALSE ||
            _System_is_binary(stardata) == _System_is_binary(stardata->previous_stardata)
            )
        &&
        (
            /*
             * check for pending events
             */
            check_events_pending == FALSE ||
            events_pending(stardata) == FALSE
            )

        );
}
