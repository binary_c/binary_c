#include "binary_c.h"

#define _No_events                                              \
    (stardata->common.had_event == FALSE                        \
     && stardata->previous_stardata != NULL                     \
     && stardata->previous_stardata->common.had_event == FALSE)

Boolean Pure_function_if_no_debug can_reject_with_same_timestep(struct stardata_t * Restrict const stardata)
{
    /*
     * Return TRUE if we're allowed to reject and restart
     * timestep without changing the timestep.
     */
    const Boolean x =
        /*
         * We cannot reject if we had events
         */
        Boolean_(_No_events);

    Dprint("at %30.20g model %d can reject timestep? %s\n",
           stardata->model.time,
           stardata->model.model_number,
           Yesno(x));

    return x;
}
