#pragma once
#ifndef EVOLUTION_SPLIT_CODES_H
#define EVOLUTION_SPLIT_CODES_H

#include "evolution_split_codes.def"

/*
 * Construct evolution split codes
 */
#undef X
#define X(CODE,TEXT) CODE,
enum { EVOLUTION_SPLIT_CODES };

#undef X
#define X(CODE,TEXT) #CODE,
static char * evolution_split_code_macros[] Maybe_unused = { EVOLUTION_SPLIT_CODES };

#undef X
#define X(CODE,TEXT) TEXT,
static char * evolution_split_code_strings[] Maybe_unused = { EVOLUTION_SPLIT_CODES };

#define Evolution_split_string(N) Array_string(evolution_split_code_strings,(N))

#undef X
#endif // EVOLUTION_SPLIT_CODES_H
