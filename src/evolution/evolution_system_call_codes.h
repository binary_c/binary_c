#pragma once
#ifndef EVOLUTION_SYSTEM_CALL_CODES_H
#define EVOLUTION_SYSTEM_CALL_CODES_H

#include "evolution_system_call_codes.def"

/*
 * Construct evolution system_call codes
 */
#undef X
#define X(CODE,TEXT) CODE,
enum { EVOLUTION_SYSTEM_CALL_CODES };

#undef X
#define X(CODE,TEXT) #CODE,
static char * evolution_system_call_code_macros[] Maybe_unused = { EVOLUTION_SYSTEM_CALL_CODES };

#undef X
#define X(CODE,TEXT) TEXT,
static char * evolution_system_call_code_strings[] Maybe_unused = { EVOLUTION_SYSTEM_CALL_CODES };

#define Evolution_system_call_string(N) Array_string(evolution_system_call_code_strings,(N))

#undef X
#endif // EVOLUTION_SYSTEM_CALL_CODES_H
