#pragma once
#ifndef EVOLUTION_PROTOTYPES_H
#define EVOLUTION_PROTOTYPES_H

#include "../binary_c_parameters.h"
#include "../binary_c_macros.h"
#include "../binary_c_structures.h"
#include "../binary_c_debug.h"

int time_evolution(struct stardata_t * Restrict const stardata,
                   Evolution_system_type system_type);
void evolution_time_zero_setup(struct stardata_t * Restrict const stardata);
int evolve_system(struct stardata_t * Restrict const stardata);
int evolve_system_binary_c(struct stardata_t * Restrict const stardata);

int stellar_evolution(struct stardata_t * Restrict const stardata,
                      const int codetype);
void update_binary_star_variables(struct stardata_t * Restrict const stardata,
                                  int * Restrict const retval);
void initialize_parameters(struct stardata_t * Restrict const stardata);
void reinitialize_parameters(struct stardata_t * Restrict const stardata);
void set_next_timestep(struct stardata_t * Restrict const stardata);
Boolean loop_evolution(struct stardata_t * Restrict const stardata);
void end_of_evolution(struct stardata_t * Restrict const stardata);
void initialize_system_every_timestep(struct stardata_t * Restrict const stardata);
Boolean Pure_function check_for_time_exhaustion(struct stardata_t * Restrict const stardata,
                                                const int intpol);
Boolean to_loop_or_not_to_loop(struct stardata_t * Restrict const stardata);
void update_time(struct stardata_t * Restrict const stardata);

void start_of_evolution(struct stardata_t * Restrict const stardata);
void init_star(struct stardata_t * const stardata,
               struct star_t * const star);
void init_model(struct model_t * Restrict const model);
void init_common(struct stardata_t * Restrict const stardata);

void evolution_nanchecks(struct stardata_t * Restrict const stardata Maybe_unused);
Boolean Pure_function_if_no_debug system_is_observationally_single(struct stardata_t * Restrict const stardata);
void zero_derivatives(struct stardata_t * Restrict const stardata);
void evolution_cleanup(struct stardata_t * Restrict const stardata,
                       const Boolean close_logs);
int evolution_loop_top(struct stardata_t * Restrict const stardata);
void start_RLOF(struct stardata_t * Restrict const stardata);

#ifdef NUCSYN
void evolution_nucsyn(struct stardata_t * Restrict const stardata);
#endif // NUCSYN
int loop_check(struct stardata_t * Restrict const stardata, const int stop);

void evolution_difflog(struct stardata_t * Restrict const stardata);


void evolution_save_to_previous(struct stardata_t * Restrict const stardata);

void evolution_restore_from(struct stardata_t * Restrict const stardata,
                            struct stardata_t * Restrict const from_stardata,
                            const Boolean preserve_events);
void evolution_restore_from_previous(struct stardata_t * Restrict const stardata,
                            const Boolean preserve_events);
void evolution_restore_and_reduce_timestep(struct stardata_t * Restrict const stardata,
                            const Boolean preserve_events);
void evolution_restore_but_keep_events(struct stardata_t * const stardata);

int evolution_rejected(struct stardata_t * Restrict const stardata);
void evolution_success(struct stardata_t * Restrict const stardata);
int evolution_split(struct stardata_t * Restrict const stardata,
                    int * Restrict const status);

void zero_stellar_derivatives(struct stardata_t * Restrict const stardata);
void update_the_time(struct stardata_t * Restrict const stardata);
void update_phase_variables(struct stardata_t * Restrict const stardata);
Rejection Pure_function check_reject_flags(struct stardata_t * Restrict const stardata);

void zero_stellar_mass_and_angmom_derivatives(struct stardata_t * Restrict const stardata);

Boolean Pure_function_if_no_debug can_reject_with_same_timestep(struct stardata_t * Restrict const stardata);
Boolean Pure_function_if_no_debug can_reject_and_shorten_timestep(struct stardata_t * Restrict const stardata);
void pre_evolution(struct stardata_t * Restrict const stardata);
void post_evolution(struct stardata_t * Restrict stardata);
void evolution_time_explicit(struct stardata_t * Restrict const stardata);
void evolution_forward_Euler(struct stardata_t * Restrict const stardata);
void evolution_rejection_shorten_tests(struct stardata_t * Restrict const stardata);
void evolution_rejection_same_tests(struct stardata_t * Restrict const stardata);
void free_difflogstack(struct difflogstack_t * Restrict const logstack);

void terminate_evolution(struct stardata_t * const stardata,
                         const char * const message);

Boolean smooth_evolution(struct stardata_t * const stardata);
Boolean smooth_evolution_checker(
    struct stardata_t * const stardata,
    const Boolean check_stellar_type_change,
    const Boolean check_duplicity_change,
    const Boolean check_events_pending
    );
Boolean check_require_convergence_steps(struct stardata_t * const stardata);

void set_derived_derivatives(struct stardata_t * const stardata,
                             struct star_t * new,
                             Star_number k);
Boolean Pure_function check_for_evolution_stop(struct stardata_t * const stardata);
void calculate_derivatives(struct stardata_t * const stardata,
                           const Boolean RLOF);
int apply_derivatives(struct stardata_t * const stardata,
                      const Boolean RLOF,
                      const Boolean can_reject,
                      const Boolean can_reject_and_shorten);

int update_system_by_dt(struct stardata_t * Restrict const stardata,
                        const Evolution_system_type system_type,
                        const Boolean update_time);

void evolution_detect_stellar_type_change(struct stardata_t * const stardata);
Boolean stellar_type_exceeded(struct stardata_t * const stardata,
                              struct star_t * const star);

int evolve_star(struct stardata_t * const stardata,
                struct star_t * const star,
                const int codetype);

char * evolution_rejection_label(struct stardata_t * Restrict const stardata,
                                 Reject_index reject_reason);
struct double_array_t * evolutionary_command_times(struct stardata_t * Restrict const stardata);
void apply_evolutionary_commands(struct stardata_t * Restrict const stardata,
                                 const Boolean apply_derivatives,
                                 const Boolean apply_events,
                                 const Boolean delete_expired);
void sanity_checks(struct stardata_t * const stardata);

#endif /* EVOLUTION_PROTOTYPES_H */
