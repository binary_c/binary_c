#include "../binary_c.h"
No_empty_translation_unit_warning;


void evolution_restore_from_previous(struct stardata_t * Restrict const stardata,
                            const Boolean preserve_events)
{
    /*
     * Restore stardata from previous_stardata.
     */
    evolution_restore_from(stardata,
                           stardata->previous_stardata,
                           preserve_events);
}
