#pragma once
#ifndef EVOLUTION_DIFFLOG_H
#define EVOLUTION_DIFFLOG_H

/*
 * Structs, macros and prototypes for the evolution difflog
 */

#define Stack_debug_print(...) /* do nothing */


#define RLOFing(D,K)                                                    \
    (                                                                   \
        (D)->stellar_type[(K)]!=MASSLESS_REMNANT &&                     \
        (D)->radius[(K)] > 0.0 &&                                       \
        (D)->roche_radius[(K)] > 0.0 &&                                 \
        Is_not_zero((D)->roche_radius[(K)]) &&                          \
        More_or_equal((D)->radius[(K)]  , (D)->roche_radius[(K)])       \
        )

static void set_diffstats(const struct stardata_t * const Restrict stardata,
                          struct diffstats_t * const Restrict now);

static Boolean cf_diffstats(struct stardata_t * Restrict const stardata,
                            const struct diffstats_t * Restrict const now,
                            const struct diffstats_t * Restrict const prev,
                            Boolean * Restrict const flags,
                            Boolean * Restrict const prevflags Maybe_unused,
                            Boolean lockflags[NUMBER_OF_STARS][LOCK_NFLAGS+1]);

static void dolog(struct stardata_t * Restrict const stardata,
                  const char * const Restrict cin,
                  const char * const leadchar);

static void logging_dependencies(struct stardata_t * Restrict const stardata,
                                 Boolean * const flags Maybe_unused,
                                 Boolean lockflags[NUMBER_OF_STARS][LOCK_NFLAGS+1]);

static void push_stacklog(struct difflogstack_t * const logstack,
                          struct stardata_t * const stardata,
                          const char * const cin);

#ifdef __HAVE_GNU_QSORT_R
static int compare_logitems(const void * Restrict a,
                            const void * Restrict b,
                            void * arg Maybe_unused);
#endif // __HAVE_GNU_QSORT_R
#ifdef __HAVE_BSD_QSORT_R
static int compare_logitems(void * arg Maybe_unused,
                            const void * Restrict a,
                            const void * Restrict b);
#endif
static struct difflogstack_t * new_difflogstack(void);
static void clean_difflogstack(struct difflogstack_t * const logstack);

static void vdh_log(struct stardata_t * const Restrict stardata,
                    struct diffstats_t * Restrict const now,
                    struct diffstats_t * Restrict const prev,
                    const Boolean any_true,
                    Boolean * Restrict const flags);

#endif // EVOLUTION_DIFFLOG_H
