#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Function to evolve a star
 */
/*
#undef Dprint
#define Dprint(...)                                                 \
    printf("evolve_star %d model %d time %g 1-Xsum old %g new %g ", \
           __LINE__,                                                \
           stardata->model.model_number,                            \
           stardata->model.time,                                    \
           1.0-nucsyn_totalX(star->Xenv),                           \
           1.0-nucsyn_totalX(newstar->Xenv));                       \
    printf(__VA_ARGS__);
*/

int evolve_star(struct stardata_t * const stardata,
                       struct star_t * const star,
                       const int codetype)
{
    int retval = EVOLUTION_KEEP_LOOPING_RESULT;
    struct model_t * model = &stardata->model;

    /*
     * Set up temp space if required
     */
    if(stardata->tmpstore->stellar_evolution_newstar == NULL)
    {
        stardata->tmpstore->stellar_evolution_newstar = New_star;
    }

    /*
     * Make a copy of the star in newstar
     */
    struct star_t * const newstar = stardata->tmpstore->stellar_evolution_newstar;
    Dprint("acquire stellar parameters for star %d\n",star->starnum);
    copy_star(stardata,
              star,
              newstar);

    /*
     * Update the age
     */
    newstar->age = model->time - newstar->epoch;

    Dprint("stellarev top star %d: Set age = time=%g - epoch=%g = %g\n",
           newstar->starnum,
           model->time,
           newstar->epoch,
           newstar->age);

#ifdef ACQUIRE_TEST_FOR_NEGATIVE_AGES
    /* allow small numerical problems? */
    if(newstar->age >-1e-3 && newstar->age<0.0)
    {
        newstar->age = 0.0;
    }
    else if(newstar->age < -1e-2)
    {
        Exit_binary_c(stardata,
                      BINARY_C_ACQUIRE_NEGATIVE_AGE,
                      "(acquire_stellar_parameters) Age is negative oops : time=%g epoch=%g -> age=%g dtm=%g\n",
                      model->time,
                      newstar->epoch,
                      newstar->age,
                      model->dtm);
    }
#endif // ACQUIRE_TEST_FOR_NEGATIVE_AGES

#ifdef ACQUIRE_FIX_NEGATIVE_AGES
    /* fudge to force age to be position */
    newstar->age = Max(0.0, newstar->age);
#endif//ACQUIRE_FIX_NEGATIVE_AGES

#ifdef WD_KICKS___DEPRECATED
    /* Kick WD at the end of the AGB */
    Dprint("WD kick check %d %d\n",newstar->kick_WD,newstar->SN_type);
    if(newstar->kick_WD==TRUE && model->sgl==FALSE)
    {
        newstar->kick_WD=FALSE;
        newstar->already_kicked_WD=TRUE;
        int it,nkicks;
#ifdef NUCSYN
        if(stardata->preferences->wd_kick_when==WD_KICK_AT_EVERY_PULSE)
        {
            nkicks = ((int)(newstar->num_thermal_pulses+0.01))-newstar->prev_kick_pulse_number;
        }
        else
#endif //NUCSYN
        {
            nkicks = 1;
        }
        newstar->SN_type = SN_WDKICK;
        for(it=0;it<nkicks;it++)
        {
            stardata->preferences->sn_kick_distribution[SN_WDKICK] =
                KICK_VELOCITY_FIXED;
            monte_carlo_kick(star,
                             star,
                             newstar->mass,
                             newstar->mass,
                             stardata->star[Other_star(k)].mass,
                             &stardata->common.orbit.eccentricity,
                             &stardata->common.orbit.separation,
                             &stardata->common.orbit.angular_momentum,
                             stardata,
                             stardata);
        }
        newstar->SN_type = SN_NONE;

#ifdef NUCSYN
        /* save pulse number of previous kick */
        newstar->prev_kick_pulse_number=((int)(newstar->num_thermal_pulses+0.01));
#endif//NUCSYN

        if(Is_not_zero(stardata->preferences->sn_kick_dispersion[SN_WDKICK]))
        {
            Append_logstring(LOG_SN_AND_KICK,"WDKICK %3.1f km/s",stardata->preferences->sn_kick_dispersion[SN_WDKICK]);
        }

        /* if broken binary, return */
        if(stardata->common.orbit.eccentricity > 1.0)
        {
            return EVOLUTION_SYSTEM_IS_BROKEN_APART;
        }
    }
#endif//WD_KICKS

    Dprint("age=%12.12e from time=%12.12e epoch=%12.12e phase_start_mass=%g mass(t)=%g\n",
           newstar->age,
           model->time,
           newstar->epoch,
           newstar->phase_start_mass,
           newstar->mass);

    if(codetype == DETACHED_SYSTEM_CALL)
    {
        /* non-RLOF */
        Dprint("Early call intpol=%d\n",model->intpol);
        newstar->aj0 = newstar->age;

        const Core_type core_type = ID_core(newstar->stellar_type);

        if(model->intpol == 0)
        {
            newstar->mcxx = core_type == CORE_NONE ? 0.0 :
                newstar->core_mass[core_type];
        }
        else if(core_type != CORE_NONE &&
                model->intpol>0)
        {
            newstar->core_mass[ID_core(newstar->stellar_type)] = newstar->mcxx;
        }
        newstar->mass_at_RLOF_start = newstar->phase_start_mass;
    }

    if(newstar->stellar_type < NEUTRON_STAR &&
       newstar->mass > MAXIMUM_STELLAR_MASS)
    {
        /*
         * Masses over 100Msun should probably not be trusted in the
         * evolution formulae. MAXIMUM_STELLAR_MASS is usually
         * set to 100.
         */
        strip_supermassive_star(stardata,
                                newstar);
        return EVOLUTION_STOP_LOOPING_RESULT;
    }

    /*
     * Compute stellar structure
     */
    stellar_structure(stardata,
                      STELLAR_STRUCTURE_CALLER_stellar_evolution,
                      newstar,
                      NULL);

    Dprint("post stellar structure\n");

    /*
     * Save derivatives
     */
    if(model->intermediate_step == FALSE)
    {
        set_derived_derivatives(stardata,
                                newstar,
                                newstar->starnum);
    }

    /*
     * Todo : replace the use of rdot with derivative[]
     */
    if(codetype == DETACHED_SYSTEM_CALL)
    {
        /*
         * If detached, set radius derivative for
         * Roche-lobe overflow calculations
         */
        if(Is_not_zero(model->dtm))
        {
            const double dr = newstar->radius - star->radius;
            newstar->rdot = fabs(dr)/model->dtm;
            newstar->drdt = dr;
        }
        else
        {
            newstar->rdot = 0.0;
            newstar->drdt = 0.0;
        }
    }

    {
#ifdef BSE
        const double tm = star->tm;
        const double tn = star->tn;
#else
        const double tm = 0.0; // main sequence lifetime
        const double tn = 0.0; // nuclear burning lifetime
#endif

        Dprint("ret  stellar_structure m0=%12.12g age=%12.12g mt=%12.12g tm=%30.22g tn=%30.22g rm=%12.12g lum=%12.12g kw=%d mc=%12.12g rc=%12.12g mcGB=%g mcCO=%g SN=%d\n",
               star->phase_start_mass,
               star->age,
               star->mass,
               tm,
               tn,
               star->radius,
               newstar->luminosity,
               newstar->stellar_type,
               Outermost_core_mass(newstar),
               newstar->radius,
               newstar->core_mass[CORE_He],
               newstar->core_mass[CORE_CO],
               newstar->SN_type);
    }
    Dprint("Check for stellar type change k=%d, kw=%d, kstar=%d, sgl=%d -> changed? %s\n",
           newstar->starnum,
           newstar->stellar_type,
           star->stellar_type,
           model->sgl,
           Yesno(newstar->stellar_type != star->stellar_type));

    if(newstar->stellar_type != star->stellar_type)
    {
        const Core_type core_type = ID_core(newstar->stellar_type);
        newstar->phase_start_core_mass =
            core_type == CORE_NONE ? 0.0 : newstar->core_mass[core_type];

        if(codetype==DETACHED_SYSTEM_CALL)
        {
            newstar->stellar_timestep = 0.01;
        }

        if(newstar->stellar_type == MASSLESS_REMNANT)
        {
            /* Accretion induced collapse can get you here */
            Dprint("SN with no remnant (codetype=%d)\n",codetype);
            return EVOLUTION_SYSTEM_IS_BROKEN_APART;
        }
        newstar->phase_start_mass = fabs(newstar->phase_start_mass);
        newstar->mass_at_RLOF_start = newstar->phase_start_mass; // required for helium nova prevention
        newstar->epoch = model->time - newstar->age;

        Dprint("Set epoch (2) = %g from %g - %g (newstar->age = %g)\n",
               newstar->epoch,
               model->time,
               star->age,
               newstar->age);
    }

    Dprint("Det. rdot = %g (newrad=%g oldrad=%g dtm=%g)\n",
           newstar->rdot,
           newstar->radius,
           star->radius,
           model->dtm);

    /*
     * Determine stellar evolution timescales
     * and the timestep required
     */
    if(stardata->model.intermediate_step == FALSE)
    {
        Dprint("call timestep time remaining %g\n",
               model->time_remaining);
        double dt = 0.0; /* must be set: will be output in Dprints */
        stellar_timestep(newstar->stellar_type,
                         newstar->age,
                         newstar->tm,
                         newstar->tn,
                         NULL,
                         &dt,
                         &model->time_remaining,
                         newstar,
                         stardata);
        Dprint("set stellar timestep=%g from stellar_type=%d dt=%g time_remaining=%g MINIMUM_STELLAR_TIMESTEP %g\n",
               newstar->stellar_timestep,
               newstar->stellar_type,
               dt,
               model->time_remaining,
               MINIMUM_STELLAR_TIMESTEP);
    }

    /*
     * Update stellar structure:
     * derived variables
     */
    newstar->effective_radius =
        (newstar->roche_radius > TINY &&
         newstar->radius > newstar->roche_radius) ?
        Max(newstar->roche_radius,
            newstar->core_radius) : newstar->radius;
    Dprint("effective radius is %g\n",newstar->effective_radius);

#ifdef XRAY_LUMINOSITY
    newstar->Xray_luminosity = Xray_luminosity(newstar);
#endif
#ifdef GIANT_CRICTICAL_Q_CHEN_HAN
    // save base of the giant branch luminosity and radius
    // as well as the A0 parameter
    if(Is_zero(newstar->A0) &&
       newstar->stellar_type > HERTZSPRUNG_GAP)
    {
        newstar->A0 = log10(newstar->radius);
    }
#endif// GIANT_CRICTICAL_Q_CHEN_HAN

    /*
     * We've set up newstar, copy back onto the star
     */
    copy_star(stardata,
              newstar,
              star);

    Dprint("Copy newstar %d==%d (%p) ->star (%p), set age %g\n",
           newstar->starnum,
           star->starnum,
           (void*)newstar,
           (void*)star,
           star->age);
    Dprint("TMS set star %d = %30.22e\n",star->starnum,star->tms);

    return retval;
}
