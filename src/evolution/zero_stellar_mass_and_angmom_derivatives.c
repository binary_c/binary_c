#include "../binary_c.h"
No_empty_translation_unit_warning;


void zero_stellar_mass_and_angmom_derivatives(struct stardata_t * Restrict const stardata)
{
    /*
     * Set all stellar mass and angular momentum derivatives
     *  to zero
     */
    Star_number k;
    Starloop(k)
    {
        struct star_t * star = &stardata->star[k];
        Zero_stellar_mass_derivatives(star);
        Zero_stellar_angmom_derivatives(star);
    }
}
