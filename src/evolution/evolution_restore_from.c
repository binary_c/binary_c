#include "../binary_c.h"
No_empty_translation_unit_warning;

void evolution_restore_from(struct stardata_t * Restrict const stardata,
                            struct stardata_t * Restrict const from_stardata,
                            const Boolean preserve_events)
{
    /*
     * Restore stardata from from_stardata, if this is possible.
     *
     * Take note to erase the event stack first, unless preserve_events is
     * TRUE in which case events are allowed to happen.
     *
     */
    if(from_stardata!=NULL)
    {
        Foreach_star(star)
        {
            Dprint("Star %d\n",
                   star->starnum);
        }

        Dprint("Restore stardata from %p (t=%30.12e) onto %p (%30.12e)\n",
               (void*)from_stardata,
               from_stardata->model.time,
               (void*)stardata,
               stardata->model.time);

        int n_events = 0;
        struct binary_c_event_t ** events = NULL;
        if(preserve_events == TRUE)
        {
            n_events = stardata->common.n_events;
            events = stardata->common.events;
        }
        else
        {
            erase_events(stardata);
        }
        int n_rejected = stardata->model.n_rejected_models;
        int n_rejected_without_shorten = stardata->model.n_rejected_models_without_shorten;

        /*
         * Copy stardata, but preserve the previous_stardata stack
         * and stardata_stack
         */
        struct stardata_t * mask = stardata->model.restore_mask;
        struct stardata_t * mask_contents = stardata->model.restore_mask_contents;

        Dprint("Copy %p with prefs %p onto %p, mask = %p, mask_contents = %p\n",
               (void*)from_stardata,
               (void*)from_stardata->preferences,
               (void*)stardata,
               (void*)mask,
               (void*)mask_contents);

#ifdef ADAPTIVE_RLOF2
        Dprint(">>> pre restore %g \n",stardata->star[0].adaptive2_mdot);
#endif

        copy_stardata(
            from_stardata,
            stardata,
            COPY_STARDATA_MAINTAIN_TO_STACK_POINTERS,
            COPY_STARDATA_PERSISTENT_FROM_POINTER
            );

        /*
         * mask stardata (if necessary)
         */
        apply_stardata_mask(stardata,
                            mask,
                            mask_contents);

#ifdef NEW_ADAPTIVE_RLOF_CODE
        printf("restored a = %30.20e\n",stardata->common.orbit.separation);
#endif//NEW_ADAPTIVE_RLOF_CODE
        /*
         * free masks now they have been applied
         */
        Safe_free_nocheck(mask);
        Safe_free_nocheck(mask_contents);


        /*
         * Restore events
         */
        if(preserve_events == TRUE)
        {
            stardata->common.n_events = n_events;
            stardata->common.events = events;
        }
        stardata->model.n_rejected_models = n_rejected;
        stardata->model.n_rejected_models_without_shorten = n_rejected_without_shorten;
    }
}
