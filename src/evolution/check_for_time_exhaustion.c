#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Check if time has run out
 */

#ifdef REVERSE_TIME
/* use absolute time */
#define The_time fabs(stardata->model.time)
#else
#define The_time (stardata->model.time)
#endif // REVERSE_TIME


Boolean Pure_function check_for_time_exhaustion(struct stardata_t * const stardata,
                                                const int intpol)
{
    const Boolean exhausted = Boolean_(
        (
            stardata->preferences->max_model_number>0 &&
            stardata->model.model_number >= stardata->preferences->max_model_number
            ) ||
        (
            intpol==0 &&
            More_or_equal(
                The_time + stardata->preferences->minimum_timestep,
                stardata->model.max_evolution_time
                )
            )
        );

    return exhausted;
}
