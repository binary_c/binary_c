#include "../binary_c.h"
No_empty_translation_unit_warning;


void evolution_success(struct stardata_t * Restrict const stardata)
{
    /*
     * The current stardata is good, i.e. not rejected.
     */
    modulate_zoomfac(stardata,
                     ZOOMFAC_INCREASE_TIMESTEP);

#ifdef ADAPTIVE_RLOF2
    stardata->model.adaptive_RLOF2_reject_count = 0;
    stardata->model.adaptive_RLOF2_adapt_count = 0;
    stardata->model.adapting_RLOF2 = FALSE;
    {
        Star_number k;
        Starloop(k)
        {
            stardata->star[k].adaptive2_converged = FALSE;
        }
    }
#endif // ADAPTIVE_RLOF2

    Dprint("evolution succesful %d %30.20g %30.20g %30.20g %30.20g %g %g\n",
           stardata->model.model_number,
           stardata->model.time,
           stardata->model.dtm,
           stardata->model.dt_zoomfac,//4
           Is_not_zero(stardata->star[0].roche_radius) ? (stardata->star[0].radius / stardata->star[0].roche_radius) : 0.0, //5
           Is_not_zero(stardata->common.orbit.angular_frequency) ? (stardata->star[0].omega / stardata->common.orbit.angular_frequency) : 0.0,//6
           Is_not_zero(stardata->common.orbit.angular_frequency) ? (stardata->star[1].omega / stardata->common.orbit.angular_frequency) : 0.0//7
        );
}
