#include "../binary_c.h"
No_empty_translation_unit_warning;

#define Extend_times(N)                                 \
    {                                                   \
        double * const _new_times =                     \
            Realloc(array->doubles,                     \
                    sizeof(double)*(array->n+(N)));     \
        if(_new_times != NULL)                          \
        {                                               \
            array->doubles = _new_times;                \
        }                                               \
        else                                            \
        {                                               \
            return NULL;   /* error! */                 \
        }                                               \
    }

#define Push_times(TIME)                        \
    {                                           \
        if((TIME)>stardata->model.time)         \
        {                                       \
            Extend_times(1);                    \
            array->doubles[array->n] = (TIME);  \
            array->n++;                         \
        }                                       \
    }

struct double_array_t * evolutionary_command_times(struct stardata_t * Restrict const stardata)
{
    /*
     * Return an array of evolutionary-command times
     */
    struct cdict_t * command_cdict = stardata->preferences->commands;
    if(command_cdict == NULL)
    {
        return NULL;
    }
    else
    {
        struct double_array_t * const array = new_double_array(0);
        CDict_loop(command_cdict, event)
        {
            if(event->value.type == CDICT_DATA_TYPE_CDICT)
            {
                CDict_loop(event->value.value.cdict_pointer_data, command)
                {
                    struct cdict_t * const cdict = command->value.value.cdict_pointer_data;
                    char * const subcommand = command->key.key.string_data;

                    if(Strings_equal(subcommand, "accrete period"))
                    {
                        double start = CDict_nest_get_data_value(cdict,
                                                                 start,
                                                                 "start");
                        double end = CDict_nest_get_data_value(cdict,
                                                               end,
                                                               "end");
                        Push_times(start);
                        Push_times(end);
                    }
                    else if(Strings_equal(subcommand, "accrete event"))
                    {
                        double at = CDict_nest_get_data_value(cdict,
                                                              at,
                                                              "at");
                        Push_times(at);
                    }
                }
            }
        }
        return array;
    }
}
