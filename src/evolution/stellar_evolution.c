#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Do stellar evolution to acquire stellar parameters
 * (M, R, L, Mc, etc.) at time model->time
 *
 * "codetype" is:
 *
 * DETACHED_SYSTEM_CALL : this is during detached evolution, not RLOF
 * RLOF_SYSTEM_CALL  : called later in the main evolution loop, this is during RLOF
 *
 * Return value:
 * EVOLUTION_SYSTEM_IS_BROKEN_APART
 * STOP_LOOPING_RESULT
 * KEEP_LOOPING_RESULT
 *
 */

int stellar_evolution(struct stardata_t * Restrict const stardata,
                      const int codetype)
{
    int retval = EVOLUTION_KEEP_LOOPING_RESULT;

    Dprint("Stellar evolution evolving starloop");

    Foreach_evolving_star(star)
    {
        const int ret = evolve_star(stardata,
                                    star,
                                    codetype);
        if(ret != EVOLUTION_KEEP_LOOPING_RESULT)
        {
            retval = ret;
        }
    } // loop over evolving stars

    /*
     * Timestep should be set for massless remnants
     * to allow logging.
     */
    Foreach_star(star)
    {
        if(star->stellar_type==MASSLESS_REMNANT)
        {
            star->hybrid_HeCOWD = FALSE;
            double dt;
            stellar_timestep(star->stellar_type,
                             star->age,
                             star->tm,
                             star->tn,
                             NULL,
                             &dt,
                             &stardata->model.time_remaining,
                             star,
                             stardata);
        }
    }

    if(retval == EVOLUTION_KEEP_LOOPING_RESULT)
    {
        handle_massless_remnants(stardata);
        set_kelvin_helmholtz_times(stardata);
    }

    return retval;
}
