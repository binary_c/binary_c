#include "../binary_c.h"
No_empty_translation_unit_warning;


void zero_derivatives(struct stardata_t * Restrict const stardata)
{
    /* set all derivatives are zero */
    Foreach_star(star)
    {
        for(int i=0;i<DERIVATIVE_STELLAR_NUMBER;i++)
        {
            star->derivative[i]=0.0;
        }
    }
    for(int i=0;i<DERIVATIVE_SYSTEM_NUMBER;i++)
    {
        stardata->model.derivative[i]=0.0;
    }
}
