#pragma once
#ifndef EVOLUTION_LOOP_CODES_H
#define EVOLUTION_LOOP_CODES_H

#include "evolution_loop_codes.def"

/*
 * Construct evolution loop codes
 */
#undef X
#define X(CODE,TEXT) EVOLUTION_##CODE,
enum { EVOLUTION_LOOP_CODES };

#undef X
#define X(CODE,TEXT) #CODE,
static char * evolution_loop_code_macros[] Maybe_unused = { EVOLUTION_LOOP_CODES };

#undef X
#define X(CODE,TEXT) TEXT,
static char * evolution_loop_code_strings[] Maybe_unused = { EVOLUTION_LOOP_CODES };

#define Evolution_loop_string(N) Array_string(evolution_loop_code_strings,(N))

#undef X

/* special case */
#define EVOLUTION_ROCHE_OVERFLOW_NONE EVOLUTION_KEEP_LOOPING_RESULT

#endif // EVOLUTION_LOOP_CODES_H
