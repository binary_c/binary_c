#include "../binary_c.h"
No_empty_translation_unit_warning;



void evolution_time_explicit(struct stardata_t * Restrict const stardata)
{
    /*
     * We've converged on our system using whatever solver...
     * now we have to perform algorithms that only work
     * using a forward Euler type solver.
     *
     * Note: we could (in theory) set these as events,
     * but it's easier to put them in here.
     */

    Foreach_star(star)
    {
        struct star_t * prevstar Maybe_unused = &stardata->previous_stardata->star[star->starnum];

#ifdef BSE

        if(star->stellar_type == TPAGB)
        {
            /*
             * Update interpulse period
             */
            star->interpulse_period =
                Karakas2002_interpulse_period(stardata,
                                              star->core_mass[CORE_He],
                                              star->mass,
                                              star->mc_1tp,
                                              stardata->common.metallicity,
                                              star->lambda_3dup,
                                              star);


            /*
             * Update time of next pulse based on the current
             * interpulse period
             */
            star->time_next_pulse = star->time_prev_pulse + star->interpulse_period;

            /*
             * Fractional number of pulses since previous pulse
             */
            const double dntpf =
                (star->age - star->time_prev_pulse)/
                star->interpulse_period;

            /*
             * Integer number of pulses (but stored in a double)
             */
            star->dntp =
                Max(0.0,
                    /* no next pulse time set */
                    Is_zero(star->time_next_pulse) ? 0.0 :
                    /* been through next dntpf pulses? */
                    star->age > star->time_next_pulse ? (double)((int)dntpf) :
                    /* has not been through the next pulse */
                    0.0);

            Dprint("TPAGBX age=%20.10e Myr, pulse prev=%20.10e next=%20.10e : age-prev = %g %g, reached? %s -> [ dntp = %20.10e = %20.10e ] NTP %g\n",
                   star->age,
                   star->time_prev_pulse,
                   star->time_next_pulse,
                   star->age - star->time_prev_pulse,
                   (star->age - star->time_prev_pulse) / star->interpulse_period,
                   Yesno(star->age > star->time_next_pulse),
                   dntpf,
                   star->dntp,
                   star->num_thermal_pulses);
            /*
             * 3rd dredge up efficiency
             */
            Boolean above_mcmin;
            star->lambda_3dup = lambda_3dup(stardata,
                                            star,
                                            &above_mcmin);
            star->dm_3dup = 0.0;

            /*
             * dntp is a positive integer or zero (stored in a double),
             * so this can only be true if dntp >= 1
             */
            if(star->dntp > 0.5)
            {
                /*
                 * Thermal pulse
                 */
                if(above_mcmin)
                {
                    /*
                     * Third dredge up
                     */
                    star->num_thermal_pulses_since_mcmin += star->dntp;
                    star->dm_3dup = star->lambda_3dup * (star->core_mass_no_3dup - star->Mc_prev_pulse_no_3dup);
                }

                /*
                 * Increse the thermal pulse count
                 */
                star->num_thermal_pulses += star->dntp;

                /*
                 * Save the core mass for the next time this function is called
                 */
                star->Mc_prev_pulse = star->core_mass[CORE_He];
                star->Mc_prev_pulse_no_3dup = star->core_mass_no_3dup;

                /*
                 * Recalculate the interpulse period and
                 * hence the time of the next pulse
                 */
                star->interpulse_period =
                    Karakas2002_interpulse_period(stardata,
                                                  star->core_mass[CORE_He],
                                                  star->mass,
                                                  star->mc_1tp,
                                                  stardata->common.metallicity,
                                                  star->lambda_3dup,
                                                  star);

                star->time_prev_pulse = star->time_next_pulse;
                star->time_next_pulse = star->time_prev_pulse +
                    star->dntp * star->interpulse_period;

            }
        }
#endif // BSE
    }
}
