#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Calculate derivatives
 */
void calculate_derivatives(struct stardata_t * const stardata,
                           const Boolean RLOF)
{
    Dprint("Main loop set kw1,kw2 dtm=%g\n",
           stardata->model.dtm);
    Nancheck(stardata->common.orbit.angular_momentum);

    stardata->model.dt = 1e6 * stardata->model.dtm;

    /*
     * Can only do things if the timestep
     * is non-zero. Often the stellar evolution
     * algorithm will set the timestep to zero
     * to update the structures without changing the
     * masses.
     */
    if(Is_not_zero(stardata->model.dt) &&
       stardata->preferences->tasks[BINARY_C_TASK_CALCULATE_DERIVATIVES])
    {
        /*
         * calculate wind loss/gain rates and
         * associated stellar/orbital derivatives
         */
        Dprint("Main loop: wind loss dtm=%g\n",
               stardata->model.dtm);
        calc_wind_loss_and_gain_rates(stardata,RLOF);
        Dprint("Main loop: initial_mass_loss dtm=%g\n",
               stardata->model.dtm);

        /*
         * Calculate RLOF rates: the stability of
         * RLOF is determined prior to being here.
         */
        if(RLOF == TRUE)
        {
            Dprint("In RLOF : calculate derivatives");
            struct RLOF_orbit_t * RLOF_orbit = Malloc(sizeof(struct RLOF_orbit_t));
            RLOF_init_dM_orbit(stardata,RLOF_orbit);
            RLOF_stars;
            const double mass_transfer_rate = RLOF_mass_transfer_rate(stardata,
                                                                      donor);

            Dprint("update masses etc. RLOF : rate = %g\n",mass_transfer_rate);
            RLOF_mass_transferred_in_one_orbit(stardata,
                                               mass_transfer_rate,
                                               RLOF_orbit);
            RLOF_orbit->dM_RLOF_transfer = -RLOF_orbit->dM_RLOF_lose;
            RLOF_orbit->dM_RLOF_accrete = RLOF_orbit->dM_RLOF_transfer;
            RLOF_dM_orbit_to_time_derivatives(stardata,RLOF_orbit);

            Dprint("RLOF in one orbit lose=%g transfer=%g accrete=%g (wind =%g, %g; M1=%g M2=%g sep=%g)\n",
                        RLOF_orbit->dM_RLOF_lose,
                        RLOF_orbit->dM_RLOF_transfer,
                        RLOF_orbit->dM_RLOF_accrete,
                        RLOF_orbit->dM_other[0],
                        RLOF_orbit->dM_other[1],
                        donor->mass,
                        accretor->mass,
                        stardata->common.orbit.separation
                );
            Safe_free(RLOF_orbit);
            Dprint("post-RLOF\n");
        }

        Dprint("Main loop: update_mass_spin_epoch dtm=%g\n",
               stardata->model.dtm);

        /* artificial mass accretion */
        artificial_mass_accretion_rates(stardata);

        /* limit accretion rates (also novae and updates stellar types) */
        limit_accretion_rates(stardata);
        Dprint("post-limit\n");

        /* Save wind accretion luminosities */
        set_wind_accretion_luminosities(stardata);
        Dprint("post-wind accretion luminosities\n");

        /* Calculate angular momentum and eccentricity derivatives */
        angular_momentum_and_eccentricity_derivatives(stardata,RLOF);
        Dprint("post angmom and ecc derivs\n");

    }
    else if(RLOF == TRUE && Is_zero(stardata->model.dtm))
    {
        Dprint("RLOF and dtm!=0\n");
        /*
         * If in RLOF, but with a timestep of zero (e.g. post-CE),
         * we still require the next timestep to be non-zero otherwise
         * the evolution algorithm will lock up.
         */
        stardata->model.dtm = RLOF_MINIMUM_TIMESTEP;
    }

    /*
     * Extra derivative calculations
     */
    Call_function_hook(extra_calculate_derivatives);

    /*
     * Apply evolution commands as set
     * in JSON file or string
     */
    apply_evolutionary_commands(stardata,TRUE,FALSE,FALSE);

    if(can_reject_and_shorten_timestep(stardata) == FALSE)
    {
        /*
         * Normally when derivatives give unphysical
         * values, we reject the evolution and try again
         * with a shorter timestep. When we cannot, we adjust
         * the derivatives, unphysically.
         *
         * We loop until there are no more failures, a maximum
         * of 10 times to avoid infinite loops.
         */
        Dprint("small deriv checks\n");
        unsigned int derivative_check_count = 0;
        while(derivative_checks(stardata) == TRUE &&
              derivative_check_count < 10)
        {
            derivative_check_count++;
        }
    }
#ifdef DISCS
    /*
     * Evolve discs
     */
    evolve_discs(stardata);
#endif
}
