#pragma once
#ifndef TIME_EVOLUTION_H
#define TIME_EVOLUTION_H

/*
 * Macros used in time_evolution.c
 */
//#define eprint(...) printf("%s",stardata->store->colours[MAGENTA]);printf(__VA_ARGS__);printf("M1=%g\n",stardata->star[0].phase_start_mass);printf("%s",stardata->store->colours[COLOUR_RESET]);fflush(stdout);
#define eprint(...) /* do nothing */

#define Modulate_solver_timestep(F)             \
    {                                           \
        stardata->model.dtm = dtm_in * (F);     \
        stardata->model.dt = dt_in * (F);       \
    }

#define Restore_solver_timestep                 \
    {                                           \
        stardata->model.dtm = dtm_in;           \
        stardata->model.dt = dt_in;             \
    }


#ifdef NEW_ADAPTIVE_RLOF_CODE
#define __X1 printf("%s FIRST pre-RLOF STEP %s\n",stardata->store->colours[RED],stardata->store->colours[COLOUR_RESET]);
#define __X2 printf("%s DONE FIRST STEP %s\n",stardata->store->colours[RED],stardata->store->colours[COLOUR_RESET]);
#else // NEW_ADAPTIVE_RLOF_CODE
#define __X1 /* do nothing */
#define __X2 /* do nothing */
#endif // NEW_ADAPTIVE_RLOF_CODE

#define Take_time_evolution_step(__UPDATE_TIME)                         \
    __extension__                                                       \
    ({                                                                  \
        int __retval;                                                   \
        if(stardata->evolving == TRUE &&                                \
           stardata->model.reject_shorten_timestep == REJECT_NONE)      \
        {                                                               \
            const Boolean require_convergence =                         \
                check_require_convergence_steps(stardata);              \
                                                                        \
            eprint("require convergence steps? %d\n",                   \
                   require_convergence);                                \
                                                                        \
            /* back up stardata in case of reject_same_timestep */      \
            struct stardata_t * this_stardata =                         \
                require_convergence == TRUE ?                           \
                New_stardata_from(stardata) : NULL;                     \
                                                                        \
            /* evolve */                                                \
            __X1;                                                       \
            eprint("call update_system_by_dt\n");                       \
            __retval = update_system_by_dt(stardata,                    \
                                           system_type,                 \
                                           (__UPDATE_TIME));            \
            eprint("retval from first step %d\n",                       \
                   __retval);                                           \
            __X2;                                                       \
                                                                        \
            /* check for rejection and rerun of this ("same") timestep */ \
            if(require_convergence == TRUE &&                           \
               stardata->model.reject_shorten_timestep ==               \
               REJECT_NONE)                                             \
            {                                                           \
                evolution_rejection_same_tests(stardata);               \
                                                                        \
                while(stardata->model.reject_same_timestep !=           \
                      REJECT_NONE)                                      \
                {                                                       \
                    eprint("Restore from previous\n");                  \
                    evolution_restore_from(stardata,                    \
                                           this_stardata,               \
                                           FALSE);                      \
                                                                        \
                    __retval = update_system_by_dt(                     \
                        stardata,                                       \
                        system_type,                                    \
                        (__UPDATE_TIME));                               \
                                                                        \
                    evolution_rejection_same_tests(stardata);           \
                }                                                       \
                                                                        \
                eprint("retval from step %d\n",__retval);               \
                Safe_free(stardata->model.restore_mask);                \
                Safe_free(stardata->model.restore_mask_contents);       \
            }                                                           \
            free_stardata(&this_stardata);                              \
        }                                                               \
        else                                                            \
        {                                                               \
            __retval = retval;                                          \
            eprint("rejected, require shorter timestep : retval from prev %d\n", \
                   __retval);                                           \
        }                                                               \
        (__retval);                                                     \
    })


#endif // TIME_EVOLUTION_H
