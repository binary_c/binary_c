#include "../binary_c.h"
No_empty_translation_unit_warning;


void post_evolution(struct stardata_t * Restrict stardata)
{
    /*
     * We are sent here after the evolution time loop has finished.
     */



    /*
     * Perhaps perform a stardata_dump
     */
    if(strlen(stardata->preferences->stardata_dump_filename))
    {
        dump_stardata(stardata,
                      stardata->preferences->stardata_dump_filename);
    }

#ifdef STELLAR_POPULATIONS_ENSEMBLE
    /*
     * Free the ensemble data if it's not deferred
     */
    if(stardata->preferences->ensemble_defer == FALSE)
    {
        free_ensemble_cdict(stardata);
    }
#endif//STELLAR_POPULATIONS_ENSEMBLE

}
