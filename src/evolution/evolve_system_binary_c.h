#ifndef EVOLVE_SYSTEM_BINARY_C_H
#define EVOLVE_SYSTEM_BINARY_C_H


/* eprint is the local debugging statement */
#define eprint(...)                                                 \
    fprintf(stdout,"%sZOOM dt_zoomfac %g : M1 %g %g : ",            \
            stardata->store->colours[YELLOW],                       \
            stardata->model.dt_zoomfac,                             \
            stardata->star[0].mass,                                 \
            stardata->star[0].phase_start_mass);                    \
    fprintf(stdout,__VA_ARGS__);                                    \
    fflush(stdout);                                                 \
    fprintf(stdout,"%s",stardata->store->colours[COLOUR_RESET]);
#undef eprint

#define eprint(...)                                             \
    printf("%s",stardata->store->ANSI_colours_table[GREEN]);    \
    printf(__VA_ARGS__);                                        \
    printf("%s",stardata->store->ANSI_colours_table[COLOUR_RESET]);
#undef eprint

#define eprint(...) Dprint(__VA_ARGS__); /* output if DEBUG */


#endif // EVOLVE_SYSTEM_BINARY_C_H
