#include "../binary_c.h"
No_empty_translation_unit_warning;


void evolution_forward_Euler(struct stardata_t * Restrict const stardata)
{
    /*
     * After the evolution loop - which can use any solver -
     * has finished, if we're not rejected, we are sent here.
     *
     * At this point the timestep
     * is the true timestep, and forward-Euler like changes to the
     * binary system can be performed.
     *
     * All algorithms which require a forward-Euler like solution
     * should run from here.
     *
     * These algorithms must NOT set the derivative arrays and expect
     * these changes to have any effect, because the apply_derivatives
     * functions are not called after this point.
     */

    /*
     * Yields and nucleosynthesis
     */
#ifdef NUCSYN
    evolution_nucsyn(stardata);
#endif // NUCSYN

#ifdef CIRCUMBINARY_DISK_DERMINE
    /* deprecated disc evolution */
    circumbinary_disk(stardata);
#endif // CIRCUMBINARY_DISK_DERMINE

    /*
     * Not-a-number checks
     */
    evolution_nanchecks(stardata);

}
