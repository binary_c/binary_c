#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "evolution_difflog.h"

/*
 * Compare stardata to previous_stardata, and log
 * if required.
 *
 * We do this by
 *
 * 1) setting a diffstats_t structure with data from the current and previous system
 * 2) use the diffstats_t structures to set some flags and prevflags
 * 3) when the flags are different from the prevflags, log
 *
 * Flags are indexed, the indices should be used through macros
 * defined in binary_c_macros.h (search for "labels used for logging").
 *
 * The "lockflags" are set to TRUE to indicate that the system
 * is in a particular state. This is useful to log things that are
 * not one-off events.
 */

void evolution_difflog(struct stardata_t * Restrict const stardata)
{
    Dprint("In difflog, stardata=%p, previous_stardata=%p (SN %d %d)\n",
           (void*)stardata,
           (void*)stardata->previous_stardata,
           stardata->star[0].SN_type,
           stardata->star[1].SN_type
        );

    /* never log when interpolating to find R=RL */
    if(stardata->model.intpol!=0) return;

    /* pointers to now and previous */
    struct diffstats_t * now;
    struct diffstats_t * prev = NULL;

    Boolean flags[NLOG_LABEL];

    Dprint("prev = %p\n",(void*)prev);
    /*
     * Set flags to FALSE
     */
    for(Log_index i=0;i<NLOG_LABEL;i++)
    {
        flags[i] = FALSE;
    }

    if(stardata->common.diffstats_set == FALSE)
    {
        /*
         * First timestep initialization
         */
        stardata->common.diffstats_set = TRUE;
        now = stardata->common.diffstats_now = &(stardata->common.diffstats[DIFFSTATS_NOW]);
        prev = stardata->common.diffstats_prev = now + DIFFSTATS_PREV;
        set_diffstats(stardata,now);

        now->roche_radius[0]=0.0;
        now->roche_radius[1]=0.0;
        memcpy(prev,now,sizeof(struct diffstats_t));

        /*
         * First timestep logging
         */
        flags[INITIAL_LABEL] = TRUE;
        Dprint("first timestep init\n");
    }
    else
    {
        /*
         * Copy old diffstats to "prev" struct
         */
        const Log_index i0 = (stardata->common.diffstats_now == &stardata->common.diffstats[DIFFSTATS_NOW]) ? DIFFSTATS_PREV : DIFFSTATS_NOW;
        now = stardata->common.diffstats_now = &(stardata->common.diffstats[i0]);
        prev = stardata->common.diffstats_prev = &(stardata->common.diffstats[1-i0]);

        /*
         * Set the current diffstats struct
         */
        Dprint("set diffstats %p %p\n",(void*)stardata,(void*)now);
        set_diffstats(stardata,now);
    }

    /*
     * Compare current and previous timesteps, setting
     * appropriate flags
     */
    const Boolean any_true =
        flags[INITIAL_LABEL] == FALSE
        ?  cf_diffstats(stardata,
                        now,
                        prev,
                        flags,
                        stardata->common.prevflags,
                        stardata->common.lockflags)
        : FALSE;
    Dprint("any true? %s\n",Yesno(any_true));

    if(stardata->preferences->vandenHeuvel_logging == TRUE)
    {
        vdh_log(stardata,
                now,
                prev,
                any_true,
                flags);
    }

    /*
     * Dependencies
     */
    Dprint("logging dependencies");
    logging_dependencies(stardata,
                         flags,
                         stardata->common.lockflags);

    /*
     * Do log and copy flags to prevflags
     */
    Dprint("do log");


    /*
     * set up the logstack in the tmpstore if we must
     */
    if(stardata->tmpstore->logstack == NULL)
    {
        stardata->tmpstore->logstack =
            new_difflogstack();
    }

    /*
     * Local pointer to the tmpstore stack
     */
    struct difflogstack_t * const logstack = stardata->tmpstore->logstack;
    Boolean setfalse[LOG_NFLAGS] = { FALSE };

    /* standard logstrings that use previous_stardata */
    for(Log_index i=0;i<LOG_NFLAGS;i++)
    {
        if(stardata->model.logstack[i] != LOG_NONE)
        {
            const Log_index j = stardata->model.logstack[i];
            if(stardata->model.logflags[j] == TRUE)
            {
                if(stardata->model.logflagstrings[j][0] == '0')
                {
                    /* use previous stardata */
                    push_stacklog(logstack,
                                  stardata->previous_stardata,
                                  stardata->model.logflagstrings[j]+1);
                }
                setfalse[j] = TRUE;
            }
        }
    }

    /* standard logstrings that use stardata */
    for(Log_index i=0;i<LOG_NFLAGS;i++)
    {
        if(stardata->model.logstack[i] != LOG_NONE)
        {
            const Log_index j = stardata->model.logstack[i];
            if(stardata->model.logflags[j] == TRUE)
            {
                if(stardata->model.logflagstrings[j][0]!='0')
                {
                    /* use current stardata */
                    push_stacklog(logstack,
                                  stardata,
                                  stardata->model.logflagstrings[j]);
                }
                setfalse[j] = TRUE;
            }
        }
    }

    /* clear already-logged flags */
    for(Log_index i=0;i<LOG_NFLAGS;i++)
    {
        if(setfalse[i] == TRUE)
        {
            const Log_index j = stardata->model.logstack[i];
            stardata->model.logflags[j]=FALSE;
            stardata->model.logflagstrings[j][0] = 0;
        }
    }

    /*
     * Do diff logging, put the results on the stacklog
     */
    for(Log_index i=0;i<NLOG_LABEL;i++)
    {
        if(flags[i] == TRUE)
        {
            push_stacklog(logstack,
                          stardata,
                          stardata->store->label[i]);
        }
        stardata->common.prevflags[i] = flags[i];
    }

    /*
     * Sort local log stack
     */
    void * arg = NULL;
    qsort_r((void*)logstack->items,
            (size_t)logstack->n,
            (size_t)sizeof(struct difflogitem_t *),
#ifdef __HAVE_GNU_QSORT_R
            (comparison_fn_r)compare_logitems,
            (void*)&arg
#endif // __HAVE_BSD_QSORT_R
#ifdef __HAVE_BSD_QSORT_R
            (void*)&arg,
            (comparison_fn_r)compare_logitems
#endif // __HAVE_GNU_QSORT_R
       );

    Safe_free(arg);

    /*
     * Dump local log stack.
     * The double loop is to ensure that the items are in order:
     * this is woefully inefficient if logstack->n is ever
     * large - in this case please rewrite the algorithm. Typically
     * n <~ 6 even in a complicated case, so currently it's not worth it.
     */
    {
        /*
         * First and last indices
         */
        int n0 = -1;
        for(Log_index i=0;i<logstack->n;i++)
        {
            for(Log_index j=0;j<logstack->n;j++)
            {
                if(i == logstack->items[j]->n)
                {
                    n0++;
                }
            }
        }

        int n = 0;
        for(Log_index i=0;i<logstack->n;i++)
        {
            for(Log_index j=0;j<logstack->n;j++)
            {
                if(i == logstack->items[j]->n)
                {
                    const char * const cc =
                        (stardata->preferences->log_groups == FALSE || n0 <= 0) ? " " : /* only one line */
                        n == 0 ? "╔" : /* first */
                        n == n0 ? "╚" : /* last */
                        "║";
                    n++;
                    Stack_debug_print("DOLOG %d (j=%d n=%d) %g %g : %s\n",
                                      i,
                                      j,
                                      logstack->items[j]->n,
                                      logstack->items[j]->m[0],
                                      logstack->items[j]->m[1],
                                      logstack->items[j]->string);
                    dolog(logstack->items[j]->stardata,
                          logstack->items[j]->string,
                          cc);
                }
            }
        }
    }

    clean_difflogstack(logstack);

    /* clear log stack and flags */
    for(Log_index i=0;i<LOG_NFLAGS;i++)
    {
        stardata->model.logstack[i] = LOG_NONE;
        stardata->model.logflags[i] = FALSE;
        stardata->model.logflagstrings[i][0] = '\0';
    }
}

#ifdef __HAVE_GNU_QSORT_R
static int compare_logitems(const void * Restrict a,
                            const void * Restrict b,
                            void * arg Maybe_unused)
#endif // __HAVE_GNU_QSORT_R
#ifdef __HAVE_BSD_QSORT_R
static int compare_logitems(void * arg Maybe_unused,
                            const void * Restrict a,
                            const void * Restrict b)
#endif // __HAVE_BSD_QSORT_R
{
    /*
     * Compare log items to determine their order
     *
     * itema and itemb are difflogitem_t structs, as defined
     * in evolution_difflog.h
     *
     * These are set in push_stacklog() [see below].
     *
     * You should compare them to determine whether to
     * return +1 or -1. You should not return 0:
     * in this case, the original order is preserved.
     *
     * For details of the sort, see the man page of qsort_r.
     */

    const struct difflogitem_t * const itema = * (struct difflogitem_t **) a;
    const struct difflogitem_t * const itemb = * (struct difflogitem_t **) b;
    Stack_debug_print("CF %p %p\n",(void*)itema,(void*)itemb);
    Stack_debug_print("%s\n%s\n",itema->string,itemb->string);
    Stack_debug_print("WITH M1 %g %g\n",itema->m[0],itemb->m[0]);
    Stack_debug_print("WITH M2 %g %g\n",itema->m[1],itemb->m[1]);

    int ret = 0;
    if(Fequal(itema->m[0], itemb->m[0]))
    {
        if(Fequal(itema->m[1], itemb->m[1]))
        {
            Stack_debug_print("both equal cf %d %d\n",itema->n,itemb->n);

            /* both equal : use n to preserve order */
            ret = itema->n > itemb->n ? +1 : -1;
        }
        else
        {
            /* use the secondary */
            Stack_debug_print("use secondary\n");
            ret = itema->m[1] > itemb->m[1] ? -1 : +1;
        }
    }
    else
    {
        /* use the primary */
        Stack_debug_print("use primary\n");
        ret = itema->m[0] > itema->m[1] ? -1 : +1;
    }

    /*
     * or the original order if nothing else
     */
    if(ret==0)
    {
        ret = itema->n > itemb->n ? +1 : -1;
    }

    Stack_debug_print("return %d : %s\n",
                      ret,
                      (ret == -1 ? itema->string :
                       ret == +1 ? itemb->string :
                       "unknown!"
                          )
        );
    return ret;
}

static void push_stacklog(struct difflogstack_t * const logstack,
                          struct stardata_t * const stardata,
                          const char * const cin)
{
    /*
     * Increase size of the stack: we double
     * it each time it exceeds the previous stack
     * to reduce the number of calls to Realloc.
     */
    if(1 + logstack->n > logstack->alloc_size)
    {
        logstack->alloc_size = Max(1+logstack->n,
                                   (size_t)(2*logstack->n));
        logstack->items =
            Realloc(logstack->items,
                    sizeof(struct difflogitem_t *)*
                    logstack->alloc_size);
    }

    /*
     * Allocate space for the new stack item
     */
    logstack->items[logstack->n] =
        Malloc(sizeof(struct difflogitem_t));

    Stack_debug_print("NEW LOGSTACK ITEM %p (list starts at %p)\n",
                      (void*)logstack->items[logstack->n],
                      (void*)logstack->items
        );

    /*
     * Shortcut to the new stack item
     */
    struct difflogitem_t * const new =  logstack->items[logstack->n];
    new->string = Malloc(sizeof(char)*STRING_LENGTH);

    /*
     * Set the new stack item information
     */
    Star_number k;
    Number_of_stars_Starloop(k)
    {
        new->m[k] = stardata->star[k].mass;
    }
    strlcpy(new->string,
            cin,
            STRING_LENGTH);
    new->n = logstack->n;
    new->stardata = stardata;

    Stack_debug_print("New string %d : %s\nM %g %g\n",
                      logstack->n,
                      new->string,
                      new->m[0],
                      new->m[1]
        );


    /*
     * Up the stack length
     */
    logstack->n++;
}


static void dolog(struct stardata_t * const Restrict stardata,
                  const char * const Restrict cin,
                  const char * const leadchar)
{
    logwrap(stardata,
            cin,
            leadchar);
}

static void set_diffstats(const struct stardata_t * const Restrict stardata,
                          struct diffstats_t * const Restrict now)
{
    Star_number k;
    now->artificial_accretion = FALSE;
    Number_of_stars_Starloop(k)
    {
        SETstar(k);
        now->mass[k] = star->mass;
        now->mc[k] = Outermost_core_mass(star);
        now->radius[k] = star->radius;
        now->roche_radius[k] = star->roche_radius;
        now->stellar_type[k] = star->stellar_type;
        now->hybrid_HeCOWD[k] = star->hybrid_HeCOWD;
        now->age[k] = star->age;
        now->tms[k] = star->tms;
        now->omega[k] = star->omega;
        now->omega_crit[k] = star->omega_crit;
        now->accretion_luminosity[k] = star->accretion_luminosity;
        now->luminosity[k] = star->luminosity;
        now->novarate[k] = star->derivative[DERIVATIVE_STELLAR_MASS_NOVA];
        now->nova[k] = star->nova;
        now->v_eq_ratio[k] = star->v_eq_ratio;
        now->SN_type[k] = star->SN_type;
        now->artificial_accretion =
            now->artificial_accretion == TRUE ? TRUE :
            Boolean_(Is_not_zero(stardata->star[k].derivative[DERIVATIVE_STELLAR_MASS_ARTIFICIAL]) ||
                     Is_not_zero(stardata->star[k].derivative[DERIVATIVE_STELLAR_ANGMOM_ARTIFICIAL]));
        now->overspin[k] = star->overspin;
    }

    now->time = stardata->model.time;
    now->orbital_separation = stardata->common.orbit.separation;
    now->orbital_period = stardata->common.orbit.period;
    now->orbital_eccentricity = stardata->common.orbit.eccentricity;
    now->orbital_angular_frequency = stardata->common.orbit.angular_frequency;
    now->sgl = stardata->model.sgl;
    if(now->artificial_accretion == FALSE)
        now->artificial_accretion = Is_not_zero(stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM_ARTIFICIAL]);
}

static Boolean cf_diffstats(struct stardata_t * Restrict const stardata,
                            const struct diffstats_t * const Restrict now,
                            const struct diffstats_t * const Restrict prev,
                            Boolean * Restrict const flags,
                            Boolean * Restrict const prevflags Maybe_unused,
                            Boolean lockflags[NUMBER_OF_STARS][LOCK_NFLAGS+1])
{
    Star_number k;
    Boolean now_rlof[NUMBER_OF_STARS],was_rlof[NUMBER_OF_STARS];

    const Boolean born_binary =
        WAS_A_STAR(0) &&
        WAS_A_STAR(1);
    const Boolean Maybe_unused system_is_binary =
        Boolean_(born_binary == TRUE &&
                 now->stellar_type[0] != MASSLESS_REMNANT &&
                 now->stellar_type[1] != MASSLESS_REMNANT &&
                 now->orbital_separation > TINY);
    const Boolean Maybe_unused system_was_binary =
        Boolean_(born_binary == TRUE &&
                 prev->stellar_type[0] != MASSLESS_REMNANT &&
                 prev->stellar_type[1] != MASSLESS_REMNANT &&
                 prev->orbital_separation > TINY);

    Number_of_stars_Starloop(k)
    {
        SETstar(k);

        /* stellar type change */
        if(now->stellar_type[k] != prev->stellar_type[k] ||
           now->hybrid_HeCOWD[k] != prev->hybrid_HeCOWD[k])
        {
            flags[TYPE_CHNGE_LABEL] = TRUE;
        }

        if(stardata->preferences->log_overspin_events == TRUE)
        {
            /* detect overspin */
            if(now->overspin[k] && !prev->overspin[k])
            {
                Append_logstring(LOG_OVERSPIN,
                                 "Start overspin star %d (M=%g Msun Ω=%g /yr Ω/Ω_crit=%g)",
                                 k,
                                 now->mass[k],
                                 now->omega[k],
                                 now->omega[k]/now->omega_crit[k]
                    );
            }
            else if(prev->overspin[k] && !now->overspin[k])
            {
                Append_logstring(LOG_OVERSPIN,
                                 "End overspin star %d (M=%g Msun Ω=%g /yr Ω/Ω_crit=%g)",
                                 k,
                                 now->mass[k],
                                 now->omega[k],
                                 now->omega[k]/now->omega_crit[k]
                    );
            }
        }

        /* detect RLOF */
        now_rlof[k] = RLOFing(now,k);
        was_rlof[k] = RLOFing(prev,k);

        if(now_rlof[k] && !was_rlof[k])
        {
            flags[k == 0 ? BEG_RCHE12_LABEL : BEG_RCHE_21_LABEL] = TRUE;

            /*
             * Extra logging for RLOF start
             */
            size_t nchars = 0;
            char ** chars = NULL;
#define _string(X,S,...) (X) ?                                          \
                __extension__                                           \
                ({                                                      \
                    chars = Realloc(chars,sizeof(char*)*(nchars+1));    \
                    if(asprintf(&chars[nchars],                         \
                                S ", "                                  \
                                Comma_arglist(__VA_ARGS__))==-1)        \
                    {                                                   \
                        chars[nchars]=NULL;                             \
                    }                                                   \
                    chars[nchars++];                                    \
                })                                                      \
                : ""

            RLOF_stars;
            const double qc = RLOF_critical_q(stardata);
            Append_logstring(LOG_ROCHE,
                             "Roche start: why? %s%s%s%s%s donor->q=%g qcrit=%g: %sstable\n",
                             _string(COMENV_GIANT_STAR,"is giant"),
                             _string(COMENV_SUBGIANT_STAR,"is subgiant (HG/CHeB)"),
                             _string(DYNAMICAL_TESTQ,"q=%g>qc=%g",donor->q,qc),
                             _string(COMENV_TESTR,"Min(R=%g,RL=%g)=%g<Rc=%g",donor->radius,donor->roche_radius,donor->effective_radius,donor->core_radius),
                             _string(COMENV_TEST_MENV,"Menv=%g>Menvmin=%g",envelope_mass(donor),stardata->preferences->minimum_donor_menv_for_comenv),
                             donor->q,
                             qc,
                             (donor->q > qc ? "un" : ""));
            for(size_t _i=0;_i<nchars;_i++)
            {
                Safe_free(chars[_i]);
            }
            Safe_free(chars);
        }
        else if(was_rlof[k] && !now_rlof[k])
        {
            flags[k == 0 ? END_RCHE12_LABEL : END_RCHE_21_LABEL] = TRUE;
        }

        /* detect artificial accretion */
        if(now->artificial_accretion == TRUE &&
           lockflags[k][LOCK_ARTIFICIAL_ACCRETION] == FALSE)
        {
            lockflags[k][LOCK_ARTIFICIAL_ACCRETION] = TRUE;
            flags[START_ARTIFICIAL_ACCRETION_LABEL] = TRUE;
        }
        else if(now->artificial_accretion == FALSE &&
                lockflags[k][LOCK_ARTIFICIAL_ACCRETION] == TRUE)
        {
            lockflags[k][LOCK_ARTIFICIAL_ACCRETION] = FALSE;
            flags[END_ARTIFICIAL_ACCRETION_LABEL] = TRUE;
        }

        /* blue stragglers */
        const Boolean blue_straggler =
            Boolean_(ON_MAIN_SEQUENCE(now->stellar_type[k]) &&
                     stardata->model.time - stardata->model.dtm > now->tms[k] &&
                     now->mass[k] > stardata->common.zero_age.mass[k] + BLUE_STRAGGLER_ACCRETED_MASS_THRESHOLD);

        if(blue_straggler == TRUE &&
           lockflags[k][LOCK_BSS] == FALSE)
        {
            lockflags[k][LOCK_BSS] = TRUE;
            flags[BEG_BSS_LABEL] = TRUE;
        }
        else if(blue_straggler == FALSE &&
                lockflags[k][LOCK_BSS] == TRUE)
        {
            lockflags[k][LOCK_BSS] = FALSE;
            flags[END_BSS_LABEL] = TRUE;
        }

        /* novae */
        {
            const Boolean novae =
                now->sgl == TRUE ? FALSE :
                (
                    stardata->preferences->individual_novae == TRUE ?

                    /*
                     * indiviudal novae algorithm:
                     * Novae are happening when there has been at least
                     * one explosion
                     */
                    Boolean_(now->nova[k] == NOVA_STATE_EXPLODING ||
                             now->nova[k] == NOVA_STATE_QUIESCENT) :
                    /*
                     * time-averaged novae (like BSE):
                     * Novae are happening when the nova mass derivative is
                     * negative (i.e. mass is being lost, on average).
                     */
                    Boolean_(now->novarate[k] < 0.0)
                    );

            if((novae == TRUE) &&
               lockflags[k][LOCK_NOVAE] == FALSE)
            {
                lockflags[k][LOCK_NOVAE] = TRUE;
                flags[BEG_NOVAE_LABEL] = TRUE;
            }
            else if((novae == FALSE) &&
                    lockflags[k][LOCK_NOVAE] == TRUE)
            {
                lockflags[k][LOCK_NOVAE] = FALSE;
                flags[END_NOVAE_LABEL] = TRUE;
            }
        }

        /* AGB star shrinkage */
        const Boolean agb = Boolean_(AGB(now->stellar_type[k]));
        if(agb == TRUE &&
           lockflags[k][LOCK_shrinkAGB] == FALSE &&
           AGB(prev->stellar_type[k]) &&
           now->radius[k] < prev->radius[k])
        {
            lockflags[k][LOCK_shrinkAGB] = TRUE;
            flags[SHRINK_AGB_LABEL] = TRUE;
        }
        else if(lockflags[k][LOCK_shrinkAGB] == TRUE &&
                !agb)
        {
            lockflags[k][LOCK_shrinkAGB] = FALSE;
        }

        /* symbiotics (k=accretor, Other_star(k)=donor) */
        {
            const Boolean symb = now->sgl == TRUE ? FALSE :
                Boolean_(now->accretion_luminosity[k] > 10.0 ||
                         now->accretion_luminosity[k] > now->luminosity[Other_star(k)]*0.01);
            if(symb == TRUE &&
               lockflags[k][LOCK_SYMBIOTIC] == FALSE)
            {
                lockflags[k][LOCK_SYMBIOTIC] = TRUE;
                flags[BEG_SYMB_LABEL] = TRUE;
            }
            else if(symb == FALSE &&
                    lockflags[k][LOCK_SYMBIOTIC] == TRUE)
            {
                lockflags[k][LOCK_SYMBIOTIC] = FALSE;
                flags[END_SYMB_LABEL] = TRUE;
            }
        }

#ifdef NUCSYN
        /*
         * Log stars by surface abundances
         */
        Abundance * const Xsurf =
            nucsyn_observed_surface_abundances(&stardata->star[k]);

        /*
         * Barium stars
         */
        Boolean Bastar;
        if(star->stellar_type > MAIN_SEQUENCE &&
           star->stellar_type <= TPAGB &&
           Outermost_core_mass(star) > 0.0 &&
           envelope_mass(star) > 0.05)
        {
            const Spectral_type stype = spectral_type(stardata,
                                                      &stardata->star[k]);

            if(stype==SPECTRAL_TYPE_G ||
               stype==SPECTRAL_TYPE_K)
            {
                const Abundance Bafe =
                    nucsyn_elemental_square_bracket("Ba",
                                                    "Fe",
                                                    Xsurf,
                                                    stardata->preferences->zero_age.XZAMS,
                                                    stardata);
                Bastar = Boolean_(Bafe > 0.5);
            }
            else
            {
                Bastar = FALSE;
            }
        }
        else
        {
            Bastar = FALSE;
        }

        if(Bastar == TRUE &&
           lockflags[k][LOCK_BARIUM_STAR] == FALSE)
        {
            lockflags[k][LOCK_BARIUM_STAR] = TRUE;
            flags[BEG_BARIUM_LABEL] = TRUE;
        }
        else if(Bastar == FALSE &&
                lockflags[k][LOCK_BARIUM_STAR] == TRUE)
        {
            lockflags[k][LOCK_BARIUM_STAR] = FALSE;
            flags[END_BARIUM_LABEL] = TRUE;
        }


        /*
         * Carbon stars
         */
        Boolean Cstar;
        if(NUCLEAR_BURNING(star->stellar_type) &&
           star->stellar_type < HeMS
            )
        {
            const Abundance C_O_ratio =
                nucsyn_elemental_abundance_ratio_by_number("C",
                                                           "O",
                                                           Xsurf,
                                                           stardata,
                                                           stardata->store);
            Cstar = Boolean_(C_O_ratio > 1.0);
        }
        else
        {
            Cstar = FALSE;
        }

        if(Cstar == TRUE &&
           lockflags[k][LOCK_CARBON_STAR1+k] == FALSE)
        {
            lockflags[k][LOCK_CARBON_STAR1+k] = TRUE;
            flags[BEG_CARBON1_LABEL+k] = TRUE;
        }
        else if(Cstar == FALSE &&
                lockflags[k][LOCK_CARBON_STAR1+k] == TRUE)
        {
            lockflags[k][LOCK_CARBON_STAR1+k] = FALSE;
            flags[END_CARBON1_LABEL+k] = TRUE;
        }

        /*
         * CEMP stars
         */
        Boolean CEMP;
        if(NUCLEAR_BURNING(star->stellar_type) &&
           star->stellar_type < HeMS)
        {
            const Abundance_ratio CFe =
                nucsyn_elemental_square_bracket("C",
                                                "Fe",
                                                Xsurf,
                                                stardata->preferences->zero_age.Xsolar,
                                                stardata);
            CEMP = Boolean_(CFe > 1.0 && Xsurf[XH1] > 0.6);
        }
        else
        {
            CEMP = FALSE;
        }

        if(CEMP == TRUE &&
           lockflags[k][LOCK_CEMP_STAR1+k] == FALSE)
        {
            lockflags[k][LOCK_CEMP_STAR1+k] = TRUE;
            flags[BEG_CEMP1_LABEL+k] = TRUE;
        }
        else if(CEMP == FALSE &&
                lockflags[k][LOCK_CEMP_STAR1+k] == TRUE)
        {
            lockflags[k][LOCK_CEMP_STAR1+k] = FALSE;
            flags[END_CEMP1_LABEL+k] = TRUE;
        }
#endif // NUCSYN

#ifdef LOG_ROTATION_STATES
        /* rapid rotators, v_eq / v_crit > 95% */
        {
            const Lock_index lockindex = LOCK_RAPID_ROTATOR1+k;
            const Boolean rapid_rotator = Boolean_(now->v_eq_ratio[k] >= 0.95);
            if(rapid_rotator == TRUE &&
               lockflags[k][lockindex] == FALSE)
            {
                flags[BEG_RAPID_ROTATOR1_LABEL+2*k] = TRUE;
                lockflags[k][lockindex] = TRUE;
            }
            else if(rapid_rotator == FALSE &&
                    lockflags[k][lockindex] == TRUE)
            {
                flags[END_RAPID_ROTATOR1_LABEL+2*k] = TRUE;
                lockflags[k][lockindex] = FALSE;
            }
        }

        /* medium rotators, 50% < v_eq / v_crit < 95% */
        {
            const Lock_index lockindex = LOCK_MEDIUM_ROTATOR1+k;
            const Boolean rapid_rotator = Boolean_(now->v_eq_ratio[k] > 0.5 &&
                                             now->v_eq_ratio[k] < 0.95);
            if(rapid_rotator == TRUE &&
               lockflags[k][lockindex] == FALSE)
            {
                flags[BEG_MEDIUM_ROTATOR1_LABEL+2*k] = TRUE;
                lockflags[k][lockindex] = TRUE;
            }
            else if(rapid_rotator == FALSE &&
                    lockflags[k][lockindex] == TRUE)
            {
                flags[END_MEDIUM_ROTATOR1_LABEL+2*k] = TRUE;
                lockflags[k][lockindex] = FALSE;
            }
        }
#endif // LOG_ROTATION_STATES

#ifdef LOG_TIDAL_LOCKING
        /* tidal locking : assume omega matching to within 10% is locked */
        {
            const Lock_index lockindex = LOCK_TIDAL_LOCK1+k;
            /* omega = yr^-1 */
            const Boolean tidally_locked =
                now->sgl == FALSE &&
                now->omega[k]/now->orbital_angular_frequency > 0.9 &&
                now->omega[k]/now->orbital_angular_frequency < 1.1;

            if(tidally_locked == TRUE &&
               lockflags[k][lockindex] == FALSE)
            {
                flags[BEG_TIDAL_LOCK1_LABEL+2*k] = TRUE;
                lockflags[k][lockindex] = TRUE;
            }
            else if(tidally_locked == FALSE &&
                    lockflags[k][lockindex] == TRUE)
            {
                flags[END_TIDAL_LOCK1_LABEL+2*k] = TRUE;
                lockflags[k][lockindex] = FALSE;
            }
        }
#endif // LOG_TIDAL_LOCKING

        /* mergers */
        if(now->stellar_type[Other_star(k)]==MASSLESS_REMNANT &&
           prev->stellar_type[Other_star(k)]!=MASSLESS_REMNANT &&
           now->mass[k] > prev->mass[k] &&
           Is_zero(now->mass[Other_star(k)]) && Is_not_zero(prev->mass[Other_star(k)]))
        {
            flags[COALESCE_LABEL]=TRUE;
        }

        /* core collapse supernovae */
        if(stardata->model.supernova)
        {
            flags[SN_LABEL] = TRUE;
        }
    }

    /* contact system */
    if((now_rlof[0] && now_rlof[1]) &&
       !(was_rlof[0] && was_rlof[1]))
    {
        flags[CONTACT_LABEL] = TRUE;
    }

    /* mass ratio inversion */
    if(system_is_binary &&
       now->stellar_type[0]!=MASSLESS_REMNANT &&
       now->stellar_type[1]!=MASSLESS_REMNANT &&
       ((now->mass[0]/Max(1e-14,now->mass[1]) < 1.0 &&
         prev->mass[0]/Max(1e-14,prev->mass[1]) > 1.0) ||
        (now->mass[0]/Max(1e-14,now->mass[1]) > 1.0 &&
         prev->mass[0]/Max(1e-14,prev->mass[1]) < 1.0)))
    {
        flags[Q_INV_LABEL] = TRUE;
    }


    /* circularization */
    if(system_is_binary &&
       Is_zero(now->orbital_eccentricity) &&
       !Is_zero(prev->orbital_eccentricity) &&
       Is_zero(stardata->model.derivative[DERIVATIVE_ORBIT_ECCENTRICITY]))
    {
        flags[CIRCULARIZED_LABEL] = TRUE;
    }

    /* mergers and/or disruption */
    if(born_binary &&
       system_was_binary == TRUE &&
       system_is_binary == FALSE &&
       Less_or_equal(now->orbital_separation,0.0) &&
       prev->orbital_separation > 0.0)
    {
        if(More_or_equal(prev->radius[0]/Max(1e-14,prev->roche_radius[0]),1.0) &&
           More_or_equal(prev->radius[1]/Max(1e-14,prev->roche_radius[1]),1.0))
        {
            flags[COALESCE_LABEL]=TRUE;
        }
        else if(flags[COALESCE_LABEL] == FALSE &&
                NEITHER_STAR_MASSLESS &&
                now->orbital_eccentricity<0.0 &&
                More_or_equal(prev->orbital_eccentricity,0.0))
        {
            flags[DISRUPT_LABEL]=TRUE;
        }
    }

    /* Cases of total disintegration */
    if(now->stellar_type[0]==MASSLESS_REMNANT &&
       now->stellar_type[1]==MASSLESS_REMNANT &&
       lockflags[0][LOCK_NO_REMNANT] == FALSE)

    {
        flags[NO_REMNT_LABEL] = TRUE;
        lockflags[0][LOCK_NO_REMNANT] = TRUE;
    }

    /* both stars off MS */
    if(now->stellar_type[0]>MAIN_SEQUENCE &&
       now->stellar_type[1]>MAIN_SEQUENCE &&
       lockflags[0][LOCK_BOTH_POST_MS] == FALSE)
    {
        flags[OFF_MS_LABEL] = TRUE;
        lockflags[0][LOCK_BOTH_POST_MS] = TRUE;
    }

    /* time evolution finished ? */
    if(check_for_time_exhaustion(stardata,0) == TRUE &&
       stardata->preferences->disable_end_logging == FALSE)
    {
        flags[MAX_TIME_LABEL] = TRUE;
    }

    Boolean any_true = FALSE;
    for(Log_index i=0;i<NLOG_LABEL;i++)
    {
        if(flags[i] == TRUE)
        {
            any_true = TRUE;
            break;
        }
    }
    return any_true;
}

static void logging_dependencies(struct stardata_t * const stardata,
                                 Boolean * const flags Maybe_unused,
                                 Boolean lockflags[NUMBER_OF_STARS][LOCK_NFLAGS+1])
{
    /*
     * Set flags in stardata (i.e. the stars and common structs)
     * which are detected above.
     * These can then be used elsewhere that stardata is available.
     */
    Star_number k;
    Number_of_stars_Starloop(k)
    {
        SETstar(k);
        star->blue_straggler = lockflags[k][LOCK_BSS];
    }
}


static void clean_difflogstack(struct difflogstack_t * const logstack)
{
    /*
     * Free the strings stored in the logstack
     */
    for(Log_index i=0;i<logstack->n;i++)
    {
        Safe_free(logstack->items[i]->string);
        Safe_free(logstack->items[i]);
    }
    logstack->n = 0;
}

static struct difflogstack_t * new_difflogstack(void)
{
    /*
     * Make a new local logstack
     */
    struct difflogstack_t * logstack = Malloc(sizeof(struct difflogstack_t));
    logstack->alloc_size = logstack->n = 0;
    logstack->items = NULL;
    return logstack;
}

static void vdh_log(struct stardata_t * const Restrict stardata,
                    struct diffstats_t * Restrict const now Maybe_unused,
                    struct diffstats_t * Restrict const prev Maybe_unused,
                    const Boolean any_true,
                    Boolean * Restrict const flags Maybe_unused)
{
    /*
     * Log for van den Heuvel diagrams
     */
    if(stardata->model.model_number == 0)

    {
        Printf("VDH TIME BINARY M1 M2 STELLAR_TYPE1 STELLAR_TYPE2 SEP PER ECC R1 R2 R1/ROL1 R2/ROL2 TEFF1 TEFF2 L1 L2 RLOF_DONOR1 RLOF_DONOR2\n");
    }
    if(any_true == TRUE)
    {
        const Boolean born_binary =
            WAS_A_STAR(0) &&
            WAS_A_STAR(1);
        const Boolean Maybe_unused system_is_binary =
            Boolean_(born_binary == TRUE &&
                     NEITHER_STAR_MASSLESS &&
                     stardata->common.orbit.separation > TINY);
        Printf("VDH %g %d %g %g %d %d %g %g %g %g %g %g %g %g %g %g %g %d %d\n",
               stardata->model.time,
               system_is_binary,
               stardata->star[0].mass,
               stardata->star[1].mass,
               stardata->star[0].stellar_type,
               stardata->star[1].stellar_type,
               stardata->common.orbit.separation,
               stardata->common.orbit.period,
               stardata->common.orbit.eccentricity,
               stardata->star[0].radius,
               stardata->star[1].radius,
               stardata->star[0].radius / stardata->star[0].roche_radius,
               stardata->star[1].radius / stardata->star[1].roche_radius,
               Teff(0),
               Teff(1),
               stardata->star[0].luminosity,
               stardata->star[1].luminosity,
               Is_not_zero(stardata->star[0].derivative[DERIVATIVE_STELLAR_MASS_RLOF_LOSS]),
               Is_not_zero(stardata->star[1].derivative[DERIVATIVE_STELLAR_MASS_RLOF_LOSS])
            );
    }
}
