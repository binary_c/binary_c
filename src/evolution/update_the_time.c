#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Update the time by adding the timestep
 * (NB use dtm because time is in megayears)
 */
void update_the_time(struct stardata_t * Restrict const stardata)
{
    stardata->model.time   +=
#ifdef REVERSE_TIME
        (stardata->preferences->reverse_time == FALSE ? +1.0 : -1.0) *
#endif // REVERSE_TIME
        stardata->model.dtm;

    if(stardata->preferences->timestep_logging == TRUE)
    {
        printf("%sincrease time by dtm=%30.12e to %30.12e (log=%30.20g)%s\n",
               stardata->store->colours[GREEN],
               stardata->model.dtm,
               stardata->model.time,
               log10(Max(1e-99,stardata->model.time)),
               stardata->store->colours[COLOUR_RESET]);
    }
}
