#include "../binary_c.h"
No_empty_translation_unit_warning;


Boolean to_loop_or_not_to_loop(struct stardata_t * Restrict const stardata)
{
    return Boolean_(++stardata->model.iter<MAX_EVOLUTION_NUM_LOOPS);
}
