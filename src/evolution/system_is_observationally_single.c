#include "../binary_c.h"
No_empty_translation_unit_warning;


Boolean Pure_function_if_no_debug system_is_observationally_single(struct stardata_t * Restrict const stardata)
{
    /*
     * Is a system single (TRUE) or binary (FALSE) in
     * an observational sense.
     */
    const Boolean single =
        System_is_single ||
        // very low mass -> not a star
        stardata->star[1].mass < MIN_OBSERVATIONAL_STELLAR_MASS ||
        // at very long period, star will be 'apparently single'
        stardata->common.orbit.period >  MAX_OBSERVATIONAL_BINARY_ORBITAL_PERIOD
        ;

    Dprint("system_is_observationally_single sgl %d : MLR1 %d : MLR2 %d  : M2<0.01 %d : P>long %d : a<0 %d : e<0 %d : single ? %d\n",
           stardata->model.sgl,
           stardata->star[0].stellar_type==MASSLESS_REMNANT ,
           stardata->star[1].stellar_type==MASSLESS_REMNANT ,
           stardata->star[1].mass < 0.010001 ,
           stardata->common.orbit.period > 1e7*YEAR_LENGTH_IN_DAYS ,
           stardata->common.orbit.separation < 0.0 ,
           stardata->common.orbit.eccentricity < 0.0,
           single);

    return single;
}
