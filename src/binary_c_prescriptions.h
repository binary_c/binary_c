#pragma once
#ifndef BINARY_C_PRESCRIPTIONS_H
#define BINARY_C_PRESCRIPTIONS_H
/*
 * The binary_c stellar population nucleosynthesis framework.
 *
 * Copyright Robert Izzard 2000-2025
 *
 * Contact rob.izzard@gmail.com
 *
 * Code repository:
 * https://gitlab.com/binary_c
 *
 * Documentation is in doc/
 * Code is in src/
 *
 * To build, run
 *
 * ---
 *
 * meson setup builddir
 * cd builddir
 * ninja binary_c_install
 *
 * ---
 *
 * Please see the files README, LICENCE and CHANGES,
 * and the doc/ directory for documentation which is mirrored
 * at https://binary_c.gitlab.io
 *
 **********************
 *
 * Purpose:
 *
 * Header file to load in the various prescriptions
 * used in binary_c
 */

#include "binary_c_stellar_cores.h"

#include "batchmode/batchmode.h"
#include "binary_star_functions/binary_star_functions_prescriptions.h"
#include "buffering/buffering.h"
#include "common_envelope/common_envelope_prescriptions.h"
#include "evolution/evolution_parameters.h"
#include "file/file_prescriptions.h"
#include "logging/logging_parameters.h"
#include "maths/bisect_parameters.h"
#include "memory/memory_parameters.h"
#include "novae/nova_parameters.h"
#include "opacity/opacity_prescriptions.h"
#include "RLOF/RLOF_parameters.h"
#include "rotation/rotation_parameters.h"
#include "single_star_functions/AGB_parameters.h"
#include "single_star_functions/magnetic_braking_parameters.h"
#include "single_star_functions/planetary_nebulae.h"
#include "single_star_functions/white_dwarf_parameters.h"
#include "stellar_timescales/stellar_timescales.h"
#include "stellar_structure/stellar_structure_prescriptions.h"
#include "spectra/spectral_types.h"
#include "supernovae/bh_prescriptions.h"
#include "timestep/timestep_parameters.h"
#include "supernovae/bh_prescriptions.h"
#include "wind/wind_prescriptions.h"

#endif // BINARY_C_PRESCRIPTIONS_H
