#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Erase all cores in a star below the given core
 */
void set_no_core_below(struct star_t * const star,
                       Core_type j)
{
    const double m = star->core_mass[j];
    Core_type i = 0;
    while(i<NUMBER_OF_CORES)
    {
        if(star->core_mass[i] < m)
        {
            star->core_mass[i] = 0.0;
        }
        i++;
    }
}
