#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Limit the mass in all the cores
 */
void trim_cores(struct star_t * const star,
                const double m)
{
    Core_type i = 0;
    while(i<NUMBER_OF_CORES)
    {
        star->core_mass[i] = Min(star->core_mass[i], m);
        i++;
    }
}
