#include "../binary_c.h"
No_empty_translation_unit_warning;

double * baryonic_core_masses(const struct star_t * const star)
{
    /*
     * Given a star, estimate its cores' baryonic masses.
     *
     * Returns a list of cores masses that needs to be freed
     * by the caller.
     */
    double * mcores = Calloc(NUMBER_OF_CORES,sizeof(double));
    double offset = 0.0;
    if(star->stellar_type != MASSLESS_REMNANT)
    {
        for(int i=NUMBER_OF_CORES-1; i>=0; i--)
        {
            if(Is_not_zero(star->core_mass[i]))
            {
                if(i==CORE_NEUTRON || i==CORE_BLACK_HOLE)
                {
                    /*
                     * This computation assumes we cannot have
                     * a black hole core inside a neutron core.
                     */
                    offset = baryonic_mass_offset(star);
                }
                mcores[i] = star->core_mass[i] + offset;
            }
        }
    }
    return mcores;
}
