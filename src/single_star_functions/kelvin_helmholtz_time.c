#include "../binary_c.h"
No_empty_translation_unit_warning;



/*
 * Return Kelvin-Helmholtz time for the star
 * (Hurley et al 2002, Eq.61) in years
 */

Time Pure_function kelvin_helmholtz_time(struct star_t * Restrict star)
{
    Time tkh;
    if(
        unlikely(star->stellar_type == MASSLESS_REMNANT ||
                 star->stellar_type == BLACK_HOLE ||
                 Is_zero(star->luminosity) ||
                 Is_zero(star->radius))
        )
    {
        tkh = 1e50;
    }
    else
    {
        const double menv = (star->mass -
                             ((ON_EITHER_MAIN_SEQUENCE(star->stellar_type)||
                               LATER_THAN_WHITE_DWARF(star->stellar_type)) ?
                              0.0 : Outermost_core_mass(star)));
        tkh = kelvin_helmholtz_time_from_LMR(star->luminosity,
                                             star->mass,
                                             menv,
                                             star->radius);
    }
    return tkh;
}

Time Pure_function kelvin_helmholtz_time_from_LMR(const double L,
                                                  const double M,
                                                  const double Menv,
                                                  const double R)
{
    const double LR = L*R;
    return (unlikely(Is_zero(LR))) ? 1e50 : (1.0e7 * M / (LR) * Menv);
}
