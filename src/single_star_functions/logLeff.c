#include "../binary_c.h"
No_empty_translation_unit_warning;


double Pure_function logLeff(const struct star_t * Restrict const star)
{
    /* 
     * calculate "logLeff" for a star
     * (thanks to Norberto Castro for his indecipherable expression :)
     *
     * The final constant is -10.6076 == -log10((Pow4(5770.0)/2.736e4)); 
     */    
    return log10(Pow4(Teff_from_star_struct(star))/(TINY+GRAVITATIONAL_CONSTANT*M_SUN*star->mass/Pow2(star->radius*R_SUN))) -10.6076;

}
