#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "../binary_c_stellar_parameters.h" /* for WR star data */

#ifdef NUCSYN

double mattsson_massloss(double mass,
                         double phase_start_mass,
                         double luminosity,
                         double teff,
                         double xc12,
                         double xc13,
                         double xo16,
                         double xh_initial,Boolean *fail);
#endif // NUCSYN


double rotational_factor(const double mt,
                         const double radius,
                         const double luminosity,
                         struct star_t *star,
                         unsigned int useme);

double calc_stellar_wind_mass_loss(const Stellar_type stellar_type,
                                   const double luminosity,
                                   const double radius,
                                   const double mt,/* current mass of star */
                                   const double roche_lobe_radius,
                                   const double metallicity,
                                   const double omega,/* Spin! */
                                   struct stardata_t * Restrict const stardata,
                                   const Star_number k /* The star number */)

{
    SETstar(k);
    const double mdot = wind_mass_loss_rate(mt,
                                            luminosity,
                                            radius,
                                            roche_lobe_radius,
                                            metallicity,
                                            omega,
                                            stellar_type,
                                            star,
                                            stardata);
    return mdot;
}





#ifdef MATTSSON_MASS_LOSS


#define IMAX 41
#define JMAX 31
#define KMAX 19

void mattsson_readcubes(
    double x[5][IMAX+1][JMAX+1][KMAX+1],
    double *m,double *Lmin,double *Lmax,
    double *Tmin,double *Tmax,double *Cmin,double *Cmax);


double mattsson_massloss_lookup(
    double x[5][IMAX+1][JMAX+1][KMAX+1],
    double *m,
    double *Lmin,
    double *Lmax,
    double Cmin,
    double Cmax,
    double Tmin,
    double Tmax,
    double mass,
    double mi,
    double L,
    double t,
    double xc12,
    double xc13,
    double xo16,
    double xhin,
    int *lf,
    int *cf);

double mattsson_massloss(double mass,
                         double phase_start_mass,
                         double luminosity,
                         double teff,
                         double xc12,
                         double xc13,
                         double xo16,
                         double xh_initial,
                         Boolean *fail)
{
    // all arrays should be reduced by 1 ! (fortran numbers :(
    double  x[5][IMAX+1][JMAX+1][KMAX+1];
    double  Lmin[5],Lmax[5],m[5];
    double  Tmin,Tmax,Cmin,Cmax;
    Boolean  set_up=FALSE;
    double massloss;
    int lf,cf;

    if(set_up==FALSE)
    {
        /* load data the first time */
        mattsson_readcubes(x,m,Lmin,Lmax,&Tmin,&Tmax,&Cmin,&Cmax);
        set_up=TRUE;
    }

    /* call the function to calculate the mass-loss rate */
    massloss=mattsson_massloss_lookup(x,m,Lmin,Lmax,Cmin,Cmax,Tmin,Tmax,
                                      mass,
                                      phase_start_mass,
                                      log10(luminosity),
                                      teff,xc12,xc13,xo16,xh_initial,
                                      &lf,&cf);

    if((cf<0)||(lf<0))
    {
        *fail=TRUE;
    }
    else
    {
        *fail=FALSE;
    }

    fprintf(stderr,"Mattsson: lookup mass=%g minit=%g L=%g teff=%g c12=%g c13=%g o16=%g xh=%g -> massloss=%g (cf=%d lf=%d)\n",
            mass,
            phase_start_mass,
            luminosity,
            teff,xc12,xc13,xo16,xh_initial,
            massloss,cf,lf);

    return(massloss);
}

void mattsson_readcubes(//double *x,
    double x[5][IMAX+1][JMAX+1][KMAX+1],
    double *m,double *Lmin,double *Lmax,
    double *Tmin,double *Tmax,double *Cmin,double *Cmax
    )
{
    /*
      C-----------------------------------------------------------------------
      C     Read-in variables:
      C
      C     m[1]    : Stellar mass 1 (0.75 Msun)
      C     m[2]    : Stellar mass 2 (1.00 Msun)
      C     m[3]    : Stellar mass 3 (1.50 Msun)
      C     m[4]    : Stellar mass 4 (2.00 Msun)
      C
      C     X[1-4]      : Mass-loss data cube for stellar mass 1-4
      C
      C     Tmin    : Minimum effective temperature (2400K)
      C     Tmax    : Maximum effective temperature (3200K)
      C
      C     Lmin[X] : Minimum luminosity for stellar mass [X] in grid
      C     Lmax[X] : Maximum luminosity for stellar mass [X] in grid
      C
      C     Cmin    : Minimum free carbon abundance in grid
      C     Cmax    : Maximum free carbon abundance in grid
      C
      C-----------------------------------------------------------------------
    */

    /* read data cubes */
    FILE *fp=fopen("/home/izzard/progs/stars/binary_c/mattsson/igrid_u4_fort.dat","r");
    int i,j,k,z;

    if(fp==NULL)
    {
        fprintf(stderr,"Could not open Mattsson's mass-loss data\n");
    }
    else
    {
        Dprint("Mattsson file open ok\n");
    }

    fscanf(fp,"%lg %lg %lg %lg %lg %lg",m+1,m+2,m+3,m+4,Tmin,Tmax);
    fscanf(fp,"%lg %lg %lg %lg %lg %lg %lg %lg %lg %lg",Lmin+1,Lmax+1,
           Lmin+2,Lmax+2,Lmin+3,Lmax+3,Lmin+4,Lmax+4,Cmin,Cmax);

    for(z=1;z<5;z++)
    {
        for(k=1;k<=KMAX;k++)
        {
            for(j=1;j<=JMAX;j++)
            {
                for(i=1;i<=IMAX;i++)
                {
                    fscanf(fp,"%lg",&(x[z][i][j][k]));
                }
            }
        }
    }
    fclose(fp);
}

double mattsson_massloss_lookup(
    double x[5][IMAX+1][JMAX+1][KMAX+1],
    double *m,
    double *Lmin,
    double *Lmax,
    double Cmin,
    double Cmax,
    double Tmin,
    double Tmax,
    double mass,
    double mi,
    double L,
    double t,
    double xc12,
    double xc13,
    double xo16,
    double xhin,
    int *lf,
    int *cf)
{

    double c,mn,ln,tn,cn,xdif,mdif,htot,ctot,otot;
    const double auh1=1.00782503;
    const double auc12=12.0,auc13=13.00335483,auo16=15.99491462;
    double tmp,massloss;
    int i,k,jh,jl;

    *lf=0;
    *cf=0;

    htot = xhin/auh1;
    ctot = xc12/auc12+xc13/auc13;
    otot = xo16/auo16;

    // this is for the fortran routine (testing)
    //printf("    L=%g;MI=%g;M=%g;T=%g\n          XC12=%g;XC13=%g;XO16=%g;XH=%g\n",L,mi,mass,t,xc12,xc13,xo16,xhin);

    if(ctot>otot)
    {
        c = log10((ctot-otot)/htot)+12.0;
    }
    else
    {
        c = 0.0;
    }

    fprintf(stderr,"Mattsson massloss: C=%g O=%g H=%g -> c=%g Cmin=%g\n",ctot,otot,htot,c,Cmin);

    if(c<Cmin)
    {
        /*
          C If C < Cmin, use other mass-loss prescription:
          C-----------------------------------------------------------------------
          C REMARK: HERE YOU MAY INSERT ANY FORMULA YOU LIKE, E.G., THE BLÖCKER-
          C         LAW, OR A LOWER LIMIT FOR THE MASS-LOSS RATE.
          C-----------------------------------------------------------------------
          * Rob says: indeed, we return, set the 'fail' flag and use
          * the backup wind (default VW93) instead
          *
          */
        massloss=1e-9;
        *cf=-1;
    }
    else
    {
        if(mass<m[1])
        {
            mn=m[1];
        }
        else
        {
            mn=mass;
        }

        if(c>Cmax)
        {
            cn=Cmax;
        }
        else
        {
            cn=c;
        }

        if(t<Tmin)
        {
            tn=Tmin;
        }
        else if(t>Tmax)
        {
            tn=Tmax;
        }
        else
        {
            tn=t;
        }

        /*
          C-----------------------------------------------------------------------
          C REMARK: MASS-LOSS RATE IS ASSUMED TO SCALE LINEARLY WITH LUMINOSITY.
          C         THIS MAY NOT BE APPROPRIATE FOR LUMINOSITIES THAT ARE MUCH
          C         HIGHER OR LOWER THAN Lmax and Lmin!!!
          C-----------------------------------------------------------------------
        */
        if((Fequal(mn,m[1])) && (L<Lmin[1]))
        {
            ln = Lmin[1];
        }
        else if(Fequal(mn,m[1]) && (L>Lmax[1]))
        {
            ln = Lmax[1];
        }
        else if  ((mn>m[1]) && (Less_or_equal(mn,m[2])) && (L<Lmin[2]))
        {
            ln = Lmin[2];
        }
        else if((mn>m[1]) && (Less_or_equal(mn,m[2])) && (L>Lmax[2]))
        {
            ln = Lmax[2];
        }
        else if((mn>m[2]) && (Less_or_equal(mn,m[3])) && (L<Lmin[3]))
        {
            ln = Lmin[3];
        }
        else if((mn>m[2]) && (Less_or_equal(mn,m[3])) && (L>Lmax[3]))
        {
            ln = Lmax[3];
        }
        else if((mn>m[3]) && (Less_or_equal(mn,m[4])) && (L<Lmin[4]))
        {
            ln = Lmin[4];
        }
        else if((mn>m[3]) && (Less_or_equal(mn,m[4])) && (L>Lmax[4]))
        {
            ln = Lmax[4];
        }
        else
        {
            ln = L;
        }

        /*
            C Find the nearest point in the data cubes:
            C-----------------------------------------------------------------------
        */

        i = (int)(40*(tn-Tmin)/(Tmax-Tmin)+0.5)+1;
        k = (int)(18*(cn-Cmin)/(Cmax-Cmin)+0.5)+1;

        // set to silly values : check later
        jl = 666;
        jh = 666;

        if(Fequal(mn,m[1]))
        {
            jl = 0; // very dodgy
            jh = (int)(0.5+30*(ln-Lmin[1])/(Lmax[1]-Lmin[1]))+1;
        }
        else if((mn>m[1]) && (Less_or_equal(mn,m[2])))
        {
            jh = (int)(0.5+30*(ln-Lmin[2])/(Lmax[2]-Lmin[2]))+1;
            jl = (int)(0.5+30*(ln-Lmin[1])/(Lmax[1]-Lmin[1]))+1;
        }
        else if((mn>m[2]) && (Less_or_equal(mn,m[3])))
        {
            jh = (int)(0.5+30*(ln-Lmin[3])/(Lmax[3]-Lmin[3]))+1;
            jl = (int)(0.5+30*(ln-Lmin[2])/(Lmax[2]-Lmin[2]))+1;
        }
        else if((mn>m[3]) && (Less_or_equal(mn,m[4])))
        {
            jh = (int)(0.5+30*(ln-Lmin[4])/(Lmax[4]-Lmin[4]))+1;
            jl = (int)(0.5+30*(ln-Lmin[3])/(Lmax[3]-Lmin[3]))+1;
        }
        /* added an else for use when M>m[4] */
        else
        {
            jh = JMAX;
            jl = JMAX;
        }

        /* check for bad indices */
        if((jh==666)||(jl==666))
        {
            Exit_binary_c(BINARY_C_OUT_OF_RANGE,
                          "Mattsson 666 error (off data cube! jh=%d jl=%d)\n",
                          jh,jl);
        }

        /* calculated always */
        tmp = exp10(L-ln);

        if((mn>m[1]) && (mn<m[2]))
        {
            mdif = m[2]-m[1];
            xdif = x[2][i][jh][k] - x[1][i][jl][k];
            massloss    = tmp*x[1][i][jl][k]+(mn-m[1])*xdif/mdif;
        }
        else if((mn>m[2]) && (mn<m[3]))
        {

            mdif = m[3]-m[2];
            xdif = x[3][i][jh][k]-x[2][i][jl][k];
            massloss = tmp*x[2][i][jl][k]+(mn-m[2])*xdif/mdif;
        }
        else if((mn>m[3]) && (mn<m[4]))
        {
            mdif = m[4]-m[3];
            xdif = x[4][i][jh][k]-x[3][i][jl][k];
            massloss = tmp*x[3][i][jl][k]+(mn-m[3])*xdif/mdif;
        }
        else if(Fequal(mn,m[1]))
        {
            massloss = tmp*x[1][i][jh][k];
        }
        else if(Fequal(mn,m[2]))
        {
            massloss    = tmp*x[2][i][jh][k];
        }
        else if(Fequal(mn,m[3]))
        {
            massloss    = tmp*x[3][i][jh][k];
        }
        else if(More_or_equal(mn,m[4]))
        {
            massloss    = tmp*x[4][i][jh][k];
        }
        else
        {
            Exit_binary_c(BINARY_C_ALGORITHM_BRANCH_FAILURE,
                          "mattsson wind loss oops : should never get to the end of the ifs\n");
        }

        if(L>4.30) *lf = -1;

        /*
         * As the star moves off the AGB it becomes blue and the mass-loss rate
         * drops: we don't want this to happen so make sure mass-loss continues
         */
        if((massloss<TINY)&&(t>3200)) massloss=1e-7;
    }
    return (massloss);
}


#endif // MATTSSON_MASS_LOSS
