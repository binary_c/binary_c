#include "../binary_c.h"
No_empty_translation_unit_warning;

double inner_core_mass(struct stardata_t * const stardata Maybe_unused,
                       struct star_t * const star,
                       Core_type i)
{
    /*
     * Return the core mass by checking types
     * >=i
     */
    double mc = 0.0;
    for(int j=i; j<NUMBER_OF_CORES; j++)
    {
        mc = Max(star->core_mass[j], mc);
    }
    return mc;
}
