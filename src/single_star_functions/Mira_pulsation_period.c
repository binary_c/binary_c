#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * "Mira"-like pulsation period for red giants,
 * from Vassiliadis and Wood (1993).
 *
 * Value returned is in days
 */

double Mira_pulsation_period(const double mass,
                             const double radius,
                             struct stardata_t * stardata)
{
    /* calculate Mira pulsation period */
    double p0 = -2.07 - 0.90*log10(mass) + 1.940 * log10(radius);
    p0 = exp10(p0);

    /* Limit p0 (radius diverges at low Menv) */
    p0 = Min(p0,2000.0);

#ifdef VW93_MIRA_SHIFT
    /* artificial shift VW93 Mira period */
    p0 = Max(p0+stardata->preferences->vw93_mira_shift,0.0);

    Dprint("Mira pulsation period (M=%g, R=%g, shift=%g) = %g\n",mass,radius,stardata->preferences->vw93_mira_shift,p0);
#else
    Dprint("Mira pulsation period (M=%g, R=%g) = %g\n",mass,radius,p0);
#endif // VW93_MIRA_SHIFT

    return p0;
}
