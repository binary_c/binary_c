#include "../binary_c.h"
No_empty_translation_unit_warning;


double radius_mass_derivative(struct stardata_t * const stardata,
                              struct star_t * const star,
                              const double mass)

{
    /* calculate (partial derivative) dr/dm */
    double drdm;
    const double dm = -1e-6 * mass;
    struct star_t * newstar = New_star_from(star);
    update_mass(stardata,newstar,dm);
    newstar->phase_start_mass = newstar->stellar_type<HERTZSPRUNG_GAP ?
        newstar->mass : newstar->phase_start_mass;
    stellar_structure(stardata,
                      STELLAR_STRUCTURE_CALLER_RLOF_mass_transfer_rate,
                      newstar,
                      NULL,
                      FALSE);

    drdm = (newstar->radius - star->radius)/dm;
    Dprint("DRDM : newR=%15.12g wasR=%15.12g -> dR=%g dM=%g -> dR/dM %g\n",
           newstar->radius,
           star->radius,
           newstar->radius - star->radius,
           dm,
           drdm);
    free_star(&newstar);
    return drdm;
}
