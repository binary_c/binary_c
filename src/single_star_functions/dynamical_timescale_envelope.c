#include "../binary_c.h"
No_empty_translation_unit_warning;


double Pure_function dynamical_timescale_envelope(const struct star_t * star)
{
    /*
     * Return the dynamical timescale of a star's envelope in years
     */
    return 5.05e-05 * sqrt(Pow3(star->radius)/envelope_mass(star));
}
