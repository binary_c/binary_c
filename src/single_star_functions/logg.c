#include "../binary_c.h"
No_empty_translation_unit_warning;


double Pure_function logg(const struct star_t * Restrict const star)
{
    /* calculate logg for a star */
    return(log10(TINY+GRAVITATIONAL_CONSTANT*M_SUN*star->mass/Pow2(star->radius*R_SUN)));
}
