#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Approximate conversion between a neutron star's
 * gravitational mass (mg/Msun) and baryonic mass (mb/Msun)
 *
 * From https://arxiv.org/pdf/1904.11995.pdf Eq. 3
 * i.e. a private communication from J. Latimer to that
 * paper's author(s).
 */
double Pure_function neutron_star_baryonic_mass(const double mg)
{
    return mg + 0.058 * mg * mg  + 0.013 * mg * mg * mg;
}
