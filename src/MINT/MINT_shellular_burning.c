#include "../binary_c.h"
No_empty_translation_unit_warning;


#if defined MINT &&                             \
    defined NUCSYN

#define Metallicity(X) (1.0-X[XH1]-X[XH2]-X[XHe3]-X[XHe4])

void MINT_shellular_burning(struct stardata_t * Restrict const stardata,
                            struct star_t * const star,
                            struct mint_shell_t * const shell,
                            struct mint_shell_t * const prevshell,
                            const Shell_index k,
                            double * const convective_sigmav,
                            double * const Xconv,
                            double * const mconv)
{
    /*
     * If slowmix is TRUE, actually burn all the layers
     * one by one (which is slow!) and then mix them.
     * Normally you don't want to do this.
     */
    const Boolean slowmix = FALSE;

    /*
     * Detect central and surface shell
     */
    //const Boolean is_central_shell = k == 0 ;
    const Boolean is_surface_shell = k == star->mint->nshells-1;

    /*
     * Locate a convection zone outer edge.
     */
    const Boolean convective_outer_edge =
        (prevshell->convective == TRUE &&
         (shell->convective == FALSE ||
          is_surface_shell));

    //struct mint_shell_t * const nextshell =
    //    is_surface_shell ? shell : (shell+1);

    /*
     * Nuclear burning in shells for MINT
     */
    Number_density * Nshell = New_isotope_array;
    Reaction_rate sigmav[SIGMAV_SIZE];

    /*
     * Calculate number densities
     */
    X_to_N(stardata->store->imnuc,
           shell->rho,
           Nshell,
           shell->X,
           ISOTOPE_ARRAY_SIZE);
    Nshell[Xe] = nucsyn_free_electron_density(Nshell,
                                              stardata->store->atomic_number);

    /*
     * Calculate reaction rates at this shell
     */
    nucsyn_set_sigmav(stardata,
                      stardata->store,
                      log10(shell->T)-0.1,
                      sigmav,
                      stardata->preferences->reaction_rate_multipliers
#ifdef RATES_OF_AMANDA_NE22PG_FIX
                      ,FALSE
#endif
        );

    if(slowmix == FALSE)
    {
        if(shell->convective == TRUE)
        {
            /*
             * Calculate contribution to
             * convective sigmav
             */
            const double fshell = shell->dm * shell->rho;
            for(Reaction_rate_index j=0; j<SIGMAV_SIZE; j++)
            {
                convective_sigmav[j] +=
                    (j == SIGMAV_3He4 ? (shell->rho) : 1.0) *
                    fshell * sigmav[j];
            }

            /*
             * Calculate contribution to the mass of each particle
             * in the convective zone
             */
            for(Isotope j=0; j<ISOTOPE_ARRAY_SIZE; j++)
            {
                Xconv[j] += shell->dm * shell->X[j];
            }
            *mconv += shell->dm;
        }


        if(convective_outer_edge == TRUE)
        {
            /*
             * Reached the outer edge of the convection zone,
             * or were convective and have reached the surface.
             *
             * Finish computation of its mean <sigmav> and
             * do nuclear burning in the convection zone.
             */
            const double mconv_squared = Pow2(*mconv);
            for(Reaction_rate_index j=0;j<SIGMAV_SIZE;j++)
            {
                convective_sigmav[j] /=
                    (j == SIGMAV_3He4 ? *mconv : 1.0) *
                    mconv_squared;
            }

            /*
             * Finalise the Xconv calculation
             */
            for(Isotope j=0; j<ISOTOPE_ARRAY_SIZE; j++)
            {
                Xconv[j] /= *mconv;
            }

            /*
             * Convert mass fraction to total number of particles
             */
            Abundance * Nconv = New_isotope_array;
            X_to_N(stardata->store->imnuc,
                   *mconv,
                   Nconv,
                   Xconv,
                   ISOTOPE_ARRAY_SIZE);

            Nconv[Xe] = nucsyn_free_electron_density(
                Nconv,
                stardata->store->atomic_number);

            /*
             * Use the mean convective_sigmav to do the nuclear burning
             * in the convection zone.
             */
            nucsyn_burning_cycles(Nconv,
                                  convective_sigmav,
                                  stardata->model.dt, /* dt in years */
                                  star->mint->shells[0].T,
                                  star->mint->shells[0].rho, // irrelevant
                                  stardata);
            Nconv[Xe] = 0.0;

            nancheck("MINT burn (convective N) ",Nconv,ISOTOPE_ARRAY_SIZE);

            /*
             * Convert number of particles back to mass
             * fractions and set in each shell in the
             * convective zone
             */
            N_to_X(stardata->store->mnuc,
                   *mconv,
                   Nconv,
                   Xconv,
                   ISOTOPE_ARRAY_SIZE);
            nancheck("MINT burn (convective X) ",Xconv,ISOTOPE_ARRAY_SIZE);

            Safe_free(Nconv);

            for(Shell_index nn=0; nn<k; nn++)
            {
                memcpy(star->mint->shells[nn].X,
                       Xconv,
                       ISOTOPE_MEMSIZE);
            }

            /*
             * Clear the convective variables
             * ready for the next convective zone
             * (if there is one)
             */
            *mconv = 0.0;
            for(Reaction_rate_index j=0;j<SIGMAV_SIZE;j++)
            {
                convective_sigmav[j] = 0.0;
            }

            for(Isotope j=0; j<ISOTOPE_ARRAY_SIZE; j++)
            {
                Xconv[j] = 0.0;
            }
        }
    }

    if(slowmix == TRUE ||
       shell->convective == FALSE)
    {
        /*
         * Radiative shell, normal burn
         * without mixing
         */
        nucsyn_burning_cycles(Nshell,
                              sigmav,
                              stardata->model.dt, /* dt in years */
                              shell->T,
                              shell->rho,
                              stardata);
        Nshell[Xe] = 0.0;
        nancheck("MINT burn (radiative N) ",Nshell,ISOTOPE_ARRAY_SIZE);
        N_to_X(stardata->store->mnuc,
               shell->rho,
               Nshell,
               shell->X,
               ISOTOPE_ARRAY_SIZE);
        nancheck("MINT burn (radiative X) ",shell->X,ISOTOPE_ARRAY_SIZE);
    }

    if(slowmix == TRUE &&
       convective_outer_edge == TRUE)
    {
        /*
         * homogenize convective zone
         */
        double m = 0.0;
        Abundance * X = New_clear_isotope_array;
        for(Shell_index nn=0; nn<k; nn++)
        {
            struct mint_shell_t * const s = &star->mint->shells[nn];
            for(Isotope j=0; j<ISOTOPE_ARRAY_SIZE; j++)
            {
                X[j] += s->X[j] * s->dm;
            }
            m += s->dm;
        }
        for(Isotope j=0; j<ISOTOPE_ARRAY_SIZE; j++)
        {
            X[j] /= m;
        }
        for(Shell_index nn=0; nn<k; nn++)
        {
            memcpy(star->mint->shells[nn].X,
                   X,
                   ISOTOPE_MEMSIZE);
        }
        Safe_free(X);
        fprintf(stderr,"slowmix\n");
    }

    Safe_free(Nshell);
}

#endif // MINT && NUCSYN
