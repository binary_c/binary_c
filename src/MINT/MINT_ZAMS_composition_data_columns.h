#pragma once
#ifndef MINT_ZAMS_COMPOSITION_DATA_COLUMNS_H
#define MINT_ZAMS_COMPOSITION_DATA_COLUMNS_H

/*
 * Define the MINT data columns.
 */

#include "MINT_data_columns_ZAMS_composition_list.def"
#undef X

/* loading actions */
#define X(NAME) MINT_ZAMS_COMPOSITION_ACTION_##NAME,
enum { MINT_ACTIONS_LIST };
#undef X

/* ZAMS_COMPOSITION parameter items list */
#define X(NAME,ACTION) MINT_ZAMS_COMPOSITION_##NAME,
enum { MINT_ZAMS_COMPOSITION_PARAMETER_ITEMS_LIST };
#undef X

/* parameter items actions */
#define X(NAME,ACTION) MINT_ZAMS_COMPOSITION_ACTION_##ACTION,
static int MINT_ZAMS_composition_parameter_actions[] Maybe_unused = { MINT_MS_PARAMETER_ITEMS_LIST };
#undef X

/* data items list */
#define X(NAME,ACTION) MINT_ZAMS_COMPOSITION_##NAME,
enum { MINT_ZAMS_COMPOSITION_DATA_ITEMS_LIST };
#undef X

/* data items actions */
#define X(NAME,ACTION) MINT_ZAMS_COMPOSITION_ACTION_##ACTION,
static int MINT_ZAMS_composition_data_actions[] Maybe_unused = { MINT_ZAMS_COMPOSITION_DATA_ITEMS_LIST };
#undef X


#endif // MINT_MS_DATA_COLUMMNS_H
