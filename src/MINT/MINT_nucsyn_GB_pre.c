#include "../binary_c.h"

No_empty_translation_unit_warning;

#if defined MINT && defined NUCSYN

void MINT_nucsyn_GB_pre(struct stardata_t * Restrict const stardata Maybe_unused,
                        struct star_t * Restrict const star)
{
    /*
     * Disable shellular nuclear burning
     */
    star->do_burn = FALSE;

    /*
     * Set the core abundances
     */
    const double mc = star->core_mass[CORE_He];
    if(Is_not_zero(star->menv))
    {
        Foreach_shell(shell)
        {
            if(shell->m < mc)
            {
                /*
                 * Burn all H to He, CNO -> N
                 */
                shell->X[XHe4] += shell->X[XH1] + shell->X[XH2] + shell->X[XHe3];
                shell->X[XH1] = 0.0;
                shell->X[XH2] = 0.0;
                shell->X[XHe3] = 0.0;
                shell->X[XN14] += shell->X[XC12] + shell->X[XC13] + shell->X[XN15] + shell->X[XO16] + shell->X[XO17] + shell->X[XO18];
                shell->X[XC12] = 0.0;
                shell->X[XC13] = 0.0;
                shell->X[XN15] = 0.0;
                shell->X[XO16] = 0.0;
                shell->X[XO17] = 0.0;
                shell->X[XO18] = 0.0;
            }
        }
    }
}
#endif // MINT && NUCSYN
