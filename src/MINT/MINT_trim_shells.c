#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef MINT

/*
 * Trim shells outside the stellar surface
 */

void MINT_trim_shells(struct stardata_t * const stardata Maybe_unused,
                      struct star_t * const star)
{
    Shell_index n = star->mint->nshells - 1;
    while(n > 0 &&
          star->mint->shells[n].m/star->mass > 1.0 + TINY)
    {
        /* shell mass is outside the star: remove it */
        //const double dm = star->mass - star->mint->shells[n].m;
        /*    printf("trim shell %d/%d : m = %30.20e vs star->mass %30.20e : excess = %g, mass in shell %g\n",
              n,
              star->mint->nshells,
              star->mint->shells[n].m,
              star->mass,
              dm,
              star->mint->shells[n].dm
              );
              fflush(NULL);
        */
        star->mint->nshells--;
        star->mint->shells = Realloc(star->mint->shells,
                                     sizeof(struct mint_shell_t) *
                                     star->mint->nshells);
        n--;
    }
}
#endif // MINT
