#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT

/*
 * Free MINT metadata associated with a data_table
 */

void MINT_free_table_metadata(struct mint_table_metadata_t ** metadata_p)
{
    if(metadata_p)
    {
        struct mint_table_metadata_t * const metadata = *metadata_p;
        if(metadata->header != NULL)
        {
            CDict_free(metadata->header->cdict);
            Safe_free(metadata->header);
        }
        Safe_free(metadata->data_available);
        Safe_free(metadata->data_map);
        if(metadata->stream != NULL)
        {
            close_stream(&metadata->stream);
        }
        if(metadata->MINT_parameter_names != NULL)
        {
            for(size_t i=0;i<metadata->n_MINT_parameter_names;i++)
            {
                Safe_free(metadata->MINT_parameter_names[i]);
            }
        }
        if(metadata->MINT_datatype_names != NULL)
        {
            for(size_t i=0;i<metadata->n_MINT_datatype_names;i++)
            {
                Safe_free(metadata->MINT_datatype_names[i]);
            }
        }
        Safe_free(metadata->MINT_parameter_names);
        Safe_free(metadata->MINT_datatype_names);
        Safe_free(metadata->MINT_to_file_map);
        Safe_free(metadata->file_to_MINT_map);
        Safe_free(metadata->filename);
    }
}
#endif//MINT
