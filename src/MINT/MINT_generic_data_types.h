#pragma once
#ifndef MINT_GENERIC_DATA_TYPES_H
#define MINT_GENERIC_DATA_TYPES_H

/*
 * Construct generic data columns list
 * which is valid for all stellar types
 */
#include "MINT_generic_data_types.def"

#undef X
#define X(NAME) MINT_##NAME,
enum { MINT_DATA_TYPES_LIST };
#undef X

#endif// MINT_GENERIC_DATA_TYPES_H
