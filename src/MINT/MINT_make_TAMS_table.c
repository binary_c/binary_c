#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT
#include "MINT.h"

Boolean MINT_make_TAMS_table(struct stardata_t * const stardata,
                             const int when Maybe_unused)
{
    /*
     * Terminal-age main-sequence (TAMS) data
     *
     * Return TRUE on success, FALSE on failure.
     */
    const Boolean vb = FALSE;
    double logM;
    const int N = 1000;
    const double min = log10(0.01);
    const double max = log10(1000.0);
    const double dlogM = (max - min) / (N - 1);

    /* one parameter : logM */
    const int nparams = 1;
    /* two data items: age and luminosity at the TAMS */
    const int ndata = 2;

    double * table_lifetimes = Malloc(sizeof(double) * N * (nparams + ndata));

    if(table_lifetimes == NULL)
    {
        Exit_binary_c(BINARY_C_MALLOC_FAILED,
                      "Failed to malloc for table_liftimes\n");
    }

    double * const table_lifetimes_start = table_lifetimes;
    if(vb)printf("PRE %p : nparams %zu ndata %zu : table_lifetimes is at %p\n",
                 (void*)stardata->store->MINT_tables[MINT_TABLE_TAMS],
                 stardata->store->MINT_tables[MINT_TABLE_MS]->nparam,
                 stardata->store->MINT_tables[MINT_TABLE_MS]->ndata,
                 (void*)table_lifetimes);

    double * result = Malloc(MINT_result_size(MINT_TABLE_MS));

    for(logM = min; logM < max+TINY; logM += dlogM)
    {
        Interpolate(stardata->store->MINT_tables[MINT_TABLE_MS],
                    ((double[]){logM,0.0}), /* log10(mass), Xc=0 */
                    result,
                    FALSE);
        /*
         * columns are: log10(M/Msun), log10(TAMS age (Myr)), log10(TAMS luminosity)
         */
        *(table_lifetimes++) = logM;
        *(table_lifetimes++) = log10(result[MINT_MS_AGE]) - 6.0;
        *(table_lifetimes++) = result[MINT_MS_LUMINOSITY];

          if(vb)printf("Lifetimes table M=%g lifetime=%g Myr L(TAMS)=%g %s\n",
          exp10(logM),
          result[MINT_MS_AGE] * 1e-6,
          exp10(result[MINT_MS_LUMINOSITY]),
          L_SOLAR
          );

    }

    NewDataTable_from_Pointer(table_lifetimes_start,
                              stardata->store->MINT_tables[MINT_TABLE_TAMS],
                              nparams,
                              ndata,
                              N);
    Safe_free(result);

    return TRUE;
}

#endif // MINT
