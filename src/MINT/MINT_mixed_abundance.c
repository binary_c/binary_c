#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Calculate the abundance of one isotope
 * in a given mixed mass range of the star.
 */

#ifdef MINT
#include "MINT.h"

#ifdef NUCSYN
double MINT_mixed_abundance(struct stardata_t * const stardata,
                            struct star_t * const star,
                            const double m_low,
                            const double m_high,
                            Isotope i)
{
    struct mint_shell_t * mixed =
        MINT_mixed_shell(stardata,star,m_low,m_high);

    const double X = mixed->X[i];

    Safe_free(mixed);

    return X;
}
#endif // NUCSYN
#endif // MINT
