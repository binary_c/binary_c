#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT
#include "MINT.h"
#include "MINT_load_grid.h"

/*
 * Update our GB table for the current properties
 * of the given star, and use the star's table.
 */
Boolean MINT_update_GB_table(struct stardata_t * const stardata,
                             struct star_t * const star)
{
    struct data_table_t * const data_table = star->mint->GB_table;
    if(data_table != NULL)
    {
        struct mint_table_metadata_t * const metadata =
            data_table->metadata;
        struct data_table_analysis_t * const analysis =
            data_table->analysis;

        /*
         * Check the mass in the first variable list
         */
        const rinterpolate_counter_t lower =
            rinterpolate_bisearch(analysis->values[0],
                                  star->mass,
                                  analysis->nvalues[0]);
        const rinterpolate_counter_t upper =
            lower + 1;

        /*
         * Hence find corresponding line numbers
         */
        const size_t lower_line_number = lower * analysis->skip[0];
        const size_t upper_line_number = (upper+1) * analysis->skip[0];

        /*
         * Check if we need to update the table.
         */
        if(lower_line_number != metadata->lower_line_number ||
           upper_line_number != metadata->upper_line_number)
        {
            const struct data_table_defer_instructions_t instructions =
                {
                    .lower_line_number = lower_line_number,
                    .upper_line_number = upper_line_number
                };

            MINT_update_table(stardata,
                              data_table,
                              &instructions,
                              analysis,
                              metadata,
                              FALSE);

            struct rinterpolate_table_t * const rtable =
                rinterpolate_table_struct(stardata->persistent_data->rinterpolate_data,
                                          data_table->data,
                                          NULL);

            if(rtable->stats)
            {
                /*
                 * Todo:
                 *
                 * Update rinterpolate's stats data, e.g. valuelist.
                 * Rest should be the same.
                 */
            }

            /*
             * Set the metadata
             */
            metadata->lower_line_number = lower_line_number;
            metadata->upper_line_number = upper_line_number;
        }
    }
    return TRUE;
}

#endif // MINT
