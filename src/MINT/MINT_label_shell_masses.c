#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * "Label" shell masses, i.e. set their masses by integrating
 * the internal mass
 */

#ifdef MINT
void MINT_label_shell_masses(struct stardata_t * const stardata Maybe_unused,
                             struct star_t * const star)
{
    double m = 0;
    for(Shell_index i=0; i<star->mint->nshells; i++)
    {
        m += star->mint->shells[i].dm;
        star->mint->shells[i].m = m;
    }
}
#endif // MINT
