#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT
#include "MINT.h"
#include "MINT_load_grid.h"

/*
 * Subroutine to load main-sequence data
 * for MINT.
 *
 * Return TRUE on success, FALSE on failure.
 */

Boolean MINT_load_COWD_grid(struct stardata_t * const stardata,
                            const int when)
{
    const int vb = 0;

    /*
     * Make lists of parameter and data item names
     * we require in our table of scalars
     */
#undef X
#define X(NAME,ACTION) Stringify_expanded(NAME),
    static const char * parameter_names[] =
        { MINT_COWD_PARAMETER_ITEMS_LIST };
    static const char * data_names[] =
        { MINT_COWD_DATA_ITEMS_LIST };
#undef X

    if(!MINT_has_table(MINT_TABLE_COWD))
    {
        stardata->store->MINT_tables[MINT_TABLE_COWD] = MINT_new_MINT_data_table();
    }

    const Boolean ret = MINT_Load_Table(
        stardata,
        MINT_TABLE_COWD,
        NULL,
        NULL,
        "MINT_COWD",
        parameter_names,
        data_names,
        MINT_COWD_parameter_actions,
        MINT_COWD_data_actions,
        when,
        NULL,
        vb);


    if(MINT_has_table(MINT_TABLE_COWD))
    {
        /*
         * make zero-age table and adjust table data
         * such that age = 0 is actually the start of each WD
         * track.
         */
        struct data_table_t * const table = stardata->store->MINT_tables[MINT_TABLE_COWD];
        double * data = NULL;
        size_t ndata = 0;
        const size_t linelen = table->ndata + table->nparam;
        double prev_mass = *(table->data + 0);
        double prev_metallicity = *(table->data + 1);

#define __TEMPERATURE_INCREASING

#ifdef __TEMPERATURE_INCREASING
        double prev_log_central_temperature = *(table->data + 2);
        for(size_t i=0; i<table->nlines; i++)
        {
            const double metallicity = *(table->data + i*linelen + 0);
            const double mass = *(table->data + i*linelen + 1);
            const double log_central_temperature = *(table->data + i*linelen + 2);
            const Boolean is_final = i == table->nlines-1;

            if(i>0 && (
                   is_final ||
                   !Fequal(prev_metallicity,metallicity) ||
                   !Fequal(prev_mass,mass))
                )
            {
                /*
                 * New Z,M pair: Save zero-age properties.
                 * For these we usually need to use prev_* but
                 * if we're the final line we should use the
                 * current line's values.
                 */
                ndata++;
                data = Realloc(data,
                               sizeof(double)*ndata*3);
                data[ndata*3 - 3] = is_final ? metallicity : prev_metallicity;
                data[ndata*3 - 2] = is_final ? mass : prev_mass;
                data[ndata*3 - 1] = is_final ? log_central_temperature : prev_log_central_temperature;
            }

            prev_mass = mass;
            prev_metallicity = metallicity;
            prev_log_central_temperature = log_central_temperature;
        }
#endif // __TEMPERATURE_INCREASING
#ifdef __TEMPERATURE_DECREASING
        for(size_t i=0; i<table->nlines; i++)
        {
            const double metallicity = *(table->data + i*linelen + 0);
            const double mass = *(table->data + i*linelen + 1);
            const double log_central_temperature = *(table->data + i*linelen + 2);

            if(
                i==0 ||
                !Fequal(prev_metallicity,metallicity) ||
                !Fequal(prev_mass,mass)
                )
            {
                /*
                 * New Z,M pair: Save zero-age properties.
                 */
                ndata++;
                data = Realloc(data,
                               sizeof(double)*ndata*3);
                data[ndata*3 - 3] = metallicity;
                data[ndata*3 - 2] = mass;
                data[ndata*3 - 1] = log_central_temperature;
            }

            prev_mass = mass;
            prev_metallicity = metallicity;
        }
#endif // __TEMPERATURE_DECREASING

        NewDataTable_from_Pointer(data,
                                  stardata->store->MINT_tables[MINT_TABLE_ZACOWD],
                                  2,
                                  1,
                                  ndata);
    }
    return ret;
}

#endif // MINT
