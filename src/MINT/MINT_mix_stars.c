#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT

/*
 * Function to mix stars in MINT
 */

void MINT_mix_stars(struct stardata_t * const stardata,
                    const Boolean eject)
{
    Dprint("merge stars of types %d %d\n",
           stardata->star[0].stellar_type,
           stardata->star[1].stellar_type);

    /*
     * Set the coalesce flag
     */
    stardata->model.coalesce = TRUE;

    /*
     * Choose which star we merge into
     */
    struct star_t * const merged_star = &stardata->star[0];

    /*
     * Set the new star's mass
     */
    if(ON_MAIN_SEQUENCE(stardata->star[0].stellar_type) &&
       ON_MAIN_SEQUENCE(stardata->star[1].stellar_type))
    {
        const double f_lost = merger_mass_loss_fraction(stardata,
                                                        MAIN_SEQUENCE);
        merged_star->mass = Sum_of_stars(mass) * (1.0 - f_lost);
        MINT_mix_stars_MS(stardata,
                          eject);
    }
    else
    {
        /*
         * post-MS merger : use BSE for now
         */
        mix_stars_BSE(stardata,
                      eject);
    }

    if(merged_star->mass <= TINY)
    {
        /*
         * Merging has evaporated the star
         */
        MINT_make_massless_remnant(stardata,
                                   merged_star);
    }

    /*
     * Set the new star's angular momentum
     * and update its rotation variables
     */
    merged_star->angular_momentum =
        stardata->preferences->merger_angular_momentum_factor *
        breakup_angular_momentum(stardata,
                                 merged_star);

    calculate_rotation_variables(merged_star,
                                 merged_star->radius);



    /*
     * Erase the other star
     */
    MINT_make_massless_remnant(stardata,
                               Other_star_struct(merged_star));
}
#endif//MINT
