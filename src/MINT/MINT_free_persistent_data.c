#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT

#define Persistent_free(P) Safe_free(stardata->persistent_data->P)

void MINT_free_persistent_data(struct stardata_t * const stardata)
{
    /*
     * Free memory allocated for MINT in the
     * binary_c persistent data struct.
     */
    for(unsigned int i=0; i<MINT_TABLE_NUMBER; i++)
    {
        if(stardata->persistent_data->MINT_mapped_tables[i] != NULL)
        {
            Safe_free_nocheck(stardata->persistent_data->MINT_mapped_tables[i]);
        }
    }
    Persistent_free(MINT_MS_result);
    Persistent_free(MINT_MS_result_chebyshev);
    Persistent_free(MINT_MS_result_conv);
    Persistent_free(MINT_GB_result);
    Persistent_free(MINT_GB_result_chebyshev);
    Persistent_free(MINT_GB_result_conv);
    Persistent_free(MINT_CHeB_result);
    Persistent_free(MINT_CHeB_result_chebyshev);
    Persistent_free(MINT_CHeB_result_conv);
    Persistent_free(MINT_COWD_result);
    Persistent_free(MINT_COWD_result_chebyshev);
    Persistent_free(MINT_COWD_result_conv);
}

#endif//MINT
