#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT
#include "MINT.h"
#include "MINT_load_grid.h"

/*
 * Subroutine to load GB data for MINT.
 *
 * Return TRUE on success, FALSE on failure.
 */

Boolean MINT_load_GB_grid(struct stardata_t * const stardata,
                          const int when)
{
    const int vb = 0;

    /*
     * Make lists of parameter and data item names
     * we require in our table of scalars
     */
#undef X
#define X(NAME,ACTION) Stringify_expanded(NAME),
    static const char * parameter_names[] =
        { MINT_GB_PARAMETER_ITEMS_LIST };
    static const char * data_names[] =
        { MINT_GB_DATA_ITEMS_LIST };
#undef X


    Foreach_star(star)
    {
        Dprint("Load GB grid star %d : Array size %zu %zu\n",
               star->starnum,
               Array_size(parameter_names),
               Array_size(data_names));

        if(star->mint->GB_table == NULL)
        {
            New_data_table(star->mint->GB_table);
        }

        const Boolean ret = MINT_Load_Table(
            stardata,
            MINT_TABLE_GB,
            star->mint->GB_table,
            NULL,
            "MINT_GB",
            parameter_names,
            data_names,
            MINT_GB_parameter_actions,
            MINT_GB_data_actions,
            when,
            NULL,
            vb
            );

        if(ret == FALSE)
        {
            return ret;
        }

        Interpolate(star->mint->GB_table,
                    NULL,
                    NULL,
                    FALSE);
    }

    return TRUE;
}

#endif // MINT






//#define __TEST_GB_TABLE
#ifdef __TEST_GB_TABLE
    /*
     * Test GB table integration
     */

#undef get_data
#define get_data(VARTYPE)                       \
    ( p[map[(VARTYPE)]+3] )

#define NPARAM 2
#define NDATA 5
    struct data_table_t * d = NULL;
    const unsigned int * const map = stardata->store->MINT_data_map[MINT_TABLE_GB];
    const size_t width =
        stardata->store->MINT_tables[MINT_TABLE_GB]->ndata +
        stardata->store->MINT_tables[MINT_TABLE_GB]->nparam;
    const size_t nl = stardata->store->MINT_tables[MINT_TABLE_GB]->nlines;
    double * p = stardata->store->MINT_tables[MINT_TABLE_GB]->data;
    double * data0 = Malloc(sizeof(double) * nl * (NPARAM + NDATA));
    {
        double * data = data0;
        for(size_t i=0; i<nl; i++)
        {
            if(0)printf(
                "XXX OLD %zu : log10(M)=%g degen=%g Mc/M=%g d(degen)/dt=%g d(Mc/M)/dt=%g\n",
                i,
                *(p+0),
                *(p+1),
                *(p+2),
                exp10(get_data(MINT_GB_FIRST_DERIVATIVE_CENTRAL_DEGENERACY)),
                get_data(MINT_GB_FIRST_DERIVATIVE_HELIUM_CORE_MASS_FRACTION)
                );

            *(data+0) = *(p+1); // degeneracy
            *(data+1) = *(p+2); // Mc/M

            *(data+2) = get_data(MINT_GB_FIRST_DERIVATIVE_CENTRAL_DEGENERACY); // log10(degen)
            *(data+3) = get_data(MINT_GB_FIRST_DERIVATIVE_HELIUM_CORE_MASS_FRACTION); // d(Mc/M)/dt
            *(data+4) = get_data(MINT_GB_LUMINOSITY); // log10(L)
            *(data+5) = get_data(MINT_GB_RADIUS); // log10(R)
            *(data+6) = get_data(MINT_GB_HELIUM_LUMINOSITY); // L(He)

            p += width;
            data += NDATA + NPARAM;
        }
    }

    NewDataTable_from_Pointer(data0,
                              d,
                              NPARAM,
                              NDATA,
                              nl);
    //  show_data_table(d);Flexit;
    struct data_table_t * mapped_table;
    {
        double x[NPARAM] = {-2,0.5};
        double r[NDATA];
        Interpolate(d,x,r,FALSE);
        mapped_table = Map_data_table(
            d,
            All_cols,
            New_res(0, 100)
            );
    }

    /* MWE : 2.64Msun */
    double mcf = 0.0789; // 0.0789
    double degen = -1.70; // -1.70
    double dt = 0.001 * 1e6;
    double t = 0.0;
    Boolean brk = FALSE;
    double Lwas = -100.0;

    while(degen < 5.0 &&
          mcf < 1.0 &&
          brk == FALSE)
    {
        double x[NPARAM] = {
            degen,
            mcf
        };
        double r[NDATA];

        /*
         * Fudge to use degen>some min when looking up
         */
        Interpolate(mapped_table,x,r,FALSE);

        const double ddegendt = (r[0]); // convert log10(d(degen)/dt) -> d(degen)/dt (J/yr)
        const double dmcfdt = r[1];//Max(r[1],0.0);
        const double L = (r[2]); // log10(L/Lsun) -> L/Lsun
        const double R = (r[3]); // log10(R/Rsun) -> R/Rsun
        const double LHe = (r[4]);
        if(LHe > 0.01)
        {
            brk = TRUE; // LHe > 0.01
        }
        const double teff = Teff_from_luminosity_and_radius(L,R);
        printf("XXX %g %g %g %g %g %g\n",
               t,
               degen,
               mcf,
               ddegendt,
               dmcfdt,
               log10(LHe));
        printf("HRDHRD %g %g\n",
               log10(teff),
               log10(L)
            );

        if(L < Lwas && teff < 3.74)
        {
            //Flexit;
        }
        Lwas = L;

        double dt0 = dt;
        dt = Min3(0.01/ddegendt,0.01/dmcfdt,dt);
        if(dt == dt0) dt*=1.2;

        degen += ddegendt * dt;
        mcf += dmcfdt * dt;
        t += dt;
    }

    Flexit;

#endif // __TEST_GB_TABLE

#ifdef _TEST_MAPPED_TABLE
#define _TEST_MAPPED_TABLE

        int * const file_column_map = stardata->store->MINT_to_file_map[MINT_TABLE_GB];
        const size_t ll = stardata->store->MINT_tables[MINT_TABLE_GB]->ndata + stardata->store->MINT_tables[MINT_TABLE_GB]->nparam;
        double * result = Malloc(MINT_mapped_result_size(MINT_TABLE_GB));
        for(size_t i=0; i<stardata->store->MINT_tables[MINT_TABLE_GB]->nlines; i++)
        {
            double * const p = stardata->store->MINT_tables[MINT_TABLE_GB]->data + i * ll;
            printf("test line %zu : %g %g %g : \n",
                   i,
                   exp10(p[0]),
                   p[1],
                   p[2]);
            Interpolate(stardata->persistent_data->MINT_mapped_tables[MINT_TABLE_GB],
                        ((double[]){
                            p[0],
                            p[1],
                            p[2]
                        }),
                        result,
                        FALSE);
            const Boolean tvb = 0;
            size_t maxdiff_col = 666666;
            double maxdiff = -1.0, maxx=666.666, maxy=666.666;
            for(size_t offset=0; offset<stardata->store->MINT_tables[MINT_TABLE_GB]->ndata; offset++)
            {
                const size_t j = stardata->store->MINT_tables[MINT_TABLE_GB]->nparam + offset;
                const double x = *(p + j);
                const double y = result[offset];
                const double d = Abs_percent_diff(x,y);

                if(tvb) printf("%zu %g %g %g ",
                               offset,x,y,d);

                if(d > maxdiff)
                {
                    maxdiff = d;
                    maxdiff_col = j;
                    maxx = x;
                    maxy = y;
                    if(tvb) printf("*** ");
                }
                if(tvb) printf(": ");
            }

            printf("maxdiff %g %% new col %zu, orig col %d (data orig %g vs interpolated %g)\n\n",
                   maxdiff,
                   maxdiff_col,
                   file_column_map[maxdiff_col],
                   maxx,
                   maxy);
        }
        Flexit;
#endif // TEST MAPPED TABLE
