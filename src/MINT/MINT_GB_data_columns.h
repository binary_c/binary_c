#pragma once
#ifndef MINT_GB_DATA_COLUMNS_H
#define MINT_GB_DATA_COLUMNS_H

/*
 * Define the MINT data columns.
 */

#include "MINT_data_columns_GB_list.def"
#undef X

/* loading actions */
#define X(NAME) MINT_GB_ACTION_##NAME,
enum { MINT_ACTIONS_LIST };
#undef X

/* MS parameter items list */
#define X(NAME,ACTION) MINT_GB_##NAME,
enum { MINT_GB_PARAMETER_ITEMS_LIST };
#undef X

/* parameter items actions */
#define X(NAME,ACTION) MINT_GB_ACTION_##ACTION,
static int MINT_GB_parameter_actions[] Maybe_unused = { MINT_GB_PARAMETER_ITEMS_LIST };
#undef X

/* data items list */
#define X(NAME,ACTION) MINT_GB_##NAME,
enum { MINT_GB_DATA_ITEMS_LIST };
#undef X

/* data items actions */
#define X(NAME,ACTION) MINT_GB_ACTION_##ACTION,
static int MINT_GB_data_actions[] Maybe_unused = { MINT_GB_DATA_ITEMS_LIST };
#undef X


#endif // MINT_GB_DATA_COLUMMNS_H
