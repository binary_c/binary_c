#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT
void MINT_alloc(struct stardata_t * const stardata)
{
    /*
     * Allocate memory for MINT
     */
    Foreach_star(star)
    {
        if(star->mint == NULL)
        {
            star->mint = Calloc(1,sizeof(struct mint_t));
        }
    }
}
#endif//MINT
