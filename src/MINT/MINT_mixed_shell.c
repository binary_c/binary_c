#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Return a shell which contains the mixed abundances of all
 * its internal shells. You will need to free this shell's
 * memory when you are done with it.
 */

#ifdef MINT
#ifdef NUCSYN

struct mint_shell_t * MINT_mixed_shell(struct stardata_t * const stardata Maybe_unused,
                                       struct star_t * const star,
                                       const double m_low,
                                       const double m_high)
{
    struct mint_shell_t * mixed_region = Malloc(sizeof(struct mint_shell_t));
    mixed_region->dm = 0.0;
    memset(mixed_region->X,0,ISOTOPE_MEMSIZE);
    struct mint_shell_t * shell = &star->mint->shells[0];
    for(Shell_index i=0;
        i<star->mint->nshells;
        i++, shell++)
    {
        if(shell->m >= m_low &&
           shell->m <= m_high)
        {
            mixed_region->dm += shell->dm;
            mixed_region->X[i] += shell->dm * shell->X[i];
        }
        else if(shell->m > m_high)
        {
            break;
        }
    }
    for(Isotope k=0; k<ISOTOPE_ARRAY_SIZE; k++)
    {
        mixed_region->X[k] /= mixed_region->dm;
    }
    return mixed_region;
}
#endif // NUCSYN
#endif // MINT
