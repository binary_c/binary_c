#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef MINT

/*
 * Get the data on the Chebyshev coordinates for a MS star of
 * known mass and central hydrogen abundance.
 */

void MINT_GB_get_chebyshev_data(struct stardata_t * Restrict const stardata Maybe_unused,
                                struct star_t * const star,
                                double * const result_cheb)
{
    Interpolate(star->mint->GB_table,
                ((double[]){
                    log10(star->mass),
                    star->core_mass[CORE_He] / star->mass
                }), /* log(mass), Xc */
                result_cheb,
                FALSE);
}

#endif // MINT
