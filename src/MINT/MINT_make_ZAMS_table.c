#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT
#include "MINT.h"

Boolean MINT_make_ZAMS_table(struct stardata_t * const stardata Maybe_unused,
                             const int when Maybe_unused)
{
    /*
     * Zero-age main-sequence (ZAMS) data tables
     *
     * Return TRUE on success, FALSE on failure.
     */

    return TRUE;
}

#endif // MINT
