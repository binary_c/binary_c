#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT

/*
 * Function to mix stars in MINT
 *
 * TODO : angular momentum
 */

void MINT_mix_stars_MS(struct stardata_t * const stardata,
                       const Boolean eject)
{
    const Boolean vb = FALSE;

    /*
     * MS + MS merger
     */
    struct star_t * newstar = New_star_from(&stardata->star[0]);
    struct star_t * prevstar = &stardata->previous_stardata->star[0];

    if(vb)
    {
        printf("Mix stars with shells %d %d\n",
               stardata->star[0].mint->nshells,
               stardata->star[1].mint->nshells);

        Foreach_evolving_star(star)
        {
            if(star->mint->nshells>10)
            {
                for(Shell_index i=star->mint->nshells-1; i>star->mint->nshells-10; i--)
                {
                    printf("star %d shell %d/%d radius %g\n",
                           star->starnum,
                           i,
                           star->mint->nshells-1,
                           star->mint->shells[i].radius);
                }
            }
        }
    }

#ifdef NUCSYN
    Foreach_star(star)
    {
        for(Shell_index i=0; i<star->mint->nshells; i++)
        {
            struct mint_shell_t * shell = &star->mint->shells[i];
            const double Xtot = nucsyn_totalX(shell->X);
            shell->X[XH2] = 0.0;
            shell->X[XHe3] = 0.0;
            const double ZZ =
                1.0 - (shell->X[XH1] +
                       shell->X[XH2] +
                       shell->X[XHe3] +
                       shell->X[XHe4]);

            const double Hwas = shell->X[XH1];
            const double Hewas = shell->X[XHe4];

            shell->X[XHe4] +=
                stardata->store->mnuc[XHe4] / (4.0 * stardata->store->mnuc[XH1])
                * shell->X[XH1];
            shell->X[XH1] = 0.0;

            const double Zmax =
                1.0 - (shell->X[XH1] +
                       shell->X[XH2] +
                       shell->X[XHe3] +
                       shell->X[XHe4]);

            shell->X[XH1] = Hwas;
            shell->X[XHe4] = Hewas;

            if(vb)
            {
                printf("PRE%d %d %g Xtot=%g XH1=%g Z=%g Zmax=%g\n",
                       (int)star->starnum,
                       (int)i,
                       shell->m,
                       Xtot,
                       shell->X[XH1],
                       ZZ,
                       Zmax
                    );
            }
        }
    }
#endif // NUCSYN

    /*
     * New mass is the sum of the two stars' masses
     * minus mass lost
     */
    const double mass_lost =
        eject == FALSE ? 0.0 :
        (Sum_of_stars_in(mass,stardata->previous_stardata) -
         newstar->mass);

    newstar->phase_start_mass = newstar->mass;
    set_no_core(newstar);

    /*
     * New central hydrogen is the minimum of the two
     * (This calculation can be improved!)
     */
    newstar->mint->XHc = Min_of_stars(mint->XHc);

    /*
     * Excess luminosity from the merger:
     *
     * We assume this is all of Eorb absorbed by
     * the star in an orbital period.
     */
    const double Eorb_cgs = orbital_energy(stardata->previous_stardata,NULL);
    const double Elost_cgs = GRAVITATIONAL_CONSTANT * newstar->mass * mass_lost * M_SUN * M_SUN / (newstar->radius * R_SUN);
    const double Ebind_i = MINT_binding_energy(stardata,&stardata->previous_stardata->star[0]) +
        MINT_binding_energy(stardata,&stardata->previous_stardata->star[1]);
    const double Ebind_f = MINT_binding_energy(stardata,newstar);
    const double Porb_s = stardata->common.orbit.period * YEAR_LENGTH_IN_SECONDS;
    const double t_dyn_s = dynamical_timescale(newstar) * YEAR_LENGTH_IN_SECONDS;
    const double timescale_s =
        Max(Porb_s,t_dyn_s);

    if(vb)
    {
        printf("ENERGY : from orbit %g, in mass lost KE %g, binding energy difference %g - %g\n",
               -Eorb_cgs, Elost_cgs, Ebind_i, Ebind_f);
    }
    newstar->accretion_luminosity =
        (-Eorb_cgs - Elost_cgs + Ebind_i - Ebind_f) / timescale_s / L_SUN;

    /*
     * recalculate assuming a fixed accretion rate of 1e-4 Msun/year
     * and that a fraction fl of this contributes to core flux,
     * the rest can (e.g.) expand the star and be radiated
     */
    const double fl = 0.1;
    newstar->accretion_luminosity =
        fl *
        GRAVITATIONAL_CONSTANT
        *
        1e-4 * M_SUN / YEAR_LENGTH_IN_SECONDS
        *
        newstar->mass * M_SUN
        /
        (newstar->radius * R_SUN)
        /
        L_SUN;

    const double Rdot = R_SUN * (newstar->radius - prevstar->radius) / timescale_s;
    const double Mdot = M_SUN * (newstar->mass - prevstar->mass) / timescale_s;

    const double Eacc1 =
        GRAVITATIONAL_CONSTANT
        *
        (newstar->mass - prevstar->mass) * M_SUN
        *
        prevstar->mass * M_SUN
        /
        (prevstar->radius * R_SUN);

    const double Eacc2 =
        -
        (
            (prevstar->mass * M_SUN * Rdot - Mdot * prevstar->radius * R_SUN) *
            log(prevstar->radius * R_SUN + Rdot * timescale_s) +
            Mdot * Rdot * timescale_s
            )
        /
        Pow2(Rdot);

    if(vb)
        printf("Eorb %g : Elost %g : timescale : Porb %g , t_dyn_s %g -> %g -> Lacc %g (excess %g = %5.2f%%) Eacc 1 = %g, 2 = %g\n",
               Eorb_cgs,
               Elost_cgs,
               Porb_s,
               t_dyn_s,
               timescale_s,
               newstar->accretion_luminosity,
               newstar->accretion_luminosity - newstar->luminosity,
               (newstar->accretion_luminosity/newstar->luminosity - 1.0)*100.0,
               Eacc1,
               Eacc2);

    /*
     * Hence new structure, including the accretion
     * luminosity that increases the central convection
     * zone extent.
     *
     * Note that we enforce dt = 0 so there's no
     * other evolution.
     */
    const double dtwas = stardata->model.dt;
    MINT_stellar_structure_MS(stardata,
                              &stardata->star[0],
                              newstar,
                              STELLAR_STRUCTURE_CALLER_MINT_MIX);
    if(vb)printf("done MS struct\n");

    MINT_merge_stars_shells_MS(stardata,
                               &stardata->star[0],
                               &stardata->star[1],
                               newstar);

    /*
     * Copy the shell data
     */
    if(MINT_has_shells(newstar))
    {
        /*
         * Sort by, probably, density.
         */
        MINT_sort_shells(stardata,
                         newstar,
                         MINT_shell_offset(newstar,rho),
                         -1);

        /*
         * Relabel mass coordinates.
         */
        MINT_label_shell_masses(stardata,
                                newstar);

        /*
         * Use non-equilibrium central convection zone
         * for mixing (just for this step)
         */
        newstar->convective_core_mass =
            MINT_MS_non_eq_conv_coremass(stardata,
                                         newstar);

#ifdef NUCSYN
        if(vb)
        {
            for(Shell_index i=0; i<newstar->mint->nshells; i++)
            {
                printf("PRE %d %g X=%g Z=%g\n",
                       (int)i,
                       newstar->mint->shells[i].m,
                       newstar->mint->shells[i].X[XH1],
                       1.0 - newstar->mint->shells[i].X[XH1] - newstar->mint->shells[i].X[XHe4]
                    );
            }
        }

        /*
         * Thermohaline mixing (also does convection)
         */
        MINT_thermohaline(stardata,newstar);

        if(vb)
        {
            for(Shell_index i=0; i<newstar->mint->nshells; i++)
            {
                printf("POST %d %g %g\n",
                       (int)i,
                       newstar->mint->shells[i].m,
                       newstar->mint->shells[i].X[XH1]);
            }
        }
#endif // NUCSYN

        /*
         * Remesh the star
         */
        MINT_remesh(stardata,
                    newstar);


#ifdef NUCSYN
        if(vb)printf("post-merger XHC %g vs nucsyn %g\n",
                     newstar->mint->XHc,
                     newstar->mint->shells[0].X[XH1]);

        struct mint_shell_t * centre = &newstar->mint->shells[0];
        const double Z = 1.0 - centre->X[XH1] - centre->X[XHe4];
        if(vb)
        {
            printf("Z = %g should be %g\n",Z,stardata->common.metallicity);
        }
        newstar->mint->XHc = centre->X[XH1];
        newstar->mint->XHec = centre->X[XHe4];

        /*
         * Require that the metallicity is correct, otherwise
         * we'll run into issues later when integrating the
         * central (MESA) hydrogen abundance.
         *
         * This rough calculation ignores metallicity changes
         * which occur because of mass changes associated with
         * 4H->He4, and ignores H2 and He3.
         */
        const double f = (1.0 - stardata->common.metallicity)/(centre->X[XH1] + centre->X[XHe4]);
        newstar->mint->XHc *= f;
        newstar->mint->XHec *= f;
#endif // NUCSYN
    }

    /*
     * restore the timestep
     */
    stardata->model.dt = dtwas;

    /*
     * Put the merged star in star[0]
     */
    copy_star(stardata,
              newstar,
              &stardata->star[0]);

    /*
     * Free the star
     */
    free_star(&newstar);
}

#endif // MINT
