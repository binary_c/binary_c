#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Thermohaline (and convective) mixing in a star
 */

#if defined MINT &&                             \
    defined NUCSYN

void MINT_renormalize_shells(struct star_t * const star)
{
    /*
     * Correct for slight mass changes
     */
    for(Shell_index k=0; k<star->mint->nshells; k++)
    {
        nucsyn_renormalize_abundance(star->mint->shells[k].X);
    }
}

#endif // MINT && NUCSYN
