#include "../binary_c.h"

No_empty_translation_unit_warning;

#if defined MINT && defined NUCSYN

void MINT_nucsyn_HG_pre(struct stardata_t * Restrict const stardata Maybe_unused,
                        struct star_t * Restrict const star)
{
    /*
     * Disable shellular burning
     */
    star->do_burn = FALSE;
}
#endif // MINT && NUCSYN
