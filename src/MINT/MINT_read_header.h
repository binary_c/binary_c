#pragma once
#ifndef MINT_READ_HEADER_H
#define MINT_READ_HEADER_H

#define Match_header(...)                       \
    Match_header_implementation("Match",        \
                                __COUNTER__,    \
                                __VA_ARGS__)

#define Match_header_implementation(LABEL,LINE,...)                 \
    __extension__                                                   \
    ({                                                              \
        struct string_array_t _my_string_array;                     \
        const char * const _my_strings[] = {"#", __VA_ARGS__};      \
        _my_string_array = (struct string_array_t)                  \
            {                                                       \
                .strings = (char**)_my_strings,                     \
                .n = (ssize_t)(sizeof(_my_strings)/                 \
                               sizeof(char*))                       \
            };                                                      \
        (Boolean)                                                   \
            (                                                       \
            string_array->n >= _my_string_array.n                   \
            &&                                                      \
            string_arrays_equal_n(                                  \
                string_array,                                       \
                &_my_string_array,                                  \
                _my_string_array.n                                  \
                )                                                   \
            );                                                      \
    })


#define Is_header_line (string_array->strings[0][0] == '#')

#endif // MINT_READ_HEADER_H
