#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT
#include "MINT.h"
#include "MINT_load_grid.h"

/*
 * Subroutine to load CHeB data
 * for MINT.
 *
 * Return TRUE on success, FALSE on failure.
 */

Boolean MINT_load_CHeB_grid(struct stardata_t * const stardata,
                            const int when)
{
    const int vb = 0;

    /*
     * Make lists of parameter and data item names
     * we require in our table of scalars
     */
#undef X
#define X(NAME,ACTION) Stringify_expanded(NAME),
    static const char * parameter_names[] =
        { MINT_CHeB_PARAMETER_ITEMS_LIST };
    static const char * data_names[] =
        { MINT_CHeB_DATA_ITEMS_LIST };
#undef X

    const Boolean ret = MINT_Load_Table(
        stardata,
        MINT_TABLE_CHeB,
        NULL,
        NULL,
        "MINT_CHeB",
        parameter_names,
        data_names,
        MINT_CHeB_parameter_actions,
        MINT_CHeB_data_actions,
        when,
        NULL,
        vb);

    if(MINT_has_table(MINT_TABLE_CHeB))
    {
        /*
         * Check derivative
         */
        struct data_table_t * const table = stardata->store->MINT_tables[MINT_TABLE_CHeB];
        double Ywas = 1.0;
        double twas = 0.0;
        double Yinit = -1.0;

        size_t line = table->nlines - 1;
        while(line != (size_t)-1)
        {
            double * const data = table->data + line * (table->nparam + table->ndata);
            const double Y = data[1];
            if(Yinit < 0.0) Yinit = Y;
            const double t = data[4];
            const double dYdt_table = -exp10(data[21]);

            const double dY = Y - Ywas;
            const double dt = t - twas;

            if(vb)
                printf("Line %zu/%zu : t=%g (was %g) dt = %g, Y = %g dY = %g ",
                       line,
                       table->nlines,
                       t,
                       twas,
                       dt,
                       Y,
                       dY);
            if(dt > TINY)
            {
                const double dYdt =
                    line > 0 ?
                    ((Y - Ywas)/(t-twas)) :
                    0.0;
                if(vb)
                    printf("dY/dt table = %g mine = %g (tscls %g %g)",
                           dYdt_table,
                           dYdt,
                           -Yinit/dYdt_table,
                           -Yinit/dYdt
                        );
                twas = t;
                Ywas = Y;

                /* replace? */
                //data[21] = log10(-dYdt);
            }
            else if(dt<-TINY)
            {
                line = 0;
            }
            if(vb) printf("\n");
            line--;
        }
    }

    return ret;
}

#endif // MINT
