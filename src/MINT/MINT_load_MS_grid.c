#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT
#include "MINT.h"
#include "MINT_load_grid.h"

/*
 * Subroutine to load main-sequence data
 * for MINT.
 *
 * Return TRUE on success, FALSE on failure.
 */

Boolean MINT_load_MS_grid(struct stardata_t * const stardata,
                          const int when)
{
    const int vb = 0;


    /*
     * Make lists of parameter and data item names
     * we require in our table of scalars
     */
#undef X
#define X(NAME,ACTION) Stringify_expanded(NAME),
    static const char * parameter_names[] =
        { MINT_MS_PARAMETER_ITEMS_LIST };
    static const char * data_names[] =
        { MINT_MS_DATA_ITEMS_LIST };
#undef X

    const Boolean ret = MINT_Load_Table(
        stardata,
        MINT_TABLE_MS,
        NULL,
        NULL,
        "MINT_MS",
        parameter_names,
        data_names,
        MINT_MS_parameter_actions,
        MINT_MS_data_actions,
        when,
        NULL,
        vb);

    return ret;
}

#endif // MINT
