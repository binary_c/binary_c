#include "../binary_c.h"

No_empty_translation_unit_warning;

#if defined MINT && defined NUCSYN

void MINT_nucsyn_TPAGB_post(struct stardata_t * Restrict const stardata Maybe_unused,
                            struct star_t * Restrict const star)
{

    const double mc_He = star->core_mass[CORE_He];
    Foreach_shell(shell)
    {
        /*
         * Use binary_c's TPAGB routine to set
         * the abundances
         */
        if(shell->m > mc_He)
        {
            Copy_abundances(star->Xenv,
                            shell->X);
        }
    }
}
#endif // MINT && NUCSYN
