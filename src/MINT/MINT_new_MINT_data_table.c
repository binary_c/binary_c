#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT
#include "MINT.h"
#include "MINT_load_grid.h"

struct data_table_t * MINT_new_MINT_data_table(void)
{
    /*
     * Make and return a new MINT data table
     */
    struct data_table_t * table = Calloc(1,sizeof(struct data_table_t));
    table->metadata = Calloc(1,sizeof(struct mint_table_metadata_t));
    return table;
}
#endif // MINT
