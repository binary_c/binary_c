#include "../binary_c.h"

No_empty_translation_unit_warning;

#ifdef MINT

/*
 * Add a new shell to the surface of the star.
 *
 * If shell is non-NULL, copy its contents into the new shell,
 * otherwise clean the shell so it has nothing in it.
 *
 * Note: there is no information in the new shell: it is
 * up to the calling function to set this.
 *
 * Returns TRUE when the shell was added, FALSE otherwise.
 */

Boolean MINT_add_shell(struct stardata_t * const Restrict stardata,
                       struct star_t * const star,
                       const struct mint_shell_t * const shell)
{
    const Boolean vb = FALSE;
    const Shell_index new_nshells = star->mint->nshells + 1;
    if(new_nshells >= MINT_HARD_MAX_NSHELLS)
    {
        Exit_binary_c(BINARY_C_OUT_OF_RANGE,
                      "Tried to add a new shell to star %d which would then have more shells than allowed by the hard limit (MINT_HARD_MAX_NSHELLS = %d)\n",
                      star->starnum,
                      (int)MINT_HARD_MAX_NSHELLS);
    }
    else if(new_nshells > stardata->preferences->MINT_maximum_nshells)
    {
        /*
         * Hit the soft limit: return
         */
        return FALSE;
    }
    else
    {
        /*
         * Increase size of the shells array
         */
        star->mint->shells =
            Realloc(star->mint->shells,
                    new_nshells * sizeof(struct mint_shell_t));

        if(star->mint->shells == NULL)
        {
            Exit_binary_c(BINARY_C_MALLOC_FAILED,
                          "Failed to allocate memory for a new shell in star %d (currently has %d shells, new shell is %d)\n",
                          star->starnum,
                          star->mint->nshells,
                          new_nshells-1);
        }
        else
        {
            if(vb)printf("mint add shell %d\n",new_nshells-1);
            struct mint_shell_t * newshell = &star->mint->shells[new_nshells-1];
            if(shell != NULL)
            {
                MINT_copy_shell(shell,
                                newshell);
            }
            else
            {
                MINT_clean_shell(newshell);
            }
            star->mint->nshells = new_nshells;
            if(vb)printf("nshells now %d\n",star->mint->nshells);
            MINT_label_shell_masses(stardata,star);
            return TRUE;
        }
    }
}

#endif // MINT
