#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT

void MINT_make_massless_remnant(struct stardata_t * const stardata,
                                struct star_t * const star)
{
    /*
     * make the star a massless remnant for MINT
     */
    stellar_structure_make_massless_remnant(stardata,star);
    Safe_free(star->mint);
}

#endif // MINT
