#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT

#include "MINT_read_header.h"

void MINT_read_header(struct stardata_t * const stardata Maybe_unused,
                      struct binary_c_stream_t * const stream,
                      struct data_table_analysis_t ** analysis_p Maybe_unused,
                      struct mint_header_t ** MINT_header,
                      const char * const table_label,
                      const Boolean vb)
{
    /*
     * Routine to read a MINT datafile header
     *
     * stream is an open binary_c_stream ready to read
     *
     * MINT_header is a header structure, allocated here.
     *
     * TODO: set data in analysis here.
     */
    Boolean loop = TRUE;
    enum {
        TASK_NONE,
        TASK_COLUMN_DESCRIPTORS,
        TASK_CHEBYSHEV
    };
    int task = TASK_NONE;

    if(vb)printf("reading header from %p at %s\n",
                 (void*)stream,
                 stream ? stream->name : NULL);
    /*
     * Make new header struct
     */
    struct mint_header_t * const header =
        *MINT_header =
        Malloc(sizeof(struct mint_header_t));

    /*
     * Make cdict variable, we refer to it as "h"
     * but it points to inside the MINT_header
     */
    CDict_new(h);
    h->force_maps = FALSE;
    header->cdict = h;

    /*
     * Save the filename in the metadata.
     * Use CDict_string() to make sure the
     * copied string is freed automatically.
     */
    CDict_nest(h,
               "filename",
               CDict_string(h,stream->name),
               "metadata");

    FILE * const fp = stream->fp;
    Boolean found_correct_header = FALSE;
    char * line = NULL;
    struct string_array_t * string_array = new_string_array(0);

    while(loop == TRUE)
    {
        line = file_splitline(stardata,
                              fp,
                              &line,
                              string_array,
                              0,
                              ' ',
                              0,
                              0);

        if(vb)printf("LINE nstrings %zd strings [0] %s, [1] %s, [2] %s -> header? %s Is Chebyshev grid? %s\n",
                     string_array->n,
                     string_array->strings[0],
                     string_array->n > 1 ? string_array->strings[1] : NULL,
                     string_array->n > 2 ? string_array->strings[2] : NULL,
                     Yesno(Is_header_line),
                     Yesno(Match_header("CHEBYSHEV_GRID"))
            );

        if(string_array->n > 0 &&
           Is_header_line)
        {
            /*
             * Header comment: parse it
             */
            if(task == TASK_NONE)
            {
                /*
                 * Check we're the right datafile
                 */
                if(Match_header("MINT","DATAFILE",table_label))
                {
                    found_correct_header = TRUE;
                }
                else if(found_correct_header == TRUE)
                {
                    /*
                     * Start of column descriptiors
                     */
                    if(Match_header("START","MINT","COLUMN","DESCRIPTORS"))
                    {
                        task = TASK_COLUMN_DESCRIPTORS;
                        if(vb) printf("Match TASK_COLUMN_DESCRIPTORS\n");
                    }
                    /*
                     * Special cases
                     */
                    else if(Match_header("METADATA_NUMBER_OF_LINES"))
                    {
                        /*
                         * Read in number of data lines to save us
                         * having to count them
                         */
                        if(string_array->n > 1)
                        {
                            struct long_int_array_t * long_int_array =
                                string_array_to_long_int_array(string_array);
                            const long int number_of_data_lines = long_int_array->long_ints[2];
                            CDict_nest_set(h,
                                           "number of data lines",
                                           (long int)number_of_data_lines,
                                           "metadata");
                            free_long_int_array(&long_int_array);
                        }
                    }
                    else if(Match_header("METADATA_START_DATA_OFFSET_BYTES"))
                    {
                        /*
                         * Read in start of data offset bytes, i.e. end of
                         * the header location.
                         */
                        if(string_array->n > 1)
                        {
                            struct long_int_array_t * long_int_array =
                                string_array_to_long_int_array(string_array);
                            const long int start_offset_bytes = long_int_array->long_ints[2];
                            CDict_nest_set(h,
                                           "data start offset bytes",
                                           (long int)start_offset_bytes,
                                           "metadata");
                            free_long_int_array(&long_int_array);
                        }
                    }
                    else if(Match_header("METADATA_LINE_LENGTH_BYTES"))
                    {
                        /*
                         * Read in line lengths and convert to offset bytes.
                         */
                        if(string_array->n > 1)
                        {
                            struct long_int_array_t * long_int_array =
                                string_array_to_long_int_array(string_array);
                            long int * const offsets = long_int_array->long_ints;
                            for(ssize_t i=3; i<long_int_array->n; i++)
                            {
                                offsets[i] += offsets[i-1];
                            }
                            CDict_nest_set(h,
                                           "data line offset bytes",
                                           (long int*)offsets+2,
                                           "metadata"
                                );
                            CDict_add_to_free_list(h,offsets);
                            Safe_free_nocheck(long_int_array);
                        }
                    }
                    else if(Match_header("METADATA_LINE_OFFSET_DIFF_BYTES"))
                    {
                        /*
                         * Read in line lengths and convert to offset bytes.
                         * Each is the diff of itself and the previous, so
                         * we have to sum the offset.
                         */
                        if(string_array->n > 1)
                        {
                            struct long_int_array_t * long_int_array =
                                string_array_to_long_int_array(string_array);
                            long int * offsets = long_int_array->long_ints;
                            size_t offset = 0;
                            for(ssize_t i=3; i<long_int_array->n; i++)
                            {
                                offset += offsets[i];
                                offsets[i] = offsets[i-1] + offset;
                            }
                            CDict_add_to_free_list(h,offsets);
                            offsets += 2;
                            CDict_nest_set(h,
                                           "data line offset bytes",
                                           (long int*)offsets,
                                           "metadata"
                                );

                            Safe_free_nocheck(long_int_array);
                        }
                    }
                    else if(Match_header("METADATA_LINE_OFFSET_BYTES"))
                    {
                        /*
                         * Read in offset bytes array
                         */
                        if(string_array->n > 1)
                        {
                            struct long_int_array_t * long_int_array =
                                string_array_to_long_int_array(string_array);
                            long int * const offsets = long_int_array->long_ints;
                            CDict_nest_set(h,
                                           "data line offset bytes",
                                           (long int*)offsets,
                                           "metadata");
                            CDict_add_to_free_list(h,offsets);
                            Safe_free_nocheck(long_int_array);
                        }
                    }
                    else if(Match_header("METADATA_PARAMETER_VALUES"))
                    {
                        /*
                         * Read in the possible values of a parameter.
                         */
                        if(string_array->n > 1)
                        {
                            struct double_array_t * double_array =
                                string_array_to_double_array(string_array,NAN);
                            const int parameter_number = (int)(0.1+double_array->doubles[2]);
                            double * const values = double_array->doubles + 3;
                            CDict_nest_set(h,
                                           (int)parameter_number, // parameter number
                                           (double*)values,
                                           "metadata",
                                           "parameter values");
                            CDict_add_to_free_list(h,double_array->doubles);
                            Safe_free_nocheck(double_array);
                        }
                    }
                    else if(Match_header("METADATA_PARAMETER_NVALUES"))
                    {
                        /*
                         * Read in the number of values of a parameter.
                         */
                        if(string_array->n > 1)
                        {
                            struct long_int_array_t * long_int_array =
                                string_array_to_long_int_array(string_array);
                            const long int * nvalues = (long int*)(long_int_array->long_ints) + 2;
                            CDict_nest_set(h,
                                           "parameter nvalues",
                                           (long int *)nvalues,
                                           "metadata");
                            CDict_add_to_free_list(h,long_int_array->long_ints);
                            Safe_free_nocheck(long_int_array);
                        }
                    }
                    else if(Match_header("METADATA_PARAMETER_SKIP"))
                    {
                        /*
                         * Read in the skip of the parameter.
                         */
                        if(string_array->n > 1)
                        {
                            struct long_int_array_t * long_int_array =
                                string_array_to_long_int_array(string_array);
                            long int * const skip = long_int_array->long_ints + 2;
                            CDict_nest_set(h,
                                           "parameter skip",
                                           (long int*)skip,
                                           "metadata");
                            CDict_add_to_free_list(h,long_int_array->long_ints);
                            Safe_free_nocheck(long_int_array);
                        }
                    }
                    else if(Match_header("CHEBYSHEV_GRID"))
                    {
                        if(vb) printf("Match task CHEBYSHEV_GRID\n");
                        /*
                         * Free memory
                         */
                        Safe_free(line);

                        /*
                         * The next line contains the masses of the
                         * Chebyshev grid points.
                         */
                        line = file_splitline(stardata,
                                              fp,
                                              &line,
                                              string_array,
                                              '#',
                                              ' ',
                                              0,
                                              0);

                        if(vb)
                        {
                            printf("Process Chebyshev line with %zd strings\n",
                                   string_array->n);
                        }

                        if(string_array->n > 1)
                        {
                            /*
                             * We have the Chebyshev masses as strings,
                             * convert to array of doubles and then put this
                             * in the table.
                             */
                            double chebyshev_grid[string_array->n];
                            struct double_array_t * double_array =
                                string_array_to_double_array(string_array,NAN);
                            memcpy(chebyshev_grid,
                                   double_array->doubles,
                                   sizeof(double)*(string_array->n));
                            free_double_array(&double_array);
                            CDict_nest(h,
                                       "number of masses",
                                       (int)(string_array->n),
                                       "Chebyshev grid"
                                );
                            CDict_nest(h,
                                       "masses",
                                       chebyshev_grid,
                                       "Chebyshev grid"
                                );
                        }
                    }
                }
            }
            else if(task == TASK_COLUMN_DESCRIPTORS)
            {
                /*
                 * Is this line a column descriptor?
                 */
                if(Match_header("END","MINT","COLUMN","DESCRIPTORS"))
                {
                    task = TASK_NONE;
                    if(vb)printf("match TASK NONE\n");
                }
                /*
                 * Default to column descriptors
                 */
                else
                {


                    if(vb)
                    {
                        printf("Check %zd : strings 1 = \"%s\", strings 2 = \"%s\", strings[2][0] = %c\n",
                               string_array->n,
                               string_array->n > 1 ? string_array->strings[1] : NULL,
                               string_array->n > 1 ? string_array->strings[2] : NULL,
                               string_array->n > 1 ? string_array->strings[2][0] : ' ');
                    }
                    if(string_array->n > 1 &&
                       isdigit(string_array->strings[2][0]))
                    {
                        /*
                         * Yes, it's a column descriptor:
                         * string_array->strings[1] == column name
                         * string_array->strings[2] == column numbers
                         */
                        const char * colname = string_array->strings[1];
                        if(vb)printf("column: descriptor %s ",
                                     colname);

                        int low,high;
                        int_range_from_string(stardata,
                                              string_array->strings[2],
                                              &low,
                                              &high);
                        if(vb)
                        {
                            printf("low - high = %d - %d\n",low,high);
                        }

                        /*
                         * Set in the settings cdict
                         */
                        CDict_nest_set(h,
                                       "low",
                                       (int)low,
                                       "columns",
                                       CDict_string(h,(char*)colname)
                            );
                        CDict_nest_set(h,
                                       "high",
                                       (int)high,
                                       "columns",
                                       CDict_string(h,(char*)(colname))
                            );
                        CDict_nest_set(h,
                                       "number of columns",
                                       (int)Max(low,high),
                                       "metadata"
                            );
                    }
                }
            }

        }
        else
        {
            /*
             * Done with header processing
             */
            loop = FALSE;
        }

        Safe_free(line);
    }
    free_string_array(&string_array);

    if(vb)
    {
        printf("done MINT_read_header\n");
        char * json_buffer = NULL;
        size_t json_size = 0;
        CDict_to_JSON(h,
                       json_buffer,
                       json_size,
                       CDICT_JSON_WHITESPACE);
        printf("%s\n",json_buffer);
        Safe_free(json_buffer);
    }

    if(found_correct_header == FALSE)
    {
        Exit_binary_c(BINARY_C_DATA_FAILED_TO_LOAD,
                      "MINT_read_header file \"%s\" with the correct table label: we were looking for \"%s\"\n",
                      stream->name,
                      table_label);
    }
}
#endif // MINT
