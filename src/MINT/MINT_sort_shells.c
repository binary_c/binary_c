#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT

#ifdef __HAVE_GNU_QSORT_R
static int MINT_sort_shells_comparator(const void * pa,
                                       const void * pb,
                                       void * arg);
#endif // __HAVE_GNU_QSORT_R
#ifdef __HAVE_BSD_QSORT_R
static int MINT_sort_shells_comparator(void * arg,
                                       const void * pa,
                                       const void * pb);
#endif // __HAVE_BSD_QSORT_R

struct MINT_sort_shells_compartor_struct_t {
    long int offset;
    int direction;
};

void MINT_sort_shells(struct stardata_t * const stardata Maybe_unused,
                      struct star_t * const star,
                      const long int offset,
                      int direction)
{
    /*
     * Sort the shells in star according to the double-precision
     * variable located at offset from the beginning of each shell
     */
    const struct MINT_sort_shells_compartor_struct_t c = {
        offset,
        direction
    };

    qsort_r(
        star->mint->shells,
        star->mint->nshells,
        sizeof(struct mint_shell_t),
#ifdef __HAVE_GNU_QSORT_R
        &MINT_sort_shells_comparator,
        (void*)&c
#endif
#ifdef __HAVE_BSD_QSORT_R
        (void*)&c,
        &MINT_sort_shells_comparator
#endif
        );
}

#ifdef __HAVE_GNU_QSORT_R
static int MINT_sort_shells_comparator(const void * pa,
                                       const void * pb,
                                       void * arg)
#endif // __HAVE_GNU_QSORT_R
#ifdef __HAVE_BSD_QSORT_R
static int MINT_sort_shells_comparator(void * arg,
                                       const void * pa,
                                       const void * pb)
#endif // __HAVE_BSD_QSORT_R
{
    const struct MINT_sort_shells_compartor_struct_t * c = arg;
    const double a = MINT_var_at_offset(pa,0,c->offset);
    const double b = MINT_var_at_offset(pb,0,c->offset);
    return a>b ? +c->direction : a<b ? -c->direction : 0;
}

#endif //MINT
