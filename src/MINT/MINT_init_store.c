#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT

void MINT_init_store(struct store_t * store)
{
    /*
     * Set up the MINT store
     */
    unsigned int i;
    for(i=0; i<MINT_TABLE_NUMBER; i++)
    {
        store->MINT_tables[i] = NULL;
        store->MINT_initial_coordinate[i] = -1.0;
    }
    store->MINT_loaded = FALSE;
}

#endif // MINT
