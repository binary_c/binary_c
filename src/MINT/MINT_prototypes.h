#pragma once
#ifndef MINT_PROTOTYPES_H
#define MINT_PROTOTYPES_H

/*
 * Setup functions
 */
void MINT_init(struct stardata_t * const stardata);
void MINT_init_stars(struct stardata_t * const stardata);
void MINT_init_star(struct stardata_t * const stardata,
                    struct star_t * const star);
Boolean MINT_could_data_be_available(struct stardata_t * const stardata,
                                     struct star_t * const star,
                                     const int index);
void MINT_make_generic_map(struct stardata_t * const stardata);
void MINT_check_variables(struct stardata_t * const stardata);

#ifdef MINT
void MINT_alloc(struct stardata_t * const stardata);
void MINT_clear(struct stardata_t * const stardata);

int MINT_load_grid(struct stardata_t * const stardata);
Boolean MINT_load_no_grid(struct stardata_t * const stardata Maybe_unused,
                          const int when Maybe_unused);
void MINT_init_store(struct store_t * store);
void MINT_free(struct stardata_t * const Restrict stardata);
void MINT_free_star(struct star_t * const Restrict star);
void MINT_free_store(struct store_t * const store);
void MINT_free_tmpstore(struct tmpstore_t * const tmpstore);
void MINT_free_persistent_data(struct stardata_t * const stardata);
void MINT_copy_star(struct stardata_t * const stardata,
                    const struct star_t * const Restrict src,
                    struct star_t * const Restrict dest,
                    struct mint_shell_t * const mint_shells_p,
                    const Shell_index mint_nshells);

void MINT_load_data(struct stardata_t * const stardata);
void MINT_label_shell_masses(struct stardata_t * const stardata Maybe_unused,
                             struct star_t * const star);
void MINT_read_header(struct stardata_t * const stardata Maybe_unused,
                      struct binary_c_stream_t * const stream,
                      struct data_table_analysis_t ** analysis_p Maybe_unused,
                      struct mint_header_t ** MINT_header,
                      const char * const table_label,
                      Boolean vb);
char * MINT_filename(struct stardata_t * const stardata,
                     const char * const data_directory,
                     const unsigned int table_id);
/*
 * Grid load functions
 */
Boolean MINT_table_loader(struct stardata_t * const stardata,
                          const unsigned int MINT_table_id,
                          struct data_table_t * data_table,
                          char * filename,
                          const char * header_string,
                          const char ** parameter_names,
                          const size_t n_parameter_names,
                          const char ** data_names,
                          const size_t n_data_names,
                          const int * parameter_actions,
                          const int * data_actions,
                          const int when,
                          struct data_table_defer_instructions_t * instructions,
                          int vb);
Boolean MINT_load_MS_grid(struct stardata_t * const stardata,
                          const int when);
Boolean MINT_load_GB_grid(struct stardata_t * const stardata,
                          const int when);
Boolean MINT_update_GB_table(struct stardata_t * const stardata,
                             struct star_t * const star);
Boolean MINT_make_ZAGB_table(struct stardata_t * const stardata,
                             const int when);
Boolean MINT_load_ZAMS_composition_grid(struct stardata_t * const stardata,
                                        const int when);
Boolean MINT_make_ZAMS_table(struct stardata_t * const stardata,
                             const int when);
Boolean MINT_make_TAMS_table(struct stardata_t * const stardata,
                             const int when);
Boolean MINT_load_CHeB_grid(struct stardata_t * const stardata,
                            const int when);
Boolean MINT_make_CHeB_ZA_table(struct stardata_t * const stardata,
                                const int when);
Boolean MINT_make_CHeB_TA_table(struct stardata_t * const stardata,
                                const int when);
Boolean MINT_load_COWD_grid(struct stardata_t * const stardata,
                            const int when);
void MINT_map_COWD_table(struct stardata_t * const stardata);

/*
 * Stellar structure
 */
int MINT_stellar_structure(struct stardata_t *const stardata,
                           struct star_t *const oldstar,
                           struct star_t * const newstar,
                           const Caller_id caller_id);

int MINT_call_BSE_stellar_structure(struct stardata_t * const stardata,
                                    struct star_t * const oldstar,
                                    struct star_t * const newstar,
                                    const Caller_id caller_id Maybe_unused);

Stellar_type MINT_stellar_structure_MS(struct stardata_t * Restrict const stardata,
                                       struct star_t * const prevstar,
                                       struct star_t * const newstar,
                                       const Caller_id caller_id Maybe_unused);
Stellar_type MINT_stellar_structure_HG(struct stardata_t * Restrict const stardata,
                                       struct star_t * const prevstar,
                                       struct star_t * const newstar,
                                       const Caller_id caller_id Maybe_unused);
Stellar_type MINT_stellar_structure_GB(struct stardata_t * Restrict const stardata,
                                       struct star_t * const prevstar,
                                       struct star_t * const newstar,
                                       const Caller_id caller_id Maybe_unused);
Stellar_type MINT_stellar_structure_CHeB(struct stardata_t * Restrict const stardata,
                                         struct star_t * const prevstar,
                                         struct star_t * const newstar,
                                         const Caller_id caller_id Maybe_unused);
Stellar_type MINT_stellar_structure_EAGB(struct stardata_t * Restrict const stardata,
                                         struct star_t * const prevstar,
                                         struct star_t * const newstar,
                                         const Caller_id caller_id Maybe_unused);
Stellar_type MINT_stellar_structure_TPAGB(struct stardata_t * Restrict const stardata,
                                          struct star_t * const prevstar,
                                          struct star_t * const newstar,
                                          const Caller_id caller_id Maybe_unused);
Stellar_type MINT_stellar_structure_HeMS(struct stardata_t * Restrict const stardata,
                                         struct star_t * const prevstar,
                                         struct star_t * const newstar,
                                         const Caller_id caller_id Maybe_unused);
Stellar_type MINT_stellar_structure_HeHG(struct stardata_t * Restrict const stardata,
                                         struct star_t * const prevstar,
                                         struct star_t * const newstar,
                                         const Caller_id caller_id Maybe_unused);
Stellar_type MINT_stellar_structure_HeGB(struct stardata_t * Restrict const stardata,
                                         struct star_t * const prevstar,
                                         struct star_t * const newstar,
                                         const Caller_id caller_id Maybe_unused);
Stellar_type MINT_stellar_structure_HeWD(struct stardata_t * Restrict const stardata,
                                         struct star_t * const prevstar,
                                         struct star_t * const newstar,
                                         const Caller_id caller_id Maybe_unused);
Stellar_type MINT_stellar_structure_COWD(struct stardata_t * Restrict const stardata,
                                         struct star_t * const prevstar,
                                         struct star_t * const newstar,
                                         const Caller_id caller_id Maybe_unused);
Stellar_type MINT_stellar_structure_ONeWD(struct stardata_t * Restrict const stardata,
                                          struct star_t * const prevstar,
                                          struct star_t * const newstar,
                                          const Caller_id caller_id Maybe_unused);
Stellar_type MINT_stellar_structure_NS(struct stardata_t * Restrict const stardata,
                                       struct star_t * const prevstar,
                                       struct star_t * const newstar,
                                       const Caller_id caller_id Maybe_unused);
Stellar_type MINT_stellar_structure_BH(struct stardata_t * Restrict const stardata,
                                       struct star_t * const prevstar,
                                       struct star_t * const newstar,
                                       const Caller_id caller_id Maybe_unused);
Stellar_type MINT_stellar_structure_massless_remnant(struct stardata_t * Restrict const stardata Maybe_unused,
                                                     struct star_t * const prevstar Maybe_unused,
                                                     struct star_t * const newstar,
                                                     const Caller_id caller_id Maybe_unused);



Boolean MINT_rejuvenate_MS(struct stardata_t * const stardata,
                           struct star_t * star_was,
                           struct star_t * star_is);

/*
 * Stellar-evolution phase functions
 */
double MINT_MS_lifetime(struct stardata_t * const stardata,
                        struct star_t * const star);
void MINT_CHeB_ZA(struct stardata_t * const stardata,
                  const double mass,
                  double * const CHeB_ZA_luminosity);
void MINT_CHeB_TA(struct stardata_t * const stardata,
                  const double mass,
                  double * const CHeB_lifetime,
                  double * const CHeB_TA_luminosity);
/*
 * Initial conditions
 */
double MINT_initial_XHc(struct stardata_t * const stardata);

void MINT_make_massless_remnant(struct stardata_t * const stardata,
                                struct star_t * const star);

/*
 * Meshing
 */
void MINT_remesh(struct stardata_t * const stardata,
                 struct star_t * const star);
void MINT_split_shell(struct stardata_t * const stardata Maybe_unused,
                      struct star_t * const star Maybe_unused,
                      const int k);
void MINT_merge_shells(struct stardata_t * const stardata Maybe_unused,
                       struct star_t * const star Maybe_unused,
                       struct mint_shell_t * const from,
                       struct mint_shell_t * const into);
void MINT_resolve_core_boundary(struct stardata_t * Restrict const stardata Maybe_unused,
                                struct star_t * const star,
                                const double band_centre_mass,
                                const double band_thickness_mass,
                                const double thinnest_dm,
                                const double default_dm,
                                const Boolean bail_above_core);
/*
 * Derivative application function
 */
Boolean MINT_apply_stellar_derivatives(struct stardata_t * const stardata,
                                       const double dt,
                                       const Boolean can_reject,
                                       const Boolean can_reject_and_shorten);

/*
 * Derivative check functions
 */

Pure_function
Boolean MINT_derivative_check_stellar_central_hydrogen(struct stardata_t * const stardata Maybe_unused,
                                                       void * const data Maybe_unused,
                                                       const double previous_value Maybe_unused,
                                                       const double value,
                                                       const double derivative Maybe_unused,
                                                       const double dt Maybe_unused,
                                                       const Derivative nd Maybe_unused,
                                                       const Derivative_group derivative_group Maybe_unused);

Pure_function
Boolean MINT_derivative_check_stellar_central_helium(struct stardata_t * stardata Maybe_unused,
                                                     void * data Maybe_unused,
                                                     const double previous_value Maybe_unused,
                                                     const double value,
                                                     const double derivative Maybe_unused,
                                                     const double dt Maybe_unused,
                                                     const Derivative nd Maybe_unused,
                                                     const Derivative_group derivative_group Maybe_unused);


Pure_function
Boolean MINT_derivative_check_stellar_central_carbon(struct stardata_t * const stardata Maybe_unused,
                                                     void * const data Maybe_unused,
                                                     const double previous_value Maybe_unused,
                                                     const double value,
                                                     const double derivative Maybe_unused,
                                                     const double dt Maybe_unused,
                                                     const Derivative nd Maybe_unused,
                                                     const Derivative_group derivative_group Maybe_unused);

Pure_function
Boolean MINT_derivative_check_stellar_central_oxygen(struct stardata_t * stardata Maybe_unused,
                                                     void * data Maybe_unused,
                                                     const double previous_value Maybe_unused,
                                                     const double value,
                                                     const double derivative Maybe_unused,
                                                     const double dt Maybe_unused,
                                                     const Derivative nd Maybe_unused,
                                                     const Derivative_group derivative_group Maybe_unused);


Pure_function
Boolean MINT_derivative_check_stellar_central_degeneracy(struct stardata_t * const stardata Maybe_unused,
                                                         void * const data Maybe_unused,
                                                         const double previous_value Maybe_unused,
                                                         const double value Maybe_unused,
                                                         const double derivative Maybe_unused,
                                                         const double dt Maybe_unused,
                                                         const Derivative nd Maybe_unused,
                                                         const Derivative_group derivative_group Maybe_unused);

/*
 * Stellar merging
 */
void MINT_mix_stars(struct stardata_t * const stardata,
                    const Boolean eject);
void MINT_mix_stars_MS(struct stardata_t * const stardata,
                       const Boolean eject);
void MINT_merge_stars_shells_MS(struct stardata_t * const stardata,
                                struct star_t * const star1,
                                struct star_t * const star2,
                                struct star_t * const newstar);
#ifdef NUCSYN
double MINT_core(struct star_t * star,
                 Isotope n,
                 Abundance threshold);
Shell_index MINT_core_index(struct star_t * star,
                            Isotope n,
                            Abundance threshold);
void MINT_shellular_burning(struct stardata_t * Restrict const stardata,
                            struct star_t * const newstar,
                            struct mint_shell_t * const shell,
                            struct mint_shell_t * const prevshell,
                            const Shell_index k,
                            double * const convective_sigmav,
                            double * const Xconv,
                            double * const mconv);

struct mint_shell_t * MINT_mixed_shell(struct stardata_t * const stardata,
                                       struct star_t * const star,
                                       const double m_low,
                                       const double m_high);
double MINT_mixed_abundance(struct stardata_t * const stardata,
                            struct star_t * const star,
                            const double m_low,
                            const double m_high,
                            Isotope i);
void MINT_nucsyn(struct stardata_t * const Restrict stardata);
void MINT_X_from_surface(const struct stardata_t * const Restrict stardata Maybe_unused,
                         const struct star_t * const star,
                         const double dm_skip,
                         const double dm,
                         Abundance * const X);

void MINT_binary(struct stardata_t * const Restrict stardata);
void MINT_radioactive_decay(struct stardata_t * const Restrict stardata);
void MINT_thermohaline(struct stardata_t * const stardata,
                       struct star_t * const star);
void MINT_renormalize_shells(struct star_t * const star);

void MINT_nucsyn_MS_pre(struct stardata_t * Restrict const stardata,
                        struct star_t * Restrict const star);
void MINT_nucsyn_MS_post(struct stardata_t * Restrict const stardata,
                         struct star_t * Restrict const star);
void MINT_nucsyn_HG_pre(struct stardata_t * Restrict const stardata,
                        struct star_t * Restrict const star);
void MINT_nucsyn_GB_pre(struct stardata_t * Restrict const stardata,
                        struct star_t * Restrict const star);
void MINT_nucsyn_CHeB_pre(struct stardata_t * Restrict const stardata,
                          struct star_t * Restrict const star);
void MINT_nucsyn_EAGB_pre(struct stardata_t * Restrict const stardata,
                          struct star_t * Restrict const star);
void MINT_nucsyn_TPAGB_pre(struct stardata_t * Restrict const stardata,
                           struct star_t * Restrict const star);
void MINT_nucsyn_HeMS_pre(struct stardata_t * Restrict const stardata,
                          struct star_t * Restrict const star);
void MINT_nucsyn_HeHG_pre(struct stardata_t * Restrict const stardata,
                          struct star_t * Restrict const star);
void MINT_nucsyn_HeGB_pre(struct stardata_t * Restrict const stardata,
                          struct star_t * Restrict const star);
void MINT_nucsyn_HeWD_pre(struct stardata_t * Restrict const stardata,
                          struct star_t * Restrict const star);
void MINT_nucsyn_COWD_pre(struct stardata_t * Restrict const stardata,
                          struct star_t * Restrict const star);
void MINT_nucsyn_ONeWD_pre(struct stardata_t * Restrict const stardata,
                           struct star_t * Restrict const star);
void MINT_nucsyn_NS_pre(struct stardata_t * Restrict const stardata,
                        struct star_t * Restrict const star);
void MINT_nucsyn_BH_pre(struct stardata_t * Restrict const stardata,
                        struct star_t * Restrict const star);

#endif // NUCSYN
void MINT_Kippenhahn(struct stardata_t * Restrict const stardata,
                     struct star_t * const star);

void MINT_trim_shells(struct stardata_t * const stardata Maybe_unused,
                      struct star_t * const star);
void MINT_sort_shells(struct stardata_t * const stardata,
                      struct star_t * const star,
                      long int offset,
                      int direction);
void MINT_convection(struct stardata_t * const stardata Maybe_unused,
                     struct star_t * star);
double MINT_MS_non_eq_conv_coremass(struct stardata_t * const stardata,
                                    struct star_t * star);
void MINT_mix_star(struct stardata_t * const stardata,
                   struct star_t * const star,
                   const double lower,
                   const double upper);

/*
 * Chebyshev grid functions
 */
void MINT_MS_get_chebyshev_data(struct stardata_t * Restrict const stardata Maybe_unused,
                                struct star_t * const newstar,
                                double * const result_cheb);
int MINT_MS_set_chebyshev_data(struct stardata_t * Restrict const stardata Maybe_unused,
                               struct star_t * const newstar,
                               struct mint_shell_t * const shell,
                               const double * const result_cheb,
                               const unsigned int nin,
                               const unsigned int * const map,
                               const Boolean * const available);


void MINT_GB_get_chebyshev_data(struct stardata_t * Restrict const stardata Maybe_unused,
                                struct star_t * const newstar,
                                double * const result_cheb);
int MINT_GB_set_chebyshev_data(struct stardata_t * Restrict const stardata Maybe_unused,
                               struct star_t * const newstar,
                               struct data_table_t * const data_table,
                               struct mint_shell_t * const shell,
                               const double * const coords,
                               const double * const result_cheb,
                               const unsigned int nin,
                               const unsigned int * const map,
                               const Boolean * const available);

void MINT_CHeB_get_chebyshev_data(struct stardata_t * Restrict const stardata Maybe_unused,
                                  struct star_t * const star,
                                  double * const result_cheb);
int MINT_CHeB_set_chebyshev_data(struct stardata_t * Restrict const stardata Maybe_unused,
                                 struct star_t * const newstar,
                                 struct mint_shell_t * const shell,
                                 const double * const coords,
                                 const double * const result_cheb,
                                 const unsigned int nin,
                                 const unsigned int * const map,
                                 const Boolean * const available);


double MINT_ZAMS_core_mass_from_luminosity(struct stardata_t * const stardata,
                                           struct star_t * star,
                                           const double L);

double MINT_binding_energy(struct stardata_t * const stardata,
                           struct star_t * const star);

double MINT_partial_binding_energy(struct stardata_t * const stardata,
                                   struct star_t * const star,
                                   const double m_low,
                                   const double m_high,
                                   struct mint_shell_t ** low_shell,
                                   struct mint_shell_t ** high_shell);

Boolean MINT_add_shell(struct stardata_t * const Restrict stardata,
                       struct star_t * const star,
                       const struct mint_shell_t * const shell);

void MINT_map_star_to_BSE(struct stardata_t * stardata Maybe_unused,
                          struct star_t * oldstar,
                          struct star_t * MINT_star);
void MINT_clear_evolutionary_derivatives(struct stardata_t * const stardata,
                                         struct star_t * const star);
#ifdef __EXPERIMENTAL
void MINT_save_state(struct stardata_t * const stardata);
void MINT_load_state(struct stardata_t * const stardata);
#endif // __EXPERIMENTAL
void MINT_update_table(struct stardata_t * const stardata,
                       const struct data_table_t * const data_table,
                       const struct data_table_defer_instructions_t * instructions,
                       const struct data_table_analysis_t * const analysis,
                       struct mint_table_metadata_t * metadata,
                       const int vb);
struct data_table_analysis_t * MINT_analyse_table(struct stardata_t * const stardata,
                                                  struct mint_table_metadata_t * const metadata,
                                                  const size_t n_parameters,
                                                  const Boolean vb);
struct data_table_t * MINT_new_MINT_data_table(void);

void MINT_free_table_metadata(struct mint_table_metadata_t ** metadata_p);

#endif // MINT

#endif // MINT_PROTOTYPES_H

void Maybe_unused MINT_dummy_function(void);
