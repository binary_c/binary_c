#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT

void MINT_merge_shells(struct stardata_t * const stardata Maybe_unused,
                       struct star_t * const star Maybe_unused,
                       struct mint_shell_t * const from,
                       struct mint_shell_t * const into)
{
    /*
     * Merge shell "from" with shell "into" leaving the results in
     * "into"
     */
#define _combine(V) into->V=((from->V * from->dm + into->V * into->dm)/(from->dm + into->dm)); from->V = 0.0;

    /*
     * merge most variables by averaging by mass
     */
    _combine(T);
    _combine(rho);
    _combine(gas_pressure);
    _combine(radiation_pressure);
    _combine(total_pressure);
    _combine(radius);

#ifdef NUCSYN
    for(Isotope i=0; i<ISOTOPE_ARRAY_SIZE;i++)
    {
        _combine(X[i]);
    }
#endif // NUCSYN

    /* if one is convective, assume both are */
    into->convective = from->convective || into->convective;

    /* combine mass */
    into->dm += from->dm;
    from->dm = -1.0;

    /* reduce the shell count */
    star->mint->nshells--;
}

#endif // MINT
