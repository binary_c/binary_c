#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Initialize a star in MINT.
 *
 * Note: if you are initializing both stars,
 * us MINT_init_stars() - this will be faster.
 */
#ifdef MINT
void MINT_init_star(struct stardata_t * const stardata,
                    struct star_t * const star)
{
#ifdef NUCSYN
    double Xinit[ISOTOPE_ARRAY_SIZE];
    nucsyn_initial_abundances(stardata,
                              Xinit,
                              stardata->common.metallicity,
                              stardata->preferences->initial_abundance_mix,
                              TRUE);

#endif //NUCSYN

    star->mint->nshells = 200;
    Foreach_shell(shell)
    {
        shell->dm = star->mass / star->mint->nshells;
        shell->T = 0.0;
        shell->rho = 0.0;
        shell->convective = FALSE;
#ifdef NUCSYN
        Copy_abundances(Xinit,
                        shell->X);
#endif// NUCSYN
    }
    MINT_label_shell_masses(stardata,
                            star);


#ifdef NUCSYN
    if(stardata->preferences->MINT_use_ZAMS_profiles == TRUE)
    {
        double * result_cheb = Malloc(MINT_result_size(MINT_TABLE_ZAMS_COMPOSITION));
        unsigned int n = 0;
        struct mint_table_metadata_t * const metadata =
            stardata->store->MINT_tables[MINT_TABLE_ZAMS_COMPOSITION]->metadata;
        const double * const coords = CDict_nest_get_entry(
            metadata->header->cdict,
            "Chebyshev grid",
            "masses"
            )->value.value.double_array_data;
        const unsigned int ncheb = CDict_nest_get_entry(
            metadata->header->cdict,
            "Chebyshev grid",
            "number of masses"
            )->value.value.int_data;
        const unsigned int * const map = metadata->data_map;
        const Boolean * const available = metadata->data_available;

        /*
         * Interpolation in the Chebyshev grid:
         * either linear or power law
         */
#define _interpolate_linear(F,INDEX)            \
        ((1.0-(F))*result_cheb[(INDEX)+n-1] +   \
         (F)*result_cheb[(INDEX)+n])
#define _interpolate_powerlaw(F,INDEX)                      \
        (exp10(                                             \
            ((1.0-(F))*log10(result_cheb[(INDEX)+n-1]) +    \
             (F)*log10(result_cheb[(INDEX)+n]))             \
            ))
#define _interpolate(F,INDEX)                   \
        (_interpolate_linear((F),(INDEX)))

/*
 * __interpolate returns the data or 0.0 if not available
 */
#define __interpolate(INDEX)                    \
        (available[(INDEX)] == TRUE             \
         ? (_interpolate(f,map[(INDEX)]))       \
         : 0.0)

        Interpolate(stardata->store->MINT_tables[MINT_TABLE_ZAMS_COMPOSITION],
                    ((const double[]){
                        log10(star->mass)
                    }),
                    result_cheb,
                    FALSE);

        Foreach_shell(shell)
        {
            const double qcoord = shell->m / star->mass; /* relative mass coord */
            while(coords[n] < qcoord &&
                  n < ncheb - 1)
            {
                n++;
            }
            if(n==0) n=1;
            if(unlikely(n>=ncheb)) n = ncheb - 1; /* should not happen */
            const double f = Limit_range((qcoord - coords[n-1]) / (coords[n] - coords[n-1]),
                                         0.0,1.0);

#define __set(ISOTOPE)                                                  \
                                                                        \
            if(available[MINT_ZAMS_COMPOSITION_CHEBYSHEV_##ISOTOPE##_ZAMS]) \
            {                                                           \
                shell->X[X##ISOTOPE] = exp10(__interpolate(MINT_ZAMS_COMPOSITION_CHEBYSHEV_##ISOTOPE##_ZAMS)); \
            }

            __set(H1);
            __set(H2);
            __set(He3);
            __set(He4);
            __set(Li7);
            __set(C12);
            __set(C13);
            __set(N14);
            __set(N15);
            __set(O16);
            __set(O17);
            __set(O18);
            __set(F19);
            __set(Ne20);
            __set(Mg24);
        }
        Safe_free(result_cheb);
    } // use_ZAMS_profiles check
#endif//NUCSYN
}
#endif//MINT
