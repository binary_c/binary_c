#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT

void MINT_free_star(struct star_t * const Restrict star)
{
    /*
     * Free memory allocated for MINT in each star struct
     *
     * Note that we do NOT want to free the data tables
     * stored in the star->mint struct.
     */
    if(star != NULL &&
       star->mint != NULL)
    {
        Safe_free(star->mint->shells);
        Safe_free(star->mint);
    }
}

#endif // MINT
