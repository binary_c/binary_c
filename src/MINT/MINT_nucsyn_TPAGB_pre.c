#include "../binary_c.h"

No_empty_translation_unit_warning;

#if defined MINT && defined NUCSYN

void MINT_nucsyn_TPAGB_pre(struct stardata_t * Restrict const stardata Maybe_unused,
                           struct star_t * Restrict const star)
{
    /*
     * Disable shellular nuclear burning
     */
    star->do_burn = FALSE;

    const double mc_He = star->core_mass[CORE_He];
    const double mc_CO = star->core_mass[CORE_CO];
    const double mc_ONe = star->core_mass[CORE_ONe];

    Foreach_shell(shell)
    {
        if(shell->m < mc_ONe)
        {
            /*
             * In the ONe core, all He is burnt to O/Ne/Mg
             */
            shell->X[XH1] = 0.0;
            shell->X[XH2] = 0.0;
            shell->X[XHe3] = 0.0;
            shell->X[XHe4] = 0.0;
            shell->X[XNe22] += shell->X[XN14];
            shell->X[XN14] = 0.0;
            shell->X[XC12] = 0.0;
            shell->X[XNe20] = 0.18;
            shell->X[XMg24] = 0.02;
            shell->X[XO16] = 1.0 - nucsyn_totalX(shell->X);
        }
        else if(shell->m < mc_CO)
        {
            /*
             * In the CO core, all He is burnt to C/O
             */
            shell->X[XH1] = 0.0;
            shell->X[XH2] = 0.0;
            shell->X[XHe3] = 0.0;
            shell->X[XHe4] = 0.0;
            shell->X[XNe22] += shell->X[XN14];
            shell->X[XN14] = 0.0;
            shell->X[XC12] = 0.8;
            shell->X[XO16] = 1.0 - nucsyn_totalX(shell->X);
        }
        else if(shell->m < mc_He)
        {
            /*
             * Evolve helium in the core.
             * Note: we do not know the helium-burning core mass,
             *       because this is not included in BSE.
             *       We assume all the core is burning helium,
             *       such that at the beginning of CHeB Y=1-Z,
             *       and at the end Y=0.
             *
             * Shells which are newly ingested in the core
             * have X>1, so in those we immediately
             * convert H to He, and CNO to N, prior to
             * helium burning.
             */
            if(shell->X[XH1] > 0.0)
            {
                shell->X[XHe4] += shell->X[XH1] + shell->X[XH2] + shell->X[XHe3];
                shell->X[XH1] = 0.0;
                shell->X[XH2] = 0.0;
                shell->X[XHe3] = 0.0;
                shell->X[XN14] += shell->X[XC12] + shell->X[XC13] + shell->X[XN15] + shell->X[XO16] + shell->X[XO17] + shell->X[XO18];
                shell->X[XC12] = 0.0;
                shell->X[XC13] = 0.0;
                shell->X[XN15] = 0.0;
                shell->X[XO16] = 0.0;
                shell->X[XO17] = 0.0;
                shell->X[XO18] = 0.0;
            }
        }
        else
        {
            /*
             * Use binary_c's TPAGB routine to set
             * the abundances
             */
            Copy_abundances(star->Xenv,
                            shell->X);
        }
    }
}
#endif // MINT && NUCSYN
