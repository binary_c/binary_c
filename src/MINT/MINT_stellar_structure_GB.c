#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT

/*
 * MINT function to calculate stellar structure
 * during the first ascent of the red giant branch.
 *
 * Note that prevstar can be NULL, in which case
 * we cannot compare to the previous star to do (say)
 * rejuvenation.
 *
 * TODO:
 * * helium ignition point is based on BSE and is
 * incorrect for MINT.
 *
 * * need to know how to go from HG to GB
 */


Stellar_type MINT_stellar_structure_GB(struct stardata_t * Restrict const stardata Maybe_unused,
                                       struct star_t * const prevstar Maybe_unused,
                                       struct star_t * const newstar,
                                       const Caller_id caller_id Maybe_unused)
{
    Boolean vb = TRUE;
#if DEBUG >=1
    vb = TRUE;
#endif // DEBUG
    const Boolean vb_ig = TRUE;

    if(newstar->mint->GB_table == NULL)
    {
        /*
         * First time setup
         */
        newstar->mint->GB_table = Calloc(1,sizeof(struct data_table_t));
        newstar->mint->GB_table->metadata = Calloc(1,sizeof(struct mint_table_metadata_t));
    }

    struct mint_table_metadata_t * const metadata =
        newstar->mint->GB_table->metadata;

    MINT_update_GB_table(stardata,
                         newstar);

    const Boolean * const available = metadata->data_available;
    if(vb) printf("MINT_stellar_structure_GB : MINT data available? %s\n",
                  Yesno(available!=NULL));

    const unsigned int * const map = metadata->data_map;
    double helium_ignited = 0.0;

#undef _EXIT_ON_ERROR
#ifdef _EXIT_ON_ERROR
    if(available == NULL)
    {
        Exit_binary_c(BINARY_C_ALLOC_FAILED,
                      "stardata->store->MINT_data_available[MINT_TABLE_HG] is NULL. This should not happen.");
    }
    if(map == NULL)
    {
        Exit_binary_c(BINARY_C_ALLOC_FAILED,
                      "stardata->store->MINT_data_map[MINT_TABLE_HG] is NULL. This should not happen.");
    }
#endif//_EXIT_ON_ERROR

    /*
     * If we have the GB data table, use it
     */
    if(available != NULL &&
       map != NULL)
    {
        double * result, * result_chebyshev, * result_conv Maybe_unused;
        if(stardata->persistent_data->MINT_GB_result == NULL)
        {
            const size_t s = sizeof(double)*newstar->mint->GB_table->ndata;
            result = stardata->persistent_data->MINT_GB_result =
                Malloc(s);
            result_chebyshev =
                stardata->persistent_data->MINT_GB_result_chebyshev =
                Malloc(s);
            result_conv =
                stardata->persistent_data->MINT_GB_result_conv =
                Malloc(s);

        }
        else
        {
            result =
                stardata->persistent_data->MINT_GB_result;
            result_chebyshev =
                stardata->persistent_data->MINT_GB_result_chebyshev;
            result_conv =
                stardata->persistent_data->MINT_GB_result_conv;
        }

        /*
         * Interpolate to obtain current stellar structure
         */
        Dprint("GB star MINT algorithm\n");

        /*
         * Define a macro to get the data, automatically
         * handling logged data.
         */
#undef get_data
#define get_data(VARTYPE)                                               \
        (                                                               \
            available[(VARTYPE)]==TRUE ? (                              \
                MINT_GB_data_actions[VARTYPE]==MINT_GB_ACTION_LOG10 ?   \
                exp10(result[map[(VARTYPE)]]) :                         \
                result[map[(VARTYPE)]]                                  \
                ) : 0.0                                                 \
            )

        newstar->stellar_type = GIANT_BRANCH;

        /*
         * Interpolate to find stellar structure
         */
        Dprint("call interpolate for stellar structure M=%g degeneracy=%g McHe/M=%g\n",
               newstar->mass,
               newstar->mint->central_degeneracy,
               newstar->core_mass[CORE_He] / newstar->mass);


        const double interpolation_coordinates[3] = {
            newstar->mass,
            newstar->mint->central_degeneracy,
            newstar->core_mass[CORE_He] / newstar->mass
        };
        printf("interpolate GB table %lu: %g %g %g\n",
               newstar->mint->GB_table->nlines,
               interpolation_coordinates[0],
               interpolation_coordinates[1],
               interpolation_coordinates[2]
            );
        fflush(NULL);

        Interpolate(newstar->mint->GB_table,
                    interpolation_coordinates,
                    result,
                    FALSE);

        /*
         * helium core mass derivative
         */
        newstar->derivative[DERIVATIVE_STELLAR_He_CORE_MASS] =
            get_data(MINT_GB_FIRST_DERIVATIVE_HELIUM_CORE_MASS_FRACTION) * newstar->mass;
            //3e-9 * newstar->mass;

        /*
         * central degeneracy derivative
         */
        newstar->derivative[DERIVATIVE_STELLAR_CENTRAL_DEGENERACY] =
            get_data(MINT_GB_FIRST_DERIVATIVE_CENTRAL_DEGENERACY);
        //2.5e-7;

        if(vb)
        {
            printf("t=%g HG/GB model %d dt=%g : [M=%g log10(M)=%g degen=%g Mc/M=%g] Mc=%g : dMc/dt=%g ddegen/dt=%g\n",
                   stardata->model.time,
                   stardata->model.model_number,
                   stardata->model.dt,
                   newstar->mass,
                   log10(newstar->mass),
                   newstar->mint->central_degeneracy,
                   newstar->core_mass[CORE_He]/newstar->mass,
                   newstar->core_mass[CORE_He],
                   newstar->derivative[DERIVATIVE_STELLAR_He_CORE_MASS],
                   newstar->derivative[DERIVATIVE_STELLAR_CENTRAL_DEGENERACY]);
        }

        /*
         * Stellar structure
         */
        newstar->radius = get_data(MINT_GB_RADIUS);
        newstar->luminosity = get_data(MINT_GB_LUMINOSITY);
        newstar->mint->helium_luminosity = get_data(MINT_GB_HELIUM_LUMINOSITY);

        printf("GBINT %g %g %g -> %g %g %g : d/dt degen %g Mc %g : L=%g R=%g LHe=%g\n",
               log10(newstar->mass),
               newstar->mint->central_degeneracy,
               newstar->core_mass[CORE_He] / newstar->mass,
               interpolation_coordinates[0],
               interpolation_coordinates[1],
               interpolation_coordinates[2],
               newstar->derivative[DERIVATIVE_STELLAR_He_CORE_MASS],
               newstar->derivative[DERIVATIVE_STELLAR_CENTRAL_DEGENERACY],
               newstar->luminosity,
               newstar->radius,
               newstar->mint->helium_luminosity
            );


        newstar->mint->warning = get_data(MINT_GB_WARNING_FLAG);
        newstar->derivative[DERIVATIVE_STELLAR_CENTRAL_HYDROGEN] = 0.0;
        newstar->derivative[DERIVATIVE_STELLAR_CENTRAL_HELIUM] = 0.0;
        newstar->derivative[DERIVATIVE_STELLAR_CENTRAL_CARBON] = 0.0;
        helium_ignited = get_data(MINT_GB_HELIUM_IGNITED_FLAG);
        newstar->mint->log_helium_luminosity_fraction = get_data(MINT_GB_LOG_HELIUM_LUMINOSITY_FRACTION);
        newstar->core_radius = 0.0;
        newstar->convective_core_mass = newstar->mass * get_data(MINT_GB_CONVECTIVE_CORE_MASS_OVERSHOOT_FRACTION);
        newstar->convective_core_radius = newstar->radius * get_data(MINT_GB_CONVECTIVE_CORE_RADIUS_OVERSHOOT_FRACTION);
        newstar->menv = newstar->mass * get_data(MINT_GB_CONVECTIVE_ENVELOPE_MASS_FRACTION);
        newstar->renv = newstar->radius * (1.0 - get_data(MINT_GB_CONVECTIVE_ENVELOPE_RADIUS_FRACTION));
        newstar->mint->mean_molecular_weight_star = get_data(MINT_GB_MEAN_MOLECULAR_WEIGHT_AVERAGE);
        newstar->mint->mean_molecular_weight_core = get_data(MINT_GB_MEAN_MOLECULAR_WEIGHT_CORE);
        newstar->moment_of_inertia_factor = get_data(MINT_GB_MOMENT_OF_INERTIA_FACTOR);
        newstar->E2 =
            available[MINT_GB_TIDAL_E2] ?
            get_data(MINT_GB_TIDAL_E2) :
            E2(stardata,newstar);
        newstar->E_Zahn = get_data(MINT_GB_TIDAL_E_FOR_LAMBDA);
        newstar->core_stellar_type = HeWD;
        Dprint("Mc=%g dMc(He)/dt=%g (~tGB=%g) L=%g R=%g\n",
               newstar->core_mass[CORE_He],
               newstar->derivative[DERIVATIVE_STELLAR_He_CORE_MASS],
               newstar->core_mass[CORE_He]/
               newstar->derivative[DERIVATIVE_STELLAR_He_CORE_MASS],
               newstar->luminosity,
               newstar->radius);

        if(vb_ig)
        {
            printf("CHECK HEIG [ M=%g,Mc/M=%g,degen=%g ] L= %g, R= %g, LHe=%g, ignited flag %g \n",
                   newstar->mass,
                   newstar->core_mass[CORE_He]/newstar->mass,
                   newstar->mint->central_degeneracy,
                   newstar->luminosity,
                   newstar->radius,
                   newstar->mint->helium_luminosity,
                   helium_ignited);
        }

        if(newstar->core_mass[CORE_He] < 0.0)
        {
            Exit_binary_c(BINARY_C_MINT_ERROR,
                          "OOPS MINT GB Mc<0\n");
        }

        if(MINT_has_shells(newstar))
        {
            MINT_GB_get_chebyshev_data(stardata,
                                       newstar,
                                       result_chebyshev);

            /*
             * Chebyshev coordinates
             */
            const double * const coords = CDict_nest_get_entry(
                metadata->header->cdict,
                "Chebyshev grid",
                "masses"
                )->value.value.double_array_data;

            int n_cheb = 0;

            Foreach_shell(newstar,shell)
            {
                /*
                 * Set stellar shell data from Chebyshev data
                 */
                n_cheb = MINT_GB_set_chebyshev_data(stardata,
                                                    newstar,
                                                    newstar->mint->GB_table,
                                                    shell,
                                                    coords,
                                                    result_chebyshev,
                                                    n_cheb,
                                                    map,
                                                    available);

                /*
                 * Identify instantly-mixed region from the
                 * convective core mass : note this needs
                 * updating to include the overshooting region
                 * and the surface convection zone
                 */
                shell->convective = (shell->m < newstar->convective_core_mass) ? TRUE : FALSE;

            } /* loop over shells */

            /*
             * Make the envelope cool
             */
            Dprint("Shell burning and mixing\n");
            Foreach_shell(newstar,shell)
            {
                shell->T = 0.0;
            }

            /*
             * Set the envelope convection zone and core
             */
            if(Is_not_zero(newstar->menv))
            {
                const double m_inner_edge = newstar->mass - newstar->menv;

                Foreach_shell(newstar,shell)
                {
                    shell->convective =
                        (shell->m > m_inner_edge) ? TRUE : FALSE;
                }
            }


            if(newstar->mass - newstar->core_mass[CORE_He] < 1e-4)
            {
                /*
                 * We've depleted the hydrogen envelope
                 */
                if(newstar->core_mass[CORE_He] > mc_heburn(newstar->phase_start_mass))
                {
                    /*
                     * Core is massive (hence hot) enough to
                     * ignite helium even though the envelope has
                     * been stripped.
                     */
                    newstar->stellar_type = HeMS;
                }
                else
                {
                    /*
                     * Core is too low in mass, hence too cool,
                     * to ignite helium. Set to the core stellar type
                     * (usually HeWD as set above).
                     */
                    newstar->stellar_type = newstar->core_stellar_type;
                }
                Dprint("H-envelope depleted : change stellar type to %d\n",
                       newstar->stellar_type);
            }
        }
    }

    Boolean ignite_helium = FALSE;

#ifdef DEPRECATED__BSE
    /*
     * Check for helium ignition
     */
    const Boolean degenerate_core = newstar->phase_start_mass < stardata->common.metallicity_parameters[ZPAR_MASS_HE_FLASH];
    if(degenerate_core)
    {
        /*
         * Degenerate core: assume this ignites at ~0.47Msun
         */
        if(newstar->core_mass[CORE_He] > 0.47)
        {
            if(vb_ig) printf("HEIG flash\n");
            ignite_helium = TRUE;
        }
        else
        {
            ignite_helium = FALSE;
        }
    }
    else
    {
        /*
         * Non-degenerate core
         */
        const double L_ig = lheif(newstar->phase_start_mass,
                                  stardata->common.metallicity_parameters[ZPAR_MASS_HE_FLASH],
                                  stardata->common.giant_branch_parameters);
        if(newstar->luminosity > L_ig)
        {
            ignite_helium = TRUE;
            if(vb_ig) printf("HEIG L = %g > L_ig = %g\n",
                             newstar->luminosity,
                             L_ig);
        }
        else
        {
            ignite_helium = FALSE;
            if(vb_ig) printf("HEIG L = %g < L_ig = %g\n",
                             newstar->luminosity,
                             L_ig);
        }
    }

    if(vb_ig) printf("HEIG degen? %s ignite? %s\n",
                     Yesno(degenerate_core),
                     Yesno(ignite_helium));

#endif
    //if(newstar->mint->helium_luminosity > 1.0)//1e-2)
    if(1 && helium_ignited > 0.5)
    {
        if(vb_ig) printf("HEIG ignites!\n");
        ignite_helium = TRUE;
        Flexit;
    }

    if(0 && More_or_equal(newstar->mint->log_helium_luminosity_fraction,-1.0))
    {
        if(vb_ig) printf("HEIG ignites!\n");
        ignite_helium = TRUE;
    }

    if(ignite_helium)
    {
        Dprint("Helium ignition (M=%g, Mc=%g)\n",
               newstar->mass,
               newstar->core_mass[CORE_He]);
        newstar->stellar_type = CHeB;
    }

    /*
     * Check for conversion to a helium star
     */
    if(More_or_equal(newstar->core_mass[CORE_He],
                     newstar->mass))
    {
        newstar->stellar_type = HeMS;
    }


    /*
    static int GBcount = 0;
    GBcount++;
    if(GBcount>10) Flexit;
    */
    return newstar->stellar_type;
}

#endif // MINT
