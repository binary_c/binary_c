#include "../binary_c.h"

No_empty_translation_unit_warning;

#if defined MINT && defined NUCSYN

/*
 * Radioactive decay in MINT
 */

void MINT_radioactive_decay(struct stardata_t * const Restrict stardata)
{
    Foreach_evolving_star(star)
    {
        for(Shell_index i=0; i<star->mint->nshells; i++)
        {
            nucsyn_do_radioactive_decay(stardata,
                                        star->mint->shells[i].X);
        }
    }
}

#endif // MINT && NUCSYN
