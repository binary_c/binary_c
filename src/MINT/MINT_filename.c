#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT
#include "MINT.h"

/*
 * Construct a MINT_filename for table ID table_id,
 * and return the pointer to it. This pointer will
 * need to be freed later.
 */

char * MINT_filename(struct stardata_t * const stardata Maybe_unused,
                     const char * const data_directory,
                     const unsigned int table_id)
{
#undef X
#define X(ID,FILENAME,FUNCTION,FAILBOOL,WHEN) FILENAME,
    static char * const filenames[] = { MINT_TABLES_LIST };
#undef X

    char * const f = table_id < MINT_TABLE_NUMBER ? filenames[table_id] : NULL;
    char * filename = NULL;


    if(f != NULL)
    {
        const int i = asprintf(&filename,
                               "%s/%s",
                               data_directory,
                               f);
        if(i < 0)
        {
            filename = NULL;
            Exit_binary_c(BINARY_C_MALLOC_FAILED,
                          "asprintf failed (memory allocation failed?)");
        }
        else
        {
            {
                /*
                 * Replace ZZ with appropriately formatted metallicity
                 */
                char * metallicity;
                const int n = asprintf(&metallicity,
                                       "Z%7.2e",
                                       stardata->preferences->MINT_metallicity);
                if(n == -1)
                {
                    Exit_binary_c(BINARY_C_ALLOC_FAILED,
                                  "Could not perform asprintf, presumably memory alloc failed.");
                }
                else
                {
                    printf("REPLACE FILENAME %s\n",filename);
                    while(string_replace(&filename,
                                         "ZZ",
                                         metallicity))
                    {
                    }
                    Safe_free(metallicity);
                }
            }
        }
    }

    Dprint("Made filename(%u) = %s\n",table_id,filename);
    return filename;
}
#endif//MINT
