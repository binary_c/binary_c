#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef MINT
#include "MINT.h"
#include "MINT_load_grid.h"

/*
 * Subroutine to load data from a MINT table into
 * a binary_c interpolation table and set up
 * the MINT metadata for this table.
 *
 * Return TRUE on success, FALSE on failure,
 * although some errors are fatal (we call
 * Exit_binary_c).
 *
 * vb = 1 means output some verbose info
 * vb = 2 means output all verbose info
 */
static char * automatically_generate_filename(
    struct stardata_t * const stardata,
    const char * header_string,
    const unsigned int MINT_table_id,
    Boolean * const allocated,
    const Boolean vb);

/* force printf to flush */
//#undef printf
//#define printf(...) {printf(__VA_ARGS__);fflush(stdout);}

/*
 * MINT_parameter_names and MINT_datatype_names
 * are the names as referred to internally in MINT.
 */


#define vbprintf(...)                           \
    if(vb ||                                    \
       stardata->preferences->MINT_filename_vb) \
    {                                           \
        printf(__VA_ARGS__);                    \
    }

Boolean MINT_table_loader(struct stardata_t * const stardata,
                          const unsigned int MINT_table_id,
                          struct data_table_t * data_table,
                          char * filename,
                          const char * header_string,
                          const char ** MINT_parameter_names,
                          const size_t n_MINT_parameter_names,
                          const char ** MINT_datatype_names,
                          const size_t n_MINT_datatype_names,
                          const int * parameter_actions,
                          const int * data_actions,
                          const int when,
                          struct data_table_defer_instructions_t * instructions,
                          int vb)
{
    Boolean warned_ncols_in_file1 = FALSE;
    Boolean warned_ncols_in_file2 = FALSE;
    Boolean filename_allocated = FALSE;
    struct mint_table_metadata_t * metadata;

    /*
     * If we are not given a data_table to populate,
     * use the default MINT location
     */
    if(data_table == NULL)
    {
        if(stardata->store->MINT_tables[MINT_table_id] == NULL)
        {
            /*
             * Construct it and allocate metadata
             */
            New_data_table(stardata->store->MINT_tables[MINT_table_id]);
            stardata->store->MINT_tables[MINT_table_id]->metadata =
                Calloc(1,sizeof(struct mint_table_metadata_t));
            if(stardata->store->MINT_tables[MINT_table_id]->metadata == NULL)
            {
                Exit_binary_c(BINARY_C_MALLOC_FAILED,
                              "Failed to allocate metadata for MINT table %u\n",
                              MINT_table_id);
            }
        }

        data_table = stardata->store->MINT_tables[MINT_table_id];
    }

    /*
     * Allocate metadata if it is not set
     */
    if(data_table->metadata == NULL)
    {
        data_table->metadata = Calloc(1,sizeof(struct mint_table_metadata_t));
        if(data_table->metadata == NULL)
        {
            Exit_binary_c(BINARY_C_MALLOC_FAILED,
                          "Failed to allocate new metadata in MINT table %p.",
                          (void*)data_table);
        }
    }
    metadata = data_table->metadata;

    vbprintf("MINT load table %s from %s, function %p, MINT_dir %s, n_MINT_parameter_names %zu, n_MINT_datatype_names %zu : already have table? %s\n",
             header_string,
             filename,
             (void*)data_table,
             stardata->preferences->MINT_dir,
             n_MINT_parameter_names,
             n_MINT_datatype_names,
             Yesno(MINT_has_table(MINT_table_id)));

    //if(MINT_has_table(MINT_table_id) == FALSE)
    {

        /*
         * If filename is NULL, generate the filename
         * automatically
         */
        if(filename == NULL)
        {
            filename =
                automatically_generate_filename(stardata,
                                                header_string,
                                                MINT_table_id,
                                                &filename_allocated,
                                                vb);
        }

        /*
         * Check we have been given a filename.
         *
         * If not, this is a formal (non-recoverable) error, because
         * we don't know where to look.
         */
        if(filename == NULL)
        {
            Exit_binary_c(BINARY_C_FILE_OPEN_ERROR,
                          "filename is NULL when trying to load MINT table %u\n",
                          MINT_table_id);
        }
        else
        {
            Dprint("MINT table data filename is \"%s\"\n",
                   filename);

            /*
             * Set up metadata
             */
            metadata->table_id = MINT_table_id;
            metadata->filename = strdup(filename);
            metadata->MINT_parameter_names = Malloc(sizeof(char*)*n_MINT_parameter_names);
            if(metadata->MINT_parameter_names == NULL)
            {
                Exit_binary_c(BINARY_C_MALLOC_FAILED,
                              "Failed to allocate memory for MINT_parameter_names in metadata = %p\n",
                              (void*)metadata);
            }
            metadata->n_MINT_parameter_names = n_MINT_parameter_names;
            for(size_t i=0;i<n_MINT_parameter_names;i++)
            {
                metadata->MINT_parameter_names[i] = strdup(MINT_parameter_names[i]);
            }
            metadata->MINT_datatype_names = Malloc(sizeof(char*)*n_MINT_datatype_names);
            if(metadata->MINT_datatype_names == NULL)
            {
                Exit_binary_c(BINARY_C_MALLOC_FAILED,
                              "Failed to allocate memory for MINT_datatype_names in metadata = %p\n",
                              (void*)metadata);
            }
            metadata->n_MINT_datatype_names = n_MINT_datatype_names;
            for(size_t i=0;i<n_MINT_datatype_names;i++)
            {
                metadata->MINT_datatype_names[i] = strdup(MINT_datatype_names[i]);
            }
            metadata->parameter_actions = (int*)parameter_actions;
            metadata->data_actions = (int*)data_actions;

            /*
             * Open stream to the data
             */
            vbprintf("try to open stream from file \"%s\"\n",filename);

            metadata->stream = open_stream(stardata,
                                           filename);

            vbprintf("opened data, stream = %p (->fp = %p, type %u)\n",
                   (void*)metadata->stream,
                   metadata->stream ? (void*)metadata->stream->fp : NULL,
                   metadata->stream ? metadata->stream->type : STREAM_TYPE_NONE);

            if(metadata->stream == NULL)
            {
                /*
                 * Error?
                 */
                Safe_free(filename);
                return FALSE;
            }
            else
            {
                /*
                 * Warn if we're using test_data directory
                 */
                const char * const test_data = strstr(filename,"test_data");
                if(test_data != NULL
                   &&
                   stardata->preferences->MINT_disable_grid_load_warnings == FALSE)
                {
                    MINT_warning("using a test_data file at \"%s\"\n",
                                 filename);
                }

                /*
                 * Get headers from the data file
                 */
                struct data_table_analysis_t * analysis = NULL;
                struct mint_header_t * MINT_header;
                MINT_read_header(stardata,
                                 metadata->stream,
                                 &analysis,
                                 &MINT_header,
                                 header_string,
                                 vb);
                metadata->header = (struct mint_header_t*)MINT_header;
                metadata->header_string = (char*)header_string;

                /*
                 * Get the number of lines of data : we need
                 * this to allocate memory for the data table.
                 */
                struct cdict_entry_t * linecount_entry = CDict_nest_get_entry(
                    MINT_header->cdict,
                    "metadata",
                    "number of data lines"
                    );
                const long int flc =
                    linecount_entry != NULL ?
                    linecount_entry->value.value.int_data :
                    file_linecount(stardata,
                                   filename,
                                   MINT_HEADER_CHAR,
                                   0);
                vbprintf("has %ld lines of data excluding comments\n",flc);

                if(flc<0)
                {
                    /*
                     * file_linecount returns negative : error!
                     */
                    Exit_binary_c(BINARY_C_FILE_OPEN_ERROR,
                                  "File \"%s\" has no data lines?\n",
                                  filename);
                }
                metadata->nl = (size_t) flc;
                metadata->nl_full_table = metadata->nl;

                /*
                 * Stack of strings containing missing data names
                 * (filled in below using _push_string() macro)
                 */
                size_t chebyshev_start_column = 0;


                /*
                 * Check that the required MINT parameters and data are in the file
                 */
                {
                    struct cdict_t * const coldict = *(struct cdict_t **) CDict_nest_get_data_pointer(
                        MINT_header->cdict,
                        "columns"
                        );

                    CDict_sorted_loop(coldict, ee)
                    {
                        char * valuestring;
                        int ret = CDict_entry_to_key_and_value_strings(coldict,
                                                                       ee,
                                                                       valuestring);
                        if(ret > 0)
                        {
                            char * keystring = ee->key.string;
                            Boolean found = FALSE;
                            for(size_t i=0; found==FALSE && i<n_MINT_parameter_names; i++)
                            {
                                if(Strings_equal(keystring,MINT_parameter_names[i]))
                                {
                                    found = TRUE;
                                }
                            }
                            for(size_t i=0; found==FALSE && i<n_MINT_datatype_names; i++)
                            {
                                if(Strings_equal(keystring,MINT_datatype_names[i]))
                                {
                                    found = TRUE;
                                }
                            }
                            if(found == FALSE)
                            {
                                _push_string(metadata->missing_MINT_types_strings,
                                             keystring);
                            }
                        }
                        Safe_free(valuestring);
                    }
                }

                if(when == MINT_LOAD_TABLE_DEFER &&
                   instructions == NULL)
                {
                    /*
                     * We require a table analysis if one has not
                     * already been performed.
                     */
                    if(analysis == NULL)
                    {
                        analysis = MINT_analyse_table(stardata,
                                                      metadata,
                                                      n_MINT_parameter_names,
                                                      vb);
                        /*
                         * Reduce the number of lines in the table
                         */
                        metadata->nl = Min(metadata->nl,
                                           analysis->skip[0] * 2);

                    }

                    /*
                     * Set instructions for future load
                     */
                    instructions = Malloc(sizeof(struct data_table_defer_instructions_t));
                    if(instructions == NULL)
                    {
                        Exit_binary_c(BINARY_C_MALLOC_FAILED,
                                      "Failed to allocate deferred-table instructions.");
                    }
                    instructions->lower_line_number = 0;
                    instructions->upper_line_number = metadata->nl;
                }

                /*
                 * Go back to the beginning of the file
                 */
                restart_stream(stardata,
                               metadata->stream);
                vbprintf("fseeked stream %p ->fp = %p to 0\n",
                       (void*)metadata->stream,
                       (void*)metadata->stream->fp);

                /*
                 * Get the number of data columns from the
                 * table header
                 */
                struct cdict_entry_t * e = CDict_nest_get_entry(
                    MINT_header->cdict,
                    "metadata",
                    "number of columns"
                    );
                if(e == NULL)
                {
                    Exit_binary_c(BINARY_C_POINTER_FAILURE,
                                  "Unknown number of columns when loading data for %s from %s",
                                  header_string,
                                  filename);
                }
                const unsigned int ncols_in_file = e->value.value.int_data;


                /*
                 * Allocate space for data availability Booleans,
                 * and default them to FALSE (0).
                 */
                Boolean * const available =
                    metadata->data_available =
                    Malloc(sizeof(Boolean) * n_MINT_datatype_names);
                if(available == NULL)
                {
                    Exit_binary_c(BINARY_C_MALLOC_FAILED,
                                  "Failed to allocated available array.")
                }
                metadata->data_available_size =
                    n_MINT_datatype_names;
                for(size_t i=0; i<n_MINT_datatype_names; i++)
                {
                    available[i] = FALSE;
                }

                metadata->data_available = available;
                metadata->data_available_size = n_MINT_datatype_names;

                /*
                 * Set up column data map: this takes a MINT
                 * data type as input, and returns the column
                 * in the interpolation table to which this refers.
                 */
                unsigned int * const map =
                    metadata->data_map =
                    Malloc(sizeof(unsigned int) * n_MINT_datatype_names);
                if(map == NULL)
                {
                    Exit_binary_c(BINARY_C_MALLOC_FAILED,
                                  "Failed to allocate memory for map.");
                }
                metadata->data_map_size = n_MINT_datatype_names;

                /*
                 * Hence acquire the number of columns in the file for
                 * parameters and data.
                 */
                int max_map_size = 0;
                vbprintf("We have %zu parameter names\n",n_MINT_parameter_names);

                for(size_t i=0; i<n_MINT_parameter_names; i++)
                {
                    vbprintf("Parameter %zu/%zu is %s\n",i,n_MINT_parameter_names,MINT_parameter_names[i]);
                    if(MINT_parameter_names[i] == NULL)
                    {
                        Exit_binary_c(BINARY_C_DATA_FAILED_TO_LOAD,
                                      "Parameter %zu/%zu is NULL when it should be a string : oops.",
                                      i,
                                      n_MINT_parameter_names);
                    }
                    const int range[2] = MINT_Column_range_with_string(MINT_parameter_names[i]);
                    if(range[0] >= 0)
                    {
                        metadata->nparam += (unsigned int)(range[1] - range[0]) + 1;
                    }
                    else
                    {
                        /*
                         * Not found in the file
                         */
                        _push_string(metadata->missing_from_file_strings,
                                     MINT_parameter_names[i]);
                    }
                }
                Boolean started_chebyshev_list = FALSE;

                /*
                 * Check for missing data and construct the
                 * file<>interpolation table column mapping
                 */
                for(size_t i=0; i<n_MINT_datatype_names; i++)
                {
                    vbprintf("Data item %zu is %s\n",i,MINT_datatype_names[i]);

                    const int range[2] = MINT_Column_range_with_string(MINT_datatype_names[i]);

                    vbprintf("range data %zu %d %d from %s\n",
                             i,
                             range[0],
                             range[1],
                             MINT_datatype_names[i]);

                    const Boolean have_data = range[0] >= 0 ? TRUE : FALSE;

                    /*
                     * Check if we're a scalar or Chebyshev data item
                     */
                    if(strstr(MINT_datatype_names[i],"CHEBYSHEV") != NULL)
                    {
                        /*
                         * Chebyshev data item
                         */
                        started_chebyshev_list = TRUE;
                        if(chebyshev_start_column == 0)
                        {
                            chebyshev_start_column = i;
                        }
                    }
                    else
                    {
                        /*
                         * Scalar, check we haven't started a
                         * Chebyshev list
                         */
                        if(have_data == TRUE)
                        {
                            metadata->nscalars++;
                        }
                        if(started_chebyshev_list == TRUE)
                        {
                            Exit_binary_c(
                                BINARY_C_DATA_FAILED_TO_LOAD,
                                "File %s (table %s) has a scalar after a Chebyshev list. This is an error: the scalar data should be first, followed by Chebyshev data.",
                                filename,
                                header_string);
                        }
                    }

                    /*
                     * Check if data is available
                     */
                    if(have_data == TRUE)
                    {
                        max_map_size = Max3(max_map_size,
                                            range[0],
                                            range[1]);

                        /*
                         * required data are in the file
                         */
                        available[i] = TRUE;
                        map[i] = metadata->ndata;
                        metadata->ndata += (unsigned int)(range[1] - range[0]) + 1;
                    }
                    else
                    {
                        /*
                         * Missing data in the file
                         */
                        available[i] = FALSE;
                        map[i] = 0;
                        _push_string(metadata->missing_from_file_strings,
                                     MINT_datatype_names[i]);
                    }

                    vbprintf("Set avail %zu %s at map %u\n",
                             i,
                             Truefalse(available[i]),
                             map[i]);

                }
                vbprintf("nscalars %d\n",(int)metadata->nscalars);

                /*
                 * Set up the file <> interpolation table column
                 * mapper, useful for debugging.
                 */
                max_map_size += 1;
                int * const MINT_to_file_map =
                    metadata->MINT_to_file_map =
                    Malloc(sizeof(int) * max_map_size);
                int * const file_to_MINT_map =
                    metadata->file_to_MINT_map =
                    Malloc(sizeof(int) * max_map_size);

                if(MINT_to_file_map == NULL ||
                   file_to_MINT_map == NULL)
                {
                    Exit_binary_c(BINARY_C_MALLOC_FAILED,
                                  "Failed to allocate memory for MINT_to_file_map == %p or file_to_MINT_map == %p\n",
                                  (void*)MINT_to_file_map,
                                  (void*)file_to_MINT_map);
                }

                metadata->MINT_to_file_map_size = (int)max_map_size;
                metadata->file_to_MINT_map_size = (int)max_map_size;

                /*
                 * Fill with impossible columns (-1)
                 */
                for(int i=0; i<max_map_size; i++)
                {
                    MINT_to_file_map[i] = -1;
                    file_to_MINT_map[i] = -1;
                }


                /*
                 * Check the number of data per line of the
                 * file agrees with the number of data per line
                 * of the header
                 */
                metadata->nperline = metadata->nparam + metadata->ndata;

                /*
                 * Warn if data may be missing or unset
                 */
                if(stardata->preferences->MINT_disable_grid_load_warnings == FALSE)
                {
                    if(warned_ncols_in_file2 == FALSE &&
                       ncols_in_file < metadata->nperline)
                    {
                        Printf("Warning, number of columns in the data (%u) != number of columns required to fill MINT's data table (nparam + ndata = %u) : some data will not be set in the interpolation table. File: %s, table %s.\n",
                               ncols_in_file,
                               metadata->nperline,
                               filename,
                               header_string);
                        warned_ncols_in_file2 = TRUE;
                    }
                    else if(ncols_in_file != metadata->nperline &&
                            warned_ncols_in_file1 == FALSE)
                    {
                        Printf("Warning: ncols_in_file = %u != nparam + ndata = %u, suggests some data in the MINT dataset are unused. This might be ok, but might not be. File: %s, table %s.\n",
                               ncols_in_file,
                               metadata->nperline,
                               filename,
                               header_string);
                        warned_ncols_in_file1 = TRUE;
                    }

                    /*
                     * Which columns are these?
                     */
                    _show_string_stack;
                    _free_string_stack;
                }

                /*
                 * Allocate memory for the interpolation table
                 * if required. We use metadata->table if it
                 * exists, and set this so it is certainly exists
                 * if we are called again for this table.
                 *
                 * Note:
                 * we should Calloc so that if data are not
                 * available, they are at least zero.
                 */
                if(data_table->data == NULL)
                {
                    data_table->data =  Calloc(metadata->nl * (size_t)metadata->nperline,
                                               sizeof(double));
                    if(data_table->data == NULL)
                    {
                        Exit_binary_c(BINARY_C_MALLOC_FAILED,
                                      "Failed to allocate space for data in data_table at %p of size %zu * %zu = %zu\n",
                                      (void*)data_table,
                                      metadata->nl * (size_t)metadata->nperline,
                                      sizeof(double),
                                      metadata->nl * (size_t)metadata->nperline * sizeof(double));
                    }
                }

                double * const table = data_table->data;

                vbprintf("table at %p has %zu lines (out of %zu) each of length %u\n",
                         (void*)table,
                         metadata->nl,
                         metadata->nl_full_table,
                         metadata->nperline);

                if(table == NULL)
                {
                    Exit_binary_c(BINARY_C_MALLOC_FAILED,
                                  "Malloc failed for table\n");
                }

                if(when != MINT_LOAD_TABLE_DEFER)
                {
                    /*
                     * Read in the table data now, subject to instructions
                     * as were set above
                     */
                    MINT_update_table(stardata,
                                      data_table,
                                      instructions,
                                      analysis,
                                      metadata,
                                      vb);
                }

                /*
                 * Free instructions if they are allocated
                 */
                if(instructions != NULL)
                {
                    Safe_free(instructions);
                }

                /*
                  if(stardata->preferences->MINT_disable_grid_load_warnings == FALSE &&
                  missing_from_file_strings_n > 0)
                  {
                  printf("The following data types were missing from the %s data table loaded from %s :\n",
                  header_string,
                  filename);
                  for(size_t i=0; i<missing_from_file_strings_n; i++)
                  {
                  printf("%s\n",missing_from_file_strings[i]);
                  }
                  printf("\n");
                  Safe_free(missing_from_file_strings);
                  }
                */

                vbprintf("data table has been set at %p is %g,%g,%g,... : nparam = %u, ndata = %u, nlines = %zu (full table has %zu lines)\n",
                         (void*)table,
                         table[0],
                         table[1],
                         table[2],
                         metadata->nparam,
                         metadata->ndata,
                         metadata->nl,
                         metadata->nl_full_table
                    );

                /*
                 * Construct the binary_c interpolation table
                 */
                data_table->nlines = metadata->nl;
                data_table->ndata = metadata->ndata;
                data_table->nparam = metadata->nparam;
                data_table->metadata = metadata;
                data_table->metadata_free_function = (void (*)(void *))MINT_free_table_metadata;
                data_table->data = table;
                data_table->analysis = analysis;

                vbprintf("MINT data table %u %s has been loaded from %s: nparam = %u, ndata = %u, nl = %zu, analysis = %p\n",
                         MINT_table_id,
                         header_string,
                         filename,
                         metadata->nparam,
                         metadata->ndata,
                         metadata->nl,
                         (void*)analysis
                    );
            }
        }

        if(filename_allocated == TRUE)
        {
            Safe_free(filename);
        }
    }

    vbprintf("Loaded grid\n");

    /*
     * Flush now in case there were warnings
     */
    fflush(stdout);

    return TRUE;
}


static char * automatically_generate_filename(
    struct stardata_t * const stardata,
    const char * header_string,
    const unsigned int MINT_table_id,
    Boolean * const allocated,
    const Boolean vb)
{
    char * filename = NULL;
    if(vb || stardata->preferences->MINT_filename_vb)
    {
        printf("Auto gen filename %s\n",header_string);
    }

    /*
     * Check for an appropriate environment variable
     * and use it if found
     */
    char * envvar = NULL;
    if(asprintf(&envvar,
                "%s_FILENAME",
                header_string) >= 0)
    {
        filename = getenv(envvar);
    }
    Safe_free(envvar);

    if(filename != NULL)
    {
        /*
         * Use MINT_<stellar_type>_FILENAME from envvar
         */
        *allocated = FALSE;
        if(vb || stardata->preferences->MINT_filename_vb)
        {
            printf("Using envvar at %s\n",filename);
        }
    }
    else
    {

        /*
         * MINT_dirs is a colon separated list of paths
         * over which we should loop.
         */
        struct string_array_t * MINT_dirs = new_string_array(0);
        string_split_preserve(stardata,
                              stardata->preferences->MINT_dir,
                              MINT_dirs,
                              ':',
                              1,
                              0,
                              FALSE);

        for(ssize_t i=0; i<MINT_dirs->n; i++)
        {
            char * MINT_dir = MINT_dirs->strings[i];
            if(vb || stardata->preferences->MINT_filename_vb)
            {
                printf("Checking MINT_dir = %s\n",MINT_dir);
            }

            /*
             * Construct filename
             */
            filename = MINT_filename(stardata,
                                     MINT_dir,
                                     MINT_table_id);

            if(vb || stardata->preferences->MINT_filename_vb)
            {
                printf("Constructed filename %s\n",filename);
            }

            if(filename == NULL)
            {
                Exit_binary_c(BINARY_C_MINT_FILENAME_ERROR,
                              "No MINT filename found for table %u %s\n",
                              MINT_table_id,
                              header_string);
            }
            else
            {
                *allocated = TRUE;
            }

            /*
             * Check if the file is there but with a zipped extension,
             * in which case we replace filename with the full
             * extended filename
             */
            char * filename_alt = check_file_exists(filename);
            if(filename_alt != NULL)
            {
                /*
                 * Found a file: use it!
                 */
                Safe_free(filename);
                filename = filename_alt;
                if(vb || stardata->preferences->MINT_filename_vb)
                {
                    printf("actually using %s\n",filename);
                }

                if(stardata->preferences->MINT_filename_vb == TRUE)
                {
                    printf("MINT using filename %s\n",filename);
                }

                /*
                 * Found a file we should use, break out and return
                 */
                break;
            }
            else
            {
                /*
                 * The file does not exist either in unzipped
                 * or zipped form. Oops!
                 */
                if(stardata->preferences->MINT_filename_vb == TRUE)
                {
                    printf("MINT cannot find a file at %s\n",filename);
                }
            }
        }

        /*
         * free MINT_dirs string array
         */
        free_string_array(&MINT_dirs);
    }

    /*
     * Error if we found no file
     */
    if(filename == NULL)
    {
        Exit_binary_c(BINARY_C_MINT_FILENAME_ERROR,
                      "No MINT filename found for table %u %s\n",
                      MINT_table_id,
                      header_string);
    }
    else
    {
        return filename;
    }
}



#endif // MINT
