#pragma once

#ifndef COMMON_ENVELOPE_EVOLUTION_BINARY_C_PROTOTYPES_H
#define COMMON_ENVELOPE_EVOLUTION_BINARY_C_PROTOTYPES_H

#ifdef _COMMON_ENVELOPE_BINARY_C_STATIC_FUNCTIONS

static void _opening_log(_CE_args_in,
                         struct common_envelope_state_t * const state);
static void _closing_log(_CE_args_in,
                         struct common_envelope_state_t * const state);
static void _update_stellar_structures(_CE_args_in,
                                       ...);
static void _erase_events(_CE_args_in);
static double _gamma(_CE_args_in,
                     struct common_envelope_state_t * const state Maybe_unused);
static double _alpha(_CE_args_in,
                     struct common_envelope_state_t * const state Maybe_unused);

static struct common_envelope_state_t * _CE_copy_state(struct stardata_t * const stardata,
                                                       struct common_envelope_state_t * const in,
                                                       const int phase);

static struct common_envelope_state_t * _CE_set_input_state_from_stars(_CE_args_in);

static void _CE_set_binary_c_from_state(_CE_args_in,
                                        struct common_envelope_state_t * const state);
static void _update_binding_energy(struct stardata_t * const stardata,
                                   struct common_envelope_state_t * const state);

static void _update_orbital_energy(_CE_args_in,
                                   struct common_envelope_state_t * const state);

static struct common_envelope_state_t *
_common_envelope_phase(_CE_args_in,
                       struct common_envelope_state_t * const pre_ejection_state);

static Boolean _core_accretion(_CE_args_in,
                               struct common_envelope_state_t * const state);

static char * _merger_info_string(_CE_args_in,
                                  Boolean * const merger_info_alloced);

static void _calc_effective_core_masses(struct stardata_t * const stardata,
                                        struct common_envelope_state_t * const state);
static void _calc_effective_core_radii(struct common_envelope_state_t * const state);

static void _calc_roche_lobe_radii(struct common_envelope_state_t * const state);
static void _calc_cores_roche_lobe_radii(struct common_envelope_state_t * const state);
static Boolean _try_to_eject_envelope(_CE_args_in,
                                      struct common_envelope_state_t * const pre_CE_state,
                                      struct common_envelope_state_t * const state);

static void _detect_merger(_CE_args_in,
                           struct common_envelope_state_t * const state);

static void _disc_formation(_CE_args_in,
                            struct common_envelope_state_t * const state);
static void _cleanup(_CE_args_in,
                     struct common_envelope_state_t ** const CE_state);

static void _reset_orbit(_CE_args_in,
                         struct common_envelope_state_t * const state Maybe_unused,
                         ...);
static void _update_orbit(_CE_args_in,
                          struct common_envelope_state_t * const state Maybe_unused);
static Boolean _solve_orbit(_CE_args_in,
                            struct common_envelope_state_t * const state Maybe_unused);
static void _mix_stars(_CE_args_in,
                       struct common_envelope_state_t * const state);

static double _binding_energy_merged_energy_check(_CE_args_in,
                                                  struct common_envelope_state_t * const merged_state,
                                                  struct common_envelope_state_t * const state,
                                                  const double mass,
                                                  const double binding_energy_target,
                                                  const double baryonic_mass_offset);
static double _binding_energy_merged_energy_check_wrapper(const double mass,
                                                          void * p);

static double _binding_energy_merged_energy(_CE_args_in,
                                            struct common_envelope_state_t * const merged_state,
                                            struct common_envelope_state_t * const state);

static void _CE_update_state_from_stars(_CE_args_in,
                                        struct common_envelope_state_t * const state,
                                        const int location);
static void _update_detached_stellar_spins(_CE_args_in,
                                           struct common_envelope_state_t * const state Maybe_unused);

static void _update_merged_stellar_spin(_CE_args_in,
                                        struct common_envelope_state_t * const pre_CE_state);

static void _add_envelope_to_remnants(_CE_args_in,
                                      struct common_envelope_state_t * const pre_CE_state,
                                      struct common_envelope_state_t * const state);
static double _add_envelope_to_remnant(_CE_args_in,
                                       struct common_envelope_state_t * const pre_CE_state,
                                       struct common_envelope_state_t * const state,
                                       struct star_t * const pre_star,
                                       const double target_radius_in,
                                       const double initial_core_mass,
                                       const Stellar_type initial_stellar_type);

static void _free_state(struct common_envelope_state_t ** statep);



static void _check_stripped_cores(_CE_args_in,
                                  struct common_envelope_state_t * const post_CE_state);

static double _Rflat(const double core_mass);
static double _Rhalt(const double core_mass);
static Boolean _Politano2010_merge_criterion(struct stardata_t * const stardata,
                                             struct common_envelope_state_t * const state,
                                             const double a_ej,
                                             const Boolean use_effective_cores);
#ifdef NUCSYN
static void _init_comenv_nucsyn(_CE_args_in);
#endif // NUCSYN
static double _ejecta_and_nucsyn(_CE_args_in,
#ifdef NUCSYN
                                 struct stardata_t * const input_stardata,
#endif // NUCSYN
                                 struct common_envelope_state_t * const state_pre,
                                 struct common_envelope_state_t * const state_post
#ifdef NUCSYN
                                 ,Abundance ** Xej
#endif // NUCSYN
    );
static void _compute_stellar_age(struct stardata_t * const stardata,
                                 struct star_t * const star);

static void _return_angmom(_CE_args_in,
                           struct common_envelope_state_t * const state_pre,
                           struct common_envelope_state_t * const state_post);

#endif // _COMMON_ENVELOPE_BINARY_C_STATIC_FUNCTIONS

#endif // COMMON_ENVELOPE_EVOLUTION_BINARY_C_PROTOTYPES_H
