/*
 * Polytropic common envelope parameters
 *
 * TODO
 *
 * Debugging
 * Calculate Rs based on a choice of Ms, rather than choosing them both.
 * Improve chi calculation
 * Compare to http://adsabs.harvard.edu/cgi-bin/bib_query?arXiv:1503.02750
 */

#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef COMENV_POLYTROPES

#define COMENV_POLYTROPES_DEBUG

#include "common_envelope_polytrope.h"

double common_envelope_polytrope(
    double m,
    double mc,
    double r,
    double rc,
#ifdef NUCSYN
    Abundance *X, // abundance in the envelope
#endif
    struct stardata_t * stardata
    )
{
    /* convert units to cgs */
    m *= M_SUN;
    mc *= M_SUN;
    r *= R_SUN;
    rc *= R_SUN;
    const double G = GRAVITATIONAL_CONSTANT;

    /* common envelope split location */
    double ms = Min(m, mc * stardata->preferences->comenv_splitmass);

    /* stellar envelope radius, mass */
    double me = m - mc;

    /* polytropic index */
    double n = 3.0/2.0;

    /* ionisation energy per gram */
    double chi =
#ifdef NUCSYN
        nucsyn_recombination_energy_per_gram(stardata,X);
#else
        1.3 * RYDBERG_ENERGY / M_PROTON;
#endif
    double GMMR = - G * m * me / r;

    /* interpolation parameters */
    double p[TABLE_COMMON_ENVELOPE_POLYTROPE3_NPARAMS] = {
       mc/m,
       rc/r,
       0.0 // we don't know (rs-rc)/(r-rc) yet because we don't know rs yet
    };

    /* do the 2D interpolation */

    /* interpolation results */
    double x[TABLE_COMMON_ENVELOPE_POLYTROPE2_NDATA];

    Interpolate(stardata->store->comenv_polytrope_2d,
                p,
                x,
                INTERPOLATE_USE_CACHE);

    /* polytrope's properties */
    double alpha = r / x[0];
    double beta = x[1];
#ifdef COMENV_POLYTROPES_DEBUG
    double eta_c = x[2];
#endif
    double Maybe_unused err = x[3];

    double q = mc / (4.0 * PI * beta * Pow3(alpha) ); // core density

    double K = 4.0 * PI * G * Pow2(alpha) / ((1.0 + n) * pow(q, 1.0/n - 1.0));

#ifdef COMENV_POLYTROPES_DEBUG
    /* logging */
    printf("Comenv polytrope: \n%-15s =% 20g (% 20g Msun)\n%-15s =% 20g (% 20g Msun)\n%-15s =% 20g (% 20g Msun)\n%-15s =% 20g (% 20g Msun) = %g * Mc\n%-15s =% 20g (% 20g Rsun)\n%-15s =% 20g (% 20g Rsun)\n%-15s =% 20g\n%-15s = % 20g\n%-15s = % 20g\n%-15s = % 20g\n",
           "M",m,m/M_SUN,
           "Me",me,me/M_SUN,
           "Mc",mc,mc/M_SUN,
           "Ms",ms,ms/M_SUN,ms/mc,
           "R",r,r/R_SUN,
           "Rc",rc,rc/R_SUN,
           "n",n,
           "chi",chi,
           "Ryd/g",RYDBERG_ENERGY/M_PROTON,
           "-G M Me / R",GMMR);


    printf("\n************************************************************\n");
    printf("2D Interpolation parameters:\n  p0 = Mc/Me = %g\n  p1 = rc/r = %g\n",
           p[0],
           p[1]);
    printf("2D interpolation result:\n  x0 = eta_e = % 20g\n  x1 = beta  = % 20g\n  x2 = eta_c = % 20g\n  x3 = err   = % 20g\n",x[0],x[1],x[2],x[3]);
    printf("alpha = %g\n",alpha);
    printf("beta = %g\n",beta);
    printf("eta_c = %g\n",eta_c);
    printf("q = %g\n",q);
    printf("K = %g\n",K);
#endif

    /* calculate Rs(Ms) */
    double rs;
    if(1)
    {
        /*
         * The 3d table gives us Ms(Rs) as y0.
         * Make a lookup table of Ms,Rs
         * so we can calculate Rs
         */
        rs = rc; // start at the core
        const int ntable = POLYTROPE_MSRS_TABLE_LENGTH; // number of lines of the data table
        // ratio is rs/rc : rs can be between rc and r, so ratio rs/rc is from 1 to r/rc
        double ratio_min = 1.0;
        double ratio_max = r/rc;

#ifdef COMENV_POLYTROPES_DEBUG
        printf("\n************************************************************\n\nMaking table to interpolate Rs(Ms)\n\n");
        printf("Ratio rs/rc can be from from min = %g to max = %g\n",ratio_min,ratio_max);
#endif

        if(!Fequal(ratio_min, ratio_max))
        {
            double logratio,dlogratio = (log10(ratio_max) - log10(ratio_min))/(double)(ntable-1);
            double * tabledata = stardata->store->comenv_polytrope_rsms->data;
            double * pointer = tabledata;

            int line = 0;
            for(logratio =  log10(ratio_min);
                Less_or_equal(logratio, log10(ratio_max));
                logratio += dlogratio)
            {
                double ratio = exp10( logratio);
                double ptmp[3]={
                    p[0],
                    p[1],
                    (ratio-1.0)/(r/rc-1.0)
                };

                double ytmp[TABLE_COMMON_ENVELOPE_POLYTROPE3_NDATA];
                //printf("Interpolate %d : parameters %g %g %g\n",line,ptmp[0],ptmp[1],ptmp[2]);
                Interpolate(stardata->store->comenv_polytrope_3d,
                            ptmp,
                            ytmp,
                            FALSE);
                //printf("Interpolate result     %g %g %g\n",ytmp[0],ytmp[1],ytmp[2]);

                double newms = ytmp[0] * q * Pow3(alpha);
                *(pointer++) = Max(0.0,Min(1.0,newms/m));
                *(pointer++) = log10(ratio);
                /*
                  printf("add to table (%g %g %g): at Rs/Rc=%g (Rs/R = %g; Rs = %g Rsun) -> Ms = (y0=%g) * (q=%g) * (alpha=%g)^3  = %g = %g Msun -> Ms/M = %g\n",
                  ptmp[0],
                  ptmp[1],
                  ptmp[2],

                  ratio,
                  ratio*rc/r,
                  ratio*rc/R_SUN,

                  ytmp[0],
                  q,
                  alpha,

                  newms,
                  newms / M_SUN,
                  newms / m
                  );
                  fflush(stdout);
                */
                line++;
            }


#ifdef COMENV_POLYTROPES_DEBUG
        if(0)
        {
            int j;
            printf("\nRs(Ms) table\n\n");
            printf("%12s : %20s : %12s : %12s : %12s\n","Line","Ms/M","Rs/Rc","Rs/Rsun","Rs/R");
            for(j=0;j<ntable;j++)
            {
                double rs = exp10(tabledata[j*2+1])*rc;
                printf("% 12d : % 20.12g : % 12g : % 12g : % 12g\n",
                       j,
                       tabledata[j*2],
                       rs,
                       rs/R_SUN,
                       rs/r
                    );
            }
        }
#endif

        /*
         * Interpolate to find Rs(Ms)
         */
            double mtmp[1]={ms/m};
            double rtmp[1];

            Interpolate(stardata->store->comenv_polytrope_rsms,
                        mtmp, // ratio Ms/M
                        rtmp, // ratio Rs/Rc
                        INTERPOLATE_USE_CACHE);

            rtmp[0] = exp10(rtmp[0]);
            rs = rtmp[0] * rc;
#ifdef COMENV_POLYTROPES_DEBUG
            printf("\nInterpolate for Rs: at Ms/M = %g (Ms = %g Msun, Ms/Mc = %g) -> ratio Rs/Rc = %g -> Rs = %g = %g Rsun\n",
                   mtmp[0],
                   mtmp[0]*m/M_SUN,
                   mtmp[0]*m/mc,
                   rtmp[0],
                   rs,
                   rs/R_SUN);
#endif //COMENV_POLYTROPES_DEBUG
        }
        else
        {
            /* ms = m hence splitpoint is at the surface */
            rs = r;
        }
    }

    /*
     * Now we have Rs we can do the 3D interpolation
     * for the common envelope
     */

    /*
     * Set p2 interpolation parameter
     */
    p[2]=(rs/rc-1.0)/(r/rc-1.0);

    /*
     * Interpolate for envelope structure
     */
    double y[TABLE_COMMON_ENVELOPE_POLYTROPE3_NDATA];
    Interpolate(stardata->store->comenv_polytrope_3d,
                p,
                y,
                INTERPOLATE_USE_CACHE);

    /* hence theta_s */
    double theta_s = pow(y[1],1.0/n);

    /* calculate E1, E2 and E3 */
    double E[4],E1[4];
#ifdef COMENV_POLYTROPES_DEBUG
    char * Estrings[4] = { "", "Gravity","Recombination","Enthalpy" };
#endif
    double multipliers[4] = {
        1.0,
        1.0,
        stardata->preferences->lambda_ionisation,
        stardata->preferences->lambda_enthalpy
    };
    /*
      fprintf(stderr,"MULT %g %g\n",
      stardata->preferences->lambda_ionisation,
      stardata->preferences->lambda_enthalpy);
    */
    E1[1] = - 0.5 * G * ( Pow2(m) / r - 0.0 * Pow2(ms) / rs );
    E1[2] = - 0.5 * (1.0 + n) * K * pow(q, 1.0/n) * ms * theta_s;
    E1[3] = - 0.5 * (1.0 + n) * K * pow(q, 1.0 + 1.0/n) * Pow3(alpha) * y[2];

    E[1] = E1[1] + E1[2] + E1[3];

    E[2] = chi * ( m - ms );

    E[3] =
        // 5.0/2.0 * // Gibbs : NB this includes the enthalpy!
        3.0/2.0 * // Internal energy : use this for Dewi & Tauris
        // 1.0 * // Enthalpy : no internal energy
        K * Pow3(alpha) * pow(q, 1.0 + 1.0/n) * y[2];


/* macro to calculate lambda from a binding energy */
#define Lambda(E) (GMMR/(E))
/* lambda if bound, otherwise 100 */
#define Lambda_limited(E) ((E)<-TINY ? Lambda(E) : 100.0)

#define logLambda(N)                                                    \
    printf("%20s : E%d = % 15g * % 15g = % 15g : lambda%d = % 15g (%s)\n", \
           Estrings[(N)],                                               \
           (N),                                                         \
           multipliers[(N)],                                            \
           E[(N)],                                                      \
           multipliers[(N)]*E[(N)],                                     \
           (N),                                                         \
           Lambda_limited(multipliers[(N)]*E[(N)]),                     \
           (E[(N)] < -TINY ? "Bound" : "Unbound")                        \
        );

    double Ebind=0.0;
    int i;
    for(i=1;i<=3;i++)
    {
        Ebind += multipliers[i] * E[i];
    }
    double lambda = Lambda_limited(Ebind);
    //fprintf(stderr,"LAMBDA %g\n",lambda);

#ifdef COMENV_POLYTROPES_DEBUG

    /* logging */
    printf("\n************************************************************\n");
    printf("3D Interpolation parameters:\n  p0 = Mc/Me = %g\n  p1 = Rc/R = %g\n  p2 = (Rs/Rc-1)/(R/Rc-1) = (%g-1)/(%g-1) = %g/%g = %g\n",
           p[0],
           p[1],
           rs/rc,
           r/rc,
           (rs/rc-1.0),
           (r/rc-1.0),
           p[2]);
    printf("3D interpolation result:\n  y0 = (Ms-Mc)/(q alpha^3) = %g\n  y1 = theta_s^n = %g\n  y2 = 4pi * integral = %g\n",y[0],y[1],y[2]);
    printf("theta_s = % 20g\n",theta_s);
    printf("E1 = %g + %g + %g = %g\n",E1[1],E1[2],E1[3],E[1]);
    printf(
        "E2 : is %g, should be (m - q * Pow3(alpha) * y[0]) = %g \n",
        E[2],
        chi * (m - q * Pow3(alpha) * y[0])
        );
    printf("\n");
    for(i=1;i<=3;i++)
    {
        logLambda(i);
    }

    printf("\nBinding energy is %g erg, log10(-Ebind) = %g\n",
           Ebind,
           log10(-Ebind));
    printf("\nSpecific gravitational binding energy = %g / %g = %g erg g^-1\n",
           E[1],
           m,
           E[1]/m);

    if(Ebind>0.0)
    {
        printf("WARNING : envelope is unbound (Ebind > 0) : will set lambda = +100\n");
    }
    printf("\nlambda from polytrope = %g / (%g + %g + %g) = %g / %g = %g\n",
           GMMR,
           multipliers[1]*E[1],
           multipliers[2]*E[2],
           multipliers[3]*E[3],
           GMMR,
           Ebind,
           lambda
        );

#endif

    return lambda;
}
#endif // COMENV_POLYTROPES
