#pragma once
#ifndef COMMON_ENVELOPE_H
#define COMMON_ENVELOPE_H

#include "../binary_c.h"
No_empty_translation_unit_warning;


#define ACCURACY 1.0e-6

#define HEADER "MIXING (COMENV) "

#define CGS_ENERGY (-M_SUN*M_SUN*GRAVITATIONAL_CONSTANT/R_SUN)

#define LARGE_LAMBDA LARGE_FLOAT


#define CEprint(...)                            \
    {                                           \
        printf("CEE %40s:%5d (ycheck %g %g) ",  \
               __func__,                        \
               __LINE__,                                                \
               stardata->common.zero_age.mass[0] + stardata->common.zero_age.mass[1], \
        stardata->star[0].mass + stardata->star[1].mass + sum_C_double_array(stardata->star[0].Xyield,ISOTOPE_ARRAY_SIZE) + sum_C_double_array(stardata->star[1].Xyield,ISOTOPE_ARRAY_SIZE)); \
        printf(__VA_ARGS__);                    \
        fflush(stdout);                         \
    }

/*
 * Route the common-envelope debugging through
 * Dprint
 */
#undef CEprint
#define CEprint(...) Dprint(__VA_ARGS__);

#endif // COMMON_ENVELOPE_H
