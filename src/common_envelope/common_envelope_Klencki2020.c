#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "common_envelope_evolution.h"
enum{
    _R1 = 0,
    _R2 = 1,
    _Rmax =2,
    _a = 0,
    _b = 1,
    _c = 2,
    _d = 3
};

double Pure_function common_envelope_Klencki2020(const double convfrac,
                                                 const double m0,
                                                 const double lum,
                                                 const double rad,
                                                 const double rzams,
                                                 const Stellar_type stellar_type Maybe_unused,
                                                 struct stardata_t * Restrict const stardata)
{
    double lambda;
    if(m0 > 10.0)
    {
        /*
         * Klencki+ 2020 fit to common-envelope lambda
         *
         * From data at
         * https://ftp.science.ru.nl/astro/jklencki/Klencki_etal_2020_CE_paper/lambda_R_fit.dat
         *
         * See https://arxiv.org/pdf/2006.11286.pdf
         */

        if(stardata->persistent_data->comenv_Klencki_2020_Rmax == NULL)
        {
            /*
             * New resolution in R/Rmax
             */
            const int new_resolution = 100; // must be >=4

            /*
             * Rmin : something small
             */
            const double Rmin_ratio = 1e-4;

            /*
             * First we have to map out the Klencki table from
             * (M,Z) to (M,Z,R/Rmax)
             * because while the fits work *for each M,Z* they
             * their coefficients cannot be interpolated.
             */
            struct data_table_t * table_raw;
            struct data_table_t * table_Rmax;
            struct data_table_t * table_loglambda;
            New_data_table(table_raw);
            New_data_table(table_Rmax);
            New_data_table(table_loglambda);

            /* set in stardata for later freeing */
            stardata->persistent_data->comenv_Klencki_2020_Rmax = table_Rmax;
            stardata->persistent_data->comenv_Klencki_2020_loglambda = table_loglambda;

            /* load the raw data table */
            Load_table(table_raw,
                       "src/common_envelope/common_envelope_klencki.dat",
                       All_cols,
                       2,
                       0,
                       FALSE);
            const size_t ll = table_raw->nparam + table_raw->ndata;
            CEprint("Loaded %zu lines of raw data for Klencki 2020 processing\n",
                   table_raw->nlines);

            /* make Rmax table */
            table_Rmax->nparam = 2; /* M, Z */
            table_Rmax->ndata = 1; /* Rmax */
            const size_t ll_Rmax = table_Rmax->ndata + table_Rmax->nparam;
            table_Rmax->nlines = table_raw->nlines;
            table_Rmax->data = Malloc(sizeof(double)*ll_Rmax*table_Rmax->nlines);
            size_t i_table_Rmax = 0;

            /* make new fit table */
            table_loglambda->nparam = 3; /* M, Z, R/Rmax */
            table_loglambda->ndata = 1; /* loglambda */
            const size_t ll_loglambda = table_loglambda->nparam + table_loglambda->ndata;
            table_loglambda->nlines = new_resolution * table_raw->nlines;
            table_loglambda->data = Malloc(sizeof(double)*ll_loglambda*table_loglambda->nlines);
            size_t i_loglambda = 0;

            for(size_t n=0; n<table_raw->nlines; n++)
            {
                const size_t line_offset = n * ll;
                const double * const line = table_raw->data + line_offset;
                const double M0 = *(line + 0);
                const double Z = *(line + 1);
                const double R12 = *(line + 2);
                const double R23 = *(line + 3);
                const double Rmax = *(line + 4);
                const double Rmin = Rmin_ratio * Rmax;
                const double dradius = (Rmax - Rmin)/((double)(new_resolution-1));

                /*
                 * Set Rmax table
                 */
                {
                    double * const p = table_Rmax->data + i_table_Rmax * ll_Rmax;
                    *(p+0) = M0;
                    *(p+1) = Z;
                    *(p+2) = Rmax;
                    i_table_Rmax++;
                }

                /*
                 * Set loglambda table
                 */
                for(double radius = Rmin; radius <= Rmax+dradius*0.5; radius += dradius)
                {
                    /* choose coefficient set */
                    const unsigned int set =
                        radius <= R12 ? 1 :
                        (radius <= R23 || Is_zero(*(line+5+8))) ? 2 :
                        3;
                    const size_t offset = 5 + (set-1)*4;
                    const double * const coeffs = line + offset;
                    const double lograd = log10(radius);
                    const double loglambda = Cubic(lograd,
                                                   coeffs[_d],
                                                   coeffs[_c],
                                                   coeffs[_b],
                                                   coeffs[_a]);

                    /*
                     * Set data in the loglambda table
                     */
                    double * const p = table_loglambda->data + i_loglambda * ll_loglambda;
                    *(p + 0) = M0;
                    *(p + 1) = Z;
                    *(p + 2) = radius/Rmax;
                    *(p + 3) = loglambda;
                    i_loglambda++;
                }
            }

            /* free raw table */
            Delete_data_table(table_raw);
        }

        /* first, get Rmax from M,Z */
        double Rmax;
        {
            const double x[2] = {
                m0,
                stardata->common.metallicity
            };
            double result[1];
            Interpolate(stardata->persistent_data->comenv_Klencki_2020_Rmax,
                        x,
                        result,
                        FALSE);
            Rmax = result[0];
        }

        /* now get log10(lambda) */
        double loglambda;
        {
            const double x[3] = {
                m0,
                stardata->common.metallicity,
                rad/Rmax
            };
            double result[1];
            Interpolate(stardata->persistent_data->comenv_Klencki_2020_loglambda,
                        x,
                        result,
                        FALSE);
            loglambda = result[0];
        }

        lambda = exp10(loglambda);
        CEprint("Klencki 2020 : M0=%g Z=%g Rmax=%g rad/Rmax=%g loglambda=%g lambda=%g\n",
               m0,
               stardata->common.metallicity,
               Rmax,
               rad/Rmax,
               loglambda,
               lambda);
    }
    else
    {
        /*
         * Below 10Msun, use Dewi + Tauris fit
         */
        lambda = common_envelope_dewi_tauris(convfrac,
                                             lum,
                                             m0,
                                             rzams,
                                             rad,
                                             stellar_type,
                                             stardata);
    }

    return lambda;
}
