#include "../binary_c.h"

No_empty_translation_unit_warning;

#ifdef MINT

#define LARGE_ENERGY 1e100

static double _common_envelope_lambda(struct stardata_t * const stardata,
                                      struct star_t * const star,
                                      struct star_t * const companion);
static double _orbital_energy(struct star_t * const donor_star,
                              struct star_t * const accretor_star,
                              struct orbit_t * const orbit);
static struct star_t * _stripped_core(struct stardata_t * stardata,
                                      struct star_t * star);

int common_envelope_evolution_MINT (
    struct star_t * const donor_star Maybe_unused,
    struct star_t * const accretor_star Maybe_unused,
    struct stardata_t * const stardata Maybe_unused,
    const int comenv_prescription Maybe_unused
    )
{

    /*
     * Common envelope evolution : MINT algorithm
     *
     * The "donor" star is the one that initiates the Roche-lobe
     * overflow that leads to the formation of a common envelope.
     *
     * The other star, which is normally a dwarf, is the accretor.
     */
    if(stardata->preferences->MINT_use_fallback_comenv == TRUE)
    {
        MINT_warning("MINT is using fallback BSE-style comenv : stellar types %d %d\n",
                     stardata->star[0].stellar_type,
                     stardata->star[1].stellar_type);

        /*
         * Map both stars to BSE
         */
        Foreach_star(star)
        {
            if(star->stellar_structure_algorithm[STELLAR_STRUCTURE_ALGORITHM_MINT] == TRUE)
            {
                MINT_map_star_to_BSE(stardata,NULL,star);
                star->stellar_structure_algorithm[STELLAR_STRUCTURE_ALGORITHM_MODIFIED_BSE] = TRUE;
                star->stellar_structure_algorithm[STELLAR_STRUCTURE_ALGORITHM_MINT] = FALSE;
            }
        }

        return common_envelope_evolution_BSE(
            donor_star,
            accretor_star,
            stardata,
            comenv_prescription);
    }

    /*
     * Back up stars passed in for later logging
     */
#if defined NUCSYN && defined STELLAR_POPULATIONS_ENSEMBLE
    struct star_t * donor_backup = New_star_from(donor_star);
//    struct star_t * accretor_backup = New_star_from(accretor_star);
#endif
    /*
     * Save orbit passed in, we work on orbit
     */
    struct orbit_t * orbit = Malloc_from(&stardata->common.orbit,
                                         struct orbit_t);

    Dprint("Start comenv MINT with prescription %d\n",comenv_prescription);

    if(comenv_prescription == COMENV_BSE ||
       comenv_prescription == COMENV_NANDEZ2016)
    {
        /*
         * Alpha-lambda prescriptions
         */
        const double Eorb_i = _orbital_energy(donor_star,
                                              accretor_star,
                                              &stardata->common.orbit);
        double Eorb_f,delta_E;
        double Ebind_i = 0.0;

        if(comenv_prescription == COMENV_BSE)
        {
            /*
             * Hurley et al. 2002 prescription
             *
             * This is a classical alpha-lambda based on the
             * giant donor's binding energy
             */

            /*
             * Calculate binding energy of the giant donor
             */
            double Ebind_donor = GIANT_LIKE_STAR(donor_star->stellar_type) ?
                (donor_star->mass * (donor_star->mass - donor_star->core_mass[CORE_He])/
                 (donor_star->radius * _common_envelope_lambda(stardata,donor_star,accretor_star))) : 0.0;

            /*
             * Calculate the binding energy of the accretor, if it's a giant.
             */
            double Ebind_accretor = GIANT_LIKE_STAR(accretor_star->stellar_type) ?
                (accretor_star->mass * (accretor_star->mass - accretor_star->core_mass[CORE_He])/
                 (accretor_star->radius * _common_envelope_lambda(stardata,accretor_star,donor_star))) : 0.0;

            /*
             * Total binding energy of the envelope
             */
            Ebind_i = Ebind_accretor + Ebind_donor;

            /*
             * Energy donated to the envelope
             */
            delta_E = Ebind_i / stardata->preferences->alpha_ce[stardata->model.comenv_count];

        }
        else if(comenv_prescription == COMENV_NANDEZ2016)
        {
            /*
             * Nandez et al. 2016 prescription
             *
             * This has the total alpha*lambda calculated according to their
             * prescription.
             */
            const double alpha_lambda = calc_alpha_lambda_Nandez2016(
                donor_star->mass,
                accretor_star->core_mass[CORE_He],
                donor_star->core_mass[CORE_He]);

            delta_E =
                donor_star->mass * (donor_star->mass - donor_star->core_mass[CORE_He]) /
                (donor_star->radius * alpha_lambda) ;
            if(GIANT_LIKE_STAR(accretor_star->stellar_type))
            {
                Ebind_i +=
                    accretor_star->mass * (accretor_star->mass - accretor_star->core_mass[CORE_He]) /
                    (accretor_star->radius * alpha_lambda);
            }

            delta_E = Ebind_i / alpha_lambda;
        }
        else
        {
            Exit_binary_c(BINARY_C_ALGORITHM_OUT_OF_RANGE,
                          "Unknown alpha-lambda comenv_prescription %d\n",
                          comenv_prescription);
        }

        /*
         * Inject energy from the orbital decay into the final orbit
         * to see if it is bound.
         */
        Eorb_f = Eorb_i + delta_E;

        /*
         * HPhd : Eq. 3.139
         */
        orbit->separation = -2.0 * Eorb_f /
            (donor_star->core_mass[CORE_He] * Effective_core_mass(accretor_star));
    }
    else if(comenv_prescription==COMENV_NELEMANS_TOUT)
    {
        /*
         * Nelemans and Tout prescription
         */
        orbit->separation = nelemans_separation(stardata->common.orbit.separation,
                                                donor_star->mass,
                                                accretor_star->mass,
                                                Effective_core_mass(donor_star),
                                                Effective_core_mass(accretor_star),
                                                stardata->preferences->nelemans_gamma,
                                                stardata);
    }
    else if(comenv_prescription == COMENV_GE2022)
    {
        Dprint("Ge's comenv prescription\n");


        /*
         * Compute the binding energy as a function of mass stripped
         */
        Dprint("Star %d is being stripped, stellar type %s, M=%g Mc=%g\n",
               donor_star->starnum,
               Stellar_type_string(donor_star->stellar_type),
               donor_star->mass,
               donor_star->core_mass[CORE_He]);

        double m = donor_star->mass;
        Dprint("Loop from %g to %g\n",m,donor_star->core_mass[CORE_He]);
        struct mint_shell_t * shell_high = NULL;
        struct mint_shell_t * shell_low = NULL;
        while(m > donor_star->core_mass[CORE_He])
        {
            const double binding_energy = MINT_partial_binding_energy(stardata,
                                                                      donor_star,
                                                                      m,
                                                                      donor_star->mass,
                                                                      &shell_low,
                                                                      &shell_high);

            if(shell_high == NULL)
            {
                /* surface */
                shell_high = &donor_star->mint->shells[donor_star->mint->nshells-1];
            }
            if(shell_low == NULL)
            {
                shell_low = &donor_star->mint->shells[0];
            }
            if(shell_low != NULL)
            {
                Dprint("MMM %g %g %g\n",
                       m,
                       shell_low != NULL ? shell_low->radius : donor_star->radius,
                       -binding_energy);
            }
            m -= 0.01;
        }

        Exit_binary_c(2,"Exit Ge 2022 comenv\n");
    }
    else
    {
        Exit_binary_c(BINARY_C_ALGORITHM_OUT_OF_RANGE,
                      "Unknown comenv_prescription %d\n",
                      comenv_prescription);
    }


    Boolean merge_stars = FALSE;

    /*
     * Construct the stripped cores of the stars
     */
    struct star_t * donor_core = _stripped_core(stardata,donor_star);
    struct star_t * accretor_core = _stripped_core(stardata,accretor_star);

    if(Less_or_equal(orbit->separation,0.0))
    {
        /*
         * separation is zero or negative, implies a merged system
         */
        merge_stars = TRUE;
    }
    else
    {
        /*
         * Separation > 0:
         * Construct a binary system made up of the stripped core
         * of the donor giant and the (possibly stripped) companion
         */


        /*
         * We now know the separation, update Roche lobe radii
         * and check for Roche lobe overflow of the cores
         */
        determine_roche_lobe_radius(stardata,orbit,donor_core);
        determine_roche_lobe_radius(stardata,orbit,accretor_core);
        merge_stars = Boolean_(donor_core->radius > donor_core->roche_radius ||
                               accretor_core->radius > accretor_core->roche_radius);

        if(merge_stars == FALSE)
        {
            /*
             * Perhaps just fill the Roche lobes of the remnants?
             */

        }
    }

    if(merge_stars == TRUE)
    {
        /*
         * Merge the stars.
         */
//        merge_stars(orbit,donor_star,accretor_star);

        /*
         * Remove energy delta_E which came from the orbit.
         */

    }
    else
    {
        /*
         * Cores do not merge:
         * Make a binary system which contains the stripped cores
         * at the appropriate separation.
         */
        copy_star(stardata,donor_core,donor_star);
        copy_star(stardata,accretor_core,accretor_star);
        memcpy(&stardata->common.orbit,orbit,sizeof(struct orbit_t));
    }

    free_star(&donor_core);
    free_star(&accretor_core);

#if defined NUCSYN && \
    defined STELLAR_POPULATIONS_ENSEMBLE

    /*
     * If there is a merger, make the comenv type negative
     */
    if(stardata->model.coalesce == TRUE)
    {
        stardata->model.comenv_type =
            donor_backup->stellar_type == HG ? ENSEMBLE_COMENV_HG_MERGER :
            donor_backup->stellar_type == GIANT_BRANCH ? ENSEMBLE_COMENV_GB_MERGER :
            donor_backup->stellar_type == CHeB ? ENSEMBLE_COMENV_CHeB_MERGER :
            donor_backup->stellar_type == EAGB ? ENSEMBLE_COMENV_EAGB_MERGER :
            donor_backup->stellar_type == TPAGB ? ENSEMBLE_COMENV_TPAGB_MERGER :
            donor_backup->stellar_type == HeHG ? ENSEMBLE_COMENV_HeHG_MERGER :
            donor_backup->stellar_type == HeGB ? ENSEMBLE_COMENV_HeGB_MERGER :
            0;

        stardata->model.comenv_type *= -1;
    }
    else
    {
        stardata->model.comenv_type =
            donor_backup->stellar_type == HG ? ENSEMBLE_COMENV_HG_DETACHED :
            donor_backup->stellar_type == GIANT_BRANCH ? ENSEMBLE_COMENV_GB_DETACHED :
            donor_backup->stellar_type == CHeB ? ENSEMBLE_COMENV_CHeB_DETACHED :
            donor_backup->stellar_type == EAGB ? ENSEMBLE_COMENV_EAGB_DETACHED :
            donor_backup->stellar_type == TPAGB ? ENSEMBLE_COMENV_TPAGB_DETACHED :
            donor_backup->stellar_type == HeHG ? ENSEMBLE_COMENV_HeHG_DETACHED :
            donor_backup->stellar_type == HeGB ? ENSEMBLE_COMENV_HeGB_DETACHED :
            0;
    }

    /* 0 is a failure : shouldn't ever happen */
    if(stardata->model.comenv_type==0)
    {
        Exit_binary_c(BINARY_C_WRONG_ARGUMENT,"comenv_type set to 0, i.e. unknown, from donor_backup->stellar_type=%d\n",
                      donor_backup->stellar_type);
    }
#endif // NUCSYN

    Exit_binary_c(2,"Exit common_envelope_evolution_MINT\n");

    return 0;
}


static double _common_envelope_lambda(struct stardata_t * const stardata,
                                      struct star_t * const star,
                                      struct star_t * const companion Maybe_unused)
{
    return common_envelope_lambda(star->phase_start_mass,
                                  star->core_mass[CORE_He],
                                  star->luminosity,
                                  star->radius,
                                  star->rzams,
                                  star->core_radius,
                                  star->menv / (star->mass - star->core_mass[CORE_He]),
                                  star->stellar_type,
#ifdef NUCSYN
                                  star->Xenv,
#endif
                                  stardata);
}

static double _orbital_energy(struct star_t * const donor_star,
                              struct star_t * const accretor_star,
                              struct orbit_t * const orbit)
{
    return
        Is_zero(orbit->separation) ?
        LARGE_ENERGY :
        (-donor_star->core_mass[CORE_He] *
         (
             GIANT_LIKE_STAR(accretor_star->stellar_type) ?
             accretor_star->core_mass[CORE_He] :
             accretor_star->mass
             )
         /(2.0 * orbit->separation * (1.0 - Pow2(orbit->eccentricity)))
            );
}

static struct star_t * _stripped_core(struct stardata_t * const stardata,
                                      struct star_t * const star)
{
    /*
     * Convert the star into its stripped core.
     *
     * Return a pointer to the core structure.
     */
    struct star_t * core = New_star_from(star);
    core->mass = core->core_mass[CORE_He];
    stellar_structure(stardata,
                      STELLAR_STRUCTURE_CALLER_common_envelope_evolution,
                      star,
                      core,
                      FALSE);

    return core;
}


#endif//MINT
