#include "../binary_c.h"
No_empty_translation_unit_warning;
#include "common_envelope_evolution.h"

/*
 * Function to calculate the common-envelope
 * structure parameter, lambda.
 *
 *
 * INPUT:
 *
 * m0 = "initial mass" (Msun)
 * rad = radius of star (Rsun)
 * lum = luminosity of star (Lsun)
 * convfrac = menv_conv / (M-Mc)
 *            is the fraction of the envelope that is convective
 */

Constant_function double common_envelope_lambda(const double m0,
                                                const double mc Maybe_unused,
                                                const double lum,
                                                const double rad,
                                                const double rzams,
                                                const double rc Maybe_unused,
                                                const double convfrac,
                                                const Stellar_type stellar_type,
#ifdef NUCSYN
                                                Abundance * const Xenv Maybe_unused,
#endif
                                                struct stardata_t * Restrict const stardata)
{
    double result;

#ifdef LOG_CE_PARAMETERS
    double lambda_pre_ion=0.0;
#endif
    /*
     * A function to estimate lambda for common-envelope.
     */

    CEprint("Lambda CE estimation: stellar_type=%d m0=%g mc=%g convfrac=%g lum=%g rad=%g rzams=%g\n",
            stellar_type,m0,mc,convfrac,lum,rad,rzams);

    /* perhaps use command-line specified lambda_ce */
    if(stardata->preferences->lambda_ce[stardata->model.comenv_count]>0.0)
    {
        result = stardata->preferences->lambda_ce[stardata->model.comenv_count];
        Dprint("Choose lambda_ce from the command line (is %g)\n",result);
    }
    else
    {
        if(Fequal(stardata->preferences->lambda_ce[stardata->model.comenv_count],
                  LAMBDA_CE_DEWI_TAURIS))
        {
            result = common_envelope_dewi_tauris(convfrac,
                                                 lum,
                                                 m0,
                                                 rzams,
                                                 rad,
                                                 stellar_type,
                                                 stardata);
        }
        else if(Fequal(stardata->preferences->lambda_ce[stardata->model.comenv_count],
                       LAMBDA_CE_WANG_2016))
        {
            result = common_envelope_wang2016(m0,
                                              rad,
                                              stellar_type,
                                              stardata);
        }
        else if(Fequal(stardata->preferences->lambda_ce[stardata->model.comenv_count],
                       LAMBDA_CE_KLENCKI_2020))
        {
            CEprint("KLENCKI convfrac=%g m=%g lum=%g rad=%g rzams=%g st=%d\n",
                    convfrac,
                    m0,
                    lum,
                    rad,
                    rzams,
                    stellar_type);
            result = common_envelope_Klencki2020(convfrac,
                                                 m0,
                                                 lum,
                                                 rad,
                                                 rzams,
                                                 stellar_type,
                                                 stardata);
        }

#ifdef COMENV_POLYTROPES
        else if(Fequal(stardata->preferences->lambda_ce[stardata->model.comenv_count],
                       LAMBDA_CE_POLYTROPE))
        {

            result = common_envelope_polytrope(m0,mc,rad,rc,
#ifdef NUCSYN
                                               Xenv,
#endif//NUCSYN
                                               stardata);

        }
#endif // COMENV_POLYTROPES
        else
        {
            result = 0.0;
            Exit_binary_c(BINARY_C_ALGORITHM_BRANCH_FAILURE,
                          "Common envelope prescription %g has no coded counterpart.\n",
                          stardata->preferences->lambda_ce[stardata->model.comenv_count]);
        }
    }

    result = Max(LAMBDA_CE_MIN,result);

    Dprint("CELAMF %g (lambda_ion=%g)\n",
           result,
           stardata->preferences->lambda_ionisation[stardata->model.comenv_count]
        );

#ifdef LOG_CE_PARAMETERS
    {
        char s[255]=sprintf "Common envelope : lambda=%g (pre-ion=%g)\n",
            result,lambda_pre_ion;
        output_string_to_logfile(stardata,s);
    }
#endif

    return result;
}
