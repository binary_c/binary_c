#include "../binary_c.h"
No_empty_translation_unit_warning;

void * stack_pop(struct binary_c_stack_t * const Restrict stack)
{
    /*
     * Pop item off the top of the stack,
     * or NULL if the stack is empty.
     *
     * Note: we do not change the allocation
     * in the stack, we assume it will be reused.
     * Call stack_shrink to do that.
     */
    if(stack->count == 0)
    {
        return NULL;
    }
    else
    {
        stack->count--;
        return stack->items[stack->count];
    }
}
