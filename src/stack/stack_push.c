#include "../binary_c.h"
No_empty_translation_unit_warning;

ssize_t stack_push(struct stardata_t * const Restrict stardata,
                   struct binary_c_stack_t * const Restrict stack,
                   void * const Restrict item)
{
    /*
     * Push item (void* pointer) onto the stack.
     *
     * Returns the number of items on the stack,
     * or -1 on error.
     */
    const size_t new_count = stack->count + 1;

    if(new_count > stack->allocated)
    {
        const size_t new_alloc = Max((size_t)1,stack->allocated) * 2;
        void * const new_items = Realloc(stack->items,
                                         sizeof(void*)*new_alloc);
        if(new_items == NULL)
        {
            /*
             * Error
             */
            return (ssize_t)-1;
        }
        else
        {
            stack->allocated = new_alloc;
            stack->items = new_items;
        }
    }

    stack->items[stack->count] = item;
    stack->count = new_count;
    return (ssize_t)stack->count;
}
