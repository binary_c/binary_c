#ifndef STACK_PROTOTYPES_H
#define STACK_PROTOTYPES_H

/*
 * Prototypes for binary_c's simple void* stack
 */
struct binary_c_stack_t * stack_new(void);
ssize_t stack_push(struct stardata_t * const Restrict stardata,
                   struct binary_c_stack_t * const Restrict stack,
                   void * const Restrict item);
void * stack_pop(struct binary_c_stack_t * const Restrict stack);
void stack_shrink(struct stardata_t * Restrict const stardata,
                  struct binary_c_stack_t * const Restrict stack);
void stack_free(struct binary_c_stack_t ** Restrict stackp);

#endif // STACK_PROTOTYPES_H
