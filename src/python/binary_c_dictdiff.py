from deepdiff import DeepDiff
from math import isclose as is_close

class binary_c_dictdiff(DeepDiff):

    """ class to compare dict1 to dict2. Floating-point numbers are compared using significant_digits significant digits, if given, or numbers are comparing within math_epsilon (passed to math.isclose()) for equality.

    We inherit the deepdiff class then override its _diff_numbers to include a relative tolerance also as the rel_tol parameter.
    """
    def __init__(self,*args,**kwargs):
        # set the rel_tol class parameter and delete it
        # so that DeepDiff does not complain
        self.rel_tol = kwargs.pop('rel_tol',None)

        # rename DeepDiff's _diff_numbers so we can
        # call it (from here) later
        DeepDiff._diff_numbers_orig = DeepDiff._diff_numbers

        # override DeepDiff's _diff_numbers to use our function
        DeepDiff._diff_numbers = self._diff_numbers

        # call DeepDiff to do the hard work
        DeepDiff.__init__(self,*args,**kwargs)

    def _diff_numbers(self, level, local_tree=None):
        """Diff two numbers using rel_tol or call DeepDiff's function."""

        t1_type = "number" if self.ignore_numeric_type_changes else level.t1.__class__.__name__
        t2_type = "number" if self.ignore_numeric_type_changes else level.t2.__class__.__name__

        if self.rel_tol is not None:
            if not is_close(level.t1, level.t2, rel_tol=self.rel_tol):
                self._report_result('values_changed', level, local_tree=local_tree)
        else:
            # call DeepDiff's function to diff numbers
            DeepDiff._diff_numbers_orig(self, level, local_tree=local_tree)
