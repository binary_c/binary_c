#!/usr/bin/env python3

############################################################
#
# Script to process Radice et al. (2019) NSNS merger
# data to make NSNS ejecta yields for binary_c.
#
# Run the script from the (single!) directory into which you
# download the tar files.
#
# These are at
#    https://zenodo.org/record/3588344
#
# I recommend installing "zenodo_get" and using
# that:
#
# pip3 install zenodo_get
# zenodo_get https://zenodo.org/record/3588344
#
############################################################

import binarycpython
import glob
import h5py
import numpy as np
from mendeleev import element
from mendeleev.fetch import fetch_table
import radioactivedecay as rd
import tarfile

############################################################
# constants
age_universe_yr = 14.0e9 # years
age_universe_s = age_universe_yr * 365.25 * 24.0 * 3600.0 # s
_inf = float('inf')
binary_c_version = binarycpython.utils.population_extensions.version_info.version_info(parsed=True).return_binary_c_version_info()

# options
vb = False
code = True
# time for which we decay isotopes, in years
radioactive_decay_time = age_universe_yr
tasks = {
    'isotopes' : True,
    'Ye' : True,
    'entropy' : True,
    'timescale' : True,
    'show_yield_table' : False,
    'make_ejecta_table' : True
}

# make list of files to include in the table
filenames = sorted([x for x in glob.glob('*.tar') if not x.endswith(('LR.tar','HR.tar','M0.tar')) and not x.startswith('EOS')])

############################################################
# make a dict of binary_c isotopes : we only make columns
# in our final yield table that are actually in binary_c.
# This is because binary_c has ~400 isotopes, while
# Radice2019 has ~7000.
#
binary_c_isotopes = {}
for index in binary_c_version['isotopes']:
    isotope = binary_c_version['isotopes'][index]['name']
    binary_c_isotopes[isotope] = index
if vb:
    print(f"Loaded {len(binary_c_isotopes)} binary_c isotopes")

############################################################
# read in Radice2019's table 2 information
#
table2 = {}
with open('Table2.txt') as f:
    for line in f:
        x = line.split()
        key = x.pop(0)
        table2[key] = x

############################################################
# get nucsyn data from Radice2019 NSNS simulations
# and generate python code to map from the ~7000 Radice2019
# isotopes to ~400 binary_c isotopes. We use the
# mendeleev module to construct the isotope symbol
# and the radioactivedecay module to decay each unstable
# isotope for radioactive_decay_time (years).
#
h5f = h5py.File('tabulated_nucsyn.h5','r')
atomic_masses = h5f['A']
atomic_numbers = h5f['Z']
trajectory_yields = h5f['Y_final']
c_indent = '    '
c_newline = "\n"
python_code = {}
if tasks['isotopes'] == True:
    # determine isotope list and decay products
    elements_table = fetch_table('elements')

    # i is the index in the Radice2019 table
    for i,(A,Z) in enumerate(zip(atomic_masses,atomic_numbers)):
        # construct isotope symbol
        Z = int(Z)
        if Z == 0:
            el = 'n'
        else:
            el = elements_table[['symbol']].iloc[Z-1].iloc[0]
        isotope = f"{el}{A}"

        if isotope == 'n1':
            # neutrons : special case
            radioactive = True
            half_life_s = 10.3 * 60 # seconds
        else:
            # other isotopes : determine stability
            try:
                nuc = rd.Nuclide(isotope)
            except:
                nuc = None

            if nuc:
                if nuc.half_life('s') == _inf:
                    # infinite half-life == stable
                    radioactive = False
                    half_life_s = _inf
                else:
                    # otherwise unstable
                    radioactive = True
                    half_life_s = nuc.half_life('s')
            else:
                # fallback
                radioactive = False
                half_life_s = -1.0

        if half_life_s == -1.0:
            if vb:
                print(f"{i} {isotope} skip")
        else:

            if radioactive == True:
                if el == 'n':
                    # neutrons decay to protons
                    products = {'H1':1.0}
                else:
                    try:
                        # make 1.0 Bq of this stuff (could be any unit!)
                        mix = rd.Inventory({isotope: 1.0},'Bq')

                        # decay for required time
                        decayed = mix.decay(radioactive_decay_time,'y').mass_fractions()

                        # make a dict of decay products
                        products = {}
                        for x in decayed:
                            if decayed[x] > 1e-14:
                                _x = x.replace('-','') # e.g. C-12 to C12
                                products[_x] = decayed[x]

                    except:
                        # something failed: this is fatal!
                        print(f"failed to decay {isotope}")
                        sys.exit(1)

                if vb:
                    print(f"{i} {isotope} decays to {products}")

                # make python code to decay the isotopes
                for decay_isotope in products:
                    # initialise
                    if not decay_isotope in python_code:
                        python_code[decay_isotope] = ''

                    # set binary_c isotope from a sum of
                    # the (many) NSNS isotopes
                    if binary_c_isotopes.get(decay_isotope,False) != False:
                        python_code[decay_isotope] += f"+ {products[decay_isotope]} * XNSNS[{i}]"

            else:
                if vb:
                    print(f"{i} {isotope} stable")
                if code and binary_c_isotopes.get(isotope,False) != False:
                    # initialise
                    if not isotope in python_code:
                        python_code[isotope] = ''

                    # set python code for stable isotope
                    # (no decay required)
                    python_code[isotope] += f"+ XNSNS[{i}]"



if tasks['show_yield_table']:
    # make and output the full hydro trajectory yield table
    entropy_list = list(h5f['s'])
    Ye_list = list(h5f['Ye'])
    timescale_list = list(h5f['tau'])
    for iYe,Ye in enumerate(Ye_list):
        for ientr,entropy in enumerate(entropy_list):
            for itau,timescale in enumerate(timescale_list):
                # list of NSNS ejecta
                XNSNS = trajectory_yields[iYe][ientr][itau]
                print(f"{Ye} {entropy} {timescale} {' '.join(np.char.mod('%g',XNSNS))}")

# loop over all valid tar files, construct final isotope table
for ifile,file in enumerate(filenames):
    archive = file.removesuffix('.tar')
    if vb:
        print(f"Process tar file: {file}")
    tar = tarfile.open(file,'r')
    try:
        files = {
            'hist_ye_entropy' : tar.extractfile(f"{archive}/ejecta/hist_ye_entropy_tau.h5"),
        }
    except:
        print("Failed to load one of the required files")
        sys.exit(1)

    if tasks['make_ejecta_table']:
        h5f = h5py.File(files['hist_ye_entropy'],'r')
        masses = list(h5f['mass']);
        Yes = list(h5f['Ye']);
        entropies = list(h5f['entropy']);
        taus = list(h5f['tau']);

        # add up yields for this model by looping over
        # the Ye, entropy and timescales (tau)
        #
        # These (7000+ isotopes) are put in a
        # list called XNSNS so we can eval the
        # python code
        XNSNS = []
        mtot = 0.0
        for iYe,Ye in enumerate(Yes):
            for ient,entropy in enumerate(entropies):
                for itau,timescale in enumerate(taus):
                    # get the mass at this Ye, entropy, timescale
                    m = masses[iYe][ient][itau]

                    # save total mass ejected (Msun)
                    mtot += m

                    # mass-weighted yields
                    y = m * trajectory_yields[iYe][ient][itau]
                    if len(XNSNS)==0:
                        XNSNS = y
                    else:
                        XNSNS += y

        # add up ejecta from the various decayed isotopes
        #
        # we use a python list so we can set it to strings
        # for debugging if required
        XNSNS_binary_c = [0.0] * len(binary_c_isotopes)
        if vb:
            print(f"XNSNS_binary_c len {len(binary_c_isotopes)}")
        Xtot = 0.0
        for isotope,i in binary_c_isotopes.items():
            if vb:
                print(f"Binary_c isotope {i} = {isotope}")
            if isotope in python_code:
                if vb:
                    print(f"Have {isotope} as code {python_code[isotope]} ")

                # eval the python code to compute the stable
                # amount of whatever
                x = eval(python_code[isotope])
                Xtot += x # normalize

                # set ejecta mass fraction in XNSNS_binary_c
                #XNSNS_binary_c[i] = f"{isotope}={x}" # string debugging output
                XNSNS_binary_c[i] = x

        # make list of ejecta, normalizing as we go
        lstring = ' '.join('%g' % (x/Xtot) for x in XNSNS_binary_c)
        #lstring = ' '.join('%s' % (x) for x in XNSNS_binary_c) # string debugging output

        m1 = table2[archive][1]
        m2 = table2[archive][2]

        # first time: output header
        if ifile == 0:
            print(f"#archive m1 m2 mejected {' '.join(sorted(binary_c_isotopes.keys(),key=lambda x:binary_c_isotopes[x]))}")

        # output
        print(f"{archive} {m1} {m2} {mtot} {lstring}")

    # close tar file
    tar.close()
