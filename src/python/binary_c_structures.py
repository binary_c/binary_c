'''Wrapper for binary_c_structures.h

Generated with:
./ctypesgen.py --output-language=python --cpp=gcc -E     -DALIGNSIZE=16   -fPIC -Wall -Wstrict-prototypes    -mtune=native -march=native  -DCPUFREQ=3500  -DLINUX  -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64  -g -fPIC -fno-stack-protector   -rdynamic -fsignaling-nans -fno-finite-math-only  -Og  -DGIT_REVISION=3863:20190221:e840774b -DGIT_URL="gitlab@gitlab.eps.surrey.ac.uk:ri0005/binary_c.git" -DGIT_BRANCH="triple"  -D__HAVE_AVX__  -mavx  -D__HAVE_DRAND48__  -D__HAVE_GSL__  -DUSE_GSL  -I/home/izzard/include  -D__HAVE_LIBBACKTRACE__  -D__HAVE_LIBBFD__  -D__HAVE_LIBBSD__  -D__HAVE_LIBIBERTY__  -D__HAVE_LIBMEMOIZE__  -D__HAVE_MALLOC_H__  -D__HAVE_PKG_CONFIG__  -D__HAVE_VALGRIND__  -D__SHOW_STARDATA__   -fsignaling-nans -fno-finite-math-only -ffast-math -fsignaling-nans -fno-finite-math-only --runtime-libdir=.. --compile-libdir=.. -L.. -I.. -lbinary_c ../binary_c_structures.h -o binary_c_structures.py

Do not modify this file.
'''

__docformat__ =  'restructuredtext'

# Begin preamble

import ctypes, os, sys
from ctypes import *

_int_types = (c_int16, c_int32)
if hasattr(ctypes, 'c_int64'):
    # Some builds of ctypes apparently do not have c_int64
    # defined; it's a pretty good bet that these builds do not
    # have 64-bit pointers.
    _int_types += (c_int64,)
for t in _int_types:
    if sizeof(t) == sizeof(c_size_t):
        c_ptrdiff_t = t
del t
del _int_types

class c_void(Structure):
    # c_void_p is a buggy return type, converting to int, so
    # POINTER(None) == c_void_p is actually written as
    # POINTER(c_void), so it can be treated as a real pointer.
    _fields_ = [('dummy', c_int)]

def POINTER(obj):
    p = ctypes.POINTER(obj)

    # Convert None to a real NULL pointer to work around bugs
    # in how ctypes handles None on 64-bit platforms
    if not isinstance(p.from_param, classmethod):
        def from_param(cls, x):
            if x is None:
                return cls()
            else:
                return x
        p.from_param = classmethod(from_param)

    return p

class UserString:
    def __init__(self, seq):
        if isinstance(seq, basestring):
            self.data = seq
        elif isinstance(seq, UserString):
            self.data = seq.data[:]
        else:
            self.data = str(seq)
    def __str__(self): return str(self.data)
    def __repr__(self): return repr(self.data)
    def __int__(self): return int(self.data)
    def __long__(self): return long(self.data)
    def __float__(self): return float(self.data)
    def __complex__(self): return complex(self.data)
    def __hash__(self): return hash(self.data)

    def __cmp__(self, string):
        if isinstance(string, UserString):
            return cmp(self.data, string.data)
        else:
            return cmp(self.data, string)
    def __contains__(self, char):
        return char in self.data

    def __len__(self): return len(self.data)
    def __getitem__(self, index): return self.__class__(self.data[index])
    def __getslice__(self, start, end):
        start = max(start, 0); end = max(end, 0)
        return self.__class__(self.data[start:end])

    def __add__(self, other):
        if isinstance(other, UserString):
            return self.__class__(self.data + other.data)
        elif isinstance(other, basestring):
            return self.__class__(self.data + other)
        else:
            return self.__class__(self.data + str(other))
    def __radd__(self, other):
        if isinstance(other, basestring):
            return self.__class__(other + self.data)
        else:
            return self.__class__(str(other) + self.data)
    def __mul__(self, n):
        return self.__class__(self.data*n)
    __rmul__ = __mul__
    def __mod__(self, args):
        return self.__class__(self.data % args)

    # the following methods are defined in alphabetical order:
    def capitalize(self): return self.__class__(self.data.capitalize())
    def center(self, width, *args):
        return self.__class__(self.data.center(width, *args))
    def count(self, sub, start=0, end=sys.maxint):
        return self.data.count(sub, start, end)
    def decode(self, encoding=None, errors=None): # XXX improve this?
        if encoding:
            if errors:
                return self.__class__(self.data.decode(encoding, errors))
            else:
                return self.__class__(self.data.decode(encoding))
        else:
            return self.__class__(self.data.decode())
    def encode(self, encoding=None, errors=None): # XXX improve this?
        if encoding:
            if errors:
                return self.__class__(self.data.encode(encoding, errors))
            else:
                return self.__class__(self.data.encode(encoding))
        else:
            return self.__class__(self.data.encode())
    def endswith(self, suffix, start=0, end=sys.maxint):
        return self.data.endswith(suffix, start, end)
    def expandtabs(self, tabsize=8):
        return self.__class__(self.data.expandtabs(tabsize))
    def find(self, sub, start=0, end=sys.maxint):
        return self.data.find(sub, start, end)
    def index(self, sub, start=0, end=sys.maxint):
        return self.data.index(sub, start, end)
    def isalpha(self): return self.data.isalpha()
    def isalnum(self): return self.data.isalnum()
    def isdecimal(self): return self.data.isdecimal()
    def isdigit(self): return self.data.isdigit()
    def islower(self): return self.data.islower()
    def isnumeric(self): return self.data.isnumeric()
    def isspace(self): return self.data.isspace()
    def istitle(self): return self.data.istitle()
    def isupper(self): return self.data.isupper()
    def join(self, seq): return self.data.join(seq)
    def ljust(self, width, *args):
        return self.__class__(self.data.ljust(width, *args))
    def lower(self): return self.__class__(self.data.lower())
    def lstrip(self, chars=None): return self.__class__(self.data.lstrip(chars))
    def partition(self, sep):
        return self.data.partition(sep)
    def replace(self, old, new, maxsplit=-1):
        return self.__class__(self.data.replace(old, new, maxsplit))
    def rfind(self, sub, start=0, end=sys.maxint):
        return self.data.rfind(sub, start, end)
    def rindex(self, sub, start=0, end=sys.maxint):
        return self.data.rindex(sub, start, end)
    def rjust(self, width, *args):
        return self.__class__(self.data.rjust(width, *args))
    def rpartition(self, sep):
        return self.data.rpartition(sep)
    def rstrip(self, chars=None): return self.__class__(self.data.rstrip(chars))
    def split(self, sep=None, maxsplit=-1):
        return self.data.split(sep, maxsplit)
    def rsplit(self, sep=None, maxsplit=-1):
        return self.data.rsplit(sep, maxsplit)
    def splitlines(self, keepends=0): return self.data.splitlines(keepends)
    def startswith(self, prefix, start=0, end=sys.maxint):
        return self.data.startswith(prefix, start, end)
    def strip(self, chars=None): return self.__class__(self.data.strip(chars))
    def swapcase(self): return self.__class__(self.data.swapcase())
    def title(self): return self.__class__(self.data.title())
    def translate(self, *args):
        return self.__class__(self.data.translate(*args))
    def upper(self): return self.__class__(self.data.upper())
    def zfill(self, width): return self.__class__(self.data.zfill(width))

class MutableString(UserString):
    """mutable string objects

    Python strings are immutable objects.  This has the advantage, that
    strings may be used as dictionary keys.  If this property isn't needed
    and you insist on changing string values in place instead, you may cheat
    and use MutableString.

    But the purpose of this class is an educational one: to prevent
    people from inventing their own mutable string class derived
    from UserString and than forget thereby to remove (override) the
    __hash__ method inherited from UserString.  This would lead to
    errors that would be very hard to track down.

    A faster and better solution is to rewrite your program using lists."""
    def __init__(self, string=""):
        self.data = string
    def __hash__(self):
        raise TypeError("unhashable type (it is mutable)")
    def __setitem__(self, index, sub):
        if index < 0:
            index += len(self.data)
        if index < 0 or index >= len(self.data): raise IndexError
        self.data = self.data[:index] + sub + self.data[index+1:]
    def __delitem__(self, index):
        if index < 0:
            index += len(self.data)
        if index < 0 or index >= len(self.data): raise IndexError
        self.data = self.data[:index] + self.data[index+1:]
    def __setslice__(self, start, end, sub):
        start = max(start, 0); end = max(end, 0)
        if isinstance(sub, UserString):
            self.data = self.data[:start]+sub.data+self.data[end:]
        elif isinstance(sub, basestring):
            self.data = self.data[:start]+sub+self.data[end:]
        else:
            self.data =  self.data[:start]+str(sub)+self.data[end:]
    def __delslice__(self, start, end):
        start = max(start, 0); end = max(end, 0)
        self.data = self.data[:start] + self.data[end:]
    def immutable(self):
        return UserString(self.data)
    def __iadd__(self, other):
        if isinstance(other, UserString):
            self.data += other.data
        elif isinstance(other, basestring):
            self.data += other
        else:
            self.data += str(other)
        return self
    def __imul__(self, n):
        self.data *= n
        return self

class String(MutableString, Union):

    _fields_ = [('raw', POINTER(c_char)),
                ('data', c_char_p)]

    def __init__(self, obj=""):
        if isinstance(obj, (str, unicode, UserString)):
            self.data = str(obj)
        else:
            self.raw = obj

    def __len__(self):
        return self.data and len(self.data) or 0

    def from_param(cls, obj):
        # Convert None or 0
        if obj is None or obj == 0:
            return cls(POINTER(c_char)())

        # Convert from String
        elif isinstance(obj, String):
            return obj

        # Convert from str
        elif isinstance(obj, str):
            return cls(obj)

        # Convert from c_char_p
        elif isinstance(obj, c_char_p):
            return obj

        # Convert from POINTER(c_char)
        elif isinstance(obj, POINTER(c_char)):
            return obj

        # Convert from raw pointer
        elif isinstance(obj, int):
            return cls(cast(obj, POINTER(c_char)))

        # Convert from object
        else:
            return String.from_param(obj._as_parameter_)
    from_param = classmethod(from_param)

def ReturnString(obj, func=None, arguments=None):
    return String.from_param(obj)

# As of ctypes 1.0, ctypes does not support custom error-checking
# functions on callbacks, nor does it support custom datatypes on
# callbacks, so we must ensure that all callbacks return
# primitive datatypes.
#
# Non-primitive return values wrapped with UNCHECKED won't be
# typechecked, and will be converted to c_void_p.
def UNCHECKED(type):
    if (hasattr(type, "_type_") and isinstance(type._type_, str)
        and type._type_ != "P"):
        return type
    else:
        return c_void_p

# ctypes doesn't have direct support for variadic functions, so we have to write
# our own wrapper class
class _variadic_function(object):
    def __init__(self,func,restype,argtypes):
        self.func=func
        self.func.restype=restype
        self.argtypes=argtypes
    def _as_parameter_(self):
        # So we can pass this variadic function as a function pointer
        return self.func
    def __call__(self,*args):
        fixed_args=[]
        i=0
        for argtype in self.argtypes:
            # Typecheck what we can
            fixed_args.append(argtype.from_param(args[i]))
            i+=1
        return self.func(*fixed_args+list(args[i:]))

# End preamble

_libs = {}
_libdirs = ['..', '..']

# Begin loader

# ----------------------------------------------------------------------------
# Copyright (c) 2008 David James
# Copyright (c) 2006-2008 Alex Holkner
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#  * Neither the name of pyglet nor the names of its
#    contributors may be used to endorse or promote products
#    derived from this software without specific prior written
#    permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
# ----------------------------------------------------------------------------

import os.path, re, sys, glob
import platform
import ctypes
import ctypes.util

def _environ_path(name):
    if name in os.environ:
        return os.environ[name].split(":")
    else:
        return []

class LibraryLoader(object):
    def __init__(self):
        self.other_dirs=[]

    def load_library(self,libname):
        """Given the name of a library, load it."""
        paths = self.getpaths(libname)

        for path in paths:
            if os.path.exists(path):
                return self.load(path)

        raise ImportError("%s not found." % libname)

    def load(self,path):
        """Given a path to a library, load it."""
        try:
            # Darwin requires dlopen to be called with mode RTLD_GLOBAL instead
            # of the default RTLD_LOCAL.  Without this, you end up with
            # libraries not being loadable, resulting in "Symbol not found"
            # errors
            if sys.platform == 'darwin':
                return ctypes.CDLL(path, ctypes.RTLD_GLOBAL)
            else:
                return ctypes.cdll.LoadLibrary(path)
        except OSError,e:
            raise ImportError(e)

    def getpaths(self,libname):
        """Return a list of paths where the library might be found."""
        if os.path.isabs(libname):
            yield libname
        else:
            # FIXME / TODO return '.' and os.path.dirname(__file__)
            for path in self.getplatformpaths(libname):
                yield path

            path = ctypes.util.find_library(libname)
            if path: yield path

    def getplatformpaths(self, libname):
        return []

# Darwin (Mac OS X)

class DarwinLibraryLoader(LibraryLoader):
    name_formats = ["lib%s.dylib", "lib%s.so", "lib%s.bundle", "%s.dylib",
                "%s.so", "%s.bundle", "%s"]

    def getplatformpaths(self,libname):
        if os.path.pathsep in libname:
            names = [libname]
        else:
            names = [format % libname for format in self.name_formats]

        for dir in self.getdirs(libname):
            for name in names:
                yield os.path.join(dir,name)

    def getdirs(self,libname):
        '''Implements the dylib search as specified in Apple documentation:

        http://developer.apple.com/documentation/DeveloperTools/Conceptual/
            DynamicLibraries/Articles/DynamicLibraryUsageGuidelines.html

        Before commencing the standard search, the method first checks
        the bundle's ``Frameworks`` directory if the application is running
        within a bundle (OS X .app).
        '''

        dyld_fallback_library_path = _environ_path("DYLD_FALLBACK_LIBRARY_PATH")
        if not dyld_fallback_library_path:
            dyld_fallback_library_path = [os.path.expanduser('~/lib'),
                                          '/usr/local/lib', '/usr/lib']

        dirs = []

        if '/' in libname:
            dirs.extend(_environ_path("DYLD_LIBRARY_PATH"))
        else:
            dirs.extend(_environ_path("LD_LIBRARY_PATH"))
            dirs.extend(_environ_path("DYLD_LIBRARY_PATH"))

        dirs.extend(self.other_dirs)
        dirs.append(".")
        dirs.append(os.path.dirname(__file__))

        if hasattr(sys, 'frozen') and sys.frozen == 'macosx_app':
            dirs.append(os.path.join(
                os.environ['RESOURCEPATH'],
                '..',
                'Frameworks'))

        dirs.extend(dyld_fallback_library_path)

        return dirs

# Posix

class PosixLibraryLoader(LibraryLoader):
    _ld_so_cache = None

    def _create_ld_so_cache(self):
        # Recreate search path followed by ld.so.  This is going to be
        # slow to build, and incorrect (ld.so uses ld.so.cache, which may
        # not be up-to-date).  Used only as fallback for distros without
        # /sbin/ldconfig.
        #
        # We assume the DT_RPATH and DT_RUNPATH binary sections are omitted.

        directories = []
        for name in ("LD_LIBRARY_PATH",
                     "SHLIB_PATH", # HPUX
                     "LIBPATH", # OS/2, AIX
                     "LIBRARY_PATH", # BE/OS
                    ):
            if name in os.environ:
                directories.extend(os.environ[name].split(os.pathsep))
        directories.extend(self.other_dirs)
        directories.append(".")
        directories.append(os.path.dirname(__file__))

        try: directories.extend([dir.strip() for dir in open('/etc/ld.so.conf')])
        except IOError: pass

        unix_lib_dirs_list = ['/lib', '/usr/lib', '/lib64', '/usr/lib64']
        if sys.platform.startswith('linux'):
            # Try and support multiarch work in Ubuntu
            # https://wiki.ubuntu.com/MultiarchSpec
            bitage = platform.architecture()[0]
            if bitage.startswith('32'):
                # Assume Intel/AMD x86 compat
                unix_lib_dirs_list += ['/lib/i386-linux-gnu', '/usr/lib/i386-linux-gnu']
            elif bitage.startswith('64'):
                # Assume Intel/AMD x86 compat
                unix_lib_dirs_list += ['/lib/x86_64-linux-gnu', '/usr/lib/x86_64-linux-gnu']
            else:
                # guess...
                unix_lib_dirs_list += glob.glob('/lib/*linux-gnu')
        directories.extend(unix_lib_dirs_list)

        cache = {}
        lib_re = re.compile(r'lib(.*)\.s[ol]')
        ext_re = re.compile(r'\.s[ol]$')
        for dir in directories:
            try:
                for path in glob.glob("%s/*.s[ol]*" % dir):
                    file = os.path.basename(path)

                    # Index by filename
                    if file not in cache:
                        cache[file] = path

                    # Index by library name
                    match = lib_re.match(file)
                    if match:
                        library = match.group(1)
                        if library not in cache:
                            cache[library] = path
            except OSError:
                pass

        self._ld_so_cache = cache

    def getplatformpaths(self, libname):
        if self._ld_so_cache is None:
            self._create_ld_so_cache()

        result = self._ld_so_cache.get(libname)
        if result: yield result

        path = ctypes.util.find_library(libname)
        if path: yield os.path.join("/lib",path)

# Windows

class _WindowsLibrary(object):
    def __init__(self, path):
        self.cdll = ctypes.cdll.LoadLibrary(path)
        self.windll = ctypes.windll.LoadLibrary(path)

    def __getattr__(self, name):
        try: return getattr(self.cdll,name)
        except AttributeError:
            try: return getattr(self.windll,name)
            except AttributeError:
                raise

class WindowsLibraryLoader(LibraryLoader):
    name_formats = ["%s.dll", "lib%s.dll", "%slib.dll"]

    def load_library(self, libname):
        try:
            result = LibraryLoader.load_library(self, libname)
        except ImportError:
            result = None
            if os.path.sep not in libname:
                for name in self.name_formats:
                    try:
                        result = getattr(ctypes.cdll, name % libname)
                        if result:
                            break
                    except WindowsError:
                        result = None
            if result is None:
                try:
                    result = getattr(ctypes.cdll, libname)
                except WindowsError:
                    result = None
            if result is None:
                raise ImportError("%s not found." % libname)
        return result

    def load(self, path):
        return _WindowsLibrary(path)

    def getplatformpaths(self, libname):
        if os.path.sep not in libname:
            for name in self.name_formats:
                dll_in_current_dir = os.path.abspath(name % libname)
                if os.path.exists(dll_in_current_dir):
                    yield dll_in_current_dir
                path = ctypes.util.find_library(name % libname)
                if path:
                    yield path

# Platform switching

# If your value of sys.platform does not appear in this dict, please contact
# the Ctypesgen maintainers.

loaderclass = {
    "darwin":   DarwinLibraryLoader,
    "cygwin":   WindowsLibraryLoader,
    "win32":    WindowsLibraryLoader
}

loader = loaderclass.get(sys.platform, PosixLibraryLoader)()

def add_library_search_dirs(other_dirs):
    loader.other_dirs = other_dirs

load_library = loader.load_library

del loaderclass

# End loader

add_library_search_dirs(['..', '..'])

# Begin libraries

_libs["binary_c"] = load_library("binary_c")

# 1 libraries
# End libraries

# No modules

__off_t = c_long # /usr/include/x86_64-linux-gnu/bits/types.h: 140

__off64_t = c_long # /usr/include/x86_64-linux-gnu/bits/types.h: 141

# /usr/include/x86_64-linux-gnu/bits/libio.h: 245
class struct__IO_FILE(Structure):
    pass

FILE = struct__IO_FILE # /usr/include/x86_64-linux-gnu/bits/types/FILE.h: 7

_IO_lock_t = None # /usr/include/x86_64-linux-gnu/bits/libio.h: 154

# /usr/include/x86_64-linux-gnu/bits/libio.h: 160
class struct__IO_marker(Structure):
    pass

struct__IO_marker.__slots__ = [
    '_next',
    '_sbuf',
    '_pos',
]
struct__IO_marker._fields_ = [
    ('_next', POINTER(struct__IO_marker)),
    ('_sbuf', POINTER(struct__IO_FILE)),
    ('_pos', c_int),
]

struct__IO_FILE.__slots__ = [
    '_flags',
    '_IO_read_ptr',
    '_IO_read_end',
    '_IO_read_base',
    '_IO_write_base',
    '_IO_write_ptr',
    '_IO_write_end',
    '_IO_buf_base',
    '_IO_buf_end',
    '_IO_save_base',
    '_IO_backup_base',
    '_IO_save_end',
    '_markers',
    '_chain',
    '_fileno',
    '_flags2',
    '_old_offset',
    '_cur_column',
    '_vtable_offset',
    '_shortbuf',
    '_lock',
    '_offset',
    '__pad1',
    '__pad2',
    '__pad3',
    '__pad4',
    '__pad5',
    '_mode',
    '_unused2',
]
struct__IO_FILE._fields_ = [
    ('_flags', c_int),
    ('_IO_read_ptr', String),
    ('_IO_read_end', String),
    ('_IO_read_base', String),
    ('_IO_write_base', String),
    ('_IO_write_ptr', String),
    ('_IO_write_end', String),
    ('_IO_buf_base', String),
    ('_IO_buf_end', String),
    ('_IO_save_base', String),
    ('_IO_backup_base', String),
    ('_IO_save_end', String),
    ('_markers', POINTER(struct__IO_marker)),
    ('_chain', POINTER(struct__IO_FILE)),
    ('_fileno', c_int),
    ('_flags2', c_int),
    ('_old_offset', __off_t),
    ('_cur_column', c_ushort),
    ('_vtable_offset', c_char),
    ('_shortbuf', c_char * 1),
    ('_lock', POINTER(_IO_lock_t)),
    ('_offset', __off64_t),
    ('__pad1', POINTER(None)),
    ('__pad2', POINTER(None)),
    ('__pad3', POINTER(None)),
    ('__pad4', POINTER(None)),
    ('__pad5', c_size_t),
    ('_mode', c_int),
    ('_unused2', c_char * (((15 * sizeof(c_int)) - (4 * sizeof(POINTER(None)))) - sizeof(c_size_t))),
]

__jmp_buf = c_long * 8 # /usr/include/x86_64-linux-gnu/bits/setjmp.h: 31

# /usr/include/x86_64-linux-gnu/bits/types/__sigset_t.h: 8
class struct_anon_6(Structure):
    pass

struct_anon_6.__slots__ = [
    '__val',
]
struct_anon_6._fields_ = [
    ('__val', c_ulong * (1024 / (8 * sizeof(c_ulong)))),
]

__sigset_t = struct_anon_6 # /usr/include/x86_64-linux-gnu/bits/types/__sigset_t.h: 8

# /usr/include/setjmp.h: 33
class struct___jmp_buf_tag(Structure):
    pass

struct___jmp_buf_tag.__slots__ = [
    '__jmpbuf',
    '__mask_was_saved',
    '__saved_mask',
]
struct___jmp_buf_tag._fields_ = [
    ('__jmpbuf', __jmp_buf),
    ('__mask_was_saved', c_int),
    ('__saved_mask', __sigset_t),
]

jmp_buf = struct___jmp_buf_tag * 1 # /usr/include/setjmp.h: 45

Boolean = c_uint # /home/izzard/git/binary_c/izzard/src/binary_c_types.h: 14

# /home/izzard/git/binary_c/izzard/src/maths/binary_c_drandr_types.h: 37
class struct_binary_c_drand48_data(Structure):
    pass

struct_binary_c_drand48_data.__slots__ = [
    '__x',
    '__old_x',
    '__c',
    '__init',
    '__a',
]
struct_binary_c_drand48_data._fields_ = [
    ('__x', c_uint * 3),
    ('__old_x', c_uint * 3),
    ('__c', c_uint),
    ('__init', c_uint),
    ('__a', c_ulonglong),
]

brent_function = CFUNCTYPE(UNCHECKED(c_double), c_double, POINTER(None)) # /home/izzard/git/binary_c/izzard/src/binary_c_types.h: 71

# /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 36
class struct_data_table_t(Structure):
    pass

struct_data_table_t.__slots__ = [
    'data',
    'nlines',
    'nparam',
    'ndata',
]
struct_data_table_t._fields_ = [
    ('data', POINTER(c_double)),
    ('nlines', c_int),
    ('nparam', c_int),
    ('ndata', c_int),
]

# /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 65
class struct_memoize_hash_t(Structure):
    pass

# /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 63
class struct_binary_c_file(Structure):
    pass

struct_binary_c_file.__slots__ = [
    'memo',
    'fp',
    'size',
    'maxsize',
    'path',
]
struct_binary_c_file._fields_ = [
    ('memo', POINTER(struct_memoize_hash_t)),
    ('fp', POINTER(FILE)),
    ('size', c_size_t),
    ('maxsize', c_size_t),
    ('path', c_char * 4096),
]

# /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 126
class struct_hsearch_data(Structure):
    pass

# /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 127
class struct_element_info_t(Structure):
    pass

# /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 115
class struct_libbinary_c_store_t(Structure):
    pass

struct_libbinary_c_store_t.__slots__ = [
    'mnuc',
    'imnuc',
    'mnuc_amu',
    'imnuc_amu',
    'molweight',
    'ZonA',
    'atomic_number',
    'nucleon_number',
    'atomic_number_hash',
    'element_info',
    'icache',
    'collision_matrix',
    'label',
    'jaschek_jaschek_dwarf',
    'jaschek_jaschek_giant',
    'jaschek_jaschek_supergiant',
    'miller_bertolami',
    'miller_bertolami_coeffs_L',
    'miller_bertolami_coeffs_R',
    'Holly_1DUP_table',
    'massive_MS_lifetimes',
    'Eldridge2012_colours',
    'sigmav',
    's_process_Gallino',
    'novae_JH98_CO',
    'novae_JH98_ONe',
    'tableh1',
    'tableh2',
    'tableh3',
    'tableb1',
    'tableb2',
    'tableb3',
    'tableg1',
    'tableg2',
    'tableg3',
    'comenv_maxR_table',
    'zgr',
    'tgr',
    'ggr',
    'ubv',
    'Karakas_ncal',
    'built',
    'debug_stopping',
]
struct_libbinary_c_store_t._fields_ = [
    ('mnuc', POINTER(c_double)),
    ('imnuc', POINTER(c_double)),
    ('mnuc_amu', POINTER(c_double)),
    ('imnuc_amu', POINTER(c_double)),
    ('molweight', POINTER(c_double)),
    ('ZonA', POINTER(c_double)),
    ('atomic_number', POINTER(c_int)),
    ('nucleon_number', POINTER(c_int)),
    ('atomic_number_hash', POINTER(struct_hsearch_data)),
    ('element_info', POINTER(struct_element_info_t)),
    ('icache', POINTER(POINTER(c_uint))),
    ('collision_matrix', POINTER(c_int) * 15),
    ('label', POINTER(c_char) * 40),
    ('jaschek_jaschek_dwarf', POINTER(struct_data_table_t)),
    ('jaschek_jaschek_giant', POINTER(struct_data_table_t)),
    ('jaschek_jaschek_supergiant', POINTER(struct_data_table_t)),
    ('miller_bertolami', POINTER(struct_data_table_t)),
    ('miller_bertolami_coeffs_L', POINTER(struct_data_table_t)),
    ('miller_bertolami_coeffs_R', POINTER(struct_data_table_t)),
    ('Holly_1DUP_table', POINTER(struct_data_table_t)),
    ('massive_MS_lifetimes', POINTER(struct_data_table_t)),
    ('Eldridge2012_colours', POINTER(struct_data_table_t)),
    ('sigmav', POINTER(struct_data_table_t)),
    ('s_process_Gallino', POINTER(struct_data_table_t)),
    ('novae_JH98_CO', POINTER(struct_data_table_t)),
    ('novae_JH98_ONe', POINTER(struct_data_table_t)),
    ('tableh1', POINTER(c_double)),
    ('tableh2', POINTER(c_double)),
    ('tableh3', POINTER(c_double)),
    ('tableb1', POINTER(c_double)),
    ('tableb2', POINTER(c_double)),
    ('tableb3', POINTER(c_double)),
    ('tableg1', POINTER(c_double)),
    ('tableg2', POINTER(c_double)),
    ('tableg3', POINTER(c_double)),
    ('comenv_maxR_table', POINTER(struct_data_table_t)),
    ('zgr', POINTER(c_double)),
    ('tgr', POINTER(c_double)),
    ('ggr', POINTER(c_double)),
    ('ubv', POINTER(POINTER(POINTER(POINTER(c_double))))),
    ('Karakas_ncal', POINTER(struct_data_table_t)),
    ('built', Boolean),
    ('debug_stopping', Boolean),
]

# /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 241
class struct_cmd_line_arg_t(Structure):
    pass

# /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 240
class struct_libbinary_c_tmpstore_t(Structure):
    pass

struct_libbinary_c_tmpstore_t.__slots__ = [
    'cmd_line_args',
    'arg_count',
    'cdf_table',
    'interpolate_table',
    'interpolate_f',
    'interpolate_table_ids',
    'number_of_interpolation_tables',
    'interpolate_cache_array',
    'interpolate_cache',
    'interpolate_cache_match_line',
    'interpolate_cache_spin_line',
    'interpolate_steps_array',
    'interpolate_varcount_array',
    'raw_buffer',
    'raw_buffer_size',
    'batchstring',
    'batchstring2',
    'batchargs',
    'ensemble_type',
    'Xacc',
    'Xenv',
    'nXacc',
    'nXenv',
    'Xdonor',
    'Xaccretor',
    'Xwind_gain',
    'Xwind_loss',
    'XRLOF_and_decretion_loss',
    'XRLOF_gain',
    'Xnovae',
    'comenv_lambda_data',
    'comenv_lambda_table',
]
struct_libbinary_c_tmpstore_t._fields_ = [
    ('cmd_line_args', POINTER(struct_cmd_line_arg_t)),
    ('arg_count', c_int),
    ('cdf_table', POINTER(struct_data_table_t)),
    ('interpolate_table', POINTER(c_double)),
    ('interpolate_f', POINTER(c_double)),
    ('interpolate_table_ids', POINTER(POINTER(c_double))),
    ('number_of_interpolation_tables', c_int),
    ('interpolate_cache_array', POINTER(POINTER(c_double))),
    ('interpolate_cache', POINTER(c_double)),
    ('interpolate_cache_match_line', c_int * 50),
    ('interpolate_cache_spin_line', c_int * 50),
    ('interpolate_steps_array', POINTER(POINTER(c_int))),
    ('interpolate_varcount_array', POINTER(POINTER(c_int))),
    ('raw_buffer', String),
    ('raw_buffer_size', c_int),
    ('batchstring', String),
    ('batchstring2', String),
    ('batchargs', POINTER(POINTER(c_char))),
    ('ensemble_type', POINTER(c_int)),
    ('Xacc', POINTER(c_double) * 2),
    ('Xenv', POINTER(c_double) * 2),
    ('nXacc', POINTER(c_double) * 2),
    ('nXenv', POINTER(c_double) * 2),
    ('Xdonor', POINTER(c_double)),
    ('Xaccretor', POINTER(c_double)),
    ('Xwind_gain', POINTER(c_double)),
    ('Xwind_loss', POINTER(c_double)),
    ('XRLOF_and_decretion_loss', POINTER(c_double)),
    ('XRLOF_gain', POINTER(c_double)),
    ('Xnovae', POINTER(c_double)),
    ('comenv_lambda_data', POINTER(c_double)),
    ('comenv_lambda_table', POINTER(struct_data_table_t)),
]

# /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 1047
class struct_libbinary_c_star_t(Structure):
    pass

# /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 329
class struct_new_supernova_t(Structure):
    pass

struct_new_supernova_t.__slots__ = [
    'new_stellar_structure',
    'new_companion_structure',
    'X',
]
struct_new_supernova_t._fields_ = [
    ('new_stellar_structure', POINTER(struct_libbinary_c_star_t)),
    ('new_companion_structure', POINTER(struct_libbinary_c_star_t)),
    ('X', c_double * (((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1)),
]

# /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 866
class struct_splitinfo_t(Structure):
    pass

# /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 340
class struct_libbinary_c_preferences_t(Structure):
    pass

struct_libbinary_c_preferences_t.__slots__ = [
    'the_initial_abundances',
    'initial_abundance_multiplier',
    'minimum_timestep',
    'maximum_timestep',
    'maximum_nuclear_burning_timestep',
    'tpagb_superwind_mira_switchon',
    'tpagb_reimers_eta',
    'vwind_multiplier',
    'mass_accretion_rate1',
    'mass_accretion_rate2',
    'angular_momentum_accretion_rate1',
    'angular_momentum_accretion_rate2',
    'angular_momentum_accretion_rate_orbit',
    'accretion_start_time',
    'accretion_end_time',
    'minimum_donor_menv_for_comenv',
    'magnetic_braking_factor',
    'magnetic_braking_gamma',
    'wr_wind_fac',
    'alpha_ce',
    'lambda_ce',
    'lambda_ionisation',
    'lambda_enthalpy',
    'accretion_limit_eddington_multiplier',
    'accretion_limit_thermal_multiplier',
    'accretion_limit_dynamical_multiplier',
    'donor_limit_thermal_multiplier',
    'donor_limit_dynamical_multiplier',
    'nova_retention_fraction',
    'beta_reverse_nova',
    'nova_irradiation_multiplier',
    'nova_faml_multiplier',
    'rlof_angmom_gamma',
    'nonconservative_angmom_gamma',
    'alphaCB',
    'gb_reimers_eta',
    'CRAP_parameter',
    'Bondi_Hoyle_accretion_factor',
    'max_tpagb_core_mass',
    'chandrasekhar_mass',
    'max_neutron_star_mass',
    'minimum_mass_for_carbon_ignition',
    'minimum_mass_for_neon_ignition',
    'max_HeWD_mass',
    'minimum_helium_ignition_core_mass',
    'gbwindfac',
    'agbwindfac',
    'tidal_strength_factor',
    'merger_angular_momentum_factor',
    'wind_djorb_fac',
    'lw',
    'mass_accretion_for_eld',
    'WD_accretion_rate_novae_upper_limit_hydrogen_donor',
    'WD_accretion_rate_new_giant_envelope_lower_limit_hydrogen_donor',
    'WD_accretion_rate_novae_upper_limit_helium_donor',
    'WD_accretion_rate_new_giant_envelope_lower_limit_helium_donor',
    'WD_accretion_rate_novae_upper_limit_other_donor',
    'WD_accretion_rate_new_giant_envelope_lower_limit_other_donor',
    'mass_for_Hestar_Ia_lower',
    'mass_for_Hestar_Ia_upper',
    'vw93_mira_shift',
    'vw93_multiplier',
    'nelemans_gamma',
    'nelemans_minq',
    'nelemans_max_frac_j_change',
    'nelemans_n_comenvs',
    'nelemans_recalc_eccentricity',
    'comenv_post_eccentricity',
    'hachisu_qcrit',
    'timestep_modulator',
    'RLOF_timestep_modulator',
    'RLOF_mdot_factor',
    'f_massloss_during_MS_mergers',
    'mixingpar_MS_mergers',
    'qcrit',
    'qcrit_degenerate',
    'minimum_envelope_mass_for_third_dredgeup',
    'lambda_min',
    'delta_mcmin',
    'lambda_multiplier',
    'dtfac',
    'hbbtfac',
    'c13_eff',
    'mc13_pocket_multiplier',
    'yields_dt',
    'yields_logdt',
    'yields_startlogtime',
    'observable_radial_velocity_minimum',
    'start_time',
    'api_log_filename_prefix',
    'log_filename',
    'cmd_line_random_seed',
    'gbwind',
    'tpagbwind',
    'wr_wind',
    'wd_kick_when',
    'wd_kick_pulse_number',
    'wd_kick_direction',
    'sn_kick_distribution',
    'sn_kick_dispersion',
    'sn_kick_companion',
    'wind_angular_momentum_loss',
    'jorb_RLOF_transfer_model',
    'gravitational_radiation_model',
    'BH_prescription',
    'E2_prescription',
    'RLOF_interpolation_method',
    'nova_retention_method',
    'WRLOF_method',
    'wind_mass_loss',
    'wind_mass_loss_algorithm',
    'comenv_prescription',
    'qcrit_giant_branch_method',
    'comenv_ejection_spin_method',
    'comenv_merger_spin_method',
    'magnetic_braking_algorithm',
    'monte_carlo_sn_kicks',
    'post_SN_orbit_method',
    'third_dup',
    'random_systems',
    'batchmode',
    'batch_submode',
    'no_arg_checking',
    'rlperi',
    'rotationally_enhanced_mass_loss',
    'rotationally_enhanced_exponent',
    'repeat',
    'RLOF_method',
    'small_envelope_method',
    'initial_abundance_mix',
    'initial_abunds_only',
    'type_Ia_MCh_supernova_algorithm',
    'type_Ia_sub_MCh_supernova_algorithm',
    'Seitenzahl2013_model',
    'NeNaMgAl',
    'yields_logtimes',
    'no_thermohaline_mixing',
    'pre_main_sequence',
    'pre_main_sequence_fit_lobes',
    'internal_buffering',
    'internal_buffering_compression',
    'hachisu_disk_wind',
    'show_minimum_separation_for_instant_RLOF',
    'show_minimum_orbital_period_for_instant_RLOF',
    'stellar_structure_algorithm',
    'stellar_structure_function',
    'AGB_core_algorithm',
    'AGB_radius_algorithm',
    'AGB_luminosity_algorithm',
    'AGB_3dup_algorithm',
    'evolution_splitting',
    'current_splitdepth',
    'evolution_splitting_maxdepth',
    'splitinfo',
    'evolution_splitting_sn_n',
    'speedtests',
    'stardata_dump_filename',
]
struct_libbinary_c_preferences_t._fields_ = [
    ('the_initial_abundances', c_double * (((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1)),
    ('initial_abundance_multiplier', c_double * (((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1)),
    ('minimum_timestep', c_double),
    ('maximum_timestep', c_double),
    ('maximum_nuclear_burning_timestep', c_double),
    ('tpagb_superwind_mira_switchon', c_double),
    ('tpagb_reimers_eta', c_double),
    ('vwind_multiplier', c_double),
    ('mass_accretion_rate1', c_double),
    ('mass_accretion_rate2', c_double),
    ('angular_momentum_accretion_rate1', c_double),
    ('angular_momentum_accretion_rate2', c_double),
    ('angular_momentum_accretion_rate_orbit', c_double),
    ('accretion_start_time', c_double),
    ('accretion_end_time', c_double),
    ('minimum_donor_menv_for_comenv', c_double),
    ('magnetic_braking_factor', c_double),
    ('magnetic_braking_gamma', c_double),
    ('wr_wind_fac', c_double),
    ('alpha_ce', c_double),
    ('lambda_ce', c_double),
    ('lambda_ionisation', c_double),
    ('lambda_enthalpy', c_double),
    ('accretion_limit_eddington_multiplier', c_double),
    ('accretion_limit_thermal_multiplier', c_double),
    ('accretion_limit_dynamical_multiplier', c_double),
    ('donor_limit_thermal_multiplier', c_double),
    ('donor_limit_dynamical_multiplier', c_double),
    ('nova_retention_fraction', c_double),
    ('beta_reverse_nova', c_double),
    ('nova_irradiation_multiplier', c_double),
    ('nova_faml_multiplier', c_double),
    ('rlof_angmom_gamma', c_double),
    ('nonconservative_angmom_gamma', c_double),
    ('alphaCB', c_double),
    ('gb_reimers_eta', c_double),
    ('CRAP_parameter', c_double),
    ('Bondi_Hoyle_accretion_factor', c_double),
    ('max_tpagb_core_mass', c_double),
    ('chandrasekhar_mass', c_double),
    ('max_neutron_star_mass', c_double),
    ('minimum_mass_for_carbon_ignition', c_double),
    ('minimum_mass_for_neon_ignition', c_double),
    ('max_HeWD_mass', c_double),
    ('minimum_helium_ignition_core_mass', c_double),
    ('gbwindfac', c_double),
    ('agbwindfac', c_double),
    ('tidal_strength_factor', c_double),
    ('merger_angular_momentum_factor', c_double),
    ('wind_djorb_fac', c_double),
    ('lw', c_double),
    ('mass_accretion_for_eld', c_double),
    ('accretion_rate_novae_upper_limit', c_double),
    ('accretion_rate_soft_xray_upper_limit', c_double),
    ('mass_for_Hestar_Ia_lower', c_double),
    ('mass_for_Hestar_Ia_upper', c_double),
    ('vw93_mira_shift', c_double),
    ('vw93_multiplier', c_double),
    ('nelemans_gamma', c_double),
    ('nelemans_minq', c_double),
    ('nelemans_max_frac_j_change', c_double),
    ('nelemans_n_comenvs', c_double),
    ('nelemans_recalc_eccentricity', Boolean),
    ('comenv_post_eccentricity', c_double),
    ('hachisu_qcrit', c_double),
    ('timestep_modulator', c_double),
    ('RLOF_timestep_modulator', c_double),
    ('RLOF_mdot_factor', c_double),
    ('f_massloss_during_MS_mergers', c_double),
    ('mixingpar_MS_mergers', c_double),
    ('qcrit', c_double * 16),
    ('qcrit_degenerate', c_double * 16),
    ('minimum_envelope_mass_for_third_dredgeup', c_double),
    ('lambda_min', c_double),
    ('delta_mcmin', c_double),
    ('lambda_multiplier', c_double),
    ('dtfac', c_double),
    ('hbbtfac', c_double),
    ('c13_eff', c_double),
    ('mc13_pocket_multiplier', c_double),
    ('yields_dt', c_double),
    ('yields_logdt', c_double),
    ('yields_startlogtime', c_double),
    ('observable_radial_velocity_minimum', c_double),
    ('start_time', c_double),
    ('api_log_filename_prefix', c_char * 4096),
    ('log_filename', c_char * 4096),
    ('cmd_line_random_seed', c_long),
    ('gbwind', c_int),
    ('tpagbwind', c_int),
    ('wr_wind', c_int),
    ('wd_kick_when', c_int),
    ('wd_kick_pulse_number', c_int),
    ('wd_kick_direction', c_int),
    ('sn_kick_distribution', c_int * 19),
    ('sn_kick_dispersion', c_double * 19),
    ('sn_kick_companion', c_double * 19),
    ('wind_angular_momentum_loss', c_int),
    ('jorb_RLOF_transfer_model', c_int),
    ('gravitational_radiation_model', c_int),
    ('BH_prescription', c_int),
    ('E2_prescription', c_int),
    ('RLOF_interpolation_method', c_int),
    ('nova_retention_method', c_int),
    ('WRLOF_method', c_int),
    ('wind_mass_loss', Boolean),
    ('wind_mass_loss_algorithm', c_int),
    ('comenv_prescription', c_int),
    ('qcrit_giant_branch_method', c_int),
    ('comenv_ejection_spin_method', c_int),
    ('comenv_merger_spin_method', c_int),
    ('magnetic_braking_algorithm', c_int),
    ('monte_carlo_sn_kicks', Boolean),
    ('post_SN_orbit_method', c_int),
    ('third_dup', Boolean),
    ('random_systems', c_int),
    ('batchmode', c_int),
    ('batch_submode', c_int),
    ('no_arg_checking', Boolean),
    ('rlperi', Boolean),
    ('rotationally_enhanced_mass_loss', c_int),
    ('rotationally_enhanced_exponent', c_double),
    ('repeat', c_int),
    ('RLOF_method', c_int),
    ('small_envelope_method', c_int),
    ('initial_abundance_mix', c_uint),
    ('initial_abunds_only', Boolean),
    ('type_Ia_MCh_supernova_algorithm', c_int),
    ('type_Ia_sub_MCh_supernova_algorithm', c_int),
    ('Seitenzahl2013_model', c_char * 12),
    ('NeNaMgAl', Boolean),
    ('yields_logtimes', Boolean),
    ('no_thermohaline_mixing', Boolean),
    ('pre_main_sequence', Boolean),
    ('pre_main_sequence_fit_lobes', Boolean),
    ('internal_buffering', c_int),
    ('internal_buffering_compression', c_int),
    ('hachisu_disk_wind', Boolean),
    ('show_minimum_separation_for_instant_RLOF', Boolean),
    ('show_minimum_orbital_period_for_instant_RLOF', Boolean),
    ('stellar_structure_algorithm', c_int),
    ('stellar_structure_function', CFUNCTYPE(UNCHECKED(None), c_int)),
    ('AGB_core_algorithm', c_int),
    ('AGB_radius_algorithm', c_int),
    ('AGB_luminosity_algorithm', c_int),
    ('AGB_3dup_algorithm', c_int),
    ('evolution_splitting', Boolean),
    ('current_splitdepth', c_int),
    ('evolution_splitting_maxdepth', c_int),
    ('splitinfo', POINTER(struct_splitinfo_t) * 10),
    ('evolution_splitting_sn_n', c_int),
    ('speedtests', Boolean),
    ('stardata_dump_filename', c_char * 4096),
]

# /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 1671
class struct_libbinary_c_stardata_t(Structure):
    pass

struct_splitinfo_t.__slots__ = [
    'stardata',
    'depth',
    'count',
]
struct_splitinfo_t._fields_ = [
    ('stardata', POINTER(struct_libbinary_c_stardata_t)),
    ('depth', c_int),
    ('count', c_int),
]

# /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 876
class struct_diffstats_t(Structure):
    pass

struct_diffstats_t.__slots__ = [
    'mass',
    'mc',
    'radius',
    'roche_radius',
    'age',
    'tms',
    'omega',
    'luminosity',
    'accretion_luminosity',
    'orbital_separation',
    'orbital_period',
    'orbital_eccentricity',
    'orbital_angular_frequency',
    'novarate',
    'v_eq_ratio',
    'time',
    'stellar_type',
    'SN_type',
    'sgl',
    'artificial_accretion',
]
struct_diffstats_t._fields_ = [
    ('mass', c_double * 2),
    ('mc', c_double * 2),
    ('radius', c_double * 2),
    ('roche_radius', c_double * 2),
    ('age', c_double * 2),
    ('tms', c_double * 2),
    ('omega', c_double * 2),
    ('luminosity', c_double * 2),
    ('accretion_luminosity', c_double * 2),
    ('orbital_separation', c_double),
    ('orbital_period', c_double),
    ('orbital_eccentricity', c_double),
    ('orbital_angular_frequency', c_double),
    ('novarate', c_double * 2),
    ('v_eq_ratio', c_double * 2),
    ('time', c_double),
    ('stellar_type', c_int * 2),
    ('SN_type', c_int * 2),
    ('sgl', Boolean),
    ('artificial_accretion', Boolean),
]

# /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 894
class struct_probability_distribution_t(Structure):
    pass

struct_probability_distribution_t.__slots__ = [
    'func',
    'xmin',
    'xmax',
    'x',
    'table',
    'nlines',
    'maxcount',
    'error',
]
struct_probability_distribution_t._fields_ = [
    ('func', CFUNCTYPE(UNCHECKED(c_double), POINTER(c_double))),
    ('xmin', c_double),
    ('xmax', c_double),
    ('x', c_double),
    ('table', POINTER(c_double)),
    ('nlines', c_int),
    ('maxcount', c_int),
    ('error', c_int),
]

# /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 1035
class struct_derivative_t(Structure):
    pass

struct_derivative_t.__slots__ = [
    'mass',
    'eccentricity',
    'specific_angular_momentum',
    'other',
]
struct_derivative_t._fields_ = [
    ('mass', c_double),
    ('eccentricity', c_double),
    ('specific_angular_momentum', c_double),
    ('other', c_double),
]

struct_libbinary_c_star_t.__slots__ = [
    'timescales',
    'luminosities',
    'GB',
    'new_supernova',
    'derivative',
    'mass',
    'radius',
    'effective_radius',
    'roche_radius',
    'roche_radius_at_periastron',
    'rmin',
    'phase_start_mass',
    'phase_start_core_mass',
    'age',
    'stellar_type_tstart',
    'epoch',
    'core_mass',
    'GB_core_mass',
    'CO_core_mass',
    'max_MS_core_mass',
    'core_radius',
    'luminosity',
    'accretion_luminosity',
    'omega',
    'omega_crit',
    'omega_core',
    'omega_core_crit',
    'l_acc',
    'angular_momentum',
    'core_angular_momentum',
    'q',
    'tbgb',
    'tms',
    'dM_in_timestep',
    'vwind',
    'mdot',
    'mcxx',
    'rol0',
    'aj0',
    'mass_at_RLOF_start',
    'rdot',
    'drdt',
    'stellar_timestep',
    'tm',
    'tn',
    'tkh',
    'max_EAGB_He_core_mass',
    'menv',
    'renv',
    'k2',
    'compact_core_mass',
    'effective_zams_mass',
    'pms_mass',
    'mcx_EAGB',
    'rzams',
    'rtms',
    'last_mass',
    'A0',
    'Xray_luminosity',
    'vrot0',
    'v_sph',
    'v_eq',
    'v_crit_sph',
    'v_crit_eq',
    'omega_ratio',
    'v_sph_ratio',
    'v_eq_ratio',
    'disc_mdot',
    'rotfac',
    'prev_r',
    'prev_dm',
    'roldot',
    'dmdr',
    'nth',
    'RLOF_tkh',
    'RLOF_starttime',
    'num_thermal_pulses',
    'num_thermal_pulses_since_mcmin',
    'dntp',
    'menv_1tp',
    'dmc_prev_pulse',
    'dm_3dup',
    'core_mass_no_3dup',
    'mc_bgb',
    'initial_mcx_EAGB',
    'time_first_pulse',
    'time_prev_pulse',
    'time_next_pulse',
    'model_time_first_pulse',
    'interpulse_period',
    'mc_1tp',
    'lambda_3dup',
    'nova_beta',
    'nova_faml',
    'v_Rwx',
    'v_Rwy',
    'v_Rwz',
    'v_Rw',
    'spiky_luminosity',
    'prev_tagb',
    'rho',
    'temp',
    'mira',
    'start_HG_mass',
    'hezamsmetallicity',
    'he_t',
    'he_f',
    'f_burn',
    'f_hbb',
    'temp_mult',
    'temp_rise_fac',
    'fmdupburn',
    'ftimeduphbb',
    'mixdepth',
    'mixtime',
    'dmmix',
    'conv_base',
    'dmacc',
    'dm_companion_SN',
    'max_mix_depth',
    'prev_depth',
    'he_mcmax',
    'mconv',
    'thickest_mconv',
    'Xenv',
    'Xacc',
    'Xinit',
    'Xyield',
    'Xsurfs',
    'Xhbb',
    'XWR0',
    'Xsource',
    'MS_mass',
    'mc_gb_was',
    'stellar_colour',
    'stellar_type',
    'detached_stellar_type',
    'companion_type',
    'starnum',
    'kick_WD',
    'already_kicked_WD',
    'dtlimiter',
    'SN_type',
    'was_SN',
    'prev_kick_pulse_number',
    'sn_last_time',
    'rstar',
    'first_dredge_up',
    'second_dredge_up',
    'newhestar',
    'first_dup_phase_in_firsttime',
    'blue_straggler',
    'PMS',
    'reject',
    'compact_core_type',
]
struct_libbinary_c_star_t._fields_ = [
    ('timescales', POINTER(c_double)),
    ('luminosities', POINTER(c_double)),
    ('GB', POINTER(c_double)),
    ('new_supernova', POINTER(struct_new_supernova_t)),
    ('derivative', c_double * 36),
    ('mass', c_double),
    ('radius', c_double),
    ('effective_radius', c_double),
    ('roche_radius', c_double),
    ('roche_radius_at_periastron', c_double),
    ('rmin', c_double),
    ('phase_start_mass', c_double),
    ('phase_start_core_mass', c_double),
    ('age', c_double),
    ('stellar_type_tstart', c_double),
    ('epoch', c_double),
    ('core_mass', c_double),
    ('GB_core_mass', c_double),
    ('CO_core_mass', c_double),
    ('max_MS_core_mass', c_double),
    ('core_radius', c_double),
    ('luminosity', c_double),
    ('accretion_luminosity', c_double),
    ('omega', c_double),
    ('omega_crit', c_double),
    ('omega_core', c_double),
    ('omega_core_crit', c_double),
    ('l_acc', c_double),
    ('angular_momentum', c_double),
    ('core_angular_momentum', c_double),
    ('q', c_double),
    ('tbgb', c_double),
    ('tms', c_double),
    ('dM_in_timestep', c_double),
    ('vwind', c_double),
    ('mdot', c_double),
    ('mcxx', c_double),
    ('rol0', c_double),
    ('aj0', c_double),
    ('mass_at_RLOF_start', c_double),
    ('rdot', c_double),
    ('drdt', c_double),
    ('stellar_timestep', c_double),
    ('tm', c_double),
    ('tn', c_double),
    ('tkh', c_double),
    ('max_EAGB_He_core_mass', c_double),
    ('menv', c_double),
    ('renv', c_double),
    ('k2', c_double),
    ('compact_core_mass', c_double),
    ('effective_zams_mass', c_double),
    ('pms_mass', c_double),
    ('mcx_EAGB', c_double),
    ('rzams', c_double),
    ('rtms', c_double),
    ('last_mass', c_double),
    ('A0', c_double),
    ('Xray_luminosity', c_double),
    ('vrot0', c_double),
    ('v_sph', c_double),
    ('v_eq', c_double),
    ('v_crit_sph', c_double),
    ('v_crit_eq', c_double),
    ('omega_ratio', c_double),
    ('v_sph_ratio', c_double),
    ('v_eq_ratio', c_double),
    ('disc_mdot', c_double),
    ('rotfac', c_double),
    ('prev_r', c_double),
    ('prev_dm', c_double),
    ('roldot', c_double),
    ('dmdr', c_double),
    ('nth', c_double),
    ('RLOF_tkh', c_double),
    ('RLOF_starttime', c_double),
    ('num_thermal_pulses', c_double),
    ('num_thermal_pulses_since_mcmin', c_double),
    ('dntp', c_double),
    ('menv_1tp', c_double),
    ('dmc_prev_pulse', c_double),
    ('dm_3dup', c_double),
    ('core_mass_no_3dup', c_double),
    ('mc_bgb', c_double),
    ('initial_mcx_EAGB', c_double),
    ('time_first_pulse', c_double),
    ('time_prev_pulse', c_double),
    ('time_next_pulse', c_double),
    ('model_time_first_pulse', c_double),
    ('interpulse_period', c_double),
    ('mc_1tp', c_double),
    ('lambda_3dup', c_double),
    ('nova_beta', c_double),
    ('nova_faml', c_double),
    ('v_Rwx', c_double),
    ('v_Rwy', c_double),
    ('v_Rwz', c_double),
    ('v_Rw', c_double),
    ('spiky_luminosity', c_double),
    ('prev_tagb', c_double),
    ('rho', c_double),
    ('temp', c_double),
    ('mira', c_double),
    ('start_HG_mass', c_double),
    ('hezamsmetallicity', c_double),
    ('he_t', c_double),
    ('he_f', c_double),
    ('f_burn', c_double),
    ('f_hbb', c_double),
    ('temp_mult', c_double),
    ('temp_rise_fac', c_double),
    ('fmdupburn', c_double),
    ('ftimeduphbb', c_double),
    ('mixdepth', c_double),
    ('mixtime', c_double),
    ('dmmix', c_double),
    ('conv_base', c_double),
    ('dmacc', c_double),
    ('dm_companion_SN', c_double),
    ('max_mix_depth', c_double),
    ('prev_depth', c_double),
    ('he_mcmax', c_double),
    ('mconv', c_double),
    ('thickest_mconv', c_double),
    ('Xenv', c_double * (((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1)),
    ('Xacc', c_double * (((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1)),
    ('Xinit', c_double * (((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1)),
    ('Xyield', c_double * (((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1)),
    ('Xsurfs', c_double * (((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1)),
    ('Xhbb', c_double * (((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1)),
    ('XWR0', c_double * (((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1)),
    ('Xsource', (c_double * (((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1)) * 26),
    ('MS_mass', c_double),
    ('mc_gb_was', c_double),
    ('stellar_colour', c_double * 24),
    ('stellar_type', c_int),
    ('detached_stellar_type', c_int),
    ('companion_type', c_int),
    ('starnum', c_int),
    ('kick_WD', Boolean),
    ('already_kicked_WD', Boolean),
    ('dtlimiter', c_int),
    ('SN_type', c_int),
    ('was_SN', c_int),
    ('prev_kick_pulse_number', c_int),
    ('sn_last_time', c_int),
    ('rstar', Boolean),
    ('first_dredge_up', Boolean),
    ('second_dredge_up', Boolean),
    ('newhestar', Boolean),
    ('first_dup_phase_in_firsttime', Boolean),
    ('blue_straggler', Boolean),
    ('PMS', Boolean),
    ('reject', Boolean),
    ('compact_core_type', c_int),
]

# /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 1387
class struct_libbinary_c_model_t(Structure):
    pass

struct_libbinary_c_model_t.__slots__ = [
    'log_fp',
    'api_log_fp',
    'derivative',
    'probability',
    'time',
    'max_evolution_time',
    'dt',
    'dtm',
    'time_remaining',
    'dtmwas',
    'dt_zoom',
    'prev_log_t',
    'tphys0',
    'tphys00',
    'dtm0',
    'next_yield_out',
    'prev_yield_out',
    'oldyields',
    'oldyieldsources',
    'lastensembleouttime',
    'ensemble_store',
    'gce_prevt',
    'ndonor',
    'naccretor',
    'id_number',
    'rejectcount',
    'intpol',
    'iter',
    'comenv_type',
    'reject',
    'RLOF_start',
    'RLOF_end',
    'supernova',
    'coel',
    'merged',
    'inttry',
    'sgl',
    'prec',
    'com',
    'supedd',
    'restart_run',
    'rerun',
    'exit_after_end',
    'in_RLOF',
    'bflag',
    'disk',
    'novae',
    'logflags',
    'logstack',
    'hachisu',
    'novalogged',
    'doingsplit',
    'split_last_time',
    'logflagstrings',
]
struct_libbinary_c_model_t._fields_ = [
    ('log_fp', POINTER(FILE)),
    ('api_log_fp', POINTER(FILE)),
    ('derivative', c_double * 18),
    ('probability', c_double),
    ('time', c_double),
    ('max_evolution_time', c_double),
    ('dt', c_double),
    ('dtm', c_double),
    ('time_remaining', c_double),
    ('dtmwas', c_double),
    ('dt_zoom', c_double),
    ('prev_log_t', c_double),
    ('tphys0', c_double),
    ('tphys00', c_double),
    ('dtm0', c_double),
    ('next_yield_out', c_double),
    ('prev_yield_out', c_double),
    ('oldyields', c_double * (((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1)),
    ('oldyieldsources', ((c_double * (((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1)) * 26) * 2),
    ('lastensembleouttime', c_double),
    ('ensemble_store', (c_double * 365) * 2),
    ('gce_prevt', c_double),
    ('ndonor', c_int),
    ('naccretor', c_int),
    ('id_number', c_int),
    ('rejectcount', c_int),
    ('intpol', c_int),
    ('iter', c_int),
    ('comenv_type', c_int),
    ('reject', Boolean),
    ('RLOF_start', Boolean),
    ('RLOF_end', Boolean),
    ('supernova', Boolean),
    ('coel', Boolean),
    ('merged', Boolean),
    ('inttry', Boolean),
    ('sgl', Boolean),
    ('prec', Boolean),
    ('com', Boolean),
    ('supedd', Boolean),
    ('restart_run', Boolean * 2),
    ('rerun', Boolean),
    ('exit_after_end', Boolean),
    ('in_RLOF', Boolean),
    ('bflag', Boolean),
    ('disk', Boolean),
    ('novae', Boolean),
    ('logflags', Boolean * 15),
    ('logstack', c_int * 15),
    ('hachisu', Boolean),
    ('novalogged', Boolean),
    ('doingsplit', Boolean),
    ('split_last_time', Boolean),
    ('logflagstrings', (c_char * 4096) * 15),
]

# /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 1519
class struct_libbinary_c_common_t(Structure):
    pass

struct_libbinary_c_common_t.__slots__ = [
    'logflag',
    'diffstats',
    'prev',
    'memo',
    'argv',
    'parameters_metallicity',
    'main_sequence_parameters',
    'giant_branch_parameters',
    'metallicity_parameters',
    'orbital_period',
    'metallicity',
    'effective_metallicity',
    'eccentricity',
    'separation',
    'orbital_angular_momentum',
    'orbital_angular_frequency',
    'tcirc',
    'km0',
    'max_mass_for_second_dredgeup',
    'zams_period',
    'zams_separation',
    'zams_eccentricity',
    'mdot_RLOF',
    'prevt_mdot_RLOF',
    'monte_carlo_kick_normfac',
    'XZAMS',
    'Xsolar',
    'nucsyn_metallicity',
    'dtguess',
    'nucsyn_log_count',
    'nucsyn_short_log_count',
    'mdot_RLOF_adaptive',
    'mdot_RLOF_H02',
    'suggested_timestep',
    'RLOF_rwas',
    'RLOF_rolwas',
    'done_single_star_lifetime',
    'pms',
    'sn_sc_header',
    'file_log_prev_rlof_exit',
    'file_log_prevtype',
    'file_log_prevstring',
    'file_log_n_rlof',
    'file_log_cached',
    'random_seed',
    'init_random_seed',
    'random_buffer_set',
    'random_buffer',
    'argc',
    'model_number',
    'interpolate_debug',
    'diffstats_set',
    'prevflags',
    'lockflags',
]
struct_libbinary_c_common_t._fields_ = [
    ('logflag', Boolean),
    ('diffstats', struct_diffstats_t),
    ('prev', struct_diffstats_t),
    ('memo', POINTER(struct_memoize_hash_t)),
    ('argv', POINTER(POINTER(c_char))),
    ('parameters_metallicity', c_double),
    ('main_sequence_parameters', c_double * 144),
    ('giant_branch_parameters', c_double * 116),
    ('metallicity_parameters', c_double * 15),
    ('orbital_period', c_double),
    ('metallicity', c_double),
    ('effective_metallicity', c_double),
    ('eccentricity', c_double),
    ('separation', c_double),
    ('orbital_angular_momentum', c_double),
    ('orbital_angular_frequency', c_double),
    ('tcirc', c_double),
    ('km0', c_double),
    ('max_mass_for_second_dredgeup', c_double),
    ('zams_period', c_double),
    ('zams_separation', c_double),
    ('zams_eccentricity', c_double),
    ('mdot_RLOF', c_double),
    ('prevt_mdot_RLOF', c_double),
    ('monte_carlo_kick_normfac', c_double),
    ('XZAMS', c_double * (((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1)),
    ('Xsolar', c_double * (((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((0 + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1) + 1)),
    ('nucsyn_metallicity', c_double),
    ('dtguess', c_double * 5),
    ('nucsyn_log_count', c_int),
    ('nucsyn_short_log_count', c_int),
    ('mdot_RLOF_adaptive', c_double),
    ('mdot_RLOF_H02', c_double),
    ('suggested_timestep', c_double),
    ('RLOF_rwas', c_double),
    ('RLOF_rolwas', c_double),
    ('done_single_star_lifetime', Boolean),
    ('pms', Boolean * 2),
    ('sn_sc_header', Boolean),
    ('file_log_prev_rlof_exit', c_double),
    ('file_log_prevtype', c_char * 20),
    ('file_log_prevstring', String),
    ('file_log_n_rlof', c_int),
    ('file_log_cached', Boolean),
    ('random_seed', c_long),
    ('init_random_seed', c_long),
    ('random_buffer_set', Boolean),
    ('random_buffer', struct_binary_c_drand48_data),
    ('argc', c_int),
    ('model_number', c_int),
    ('interpolate_debug', Boolean),
    ('diffstats_set', Boolean),
    ('prevflags', Boolean * 40),
    ('lockflags', (Boolean * (16 + 1)) * 2),
]

struct_libbinary_c_stardata_t.__slots__ = [
    'common',
    'star',
    'model',
    'preferences',
    'store',
    'tmpstore',
    'previous_stardata',
    'batchmode_setjmp_buf',
    'warmup_timer_offset',
    'cpu_is_warm',
    'evolving',
]
struct_libbinary_c_stardata_t._fields_ = [
    ('common', struct_libbinary_c_common_t),
    ('star', struct_libbinary_c_star_t * 2),
    ('model', struct_libbinary_c_model_t),
    ('preferences', POINTER(struct_libbinary_c_preferences_t)),
    ('store', POINTER(struct_libbinary_c_store_t)),
    ('tmpstore', POINTER(struct_libbinary_c_tmpstore_t)),
    ('previous_stardata', POINTER(struct_libbinary_c_stardata_t)),
    ('batchmode_setjmp_buf', jmp_buf),
    ('warmup_timer_offset', c_double),
    ('cpu_is_warm', Boolean),
    ('evolving', Boolean),
]

# /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 1711
class struct_RLOF_orbit_t(Structure):
    pass

struct_RLOF_orbit_t.__slots__ = [
    'dM_RLOF_lose',
    'dM_RLOF_transfer',
    'dM_RLOF_accrete',
    'dM_other',
]
struct_RLOF_orbit_t._fields_ = [
    ('dM_RLOF_lose', c_double),
    ('dM_RLOF_transfer', c_double),
    ('dM_RLOF_accrete', c_double),
    ('dM_other', c_double * 2),
]

# /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 1776
class struct_coordinate_t(Structure):
    pass

struct_coordinate_t.__slots__ = [
    'x',
    'y',
    'z',
]
struct_coordinate_t._fields_ = [
    ('x', c_double),
    ('y', c_double),
    ('z', c_double),
]

# /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 1783
class struct_kick_system_t(Structure):
    pass

struct_kick_system_t.__slots__ = [
    'separation',
    'eccentric_anomaly',
    'mean_anomaly',
    'orbital_speed',
    'orbital_speed_squared',
    'sinbeta',
    'cosbeta',
    'kick_speed',
    'kick_speed_squared',
    'phi',
    'sinphi',
    'cosphi',
    'omega',
    'sinomega',
    'cosomega',
    'new_orbital_speed',
    'new_orbital_speed_squared',
    'hn',
    'hn_squared',
    'bound_before_explosion',
]
struct_kick_system_t._fields_ = [
    ('separation', c_double),
    ('eccentric_anomaly', c_double),
    ('mean_anomaly', c_double),
    ('orbital_speed', c_double),
    ('orbital_speed_squared', c_double),
    ('sinbeta', c_double),
    ('cosbeta', c_double),
    ('kick_speed', c_double),
    ('kick_speed_squared', c_double),
    ('phi', c_double),
    ('sinphi', c_double),
    ('cosphi', c_double),
    ('omega', c_double),
    ('sinomega', c_double),
    ('cosomega', c_double),
    ('new_orbital_speed', c_double),
    ('new_orbital_speed_squared', c_double),
    ('hn', c_double),
    ('hn_squared', c_double),
    ('bound_before_explosion', Boolean),
]

# /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 1829
class struct_opacity_t(Structure):
    pass

struct_opacity_t.__slots__ = [
    'temperature',
    'density',
    'Z',
    'Fe',
    'H',
    'He',
    'C',
    'N',
    'O',
    'alpha',
]
struct_opacity_t._fields_ = [
    ('temperature', c_double),
    ('density', c_double),
    ('Z', c_double),
    ('Fe', c_double),
    ('H', c_double),
    ('He', c_double),
    ('C', c_double),
    ('N', c_double),
    ('O', c_double),
    ('alpha', c_double),
]

# /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 1842
class struct_equation_of_state_t(Structure):
    pass

struct_equation_of_state_t.__slots__ = [
    'error',
    'X',
    'Y',
    'Z',
    'temperature',
    'density',
    'lnf',
    'P',
    'Pgas',
    'Prad',
    'xh1',
    'xhe1',
    'xhe2',
    'nH2',
    'nHI',
    'nHII',
    'nHeI',
    'nHeII',
    'nHeIII',
    'ne',
    'nt',
    'gradad',
    'gradrad',
    'cs_ad',
    'Cp',
    'Cv',
    'U',
    'dP_dT_rho',
    'dP_drho_T',
    'dlnrho_dlnT_P',
    'include_radiation_in_ionization_region',
]
struct_equation_of_state_t._fields_ = [
    ('error', c_double),
    ('X', c_double),
    ('Y', c_double),
    ('Z', c_double),
    ('temperature', c_double),
    ('density', c_double),
    ('lnf', c_double),
    ('P', c_double),
    ('Pgas', c_double),
    ('Prad', c_double),
    ('xh1', c_double),
    ('xhe1', c_double),
    ('xhe2', c_double),
    ('nH2', c_double),
    ('nHI', c_double),
    ('nHII', c_double),
    ('nHeI', c_double),
    ('nHeII', c_double),
    ('nHeIII', c_double),
    ('ne', c_double),
    ('nt', c_double),
    ('gradad', c_double),
    ('gradrad', c_double),
    ('cs_ad', c_double),
    ('Cp', c_double),
    ('Cv', c_double),
    ('U', c_double),
    ('dP_dT_rho', c_double),
    ('dP_drho_T', c_double),
    ('dlnrho_dlnT_P', c_double),
    ('include_radiation_in_ionization_region', c_int),
]

# /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 1894
class struct_envelope_shell_t(Structure):
    pass

struct_envelope_shell_t.__slots__ = [
    'm',
    'dm',
    'r',
    'tau',
    'temperature',
    'density',
    'pressure',
    'grad',
    'gradad',
    'gradrad',
    'xion_h1',
    'xion_he1',
    'xion_he2',
    'X',
    'Y',
    'Z',
]
struct_envelope_shell_t._fields_ = [
    ('m', c_double),
    ('dm', c_double),
    ('r', c_double),
    ('tau', c_double),
    ('temperature', c_double),
    ('density', c_double),
    ('pressure', c_double),
    ('grad', c_double),
    ('gradad', c_double),
    ('gradrad', c_double),
    ('xion_h1', c_double),
    ('xion_he1', c_double),
    ('xion_he2', c_double),
    ('X', c_double),
    ('Y', c_double),
    ('Z', c_double),
]

# /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 1913
class struct_envelope_t(Structure):
    pass

struct_envelope_t.__slots__ = [
    'shells',
    'logl',
    'logt0',
    'logteff',
    'm',
    'fm',
    'x',
    'y',
    'z',
    'alpha',
    'rop',
    'tmax',
    'tauf',
    'nshells',
    'E_bind',
    'E1',
    'E2',
    'E3',
    'lambda_bind',
    'moment_of_inertia',
]
struct_envelope_t._fields_ = [
    ('shells', POINTER(struct_envelope_shell_t)),
    ('logl', c_double),
    ('logt0', c_double),
    ('logteff', c_double),
    ('m', c_double),
    ('fm', c_double),
    ('x', c_double),
    ('y', c_double),
    ('z', c_double),
    ('alpha', c_double),
    ('rop', c_double),
    ('tmax', c_double),
    ('tauf', c_double),
    ('nshells', c_int),
    ('E_bind', c_double),
    ('E1', c_double),
    ('E2', c_double),
    ('E3', c_double),
    ('lambda_bind', c_double),
    ('moment_of_inertia', c_double),
]

# /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 1935
class struct_stardata_dump_t(Structure):
    pass

struct_stardata_dump_t.__slots__ = [
    'dump_format',
    'svn_revision',
    'git_revision',
    'versionstring',
    'versioninfo',
    'sizeof_stardata_t',
    'sizeof_preferences_t',
    'preferences',
    'stardata',
    'previous_stardata',
]
struct_stardata_dump_t._fields_ = [
    ('dump_format', c_int),
    ('svn_revision', c_int),
    ('git_revision', c_char * 4096),
    ('versionstring', c_char * 50),
    ('versioninfo', c_char * (1024 * 1024)),
    ('sizeof_stardata_t', c_size_t),
    ('sizeof_preferences_t', c_size_t),
    ('preferences', struct_libbinary_c_preferences_t),
    ('stardata', struct_libbinary_c_stardata_t),
    ('previous_stardata', struct_libbinary_c_stardata_t),
]

# /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 1950
class struct_GSL_args_t(Structure):
    pass

struct_GSL_args_t.__slots__ = [
    'args',
    'error',
    'uselog',
    'func',
]
struct_GSL_args_t._fields_ = [
    ('args', c_void_p),
    ('error', c_int),
    ('uselog', Boolean),
    ('func', brent_function),
]

data_table_t = struct_data_table_t # /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 36

memoize_hash_t = struct_memoize_hash_t # /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 65

binary_c_file = struct_binary_c_file # /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 63

hsearch_data = struct_hsearch_data # /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 126

element_info_t = struct_element_info_t # /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 127

libbinary_c_store_t = struct_libbinary_c_store_t # /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 115

cmd_line_arg_t = struct_cmd_line_arg_t # /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 241

libbinary_c_tmpstore_t = struct_libbinary_c_tmpstore_t # /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 240

libbinary_c_star_t = struct_libbinary_c_star_t # /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 1047

new_supernova_t = struct_new_supernova_t # /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 329

splitinfo_t = struct_splitinfo_t # /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 866

libbinary_c_preferences_t = struct_libbinary_c_preferences_t # /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 340

libbinary_c_stardata_t = struct_libbinary_c_stardata_t # /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 1671

diffstats_t = struct_diffstats_t # /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 876

probability_distribution_t = struct_probability_distribution_t # /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 894

derivative_t = struct_derivative_t # /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 1035

libbinary_c_model_t = struct_libbinary_c_model_t # /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 1387

libbinary_c_common_t = struct_libbinary_c_common_t # /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 1519

RLOF_orbit_t = struct_RLOF_orbit_t # /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 1711

coordinate_t = struct_coordinate_t # /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 1776

kick_system_t = struct_kick_system_t # /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 1783

opacity_t = struct_opacity_t # /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 1829

equation_of_state_t = struct_equation_of_state_t # /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 1842

envelope_shell_t = struct_envelope_shell_t # /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 1894

envelope_t = struct_envelope_t # /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 1913

stardata_dump_t = struct_stardata_dump_t # /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 1935

GSL_args_t = struct_GSL_args_t # /home/izzard/git/binary_c/izzard/src/binary_c_structures.h: 1950

# No inserted files

