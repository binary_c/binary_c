############################################################
#
# Functions used in the unit_tests.py script,
# part of binary_c testing framework.
#
# (c) Robert Izzard 2022+
#
############################################################

from binary_c_ensemble_manager import binary_c_ensemble_manager
import bz2
import collections
import git
import itertools
import json
import multiprocessing
import os
import pprint
import psutil
import queue
import re
import subprocess
import sys
from deepdiff import DeepDiff
from prettytable import PrettyTable
pp = pprint.PrettyPrinter(indent=4)

class binary_c_unit_tests:
    """
    Binary_c's unit tests object.
    """

    ############################################################
    #
    # class variables
    #
    # You might want to change math_epsilon, but probably
    # should not change the rest.
    #
    # If you add new tests, you will have to generate new
    # test results by setting generate==True and running the
    # unit_tests.py script. Otherwise, generate should be False.
    #
    ############################################################
    binary_c = 'binary_c'
    c_log = '/dev/stdout' # log_filename output location : do not change!
    c_log_prefix = '__BINARY_C_LOG__'
    args_prefix = ['/usr/bin/env',
                   'BINARY_C_LOG_PREFIX=' + c_log_prefix]
    math_epsilon = 1e-6 # maybe change this
    generate = False
    test_results_filename = None
    manager = None
    job_queue = None
    result_queue = None
    encoding = 'utf-8'
    vb = False

    # use all logical cores
    if os.getenv('NUM_THREADS'):
        nprocesses = int(os.getenv('NUM_THREADS'))
    elif os.getenv('OMP_NUM_THREADS'):
        nprocesses = int(os.getenv('OMP_NUM_THREADS'))
    else:
        nprocesses =  max(1, psutil.cpu_count(logical=True))
    processes = []

    def __init__(self):
        if os.getenv('BINARY_C') == None:
            print("Please set the BINARY_C environment variable to point to the location of the binary_c checkout.")
            sys.exit(1)

        # set up filename : requires BINARY_C to be set
        self.test_results_filename = os.path.join(os.getenv('BINARY_C'),
                                                  'src',
                                                  'python',
                                                  'test_results.bz2')


        return None

    @staticmethod
    def _quotewrap(list):
        """
        Given a list, wrap each item in double quotes and return the new list
        """
        return [f'"{_x}"' for _x in list]

    def run_binary_c(self,key=None,args=[],prefix=None,vb=False):
        """
        Given a dict of args, run binary_c and return the output.

        Argument "key" is the name of the system, e.g. M2 to label
        a 2Msun star. This should be a string that can be used as
        a dict key.

        Returns a tuple (key,c_log=[],output=[]) where c_log is a list
        of lines of the log filename, and output is whatever is sent
        to stdout.

        Optional argument "prefix" is a list of arguments prefixed
        to the args, e.g. /usr/bin/env calls to set up the environment.

        Optional argument vb gives verbose output.

        TODO : get he return value of binary_c
        """
        if vb:
            print(f"Running binary_c system {key}")

        # prefix can be a call to env, for example
        # if empty, set to None
        if prefix is None:
            arglist = []
        else:
            arglist = prefix

        # TESTING ONLY:
        # uncomment this to change the physics
        #args += ['tpagbwind',2]

        # build and execute command
        cmd = ' '.join(
            self._quotewrap(arglist + [self.binary_c] + args)
            )

        proc = subprocess.Popen(cmd,
                                shell=True,
                                stdout=subprocess.PIPE)

        # split the c_log output into a separate array,
        # and return this and whatever is in stdout
        c_log = []
        output = []
        for line in iter(proc.stdout.readline,b''):
            l = line.decode(self.encoding).rstrip()
            if l.startswith(self.c_log_prefix):
                c_log.append(l[len(self.c_log_prefix):].split())
            else:
                output.append(l)
        return (key,c_log,output)

    def run_tests(self,tests):
        """
        Wrapper to run all the tests.
        """

        # perhaps load expected results : if we are generating
        # them, of course don't load them
        if not self.generate:
            print("Loading expected data ... ",end='')
            expected_results = self.load_expected_results()
            print("done")
        else:
            expected_results = {}

        # the results of each test are stored in all_results
        # so we can compare to expected_results
        all_results = {}

        # loop over tests
        for name in tests:
            skip_test = tests[name].get('skip',False)

            if skip_test:
                continue

            # make multithreading queue
            self.make_queues()

            print(f"Test: {name}")
            test = tests[name]

            # verbose logging?
            vb = tests[name].get('vb',False) or self.vb
            test['vb'] = vb

            expected = expected_results.get(name,None)

            if expected == None and not self.generate:
                print("Error: we have no expected results for this test, and generate if False. You need to generate the test results before you can compare them to anything.")
                sys.exit()

            # there are many tests, choose which
            # we should run

            if test['type'] == 'stellar type duration':
                results = self.test_duration(test)

            elif test['type'] == 'ensemble':
                results = self.test_ensemble(test)

            else:
                # test unknown
                print(f"Unknown test {name}")
                sys.exit(1)

            results = self.format_results(results)

            if self.generate:
                # save to all_results for later JSON generation
                if vb:
                    print("Generating results")
                all_results[name] = results

            else:
                # otherwise compare these results to expected
                if vb:
                    print("Running comparison")

                r = self.compare(
                    results,
                    expected,
                    name=name,
                    math_epsilon=test.get('math_epsilon',None)
                )
                if r != 0:
                    # failure: return code
                    return r

        if self.generate:
            print(" ... saving generated results.")
            self.save_expected_results(all_results)

        return 0

    def format_results(self,results):
        """
        Format results appropriately, i.e. convert ints and floats to strings
        as used by JSON. If we fail to do this, the loaded JSON will fail
        to compare appropriately to our dict.
        """
        return json.loads(json.dumps(results))

    def save_expected_results(self,all_results):
        """
        Save all results to one file, as JSON, for later comparison.
        This should be called when self.generate is True.
        """
        with bz2.open(self.test_results_filename,'wt',encoding=self.encoding) as f:
            json.dump(all_results,f)

    def load_expected_results(self):
        """
        Load expected results from a JSON file.

        Note: this means that ints and float keys are actually strings.
        """
        with bz2.open(self.test_results_filename,'rt') as f:
            return json.load(f)

    def compare(self,
                expected_results,
                test_results,
                name=None,
                math_epsilon=None):
        """
        Function to compare test_results to expected_results,
        and if they differ, stop.

        The optional parameter, math_epsilon, defaults
        to the object value (self.math_epislon) but can
        also be passed in to override the default.

        "name" is the human-readable name of the test.
        """
        if name == None:
            name = "unknown test name"
        if not math_epsilon:
            math_epsilon = self.math_epsilon

        # when we compare we must
        # convert keys and values to float: we need
        # to force this because deepdiff only applies the
        # epsilon comparison (using isclose()) to floats
        r = DeepDiff(self.dict_data_to_floats(test_results),
                     self.dict_data_to_floats(expected_results),
                     math_epsilon=math_epsilon)

        if len(r)==0:
            print(f" ... test {name} passed")
            return 0
        else:
            print(f" ... test {name} failed:")
            pp.pprint(r)
            return 1

    def make_args_list_and_key(self,argdict,key_list=None,args=[]):
        key = ""
        args = list(args).copy()
        if key_list == None:
            key_list = list(argdict.keys())
        for x in key_list:
            args += [x,argdict[x]]
            key += str(x) + '=' + str(argdict[x]) + ' '
        return args,key



    def test_duration(self,test):
        """
        Test function that calculates the duration spent in each phase of evolution.
        """
        vb = test.get('vb',False)
        test_results = {}
        outputs = {}
        c_logs = {}

        # expand the arguments into physics
        physics = binary_c_ensemble_manager.expand_args(test['binary_c_args'])
        fixed_args = ('log_filename','/dev/stdout')

        # queue systems
        for argdict in physics:
            args,key = self.make_args_list_and_key(argdict,
                                                   args=fixed_args)
            self.queue_system(args,self.args_prefix,key,vb=vb)

        # let the queues finish and get the results
        (c_logs,outputs) = self.finished_queueing()

        # now we have the logs and outputs,
        # extract durations in each stellar evolution phase
        for argdict in physics:
            args,key = self.make_args_list_and_key(argdict,
                                                   args=fixed_args)
            mass = argdict['M_1']
            c_log = c_logs[key]
            output = outputs[key]
            stellar_type = None
            time = 0.0
            stats = {
                'start' : {},
                'end' : {},
                'duration' : {},
            }
            for line in c_log[1:]:
                time = float(line[0])
                if stellar_type is None or line[3] != stellar_type:
                    # stellar type change
                    if stellar_type:
                        stats['end'][stellar_type] = time
                        stats['duration'][stellar_type] = \
                            stats['end'][stellar_type] - stats['start'][stellar_type]
                    stellar_type = line[3]
                    stats['start'][stellar_type] = time

            self.add_test_results(key,
                                  test,
                                  test_results,
                                  stats)
        return test_results

    def add_test_results(self,
                         key,
                         test,
                         test_results,
                         test_output):
        """
        """
        # save the test_results on a key by key basis
        test_results[key] = test_output

    def test_ensemble(self,test):
        """
        Test function that generates an ensemble for each star.
        """
        vb = test.get('vb',False)
        test_results = {}
        outputs = {}
        c_logs = {}

        # expand the arguments into physics
        physics = binary_c_ensemble_manager.expand_args(test['binary_c_args'])

        for argdict in physics:
            args,key = self.make_args_list_and_key(argdict)

            # args tell binary_c how to behave:
            # we use the scalar and chemical yield filters
            args += [
                'log_filename','/dev/stdout',
                'ensemble', 'True',
                'ensemble_filters_off', 'True',
                'ensemble_logtimes', 'True', # log
                'ensemble_logdt',0.1,
                'ensemble_startlogtime',0.1,
                ]
            for x in test['ensemble filters']:
                args += ['ensemble_filter_' + x , 1]

            if vb:
                print(f"Queue {key} = {args}")

            # queue the systems
            self.queue_system(args,
                              self.args_prefix,
                              key,
                              vb=vb)

        # let the queues finish and get the results
        if vb:
            print("Waiting for threads to join...")
        (c_logs,outputs) = self.finished_queueing()

        # now we have the logs and outputs,
        # extract durations in each stellar evolution phase
        for argdict in physics:
            args,key = self.make_args_list_and_key(argdict)
            c_log = c_logs[key]
            output = outputs[key]

            # clean up non-ensemble output
            for i,line in enumerate(output):
                # remove "Tick count" as this number is not deterministic
                output[i] = re.sub('^Tick.*','',output[i])
                output[i] = re.sub('^SINGLE_STAR_LIFETIME.*','',output[i])
                # split on space and commas
                output[i] = re.split("\s+|,", output[i])
                # remove empty data
                output[i] = [x for x in output[i] if x != '']
                # convert to float if possible
                output[i] = self.tryfloatlist(output[i])

            self.add_test_results(key,
                                  test,
                                  test_results,
                                  output)

        return test_results

    def make_queues(self):
        """
        Function to make unit-testing multithreading queues.
        """
        if self.manager == None:
            self.manager = multiprocessing.Manager()
        self.result_queue = self.manager.Queue()
        self.job_queue = self.manager.Queue()
        self.processes = []
        for ID in range(self.nprocesses):
            self.processes.append(
                multiprocessing.Process(
                    target = self.run_binary_c_systems,
                    args = (self.job_queue,self.result_queue),
                )
            )
        self.start_processes()

    def join_processes(self):
        """
        Join processes and return dicts of their c_logs and outputs.
        """
        for p in self.processes:
            p.join()
        c_logs = {}
        outputs = {}
        sentinel = object()
        for key,c_log,output in iter(self.result_queue.get,sentinel):
            c_logs[key] = c_log
            outputs[key] = output
            if self.result_queue.empty():
                break
        return c_logs,outputs

    def start_processes(self):
        """
        Function to start required processes
        """
        for p in self.processes:
            p.start()

    def send_stops_to_job_queue(self):
        """
        Function to send STOP messages to the job queue,
        hence stop the job queue.
        """
        for _ in range(self.nprocesses):
            self.job_queue.put("STOP")

    def run_binary_c_systems(self,
                             job_queue,
                             result_queue,
                             vb=False):
        """
        Worker function to run queued binary_c systems
        """
        for args,prefix,key,vb in iter(self.job_queue.get,
                                       "STOP"):
            if vb:
                print(f"Run binary_c : {args}")
            try:
                self.result_queue.put(
                    self.run_binary_c(key,
                                      args,
                                      prefix,
                                      vb=vb),
                    block=True)
            except:
                print("Failed to run system or queue its result.")
                exit()

    def queue_system(self,args,prefix,key,vb=False):
        """
        Function to queue a system of given args labelled by key.
        """
        self.job_queue.put((args,prefix,key,vb))

    def finished_queueing(self):
        """
        Finished queueing: return joined results.
        """
        self.send_stops_to_job_queue()
        return self.join_processes()


    def tryfloat(self,x):
        """
        Convert scalar x to float if possible.
        """
        try:
            newx = float(x)
        except:
            newx = x
        return newx

    def tryfloatlist(self,l):
        """
        Convert list l to floats, if possible.
        """
        newl = []
        for x in l:
            newl.append(self.tryfloat(x))
        return newl

    def dict_data_to_floats(self,
                            input_dict: dict) -> dict:
        """
        Function to convert all the keys and values of the
        dictionary to floats

        we need to convert keys to floats:
            this is ~ a factor 10 faster than David's ``recursive_change_key_to_float``
            routine, probably because this version only does the float conversion, nothing else.

        Args:
            input_dict: dict of which we want to turn all the keys to float types if possible

        Returns:
            new_dict: dict of which the keys have been turned to float types where possible
        """

        # this adopts the type correctly *and* is fast
        new_dict = type(input_dict)()

        for k, v in input_dict.items():
            # convert key to a float, if we can
            # otherwise leave as is
            newkey = self.tryfloat(k)

            # act on value(s)
            if isinstance(v, list):
                # list data
                new_dict[newkey] = [
                    self.dict_data_to_floats(item)
                    if isinstance(item, collections.abc.Mapping)
                    else self.tryfloat(item)
                    for item in v
                ]
            elif isinstance(v, collections.abc.Mapping):
                # dict, ordereddict, etc. data
                new_dict[newkey] = self.dict_data_to_floats(v)
            else:
                # assume all other data are scalars
                new_dict[newkey] = self.tryfloat(v)

        return new_dict
