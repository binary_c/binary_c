#!/usr/bin/env python3

"""

Script to make CEMP stats given ensemble runs.

Args:
    1) the ensemble data file
    2) (optional) the output PDF file, if not set we use EMP.pdf.

"""
from binarycpython.utils.ensemble import load_ensemble
from binarycpython.utils.dicts import AutoVivificationDict
import bz2
import collections
import gzip
import json
import math
import matplotlib
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import py_rinterpolate
from scipy.ndimage import gaussian_filter1d
from scipy.interpolate import make_interp_spline, BSpline
import seaborn as sns
import sys

# options in cmdline arguments
ensemble_file = str(sys.argv[1])
pdffile = sys.argv[2] if len(sys.argv)>2 else "EMP.pdf"
pdf = PdfPages(pdffile)

# load the ensemble and check it has data
print("Ensemble data from " + ensemble_file)
ensemble = load_ensemble(ensemble_file)['ensemble']
if not 'scalars' in ensemble:
    print("No scalars found in the ensemble: have your stars run for long enough?")
    exit()

# combine data from all the time bins to make "halo" counts
EMP_types = ("EMP","XEMP","CEMP","CNEMP","NEMP")
stellar_type_names = ["LMMS","MS","HG","GB","CHeB","EAGB","TPAGB","HeMS","HeHG","HeGB","HeWD","COWD","ONeWD","NS","BH"]
system_types = ('all stars','single','binary')
count = AutoVivificationDict()
for system in system_types:
    for EMP_type in EMP_types:
        count[system][EMP_type] = 0.0

for EMP_type in EMP_types:
    for system_type in system_types:
        if system_type in ensemble['scalars'] and \
           EMP_type in ensemble['scalars'][system_type]:
            for time in ensemble['scalars'][system_type][EMP_type].keys():
                count[system_type][EMP_type] += float(ensemble['scalars'][system_type][EMP_type][time])


for EMP_type in EMP_types:
    print("{type:5} : {count:8.2e} : {type:5}/EMP = {ratio:8.2f} % : binfrac {binfrac:8.2f} %".format(
        type=EMP_type,
        count=count['all stars'][EMP_type],
        ratio=100.0*count['all stars'][EMP_type]/count['all stars']['EMP'],
        binfrac=100.0*count['binary'][EMP_type]/count['all stars'][EMP_type] if count['all stars'][EMP_type] else 0.0
    ))

# combine HRD data
hrd = ensemble['HRD']
for subtype in hrd.keys():
    ensemble['HRD'][subtype]['all'] = AutoVivificationDict()
    for ratio in ensemble['HRD'][subtype]:
        for logTeff in ensemble['HRD'][subtype][ratio]['all']['logTeff']:
            for y in ['logL','logg']:
                for ydatum in ensemble['HRD'][subtype][ratio]['all']['logTeff'][logTeff][y]:
                    ensemble['HRD'][subtype]['all']['logTeff'][logTeff][y][ydatum] += ensemble['HRD'][subtype][ratio]['all']['logTeff'][logTeff][y][ydatum]


############################################################
# Seaborn plots
############################################################
plot_opts = {
    'force_all_plots' : True,
    'allow_smoothing' : True,
}
plots = collections.OrderedDict({

    'initial period distribution' : {
        'plot': True,
        'key' : 'log initial period distribution : label->dist',
        'subtypes' : ["EMP","XEMP","CEMP","CNEMP","NEMP"],
        'data' : "ensemble['initial distributions'][subtype + ' log initial period distribution : label->dist']['all']['log initial period']",
        'process' : {
            'type' : 'Gaussian',
            'width':0.3,
            'truncate':2.5,
            'forcezero' : False,
            'fill' : True,
        },
        'xlabel' : "$\log_{10}$(initial period/days)",
        'ylabel' : "Number of stars",
        'title' : "",
        'xlim' : [-0.5,10.5],
        'ylim' : [0,0.25],
    },
    'initial primary mass distribution' : {
        'plot': True,
        'key' : 'log initial primary mass distribution : label->dist',
        'subtypes' : ["EMP","XEMP","CEMP","CNEMP","NEMP"],
        'data' : "ensemble['initial distributions'][subtype + ' log initial primary mass distribution : label->dist']['all']['log initial primary mass'] if (subtype+' log initial primary mass distribution : label->dist') in ensemble['initial distributions'] else ensemble['initial distributions'][subtype + ' log initial mass distribution : label->dist']['all']['log initial mass']",
        'process' : {
            'type' : 'Gaussian',
            'width' : 0.1,
            'truncate' : 1.0,
            'forcezero' : True,
            'fill' : True,
            },
        'xlabel' : "$\log_{10}$(initial primary mass)",
        'ylabel' : "Number of stars",
        'title' : "",
        'yscale' : 'log',
        #'xlim': [-0.1,0.8],
        #'ylim': [1e-3,11],

    },
    'initial secondary mass distribution' : {
        'plot' : True,
        'key' : 'log initial secondary mass distribution : label->dist',
        'subtypes' : ["EMP","XEMP","CEMP","CNEMP","NEMP"],
        'data' : "ensemble['initial distributions'][subtype + ' log initial secondary mass distribution : label->dist']['all']['log initial secondary mass']",
        'xlabel' : "$\log_{10}$(initial secondary mass)",
        'ylabel' : "Number of stars",
        'title' : "",
        'xlim' : [-1.2,0.3],
        'ylim' : [-0.1, 5.0],
    },
    'EMP period distributions' : {
        'plot' : True,
        'subtypes': ["EMP","XEMP","CEMP","CNEMP","NEMP"],
        'data' : "ensemble['distributions'][subtype + ' period distribution : label->dist']['all']['log orbital period']",
        'process' : {
            'type' : 'Gaussian',
            'width':0.2,
            'truncate':2.0,
            'forcezero' : False,
            'fill' : True,
        },
        'xlabel' : "$\log_{10}$(orbital period/days)",
        'ylabel' : "Number of stars",
        'title' : "",
        'xlim' : [-4,9],
        'ylim' : [-0.02, 0.5],
    },
    'CEMP period distributions vs stellar type' : {
        'plot' : True if 'CEMP period distribution : label->dist' in ensemble['distributions'] else False,
        'subtypes': ensemble['distributions']['CEMP period distribution : label->dist']["stellar type"].keys() if 'CEMP period distribution : label->dist' in ensemble['distributions'] else None,
        'plotnamemap' : "stellar_type_names[int(subtype)]",
        'data' : "ensemble['distributions']['CEMP period distribution : label->dist']['stellar type'][subtype]['log orbital period']",
        'process' : {
            'type' : 'Gaussian',
            'width':0.2,
            'truncate':2.0,
            'forcezero' : False,
            'fill' : True,
        },
        'xlabel' : "$\log_{10}$(CEMP orbital period/days)",
        'ylabel' : "$\log_{10}$(Number of stars)",
        'title' : "",
        'xlim' : [-2.2,3.8],
        'ylim' : [-0.002, 0.06],

    },
    'Numbers vs time' : {
        'plot' : True,
        'subtypes' :  ["EMP","XEMP","CEMP","CNEMP","NEMP"],
        'data' : "ensemble['scalars']['all stars'][subtype]",
        'xlabel' : 'Time/Myr',
        'ylabel' : 'Number of stars',
        'xlim' : [11000, 14000],
        },

    'CEMP HRD' : {
        'plot' : 'heatmap',
        'data' : "ensemble['HRD']['CEMP']['all']",
        '2Dkeys' : ['logTeff','logL'],
        'xlabel' : '$\log(T_\mathrm{eff}/\mathrm{K})$',
        'ylabel' : '$\log(L/\mathrm{L}_{\odot})$',
        'process' : None,
        'logz' : True,
        'invert_xaxis' : True,
        'invert_yaxis' : True,
        'cbox_label' : '$\log_{10}$(Number of stars)',
        'title' : 'CEMP HRD',
    },
    'CEMP obs HRD' : {
        'plot' : 'heatmap',
        'data' : "ensemble['HRD']['CEMP']['all']",
        '2Dkeys' : ['logTeff','logg'],
        'xlabel' : '$\log(T_\mathrm{eff}/\mathrm{K})$',
        'ylabel' : '$\log(g)$',
        'process' : None,
        'logz' : True,
        'invert_xaxis' : True,
        'invert_yaxis' : False,
        'cbox_label' : '$\log_{10}$(Number of stars)',
        'title' : 'CEMP obs. HRD',
    },
    'HRD contour' : {
        'plot' : 'contour',
        'subtypes' : ['XEMP','CEMP','NEMP'],
        'data' : "ensemble['HRD'][subtype]['all']",
        '2Dkeys' : ['logTeff','logL'],
        'gridsize' : 200,
        'xlim' : [4.25,3.25],
        'ylim' : [-0.5,3.5],
        'shade' : False,
        'alpha' : 0.5,
        'xlabel' : '$\log(T_\mathrm{eff}/\mathrm{K})$',
        'ylabel' : '$\log(L/\mathrm{L}_{\odot})$',
        'process' : None,
        'logz' : False, # cannot be True for a contour plot
        'invert_xaxis' : True,
        'invert_yaxis' : True,
        'cbox_label' : '$\log_{10}$(Number of stars)',
    },
    'HRD filled contour' : {
        'plot' : 'contour',
        'subtypes' : ['XEMP','CEMP','NEMP'],
        'data' : "ensemble['HRD'][subtype]['all']",
        '2Dkeys' : ['logTeff','logL'],
        'gridsize' : 200,
        'xlim' : [4.25,3.25],
        'ylim' : [-0.5,3.5],
        'shade' : True,
        'alpha' : 0.3,
        'xlabel' : '$\log(T_\mathrm{eff}/\mathrm{K})$',
        'ylabel' : '$\log(L/\mathrm{L}_{\odot})$',
        'process' : None,
        'logz' : False, # cannot be True for a contour plot
        'invert_xaxis' : True,
        'invert_yaxis' : True,
        'cbox_label' : '$\log_{10}$(Number of stars)',
    },
})

def ordered(dict):
    """
    Return an ordered version of dict
    """
    return collections.OrderedDict(
        sorted(dict.items(),
               key = lambda x:(float(x[0]))))

def find_nearest_index(array,value):
    """
    Find index of nearest item in array to value.
    """
    array = np.asarray(array)
    idx = (np.abs(array-value)).argmin()
    return idx

def mindiff(list,tol=1e-10):
    """
    Given a list of unique values, within tol(=1e-7), find the
    minimum difference between any two elements.

    Return None on error.
    """
    if len(list)<2:
        return None

    for index,x in enumerate(sorted(list)):
        if index == 0:
            mindiff = sys.float_info.max
        else:
            diff = list[index]-list[index-1]
            if index > 1 and math.fabs(1.0-diff/mindiff) > tol:
                mindiff = min(mindiff,math.fabs(mindiff - diff))
            else:
                mindiff = min(mindiff,diff)

    if mindiff == sys.float_info.max:
        return None
    return mindiff

def non_uniform_savgol(x, y, window, polynom):
    """
    Applies a Savitzky-Golay filter to y with non-uniform spacing
    as defined in x

    This is based on https://dsp.stackexchange.com/questions/1676/savitzky-golay-smoothing-filter-for-not-equally-spaced-data
    The borders are interpolated like scipy.signal.savgol_filter would do

    Parameters
    ----------
    x : array_like
        List of floats representing the x values of the data
    y : array_like
        List of floats representing the y values. Must have same length
        as x
    window : int (odd)
        Window length of datapoints. Must be odd and smaller than x
    polynom : int
        The order of polynom used. Must be smaller than the window size

    Returns
    -------
    np.array of float
        The smoothed y values
    """
    if len(x) != len(y):
        raise ValueError('"x" and "y" must be of the same size')

    if len(x) < window:
        raise ValueError('The data size must be larger than the window size')

    if type(window) is not int:
        raise TypeError('"window" must be an integer')

    if window % 2 == 0:
        raise ValueError('The "window" must be an odd integer')

    if type(polynom) is not int:
        raise TypeError('"polynom" must be an integer')

    if polynom >= window:
        raise ValueError('"polynom" must be less than "window"')

    half_window = window // 2
    polynom += 1

    # Initialize variables
    A = np.empty((window, polynom))     # Matrix
    tA = np.empty((polynom, window))    # Transposed matrix
    t = np.empty(window)                # Local x variables
    y_smoothed = np.full(len(y), np.nan)

    # Start smoothing
    for i in range(half_window, len(x) - half_window, 1):
        # Center a window of x values on x[i]
        for j in range(0, window, 1):
            t[j] = x[i + j - half_window] - x[i]

        # Create the initial matrix A and its transposed form tA
        for j in range(0, window, 1):
            r = 1.0
            for k in range(0, polynom, 1):
                A[j, k] = r
                tA[k, j] = r
                r *= t[j]

        # Multiply the two matrices
        tAA = np.matmul(tA, A)

        # Invert the product of the matrices
        tAA = np.linalg.inv(tAA)

        # Calculate the pseudoinverse of the design matrix
        coeffs = np.matmul(tAA, tA)

        # Calculate c0 which is also the y value for y[i]
        y_smoothed[i] = 0
        for j in range(0, window, 1):
            y_smoothed[i] += coeffs[0, j] * y[i + j - half_window]

        # If at the end or beginning, store all coefficients for the polynom
        if i == half_window:
            first_coeffs = np.zeros(polynom)
            for j in range(0, window, 1):
                for k in range(polynom):
                    first_coeffs[k] += coeffs[k, j] * y[j]
        elif i == len(x) - half_window - 1:
            last_coeffs = np.zeros(polynom)
            for j in range(0, window, 1):
                for k in range(polynom):
                    last_coeffs[k] += coeffs[k, j] * y[len(y) - window + j]

    # Interpolate the result at the left border
    for i in range(0, half_window, 1):
        y_smoothed[i] = 0
        x_i = 1
        for j in range(0, polynom, 1):
            y_smoothed[i] += first_coeffs[j] * x_i
            x_i *= x[i] - x[half_window]

    # Interpolate the result at the right border
    for i in range(len(x) - half_window, len(x), 1):
        y_smoothed[i] = 0
        x_i = 1
        for j in range(0, polynom, 1):
            y_smoothed[i] += last_coeffs[j] * x_i
            x_i *= x[i] - x[-half_window - 1]

    return y_smoothed


def fill(original_data,tol=1e-10):
    """
    Fill x,y data lists with zero when there's no data
    """
    xdata = list(original_data.keys())
    ydata = list(original_data.values())
    dx = mindiff(xdata)

    if not dx:
        print("Warning: mindiff failed in fill() on {} data items. Returning original data.".format(len(xdata)))
        return original_data

    maxx = xdata[-1]
    x = xdata[0]
    newdata = {}
    index = 0
    while x<=maxx:
        matchi = find_nearest_index(xdata,x)
        matchx = xdata[matchi]

        if (x==0.0 and matchx==0.0) or abs(1.0-matchx/x)<=tol:
            newdata[x] = ydata[index]
            index += 1
        else:
            # missing data
            newdata[x] = 0.0
        x += dx

    return ordered(newdata)

def data_processing_1D(original_data,opts):
    """
    Process original_data, a dict of data, using the given method with its opts dict.
    Usually, this means we should smooth the data.

    Args:
        original_data : a dict of data with the x values as keys, y values as values.
        opts : a dict of options appropriate to the method

           Gaussian: width, truncate. These are the width of the Gaussian kernel and
                     how many widths outside the centre it should be truncated.
                     forcezero (default False) if True then when the y value in the original
                     data is zero, the y valuein the smoothed data is forced to be zero

           Savgol: width and polynom. See the function for details.

    Note: the x-spacing in the original data must be constant,
          which is usually the case with binary_c's ensembles.
          There can be gaps in the data as long as these are not
          almost everywhere.
    """

    # if we are required to fill the data with zeros, do so
    if opts.get('fill',None):
        original_data = fill(original_data)

    # convert original data to numpy arrays
    xdata = list(original_data.keys())
    ydata = list(original_data.values())

    # get original (const) spacing
    dx = mindiff(xdata)

    if not dx:
        print("Warning: mindiff failed in data_processing_1D() on {} data items. Returning original data.".format(len(xdata)))
        return original_data

    process = opts.get('process',None)
    if process:
        method = process.get('type',None)
    else:
        method = None

    if method is None or method == 'None' or \
       plot_opts['allow_smoothing'] is False:
        # no smoothing
        return original_data

    elif method == 'Gaussian':
        use_processing_opts = {
            'width' : 1.0,
            'truncate' : 4.0,
            'forcezero' : False
        }
        use_processing_opts.update(process)

        ysmooth = gaussian_filter1d(np.array(ydata),
                                    use_processing_opts['width']/dx,
                                    truncate=use_processing_opts['truncate'])
        if use_processing_opts['forcezero']:
            for index,y in enumerate(ydata):
                if y==0.0:
                    ysmooth[index] = 0.0

    elif method == 'Savgol':
        use_processing_opts = {
            'width' : 11,
            'polynom' : 1,
            }
        use_processing_opts.update(opts)
        ysmooth = non_uniform_savgol(xdata,
                                     ydata,
                                     use_processing_opts['width'],
                                     use_processing_opts['polynom'])

    # convert to dict and return
    ret = {}
    for i,x in enumerate(xdata):
        ret[x] = ysmooth[i]
    return ret

def doplot(key):
    """
    Return True if we want to make plot with the given key, otherwise False.

    If force_all_plots is set, we return that.
    """
    if plot_opts['force_all_plots']:
        return plot_opts['force_all_plots']
    else:
        if plots[key]['plot'] is False:
            return False
        else:
            return True


def get_data(opts):
    """
    Function to get data given an opts dict
    """

    # get raw data
    if opts['plot'] == 'heatmap' or opts['plot'] == 'contour':
        # 2D plot
        plotdata = {}
        xkey = opts['2Dkeys'][0]
        ykey = opts['2Dkeys'][1]
        dologz = opts['logz'] if 'logz' in opts else False

        if opts['plot'] == 'heatmap':
            # cannot have subtypes
            try:
                data = eval(opts['data'])
            except:
                data = None

            if data:
                xvalues = list(sorted(data[opts['2Dkeys'][0]]))
                _y = {}
                for x in xvalues:
                    for y in data[opts['2Dkeys'][0]][x][opts['2Dkeys'][1]]:
                        _y[y] = 1
                yvalues = sorted(_y.keys())
                for x in xvalues:
                    plotdata[x] = {}
                    for y in yvalues:
                        if y in data[xkey][x][ykey]:
                            _z = data[xkey][x][ykey][y]
                            if dologz:
                                plotdata[x][y] = math.log10(_z)
                            else:
                                plotdata[x][y] = _z
                        else:
                            plotdata[x][y] = None
        elif opts['plot'] == 'contour':
            # contour : can have subtypes
            plotdata = []
            if 'subtypes' in opts:
                dosubtypes = opts['subtypes']
            else:
                dosubtypes = [None]

            for subtype in dosubtypes:
                try:
                    data = eval(opts['data'])
                    dologz = opts['logz'] if 'logz' in opts else False
                    xvalues = list(sorted(data[opts['2Dkeys'][0]]))
                    _y = {}
                    for x in xvalues:
                        for y in data[opts['2Dkeys'][0]][x][opts['2Dkeys'][1]]:
                            _y[y] = 1
                    yvalues = sorted(_y.keys())

                    for x in xvalues:
                        for y in yvalues:
                            if y in data[xkey][x][ykey]:
                                _z = data[xkey][x][ykey][y]
                                if dologz:
                                    _z = math.log10(_z)
                            else:
                                _z = None
                            if subtype:
                                plotdata.append([x,y,_z,subtype])
                            else:
                                plotdata.append([x,y,_z])
                except:
                    pass

            plotdata = np.array(plotdata)

    else:
        # 1D plot
        plotdata = {}
        if 'subtypes' in opts and opts['subtypes'] is not None:
            for subtype in opts['subtypes']:
                if 'plotnamemap' in opts:
                    plotname = eval(opts['plotnamemap'])
                else:
                    plotname = subtype

                if 'data' in opts:
                    try:
                        data = eval(opts['data'])
                        plotdata[plotname] = data_processing_1D(ordered(data),
                                                                opts)
                    except:
                        pass
        else:
            plotdata = data_processing_1D(ordered(eval(opts['data'])),
                                          opts)
    return plotdata

def makeplot(plot,**kwargs):
    """
    Function to make a plot
    """
    if plots[plot]['plot'] is False or doplot(plot) is False:
        return

    opts = plots[plot]
    data = get_data(opts)

    # cannot plot no data
    if len(data) == 0:
        return

    sns.set_style("whitegrid",
                  {
                      'figure.facecolor' : 'white',
                      'axes.facecolor':'#F5F5F5',
                      'axes.edgecolor':'black',
                      'axes.grid' : False,
                      'axes.axisbelow': True,
                      'axes.spines.bottom' : True,
                      'axes.spines.top' : True,
                      'axes.spines.left' : True,
                      'axes.spines.right' : True,
                      'xtick.color' : 'black',
                      'xtick.direction' : u'in',
                      'xtick.bottom' : True,
                      'xtick.top' : True,
                      'ytick.color' : 'black',
                      'ytick.direction' : u'in',
                      'ytick.left' : True,
                      'ytick.right' : True,
                      'ytick.major.size' : 10.0,
                      'ytick.minor.size' : 10.0,
                  })

    # set colour palette
    sns.set_palette(opts.get('palette','dark'))

    if opts['plot'] == 'heatmap' or opts['plot'] == 'contour':
        # 2D data
        if opts['plot'] == 'heatmap':
            # 2D heatmap
            pdata = pd.DataFrame.from_dict(data)
            ax = sns.heatmap(data=pdata,
                             cmap = 'cubehelix_r',
                             linewidth=0,
                             annot=False,
                             cbar_kws={'label':opts.get('cbox_label',None)})

            if opts.get('invert_yaxis',False):
                ax.invert_yaxis()
        elif opts['plot'] == 'contour':
            # 2D contour plot (kde = kernel density estimation)
            xkey = opts['2Dkeys'][0]
            ykey = opts['2Dkeys'][1]

            # set columns used by kdeplot
            columns = opts['2Dkeys'] + ['weight']
            if opts.get('subtypes',None):
                columns += ['subtype']

            # set colour map
            if 'subtypes' in opts:
                cmap = None
            else:
                cmap = opts.get('cmap','Reds')

            # convert data to pandas dataframe with appropriate columns
            pdata = pd.DataFrame(data = data,
                                 columns = columns)
            ax = sns.kdeplot(data=pdata,
                             x = opts['2Dkeys'][0],
                             y = opts['2Dkeys'][1],
                             weights = 'weight',
                             #cmap = cmap,
                             shade = opts.get('shade',False),
                             alpha = opts.get('alpha',1.0),
                             gridsize = opts.get('gridsize',200),
                             hue = 'subtype' if 'subtypes' in opts else None,
                             )

        if opts.get('invert_xaxis',False):
            ax.invert_xaxis()
        for _,spine in ax.spines.items():
            spine.set_visible(True)
    else:
        # 1D line plot
        ax = sns.lineplot(data=data,
                          estimator=None)

    if ax:
        ax.grid = False
        set_args = ['title',
                    'xlabel','ylabel',
                    'xscale','yscale',
                    'xlim','ylim']
        for arg in set_args:
            x = opts.get(arg,None)
            if x:
                ax.set(**{ arg : opts[arg] })

        pdf.savefig()
        plt.close()
    return

############################################################
# make plots
############################################################
for plot in plots:
    makeplot(plot)

pdf.close()
print("Plotted in {}".format(pdffile))
