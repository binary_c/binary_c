#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Function to update the cumulative RLOF parameters
 * NOTE: in the next release this function will be expanded and will include things on angular momentum through different channels.
 */
#ifdef EVENT_BASED_LOGGING

void event_based_logging_update_cumulative_RLOF_parameters(struct stardata_t * stardata)
{
    /* get RLOFing stars */
    RLOF_stars;

    /* Time */
    stardata->common.event_based_logging_RLOF_episode_total_time_spent_masstransfer += stardata->model.dt;


    // NOTE: Since the unstable mass transfer routines do not work so well with the derivatives (YET), we should use the absolute difference between current and previous state
    Dprint("event_based_logging_update_cumulative_RLOF_parameters\n");

    /* mass */
    if(stardata->model.event_based_logging_RLOF_unstable == TRUE)
    {
        Dprint("UNSTABLE CUMULATIVE\n")

        /*
         * NOTE:
         *
         * The common envelope situation is a bit tricky because some of
         * the things below change in meaning compared to stable.
         * we should specify things better.
         *
         * event_based_logging_RLOF_episode_total_mass_accreted, for example,
         * is a bit unclear here.
         */
        stardata->common.event_based_logging_RLOF_episode_total_mass_lost_from_common_envelope = (stardata->previous_stardata->star[stardata->model.naccretor].mass + stardata->previous_stardata->star[stardata->model.ndonor].mass) - (stardata->star[stardata->model.naccretor].mass + stardata->star[stardata->model.ndonor].mass);
        stardata->common.event_based_logging_RLOF_episode_total_mass_lost += (stardata->previous_stardata->star[stardata->model.naccretor].mass + stardata->previous_stardata->star[stardata->model.ndonor].mass) - (stardata->star[stardata->model.naccretor].mass + stardata->star[stardata->model.ndonor].mass);
        stardata->common.event_based_logging_RLOF_episode_total_mass_accreted += fmax(0, stardata->star[stardata->model.naccretor].mass - stardata->previous_stardata->star[stardata->model.naccretor].mass);
        stardata->common.event_based_logging_RLOF_episode_total_mass_transferred += envelope_mass(&stardata->previous_stardata->star[stardata->model.ndonor]);
    }
    else
    {
        Dprint("STABLE CUMULATIVE\n")

        stardata->common.event_based_logging_RLOF_episode_total_mass_lost_from_accretor += accretor->derivative[DERIVATIVE_STELLAR_MASS_NONCONSERVATIVE_LOSS] * stardata->model.dt;
        stardata->common.event_based_logging_RLOF_episode_total_mass_lost += accretor->derivative[DERIVATIVE_STELLAR_MASS_NONCONSERVATIVE_LOSS] * stardata->model.dt;
        stardata->common.event_based_logging_RLOF_episode_total_mass_accreted += accretor->derivative[DERIVATIVE_STELLAR_MASS_RLOF_GAIN] * stardata->model.dt;
        stardata->common.event_based_logging_RLOF_episode_total_mass_transferred += fabs(donor->derivative[DERIVATIVE_STELLAR_MASS_RLOF_LOSS] * stardata->model.dt);
    }

    Dprint("stardata->common.event_based_logging_RLOF_episode_total_mass_lost_from_common_envelope: %g\n", stardata->common.event_based_logging_RLOF_episode_total_mass_lost_from_common_envelope);
    Dprint("stardata->common.event_based_logging_RLOF_episode_total_mass_lost_from_accretor: %g\n", stardata->common.event_based_logging_RLOF_episode_total_mass_lost_from_accretor);
    Dprint("stardata->common.event_based_logging_RLOF_episode_total_mass_lost: %g\n", stardata->common.event_based_logging_RLOF_episode_total_mass_lost);
    Dprint("stardata->common.event_based_logging_RLOF_episode_total_mass_accreted: %g\n", stardata->common.event_based_logging_RLOF_episode_total_mass_accreted);
    Dprint("stardata->common.event_based_logging_RLOF_episode_total_mass_transferred: %g\n", stardata->common.event_based_logging_RLOF_episode_total_mass_transferred);

    /* Handle the disk MT information */
    if(stardata->model.event_based_logging_RLOF_disk_accretion)
    {

        /* mass */
        if(stardata->model.event_based_logging_RLOF_unstable == TRUE)
        {

        }
        else
        {
            const double mass_transferred_through_disk =
                fabs(stardata->star[stardata->model.ndonor].derivative[DERIVATIVE_STELLAR_MASS_RLOF_LOSS] * stardata->model.dt);
            stardata->common.event_based_logging_RLOF_episode_total_mass_transferred_through_disk += mass_transferred_through_disk;
        }

        /* time */
        stardata->common.event_based_logging_RLOF_episode_total_time_spent_disk_masstransfer += stardata->model.dt;

        Dprint("stardata->common.event_based_logging_RLOF_episode_total_mass_transferred_through_disk: %g\n", stardata->common.event_based_logging_RLOF_episode_total_mass_transferred_through_disk);
        Dprint("stardata->common.event_based_logging_RLOF_episode_total_time_spent_disk_masstransfer: %g\n", stardata->common.event_based_logging_RLOF_episode_total_time_spent_disk_masstransfer);
    }

    Dprint("\n");
}

#endif // EVENT_BASED_LOGGING
