#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Return TRUE if event based logging
 * is switched on
 */
Boolean event_based_logging_on(struct stardata_t * const stardata)
{
    for(int i=0; i<EVENT_BASED_LOGGING_NUMBER; i++)
    {
        if(stardata->preferences->event_based_logging[i] == TRUE)
        {
            return TRUE;
        }
    }
    return FALSE;
}
