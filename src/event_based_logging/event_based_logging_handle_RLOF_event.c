#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef EVENT_BASED_LOGGING

/* Function to add the RLOF event */
void event_based_logging_handle_RLOF_event(struct stardata_t * const stardata)
{
    /*
     * RLOF episode information
     */
    Dprint("event based log RLOF\n");

    /*
     * Function call to set all the RLOF stuff,
     * as its used between different logging routines
     */
    event_based_logging_set_all_RLOF_parameters(stardata);
    Dprint("here\n");

    /* Track the start of RLOF episode */
    if(stardata->model.event_based_logging_RLOF_starting == TRUE)
    {
        /* Set initial values of the RLOF episode */
        Dprint(
            "SETTING START RLOF PARAMETERS\n"
        );

        event_based_logging_set_initial_RLOF_parameters(stardata);
    }
    Dprint("here\n");
    /* Add values of current RLOF episode */
    if(stardata->model.event_based_logging_RLOF == TRUE)
    {
        /* Add total values of the RLOF episode */
        Dprint(
            "UPDATING CUMULATIVE RLOF PARAMETERS\n"
        );

        event_based_logging_update_cumulative_RLOF_parameters(stardata);
    }
    Dprint("here\n");
    /* Track the end of the RLOF episode */
    if(stardata->model.event_based_logging_RLOF_ending == TRUE)
    {
        /* Set final values of the RLOF episode */
        Dprint(
            "SETTING FINAL RLOF PARAMETERS\n"
        );
        event_based_logging_set_final_RLOF_parameters(stardata);

        /* Add the RLOF episode to the event logstring list */
        Dprint(
            "HANDLING RLOF EVENT LOGSTRING WRITING\n"
        );
        event_based_logging_write_RLOF_event(stardata);

        /* reset the values of the RLOF episode */
        Dprint(
            "RESETTING RLOF PARAMETERS\n"
        );
        event_based_logging_reset_RLOF_episode_values(stardata);
    }
    Dprint("here\n");
}

#endif // EVENT_BASED_LOGGING
