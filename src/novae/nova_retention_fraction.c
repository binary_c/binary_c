#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "../binary_star_functions/donor_types.h"

/*
 * Function to estimate a star's nova mass retention fraction (the fraction of accreted material lost to nova).
 */
double Pure_function nova_retention_fraction(struct stardata_t * const stardata,
                                             struct star_t * const accretor,
                                             struct star_t Maybe_unused * const donor,
                                             const double accretion_rate Maybe_unused,
                                             const double steady_burn_rate Maybe_unused)
{
    double retention_fraction = 0.0;
    if(stardata->preferences->nova_retention_method==
       NOVA_RETENTION_ALGORITHM_CONSTANT)
    {
        /*
         * Use a constant nova retention fraction
         * as in the original BSE (Hurley et al. 2002)
         * prescription. Note that this may be negative.
         */
        retention_fraction =
            HYDROGEN_DONOR ? stardata->preferences->nova_retention_fraction_H :
            HELIUM_DONOR ? stardata->preferences->nova_retention_fraction_He :
            stardata->preferences->nova_retention_fraction_H;
    }
    else if(stardata->preferences->nova_retention_method==
            NOVA_RETENTION_ALGORITHM_CLAEYS2014)
    {
        /*
         * Follow the Claeys et al. (2014) algorithm which
         * is based on Hachisu et al. (1999),
         * Kato and Hachisu (2004) and
         * Meng et al. (2009).
         */
        if(HYDROGEN_DONOR || HELIUM_DONOR)
        {
            /*
             * eta_H following Hachisu et al. (1999)
             * C14 Eqs. B2 and B3
             */
            const double etaH = white_dwarf_H_accretion_efficiency(stardata,
                                                                   donor,
                                                                   accretor,
                                                                   accretion_rate);

            /*
             * Note: we must use etaH in the He accretion efficiency calculation
             * (thanks to Karel Temmink for pointing this out). This was not
             * pointed out in Joke Claey's original code.
             */
            const double etaHe = white_dwarf_He_accretion_efficiency(stardata,
                                                                     donor,
                                                                     accretor,
                                                                     etaH * accretion_rate);

            retention_fraction = HELIUM_DONOR ? etaHe : (etaH * etaHe);
            Dprint("retention_fraction: etaH = %g, etaHe = %g -> %g = (hydrogen donor? %d helium donor ? %d)\n",
                   etaH,
                   etaHe,
                   retention_fraction,
                   HYDROGEN_DONOR,
                   HELIUM_DONOR);
        }
        else
        {
            /*
             * What to do ? Donor is carbon rich or more evolved.
             * Assume 1.0...
             */
            retention_fraction = 1.0;
        }

        /*
         * The Claeys algorithm limited the minimum not to zero
         * but to the parameter put in on the command line, so you might
         * want to do that, but probably not.
         */
        //retention_fraction = Max(stardata->preferences->nova_retention_fraction_H,
        //                         retention_fraction);
        Dprint("final retention factor %g\n",retention_fraction);

    }
    else if(stardata->preferences->nova_retention_method==
            NOVA_RETENTION_ALGORITHM_HILLMAN2015)
    {
        /*
         * Todo...
         */
        retention_fraction = 0.0;
        Exit_binary_c(
            BINARY_C_ALGORITHM_BRANCH_FAILURE,
            "nova retention algorithm %d is not yet implemented",
            stardata->preferences->nova_retention_method
            );
    }
#ifdef KEMP_NOVAE
    else if(stardata->preferences->nova_retention_method==NOVA_RETENTION_ALGORITHM_WANGWU)
    {
        if(HYDROGEN_DONOR)
        {
            /*
             * Note: Under the current (5/7/22) treatment, He novae
             * under conditions of H accretion will be using the
             * retention fraction of H, which is applied to both H and He
             * nova episodes in that timestep.
             */
            retention_fraction = Hnova_etaH(stardata,
                                            accretor);
        }
        else if(HELIUM_DONOR)
        {
            retention_fraction = Henova_etaHe(stardata,
                                              accretor);
        }
    }
#endif // KEMP_NOVAE
    else
    {
        Exit_binary_c(
            BINARY_C_ALGORITHM_BRANCH_FAILURE,
            "%d is not a valid nova retention algorithm",
            stardata->preferences->nova_retention_method
            );
    }
    return retention_fraction;
}
