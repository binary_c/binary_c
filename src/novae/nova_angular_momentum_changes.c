#include "../binary_c.h"
No_empty_translation_unit_warning;


void nova_angular_momentum_changes(
    struct stardata_t * const stardata,
    struct star_t * const star,
    const double dm,
    double * const dJstar,
    double * const dJorb
    )
{
    /*
     *  Angular momentum changes that happen because of novae.
     *
     * dm is either a mass lost (e.g. in a nova event) or the
     * time deriative. If it is a derivative, then similarly
     * dJstar and dJorb, the changes of the stellar and orbital
     * angular momenta, should point to the appropriate derivatives.
     *
     * Usually dm is negative, because the exploding star loses mass.
     *
     * If dm is positive, this function should deal with nova re-accretion.
     */

    struct star_t * const companion =
        Other_star_struct(star);

    const double q = companion->mass / star->mass;

    /*
     * Assume novae lose mass spherically,
     * and the companion gains mass in a Keplerian disc.
     */
    const double k = dm < 0.0 ? (2.0/3.0) : 1.0;

    *dJstar =
        dm * k * Pow2(star->radius) * star->omega;

    *dJorb =
        dm / star->mass *
        q / (1.0 + q) *
        stardata->common.orbit.angular_momentum;

    if(dm < 0.0)
    {
        /*
         * this is the WD undergoing novae,
         * only let this contribute to FAML
         */
        *dJorb +=
            star->nova_faml *
            dm *
            stardata->common.orbit.angular_momentum;
    }
}
