#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef KEMP_NOVAE
static double _20(const double log10Mdot);
static double _60(const double log10Mdot);
static double _70(const double log10Mdot);
static double _80(const double log10Mdot);
static double _90(const double log10Mdot);
static double _100(const double log10Mdot);
static double _110(const double log10Mdot);
static double _120(const double log10Mdot);
static double _130(const double log10Mdot);
static double _135(const double log10Mdot);
static double _144(const double log10Mdot);

double Hnova_etaH(struct stardata_t * Restrict const stardata,
                  struct star_t * const accretor)
{
    /*
     * based on data from Wang (2018)
     * https://ui.adsabs.harvard.edu/abs/2018RAA....18...49W/abstract
     *
     * note on variable naming: higher && lower in
     * the 'fit' variable name refers to the mass of the WD curve
     *
     * e.g. a 1.38 Msun WD curve will be the 'higher',
     * compared to a 1.35 Msun WD curve.
     *
     * returns: eta_H, the accretion efficiency (nova retention fraction)
     *          for a H nova at a given M_WD and Mdot.
     *
     * Required units for inputs: M_sun and M_sun/yr (for M_WD and the
     * accretion rate respectively)
     */

    const double mdot = Mdot_net(accretor);
    if(mdot <= 0.0)
    {
        return 0.0;
    }
    else
    {
        const double log10Mdot = log10(mdot);
        const double M_WD = Limit_range(accretor->mass,0.2,1.44);

        /*
         * Interpolate by mass
         */

#define FUNCLIST \
    X(  20)      \
    X(  60)      \
    X(  70)      \
    X(  80)      \
    X(  90)      \
    X( 100)      \
    X( 110)      \
    X( 120)      \
    X( 130)      \
    X( 135)      \
    X( 144)

#undef X
#define X(M100) (0.01*(M100)),
        static const double masses[] = { FUNCLIST };
#undef X
#define X(M100) _ ## M100,
        static double (*funcs[])(const double) = { FUNCLIST };
#undef X

        /*
         * Find spanning indices based on current mdot
         */
        const rinterpolate_counter_t low =
            rinterpolate_bisearch(masses,
                                  M_WD,
                                  Array_size(masses));

        const rinterpolate_counter_t high = low + 1;

        /*
         * Return interpolation, shifted by nova_eta_shift
         * and limited to the range [0,1].
         */
        return
            Limit_range(interp_lin(M_WD,
                                   masses[low],
                                   masses[high],
                                   funcs[low](log10Mdot),
                                   funcs[high](log10Mdot)) +
                        stardata->preferences->nova_eta_shift,
                        0.0,
                        1.0);

    }
}




static double _20(const double log10Mdot)
{
    if(log10Mdot > -10.8)
    {
        return 0.03375668*Pow6(log10Mdot) + 1.96936922*Pow5(log10Mdot) +
            47.85435449*Pow4(log10Mdot) + 619.95702757*Pow3(log10Mdot) +
            4516.25622905*Pow2(log10Mdot) + 17541.05736755*log10Mdot + 28378.89454540;
    }
    else
    {
        return 0.0375*log10Mdot + 0.551;
    }
}
static double _60(const double log10Mdot)
{
    if(log10Mdot > -8.097)
    {
        return 1.26882419*Pow3(log10Mdot) + 30.51329427*Pow2(log10Mdot)
            + 244.71761203*log10Mdot + 654.81045119;
    }
    else if(log10Mdot > -10.0)
    {
        return 0.0658*log10Mdot + 0.7992;
    }
    else
    {
        return 0.0375*log10Mdot + 0.515;
    }
}
static double _70(const double log10Mdot)
{
    if(log10Mdot > -8.222)
    {
        return (0.60632932*Pow3(log10Mdot) + 14.74271288*Pow2(log10Mdot) +
                       119.52930871*log10Mdot + 323.39248424);
    }
    else if(log10Mdot > -10.0)
    {
        return 0.0719*log10Mdot + 0.845;
    }
    else
    {
        return 0.0375*log10Mdot + 0.489;
    }
}


static double _80(const double log10Mdot)
{
    if(log10Mdot>-8.222)
    {
        return -0.50065077*Pow4(log10Mdot) - 14.76592690*Pow3(log10Mdot)
            - 162.29194136*Pow2(log10Mdot) - 786.96965746*log10Mdot - 1418.36919563;
    }
    else if(log10Mdot>-10.0)
    {
        return 0.0444*log10Mdot + 0.5501;
    }
    else
    {
        return 0.0375*log10Mdot + 0.472;
    }
}
static double _90(const double log10Mdot)
{
    if(log10Mdot>-8.222)
    {
        return 0.74958587*Pow4(log10Mdot) + 23.11975024*Pow3(log10Mdot) +
            267.36253222*Pow2(log10Mdot) + 1374.05805223*log10Mdot + 2648.42148035;
    }
    else if(log10Mdot > -10.0)
    {
        return 0.0321*log10Mdot + 0.4261;
    }
    else
    {
        return 0.0375*log10Mdot + 0.464;
    }
}
static double _100(const double log10Mdot)
{
    if(log10Mdot > -7.301)
    {
        return (18.23435276*Pow4(log10Mdot) + 514.63820396*Pow3(log10Mdot) +
                      5445.75095214*Pow2(log10Mdot) + 25606.40507043*log10Mdot + 45143.59657352);
    }
    else if(log10Mdot > -10.0)
    {
        return 0.061*log10Mdot + 0.6977;
    }
    else
    {
        return 0.0375*log10Mdot + 0.459;
    }
}
static double _110(const double log10Mdot)
{
    if(log10Mdot > -6.481)
    {
        return 29.464*log10Mdot + 191.59;
    }
    else if(log10Mdot>-10.0)
    {
        return 0.01257896*Pow6(log10Mdot) + 0.63924936*Pow5(log10Mdot)
            + 13.49899565*Pow4(log10Mdot) + 151.61793668*Pow3(log10Mdot) +
            955.30681729*Pow2(log10Mdot) + 3201.56125284*log10Mdot + 4458.98741717;
    }
    else
    {
        return 0.0375*log10Mdot + 0.478;
    }
}
static double _120(const double log10Mdot)
{
    if(log10Mdot > -6.398)
    {
        return 38.404*log10Mdot + 246.09;
    }
    else if(log10Mdot>-10.0)
    {
        return 0.01863275*Pow4(log10Mdot) + 0.63375932*Pow3(log10Mdot) +
            8.03922144*Pow2(log10Mdot) + 45.11100840*log10Mdot + 94.67330838;
    }
    else
    {
        return 0.0375*log10Mdot + 0.426;
    }
}
static double _130(const double log10Mdot)
{
    if(log10Mdot > -6.301)
    {
        return 63.255*log10Mdot + 399.02;
    }
    else if(log10Mdot > -10.0)
    {
        return 0.00761654*Pow6(log10Mdot) + 0.38074886*Pow5(log10Mdot) +
            7.90781049*Pow4(log10Mdot) + 87.34785494*Pow3(log10Mdot) +
            541.22637679*Pow2(log10Mdot) + 1783.79685277*log10Mdot + 2443.49600493;
    }
    else
    {
        return 0.0375*log10Mdot + 0.442;
    }
}
static double _135(const double log10Mdot)
{
    if(log10Mdot > -6.301)
    {
        return 13.644*log10Mdot + 86.505;
    }
    else if(log10Mdot > -10.0)
    {
        return 0.02356744*Pow4(log10Mdot) + 0.79605073*Pow3(log10Mdot) +
            10.02225648*Pow2(log10Mdot) + 55.77497830*log10Mdot + 116.04245802;
    }
    else
    {
        return 0.0375*log10Mdot + 0.511;
    }
}
static double _144(const double log10Mdot)
{
    if(log10Mdot > -6.301)
    {
        return 13.644*log10Mdot + 86.545;
    }
    else if(log10Mdot > -10.0)
    {
        return 0.02356744*Pow4(log10Mdot) + 0.79605073*Pow3(log10Mdot) +
            10.02225648*Pow2(log10Mdot) + 55.77497830*log10Mdot + 116.08245802;
    }
    else
    {
        return 0.0375*log10Mdot + 0.551;
    }
}
#endif // KEMP_NOVAE
