#include "../binary_c.h"
No_empty_translation_unit_warning;
#include "../binary_star_functions/donor_types.h"


/*
 * Event handling function for novae: when
 * stardata->preferences->individual_novae is TRUE this
 * function is called to update the stars and orbit
 * when a nova happens.
 *
 * When you choose individual_novae to be on, nova
 * derivatives (in star->derivative and stardata->model.derivative)
 * are zero on input to this function. We set them here if
 * __SET_DERIVS is defined so the
 * various logging functions (which follow this function call) know what
 * the approximate nova rates would be.
 *
 * Novae are treated as instanteous events here, much like
 * a supernova would be.
 */

#define __SET_DERIVS

Event_handler_function nova_event_handler(void * const eventp Maybe_unused,
                                          struct stardata_t * const stardata,
                                          void * data)
{
    //struct binary_c_event_t * const event = eventp;

    struct binary_c_nova_event_t * const nova_data =
        (struct binary_c_nova_event_t *) data;

    struct star_t * const donor = nova_data->donor;
    struct star_t * const exploder = nova_data->accretor;

#ifdef NOVA_DEBUG
    printf("caught nova event at %20.12g : exploder = %d with H-layer mass %g, donor = %d : f = %g\n",
           stardata->model.time,
           exploder->starnum,
           exploder->dm_novaH,
           donor->starnum,
           nova_data->f
        );
#endif

    /*
     * Calculate mass lost: note that dm_lost is positive,
     * while dm_loss_rate is negative.
     */
    double dm_lost;
    double dm_loss_rate;

#ifdef KEMP_NOVAE
    if(nova_data->type == NOVA_TYPE_HYDROGEN)
    {
        /*
         * Hydrogen nova
         */

        /*
         * Store previous values
         */
        const double H_was = exploder->dm_novaH;
        const double He_was = exploder->dm_novaHe;

        /*
         * How many novae have exploded?
         */
        const int n_novae = (int)(exploder->dm_novaH / exploder->dm_novaHcrit);

        /*
         * How much mass do these novae burn from H to He?
         */
        const double dm_nova = n_novae * exploder->dm_novaHcrit;

#ifdef NOVA_DEBUG
        printf("H-nova M=%g (prev step %g, prev nova %g -> mass growth %g) at %12.5f dt=%g : dm/dmcrit = %g/%g = %d -> eject dm_nova = %g\n",
               exploder->mass,
               stardata->previous_stardata->star[exploder->starnum].mass,
               exploder->mass_after_nova,
               exploder->mass - exploder->mass_after_nova,
               stardata->model.time,
               stardata->model.dt,
               exploder->dm_novaH,
               exploder->dm_novaHcrit,
               n_novae,
               dm_nova);
#endif

        /*
         * Convert this mass from H to He,
         * noting that only a fraction nova_data->f
         * is retained, the rest is lost.
         */
        exploder->dm_novaHe += nova_data->f * dm_nova;
        exploder->dm_novaH -= nova_data->f * dm_nova;
#ifdef NOVA_DEBUG
        printf("H-nova REMOVE %g * %g = %g: H shell now %g\n",
               nova_data->f,
               dm_nova,
               nova_data->f * dm_nova,
               exploder->dm_novaH);
#endif

        /*
         * Apply mass loss because of the nova explosion
         */
        dm_lost = (1.0 - nova_data->f) * dm_nova;
        exploder->dm_novaH -= dm_lost;

        /*
         * Net derivatives for logging
         */
        dm_loss_rate = -dm_lost / stardata->model.dt;
#ifdef __SET_DERIVS
        exploder->derivative[DERIVATIVE_STELLAR_MASS_NOVA] = dm_loss_rate;
        exploder->derivative[DERIVATIVE_STELLAR_H_LAYER_MASS] = -(H_was - exploder->dm_novaH) / stardata->model.dt;
        exploder->derivative[DERIVATIVE_STELLAR_He_LAYER_MASS] = -(He_was - exploder->dm_novaHe) / stardata->model.dt;
#endif
    }
    else if(nova_data->type == NOVA_TYPE_HELIUM)
    {
        /*
         * Helium nova
         */
        const double H_was = exploder->dm_novaH;
        const double He_was = exploder->dm_novaHe;

        /*
         * Accretion rate
         */
        const double accretion_rate = Mdot_gain(exploder);

        /*
         * Compute number of H and He novae
         */
        const int n_H_novae = (int)(exploder->dm_novaH / exploder->dm_novaHcrit);
        const int n_He_novae = (int)(exploder->dm_novaHe / exploder->dm_novaHecrit);

        /*
         * Hence mass converted in H and He novae
         */
        const double dm_H_nova = n_H_novae * exploder->dm_novaHcrit;
        const double dm_He_nova = n_He_novae * exploder->dm_novaHecrit;

        Dprint(
            "HE NOVA %d %d : mass that explodes %g %g : mass in shells %g %g\n",
            n_H_novae,
            n_He_novae,
            dm_H_nova,
            dm_He_nova,
            exploder->dm_novaH,
            exploder->dm_novaHe
            );

        /*
         * Do H novae first, noting that only a fraction nova_data->f
         * is retained, the rest is lost.
         */
        exploder->dm_novaH -= nova_data->f * dm_H_nova;
        exploder->dm_novaHe += nova_data->f * dm_H_nova;

        /*
         * Then do He novae, these burn into the star (i.e. CO or ONe)
         */
        double dm_lost_H, dm_lost_He;

        if(HYDROGEN_DONOR)
        {
            /*
             * Hydrogen donor
             *
             * How this works:
             *
             * You could build up sufficient He for a He nova through
             * steady accretion of H or successive H novae.
             *
             * This opens the possibility of both H and He novae
             * occuring within the same timestep, or that a He nova
             * will occur while HYDROGEN_DONOR is true.
             *
             * Therefore, we need to make sure any H or He nova that
             * occurs uses the correct accretion efficiency.
             *
             * If H novae occur as well as He novae in the timestep,
             * assume that the H novae occur first and that the He
             * nova occurs almost immediately after.
             *
             * This assumption has implications for what we do with
             * the leftover H after the H nova.
             *
             * If we assume the He nova occurs at the end of the timestep,
             * then any left over H would just be blown away with the
             * He nova.
             *
             * If we assume that the He nova occurs almost instantaneously with
             * the (last, if multiple) H nova though, instead we just say
             * that any H material that we have left over the critical
             * ignition mass for H was accreted after, the He nova, and
             * contributes to the next H nova normally.
             *
             * We assume the seccond situation.
             *
             * Note that this should not be an issue for steady H burning.
             * In this case, the H shell is zero. In the steady burning case,
             * the important thing is that we are using the He nova accretion
             * efficiency rather than the H one (assuming a prescription treating
             * these conditions differently has been selected by the user).
             */


            /*
             * compute mass lost through H novae
             */
            dm_lost_H = (1.0 - nova_data->f) * dm_H_nova;

            /*
             * recompute f to account for a He (rather than H) nova
             *
             * temporarily pretend we were accreting He all along so
             * the retention fraction function works. We do this
             * by setting the donor stellar type to HeMS, which means
             *
             * HYDROGEN_DONOR == FALSE
             * HELIUM_DONOR == TRUE
             *
             * And after calling nova_retention_fraction() just set the
             * stellar type back to whatever it was.
             */
            {
                const Stellar_type stellar_type_was = donor->stellar_type;
                donor->stellar_type = HeMS; /* pretend we are a helium donor */
                nova_data->f = nova_retention_fraction(stardata,
                                                       exploder,
                                                       donor,
                                                       accretion_rate,
                                                       nova_data->steady_burn_rate);
                donor->stellar_type = stellar_type_was; /* restore */
            }
            exploder->dm_novaHe -= nova_data->f * dm_He_nova;
            exploder->core_mass[CORE_CO] += nova_data->f * dm_He_nova;

            /*
             * Compute mass lost through He novae
             */
            dm_lost_He = (1.0 - nova_data->f) * dm_He_nova;

            /*
             * return the DONOR states (necessary) and return f to the
             * H nova value (not strictly necessary but probably a good idea)
             * HYDROGEN_DONOR=TRUE;
             * HELIUM_DONOR=FALSE;
             */
            nova_data->f = nova_retention_fraction(stardata,
                                                   exploder,
                                                   donor,
                                                   accretion_rate,
                                                   nova_data->steady_burn_rate);
        }
        else
        {
            /*
             * Helium donor
             */
            exploder->dm_novaHe -= nova_data->f * dm_He_nova;
            exploder->core_mass[CORE_CO] += nova_data->f * dm_He_nova;

            /*
             * Compute mass loss
             */
            dm_lost_H = (1.0 - nova_data->f) * dm_H_nova;
            /*
             * note that dm_H_nova will almost always be 0
             * in the case of He accretion, signifying no H
             * novae in the timestep.
             */
            dm_lost_He = (1.0 - nova_data->f) * dm_He_nova;
        }

        /*
         * Apply the mass loss
         */
        dm_lost = dm_lost_H + dm_lost_He;
        exploder->dm_novaH -= dm_lost_H;
        exploder->dm_novaHe -= dm_lost_He;

        /*
         * Net derivatives for logging
         */
        dm_loss_rate = -dm_lost / stardata->model.dt;
#ifdef __SET_DERIVS
        exploder->derivative[DERIVATIVE_STELLAR_MASS_NOVA] = dm_loss_rate;
        exploder->derivative[DERIVATIVE_STELLAR_H_LAYER_MASS] =
            -(H_was - exploder->dm_novaH) / stardata->model.dt;
        exploder->derivative[DERIVATIVE_STELLAR_He_LAYER_MASS] =
            -(He_was - exploder->dm_novaHe) / stardata->model.dt;
#endif//__SET_DERIVS
    }
    else
    {
        dm_lost = 0.0;
    }
#else
    /*
     * Only hydrogen novae
     */
    dm_lost = (1.0 - nova_data->f) * exploder->dm_novaH;
    dm_loss_rate = - dm_lost / stardata->model.dt;
#endif // KEMP_NOVAE

    /*
     * Re-accretion of a fraction
     */
    const double dm_gain = exploder->nova_beta * dm_lost;

#ifndef KEMP_NOVAE
    /*
     * Simulate a nova in the derivative
     * (although this is only really useful for logging)
     */
    exploder->derivative[DERIVATIVE_STELLAR_MASS_NOVA] = dm_loss_rate;
    donor->derivative[DERIVATIVE_STELLAR_MASS_NOVA] = + dm_gain / stardata->model.dt;

    /*
     * Remove material from the white dwarf:
     * no hydrogen is left on the surface
     *
     * Here I just remove dm_lost from dm_novaH... but you
     * could argue this material is mixed in and becomes part
     * of the white dwarf after the nova.
     */

    exploder->dm_novaH = 0.0;
#endif

    /* apply stellar mass loss */
    update_mass(stardata,exploder, -dm_lost);
    exploder->dmacc = 0.0;

    /* testing */
    exploder->mass_after_nova = exploder->mass;
    exploder->time_prev_nova = stardata->model.time;
#ifdef NOVA_DEBUG
    printf("H- SET PREV NOVA TIME %15.2f\n",
           exploder->time_prev_nova);
#endif

#ifdef NUCSYN
    /*
     * calculate nova yield abundances
     */
    double Xnova[ISOTOPE_ARRAY_SIZE];
    nucsyn_set_nova_abunds(stardata,
                           exploder,
                           donor->Xenv,//THIS IS WRONG
                           Xnova);
#endif//NUCSYN

    /*
     * yield material in nova explosion
     */
    calc_yields(stardata,
                exploder,
                dm_lost,
#ifdef NUCSYN
                Xnova,
#endif // NUCSYN
                0.0,
#ifdef NUCSYN
                NULL,
#endif // NUCSYN
                exploder->starnum,
                YIELD_NOT_FINAL,
                SOURCE_NOVAE);

    /*
     * Accrete some on the companion?
     */
    if(Is_not_zero(dm_gain))
    {

        /*
         * Put material on companion star: we put it
         * in the accretion layer, let it get mixed
         * on the next timestep
         */
        update_mass(stardata,donor,dm_gain);
        donor->dmacc += dm_gain;

#ifdef NUCSYN
        /*
         * Put accreted material into the star's accretion layer
         */
        nucsyn_dilute_shell(
            donor->dmacc,
            donor->Xacc,
            dm_gain,
            Xnova
            );

        /*
         * Perhaps mix the star's accretion layer into its envelope
         */
        if(nucsyn_thermohaline_unstable(stardata,donor))
        {
            nucsyn_thermohaline_mix_star(stardata,
                                         donor);
        }
#endif // NUCSYN

        /*
         * "negative-yield" accreted material
         */
        calc_yields(stardata,
                    donor,
                    0.0,
#ifdef NUCSYN
                    NULL,
#endif // NUCSYN
                    dm_gain,
#ifdef NUCSYN
                    Xnova,
#endif // NUCSYN
                    exploder->starnum,
                    YIELD_NOT_FINAL,
                    SOURCE_NOVAE);
    }

    /*
     * update stellar and orbital angular momenta
     * caused by mass loss from the exploder
     */
    double dJstar,dJorb;
    nova_angular_momentum_changes(
        stardata,
        exploder,
        -dm_lost,
        &dJstar,
        &dJorb
        );
    exploder->angular_momentum += dJstar;
    stardata->common.orbit.angular_momentum += dJorb;
    exploder->derivative[DERIVATIVE_STELLAR_ANGMOM_NOVA] += dJstar/stardata->model.dt;
    stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM_NOVA] += dJorb/stardata->model.dt;

    /*
     * update stellar and orbital angular momenta
     * caused by mass gained onto the donor
     */
    nova_angular_momentum_changes(
        stardata,
        donor,
        dm_gain,
        &dJstar,
        &dJorb
        );
    donor->angular_momentum += dJstar;
    stardata->common.orbit.angular_momentum += dJorb;
    stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM_NOVA] += dJorb/stardata->model.dt;
    donor->derivative[DERIVATIVE_STELLAR_ANGMOM_NOVA] += dJstar/stardata->model.dt;

    /*
     * update orbit
     */
    update_orbital_variables(stardata,
                             &stardata->common.orbit,
                             &stardata->star[0],
                             &stardata->star[1]);


    return NULL;
}
