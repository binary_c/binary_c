#include "../binary_c.h"
No_empty_translation_unit_warning;


double Pure_function nova_explosion_layer_mass(struct stardata_t * Restrict const stardata Maybe_unused,
                                               struct star_t * const accretor)

{
    /*
     * Mass of the nova layer when it explodes.
     *
     * Result is in Msun.
     */

    /*
      const double dm =
        8.66e4 * (1e-9) *
        (1.54 * pow(accretor->mass,-7.0/3.0)
         - 2.0 / accretor->mass
         +0.65 * cbrt(accretor->mass));
    */

    /*
     * Livio and Truran (1992) Eq. 1
     */
    const double dm = 6.3e-5 * pow(Pow4(100.0*accretor->radius) / accretor->mass,
                                   0.7);

//#define XYZXYZ
#ifdef XYZXYZ
    /*
     * Alternative from Martin+2011
     */
    /*
    double Pcrit = 1e20; // critical pressure
    double dMcrit = 4.0 * PI * Pow4(accretor->radius * R_SUN) * Pcrit/
        (GRAVITATIONAL_CONSTANT * accretor->mass * Pow2(M_SUN));
        printf("DM %g %g\n",dm,dMcrit);
    */

    /* Martin and Tout 2011 */
    dm = 9.42e-4 * Pow4(accretor->radius * 1e-9) / accretor->mass;

    /* Townsley and Bildsten 2004 */
    const double alpha = 3.2e-4 * pow(accretor->mass, 1.231);
    const double beta = 0.534 * pow(accretor->mass, 0.605);
    const double mdot = Mdot_net(accretor);

    dm = alpha * pow(mdot * 1e8, -beta);
#endif

    return dm;
}
