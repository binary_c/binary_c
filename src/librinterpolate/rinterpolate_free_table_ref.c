#define __BINARY_C_LINT_SKIP

#include "../binary_c_code_options.h"
#include "../binary_c_error_codes.h"
#include "../binary_c_exit_macros.h"
#include "../breakpoints/breakpoints_prototypes.h"
#include "../binary_c_exit_prototypes.h"

#ifndef __HAVE_LIBRINTERPOLATE__

#include "rinterpolate.h"
#include "rinterpolate_internal.h"

/*
 * Given a pointer to a table, find it in rinterpolate_data
 * and remove it from the list of tables.
 *
 * This function does NOT remove references to any
 * nested min_max table.
 */

void rinterpolate_free_table_ref(struct rinterpolate_data_t * RESTRICT const rinterpolate_data,
                                 struct rinterpolate_table_t * const table)
{

    if(rinterpolate_data != NULL &&
       table != NULL)
    {
        for(rinterpolate_counter_t i=0;i<rinterpolate_data->number_of_interpolation_tables;i++)
        {
            struct rinterpolate_table_t * const t = rinterpolate_data->tables[i];
            if(likely(t != NULL) &&
               unlikely(t == table))
            {
                rinterpolate_data->tables[i] = NULL;
            }
        }
    }
}

#endif // __HAVE_LIBRINTERPOLATE__

typedef int prevent_ISO_C_warning;

        