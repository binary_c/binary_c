#define __BINARY_C_LINT_SKIP

#include "../binary_c_code_options.h"
#include "../binary_c_error_codes.h"
#include "../binary_c_exit_macros.h"
#include "../breakpoints/breakpoints_prototypes.h"
#include "../binary_c_exit_prototypes.h"

#ifndef __HAVE_LIBRINTERPOLATE__

#include "rinterpolate.h"
#include "rinterpolate_internal.h"
/*
 * provide exp10() for OSX etc.
 */

#ifndef RINTERPOLATE_HAVE_NATIVE_EXP10

double exp10(double x)
{
    return pow(10.0,x);
}

#endif // __HAVE_NATIVE_EXP10

#endif // __HAVE_LIBRINTERPOLATE__

typedef int prevent_ISO_C_warning;

        