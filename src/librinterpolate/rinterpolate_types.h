#pragma once
#ifndef RINTERPOLATE_TYPES_H
#define RINTERPOLATE_TYPES_H


/* types */
typedef unsigned int rinterpolate_counter_t;
typedef int rinterpolate_signed_counter_t;
typedef double rinterpolate_float_t;
typedef bool rinterpolate_Boolean_t;



#endif // RINTERPOLATE_TYPES_H
