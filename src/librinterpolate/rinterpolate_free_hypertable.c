#define __BINARY_C_LINT_SKIP

#include "../binary_c_code_options.h"
#include "../binary_c_error_codes.h"
#include "../binary_c_exit_macros.h"
#include "../breakpoints/breakpoints_prototypes.h"
#include "../binary_c_exit_prototypes.h"

#ifndef __HAVE_LIBRINTERPOLATE__

#include "rinterpolate.h"
#include "rinterpolate_internal.h"
/*
 * Given a hypertable struct, free everything in it.
 */

void rinterpolate_free_hypertable(struct rinterpolate_hypertable_t * RESTRICT hypertable)
{
    if(hypertable)
    {
        Safe_free(hypertable->data);
        Safe_free(hypertable->f);
        Safe_free(hypertable->sum);
    }
}

#endif // __HAVE_LIBRINTERPOLATE__

typedef int prevent_ISO_C_warning;

        