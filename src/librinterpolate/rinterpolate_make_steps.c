#define __BINARY_C_LINT_SKIP

#include "../binary_c_code_options.h"
#include "../binary_c_error_codes.h"
#include "../binary_c_exit_macros.h"
#include "../breakpoints/breakpoints_prototypes.h"
#include "../binary_c_exit_prototypes.h"

#ifndef __HAVE_LIBRINTERPOLATE__

#include "rinterpolate.h"
#include "rinterpolate_internal.h"

void rinterpolate_make_steps(struct rinterpolate_data_t * RESTRICT const rinterpolate_data MAYBE_UNUSED,
                             struct rinterpolate_table_t * RESTRICT const table)
{
    /*
     * Find the variable steps in a rinterpolate data table,
     * that is how many lines between changes in a parameter
     * in each column.
     */

    /*
     * Allocate space for the steps if required
     */
    if(table->steps == NULL)
    {
        table->steps =
            Rinterpolate_calloc(table->n,sizeof(rinterpolate_counter_t));
    }

    if(unlikely(table->steps==NULL))
    {
        rinterpolate_error(RINTERPOLATE_CALLOC_FAILED,
                           "(m|c)alloc failed in interpolate() : steps/steps_array\n",
                           table->parent);
    }

    /* loop, find where variables change */
#ifdef RINTERPOLATE_DEBUG
    Rinterpolate_print("Find where vars change (i.e. set steps_array/steps) \n");
#endif

    if(table->l<2)
    {
        /* special case : one line of data */
        for(rinterpolate_counter_t j=0; j<table->n; j++)
        {
            table->steps[j] = 1;
        }
    }
    else
    {
        /* loop over columns */
        for(rinterpolate_counter_t j=0; j<table->n; j++)
        {
            table->steps[j] = table->l; // fallback

            /* loop over lines */
            for(rinterpolate_counter_t i=1; i<table->l; i++)
            {
                Rinterpolate_print("Table %p, data %p, Line %u/%u, item %u/%u : cf %g to prev %g \n",
                                   (void*)table,
                                   (void*)table->data,
                                   i,
                                   table->l,
                                   j,
                                   table->n,
                                   table->data[(i-1)*table->line_length + j],
                                   table->data[i    *table->line_length + j]);

                /*
                 * Compare this line to the previous,
                 * if different, set the step
                 */
#ifdef RINTERPOLATE_DEBUG
                Rinterpolate_print("cf. data at indices %zu and %zu\n",
                                   (size_t)((i-1)*table->line_length + j),
                                   (size_t)(i    *table->line_length + j));
                Rinterpolate_print("cf. pointers %p and %p\n",
                                   (void*)(table->data + (size_t)((i-1)*table->line_length + j)),
                                   (void*)(table->data + (size_t)(i    *table->line_length + j)));
                Rinterpolate_print("cf. data are %g and %g (delta %g)\n",
                                   table->data[(i-1)*table->line_length + j],
                                   table->data[i    *table->line_length + j],
                                   table->data[(i-1)*table->line_length + j] -
                                   table->data[i    *table->line_length + j]
                    );
                Rinterpolate_print("!Fequal? %d\n",
                                   !Fequal(table->data[(i-1)*table->line_length + j],
                                           table->data[i    *table->line_length + j]));
#endif

                if(!Fequal(table->data[(i-1)*table->line_length + j],
                           table->data[i    *table->line_length + j]))
                {
                    // change
                    table->steps[j] = i;
                    Rinterpolate_print("SET STEPS for var %u to %u\n",j,table->steps[j]);
                    i = table->l + 1; // break i loop
                }
            }
        }
    }
}

#endif // __HAVE_LIBRINTERPOLATE__

typedef int prevent_ISO_C_warning;

        