#define __BINARY_C_LINT_SKIP

#include "../binary_c_code_options.h"
#include "../binary_c_error_codes.h"
#include "../binary_c_exit_macros.h"
#include "../breakpoints/breakpoints_prototypes.h"
#include "../binary_c_exit_prototypes.h"

#ifndef __HAVE_LIBRINTERPOLATE__

#include "rinterpolate.h"
#include "rinterpolate_internal.h"

/*
 * Vector functions for librinterpolate
 */

struct rinterpolate_vector_t * rinterpolate_new_vector(const rinterpolate_counter_t n,
                                                       const rinterpolate_float_t * const data)
{
    /*
     * Make a new vector, v, of size n and return it.
     * On malloc error, return NULL.
     */
    const size_t datasize = sizeof(rinterpolate_float_t)*n;
    struct rinterpolate_vector_t * v =
        Rinterpolate_malloc(sizeof(struct rinterpolate_vector_t));
    if(v)
    {
        v->n = n;
        v->data = Rinterpolate_malloc(sizeof(rinterpolate_float_t)*n);
        if(unlikely(v->data == NULL))
        {
            /* error: v->data = NULL so free v and return NULL */
            Safe_free(v);
        }
        else if(v->data && data)
        {
            memcpy(v->data,
                   data,
                   datasize);
        }
    }
    return v;
}

struct rinterpolate_vector_t * rinterpolate_new_clear_vector(const rinterpolate_counter_t n)
{
    struct rinterpolate_vector_t * const v =
        rinterpolate_new_vector(n,NULL);
    memset(v->data,0,sizeof(rinterpolate_float_t)*n);
    return v;
}

void rinterpolate_set_vector_data(struct rinterpolate_vector_t * const v,
                                  const rinterpolate_float_t * const data)
{
    memcpy(v->data,
           data,
           sizeof(rinterpolate_float_t) * v->n);
}

void rinterpolate_free_vector(struct rinterpolate_vector_t ** v)
{
    if(v && *v)
    {
        Safe_free((*v)->data);
        Safe_free(*v);
    }
}

rinterpolate_Boolean_t
rinterpolate_vectors_equal(const struct rinterpolate_vector_t * const v1,
                           const struct rinterpolate_vector_t * const v2)
{
    if(v1->n != v2->n)
    {
        return FALSE;
    }
    else
    {
        for(rinterpolate_counter_t i=0; i<v1->n; i++)
        {
            if(!Fequal(v1->data[i],v2->data[i]))
            {
                return FALSE;
            }
        }
        return TRUE;
    }
}

rinterpolate_Boolean_t
rinterpolate_vector_equal_floatlist(const struct rinterpolate_vector_t * const v,
                                    const rinterpolate_float_t * const floatlist)
{
    return rinterpolate_vector_equal_floatlist_n(v,floatlist,v->n);
}

rinterpolate_Boolean_t
rinterpolate_vector_equal_floatlist_n(const struct rinterpolate_vector_t * const v,
                                      const rinterpolate_float_t * const floatlist,
                                      const rinterpolate_counter_t n)
{
    for(rinterpolate_counter_t i=0; i<n; i++)
    {
        if(!Fequal(v->data[i],floatlist[i]))
        {
            return FALSE;
        }
    }
    return TRUE;
}

rinterpolate_float_t
rinterpolate_vector_absdiff_floatlist(const struct rinterpolate_vector_t * const v,
                                      const rinterpolate_float_t * const floatlist)
{
    rinterpolate_float_t d = 0.0;
    for(rinterpolate_counter_t i=0; i<v->n; i++)
    {
        d += fabs(v->data[i] - floatlist[i]);
    }
    return d;
}

rinterpolate_float_t
rinterpolate_vector_diff_floatlist(const struct rinterpolate_vector_t * const v,
                                   const rinterpolate_float_t * const floatlist)
{
    rinterpolate_float_t d = 0.0;
    for(rinterpolate_counter_t i=0; i<v->n; i++)
    {
        d += v->data[i] - floatlist[i];
    }
    return d;
}

void rinterpolate_print_vector(const struct rinterpolate_vector_t * const v,
                               const char * const separator)
{
    for(rinterpolate_counter_t i=0; i<v->n; i++)
    {
        printf("%g%s",
               v->data[i],
               separator);
    }
}

#endif // __HAVE_LIBRINTERPOLATE__

typedef int prevent_ISO_C_warning;

        