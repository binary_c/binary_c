#define __BINARY_C_LINT_SKIP

#include "../binary_c_code_options.h"
#include "../binary_c_error_codes.h"
#include "../binary_c_exit_macros.h"
#include "../breakpoints/breakpoints_prototypes.h"
#include "../binary_c_exit_prototypes.h"

#ifndef __HAVE_LIBRINTERPOLATE__

#include "rinterpolate.h"
#include "rinterpolate_internal.h"

#ifdef RINTERPOLATE_CACHE
void rinterpolate_store_cache(struct rinterpolate_data_t * RESTRICT const rinterpolate_data MAYBE_UNUSED,
                              struct rinterpolate_table_t * RESTRICT const table,
                              const rinterpolate_float_t * RESTRICT const x,
                              const rinterpolate_float_t * RESTRICT const r
    )
{
    /* use the next line of the cache */
    table->cache_spin_line++;

    /* avoid falling off the end of the cache */
    table->cache_spin_line =
        table->cache_spin_line % table->cache_length;

    /* insert data : NB memcpy is definitely faster than a loop */
    memcpy(Rinterpolate_cache_param(table->cache_spin_line),x,table->n_float_sizeof);
    memcpy(RINTERPOLATE_CACHE_RESULT(table->cache_spin_line),r,table->d_float_sizeof);
}
#endif // RINTERPOLATE_CACHE

#endif // __HAVE_LIBRINTERPOLATE__

typedef int prevent_ISO_C_warning;

        