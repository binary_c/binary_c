#define __BINARY_C_LINT_SKIP

#include "../binary_c_code_options.h"
#include "../binary_c_error_codes.h"
#include "../binary_c_exit_macros.h"
#include "../breakpoints/breakpoints_prototypes.h"
#include "../binary_c_exit_prototypes.h"

#ifndef __HAVE_LIBRINTERPOLATE__

#include "rinterpolate.h"
#include "rinterpolate_internal.h"

#define _read_scalar(X) nread += fread(&(X),sizeof(X),1,fp)
#define _read_array(X,NITEMS)  nread += fread((X),sizeof(*(X)),(NITEMS),fp)
#define _read_chunk(X,NBYTES)  nread += fread((X),(NBYTES),1,fp)
#define _alloc_and_read_array(X,NITEMS)                 \
    (X) = Rinterpolate_malloc(sizeof(*(X))*(NITEMS));   \
    _read_array((X),(NITEMS))

struct rinterpolate_table_t *rinterpolate_read_table_from_file(
    struct rinterpolate_data_t * RESTRICT const rinterpolate_data,
    FILE * const fp
    )
{
    /*
     * Read a table from a file, making sure it's
     * put in rinterpolate_data
     */
    size_t nread MAYBE_UNUSED = 0;
    rinterpolate_counter_t n,d,l;
    _read_scalar(n);
    _read_scalar(d);
    _read_scalar(l);
    size_t datalen;
    _read_scalar(datalen);

    rinterpolate_float_t * data;
    _alloc_and_read_array(data,datalen);

    rinterpolate_counter_t cache_length;
    _read_scalar(cache_length);

    rinterpolate_Boolean_t analyse;
    _read_scalar(analyse);

    rinterpolate_add_new_table(
        rinterpolate_data,
        data,
        n,
        d,
        l,
        cache_length,
        analyse
        );

    struct rinterpolate_table_t * const table =
        rinterpolate_table_struct(rinterpolate_data,
                                  data,
                                  NULL);

    _read_array(table->column_is_mapped,table->n);

    for(rinterpolate_counter_t i=0; i<table->n; i++)
    {
        rinterpolate_Boolean_t has_min_max;
        _read_scalar(has_min_max);
        if(has_min_max == TRUE)
        {
            table->min_max_table[i] = rinterpolate_read_table_from_file(rinterpolate_data,
                                                                        fp);
        }
        else
        {
            table->min_max_table[i] = NULL;
        }
    }
    _read_scalar(table->auto_free_data);

    size_t label_len;
    _read_scalar(label_len);
    if(label_len > 0)
    {
        _alloc_and_read_array(table->label,label_len);
    }

    if(analyse)
    {
        rinterpolate_analyse_table(rinterpolate_data,
                                   table,
                                   "rinterpolate_read_table_from_file");
    }
    return table;
}

#endif // __HAVE_LIBRINTERPOLATE__

typedef int prevent_ISO_C_warning;

        