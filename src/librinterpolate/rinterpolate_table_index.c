#define __BINARY_C_LINT_SKIP

#include "../binary_c_code_options.h"
#include "../binary_c_error_codes.h"
#include "../binary_c_exit_macros.h"
#include "../breakpoints/breakpoints_prototypes.h"
#include "../binary_c_exit_prototypes.h"

#ifndef __HAVE_LIBRINTERPOLATE__

#include "rinterpolate.h"

int rinterpolate_table_index(struct rinterpolate_data_t * rinterpolate_data,
                             const void * const d,
                             const char * label)
{
    /*
     * Search the list of rinterpolate tables for one
     * whose data == d or label matches that passed in.
     *
     * Return the index of a matching table, or -1 if not found.
     *
     * Set d or label to NULL to ignore them.
     */
    if(d != NULL || label != NULL)
    {
        for(rinterpolate_counter_t i=0;
            i<rinterpolate_data->number_of_interpolation_tables;
            i++)
        {
            struct rinterpolate_table_t * const t = rinterpolate_data->tables[i];
            if(likely(t != NULL) &&
               (
                   unlikely(d!=NULL && t->data == d)
                   ||
                   unlikely(label != NULL && strcmp(t->label,label)==0)
                   )
                )
            {
                return (int)i;
            }
        }
    }
    return -1;
}

#endif // __HAVE_LIBRINTERPOLATE__

typedef int prevent_ISO_C_warning;

        