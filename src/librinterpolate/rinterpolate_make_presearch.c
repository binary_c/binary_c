#define __BINARY_C_LINT_SKIP

#include "../binary_c_code_options.h"
#include "../binary_c_error_codes.h"
#include "../binary_c_exit_macros.h"
#include "../breakpoints/breakpoints_prototypes.h"
#include "../binary_c_exit_prototypes.h"

#ifndef __HAVE_LIBRINTERPOLATE__

#include "rinterpolate.h"
#include "rinterpolate_internal.h"


void rinterpolate_make_presearch(struct rinterpolate_data_t * RESTRICT const rinterpolate_data MAYBE_UNUSED,
                                 struct rinterpolate_table_t * RESTRICT const table)
{
    /*
     * Find the variable presearch
     */

    if(table->presearch == NULL)
    {
        /* make table for presearch data for n columns */
        table->presearch =
            Rinterpolate_calloc(table->n,sizeof(rinterpolate_float_t *));

        if(unlikely(table->presearch == NULL))
        {
            rinterpolate_error(RINTERPOLATE_CALLOC_FAILED,
                               "Calloc of table->presearch failed and returned NULL (table = %p)\n",
                               rinterpolate_data,
                               (void*)table);
        }

        table->presearch_n = table->n;
    }

    /* loop over columns */
    for(rinterpolate_counter_t j=0; j<table->n; j++)
    {
        if(table->presearch[j] == NULL)
        {
            table->presearch[j] =
                Rinterpolate_malloc(table->varcount[j]*
                                    sizeof(rinterpolate_float_t));
            if(unlikely(table->presearch[j] == NULL))
            {
                rinterpolate_error(RINTERPOLATE_MALLOC_FAILED,
                                   "Malloc of table->presearch[j=%u] failed and returned NULL (table = %p)\n",
                                   rinterpolate_data,
                                   j,
                                   (void*)table);
            }
        }
        rinterpolate_float_t * const pline = table->presearch[j];

        /* loop over lines allocating the presearch */
        const rinterpolate_counter_t step = table->line_length * table->steps[j];
        rinterpolate_float_t * p = table->data + (size_t)j;
        for(rinterpolate_counter_t i=0; i<table->varcount[j]; i++)
        {
            pline[i] = *p;
            p += step;
        }
    }
}

#endif // __HAVE_LIBRINTERPOLATE__

typedef int prevent_ISO_C_warning;

        