#!/bin/bash

CONDOR_DIR=$HOME/condor
CONDOR_RESTART_DIR=$HOME/condor_restart

rm -r $CONDOR_DIR

python3.9 $BINARY_C/src/python/ensemble.py log_dt=1 dists=2008 r=1000 binaries=False verbosity=1 num_cores=1 max_evolution_time=1000 M1spacing=logM1  save_population_objects=False  condor=1 condor_njobs=2 condor_dir=$CONDOR_DIR  condor_memory=512  condor_postpone_sbatch=0 verbosity=1 condor_initial_dir=$BINARY_C condor_restart_dir=$CONDOR_RESTART_DIR
