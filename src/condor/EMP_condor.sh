#!/bin/bash

############################################################
# Condor example test script
############################################################
# Settings
#
R=200 # resolution 
DISTS=2008+ # 2008 or Moe
BINARIES=True # True
CONDOR=1 # set to 1 to launch
NJOBS=200 # 200
LOG_DT=300 # 300
VERBOSITY=1 # verbose output?
MAXTIME=15000 # Myr
FSAMPLE=0.1 # 0.25, 0.1, 0.01 (is a lot!)
M1SPACING=const_dt # const_dt or logM1

# for testing
#R=4; Binaries=False; NJOBS=4

############################################################
# Make commands
#
CONDOR_DIR=$HOME/data/condor-EMP-rb$R
CMD="python3 $BINARY_C/src/python/EMP_ensemble-obj.py log_dt=$LOG_DT dists=$DISTS r=$R binaries=$BINARIES verbosity=$VERBOSITY num_cores=1 max_evolution_time=$MAXTIME M1spacing=$M1SPACING save_ensemble_chunks=False save_population_objects=False condor=$CONDOR condor_njobs=$NJOBS condor_dir=$CONDOR_DIR condor_memory=1024 condor_postpone_sbatch=0 save_ensemble_chunks=0 condor_batchname=binary_c-EMP initial_dir=$BINARY_C fsample=$FSAMPLE verbosity=1"
#condor_restart_dir=/user/HS204/ri0005/condor-EMP-r100-restart

############################################################
# log things
#
echo "Running this command"
echo
echo $CMD
echo
#exit
############################################################
# do stuff
#
rm -rf $CONDOR_DIR && $CMD
