#!/bin/bash

# Condor example test script

CONDOR_DIR=$HOME/condor

rm -r $CONDOR_DIR

python3 $BINARY_C/src/python/ensemble.py log_dt=1 dists=Moe r=100 binaries=False verbosity=1 num_cores=1 max_evolution_time=1000 M1spacing=logM1  save_population_objects=False  condor=1 condor_njobs=2 condor_dir=$CONDOR_DIR  condor_memory=512  condor_postpone_sbatch=0 verbosity=1 condor_batchname=binary_c initial_dir=$BINARY_C
