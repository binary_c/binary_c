#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Log planetary nebulae in the ensemble for the
 * paper with Denise Keller and Letizia Stanghellini
 */

#ifdef STELLAR_POPULATIONS_ENSEMBLE
static double Hall_PN_fading_time_minimum(const double Mc) Maybe_unused;
static double Hall_PN_fading_time_maximum(const double Mc) Maybe_unused;
static double Vassiliadis_Wood_1994_PN_fading_time(const double Mc) ;
static double PN_fading_time(struct stardata_t * const stardata,
                             const double Mc);

#include "ensemble.h"
#include "ensemble_planetary_nebulae.h"

void ensemble_planetary_nebulae(struct stardata_t * const stardata,
                                const double real_dtp Maybe_unused)
{
    const Boolean vb = FALSE;

    /*
     * Detect a new PN by either wind or common envelope
     * ejection.
     */
    Star_number PN_star_number;
    const PN_type new_PN = ensemble_detect_new_PN(stardata,
                                                  &PN_star_number,
                                                  vb);

    /*
     * Act on it
     */
    if(Is_not_zero(stardata->model.probability) &&
       new_PN != PN_NONE)
    {
        /*
         * ID the star that is forming the PN,
         * this is the star that is a giant.
         *
         * If both are giants, choose the one that has
         * the thinnest envelope, it's likely the one that
         * makes the PN.
         */
        stardata->common.t_last_PN = stardata->model.time;

        if(PN_star_number == -1)
        {
            /*
             * Error
             */
            Exit_binary_c(BINARY_C_ALGORITHM_BRANCH_FAILURE,
                          "Failed to detect red giant that led to PN, oops");
        }
        /*
        printf("PNLOG %u %d %g %g %g %g\n",
               new_PN,
               PN_star_number,
               stardata->preferences->zero_age.mass[0],
               stardata->preferences->zero_age.mass[1],
               stardata->preferences->zero_age.separation[0],
               stardata->model.probability);
        */
        /*
         * Hence the star struct
         */
        struct star_t * const star = &stardata->star[PN_star_number];
        star->in_PN = TRUE;

        /*
         * In the LMC (Z=0.008) we have:
         * R_PN = 0.36+-0.05 pc (Stanghellini 2009 and refs within)
         * v_exp_min = 10+-4 km/s (Reid and Parker 2006)
         *
         * In the Galaxy we have:
         * R_PN = 1.34+-0.30 pc (Stanghellini et al. 2008,
         * Stanghellini an dHaywood 2010 and references therein)
         * v_exp_min = 10+-4 km/s (Acker+ 1992)
         *
         * Zfit is Max(0.008,Min(0.02,Z)) so we interpolate between
         * Z=0.008 and Z=0.02, and do not go outside this range
         * otherwise.
         */
        const double Zfit = Limit_range(stardata->common.metallicity,0.008,0.02);
        const double R_PN = PARSEC * (Zfit * -81.666 + 1.9933); // cm
        const double v_exp_min = 10.0 * 1e5; // cm/s

        /*
         * Hence the dynamical timescale of the PN ejection (y)
         */
        const double t_dyn = R_PN / v_exp_min / YEAR_LENGTH_IN_SECONDS; // yr

#ifdef NUCSYN
        const double XH = star->Xenv[XH1] + star->Xenv[XH2];
        const double XHe = star->Xenv[XHe3] + star->Xenv[XHe4];
        const double XC = star->Xenv[XC12] + star->Xenv[XC13];
        const double XN = star->Xenv[XN14] + star->Xenv[XN15];
        const double XO = star->Xenv[XO16] + star->Xenv[XO17] + star->Xenv[XO18];
        const double XNe = star->Xenv[XNe20] + star->Xenv[XNe21] + star->Xenv[XNe22];
        /*
         * Abundance by number
         */
        double Nenv[ISOTOPE_ARRAY_SIZE];
        double NZAMS[ISOTOPE_ARRAY_SIZE];
        X_to_N(stardata->store->imnuc,
               1.0,
               Nenv,
               star->Xenv,
               ISOTOPE_ARRAY_SIZE);
        X_to_N(stardata->store->imnuc,
               1.0,
               NZAMS,
               stardata->preferences->zero_age.XZAMS,
               ISOTOPE_ARRAY_SIZE);
        const double H = Nenv[XH1] + Nenv[XH2];
        const double He = Nenv[XHe3] + Nenv[XHe4];
        const double C = Nenv[XC12] + Nenv[XC13];
        const double N = Nenv[XN14] + Nenv[XN15];
        const double O = Nenv[XO16] + Nenv[XO17] + Nenv[XO18];
        const double Ne = Nenv[XNe20] + Nenv[XNe21] + Nenv[XNe22];
        const double Heinit = NZAMS[XHe3] + NZAMS[XHe4];
        const double Cinit = NZAMS[XC12] + NZAMS[XC13];
        const double Ninit = NZAMS[XN14] + NZAMS[XN15];
        const double Oinit = NZAMS[XO16] + NZAMS[XO17] + NZAMS[XO18];
        const double Neinit = NZAMS[XNe20] + NZAMS[XNe21] + NZAMS[XNe22];
        const double XH_binned = Bin_data(XH,0.01);
        const double XHe_binned = Bin_data(XHe,0.01);
        const double XC_binned = Bin_data(XC,0.001);
        const double XN_binned = Bin_data(XN,0.001);
        const double XO_binned = Bin_data(XO,0.001);
        const double XNe_binned = Bin_data(XNe,0.001);

        const double Heinit_ratio_binned = Bin_data(log10(Limit_range(He/Heinit,0.0,0.3)),0.01);
        const double Cinit_ratio_binned = Bin_data(Limit_range(log10(C/Cinit),-0.5,1.5),0.1);
        const double Ninit_ratio_binned = Bin_data(Limit_range(log10(N/Ninit),-0.5,2.5),0.1);
        const double Oinit_ratio_binned = Bin_data(log10(Limit_range(O/Oinit,-0.4,0.1)),0.01);
        const double Neinit_ratio_binned = Bin_data(Limit_range(log10(Ne/Neinit),-0.3,0.6),0.025);

        const double logCH12 = Limit_range(log10(C/H) + 12.0,7.0,10.0);
        const double logNH12 = Limit_range(log10(N/H) + 12.0,6.5,10.0);
        const double logHeH12 = Limit_range(log10(He/H) + 12.0,10.7,11.5);
        const double logHeH12_binned = Bin_data(logHeH12,0.02);
        const double logCH12_binned = Bin_data(logCH12,0.02);
        const double logNH12_binned = Bin_data(logNH12,0.02);
#else
        const double XH = 0.7;
#endif//NUCSYN


        /*
         * Transition timescale
         */
        double t_tr;
        if(Wind_PN(new_PN))
        {
            /*
             * Wind-ejected PN
             */
            const double Mdot_W = wind_Reimers(star,1.0); // Msun/yr
            const double E_H = 6.8e18; // erg/g
            const double Mdot_H = L_SUN * star->luminosity / (XH * E_H) / M_SUN * YEAR_LENGTH_IN_SECONDS; // Msun/yr
            const double delta_Menv = Menv(star->starnum); // Msun

            t_tr = delta_Menv / (Mdot_H + Mdot_W); // yr
            if(vb) printf("t_tr = %g / (%g + %g) = %g [Menv = %g]\n",
                          delta_Menv,
                          Mdot_H,
                          Mdot_W,
                          t_tr,
                          delta_Menv);
        }
        else
        {
            /*
             * Common-envelope transition time
             */
            t_tr = stardata->preferences->PN_comenv_transition_time;
        }

        /*
         * Fading timescale
         */
        const double Mc = Outermost_core_mass(star);
        const double t_f = PN_fading_time(stardata,Mc);

        /*
         * Visibility timescale
         */
        const double t_PN = Min(Max(t_dyn - t_tr, 0.0), t_f);

        /*
         * Hence dtp : the contribution to the number of stars
         * = timescale * probability
         * Note: 1e-6 converts to Myr as in the rest of binary_c
         */
        const double p = stardata->model.probability;
        const double dtp = 1e-6 * t_PN * p;

        Append_logstring(LOG_PN,
                         "PN star %d type=%u %s, Mc=%g, tscls dyn=%g tr=%g f=%g -> %g",
                         PN_star_number,
                         new_PN,
                         PN_string(new_PN),
                         Mc,
                         t_dyn,
                         t_tr,
                         t_f,
                         t_PN);

        if(vb) printf("PN detected at t = %g : type %u \"%s\" in star %d  t_dyn = %g, t_tr = %g, t_f(Mc=%g) = %g : t_PN = %g, dtp = %g * %g = %g\n",
                      stardata->model.time,
                      new_PN,
                      PN_string(new_PN),
                      PN_star_number,
                      t_dyn,
                      t_tr,
                      Mc,
                      t_f,
                      t_PN,
                      t_PN,
                      stardata->model.probability,
                      dtp);


        if(Is_not_zero(dtp))
        {
            /*
             * Number count
             */
            Set_ensemble_count(
                "distributions",
                "PN",
                "channel",(int)new_PN,
                "count"
                );
            /*
             * Log lifetimes
             */
            Set_ensemble_count(
                "distributions",
                "PN",
                "channel",(int)new_PN,
                "PN lifetime distribution",
                "PN lifetime",(double)Bin_data(log10(Limit_range(t_PN,1,1e6)),0.01)
                );

            if(stardata->preferences->zero_age.multiplicity > 1)
            {
                /*
                 * Fig 8 : N_PN vs initial separation
                 */
                const double initial_separation_binned =
                    Bin_data(
                        log10(
                            Limit_range(
                                stardata->preferences->zero_age.separation[0],
                                1e-2,
                                1e10)
                            ),
                        0.5);
                Set_ensemble_count(
                    "distributions",
                    "PN",
                    "channel",(int)new_PN,
                    "initial separation",(double)initial_separation_binned
                    );

                if(Binary_PN(new_PN))
                {
                    /*
                     * Fig 9 : N_PN vs sepration at the moment of PN
                     */
                    const double separation_binned =
                        Bin_data(
                            log10(
                                Limit_range(
                                    stardata->common.orbit.separation,
                                    1e-2,
                                    1e10)
                                ),
                            0.5);
                    Set_ensemble_count(
                        "distributions",
                        "PN",
                        "channel",(int)new_PN,
                        "separation",(double)separation_binned
                        );

                    /*
                     * Fig 10 : N_PN vs orbital period (days) at the moment of PN
                     * Also generates Fig 14.
                     */
                    const double orbital_period_binned =
                        Bin_data(
                            log10(
                                Limit_range(
                                    stardata->common.orbit.period * YEAR_LENGTH_IN_DAYS,
                                    1e-2,
                                    1e10)
                                ),
                            0.1);
                    Set_ensemble_count(
                        "distributions",
                        "PN",
                        "channel",(int)new_PN,
                        "orbital period",(double)orbital_period_binned
                        );
                }
            }

            const double binned_core_mass =
                Bin_data(Outermost_core_mass(star),0.05);
#ifdef NUCSYN
            const double zero_age_mass_binned =
                Bin_data(stardata->preferences->zero_age.mass[star->starnum],0.25);
#endif//NUCSYN

            /*
             * Fig 11 : PN candidates (NOT N_PN) vs core mass
             *          (does not include PN time weighting)
             */
            Set_ensemble_rate(
                "distributions",
                "PN",
                "channel",(int)new_PN,
                "candiate core mass distribution",(double)binned_core_mass
                );

            /*
             * Figs 12,13 : N_PN vs core mass
             */
            Set_ensemble_count(
                "distributions",
                "PN",
                "channel",(int)new_PN,
                "core mass distribution",(double)binned_core_mass
                );

#ifdef NUCSYN
            /*
             * Fig 15 C/Cinit vs Mprimary,init
             */
            Set_ensemble_count(
                "distributions",
                "PN",
                "channel",(int)new_PN,
                "Minit",(double)zero_age_mass_binned,
                "log(C/Cinit)",(double)Cinit_ratio_binned
                );

            /*
             * Fig 16 N/Ninit vs Mprimary,init
             */
            Set_ensemble_count(
                "distributions",
                "PN",
                "channel",(int)new_PN,
                "Minit",(double)zero_age_mass_binned,
                "log(N/Ninit)",(double)Ninit_ratio_binned
                );

            /*
             * Fig 17 log(C/H)+12 vs log(N/H)+12
             * Fig 18 log(N/H)+12 vs log(He/H)+12
             */
            Set_ensemble_count(
                "distributions",
                "PN",
                "channel",(int)new_PN,
                "log(C/H)+12",(double) logCH12_binned,
                "log(N/H)+12",(double) logNH12_binned
                );
            Set_ensemble_count(
                "distributions",
                "PN",
                "channel",(int)new_PN,
                "log(He/H)+12",(double) logHeH12_binned,
                "log(N/H)+12",(double) logNH12_binned
                );

            /*
             * Fig A.1 / A.4
             */
            Set_ensemble_count(
                "distributions",
                "PN",
                "channel",(int)new_PN,
                "Minit",(double) zero_age_mass_binned,
                "log(He/Heinit)",(double)Heinit_ratio_binned
                );

            /*
             * Fig A.2 / A.5
             */
            Set_ensemble_count(
                "distributions",
                "PN",
                "channel",(int)new_PN,
                "Minit",(double) zero_age_mass_binned,
                "log(O/Oinit)",(double)Oinit_ratio_binned
                );

            /*
             * Fig A.3 / A.6
             */
            Set_ensemble_count(
                "distributions",
                "PN",
                "channel",(int)new_PN,
                "Minit",(double) zero_age_mass_binned,
                "log(Ne/Neinit)",(double)Neinit_ratio_binned
                );

            /*
             * Fig B.1 He vs H
             */
            Set_ensemble_count(
                "distributions",
                "PN",
                "channel",(int)new_PN,
                "XH", (double)XH_binned,
                "XHe",(double)XHe_binned
                );

            /*
             * Fig B.2 C vs H
             */
            Set_ensemble_count(
                "distributions",
                "PN",
                "channel",(int)new_PN,
                "XH", (double)XH_binned,
                "XC",(double)XC_binned
                );

            /*
             * Fig B.3 N vs H
             */
            Set_ensemble_count(
                "distributions",
                "PN",
                "channel",(int)new_PN,
                "XH", (double)XH_binned,
                "XN",(double)XN_binned
                );

            /*
             * Fig B.4 Ne vs H
             */
            Set_ensemble_count(
                "distributions",
                "PN",
                "channel",(int)new_PN,
                "XH", (double)XH_binned,
                "XNe",(double)XNe_binned
                );

            /*
             * Fig B.5 O vs H
             */
            Set_ensemble_count(
                "distributions",
                "PN",
                "channel",(int)new_PN,
                "XH", (double)XH_binned,
                "XO",(double)XO_binned
                );

            /*
             * Fig B.6 C vs He
             */
            Set_ensemble_count(
                "distributions",
                "PN",
                "channel",(int)new_PN,
                "XHe", (double)XHe_binned,
                "XC",(double)XC_binned
                );

            /*
             * Fig B.7 N vs H
             */
            Set_ensemble_count(
                "distributions",
                "PN",
                "channel",(int)new_PN,
                "XHe", (double)XHe_binned,
                "XN",(double)XN_binned
                );

            /*
             * Fig B.8 O vs He
             */
            Set_ensemble_count(
                "distributions",
                "PN",
                "channel",(int)new_PN,
                "XHe", (double)XHe_binned,
                "XO",(double)XO_binned
                );

            /*
             * Fig B.9 Ne vs He
             */
            Set_ensemble_count(
                "distributions",
                "PN",
                "channel",(int)new_PN,
                "XHe", (double)XHe_binned,
                "XNe",(double)XNe_binned
                );

            /*
             * Fig B.10 N vs C
             */
            Set_ensemble_count(
                "distributions",
                "PN",
                "channel",(int)new_PN,
                "XC", (double)XC_binned,
                "XN",(double)XN_binned
                );

            /*
             * Fig B.11 O vs C
             */
            Set_ensemble_count(
                "distributions",
                "PN",
                "channel",(int)new_PN,
                "XC", (double)XC_binned,
                "XO",(double)XO_binned
                );

            /*
             * Fig B.12 Ne vs C
             */
            Set_ensemble_count(
                "distributions",
                "PN",
                "channel",(int)new_PN,
                "XC", (double)XC_binned,
                "XNe",(double)XNe_binned
                );

            /*
             * Fig B.13 O vs N
             */
            Set_ensemble_count(
                "distributions",
                "PN",
                "channel",(int)new_PN,
                "XN", (double)XN_binned,
                "XO",(double)XO_binned
                );

            /*
             * Fig B.14 Ne vs N
             */
            Set_ensemble_count(
                "distributions",
                "PN",
                "channel",(int)new_PN,
                "XN", (double)XN_binned,
                "XNe",(double)XNe_binned
                );

            /*
             * Fig B.15 Ne vs O
             */
            Set_ensemble_count(
                "distributions",
                "PN",
                "channel",(int)new_PN,
                "XO", (double)XO_binned,
                "XNe",(double)XNe_binned
                );
#endif // NUCSYN
        }
    }
    //if(new_PN) Flexit;

}

static double PN_fading_time(struct stardata_t * const stardata,
                             const double Mc)
{
    /*
     * Hall et al. (2013) show that stars
     * with Mc<0.27 have Teff_max < 30kK so
     * do not form PNe.
     *
     * They also give, in Fig. 6, the fading time
     * at low mass, M<0.45Msun. We use this, with
     * Mc=Min(0.45,Mc), for M<0.55Msun.
     *
     * Other stars, M>0.55Msun, use a fit to
     * Vassiliadis and Wood's results (1994)
     * as shown in Keller et al. Fig. 2.
     */
    return
        Mc < 0.27 ? 1e10 :
        Mc < 0.55 ? (
            stardata->preferences->PN_Hall_fading_time_algorithm ==
            PN_HALL_FADING_TIME_ALGORITHM_MINIMUM
            ? Hall_PN_fading_time_minimum(Mc)
            : Hall_PN_fading_time_maximum(Mc) )
        : Vassiliadis_Wood_1994_PN_fading_time(Mc);
}

static double Vassiliadis_Wood_1994_PN_fading_time(const double Mc)
{
    /*
     * M>0.55Msun, use a fit to
     * Vassiliadis and Wood's results (1994)
     * as shown in Keller et al. Fig. 2.
     */
    const double mc = Min(1.0,Max(Mc,0.55));
    return exp10(2.947  + 0.239 / (0.356 + log10(mc)));
}

/*
 * Fading times from Hall et al. (2013)
 * MNRAS 435,3
 *
 * NB for Z=0.02 based on their Fig. 6
 */
static double Hall_PN_fading_time_minimum(const double Mc)
{
    const double x = Min(0.45,Max(0.27,Mc));
    return exp10((8.4889)+(-0.85001)/x+(0.60587)*cos(x*(-26.933)+(-0.57372)));
}

static double Hall_PN_fading_time_maximum(const double Mc)
{
    const double x = Min(0.45,Max(0.27,Mc));
    return exp10((8.2808)+(-0.70708)/x+(0.49438)*cos(x*(-33.94)+(-10.136)));
}


#endif // STELLAR_POPULATIONS_ENSEMBLE
