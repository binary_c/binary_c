#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef STELLAR_POPULATIONS_ENSEMBLE

#include "ensemble.h"

void ensemble_output_to_json(struct stardata_t * const stardata,
                             const char * const filename)
{
    /*
     * On the final timestep, output the ensemble to a
     * JSON buffer.
     *
     * If filename is non-NULL, output the results to
     * said file.
     *
     * Otherwise, if we're outputting to the screen, indent
     * everything to make it look good. If we're outputting to the
     * internal buffer (i.e. in memory) do not indent
     * because it's a waste of RAM and hence processing time.
     *
     * Note: if we output to the buffer, prefix with a header
     * "ENSEMBLE_JSON" so that make_population_ensemble.pl
     * recognises the data.
     *
     *
     */
    char * json_buffer = NULL;
    size_t json_size = 0;

    Dprint("output to json %d\n",
           stardata->preferences->internal_buffering);

    /*
     * Only output if the cdict and its contents
     * are non-NULL
     */
    if(stardata->model.ensemble_cdict != NULL &&
       stardata->model.ensemble_cdict->cdict_entry_list != NULL)
    {
        if(stardata->preferences->ensemble_output_lines == TRUE)
        {
            /*
             * Output JSON as lines
             */
            Printf("ENSEMBLE_START_JSON_LINES\n");
            const Boolean sort = TRUE;
            struct cdict_iterator_t * iterator = NULL;
            struct cdict_t * const cdict = stardata->model.ensemble_cdict;
            CDict_sort(stardata->model.ensemble_cdict);
            while(TRUE)
            {
                struct cdict_entry_t * const entry = CDict_iter(cdict,
                                                                &iterator,
                                                                sort);

                for(size_t i=0; i<iterator->stack->nstack; i++)
                {
                    char * valuestring;
                    struct cdict_entry_t * this = iterator->stack->items[i] ? iterator->stack->items[i]->entry : NULL;
                    cdict_entry_to_key_and_value_strings(cdict,
                                                         this,
                                                         &valuestring);
                    Printf("\"%s\"",
                           this->key.string);
                    if(!CDict_entry_is_hash(this))
                    {
                        Printf(", %s\n",
                               valuestring);
                    }
                    else
                    {
                        Printf(", ");
                    }
                    free(valuestring);
                }

                if(entry == NULL) break;
            }
            CDict_iter(NULL,&iterator,FALSE); // free memory
            Printf("ENSEMBLE_END_JSON_LINES\n");
            return;
        }

        if(filename == NULL)
        {
            if(stardata->preferences->internal_buffering == INTERNAL_BUFFERING_STORE)
            {
                /*
                 * Output compressed JSON to internal buffer
                 */
                CDict_to_JSON(stardata->model.ensemble_cdict,
                              json_buffer,
                              json_size,
                              ",",
                              ":",
                              CDICT_JSON_NO_INDENT,
                              CDICT_JSON_NO_NEWLINES,
                              CDICT_JSON_NO_SORT);
                Printf("ENSEMBLE_JSON %s\n",json_buffer);
            }
            else
            {
                /*
                 * Output pretty JSON to screen
                 */
                CDict_to_JSON(stardata->model.ensemble_cdict,
                              json_buffer,
                              json_size,
                              ",",
                              " : ",
                              CDICT_JSON_INDENT,
                              CDICT_JSON_NEWLINES,
                              CDICT_JSON_SORT);
                Printf("%s\n",json_buffer);
            }
        }
        else
        {
            /*
             * Output to file
             */
            CDict_to_JSON(stardata->model.ensemble_cdict,
                           json_buffer,
                           json_size,
                           ",",
                           " : ",
                           CDICT_JSON_INDENT,
                           CDICT_JSON_NEWLINES,
                           CDICT_JSON_SORT);
            FILE * fp = fopen(filename,"w");
            if(fp == NULL)
            {
                Exit_binary_c(BINARY_C_FILE_OPEN_ERROR,
                              "Could not open file at \"%s\" to which we'd like to write the ensemble as JSON.",
                              filename);
            }
            else
            {
                if(fputs(json_buffer,fp)==0)
                {
                    Exit_binary_c(BINARY_C_FILE_OPEN_ERROR,
                                  "Could not write JSON buffer to file at \"%s\".",
                                  filename);
                }
                fclose(fp);
            }
        }
    }

    /*
    cdict_free(&stardata->model.ensemble_cdict,
               FALSE,
               NULL);
    stardata->persistent_data->ensemble_cdict = NULL;
    stardata->model.ensemble_cdict = NULL;
    CDict_new(cdict);
    cdict_json_to(json_buffer,
                  json_size,
                  cdict);
    stardata->model.ensemble_cdict = cdict;
    */

    Safe_free(json_buffer);
}

#endif // STELLAR_POPULATIONS_ENSEMBLE
