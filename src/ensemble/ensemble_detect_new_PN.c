#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef STELLAR_POPULATIONS_ENSEMBLE

#include "ensemble.h"
#include "ensemble_planetary_nebulae.h"

/*
 * Detect a new planetary nebula, see ensemble_planetary_nebulae()
 * for details.
 */

PN_type ensemble_detect_new_PN(const struct stardata_t * const stardata,
                               Star_number * const PN_star_number,
                               const Boolean vb)
{
    Star_number i;
    PN_type new_PN = PN_NONE;
    * PN_star_number = -1;

    if(stardata->model.time - stardata->common.t_last_PN >
       stardata->preferences->minimum_time_between_PNe)
    {
        const Boolean new_comenv = stardata->model.comenv_count > stardata->previous_stardata->model.comenv_count;
        const Boolean single = System_is_single;
        const Boolean merged =
            single == TRUE &&
            stardata->preferences->zero_age.multiplicity >= 2 &&
            stardata->model.merged == TRUE;

        Evolving_Starloop(i)
        {
            /*
             * Detect wind PN.
             *
             * We call this a PN_MERGED if there was a common-envelope
             * merger prior to the wind.
             */
            if(stardata->star[i].in_PN == FALSE &&
               ON_GIANT_BRANCH(stardata->star[i].stellar_type) &&
               Menv(i) < stardata->preferences->PPN_envelope_mass)
            {
                new_PN =
                    single == TRUE ?
                    (
                        merged == TRUE ?
                        PN_MERGED :
                        PN_SINGLE_WIND
                        )
                    :
                    PN_BINARY_WIND;
                *PN_star_number = i;
            }
        }

        /*
         * Detect comenv PN -> binary
         *
         * We can detect common envelope ejection by seeing if comenv_count
         * increases and the system remains binary. We then filter
         * to choose GB or AGB stars.
         *
         * Note that we want a hot remnant, which requires a giant with a
         * thin envelope or a white dwarf remnant. We must include this
         * check to prevent, e.g., an EAGB star being stripped
         * to a HeHG or HeGB star which is cool.
         */
        if(
            stardata->preferences->zero_age.multiplicity >= 2 &&
            single == FALSE &&
            new_comenv == TRUE &&
            ON_GIANT_BRANCH(stardata->previous_stardata->star[stardata->model.comenv_overflower].stellar_type) &&
            Possibly_hot_star(stardata->star[stardata->model.comenv_overflower].stellar_type)
            )
        {
            new_PN = PN_COMENV_EJECTION;
            *PN_star_number = 0; // irrelevant
        }

        if(new_PN != PN_NONE)
        {
            if(vb == TRUE)
            {
                printf("PN %u %d (now %g, prev %g) star %d Menv %g %d : %g %g %g %g\n",
                       new_PN,
                       stardata->model.model_number,
                       stardata->model.time,
                       stardata->common.t_last_PN,
                       *PN_star_number,
                       Menv(0),
                       PN_converged_on_envelope_mass(stardata,
                                                     &stardata->star[0]),
                       stardata->preferences->zero_age.mass[0],
                       stardata->preferences->zero_age.mass[1],
                       stardata->preferences->zero_age.separation[0],
                       stardata->model.probability);
            }

            /*
              printf("t=%g single %d merged %d new_comenv %d merged %d\n",
              stardata->model.time,
              single,
              stardata->model.merged,
              new_comenv,
              merged);
            */
        }
    }
    return new_PN;
}

Boolean PN_converged_on_envelope_mass(const struct stardata_t * const stardata,
                                      const struct star_t * const star)
{
    /*
     * return TRUE if converged on PPN_envelope_mass to within 1%
     */
    return
        Float_same_within_eps(Menv(star->starnum),
                              stardata->preferences->PPN_envelope_mass,
                              0.01);
}
#endif // STELLAR_POPULATIONS_ENSEMBLE
