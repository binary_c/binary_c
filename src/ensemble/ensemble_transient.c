#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "ensemble_transient.h"

void ensemble_transient(struct stardata_t * const stardata,
                        const double T, // time
                        const double p, // probability
                        const struct star_t * const primary, // primary star after merger
                        const struct star_t * const previous_primary_star, // primary star before merger
                        const struct star_t * const previous_secondary_star, // secondary star before merger
                        const Stellar_type iprimary,
                        const Boolean system_was_binary,
                        const Boolean system_is_binary
    )
{
    const struct preferences_t * const prefs = stardata->preferences;
    if(prefs->transient_method != TRANSIENT_METHOD_NONE)
    {
        void ** datas = NULL;
        size_t ndatas = 0;

        /*
         * Set up the datas array: these are pointers to
         * structs containing data for each of a given
         * choice of transient model parameters. Because these
         * are different for each method, we set them in
         * the generic void** array of struct pointers.
         */
        if(prefs->transient_method == TRANSIENT_METHOD_IVANOVA2013)
        {
            /*
             * The Ivanova et al. (2013) models have a parameter zeta that
             * we want to vary.
             */
            const double zetas[] = { 0.1,1.0,10.0,100.0 };
            Foreach_array(const double,zeta,zetas)
            {
                datas = Realloc(datas,sizeof(void*)*(ndatas+1));
                datas[ndatas] = Malloc(sizeof(struct Ivanova2013_data_t));
                struct Ivanova2013_data_t * s = (struct Ivanova2013_data_t*)(datas[ndatas]);
                s->zeta = zeta;
                ndatas++;
            }
        }
        else if(prefs->transient_method == TRANSIENT_METHOD_MATSUMOTO2022)
        {
            /*
             * The Matsumoto (2022) method has a parameter vbar_E
             * while is in the range 0.3 to 3 * the escape velocity.
             */
            const double vesc_star = escape_velocity(primary) * 1e-5; // km/s
            const double vbar_E_factors[] = { 0.3,1.0,3.0 };
            Foreach_array(const double,vbar_E_factor,vbar_E_factors)
            {
                datas = Realloc(datas,sizeof(void*)*(ndatas+1));
                datas[ndatas] = Malloc(sizeof(struct Matsumoto2022_data_t));
                struct Matsumoto2022_data_t * const s = (struct Matsumoto2022_data_t *)(datas[ndatas]);
                s->vbar_E_factor = vbar_E_factor;
                s->vbar_E = vbar_E_factor * vesc_star; /* mean ejection velocity */
                s->R0 = primary->radius; /* ejection radius, R0 */
                s->rho_i = 1e-11; /* recombination density */
                s->f_adiabatic = 1.0; /* efficiency of recombination */
                ndatas++;
            }
        }

        const size_t maxn = ndatas==0 ? 1 : ndatas;

        static const double Porb_binwidth = 0.1;
        static const double q_binwidth = 0.05;
        static const double ecc_binwidth = 0.05;
        static const double mass_binwidth = 0.1;
        static const double logL_binwidth = 0.1;
        static const double logR_binwidth = 0.1;
        static const double logTeff_binwidth = 0.1;

        /*
         * System parameters from previous timestep
         */
        struct stardata_t * const previous_stardata = stardata->previous_stardata;
        const double Porb_binned_pre =
            system_was_binary ?
            Bin_data(Safelog10(Max(1e-6,Min(1e10,previous_stardata->common.orbit.period))),
                     Porb_binwidth) :
            -50.0;
        const double ecc_binned_pre =
            system_was_binary ?
            Bin_data(stardata->previous_stardata->common.orbit.eccentricity,
                     ecc_binwidth) :
            -1.0;
        const double q_binned_pre =
            system_is_binary ?
            Bin_data(Min(stardata->previous_stardata->star[0].q,
                         stardata->previous_stardata->star[1].q),
                     q_binwidth) :
            -1.0;
        const double Porb_binned_post Maybe_unused =
            system_is_binary ?
            Bin_data(Safelog10(Max(1e-6,Min(1e10,stardata->common.orbit.period))),
                     Porb_binwidth) :
            -50.0;
        const double ecc_binned_post Maybe_unused =
            system_is_binary ?
            Bin_data(stardata->common.orbit.eccentricity,
                     ecc_binwidth) :
            -1.0;
        const double q_binned_post Maybe_unused =
            system_is_binary ?
            Bin_data(Min(stardata->star[0].q,
                         stardata->star[1].q),
                     q_binwidth) :
            -1.0;

        for(size_t n=0; n<maxn; n++)
        {
            /*
             * Get transient properties, e.g. luminosity and timescale,
             * for each dataset.
             */
            double timescale_days;
            double luminosity_Lsun;
            transient_properties(stardata,
                                 primary,
                                 previous_primary_star,
                                 previous_secondary_star,
                                 ndatas==0 ? NULL : (datas != NULL ? datas[n] : NULL),
                                 &timescale_days,
                                 &luminosity_Lsun
                );

            /*
            printf("Plateau %d + %d -> primary mass=%g Msun, log10(L/Lsun) = %g, log(t/d) = %g y\n",
                   previous_primary_star->stellar_type,
                   previous_secondary_star->stellar_type,
                   previous_primary_star->mass,
                   log10(luminosity_Lsun),
                   log10(timescale_days));
            */

            if(luminosity_Lsun > 0.0)
            {
                /*
                 * log in the ensemble
                 */
                static const double bin_width_log_timescale = 0.1;
                static const double bin_width_log_luminosity = 0.1;
                static const double progenitor_unresolved_logluminosity_binwidth = 0.1;
                static const double progenitor_unresolved_logTeff_binwidth = 0.1;
                static const double progenitor_unresolved_logg_binwidth = 0.1;

                const double log_timescale_binned = Bin_data(log10(timescale_days),bin_width_log_timescale);
                const double log_luminosity_binned = Bin_data(log10(luminosity_Lsun),bin_width_log_luminosity);

#ifdef NUCSYN
                static const double progenitor_chemistry_binwidth = 0.1;
                Abundance * const Xsurf = nucsyn_observed_surface_abundances((struct star_t*)previous_primary_star);

                /* shortcut for binned square brackets */
#define Sq(TOP,BOTTOM)                                  \
                Bin_data(                               \
                    nucsyn_elemental_square_bracket(    \
                        TOP,                            \
                        BOTTOM,                         \
                        Xsurf,                          \
                        prefs->zero_age.Xsolar,         \
                        stardata),                      \
                    progenitor_chemistry_binwidth       \
                    )

                const double progenitor_C = Sq("C","Fe");
                const double progenitor_N = Sq("N","Fe");
                const double progenitor_O = Sq("O","Fe");
                const double progenitor_H = Bin_data(Xsurf[XH1],
                                                     progenitor_chemistry_binwidth);
                const double progenitor_He = Bin_data(Xsurf[XHe4],
                                                      progenitor_chemistry_binwidth);
#endif // NUCSYN




                /*
                 * Log if in reasonable range
                 */
                if(log_timescale_binned >= -1.0 &&
                   log_timescale_binned <= 4.0 &&
                   log_luminosity_binned >= 2.0 &&
                   log_luminosity_binned <= 9.0)
                {
                    /*
                     * Construct unresolved logg, logTeff, logL
                     */

                    /*
                     * first linear system values (luminosity-weighted)
                     */
                    const double progenitor_unresolved_luminosity =
                        previous_primary_star->luminosity + previous_secondary_star->luminosity;
                    const double progenitor_unresolved_Teff =
                        (previous_primary_star->luminosity * Teff_from_star_struct(previous_primary_star)
                         +
                         previous_secondary_star->luminosity * Teff_from_star_struct(previous_secondary_star))
                        / progenitor_unresolved_luminosity;
                    const double progenitor_unresolved_g =
                        (previous_primary_star->luminosity * g(previous_primary_star)
                         +
                         previous_secondary_star->luminosity * g(previous_secondary_star))
                        / progenitor_unresolved_luminosity;

                    /*
                     * then binned log values
                     */
                    const double progenitor_unresolved_logg_binned =
                        Bin_data(log10(progenitor_unresolved_g),
                                 progenitor_unresolved_logg_binwidth);
                    const double progenitor_unresolved_logluminosity_binned =
                        Bin_data(log10(progenitor_unresolved_luminosity),
                                 progenitor_unresolved_logluminosity_binwidth);
                    const double progenitor_unresolved_logTeff_binned =
                        Bin_data(log10(progenitor_unresolved_Teff),
                                 progenitor_unresolved_logTeff_binwidth);

                    /*
                      printf("Transient logL=%g logt=%g\n",
                            log_luminosity_binned,
                            log_timescale_binned);
                    */

                    if(prefs->transient_method == TRANSIENT_METHOD_IVANOVA2013)
                    {
                        struct Ivanova2013_data_t * s = (struct Ivanova2013_data_t*)(datas[n]);
                        if(Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_TRANSIENTS) == TRUE)
                        {
                            Log_transient("zeta",s->zeta);
                            Log_transient_chemistry("zeta",s->zeta);
                        }
                        if(Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_TRANSIENTS_BOKEH) == TRUE)
                        {
                            Log_transient_Bokeh("zeta","s->zeta");
                        }
                    }
                    else if(prefs->transient_method == TRANSIENT_METHOD_MATSUMOTO2022)
                    {
                        struct Matsumoto2022_data_t * s = (struct Matsumoto2022_data_t*)(datas[n]);
                        if(Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_TRANSIENTS) == TRUE)
                        {
                            Log_transient("vbar_E_factor",s->vbar_E_factor);
                            Log_transient_chemistry("vbar_E_factor",s->vbar_E_factor);
                        }
                        if(Use_ensemble(STELLAR_POPULATIONS_ENSEMBLE_FILTER_TRANSIENTS_BOKEH) == TRUE)
                        {
                            Log_transient_Bokeh("vbar_E_factor",s->vbar_E_factor);
                        }
                    }
                    else
                    {
                        fprintf(stderr,
                                "Transient logL=%g logt=%g out of reasoanble range",
                                log_luminosity_binned,
                                log_timescale_binned);
                    }
                }
            }
        }

        /*
         * Free data structures
         */
        for(size_t i=0;i<ndatas;i++)
        {
            Safe_free(datas[i]);
        }
        Safe_free(datas);
    }
}
