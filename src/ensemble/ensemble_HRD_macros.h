#pragma once
#ifndef ENSEMBLE_HRD_MACROS_H
#define ENSEMBLE_HRD_MACROS_H


#define Set_ensemble_X(TYPE,                                            \
                       FILTER,                                          \
                       FILTER_TIME_SLICES,                              \
                       FILTER_PERIOD_DISTRIBUTIONS,                     \
                       FILTER_TIME_SLICES_PERIOD_DISTRIBUTIONS,         \
                       FILTER_MASS_DISTRIBUTIONS,                       \
                       ...)                                             \
    if(p->ensemble_filter[FILTER] == TRUE)                              \
    {                                                                   \
        Set_ensemble_count(                                             \
            TYPE,                                                       \
            __VA_ARGS__);                                               \
    }                                                                   \
    if(p->ensemble_filter[FILTER_TIME_SLICES] == TRUE)                  \
    {                                                                   \
        Set_ensemble_count(                                             \
            TYPE "(t)",                                                 \
            "time",(double)T,                                           \
            __VA_ARGS__);                                               \
    }                                                                   \
    if(p->ensemble_filter[FILTER_PERIOD_DISTRIBUTIONS] == TRUE)         \
    {                                                                   \
        Set_ensemble_count(                                             \
            TYPE " period distribution",                                \
            __VA_ARGS__,                                                \
            "log orbital period",Porb_binned                            \
            );                                                          \
    }                                                                   \
    if(p->ensemble_filter[FILTER_TIME_SLICES_PERIOD_DISTRIBUTIONS] == TRUE) \
    {                                                                   \
        Set_ensemble_count(                                             \
            TYPE "(t) period distribution",                             \
            "time",(double)T,                                           \
            __VA_ARGS__,                                                \
            "log orbital period",Porb_binned                            \
            );                                                          \
    }                                                                   \
    if(p->ensemble_filter[FILTER_MASS_DISTRIBUTIONS] == TRUE)           \
    {                                                                   \
        Set_ensemble_count(                                             \
            TYPE " mass distribution",                                  \
            __VA_ARGS__,                                                \
            "log system mass",                                          \
            (                                                           \
                star!=NULL ?                                            \
                Is_zero(star->mass) ? -10.0 : Bin_data(log10(star->mass),mass_binwidth) : \
                Is_zero(Sum_of_stars(mass)) ? -10.0 : Bin_data(log10(Sum_of_stars(mass)),mass_binwidth) \
                )                                                       \
            );                                                          \
    }

/*
 * Output for Bokeh
 */
#define Set_ensemble_X_Bokeh(FILTER,                                    \
                             TYPE,                                      \
                             TIME,                                      \
                             ...)                                       \
                                                                        \
    if(p->ensemble_filter[FILTER] == TRUE)                              \
    {                                                                   \
        /* binary-star statistics */                                    \
                                                                        \
        /* Porb in days : if -50 (single) */                            \
        /* keep as -50 */                                               \
        const double _per =                                             \
            Fequal(Porb_binned,-50.0) ?                                 \
            -50.0 :                                                     \
            (Porb_binned + log10(YEAR_LENGTH_IN_DAYS));                 \
                                                                        \
        /* only once per system */                                      \
        Log_HRD(                                                        \
            TYPE,                                                       \
            TIME,                                                       \
            __VA_ARGS__,                                                \
            "value"                                                     \
            );                                                          \
        Log_HRD(                                                        \
            TYPE,                                                       \
            TIME,                                                       \
            __VA_ARGS__,                                                \
            "pdist",                                                    \
            _per                                                        \
            );                                                          \
        Log_HRD(                                                        \
            TYPE,                                                       \
            TIME,                                                       \
            __VA_ARGS__,                                                \
            "edist",                                                    \
            ecc_binned                                                  \
            );                                                          \
        Log_HRD(                                                        \
            TYPE,                                                       \
            TIME,                                                       \
            __VA_ARGS__,                                                \
            "P-e map",                                                  \
            /* Porb in days : if -50 (single) */                        \
            /* keep as -50 */                                           \
            _per,                                                       \
            ecc_binned                                                  \
            );                                                          \
        Log_HRD(                                                        \
            TYPE,                                                       \
            TIME,                                                       \
            __VA_ARGS__,                                                \
            "q",                                                        \
            q_binned                                                    \
            );                                                          \
                                                                        \
        Foreach_star(star)                                              \
        {                                                               \
            if(star->stellar_type != MASSLESS_REMNANT)                  \
            {                                                           \
                /* every star */                                        \
                Log_HRD(                                                \
                    TYPE,                                               \
                    TIME,                                               \
                    __VA_ARGS__,                                        \
                    star->starnum == iprimary ? "m1dist" : "m2dist",    \
                    Is_zero(star->mass) ? -10.0 : Bin_data(log10(star->mass),mass_binwidth) \
                    );                                                  \
                Log_HRD(                                                \
                    TYPE,                                               \
                    TIME,                                               \
                    __VA_ARGS__,                                        \
                    star->starnum == iprimary ? "st1dist" : "st2dist",  \
                    star->stellar_type                                  \
                    );                                                  \
                                                                        \
                if(itrigger!=-1)                                        \
                {                                                       \
                    Log_HRD(                                            \
                        TYPE,                                           \
                        TIME,                                           \
                        __VA_ARGS__,                                    \
                        star->starnum == itrigger ? "m_trigger_dist" : "m_other_dist", \
                        Is_zero(star->mass) ? -10.0 : Bin_data(log10(star->mass),mass_binwidth) \
                        );                                              \
                    Log_HRD(                                            \
                        TYPE,                                           \
                        TIME,                                           \
                        __VA_ARGS__,                                    \
                        star->starnum == itrigger ? "st_trigger_dist" : "st_other_dist", \
                        star->stellar_type                              \
                        );                                              \
                }                                                       \
                                                                        \
                if(toggles != NULL)                                     \
                {                                                       \
                    _Multi_trigger_log(                                 \
                        HRD_DATA_TOGGLE_CORE_MASS_RATIO,                \
                        TYPE,                                           \
                        TIME,                                           \
                        "Mc/M",                                         \
                        Bin_data(                                       \
                            Outermost_core_mass(star)/star->mass,       \
                            0.1                                         \
                            ),                                          \
                        __VA_ARGS__                                     \
                        );                                              \
                    _Multi_trigger_log(                                 \
                        HRD_DATA_TOGGLE_FUNDAMENTAL_FREQUENCY,          \
                        TYPE,                                           \
                        TIME,                                           \
                        "log10(ν)",                                     \
                        Bin_data(                                       \
                            log10(                                      \
                                1.0/Limit_range(                        \
                                    dynamical_timescale(star) *         \
                                    YEAR_LENGTH_IN_SECONDS,             \
                                    1,                                  \
                                    YEAR_LENGTH_IN_SECONDS*10.0)        \
                                ),                                      \
                            0.1),                                       \
                        __VA_ARGS__                                     \
                        );                                              \
                }                                                       \
            }                                                           \
        }                                                               \
    }


#define _Multi_trigger_log(TRIGGER,                             \
                           TYPE,                                \
                           TIME,                                \
                           LABEL,                               \
                           VALUE,                               \
                           ...)                                 \
    if(toggles[TRIGGER])                                        \
    {                                                           \
        const double value = (VALUE);                           \
        Log_HRD(                                                \
            TYPE,                                               \
            TIME,                                               \
            __VA_ARGS__,                                        \
            LABEL " distribution",                              \
            value                                               \
            );                                                  \
        if(system_is_binary)                                    \
        {                                                       \
            Log_HRD(                                            \
                TYPE,                                           \
                TIME,                                           \
                __VA_ARGS__,                                    \
                star->starnum == iprimary ?                     \
                "primary "LABEL" distribution" :                \
                "secondary "LABEL" distribution",               \
                value                                           \
                );                                              \
            if(star->starnum == itrigger)                       \
            {                                                   \
                Log_HRD(                                        \
                    TYPE,                                       \
                    TIME,                                       \
                    __VA_ARGS__,                                \
                    "binary triggered "LABEL" distribution",    \
                    value                                       \
                    );                                          \
            }                                                   \
        }                                                       \
        else                                                    \
        {                                                       \
            Log_HRD(                                            \
                TYPE,                                           \
                TIME,                                           \
                __VA_ARGS__,                                    \
                "single "LABEL" distribution",                  \
                value                                           \
                );                                              \
            if(star->starnum == itrigger)                       \
            {                                                   \
                Log_HRD(                                        \
                    TYPE,                                       \
                    TIME,                                       \
                    __VA_ARGS__,                                \
                    "single triggered "LABEL" distribution",    \
                    value                                       \
                    );                                          \
            }                                                   \
        }                                                       \
        if(itrigger!=-1)                                        \
        {                                                       \
            if(star->starnum==itrigger)                         \
            {                                                   \
                Log_HRD(                                        \
                    TYPE,                                       \
                    TIME,                                       \
                    __VA_ARGS__,                                \
                    "triggered "LABEL" distribution",           \
                    value                                       \
                    );                                          \
            }                                                   \
            Log_HRD(                                            \
                TYPE,                                           \
                TIME,                                           \
                __VA_ARGS__,                                    \
                star->starnum == itrigger ?                     \
                "triggered star "LABEL" distribution" :         \
                "triggered companion "LABEL" distribution",     \
                value                                           \
                );                                              \
        }                                                       \
    }


#define Log_HRD(TYPE,TIME,...)                  \
    if(prekey!=NULL)                            \
    {                                           \
        Set_ensemble_count(                     \
            TYPE,                               \
            prekey,                             \
            TIME,                               \
            "stellar type primary",             \
            primary_star->stellar_type,         \
            "stellar type secondary",           \
            secondary_star->stellar_type,       \
            __VA_ARGS__                         \
            );                                  \
        Set_ensemble_count(                     \
            TYPE,                               \
            prekey,                             \
            TIME,                               \
            "stellar type primary",             \
            "all",                              \
            "stellar type secondary",           \
            "all",                              \
            __VA_ARGS__                         \
            );                                  \
    }                                           \
    else                                        \
    {                                           \
        Set_ensemble_count(                     \
            TYPE,                               \
            TIME,                               \
            "stellar type primary",             \
            primary_star->stellar_type,         \
            "stellar type secondary",           \
            secondary_star->stellar_type,       \
            __VA_ARGS__                         \
            );                                  \
        Set_ensemble_count(                     \
            TYPE,                               \
            TIME,                               \
            "stellar type primary",             \
            "all",                              \
            "stellar type secondary",           \
            "all",                              \
            __VA_ARGS__                         \
            );                                  \
    }

#define Set_ensemble_HRD(...)                                           \
    if(prekey!=NULL)                                                    \
    {                                                                   \
        Set_ensemble_X(                                                 \
            "HRD",                                                      \
            STELLAR_POPULATIONS_ENSEMBLE_FILTER_HRD,                    \
            STELLAR_POPULATIONS_ENSEMBLE_FILTER_HRD_TIME_SLICES,        \
            STELLAR_POPULATIONS_ENSEMBLE_FILTER_HRD_PERIOD_DISTRIBUTIONS, \
            STELLAR_POPULATIONS_ENSEMBLE_FILTER_HRD_PERIOD_DISTRIBUTIONS_TIME_SLICES, \
            STELLAR_POPULATIONS_ENSEMBLE_FILTER_HRD_MASS_DISTRIBUTIONS, \
            prekey,                                                     \
            __VA_ARGS__                                                 \
            );                                                          \
    }                                                                   \
    else                                                                \
    {                                                                   \
        Set_ensemble_X(                                                 \
            "HRD",                                                      \
            STELLAR_POPULATIONS_ENSEMBLE_FILTER_HRD,                    \
            STELLAR_POPULATIONS_ENSEMBLE_FILTER_HRD_TIME_SLICES,        \
            STELLAR_POPULATIONS_ENSEMBLE_FILTER_HRD_PERIOD_DISTRIBUTIONS, \
            STELLAR_POPULATIONS_ENSEMBLE_FILTER_HRD_PERIOD_DISTRIBUTIONS_TIME_SLICES, \
            STELLAR_POPULATIONS_ENSEMBLE_FILTER_HRD_MASS_DISTRIBUTIONS, \
            __VA_ARGS__                                                 \
            );                                                          \
    }

/*
 * __VA_ARGS__ here are { "logTeff", value, "logL", value }
 */
#define Set_ensemble_HRD_Bokeh(...)                         \
    if(stardata->preferences->ensemble_alltimes == TRUE)    \
    {                                                       \
        Set_ensemble_X_Bokeh(                               \
            STELLAR_POPULATIONS_ENSEMBLE_FILTER_HRD_BOKEH,  \
            "HRD Bokeh",                                    \
            "all times",                                    \
            __VA_ARGS__                                     \
            );                                              \
    }                                                       \
    if(stardata->preferences->ensemble_notimebins == FALSE) \
    {                                                       \
        Set_ensemble_X_Bokeh(                               \
            STELLAR_POPULATIONS_ENSEMBLE_FILTER_HRD_BOKEH,  \
            "HRD Bokeh",                                    \
            T,                                              \
            __VA_ARGS__                                     \
            );                                              \
    }


#define Set_ensemble_CMD_Bokeh(...)                         \
    if(stardata->preferences->ensemble_alltimes == TRUE)    \
    {                                                       \
        Set_ensemble_X_Bokeh(                               \
            STELLAR_POPULATIONS_ENSEMBLE_FILTER_CMD_BOKEH,  \
            "CMD Bokeh",                                    \
            "all times",                                    \
            __VA_ARGS__                                     \
            );                                              \
    }                                                       \
    if(stardata->preferences->ensemble_notimebins == FALSE) \
    {                                                       \
        Set_ensemble_X_Bokeh(                               \
            STELLAR_POPULATIONS_ENSEMBLE_FILTER_CMD_BOKEH,  \
            "CMD Bokeh",                                    \
            T,                                              \
            __VA_ARGS__                                     \
            );                                              \
    }



#define Set_ensemble_CMD(...)                                           \
    Set_ensemble_X(                                                     \
        "CMD",                                                          \
        STELLAR_POPULATIONS_ENSEMBLE_FILTER_GAIA_CMD,                   \
        STELLAR_POPULATIONS_ENSEMBLE_FILTER_GAIA_CMD_TIME_SLICES,       \
        STELLAR_POPULATIONS_ENSEMBLE_FILTER_GAIA_CMD_PERIOD_DISTRIBUTIONS, \
        STELLAR_POPULATIONS_ENSEMBLE_FILTER_GAIA_CMD_PERIOD_DISTRIBUTIONS_TIME_SLICES, \
        STELLAR_POPULATIONS_ENSEMBLE_FILTER_GAIA_CMD_MASS_DISTRIBUTIONS, \
        __VA_ARGS__);


#define HRD_DATA_TOGGLES                        \
    X(       CORE_MASS_RATIO)                   \
        X( FUNDAMENTAL_FREQUENCY)

#undef X
#define X(TYPE) HRD_DATA_TOGGLE_##TYPE,
enum { HRD_DATA_TOGGLES HRD_DATA_N_TOGGLES };

#endif // ENSEMBLE_HRD_MACROS_H
