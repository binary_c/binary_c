#pragma once
#ifndef ENSEMBLE_TRANSIENT_H
#define ENSEMBLE_TRANSIENT_H

/*
 * Macros used to log transients in the ensemble
 */
#undef FILTER
#undef TYPE

#define Log_ensemble_transient_Bokeh(FILTER,TYPE,...)                   \
    {                                                                   \
        if(prefs->ensemble_filter[FILTER] == TRUE)                      \
        {                                                               \
            Foreach_star(star)                                          \
            {                                                           \
                if(star->starnum==iprimary)                             \
                {                                                       \
                    const double _per_pre = Fequal(Porb_binned_pre,-50.0) ? -50.0 : (Porb_binned_pre + log10(YEAR_LENGTH_IN_DAYS)); \
                    /* only once per system */                          \
                    Set_ensemble_rate(                                  \
                        TYPE,                                           \
                        __VA_ARGS__,                                    \
                        "value"                                         \
                        );                                              \
                    Set_ensemble_rate(                                  \
                        TYPE,                                           \
                        __VA_ARGS__,                                    \
                        "pdist pre",                                    \
                        _per_pre                                        \
                        );                                              \
                    Set_ensemble_rate(                                  \
                        TYPE,                                           \
                        __VA_ARGS__,                                    \
                        "edist pre",                                    \
                        ecc_binned_pre                                  \
                        );                                              \
                    Set_ensemble_rate(                                  \
                        TYPE,                                           \
                        __VA_ARGS__,                                    \
                        "P-e map pre",                                  \
                        _per_pre,                                       \
                        ecc_binned_pre                                  \
                        );                                              \
                    Set_ensemble_rate(                                  \
                        TYPE,                                           \
                        __VA_ARGS__,                                    \
                        "q pre",                                        \
                        q_binned_pre                                    \
                        );                                              \
                                                                        \
                    const double _per_post = Fequal(Porb_binned_post,-50.0) ? -50.0 : (Porb_binned_post + log10(YEAR_LENGTH_IN_DAYS)); \
                    Set_ensemble_rate(                                  \
                        TYPE,                                           \
                        __VA_ARGS__,                                    \
                        "pdist post",                                   \
                        _per_post                                       \
                        );                                              \
                    Set_ensemble_rate(                                  \
                        TYPE,                                           \
                        __VA_ARGS__,                                    \
                        "edist post",                                   \
                        ecc_binned_post                                 \
                        );                                              \
                    Set_ensemble_rate(                                  \
                        TYPE,                                           \
                        __VA_ARGS__,                                    \
                        "P-e map post",                                 \
                        _per_post,                                      \
                        ecc_binned_post                                 \
                        );                                              \
                    Set_ensemble_rate(                                  \
                        TYPE,                                           \
                        __VA_ARGS__,                                    \
                        "q post",                                       \
                        q_binned_post                                   \
                        );                                              \
                }                                                       \
                Set_ensemble_rate(                                      \
                    TYPE,                                               \
                    __VA_ARGS__,                                        \
                    star->starnum == iprimary ? "m1dist post" : "m2dist post", \
                    Is_zero(star->mass) ? -10.0 : Bin_data(log10(star->mass),mass_binwidth) \
                    );                                                  \
                Set_ensemble_rate(                                      \
                    TYPE,                                               \
                    __VA_ARGS__,                                        \
                    star->starnum == iprimary ? "logL1dist post" : "logL2dist post", \
                    Is_zero(star->mass) ? -10.0 : Bin_data(log10(star->luminosity),logL_binwidth) \
                    );                                                  \
                Set_ensemble_rate(                                      \
                    TYPE,                                               \
                    __VA_ARGS__,                                        \
                    star->starnum == iprimary ? "logR1dist post" : "logR2dist post", \
                    Is_zero(star->mass) ? -10.0 : Bin_data(log10(star->radius),logR_binwidth) \
                    );                                                  \
                Set_ensemble_rate(                                      \
                    TYPE,                                               \
                    __VA_ARGS__,                                        \
                    star->starnum == iprimary ? "logTeff1dist post" : "logTeff2dist post", \
                    Is_zero(star->mass) ? -10.0 : Bin_data(log10(Teff_from_star_struct(star)),logTeff_binwidth) \
                    );                                                  \
                Set_ensemble_rate(                                      \
                    TYPE,                                               \
                    __VA_ARGS__,                                        \
                    star->starnum == iprimary ? "st1dist post" : "st2dist post", \
                    star->stellar_type                                  \
                    );                                                  \
                /* repeat for pre */                                    \
                struct star_t * const prestar = &stardata->previous_stardata->star[star->starnum]; \
                Set_ensemble_rate(                                      \
                    TYPE,                                               \
                    __VA_ARGS__,                                        \
                    star->starnum == iprimary ? "m1dist pre" : "m2dist pre", \
                    Is_zero(prestar->mass) ? -10.0 : Bin_data(log10(prestar->mass),mass_binwidth) \
                    );                                                  \
                Set_ensemble_rate(                                      \
                    TYPE,                                               \
                    __VA_ARGS__,                                        \
                    star->starnum == iprimary ? "logL1dist pre" : "logL2dist pre", \
                    Is_zero(prestar->mass) ? -10.0 : Bin_data(log10(prestar->luminosity),logL_binwidth) \
                    );                                                  \
                Set_ensemble_rate(                                      \
                    TYPE,                                               \
                    __VA_ARGS__,                                        \
                    star->starnum == iprimary ? "logR1dist pre" : "logR2dist pre", \
                    Is_zero(prestar->mass) ? -10.0 : Bin_data(log10(prestar->radius),logR_binwidth) \
                    );                                                  \
                Set_ensemble_rate(                                      \
                    TYPE,                                               \
                    __VA_ARGS__,                                        \
                    star->starnum == iprimary ? "logTeff1dist pre" : "logTeff2dist pre", \
                    Is_zero(prestar->mass) ? -10.0 : Bin_data(log10(Teff_from_star_struct(prestar)),logTeff_binwidth) \
                    );                                                  \
                Set_ensemble_rate(                                      \
                    TYPE,                                               \
                    __VA_ARGS__,                                        \
                    star->starnum == iprimary ? "st1dist pre" : "st2dist pre", \
                    prestar->stellar_type                               \
                    );                                                  \
            }                                                           \
        }                                                               \
    }




#define Log_transient(...)                          \
    Set_ensemble_rate(                              \
        "transients",                               \
        T                                           \
        Comma_arglist(__VA_ARGS__),                 \
        "stellar type primary",                     \
        previous_primary_star->stellar_type,        \
        "stellar type secondary",                   \
        previous_secondary_star->stellar_type,      \
        "log timescale / d",                        \
        log_timescale_binned,                       \
        "log luminosity / Lsun",                    \
        log_luminosity_binned                       \
        );                                          \
                                                    \
    Set_ensemble_rate(                              \
        "transients",                               \
        "all times"                                 \
        Comma_arglist(__VA_ARGS__),                 \
        "stellar type primary",                     \
        previous_primary_star->stellar_type,        \
        "stellar type secondary",                   \
        previous_secondary_star->stellar_type,      \
        "log timescale / d",                        \
        log_timescale_binned,                       \
        "log luminosity / Lsun",                    \
        log_luminosity_binned                       \
        );                                          \
                                                    \
    Set_ensemble_rate(                              \
        "transients",                               \
        T                                           \
        Comma_arglist(__VA_ARGS__),                 \
        "stellar type primary",                     \
        "all",                                      \
        "stellar type secondary",                   \
        "all",                                      \
        "log timescale / d",                        \
        log_timescale_binned,                       \
        "log luminosity / Lsun",                    \
        log_luminosity_binned                       \
        );                                          \
                                                    \
    Set_ensemble_rate(                              \
        "transients",                               \
        "all times"                                 \
        Comma_arglist(__VA_ARGS__),                 \
        "stellar type primary",                     \
        "all",                                      \
        "stellar type secondary",                   \
        "all",                                      \
        "log timescale / d",                        \
        log_timescale_binned,                       \
        "log luminosity / Lsun",                    \
        log_luminosity_binned                       \
        );                                          \
                                                    \
    /* Nadia's hypercube */                         \
    Set_ensemble_rate(                              \
        "transients hypercube",                     \
        "timescale - luminosity"                    \
        Comma_arglist(__VA_ARGS__),                 \
        T,                                          \
        progenitor_unresolved_logluminosity_binned, \
        progenitor_unresolved_logTeff_binned,       \
        progenitor_unresolved_logg_binned,          \
        log_timescale_binned,                       \
        log_luminosity_binned);                     \
                                                    \
    Set_ensemble_rate(                              \
        "transients hypercube",                     \
        "timescale - luminosity"                    \
        Comma_arglist(__VA_ARGS__),                 \
        "all",                                      \
        progenitor_unresolved_logluminosity_binned, \
        progenitor_unresolved_logTeff_binned,       \
        progenitor_unresolved_logg_binned,          \
        log_timescale_binned,                       \
        log_luminosity_binned);                     \


#define Log_transient_Bokeh(...)                                \
    Log_ensemble_transient_Bokeh(                               \
        STELLAR_POPULATIONS_ENSEMBLE_FILTER_TRANSIENTS_BOKEH,   \
        "transients Bokeh",                                     \
        T                                                       \
        Comma_arglist(__VA_ARGS__),                             \
        "stellar type primary",                                 \
        previous_primary_star->stellar_type,                    \
        "stellar type secondary",                               \
        previous_secondary_star->stellar_type,                  \
        "log timescale / d",                                    \
        log_timescale_binned,                                   \
        "log luminosity / Lsun",                                \
        log_luminosity_binned                                   \
        );                                                      \
                                                                \
    Log_ensemble_transient_Bokeh(                               \
        STELLAR_POPULATIONS_ENSEMBLE_FILTER_TRANSIENTS_BOKEH,   \
        "transients Bokeh",                                     \
        "all times"                                             \
        Comma_arglist(__VA_ARGS__),                             \
        "stellar type primary",                                 \
        previous_primary_star->stellar_type,                    \
        "stellar type secondary",                               \
        previous_secondary_star->stellar_type,                  \
        "log timescale / d",                                    \
        log_timescale_binned,                                   \
        "log luminosity / Lsun",                                \
        log_luminosity_binned                                   \
        );                                                      \
                                                                \
    Log_ensemble_transient_Bokeh(                               \
        STELLAR_POPULATIONS_ENSEMBLE_FILTER_TRANSIENTS_BOKEH,   \
        "transients Bokeh",                                     \
        T                                                       \
        Comma_arglist(__VA_ARGS__),                             \
        "stellar type primary",                                 \
        "all",                                                  \
        "stellar type secondary",                               \
        "all",                                                  \
        "log timescale / d",                                    \
        log_timescale_binned,                                   \
        "log luminosity / Lsun",                                \
        log_luminosity_binned                                   \
        );                                                      \
                                                                \
    Log_ensemble_transient_Bokeh(                               \
        STELLAR_POPULATIONS_ENSEMBLE_FILTER_TRANSIENTS_BOKEH,   \
        "transients Bokeh",                                     \
        "all times"                                             \
        Comma_arglist(__VA_ARGS__),                             \
        "stellar type primary",                                 \
        "all",                                                  \
        "stellar type secondary",                               \
        "all",                                                  \
        "log timescale / d",                                    \
        log_timescale_binned,                                   \
        "log luminosity / Lsun",                                \
        log_luminosity_binned                                   \
        );                                                      \
                                                                \
    /* Nadia's hypercube */                                     \
    Log_ensemble_transient_Bokeh(                               \
        STELLAR_POPULATIONS_ENSEMBLE_FILTER_TRANSIENTS_BOKEH,   \
        "transients Bokeh hypercube",                           \
        "timescale - luminosity"                                \
        Comma_arglist(__VA_ARGS__),                             \
        T,                                                      \
        progenitor_unresolved_logluminosity_binned,             \
        progenitor_unresolved_logTeff_binned,                   \
        progenitor_unresolved_logg_binned,                      \
        log_timescale_binned,                                   \
        log_luminosity_binned);                                 \
                                                                \
    Log_ensemble_transient_Bokeh(                               \
        STELLAR_POPULATIONS_ENSEMBLE_FILTER_TRANSIENTS_BOKEH,   \
        "transients Bokeh hypercube",                           \
        "timescale - luminosity"                                \
        Comma_arglist(__VA_ARGS__),                             \
        "all",                                                  \
        progenitor_unresolved_logluminosity_binned,             \
        progenitor_unresolved_logTeff_binned,                   \
        progenitor_unresolved_logg_binned,                      \
        log_timescale_binned,                                   \
        log_luminosity_binned);

#ifdef NUCSYN
#define Log_transient_chemistry(...)                \
    Set_ensemble_rate(                              \
        "transients hypercube",                     \
        "CNO from HRD"                              \
        Comma_arglist(__VA_ARGS__),                 \
        "all",                                      \
        progenitor_unresolved_logluminosity_binned, \
        progenitor_unresolved_logTeff_binned,       \
        progenitor_unresolved_logg_binned,          \
        progenitor_C,                               \
        progenitor_N,                               \
        progenitor_O,                               \
        log_timescale_binned,                       \
        log_luminosity_binned);                     \
                                                    \
                                                    \
    Set_ensemble_rate(                              \
        "transients hypercube",                     \
        "H+He from HRD"                             \
        Comma_arglist(__VA_ARGS__),                 \
        "all",                                      \
        progenitor_unresolved_logluminosity_binned, \
        progenitor_unresolved_logTeff_binned,       \
        progenitor_unresolved_logg_binned,          \
        progenitor_H,                               \
        progenitor_He,                              \
        log_timescale_binned,                       \
        log_luminosity_binned);

#else
#define Log_transient_chemistry(...)
#endif // NUCYSN

#endif // ENSEMBLE_TRANSIENT_H
