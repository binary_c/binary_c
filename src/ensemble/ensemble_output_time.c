
#include "../binary_c.h"
No_empty_translation_unit_warning;



/*
 * Round and truncate the current time, which is
 * the time of output to the ensemble, to give
 * a time suitable for output that does not 
 * suffer from slight floating-point differences. 
 * Hopefully.
 */

#define __DELTA 0.1
#define SHORT_ENSEMBLE_TIME 1e-6

Time ensemble_output_time(const struct stardata_t * const stardata)
{
    const Time T = fabs(stardata->model.fixed_timesteps[FIXED_TIMESTEP_ENSEMBLE].next) < SHORT_ENSEMBLE_TIME ?
        0.0 : stardata->model.fixed_timesteps[FIXED_TIMESTEP_ENSEMBLE].next;
    const double sign = copysign(1.0,T);
    const long int n =
        stardata->preferences->ensemble_logtimes==TRUE ?
        ((T + sign * __DELTA * stardata->preferences->ensemble_logdt) / stardata->preferences->ensemble_logdt) :
        ((T + sign * __DELTA * stardata->preferences->ensemble_dt) / stardata->preferences->ensemble_dt + __DELTA );
    const Time t = (double) (n * Ensemble_timestep);

    return (Time)limit_sigfigs(t,
                               ENSEMBLE_SIGNIFICANT_DIGITS);
}
