#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef STELLAR_POPULATIONS_ENSEMBLE

int ensemble_cdict_error_handler(void * s,
                                 const int error_number,
                                 const char * const format,
                                 va_list args)
{
    struct stardata_t * const stardata = (struct stardata_t * const) s;
    vExit_binary_c(BINARY_C_CDICT_ERROR,
                   format,
                   args,
                   "Ensemble cdict error %d",
                   error_number
        );
}

#endif // STELLAR_POPULATIONS_ENSEMBLE
