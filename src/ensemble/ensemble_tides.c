#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Log periods and eccentricities
 */

#ifdef STELLAR_POPULATIONS_ENSEMBLE

#include "ensemble.h"

void ensemble_tides(struct stardata_t * const stardata,
                    const double dtp,
                    const double T)
{
    const Boolean vb Maybe_unused = FALSE;

    const double Porb_binwidth = 0.1;
    const double Porb_binned =
        Bin_data(Safelog10(stardata->common.orbit.period),
                 Porb_binwidth);

    const double e_binwidth = 0.02;
    const double e_binned =
        Bin_data(stardata->common.orbit.eccentricity,
                 e_binwidth);

    /*
    const double a_binwidth = 0.1;
    const double a_binned =
        Bin_data(Safelog10(stardata->common.orbit.separation),
                 a_binwidth);

    const double q_binwidth = 0.02;
    const double q_binned =
        Bin_data(least_massive->mass / most_massive->mass,
                 q_binwidth);
    */

    if(stardata->star[0].stellar_type <= MAIN_SEQUENCE &&
       stardata->star[1].stellar_type <= MAIN_SEQUENCE)
    {
        /*
         * Period-eccentricity diagram of MSMS binaries
         *
         * There are two in the ensemble:
         * with a constant SFR
         * time slices
         */
        Set_ensemble_count(
            "distributions",
            "P vs e MSMS binaries const SFR",
            "log orbital period",(double)Limit_range(Porb_binned,-3.0,10.0),
            "eccentricity",e_binned);
        Set_ensemble_count(
            "distributions",
            "P vs e(t) MSMS binaries",
            "age", (double)T,
            "log orbital period",(double)Limit_range(Porb_binned,-3.0,10.0),
            "eccentricity",e_binned);

        /* const double m1 = Bin_data(stardata->star[0].mass,0.2);
         * const double m2 = Bin_data(stardata->star[1].mass,0.2);
         * Set_ensemble_count(
         *   "distributions",
         *   "P vs e as f(M) MSMS binaries const SFR",
         *   "primary_mass",(double)m1,
         *   "secondary_mass",(double)m2,
         *   "log orbital period",(double)Limit_range(Porb_binned,-3.0,10.0),
         *   "eccentricity",e_binned);
         */

        const Boolean radiative1 = stardata->star[0].mass > 1.2 ? TRUE : FALSE;
        const Boolean radiative2 = stardata->star[1].mass > 1.2 ? TRUE : FALSE;
        Set_ensemble_count(
            "distributions",
            "P vs e as f(M) MSMS radiative",
            "radiative primary",(Boolean)radiative1,
            "radiative secondary",(Boolean)radiative2,
            "log orbital period",(double)Limit_range(Porb_binned,-3.0,10.0),
            "eccentricity",e_binned);
    
        /*
         * const double omega1 = Bin_data(stardata->star[0].omega,10.);
         * const double omega2 = Bin_data(stardata->star[1].omega,10.);
         * Set_ensemble_count(
         *    "distributions",
         *    "Rotation as a function of P",
         *    "primary_omega",(double)omega1,
         *    "secondary_omega",(double)omega2,
         *    "log orbital period",(double)Limit_range(Porb_binned,-3.0,10.0),
         *    "eccentricity",e_binned);
         */


    }

    Star_number kk;
    Starloop(kk)
    {
        /*
         * rotational frequency / pseudo sync frequency
         * as a function of stellar type
         */
        if(Is_not_zero(stardata->common.orbit.pseudo_sync_angular_frequency))
        {
            const double ratio_to_sync =
                Bin_data(Limit_range(
                             log10(stardata->star[kk].omega / stardata->common.orbit.pseudo_sync_angular_frequency),
                             -3.0,
                             3.0
                             ),
                         0.1);
            const double ratio_to_crit = Bin_data(stardata->star[kk].omega / stardata->star[kk].omega_crit_sph,0.02);
            const double omega_ens = Bin_data(stardata->star[kk].omega,100.);

           /* Set_ensemble_count(
            *    "distributions",
            *    "rotational frequency / pseudo sync frequency",
            *    "age", (double)T,
            *    "stellar type",stardata->star[kk].stellar_type,
            *    "ratio to sync",ratio_to_sync
            *    );
            */

            Set_ensemble_count(
                "distributions",
                "rot to sync ratio vs P",
                "age", (double)T,
                "log orbital period",(double)Limit_range(Porb_binned,-3.0,10.0),
                "ratio to sync",ratio_to_sync
                );

            Set_ensemble_count(
                "distributions",
                "rot to crit ratio vs P",
                "age", (double)T,
                "log orbital period",(double)Limit_range(Porb_binned,-3.0,10.0),
                "ratio to crit",ratio_to_crit
                );

            Set_ensemble_count(
                "distributions",
                "rotation velocity vs P",
                "age", (double)T,
                "log orbital period",(double)Limit_range(Porb_binned,-3.0,10.0),
                "omega", omega_ens
                );


        }
    }
}
#endif // STELLAR_POPULATIONS_ENSEMBLE
