#pragma once
#ifndef TRANSIENTS_MACROS_H
#define TRANSIENTS_MACROS_H

/*
 * List of algorithms
 */
#include "transients_methods.def"
#undef X
#define X(METHOD) TRANSIENT_METHOD_ ## METHOD,
enum {
    TRANSIENT_METHODS_LIST
    TRANSIENT_NUMBER_OF_METHODS
};
#undef X

#endif // TRANSIENTS_MACROS_H
