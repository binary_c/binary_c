#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef BINARY_C_API
/*
 * Wrapper for generic_event_handler
 */
binary_c_Event_handler_function
binary_c_API_function
binary_c_generic_event_handler(
    void * const eventp Maybe_unused,
    struct stardata_t * const stardata,
    void * data Maybe_unused)
{
    return generic_event_handler(eventp,stardata,data);
}
#endif // BINARY_C_API
