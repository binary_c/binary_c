#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef BINARY_C_API

/*
 * wrapper for catch_events so this can be
 * called by external codes
 */
binary_c_API_function
void binary_c_catch_events(struct stardata_t * stardata)
{
    catch_events(stardata);
}


#endif // BINARY_C_API
