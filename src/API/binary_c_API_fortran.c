#include "../binary_c.h"
No_empty_translation_unit_warning;


#if defined BINARY_C_API && defined BINARY_C_API_FORTRAN

/*
 * binary_c/FORTRAN API interface functions
 *
 * Remember: variables must be passed by references
 * (i.e. as pointers).
 *
 * See apitest.f90 for an expmple of how to use these functions.
 *
 * See also
 * http://www-h.eng.cam.ac.uk/help/tpl/languages/mixinglanguages.html
 */

/* list of variables used in the F<>C interface */

/************************************************************/
/*
 * function prototypes : these are the functions
 * called by FORTRAN code, without the trailing underscore.
 */
/************************************************************/

void binary_c_API_function binary_c_fortran_api_evolve_for_dt_(double * dt,
                                                               struct stardata_t ** s)
{
    /* call the API routine to evolve the system for time dt (Myr) */
    struct stardata_t * stardata Maybe_unused = *s;
    APIDebug("fortran: Evolve stardata at %p (time=%g) for time dt=%g\n",
             (void*)(*s),
             (*s)->model.time,*dt);
    binary_c_evolve_for_dt(*s,*dt);
}

void binary_c_API_function binary_c_fortran_api_new_system_(struct stardata_t ** s,
                                                            struct stardata_t **** ps_p,
                                                            struct preferences_t *** p_p,
                                                            struct store_t ** store,
                                                            struct persistent_data_t ** persistent,
                                                            char * argv)
{
    /* use API routine to allocate and set up a new system */
    struct stardata_t *** ps = *ps_p;
    struct preferences_t ** p = *p_p;
    struct stardata_t * stardata = *s; // for debugging only
    APIDebug("MAKE NEW SYSTEM with s = %p, ps = %p, p = %p, store = %p, argv = %p\n",
             (void*)s,
             (void*)ps,
             (void*)p,
             (void*)store,
             (void*)&argv);
    APIDebug("ARGV = %s\n",argv);
    binary_c_new_system(s,ps,p,store,persistent,&argv,-1);
    APIDebug("fortran: Allocated stardata at %p (from s=%p) with preferences at %p\n",
             (void*)(*s),
             (void*)s,
             (void*)(*s)->preferences);
}

void binary_c_API_function  binary_c_fortran_api_free_memory_(
    struct stardata_t **s,
    Boolean free_preferences,
    Boolean free_stardata_struct,
    Boolean free_store,
    Boolean free_raw_buffer,
    Boolean free_persist)
{
    /* free binary_c memory : this is the preferences and the stardata */
    struct stardata_t * stardata = *s;
    APIDebug("fortran: calling free_memory stardata=%p (from s=%p) with preferences at %p\n",
             (void*)*s,
             (void*)s,
             (void*)(*s)->preferences);
    binary_c_free_memory(s,
                         free_preferences,
                         free_stardata_struct,
                         free_store,
                         free_raw_buffer,
                         free_persist);
}

void binary_c_API_function binary_c_fortran_api_free_store_contents_(
    struct store_t **store)
{
    binary_c_free_store_contents(*store);
}

void binary_c_API_function binary_c_fortran_api_buffer_info_(
    struct stardata_t ** s,
    char ** const buffer,
    size_t * const size)
{
    binary_c_buffer_info(*s,buffer,size);
}

binary_c_API_function
void binary_c_fortran_api_buffer_size_(
    struct stardata_t ** s,
    int * size
    )
{
    char * buffer;
    size_t size_t_size;
    binary_c_buffer_info(*s,&buffer,&size_t_size);
    *size = (int)size_t_size;
}

binary_c_API_function
char * binary_c_fortran_api_buffer_buffer_(
    struct stardata_t ** s
    )
{
    char * buffer;
    size_t size;
    binary_c_buffer_info(*s,&buffer,&size);
    return buffer;
}

void binary_c_API_function binary_c_fortran_api_buffer_empty_buffer_(struct stardata_t ** s)
{
    binary_c_buffer_empty_buffer(*s);
}

void binary_c_API_function binary_c_fortran_api_stardata_info_(FORTRAN_OUT_VARLIST__,struct stardata_t **s)
{
    /* return information to a fortran variable list */
    struct stardata_t * stardata = *s;

    /* star 1 structure */
    *m1 = stardata->star[0].mass;
    *mc1 = stardata->star[0].core_mass[CORE_He];
    *stellar_type1 = stardata->star[0].stellar_type;
    *r1 = stardata->star[0].radius;
    *omega1 = stardata->star[0].omega;
    *logg1 = logg(&(stardata->star[0]));
    *lum1 = stardata->star[0].luminosity;
    *teff1 = Teff(0);

    /* star 2 structure */
    *m2 = stardata->star[1].mass;
    *mc2 = stardata->star[1].core_mass[CORE_He];
    *stellar_type2 = stardata->star[1].stellar_type;
    *r2 = stardata->star[1].radius;
    *omega2 = stardata->star[1].omega;
    *logg2 = logg(&(stardata->star[1]));
    *lum2 = stardata->star[1].luminosity;
    *teff2 = Teff(1);

#ifdef NUCSYN
    /* star 1 abundances */
    const Abundance * X = nucsyn_observed_surface_abundances(&(stardata->star[0]));
    *xh_1 = X[XH1];
    *xhe_1 = X[XHe3] + X[XHe4];
    *xc_1 = X[XC12] + X[XC13];
    *xn_1 = X[XN14] + X[XN15];
    *xo_1 = X[XO16] + X[XO17] + X[XO18];
    *xfe_1 = X[XFe56];

    /* star 2 abundances */
    X = nucsyn_observed_surface_abundances(&(stardata->star[1]));
    *xh_2 = X[XH1];
    *xhe_2 = X[XHe3] + X[XHe4];
    *xc_2 = X[XC12] + X[XC13];
    *xn_2 = X[XN14] + X[XN15];
    *xo_2 = X[XO16] + X[XO17] + X[XO18];
    *xfe_2 = X[XFe56];

#else
    *xh_1 = *xhe_1 = *xc_1 = *xn_1 = *xo_1 = *xfe_1 = 0.0;
    *xh_2 = *xhe_2 = *xc_2 = *xn_2 = *xo_2 = *xfe_2 = 0.0;
#endif

    /* system information */
    *per = stardata->common.orbit.period;
    *ecc = stardata->common.orbit.eccentricity;

    *time = stardata->model.time;
    *maxtime = stardata->model.max_evolution_time;
}

#endif // BINARY_C_API && BINARY_C_API_FORTRAN
