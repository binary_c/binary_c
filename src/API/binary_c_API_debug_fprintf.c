#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BINARY_C_API

/* API debug function */
void binary_c_API_function Gnu_format_args(4,5)
    binary_c_API_debug_fprintf(struct stardata_t * const stardata,
                               const char * const filename ,
                               const int fileline,
                               const char * const format,
                               ...)
{
    /*
     * This function should not be called directly!
     * Use the Dprint macro instead.
     *
     * filename is the source code file with the Dprint statement
     *
     * fileline is the line at which the Dprint statement occurs
     *
     * format is the (printf) format string
     *
     * ... is the variable argument list
     *
     * Output format:
     *
     * Model_Time Filename : Line : message
     *
     */
    const double t = stardata!=NULL ? stardata->model.time : -1.0;
    va_list args;
    va_start(args,format);

    /* s contains the message */
    char s[MAX_DEBUG_PRINT_SIZE],sid[14],f[MAX_DEBUG_PRINT_SIZE];
    vsnprintf(s,MAX_DEBUG_PRINT_SIZE,format,args);
    chomp(s);

#ifdef BINARY_C_API
    if(stardata!=NULL && stardata->model.id_number!=-1)
    {
        snprintf(sid,14,"(star %d) ",stardata->model.id_number);
    }
    else
#endif // BINARY_C_API
    {
        sid[0]=0; // empty string
    }

    /* make the filename, remove nan e.g. in remnant (replace with n_n) */

#ifdef DEBUG_COLOURS
    if(stardata && stardata->store)
    {
        snprintf(f,MAX_DEBUG_PRINT_SIZE,"%s%-40.40s",stardata->store->colours[RED],filename);
    }
    else
    {
        snprintf(f,MAX_DEBUG_PRINT_SIZE,"%-40.40s",filename);
    }
#else
    snprintf(f,MAX_DEBUG_PRINT_SIZE,"%-40.40s",filename);
#endif

#ifdef DEBUG_REMOVE_NAN_FROM_FILENAMES
    char *x=strstr(f,"nan");
    if(x!=NULL) *(x)='_';
#endif

    /* output to the appropriate stream and flush */
#ifdef DEBUG_COLOURS
#ifdef DEBUG_LINENUMBERS
    _printf("%s%s%g %d %s%s :%s% 6d%s : %s\n",
            stardata->store->colours[CYAN],sid,t,
        stardata != NULL ? stardata->model.intpol : 0,
            stardata->store->colours[YELLOW],f,
            stardata->store->colours[GREEN],fileline,
            stardata->store->colours[COLOUR_RESET],s);
#else
    _printf("%s%s%g %d %s%s%s : %s\n",
            stardata->store->colours[CYAN],sid,t,
        stardata != NULL ? stardata->model.intpol : 0,
            stardata->store->colours[YELLOW],f,
            stardata->store->colours[COLOUR_RESET],s);
#endif //DEBUG_LINENUMBERS
#else

#ifdef DEBUG_LINENUMBERS
    _printf("%g %s :% 6d : %s\n",t,f,fileline,s);
#else
    _printf("%g %s : %s\n",t,f,s);
#endif //DEBUG_LINENUMBERS
#endif //DEBUG_COLOURS

    fflush(stdout);

#ifdef FILE_LOG
    // flush log if it exists
    if(stardata!=NULL &&
       stardata->model.log_fp!=NULL)
        fflush(stardata->model.log_fp);
#endif //FILE_LOG

    va_end(args);
}

#endif
