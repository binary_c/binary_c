#ifndef BINARY_C_API_EXCLUDED_FEATURES_H
#define BINARY_C_API_EXCLUDED_FEATURES_H
#include "../binary_c_code_options.h"
#include "../binary_c_parameters.h"

#ifdef BINARY_C_API

/* 
 * some code features are not compatible with the API,
 * disable them here
 */

#undef NO_IMMEDIATE_MERGERS
#undef SUPERNOVA_COMPANION_LOG
#undef SUPERNOVA_COMPANION_LOG2

/* you probably don't want to use the batchmode if you're using the API */
//#undef BATCHMODE

/* other features you will not want */
//#undef RANDOM_SYSTEMS
//#undef FILE_LOG
#undef RANDOM_SEED_LOG

#endif //BINARY_C_API

#endif // BINARY_C_API_EXCLUDED_FEATURES_H
