#include "../binary_c.h"
No_empty_translation_unit_warning;

#if defined BINARY_C_API && \
    defined STELLAR_POPULATIONS_ENSEMBLE
void binary_c_API_function
binary_c_output_to_json(struct stardata_t * const stardata)
{
    ensemble_output_to_json(stardata,NULL);
}

#endif // BINARY_C_API && STELLAR_POPULATIONS_ENSEMBLE
