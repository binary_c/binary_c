#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef BINARY_C_API

/*
 * wrapper for events_replace_handler_functions so this can be
 * called by external codes
 */
binary_c_API_function
void  binary_c_events_replace_handler_functions(
    struct stardata_t * const stardata,
    const Event_type type,
    Event_handler_function func(EVENT_HANDLER_ARGS))
{
    events_replace_handler_functions(stardata,
                                     type,
                                     func);
}


#endif // BINARY_C_API
