#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BINARY_C_API

void binary_c_API_function binary_c_error_buffer(struct stardata_t * Restrict const stardata,
                                                 char ** const error_buffer)
{
    /*
     * API function to access the binary_c error_buffer
     *
     * If stardata->tmpstore->error_buffer is set, 
     * set error_buffer to it, otherwise NULL.
     */
    if(stardata != NULL &&
       stardata->tmpstore != NULL &&
       stardata->tmpstore->error_buffer_set == TRUE)
    {
        buffer_info(stardata,NULL,NULL,error_buffer);
    }
    else
    {
        *error_buffer = NULL;
    }
}
#endif
