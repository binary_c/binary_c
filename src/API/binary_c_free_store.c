#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef BINARY_C_API

/*
 * Wrapper function to free the store's contents and free
 * the store.
 */
void binary_c_API_function binary_c_free_store(struct store_t ** const store_p)
{
    free_store(store_p);
}

#endif
