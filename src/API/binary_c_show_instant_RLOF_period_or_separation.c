#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef BINARY_C_API

/*
 * This function is deprecated:
 * you should use binary_c_show_instant_RLOF
 * (which does the same thing and more)
 */

Deprecated_function void binary_c_API_function binary_c_show_instant_RLOF_period_or_separation(struct stardata_t * const stardata)
{
    show_instant_RLOF(stardata);
}

#endif
