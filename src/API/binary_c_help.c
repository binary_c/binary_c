#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef BINARY_C_API

void binary_c_API_function binary_c_help(struct stardata_t * Restrict const stardata,
                                         char * argstring)
{
    /*
     * API function to put help associated with the argument in argstring
     * into buffer.
     */
    if(stardata != NULL)
    {
        int c = 0;
        const int argc = 2;
        char ** argv = Malloc(argc * sizeof(char *));

        if(argv != NULL)
        {
            if(asprintf(&argv[0],"binary_c") >= 0)
            {
                argv[1] = argstring;

                struct store_t * store;
                if(stardata->store == NULL)
                {
                    /* make new tmpstore */
                    store = New_store;
                    build_store_contents(stardata,store);
                    stardata->store = store;
                }
                else
                {
                    /* use existing store */
                    store = stardata->store;
                }
                binary_c_help_from_arg(
                    store->argstore->cmd_line_args,
                    stardata,
                    &c,
                    0, // dummy var
                    store->argstore->arg_count, // arg_count
                    argv,
                    -argc // argc
                    );
                Safe_free(argv[0]);
            }
            Safe_free(argv);
        }
    }
}
#endif//BINARY_C_API
