#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef BINARY_C_API

void binary_c_API_function binary_c_free_store_contents(struct store_t * Restrict const store)
{
    free_store_contents(store);
}

#endif
