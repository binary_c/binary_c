#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef BINARY_C_API

void binary_c_API_function binary_c_help_all(struct stardata_t * Restrict const stardata)
{
    /*
     * API function to put help associated arguments into the
     * buffer. stardata must be non-NULL for this to work,
     * because the buffer must be defined.
     */
    if(stardata != NULL)
    {
        struct tmpstore_t * tmpstore;
         if(stardata->tmpstore == NULL)
        {
            tmpstore = New_tmpstore;
            build_tmpstore_contents(tmpstore);
            stardata->tmpstore = tmpstore;
        }
        else
        {
            tmpstore = stardata->tmpstore;
        }
        binary_c_help_all_from_arg(
            stardata->store->argstore->cmd_line_args,
            stardata, // required for the buffer
            NULL, // dummy pointer
            0, // dummy var
            stardata->store->argstore->arg_count, // arg_count
            NULL, // dummy pointer
            -1 // must be negative
            );
    }
}
#endif//BINARY_C_API
