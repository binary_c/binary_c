#pragma once
#ifndef BINARY_C_TYPES_H
#define BINARY_C_TYPES_H

/*
 * The binary_c stellar population nucleosynthesis framework.
 *
 * Copyright Robert Izzard 2000-2025
 *
 * Contact rob.izzard@gmail.com
 *
 * Code repository:
 * https://gitlab.com/binary_c
 *
 * Documentation is in doc/
 * Code is in src/
 *
 * To build, run
 *
 * ---
 *
 * meson setup builddir
 * cd builddir
 * ninja binary_c_install
 *
 * ---
 *
 * Please see the files README, LICENCE and CHANGES,
 * and the doc/ directory for documentation which is mirrored
 * at https://binary_c.gitlab.io
 *
 **********************
 *
 * Purpose:
 *
 * This file contains the variable types, as typedefs and macros,
 * used in binary_c.
 */


/*
 * Define a Boolean type
 *
 * If we're using _C99, which is the standard in binary_c 2.1,
 * use the built in bool type.
 *
 * Note: if you are building binary_grid::C you should use the
 *       same setup as builds binary_c. E.g. if you use an old
 *       gcc for binary_grid::C, which has no _C99, and a new
 *       gcc for binary_c, you'll have different sizes of stardata_t
 *       which will cause problems. The lesson: be consistent.
 */
#undef FALSE
#undef TRUE

#ifdef _C99
#include <stdbool.h>
#ifndef __USE_C99_BOOLEAN__
#define __USE_C99_BOOLEAN__
#endif // __USE_C99_BOOLEAN__
#endif

#ifdef __USE_C99_BOOLEAN__
#define Boolean bool
#define FALSE (false)
#define TRUE (true)
#else
typedef unsigned short int Boolean;
#define FALSE (0)
#define TRUE (1)
#endif //_C99


/*
 * You can set __short__ to either "short", "long", or nothing,
 * and compare the code's speed. Long ints are slightly slower,
 * but the difference between short int and int is usually very
 * small.
 *
 * Usually, for safety, we set __short__ to nothing, and just
 * use ints instead.
 */
#define __short__

/*
 * Other types can be changed ...
 * but expect many function calls to break if you do.
 */
#define __int__ int
#define __double__ double
#define __long__ long
#define __unsigned__ unsigned
#define __static__ static
#define __const__ const


/*************************************************************
 * Variable types
 */
#define Stardata_type __short__ __int__
#define Star_type __short__ __int__
#define Hierarchy __unsigned__ __short__ __int__
#define Comenv_counter __short__ __int__
#define Stellar_type __short__ __int__
#define Star_number __short__ __int__
#define Multiplicity Star_number
#define Supernova __unsigned__ __short__ __int__
#define Abundance __double__
#define Time __double__
#define Temp_data_table __const__ __double__
#define Const_data_table __static__ __const__ __double__
#define Disc_type __short__ __int__
#define Ensemble_type __unsigned__ __short__ __int__
#define Ensemble_nd_key_type __unsigned__ __short__ __int__
#define Ensemble_nd_data_type __unsigned__ __short__ __int__
#define Event_counter __unsigned__ __int__
#define Disc_zone_counter __unsigned__ __short__ __int__
#define Event_type __short__ __int__
#define Wind_loss_algorithm __unsigned__ __short__ __int__
#define Number_density __double__
#define Isotope  __unsigned__ __short__ __int__ /* must be unsigned */
#define Element __unsigned__ __short__ __int__
#define staticIsotope __unsigned__ __short__ __int__
#define Nuclear_mass __double__
#define Atomic_number __short__ __int__
#define Nucleon_number __short__ __int__
#define Reaction_rate __double__
#define Reaction_rate_index __short__ __unsigned__ __int__
#define Yield_source __short__ __int__
#define Moles __double__
#define Molecular_weight __double__
#define Abundance_ratio __double__
#define WR_type __short__ __int__
#define PNtrigger __unsigned__ __short__ __int__
#define Abundance_mix __unsigned__ __short__ __int__
#define Source_number __unsigned__ __short__ __int__
#define Ensemble_index __unsigned__ __short__ __int__
#define Stellar_magnitude_index __unsigned__ __short__ __int__
#define Shell_index __short__ __int__
#define Supernova_type __short__ __int__
#define Caller_id __short__ __int__
#define Stellar_structure_algorithm __short__ __int__
#define Core_algorithm __short__ __int__
#define Radius_algorithm __short__ __int__
#define Luminosity_algorithm __short__ __int__
#define Third_dredge_up_algorithm __short__ __int__
#define Composition __double__
#define RLOF_stability __short__ __int__
#define Rejection __unsigned__ __short__ __int__
#define Reject_index __unsigned__ __short__ __int__
#define Evolution_system_type __unsigned__ __short__ __int__
#define Derivative __unsigned__ __int__
#define Derivative_group __unsigned__ __int__
#define Lock_index __unsigned__ __int__
#define Spectral_type __int__
#define Long_spectral_type __double__
#define Nova_type __unsigned__ __int__
#define Nova_state __unsigned__ __int__
#define Reaction_network __unsigned__ __int__
#define RAM __unsigned__ __long__ __int__
#define Core_type __int__
#define Pulsator_ID __unsigned__ __int__
#define PN_type __unsigned__ __int__
#define Stream_type __unsigned__ __int__
#define Kick_distribution __int__
#define Log_index __unsigned__ __int__
#define Orbiting_object_type __unsigned__ __int__
#define Accretion_regime __unsigned__ __int__

#ifdef CODESTATS
#define Codestats_counter __unsigned__ __long__ __int__
#endif

#ifdef BINARY_C_USE_LOCAL_RAND48
#include "maths/binary_c_drandr_types.h"
#endif // BINARY_C_USE_LOCAL_RAND48

#ifdef USE_DRAND48_R
#define Random_seed long int
#define Random_buffer struct drand48_data
#define Random_seed_format "%ld"
#define Random_seed_cast long int
#define Random_seed_arg_type ARG_LONG_INTEGER
#endif // USE_DRAND48_R

#ifdef USE_MERSENNE_TWISTER
#define Random_seed unsigned long long int
#define Random_buffer struct mersenne_twister_data_t
#define Random_seed_format "%llu"
#define Random_seed_cast unsigned long long int
#define Random_seed_arg_type ARG_UNSIGNED_LONG_LONG_INTEGER
#endif // USE_MERSENNE_TWISTER

/*
 * The random seed type is either a (positive) long int or
 * unsigned long long int, so map these to
 * unsigned long long int with appropriate format
 */

/* qsort_r function type */
#ifdef __HAVE_GNU_QSORT_R
typedef int (*comparison_fn_r) (const void *, const void *, void * arg);
#endif // __HAVE_GNU_QSORT_R
#ifdef __HAVE_BSD_QSORT_R
typedef int (*comparison_fn_r) (void * arg, const void *, const void *);
#endif // __HAVE_BSD_QSORT_R

/* brent bisector function type */
typedef double(*brent_function)(const double,void*);
#define brent_cast double (*)(const double,void*)


/* lsoda solver derivatives function */
typedef void (*_lsoda_f) (double, double *, double *, void *);

/* lsoda solver Jacobian function */
typedef void (*_lsoda_J) (double, double *, double **, void *);

struct lsoda_data_t
{
    double * sigmav;
    Abundance * N;
    struct stardata_t * stardata;
};

struct cvode_data_t
{
    double * sigmav;
    Abundance * N;
    struct stardata_t * stardata;
};

#define __RAM_FORMAT_SPECIFIER__ "%lu"
#define __RAM_TYPE_LIST__ \
    X(  VmRSS)            \
    X(  VmHWM)            \
    X( VmSize)            \
    X( VmPeak)




#endif // BINARY_C_TYPES_H
