#include "../binary_c.h"
No_empty_translation_unit_warning;


double lbgbdf(const double m,
              const double * Restrict const giant_branch_parameters)
{
    double result;
    double f,df,g,dg,f5,f6,f7,f8;


    /*
     * A function to evaluate the derivitive of the Lbgb function.
     * Note that this function is only valid for LM & IM stars
     * (JH 24/11/97)
     *
     * Rob: update: optimize by calculating the powers just once
     * (divide is quicker than pow)
     */

    f5 = pow(m,giant_branch_parameters[5]);
    f6 = pow(m,giant_branch_parameters[6]);
    f7 = pow(m,giant_branch_parameters[7]);
    f8 = pow(m,giant_branch_parameters[8]);
    double i=1.0/m;

    f = giant_branch_parameters[1]*f5 + giant_branch_parameters[2]*f8;
    df = giant_branch_parameters[5]*giant_branch_parameters[1]*f5*i + 
        giant_branch_parameters[8]*giant_branch_parameters[2]*f8*i;
    g = giant_branch_parameters[3] + giant_branch_parameters[4]*f7 + f6;
    dg = giant_branch_parameters[7]*giant_branch_parameters[4]*f7*i
        + giant_branch_parameters[6]*f6*i;

    result = (df*g - f*dg)/(g*g);

    return(result);
}
/***********************************************************/


