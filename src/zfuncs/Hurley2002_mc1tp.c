#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BSE
double Pure_function Hurley2002_mc1tp(const double phase_start_mass,
                        struct stardata_t * Restrict const stardata)
{
    /*
     * Core mass at the first thermal pulse, according to 
     * Hurley et al. 2000/2002.
     */
    double mcbagb = mcagbf(phase_start_mass,
                           stardata->common.giant_branch_parameters);

    double mc1tp;
    if(mcbagb > 0.8 &&
       mcbagb < stardata->common.max_mass_for_second_dredgeup)
    {
        mc1tp = Min(0.44*mcbagb+0.448, mcbagb);
    }
    else
    {
        mc1tp = mcbagb;
    }

    return mc1tp;
    
}
#endif//BSE
