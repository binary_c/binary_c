#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BSE
double Pure_function rzahbf(const double m, /** mass of star? **/
                            const double mc, /** mass of core? **/
                            const double mhefl, /** ? **/
                            const double * const giant_branch_parameters/** this replaces common block GBCFF **/) 
{
    double mm,f;
    double result;

    /*
     * A function to evaluate the ZAHB radius for LM stars. (OP 28/01/98)
     * Continuity with LHe,min for IM stars is ensured by setting
     * lx = lHeif(mhefl,z,0.0,1.0)*lHef(mhefl,z,mfgb), and the call to lzhef
     * ensures continuity between the ZAHB and the NHe-ZAMS as Menv -> 0.
     */

    mm=Max((m-mc)/(mhefl-mc),1.0e-12);

    f=(1.0+giant_branch_parameters[76])*pow(mm,giant_branch_parameters[75])/
        (1.0+giant_branch_parameters[76]*pow(mm,giant_branch_parameters[77]));

    result=(1.0-f)*rzhef(mc)+f*rgbf(m,
                                    lzahbf(m,mc,mhefl,giant_branch_parameters),
                                    giant_branch_parameters);

    return result;
}

/***********************************************************/

#endif//BSE
