#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BSE
/*
 * Function to estimate the core mass at the first thermal pulse.
 *
 * NB even if we're at the beginning of the thermal pulses,
 * we require this core mass because it may be reduced by second
 * dredge up relative to the EAGB core mass.
 */
double guess_mc1tp(const double mass,
                   const double phase_start_mass,
                   const struct star_t * Restrict const star,
                   struct stardata_t * Restrict const stardata)
{
    Dprint("mass = %g\n",mass);
    double mc1;
    int AGB_core_algorithm = stardata->preferences->AGB_core_algorithm;

    if(AGB_core_algorithm == AGB_CORE_ALGORITHM_DEFAULT)
    {
        AGB_core_algorithm =
#ifdef NUCSYN
            AGB_CORE_ALGORITHM_KARAKAS
#else
            AGB_CORE_ALGORITHM_HURLEY
#endif
            ;
    }

    {
        /*
         * Guess core mass at the first thermal pulse from
         * given algorithm.
         */
        if(AGB_core_algorithm == AGB_CORE_ALGORITHM_HURLEY)
        {
            mc1 = Hurley2002_mc1tp(phase_start_mass,stardata);
        }
        else if(AGB_core_algorithm == AGB_CORE_ALGORITHM_KARAKAS)
        {
            mc1 = Karakas2002_mc1tp(mass,phase_start_mass,stardata);
        }
        else
        {
            mc1 = 0.0;
            Exit_binary_c(BINARY_C_ALGORITHM_BRANCH_FAILURE,
                          "Unknown AGB core mass algorithm");
        }
    }

    {
        /*
         * Force massive stars to have a high mc1tp
         * i.e. avoid thermal pulses
         */
        const double f = 1.0/(1.0 + pow(0.1,mass-20.0));
        mc1 += 1e3 * f;
    }

    Dprint("Mc1TP%d (M=%g, M0=%g Z=%g, t=%g, alg=%d, st=%d, Mc=%g) = %g\n",
           star->starnum,
           mass,
           phase_start_mass,
           stardata->common.metallicity,
           stardata->model.time,
           AGB_core_algorithm,
           star->stellar_type,
           star->core_mass[CORE_He],
           mc1);

    return (mc1);
}
#endif//BSE
