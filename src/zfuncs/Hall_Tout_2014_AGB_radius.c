#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef HALL_TOUT_2014_RADII

double Pure_function Hall_Tout_2014_AGB_radius(struct stardata_t * const stardata,
                                               struct star_t * const star,
                                               const double mc,
                                               const Stellar_type core_stellar_type)
{

    /*
     * Radius of AGB stars from
     * Hall and Tout (2014) Appendix A.
     */
    const double fr = Cubic(mc,
                      7.342054,
                      -1.328317e+1,
                      1.020264e+1,
                      -2.786524);

    const double RWD = rwd(stardata,
                           star,
                           mc,
                           core_stellar_type,
                           FALSE);

    return fr * RWD;
}
#endif // HALL_TOUT_2014_RADII
