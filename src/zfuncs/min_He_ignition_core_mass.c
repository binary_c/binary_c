#include "../binary_c.h"
No_empty_translation_unit_warning;


double Pure_function min_He_ignition_core_mass(struct stardata_t * Restrict const stardata,
                                               struct star_t * Restrict const star Maybe_unused)
{
    /*
     * When preferences->minimum_helium_ignition_core_mass is set on the
     * command line, this function returns the minimum helium core mass
     * for ignition of the core of a stripped red giant.
     */
    return stardata->preferences->minimum_helium_ignition_core_mass;
}
