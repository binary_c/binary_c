#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef AXEL_RGBF_FIX

/*
 * Axel's modified ragbf function, based on BSE
 * but updated.
 */

double Pure_function ragbf(const double m, /* mass */
                           const double lum,/* luminosity */
                           const double mhelflsh Maybe_unused, /* helium flash mass */
                           const double * Restrict const giant_branch_parameters,
                           const double z)
{

    double Pure_function ragbf_orig(const double m,const double lum,
                                    const double giant_branch_parameters[],const double z);
    double Pure_function ragbf_0(const double m,const double lum,
                                 const double giant_branch_parameters[],const double z);
    double Pure_function ragbf_1(const double m,const double lum,
                                 const double giant_branch_parameters[],const double z);
    double alpha;
    double rad0;
    double rad1;
    double result;

    alpha=-(log10(z)-log10(giant_branch_parameters[GBP_AXEL0]))
        /(log10(giant_branch_parameters[GBP_AXEL0])
          -log10(giant_branch_parameters[GBP_AXEL1]));

    rad0=log10(ragbf_0(m,lum,giant_branch_parameters,giant_branch_parameters[GBP_AXEL0]));
    rad1=log10(ragbf_1(m,lum,giant_branch_parameters,giant_branch_parameters[GBP_AXEL1]));

    result=exp10((alpha*rad1 + (1.0-alpha)*rad0));

    return (result);
}

double Pure_function ragbf_orig(const double m, /* mass */
                                const double lum,/* luminosity */
                                const double giant_branch_parameters[],
                                const double z)
{
    double result=NO_RESULT_YET_FLOAT;
    double m1,a1,a4,xx;


    double lzs=(log10(z/0.02));
    double mhelf=1.995 + lzs*(0.25 + lzs*0.087);

    /*
     * A function to evaluate radius on the AGB.
     * (JH 24/11/97)
     */
    m1 = mhelf - 0.2;

    if(More_or_equal(m,mhelf))
    {
        xx = giant_branch_parameters[24];
    }
    else if(More_or_equal(m,m1))
    {
        xx = 1.0 + 5.0*(giant_branch_parameters[24]-1.0)*(m-m1);
    }
    else
    {
        xx = 1.0;
    }

    a4=xx*giant_branch_parameters[19];

    if(Less_or_equal(m,m1))
    {
        a1 = giant_branch_parameters[29] + giant_branch_parameters[30]*m;
    }
    else if(More_or_equal(m,mhelf))
    {
        a1 = Min((giant_branch_parameters[25]/
                  (pow(m,giant_branch_parameters[26]))),
                 (giant_branch_parameters[27]/
                  pow(m,giant_branch_parameters[28]))) ;

    }
    else
    {
        a1 = giant_branch_parameters[31] +
            5.0*(giant_branch_parameters[32]-
                 giant_branch_parameters[31])*(m-m1);

    }


    result = a1*(pow(lum,giant_branch_parameters[18]) +
                 giant_branch_parameters[17]*pow(lum,a4));


    return result;
}
double Pure_function ragbf_0(const double m, /* mass */
                             const double lum,/* luminosity */
                             const double giant_branch_parameters[],
                             const double z)

{
    double result=NO_RESULT_YET_FLOAT;
    double m1,a1,a4,xx;


    double lzs=(log10(z/0.02));
    double mhelf=1.995 + lzs*(0.25 + lzs*0.087);

    m1 = mhelf - 0.2;

    if(More_or_equal(m,mhelf))
    {
        xx = giant_branch_parameters[GBP2_AXEL4];
    }
    else if(More_or_equal(m,m1))
    {
        xx = 1.0 + 5.0*(giant_branch_parameters[GBP2_AXEL4]-1.0)*(m-m1);
    }
    else
    {
        xx = 1.0;
    }

    a4=xx*giant_branch_parameters[GBP_AXEL9];

    if(Less_or_equal(m,m1))
    {
        a1 = giant_branch_parameters[GBP2_AXEL9] + giant_branch_parameters[GBP2_AXEL30]*m;
    }
    else if(More_or_equal(m,mhelf))
    {
        a1 = Min((giant_branch_parameters[GBP2_AXEL5]/
                  (pow(m,giant_branch_parameters[GBP2_AXEL6]))),
                 (giant_branch_parameters[GBP2_AXEL7]/
                  pow(m,giant_branch_parameters[GBP2_AXEL8]))) ;
    }
    else
    {
        a1 = giant_branch_parameters[GBP2_AXEL31] +
            5.0*(giant_branch_parameters[GBP2_AXEL32]-
                 giant_branch_parameters[GBP2_AXEL31])*(m-m1);

    }


    result = a1*(pow(lum,giant_branch_parameters[GBP_AXEL8]) +
                 giant_branch_parameters[GBP_AXEL7]*pow(lum,a4));
    return result;
}

double Pure_function ragbf_1(const double m, /* mass */
                             const double lum,/* luminosity */
                             const double giant_branch_parameters[],
                             const double z)

{
    double result=NO_RESULT_YET_FLOAT;
    double m1,a1,a4,xx;

    double lzs=(log10(z/0.02));
    double mhelf=1.995 + lzs*(0.25 + lzs*0.087);

    m1 = mhelf - 0.2;

    if(More_or_equal(m,mhelf))
    {
        xx = giant_branch_parameters[GBP_AXEL24];
    }
    else if(More_or_equal(m,m1))
    {
        xx = 1.0 + 5.0*(giant_branch_parameters[GBP_AXEL24]-1.0)*(m-m1);
    }
    else
    {
        xx = 1.0;
    }

    a4=xx*giant_branch_parameters[GBP_AXEL19];

    if(Less_or_equal(m,m1))
    {
        a1 = giant_branch_parameters[GBP_AXEL29] + giant_branch_parameters[GBP_AXEL30]*m;
    }
    else if(More_or_equal(m,mhelf))
    {
        a1 = Min((giant_branch_parameters[GBP_AXEL25]/
                  (pow(m,giant_branch_parameters[GBP_AXEL26]))),
                 (giant_branch_parameters[GBP_AXEL27]/
                  pow(m,giant_branch_parameters[GBP_AXEL28]))) ;
    }
    else
    {
        a1 = giant_branch_parameters[GBP_AXEL31] +
            5.0*(giant_branch_parameters[GBP_AXEL32]-
                 giant_branch_parameters[GBP_AXEL31])*(m-m1);

    }


    result = a1*(pow(lum,giant_branch_parameters[GBP_AXEL18]) +
                 giant_branch_parameters[GBP_AXEL17]*pow(lum,a4));
    return(result);
}


#else // AXEL_RGBF_FIX


/*
 * The original RGBF function from BSE
 */

double ragbf(const double m, /* mass */
             const double lum,/* luminosity */
             const double mhelf, /* helium flash mass */
             const double giant_branch_parameters[])
{
    double result,m1,a1,a4;

    /*
     * A function to evaluate radius on the AGB.
     * (JH 24/11/97)
     */

    m1 = mhelf - 0.2;
    if(More_or_equal(m,mhelf))
    {
        a1 = giant_branch_parameters[24];
    }
    else if(More_or_equal(m,m1))
    {
        a1 = 1.0 + 5.0*(giant_branch_parameters[24]-1.0)*(m-m1);
    }
    else
    {
        a1 = 1.0;
    }

    a4=a1*giant_branch_parameters[19];

    if(Less_or_equal(m,m1))
    {
        a1 = giant_branch_parameters[29] + giant_branch_parameters[30]*m;
    }
    else if(More_or_equal(m,mhelf))
    {
        a1 = Min((giant_branch_parameters[25]/
                  (pow(m,giant_branch_parameters[26]))),
                 (giant_branch_parameters[27]/
                  pow(m,giant_branch_parameters[28]))) ;
    }
    else
    {
        a1 = giant_branch_parameters[31] +
            5.0*(giant_branch_parameters[32]-
                 giant_branch_parameters[31])*(m-m1);

    }

    result = a1*(pow(lum,giant_branch_parameters[18]) +
                 giant_branch_parameters[17]*pow(lum,a4));

    return(result);
}

#endif // AXEL_RGBF_FIX
