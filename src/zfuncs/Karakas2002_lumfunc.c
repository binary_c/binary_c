#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BSE

double reallynewlumfunc(Stellar_type stellar_type,
                        double mc,
                        double mc_1tp,
                        double menv,
                        double age,
                        double metallicity,
                        double dmcno3dup,
                        double mcno3dup,
                        double phase_start_mass,
                        double num_thermal_pulses,
                        double interpulse_period,
                        double time_first_pulse,
                        double time_prev_pulse,
                        double time_next_pulse,
                        struct stardata_t *stardata);

double Karakas2002_lumfunc(Stellar_type stellar_type,
                           const double m,
                           const double mc_1tp,
                           const double menv,
                           const double mcno3dup,
                           const double phase_start_mass,
                           const double num_thermal_pulses,
                           const double interpulse_period,
                           const double time_first_pulse,
                           const double time_prev_pulse,
                           const double time_next_pulse,
                           const double age,
                           const Abundance metallicity,
                           struct stardata_t * Restrict const stardata,
                           double * const GB)

{
    const double mc = m-menv;
    Dprint("mc = %g - %g = %g\n",m,menv,mc);
    /*
     * Be careful how we choose the 1st pulse luminosity:
     * between 6 and 10 Msun go smoothly from Amanda's to Jarrod's
     * fits...
     */
    const double l_jarrod = lmcgbf(mc,GB);
    Dprint("L_Jar(%g=%g-%g)=%g\n",mc,m,menv,l_jarrod);

    const double l_amanda = reallynewlumfunc(stellar_type,
                                             mc,
                                             mc_1tp,
                                             Max(0.0,menv),
                                             age,
                                             metallicity,
                                             mcno3dup - mc_1tp,
                                             mcno3dup,
                                             phase_start_mass,
                                             num_thermal_pulses,
                                             interpulse_period,
                                             time_first_pulse,
                                             time_prev_pulse,
                                             time_next_pulse,
                                             stardata);

    const double fl=1.0/(1.0+pow(0.0001,(m-7.0))); // smooth 'step' function
    Dprint("Luminosity interpolate: A=%g J=%g -> %g\n",l_amanda,l_jarrod, ((1.0-fl)*l_amanda+fl*l_jarrod));

    const double l=((1.0-fl)*l_amanda + fl*l_jarrod);
    return l;
}

double reallynewlumfunc(Stellar_type stellar_type,
                        double mc,
                        double mc_1tp,
                        double menv,
                        double age,
                        double metallicity,
                        double dmcno3dup,
                        double mcnodup,
                        double phase_start_mass,
                        double num_thermal_pulses,
                        double interpulse_period,
                        double time_first_pulse Maybe_unused,
                        double time_prev_pulse,
                        double time_next_pulse Maybe_unused,
                        struct stardata_t *stardata)
{
    Dprint("Lfrom mc=%g mc_1tp=%g menv=%g dmcno3=%g mcno=%g\n",mc,mc_1tp,menv,dmcno3dup,mcnodup);


    /* we have to make sure the core mass is >0.5 otherwise
     * we can say very little about the luminosity */
    mcnodup = Max(0.5, mcnodup);
    mc_1tp = Max(0.5, mc_1tp);
    const double menv_1tp = phase_start_mass - mc_1tp;

    // first, Lcore
    double Lcore;

    Lcore=(-3.95570e+04)+(6.27470e+04)*pow(mcnodup,5.95520e-01);
    //printf("Lcore = %g + %g * pow( %g, %g) = %g\n",
    //(-3.95570e+04),(6.27470e+04),mcnodup,5.95520e-01,Lcore);

    if(mcnodup>0.6)
    {
        Lcore=Max(Lcore,
                  (1.68530e+04+4.95870e+03*mcnodup)*
                  pow(mcnodup-5.57600e-01,3.29880e-01));
        if(dmcno3dup > 0.1)
        {
            Lcore*=Max(0.75,1.0-(dmcno3dup-0.1));
        }
    }

    // then Lenvpeak
    double Lenvpeak=((6.11360e+03)+(-2.07190e+03)*phase_start_mass)*
        (1.0+(1.38640e+00)*log(stardata->common.metallicity));

    /* fix for low Z, around M~3 */
    Lenvpeak -= 1.9e3*log10(Max(MINIMUM_METALLICITY,metallicity)/0.02);

    // modulate the envelope loss to calculate Lenv, cannot be < 0
    const double mmin=1.0;
    double Lenv;

    if(menv > mmin &&
       Lenvpeak > 0)
    {
        Lenv = Max(0.0,
                   Lenvpeak*(menv - mmin)/(menv_1tp - mmin));
    }
    else
    {
        Lenv=0.0;
     }

    Dprint("Lenv=%g from Lenvpeak=%g menv=%g mmin=%g\n",Lenv,Lenvpeak,menv,mmin);

    /*
     * turn-on parameters
     */
    double nto,f_turnon_min;
    const double x[2]={
        phase_start_mass,
        Max(MINIMUM_METALLICITY,metallicity)
    };
    double r[2];
    Dprint("call interpolate\n");
    Interpolate(stardata->store->Karakas2002_lumfunc,
                x,
                r,
                0);
    Dprint("interpolate done\n");
    nto = r[0];
    f_turnon_min = r[1];

    Dprint("interpolate %g %g, num_thermal_pulses = %g, interpulse period = %g\n",
            r[0],r[1],num_thermal_pulses,interpulse_period);

    /*
     * estimate ntp, but with fractional part
     */
    double ntp;
    if(num_thermal_pulses > -0.5 &&
       interpulse_period > TINY)
    {
        ntp = Max(0.0,
                  1.0 +
                  num_thermal_pulses +
                  (age - time_prev_pulse)/interpulse_period);
    }
    else
    {
        ntp=0.0;
    }
    const double f_turnon = Max(f_turnon_min,
                                1.0-(1.0-f_turnon_min)*exp(-Max(1.0,ntp)/nto));

    Dprint("f_turnon=%g from Max(%g, 1-(1-%g)*exp(-Max(1,ntp=%g)/%g)\n",
           f_turnon,
           f_turnon_min,
           f_turnon_min,
           ntp,nto);

    /*
     * Hence luminosity
     */
    double L = f_turnon * (Lcore + Lenv);

    /*
     * Check it's not crazy
     */
    if(L<1.0)
    {
        Exit_binary_c(BINARY_C_OUT_OF_RANGE,
                      "L=%g<1 in Karakas2002_lumfunc! Lcore=%g Lenv=%g\n",
                      L,
                      Lcore,
                      Lenv);
    }

    L = Max(KARAKAS2002_TPAGB_MIN_LUMINOSITY, L);

#if (DEBUG==1)
    if(num_thermal_pulses>0)
    {
        Dprint("Luminosity st=%d L=%g from mcnodup=%g menv_1tp=%g nto=%g f_turnon=%g Lcore=%g Lenvpeak=%g Lenv=%g mc_1tp=%g\n",
               stellar_type,L,mcnodup,menv_1tp,nto,f_turnon,Lcore,Lenvpeak,Lenv,mc_1tp);
    }
#endif

#ifdef NANCHECKS
    if(isnan(L)!=0)
    {
        Exit_binary_c(BINARY_C_EXIT_NAN,"nucsyn luminosity nan! = %g\n",L);
    }
#endif

    Dprint("Luminosity st=%d L=%g from mc=%g mcnodup=%g menv_1tp=%g nto=%g f_turnon=%g Lcore=%g Lenvpeak=%g Lenv=%g \n",stellar_type,L,mc,mcnodup,menv_1tp,nto,f_turnon,Lcore,Lenvpeak,Lenv);


#ifdef NUCSYN_SMOOTH_AGB_LUMINOSITY_TRANSITION
    /*
     * Phase in the luminosity between Jarrod's EAGB fit
     * and the fit to Amanda's TPAGB
     */
    if(menv > TINY)
    {
        double f = Max(0.0,1.0e6*(age-TPAGB_start_time(star)))/
            NUCSYN_SMOOTH_AGB_LUMINOSITY_TRANSITION_SMOOTHING_TIME;
        f = Limit_range(f,0.0,1.0);

        double l_old =
            stellar_type==TPAGB ?

            /* we want the EAGB core mass */
            l_old = lmcgbf(star->mcx_EAGB,
                           GB)
            :

            /* pre-TPAGB: use the core mass to calculate L */
            l_old = lmcgbf(mc,
                           GB);


        Dprint("Phase in L (old=%g new=%g f=%g) -> ",l_old,L,f);

        /*
         * should not get dimmer than EAGB 'tip' during interpolation,
         * but what if we lose mass? Then we'll be dimmer! BUT that
         * mass is not lost from the *core* so really we will *not* be
         * much dimmer. Odd!
         */


        Dprint("%g\n",L);

    }
#endif //NUCSYN_SMOOTH_AGB_LUMINOSITY_TRANSITION

    return L;
}



#endif//BSE
