#include "../binary_c.h"
No_empty_translation_unit_warning;


double Pure_function lbgbf(const double m,
                           const double * Restrict const giant_branch_parameters)
{
    double result;

    /*
     * A function to evaluate the luminosity at the end of the 
     * FGB ( for those models that have one )
     * Note that this function is only valid for LM & IM stars
     * (JH 24/11/97)
     */
    result=(giant_branch_parameters[1]*
            pow(m,giant_branch_parameters[5]) + 
            giant_branch_parameters[2]*
            pow(m,giant_branch_parameters[8]))/
        (giant_branch_parameters[3] + 
         giant_branch_parameters[4]*
         pow(m,giant_branch_parameters[7]) +
         pow(m,giant_branch_parameters[6]));

    return result;
}
/***********************************************************/



