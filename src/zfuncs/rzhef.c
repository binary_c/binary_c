#include "../binary_c.h"
No_empty_translation_unit_warning;


Constant_function double rzhef(const double m)
{
    /*
     * A function to evaluate Helium star 'ZAMS' radius
     */
    double result = 0.23910*pow(m,4.6)/(0.00650 + (0.1620 + m)*Pow3(m));
    return(result);
}

/*****************************************/

