#include "../binary_c.h"
No_empty_translation_unit_warning;


double fphase(struct stardata_t * const stardata,
              const double t,
              const struct star_t * const star)
{
    /*
     * Function to return the fractional age of a star
     * in its current phase of evolution
     *
     * t=current age
     */
    double fphase;
    if(star->stellar_type<2)
    {
        fphase=t/star->tms;
    }
    else if(star->stellar_type<4)
    {
        fphase=(t-star->tms)/Max(1e-10,star->tbgb-star->tms);
    }
    else
    {
        /* in later stages we do not know! */
        fphase=0.0;
        Exit_binary_c(BINARY_C_ALGORITHM_OUT_OF_RANGE,
                      "Unable to compute fphase for stellar type %d\n",
                      star->stellar_type);
    }
    return (fphase);
}
