#include "../binary_c.h"
No_empty_translation_unit_warning;


Constant_function double thick_disk_age(const double z)
{
    /* 
     * given the metallicity z calculate the minimum age of an equivalent thick 
     * disk star in Myr, based on Bensby et al. 2004
     */

    /* 
     * this is value of [Fe/H] for an AG89 initial metallicity
     * 
     * The problem is that the relation is not valid for [Fe/H]<-0.9
     * so we limit feh and assume that at lower metallicites
     * this gives a reasonable value for the halo.
     */
    double feh = Max(-0.65,log10(z/0.02));

    /* quadratic fit to Bensby et al 2004 */
    //return (1e3*((4.46840e+00)+(-1.33690e+01)*feh+(-5.31480e+00)*feh*feh));

    /* linear fit to Bensby et al 2004 */
    return 1e3 * ( 5.61 - 6.68*feh );  
}


