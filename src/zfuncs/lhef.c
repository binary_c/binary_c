#include "../binary_c.h"
No_empty_translation_unit_warning;


double Pure_function lhef(const double m,
                          const double * Restrict const giant_branch_parameters)
{
    /*
     * A function to evaluate the ratio LHe,min/LHeI  (OP 20/11/97)
     * Note that this function is everywhere <= 1, and is only valid
     * for IM stars
     */
    double result=(giant_branch_parameters[45] + 
                   giant_branch_parameters[46]*pow(m,(giant_branch_parameters[GBP_LHEF])))/
        (giant_branch_parameters[47] + pow(m,giant_branch_parameters[48]));

    return result;
}
/*****************************************************************/

