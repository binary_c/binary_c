#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef BSE

double Pure_function rtmsf(const double m, /** mass of star? **/
                           const double * Restrict const main_sequence_parameters/** this replaces common block MSCFF **/)
{


    double result;
    double rchk;

    /*
     * A function to evaluate the radius at the end of the MS
     * Note that a safety check is added to ensure Rtms > Rzams
     * when extrapolating the function to low masses.
     * (JH 24/11/97)
     */
    if(Less_or_equal(m,main_sequence_parameters[62]))
    {
    rchk=1.5*rzamsf(m,main_sequence_parameters);
    result = (main_sequence_parameters[52]+main_sequence_parameters[53]*pow(m,main_sequence_parameters[55]))/
        (main_sequence_parameters[54]+pow(m,main_sequence_parameters[56]));
    result=Max(rchk,result);
    }
    else
    {
        if(More_or_equal(m,main_sequence_parameters[62]+0.1))
    {
        double m61=pow(m,main_sequence_parameters[61]);
        result=(main_sequence_parameters[57]*Pow3(m)+
            main_sequence_parameters[58]*m61+
            main_sequence_parameters[59]*m61*m*sqrt(m))/
        (main_sequence_parameters[60]+Pow5(m));
    }
        else
        {
            result=main_sequence_parameters[63]+
                main_sequence_parameters[132]*(m-main_sequence_parameters[62]);
        }
    }


    return(result);

}

/***********************************************************/


#endif//BSE
