#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Function to determine the core mass at the base of
 * the RGB, as in Hurley+2000 Eq. 28
 */
#ifdef BSE

double Pure_function mcbgb(struct star_t * Restrict const s,
                           struct stardata_t * Restrict const stardata)
{
    const double * metallicity_parameters = stardata->common.metallicity_parameters;
    const double * giant_branch_parameters = stardata->common.giant_branch_parameters;
    double mcbgb; // returned core mass

    Boolean degenerate_core = Less_or_equal(s->phase_start_mass,
                                            metallicity_parameters[ZPAR_MASS_HE_FLASH]);

    if(degenerate_core)
    {
        /*
         * Degenerate helium core
         */
        mcbgb = mcgbf(s->bse->luminosities[L_BGB],
                      s->bse->GB,
                      s->bse->luminosities[L_LMX]);
    }
    else if(Less_or_equal(s->phase_start_mass,
                          metallicity_parameters[ZPAR_MASS_FGB]))
    {
        /*
         * Non-degenerate helium ignition on the giant branch
         * core mass
         */
        mcbgb = mcheif(s->phase_start_mass,
                       metallicity_parameters[ZPAR_MASS_HE_FLASH],
                       metallicity_parameters[9],
                       giant_branch_parameters);
    }
    else
    {
        /*
         * Non-degenerate helium ignition before the giant branch
         * core mass
         */
        mcbgb = mcheif(s->phase_start_mass,
                       metallicity_parameters[ZPAR_MASS_HE_FLASH],
                       metallicity_parameters[10],
                       giant_branch_parameters);
    }

    return mcbgb;
}
#endif // BSE
