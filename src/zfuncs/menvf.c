#include "../binary_c.h"
No_empty_translation_unit_warning;


Constant_function double menvf(const Stellar_type stellar_type,
                               const double m,
                               const double mc,
                               const double age,
                               const double tm,
                               const double tbgb)
{
    double result;
    /*
     * A function to estimate the mass of the convective envelope.
     */

    if(ON_MAIN_SEQUENCE(stellar_type))
    {
        if(Less_or_equal(m,0.35))
        {
            result = m;
        }
        else if(More_or_equal(m,1.25))
        {
            result = 0.0;
        }
        else
        {
            result = 0.350*Pow2(((1.250 - m)/0.90));
        }
        result *= pow((1.0-age/tm),0.25);
    }
    else if(stellar_type==HERTZSPRUNG_GAP)
    {
        result =(m-mc)*(age-tm)/(tbgb-tm);
    }
    else
    {
        result = m - mc;
    }
    result = Max(result,1.0E-10);
    return result;
}

/*****************************/




