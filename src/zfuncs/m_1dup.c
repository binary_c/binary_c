#include "../binary_c.h"
No_empty_translation_unit_warning;


double Constant_function m_1dup(const double m)
{
    /*
     * Depth of first dredge up as function of (initial) mass
     * and metallicity
     */

    /* fit for solar metallicity only */
    if(m>8.0 || m<0.5)
    {
        return 0.0;
    }
    else
    {
        return 2.85250e-01 - 1.24890e-01*m + 7.76500e-02*m*m -3.38320e-03*Pow3(m);

    }
}
