#include "../binary_c.h"
No_empty_translation_unit_warning;


/* wrapper function for the molecular weight :
 * if NUCSYN is on, use it, otherwise best guess
 */

double Pure_function molecular_weight(const struct star_t * Restrict const star,
                                      const struct stardata_t * Restrict const stardata)
{
    double mu;
#ifdef NUCSYN
    /* warning: should not use Xenv ! */
    mu = nucsyn_effective_molecular_weight(star->Xenv,
                                           star->stellar_type,
                                           stardata->store->molweight);
#else
    /* chemistry unknown! assume either H or He rich */
    double Y,X = star->stellar_type<=TPAGB ? 0.75 : 0.0;
    Y=1.0-X-stardata->common.metallicity;
    mu = 4.0/(6.0*X+Y+2.0);
#endif
    return(mu);
}
