#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "mc_1dup.h"

double mc_1DUP(struct stardata_t * Restrict const stardata,
               const double m,
               const double z)
{

    /*
     * (helium) Core mass at maximum extent of the convective envelope
     * at 1st dredge up as a function of initial mass
     */

    /* tabular solution */
    Const_data_table table[]={MC_1DUP_EVERT}; // data table
    double x[2]={log10(z/0.02),m}; // parameters
    double r[1]; // results
    rinterpolate(table,
                 stardata->persistent_data->rinterpolate_data,
                 2,
                 1,
                 MC_1DUP_EVERT_LINES,
                 x,
                 r,
                 0);
    return Min(m,r[0]);
}
