#include "../binary_c.h"
No_empty_translation_unit_warning;


double Pure_function mcgbf(const double lum,
                           const double * Restrict const GB,
                           const double lx)

{
    /*
     * A function to evaluate Mc given L for GB, AGB and NHe stars
     *
     * NB Low mass
     */
    return Less_or_equal(lum,lx) ?
        pow(lum/GB[L_HE_IGNITION],1.0/GB[L_HE_BURNING]) :
        pow(lum/GB[L_BGB],1.0/GB[L_LMX]);

}


/****************************************************/
