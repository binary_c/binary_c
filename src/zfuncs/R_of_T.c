#include "../binary_c.h"
No_empty_translation_unit_warning;
double Pure_function R_of_T(const struct star_t * Restrict const star,
                            const double T)
{
    /*
     * Compute the radius at a given temperature
     */
    return sqrt(
        L_SUN*star->luminosity/
        (4.0 * PI * STEFAN_BOLTZMANN_CONSTANT * Pow4(T))
        ) / R_SUN;
}
