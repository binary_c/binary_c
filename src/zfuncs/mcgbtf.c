#include "../binary_c.h"
No_empty_translation_unit_warning;


double Pure_function mcgbtf(const double t,
                            const double A,
                            const double * Restrict GB,
                            const double tinf1,
                            const double tinf2,
                            const double tx)
{
    /*
     * A function to evaluate Mc given t for GB, AGB and NHe stars
     *
     * Note that on the GB, for stars with M>MHef, this is not
     * the true core mass, rather it is a "dummy" core mass.
     */
    double result,y;

    if(Less_or_equal(t,tx))
    {
        y=GB[GB_p]-1.0;
        result = pow(y*A*GB[GB_D]*(tinf1 - t),-1.0/y);
    }
    else
    {
        y=GB[GB_q]-1.0;
        result = pow(y*A*GB[GB_B]*(tinf2 - t),-1.0/y);
    }

    return result;
}

/*
 * Time derivative of the above, i.e. dMc/dt
 * cf. Hurley + 2002 Eq.33
 */
double Pure_function dmcgbtf_dt(const double t,
                                const double A,
                                const double * Restrict GB,
                                const double tinf1,
                                const double tinf2,
                                const double tx)
{
    double result,r1,r2,y;
    const double dt = 1e-7; // a small time for the derivative

    /*
     * TODO : do this analytically!
     */
    if(Less_or_equal(t,tx))
    {
        y=GB[GB_p]-1.0;
        r1 = pow(y*A*GB[GB_D]*(tinf1 - t),-1.0/y);
        r2 = pow(y*A*GB[GB_D]*(tinf1 - t + dt),-1.0/y);
    }
    else
    {
        y=GB[GB_q]-1.0;
        /*
         * When combining schemes, t may be > tinf.
         * In this case, pretend we're in the final 10000 years
         * (0.01 Myr) of the TPAGB to calculate the core
         * growth rate.
         */
        double d = Max(0.01,tinf2 - t);
        r1 = pow(y*A*GB[GB_B]*(d),-1.0/y);
        r2 = pow(y*A*GB[GB_B]*(d + dt),-1.0/y);
    }
    result = (r1-r2)/dt;
    return result;
}
/****************************************************/
