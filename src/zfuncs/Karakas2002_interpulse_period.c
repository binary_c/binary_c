#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "../binary_c_macros.h"

double Karakas2002_interpulse_period(struct stardata_t * Restrict const stardata,
                                     const double mc,
                                     const double m,
                                     const double mc_1tp,
                                     Abundance Z,
                                     const double lambda,
                                     const struct star_t * const star Maybe_unused)
{
    /*
     * Return the interpulse period in Myr
     */

#ifdef KARAKAS2002_REFITTED_TPAGB_INTERPULSES



    /*
     * fits are ok for small core growth, limit dmc so
     * my models do not diverge (often this is at the end of the
     * AGB where the envelope is small and there are few
     * models in Amanda's dataset)
     *
     * NB This fit seems to work for the metallicities at
     * which it is fitted, but the behaviour at interpolated
     * metallicities has not been extensively (or at all!)
     * tested.
     */

    /*
     * Limit Z to fitted range
     */
    Z = Limit_range(Z,1e-4,0.02);
    const double dmc = Min(0.03,mc - mc_1tp);
    const double coeffs[1]={log10(Z)};
    Const_data_table data[] =
        {
            -4.00000000000000,8.28820e+00,-5.21350e+00,1.69620e+01,-2.80010e+01,-2.74060e+00,4.64860e+00,4.66790e-01,
            -2.39794000867204,7.74410e+00,-4.82270e+00,3.62600e+00,-7.32400e+00,-7.54200e-01,1.79180e+00,3.47060e-01,
            -2.09691001300806,7.38610e+00,-4.41250e+00,3.18040e+00,-6.27530e+00,-7.34680e-01,1.78200e+00,3.62660e-01,
            -1.69897000433602,7.03540e+00,-4.03630e+00,5.14420e+00,-1.59380e+01,-4.72540e+00,1.26170e+01,7.56250e-01
        };

    double x[8];

    /*
     * interpolate the above data in metallicity
     * to find fit coefficients
     */
    interpolate(stardata,data,1,7,4,coeffs,x,FALSE);
    Dprint("TIP mc=%g dmc=%g x[0]=%g x[1]=%g x[2]=%g x[3]=%g x[4]=%g x[5]=%g x[6]=%g\n",mc,dmc,x[0],x[1],x[2],x[3],x[4],x[5],x[6]);

    double fit = (x[0]+x[1]*mc)*(1.0+(x[2]+x[3]*mc)*dmc +(x[4]+x[5]*mc)*pow(dmc,x[6]));
    Dprint("TIP fit(logtip) =%g\n",fit);

    fit = exp10(fit - 6.0);
    printf("TIP mc=%g dmc=%g fit=%g\n",mc,dmc,fit);

    fit = Max(MINIMUM_INTERPULSE_PERIOD_MYR,fit);

    return fit;

#else
    double x;
    double zz ;
    const double z1 = 0.02;
    const double z2 = 0.008;
    const double z3 = 0.004;
    const double z4 = 0.0001;
    const double a1[] = {-3.821,1.8926};
    const double a2[] = {-4.189,1.8187};
    const double a3[] = {-4.255,1.8142};
    double a4[] = {-4.5,1.79};
    double a[2];
    double h1,h2;

    Dprint("Interpulse period from mc=%g m=%g mc_1tp=%g Z=%g lambda=%g\n",
           mc,m,mc_1tp,Z,lambda);

    /*
     * Onno's function cannot deal with Z>0.02, so cap Z at 0.02 - this is ok
     * because the WG98 version used below has no Z dependence anyway
     */
    Z = Min(0.02,Z);
    zz = log10(Z/0.02);

    /*
     * Interpulse period, eq.(11) from Wagenhuber & Groenwegen (1998).
     *
     * NB: the Z-dependence has not been included.
     * m is the current mass .
     *
     */

#if (DEBUG==1)
    x = -3.6280*(mc - 1.94540) -
        exp10((-2.080 + 0.20*(m - mc + TPAGB_MIXING_LENGTH_PARAMETER - 1.50))) -
        exp10((-0.6260 - 70.30*mc_1tp*(mc-mc_1tp)));
    Dprint("G+DJ (no Z dep) interpulse is %g\n",exp10(x-6));
#endif

/*
 * Interpulse period, as function of core mass MC, with corrections
 * for HBB, turn-on effect, and third dredge-up.
 *  Formula from W&G modified to fit Amanda's models.
 */
    if(Z>z2)
    {
        h1 = (Z-z2)/(z1-z2);
        h2 = 1.0 - h1;
        a[0] = a1[0]*h1 + a2[0]*h2;
        a[1] = a1[1]*h1 + a2[1]*h2;
    }
    else if(Z>z3)
    {
        h1 = (Z-z3)/(z2-z3);
        h2 = 1.0 - h1;
        a[0] = a2[0]*h1 + a3[0]*h2;
        a[1] = a2[1]*h1 + a3[1]*h2;
    }
    else
    {
        h1 = (Z-z4)/(z3-z4);
        h2 = 1.0 - h1;
        a[0] = a3[0]*h1 + a4[0]*h2;
        a[1] = a3[1]*h1 + a4[1]*h2;
    }

    Dprint("mc(%g)-mc_1tp(%g)=%g, a[0]=%g a[1]=%g 2nd=%g 3rd=%g\n",
           mc,mc_1tp,mc-mc_1tp,a[0],a[1],
           exp10((-2.08 - 0.353*zz + 0.2*(m - mc + TPAGB_MIXING_LENGTH_PARAMETER - 1.5))) ,
           exp10((-(0.23+0.44*mc_1tp) - 70.3*(mc_1tp-zz)*(mc-mc_1tp))));

    x = a[0]*(mc - a[1]) -

        exp10((-2.08 - 0.353*zz + 0.2*(m - mc + TPAGB_MIXING_LENGTH_PARAMETER - 1.5))) -
        exp10((-(0.23+0.44*mc_1tp) - 70.3*(mc_1tp-zz)*(mc-mc_1tp)));

    x += 0.15*lambda*lambda;

    /*
     * return period in Myr, minimum period 100 years
     */
    x = Max(MINIMUM_INTERPULSE_PERIOD_MYR,
            exp10(x - 6.0));

    Dprint("set interpulse time (zz=%g mc=%g mc_1tp=%g TPAGB_MIXING_LENGTH_PARAMETER=%g) %g\n",zz,mc,mc_1tp,TPAGB_MIXING_LENGTH_PARAMETER,exp10(x));

    return x;

#endif

}
