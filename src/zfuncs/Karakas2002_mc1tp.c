#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Core mass at the first thermal pulse, as a function total mass and
 * metallicity.
 */

double Pure_function Karakas2002_mc1tp(double m,
                                       const double phase_start_mass,
                                       struct stardata_t * stardata)
{
    /* save values passed in */
    const double m_in = m;
    const double metallicity_in = stardata->common.metallicity;
    double mc1tp; /* return value */

    /*
     * New version from revised PASA paper (thanks Onno!)
     * Added line for 1e-4<Z<0.004. Not much different to previous fit but has
     * mass break at M=2.0 rather than M=2.78 (compares well to the detailed models)
     *
     * NB fit doesn't work so well outside parameterized range! M=6.5 Z=0.02 fails by 0.02Msun
     */

    /* limit parameter range to fitted range */
    m = Limit_range(m_in,1.0,6.5);
    const double metallicity = Limit_range(metallicity_in,
                                           1e-4,
                                           0.02);

    Dprint("find mc1tp from m_in=%g (clamped to m=%g) Z=%g (clamped to %g) phase_start_mass=%g\n",
           m_in,
           m,
           metallicity_in,
           metallicity,
           phase_start_mass);

#define ASIZE 7
    const Abundance zz1 = 0.02;
    const Abundance zz2 = 0.008;
    const Abundance zz3 = 0.004;
    const Abundance zz4 = 0.0001;
    const Abundance zz=metallicity;
    double f;

    double h1,h2;
    const double a1[ASIZE] ={0.038515,1.41379,0.555145,0.039781,0.675144,3.18432,0.368777};
    const double a2[ASIZE] ={0.057689,1.42199,0.548143,0.045534,0.652767,2.90693,0.287441};
    const double a3[ASIZE] ={0.040538,1.54656,0.550076,0.054539,0.625886,2.78478,0.227620};
    const double a4[ASIZE]={-0.0202032242383,3.33072019625216,
                            0.84902589544912-0.002,0.03761533717536,0.49346591154945,
                            2.52581123711777,-0.3182707221031};

    /*
     * Either interpolate the coefficients or the core mass.
     * Interpolating the core mass is recommended, it's a more
     * stable method.
     */

//#define INTERPOLATE_COEFFS
#define INTERPOLATE_MC1TP

#ifdef INTERPOLATE_COEFFS
    double a[ASIZE];
    int i;
    if(zz>zz2)
    {
        h1 = (zz-zz2)/(zz1-zz2);
        h2 = 1.0 - h1;
        for(i=0;i<ASIZE;i++)
        {
            a[i] = a1[i]*h1 + a2[i]*h2;
        }
    }
    else  if(zz>zz3)
    {
        h1 = (zz-zz3)/(zz2-zz3);
        h2 = 1.0 - h1;
        for(i=0;i<ASIZE;i++)
        {
            a[i] = a2[i]*h1 + a3[i]*h2;
        }
    }
    else
    {
        h1 = (zz-zz4)/(zz3-zz4);
        h2 = 1.0 - h1;
        for(i=0;i<ASIZE;i++)
        {
            a[i] = a3[i]*h1 + a4[i]*h2;
        }
    }

    f=1.0/(1.0+exp((m-a[5])/a[6]));
    mc1tp=f*(-a[0]*(m-a[1])*(m-a[1])+a[2]) + (1.0-f)*(a[3]*m+a[4]);
#endif

#ifdef INTERPOLATE_MC1TP
    double y1,y2;
    /*
     * Instead of interpolating coefficients, interpolate the result of the calculation.
     * This is much more stable.
     */
    if(zz>zz2)
    {
        h1 = (zz-zz2)/(zz1-zz2);
        h2 = 1.0 - h1;

        f=1.0/(1.0+exp((m-a1[5])/a1[6]));
        y1=f*(-a1[0]*(m-a1[1])*(m-a1[1])+a1[2]) + (1.0-f)*(a1[3]*m+a1[4]);
        f=1.0/(1.0+exp((m-a2[5])/a2[6]));
        y2=f*(-a2[0]*(m-a2[1])*(m-a2[1])+a2[2]) + (1.0-f)*(a2[3]*m+a2[4]);
    }
    else  if(zz>zz3)
    {
        h1 = (zz-zz3)/(zz2-zz3);
        h2 = 1.0 - h1;
        f=1.0/(1.0+exp((m-a2[5])/a2[6]));
        y1=f*(-a2[0]*(m-a2[1])*(m-a2[1])+a2[2]) + (1.0-f)*(a2[3]*m+a2[4]);
        f=1.0/(1.0+exp((m-a3[5])/a3[6]));
        y2=f*(-a3[0]*(m-a3[1])*(m-a3[1])+a3[2]) + (1.0-f)*(a3[3]*m+a3[4]);
    }
    else
    {
        h1 = (zz-zz4)/(zz3-zz4);
        h2 = 1.0 - h1;
        f=1.0/(1.0+exp((m-a3[5])/a3[6]));
        y1=f*(-a3[0]*(m-a3[1])*(m-a3[1])+a3[2]) + (1.0-f)*(a3[3]*m+a3[4]);
        f=1.0/(1.0+exp((m-a4[5])/a4[6]));
        y2=f*(-a4[0]*(m-a4[1])*(m-a4[1])+a4[2]) + (1.0-f)*(a4[3]*m+a4[4]);
    }
    mc1tp=h1*y1+h2*y2;

    Dprint("Amanda's interpolate gave mc1tp=%g (m_in=%g m=%g Z_in=%g Z=%g)\n",mc1tp,m_in,m,metallicity_in,metallicity);
#endif

#ifdef BSE
    /*
     * we have no fits above M~6.5, interpolate to use Jarrod's
     * with a "smooth step function"
     */
    const double fl = 1.0 / (1.0 + pow(0.0001, phase_start_mass - 7.0));
    mc1tp = (1.0 - fl) * mc1tp + fl * Hurley2002_mc1tp(phase_start_mass,stardata);

#endif// BSE

#ifdef NANCHECKS
    if(isnan(mc1tp))
    {
        Exit_binary_c(BINARY_C_EXIT_NAN,"Mc1tp NAN\n");
    }
#endif

    return mc1tp;
}
