#pragma once
#ifndef BINARY_C_CDICT_H
#define BINARY_C_CDICT_H
/*
 * The binary_c stellar population nucleosynthesis framework.
 *
 * Copyright Robert Izzard 2000-2025
 *
 * Contact rob.izzard@gmail.com
 *
 * Code repository:
 * https://gitlab.com/binary_c
 *
 * Documentation is in doc/
 * Code is in src/
 *
 * To build, run
 *
 * ---
 *
 * meson setup builddir
 * cd builddir
 * ninja binary_c_install
 *
 * ---
 *
 * Please see the files README, LICENCE and CHANGES,
 * and the doc/ directory for documentation which is mirrored
 * at https://binary_c.gitlab.io
 *
 **********************
 *
 * Purpose:
 *
 * Load in cdict support in binary_c: we use the cdict library
 * to automatically make nested cdicts in C.
 */

#ifdef __HAVE_LIBCDICT__
#undef exit
#include <cdict/cdict.h>
#else
#include "libcdict/cdict.h"
#endif

#endif // BINARY_C_CDICT_H
