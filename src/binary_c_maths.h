#pragma once
#ifndef BINARY_MATHS_H
#define BINARY_MATHS_H

/*
 * The binary_c stellar population nucleosynthesis framework.
 *
 * Copyright Robert Izzard 2000-2025
 *
 * Contact rob.izzard@gmail.com
 *
 * Code repository:
 * https://gitlab.com/binary_c
 *
 * Documentation is in doc/
 * Code is in src/
 *
 * To build, run
 *
 * ---
 *
 * meson setup builddir
 * cd builddir
 * ninja binary_c_install
 *
 * ---
 *
 * Please see the files README, LICENCE and CHANGES,
 * and the doc/ directory for documentation which is mirrored
 * at https://binary_c.gitlab.io
 *
 **********************
 *
 * Purpose:
 *
 * This file contains macros used in binary_c that are
 * mathematical in nature.
 */

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif //_GNU_SOURCE
#include <math.h>
#include <float.h>
#include <stdarg.h>
#include "maths/vectors.h"



/* Handy for debugging */
#define UNUSED_INT (-666666)
#define UNUSED_FLOAT (1.0 * UNUSED_INT)
#define NO_RESULT_YET_FLOAT UNUSED_FLOAT

/*
 * If defined, allow monochecks in generic_bisect.
 */
#define BISECT_DO_MONOCHECKS

/*
 * define USE_FABS to force the use of fabs instead of
 * comparisons to +TINY and -TINY. This is usually slow.
 */
//#define USE_FABS

/*
 * defined ZERO_CHECKS_ARE_FUNCTIONS to make Is_zero etc. functions,
 * otherwise they are macros
 *
 * NB This is ignored if USE_FABS is defined
 */
//#define ZERO_CHECKS_ARE_FUNCTIONS

/*
 * Use inline functions for simple pow(x,y) operations
 */
//#define POWER_OPERATIONS_ARE_FUNCTIONS

/*
 * Timings for 1000 sytems
 # 123.17,124.58,125.47 standard run : no fabs, all others functions
 # 117.94,118.92,120.57 no fabs, all macros
 # 119.64,120.25,120.73 macros with fabs
 # 120.80,123.88,124.80 all macros, except min/max are functions[C
 # 117.80,119.01,120.97 all macros, except pow calls arefunctions
*/

/************************************************************/

/* small and large numbers */
#define ZERO (0.0)
#define TINY (1e-14)
#define VERY_TINY (1e-20)
#define REALLY_TINY (1E-200)
#define LARGE_FLOAT (1E200)

/* geometric constants */
#define PI (3.141592653589793238462643383279)
#define TWOPI (2.0*PI)

/* swap macro : A and B must be of the same type */
#define Swap(A,B) {const Autotype(A) temp=(A);(A)=(B);(B)=temp;}

/* macros to define less and more than operations */
#define Less_than(A,B) ((A)<(B))
#define More_than(A,B) ((A)>(B))

/* macros to define less and more than taking TINY into account */
#define Really_less_than(A,B) (Less_than((A),(B))&&!Fequal((A),(B)))
#define Really_more_than(A,B) (More_than((A),(B))&&!Fequal((A),(B)))

/*
 * Min and Max of two numbers
 * Note: this calculates them twice. This is not ideal,
 * and while there is a solution at https://gcc.gnu.org/onlinedocs/gcc/Typeof.html
 * this fails when the macros are nested because of
 * variable shadowing.
 *
 * The solution is at
 * https://rentzsch.tumblr.com/post/12960046342/nearly-hygienic-c-macros-via-counter
 */
#define Max_macro(A,B) ((A)>(B) ? (A) : (B))
#define Min_macro(A,B) ((A)<(B) ? (A) : (B))

#ifdef USE_GCC_EXTENSIONS

#define Max_implementation(A,B,LABEL,COUNTER)           \
    __extension__                                       \
    ({                                                  \
        __typeof__(A) Concat3(__a,LABEL,COUNTER) = (A); \
        __typeof__(B) Concat3(__b,LABEL,COUNTER) = (B); \
        Max_macro(Concat3(__a,LABEL,COUNTER),           \
                  Concat3(__b,LABEL,COUNTER));          \
    })
#define Max(A,B) Max_implementation((A),(B),Max,__COUNTER__)

#define Min_implementation(A,B,LABEL,COUNTER)           \
    __extension__                                       \
    ({                                                  \
        __typeof__(A) Concat3(__a,LABEL,COUNTER) = (A); \
        __typeof__(B) Concat3(__b,LABEL,COUNTER) = (B); \
        Min_macro(Concat3(__a,LABEL,COUNTER),           \
                  Concat3(__b,LABEL,COUNTER));          \
    })
#define Min(A,B) Min_implementation((A),(B),Min,__COUNTER__)

#else
/* fallback */
#define Max(A,B) Max_macro((A),(B))
#define Min(A,B) Min_macro((A),(B))
#endif // USE_GCC_EXTENSIONS

/* max, min of several numbers */
#define Max2(A,B) Max((A),(B))
#define Max3(A,B,C) Max((A),Max((B),(C)))
#define Max4(A,B,C,D) Max((A),Max3((B),(C),(D)))
#define Max5(A,B,C,D,E) Max((A),Max4((B),(C),(D),(E)))
#define Min2(A,B) Min((A),(B))
#define Min3(A,B,C) Min((A),Min((B),(C)))
#define Min4(A,B,C,D) Min((A),Min3((B),(C),(D)))
#define Min5(A,B,C,D,E) Min((A),Min4((B),(C),(D),(E)))

/*
 * NB minx should not be NULL
 */
#define MinX_implementation(LABEL,COUNTER,ax,ay,bx,by,_minx)        \
    __extension__                                                   \
    ({                                                              \
        const Autotype(ax) Concat3(LABEL,COUNTER,_ax) = (ax);       \
        const Autotype(bx) Concat3(LABEL,COUNTER,_bx) = (bx);       \
        const Autotype(ay) Concat3(LABEL,COUNTER,_ay) = (ay);       \
        const Autotype(by) Concat3(LABEL,COUNTER,_by) = (by);       \
        *_minx = Less_than(Concat3(LABEL,COUNTER,_ay),              \
                           Concat3(LABEL,COUNTER,_by))              \
            ? Concat3(LABEL,COUNTER,_ax)                            \
            : Concat3(LABEL,COUNTER,_bx);                           \
        Min(Concat3(LABEL,COUNTER,_ay),                             \
            Concat3(LABEL,COUNTER,_by));                            \
    })
#define MinX(ax,ay,bx,by,_minx)                             \
    MinX_implementation(MinX,__COUNTER__,ax,ay,bx,by,_minx)

/*
 * NB maxx should not be NULL
 */
#define MaxX_implementation(ax,ay,bx,by,_maxx)                  \
    __extension__                                               \
    ({                                                          \
        const Autotype(ax) Concat3(LABEL,COUNTER,_ax) = (ax);   \
        const Autotype(bx) Concat3(LABEL,COUNTER,_bx) = (bx);   \
        const Autotype(ay) Concat3(LABEL,COUNTER,_ay) = (ay);   \
        const Autotype(by) Concat3(LABEL,COUNTER,_by) = (by);   \
        *_maxx = More_than(Concat3(LABEL,COUNTER,_ay),          \
                           Concat3(LABEL,COUNTER,_by))          \
            ? Concat3(LABEL,COUNTER,_ax)                        \
            : Concat3(LABEL,COUNTER,_bx);                       \
        Max(Concat3(LABEL,COUNTER,_ay),                         \
            Concat3(LABEL,COUNTER,_by));                        \
    })
#define MaxX(ax,ay,bx,by,_maxx)                             \
    MaxX_implementation(MinX,__COUNTER__,ax,ay,bx,by,_maxx)

/*
 * Which_MIN and Which_MAX return the minimum and maximum (respectively)
 * of the list of doubles passed in and sets *which to the index
 * (0-based) of the minimum in the list.
 */
static inline double _which_Min(unsigned int * which,
                                unsigned int n,
                                ...);
static inline double _which_Max(unsigned int * which,
                                unsigned int n,
                                ...);
static inline double _which_Min(unsigned int * which,
                                unsigned int n,
                                ...)
{
    va_list args;
    va_start(args,n);

    double min = LARGE_FLOAT;
    unsigned int i;
    for(i=0;i<n;i++)
    {
        double x = va_arg(args,double);
        if(Less_than(x, min))
        {
            *which = i;
            min = x;
        }
    }
    va_end(args);
    return min;
}
static inline double _which_Max(unsigned int * which,
                                unsigned int n,
                                ...)
{
    va_list args;
    va_start(args,n);

    double max = -LARGE_FLOAT;
    unsigned int i;
    for(i=0;i<n;i++)
    {
        double x = va_arg(args,double);
        if(More_than(x, max))
        {
            *which = i;
            max = x;
        }
    }
    va_end(args);
    return max;
}

/* convenience macros for accessing _which_Min and _which_Max */
#define Which_MIN2(I,A,B) _which_Min(&(I),2,(A),(B))
#define Which_Min3(I,A,B,C) _which_Min(&(I),3,(A),(B),(C))
#define Which_Min4(I,A,B,C,D) _which_Min(&(I),4,(A),(B),(C),(D))
#define Which_Min5(I,A,B,C,D,E) _which_Min(&(I),5,(A),(B),(C),(D),(E))
#define Which_MAX2(I,A,B) _which_Max(&(I),2,(A),(B))
#define Which_Max3(I,A,B,C) _which_Max(&(I),3,(A),(B),(C))
#define Which_Max4(I,A,B,C,D) _which_Max(&(I),4,(A),(B),(C),(D))
#define Which_Max5(I,A,B,C,D,E) _which_Max(&(I),5,(A),(B),(C),(D),(E))


/*
 * Macro to calculate the fractional difference between two numbers
 * with a check to make sure we avoid 1/0 and calculate the maximum
 * difference (assumes floats/doubles)
 */
#define Signed_diff_macro(A,B) (((A)-(B))/Max(Min(fabs(A),fabs(B)),(TINY)))
#ifdef USE_GCC_EXTENSIONS
#define Signed_diff_implementation(A,B,LABEL,COUNTER)   \
    __extension__                                       \
    ({                                                  \
        __typeof__(A) Concat3(__a,LABEL,COUNTER) = (A); \
        __typeof__(B) Concat3(__b,LABEL,COUNTER) = (B); \
        Signed_diff_macro(Concat3(__a,LABEL,COUNTER),   \
                          Concat3(__b,LABEL,COUNTER));  \
    })
#define Signed_diff(A,B) Signed_diff_implementation((A),(B),Signed_diff,__COUNTER__)
#else
#define Signed_diff(A,B) Signed_diff_macro((A),(B))
#endif // USE_GCC_EXTENSIONS

/*
 * Macro to calculate the absolute fractional difference between two numbers
 * with a check to make sure we avoid 1/0 and calculate the maximum
 * difference (assumes floats/doubles)
 */
#define Abs_diff_macro(A,B) (fabs((A)-(B))/Max(Min(fabs(A),fabs(B)),(TINY)))
#ifdef USE_GCC_EXTENSIONS
#define Abs_diff_implementation(A,B,LABEL,COUNTER)   \
    __extension__                                       \
    ({                                                  \
        __typeof__(A) Concat3(__a,LABEL,COUNTER) = (A); \
        __typeof__(B) Concat3(__b,LABEL,COUNTER) = (B); \
        Abs_diff_macro(Concat3(__a,LABEL,COUNTER),   \
                          Concat3(__b,LABEL,COUNTER));  \
    })
#define Abs_diff(A,B) Abs_diff_implementation((A),(B),Abs_diff,__COUNTER__)
#else
#define Abs_diff(A,B) Abs_diff_macro((A),(B))
#endif // USE_GCC_EXTENSIONS


/*
 * Use Abs_diff to see if A and B are the same within
 * some threshold EPS
 */
#define Float_same_within_eps(A,B,EPS) (Abs_diff((A),(B))<=(EPS))

/*
 * Apply Float_same_within_eps with the double-precision
 * floating point epsilon. NB we assume that we never use
 * single-precision in binary_c
 */
#define Float_same_within_dbl_epsilon(A,B) Float_same_within_eps((A),(B),(DBL_EPSILON))
#define Float_same_within_ten_dbl_epsilon(A,B) Float_same_within_eps((A),(B),(10.0*(DBL_EPSILON)))

/*
 * Macro to calculate the absolute % difference between two numbers
 * with a check to make sure we avoid 1/0 and calculate the maximum
 * % difference (assumes floats/doubles)
 */
#define Percent_diff(A,B) (100.0*Signed_diff((A),(B)))
#define Abs_percent_diff(A,B) (100.0*Abs_diff((A),(B)))

// fast bit-shift equivalent to pow(2,A) but for integers
#define Integer_power_of_two(A) (1<<(A))

/* integer power macro (uses inline function) */
#define Power_of_integer(A,B) (powab((A),(B)))
static inline int powab(const int a,const int b) Pure_function;
static inline int powab(const int a,const int b){int r=1;int i;for(i=0;i<b;i++){r*=a;}return r;}


/*
 * Safe(r) pow(x,y) to avoid over/underflows.
 * Of course, this is a VERY crude fix. You're
 * better off fixing the underlying problem.
 */
#define Saferpow(X,Y) (pow((X),Max(-100.0,Min(100.0,(Y)))))

/*
 * Useful macros for floating point comparison, on the
 * assumption that numbers are ~ 1.0.
 *
 * Less_or_equal is supposed to be <= for floats, More_or_equal is >=, Fequal
 * is ==. The idea is that if the two numbers are similar in magnitude, A*TINY
 * will always provide an accurate test of equality. Of course, you have to
 * choose TINY appropriately - with (say) 30 bits of mantissa, that's 9E-10 so
 * 1E-20 is plenty small enough! To then get a relatively small number,
 * multiply TINY by either A or B (since they are similar!).
 *
 * The Fequal works in the same way, with a > or < check as well.
 *
 * There are two sets of macros: with and without fabs.
 *
 * The fabs function call is potentially slow, and not really necessary.
 * You might want to disable it (see options at the top of this file).
 *
 * If your numbers are likely to not be ~1.0, you should use
 * Float_same_within_dbl_epsilon and Float_same_within_ten_dbl_epsilon above.
 */
#ifdef USE_FABS

/* Macros for comparisons using fabs */
#define Is_zero(A) (fabs((A))<TINY)
#define Is_really_zero(A) (fabs((A))<REALLY_TINY)
#define Is_not_zero(A) (fabs((A))>TINY)
#define Is_really_not_zero(A) (fabs((A))>TINY)
#define Fequal(A,B) (Is_zero((A)-(B)))
#define More_or_equal(A,B) (((A)>=(B))||(Fequal((A),(B))))
#define Less_or_equal(A,B) More_or_equal((B),(A))
#define Abs_more_than(A,B) (fabs((A))>(B))
#define Abs_more_than_or_equal(A,B) (Abs_more_than((A),(B))||Fequal((A),(B)))
#define Abs_less_than(A,B) (fabs((A))<(B))
#define Abs_less_than_or_equal(A,B) (Abs_less_than((A),(B))||Fequal((A),(B)))


#else//USE_FABS

/* Macros for comparisons without using fabs */
#define Is_zero_macro(A) (((A)<TINY)&&(-(A)<TINY))
#define Is_really_zero_macro(A) (((A)<REALLY_TINY)&&(-(A)<REALLY_TINY))

#ifdef ZERO_CHECKS_ARE_FUNCTIONS
#define Is_zero(A) (func_is_zero(A))
#define Is_really_zero(A) (func_is_really_zero(A))
#else
#define Is_zero(A) (Is_zero_macro(A))
#define Is_really_zero(A) (Is_really_zero_macro(A))
#endif // ZERO_CHECKS_ARE_FUNCTIONS

static inline int func_is_zero(const double a) Pure_function;
static inline int func_is_really_zero(const double a) Pure_function;
static inline int func_is_zero(const double a){return(Is_zero_macro(a));}
static inline int func_is_really_zero(const double a){return(Is_really_zero_macro(a));}

/* derived macros */
#define Is_not_zero(A) (!(Is_zero((A))))
#define Is_really_not_zero(A) (!(Is_really_zero((A))))
#define Fequal(A,B) (Is_zero((A)-(B)))
#define More_or_equal(A,B) (((A)>=(B))||(Fequal((A),(B))))
#define Less_or_equal(A,B) More_or_equal((B),(A))
#define Neither_is_zero(A,B) (Is_not_zero(A) && \
                              Is_not_zero(B))
#define Neither_is_really_zero(A,B) (Is_really_not_zero(A) &&   \
                                     Is_really_not_zero(B))

// alternative > and < for fabs(x)<y
#define Abs_more_than(A,B) (((A)>(B))||(-(A)>(B)))
#define Abs_more_than_or_equal(A,B) (Abs_more_than((A),(B))||Fequal((A),(B)))
#define Abs_less_than(A,B) ((-(B)<(A))&&((A)<(B)))
#define Abs_less_than_or_equal(A,B) (Abs_less_than((A),(B))||Fequal((A),(B)))

#endif//USE_FABS

/* find the sign of a number */
#define Sign_macro(X) (Is_zero(X) ? +1.0 : ((X)/fabs(X)))
#define Sign_is_really_macro(X) (Is_really_zero(X) ? +1.0 : ((X)/fabs(X)))

/*
 * Sign macros, but give 0 when X is zero
 */
#define Sign_int_macro(X) ((int)(Is_zero(X) ? 0 : (((X)/fabs(X)) > 0.0 ? +1 : -1)))
#define Sign_is_really_int_macro(X) ((int)(Is_really_zero(X) ? 0 : (((X)/fabs(X)) > 0.0 ? +1 : -1)))

#ifdef USE_GCC_EXTENSIONS
#define Sign_implementation(A,LABEL,COUNTER)            \
    __extension__                                       \
    ({                                                  \
        __typeof__(A) Concat3(__a,LABEL,COUNTER) = (A); \
        Sign_macro(Concat3(__a,LABEL,COUNTER));         \
    })
#define Sign(A) Sign_implementation((A),            \
                                    Sign,           \
                                    __COUNTER__)
#else
#define Sign(A) Sign_macro(A)
#endif // USE_GCC_EXTENSIONS

#ifdef USE_GCC_EXTENSIONS
#define Sign_is_really_implementation(A,LABEL,COUNTER)      \
    __extension__                                           \
    ({                                                      \
        __typeof__(A) Concat3(__a,LABEL,COUNTER) = (A);     \
        Sign_is_really_macro(Concat3(__a,LABEL,COUNTER));   \
    })
#define Sign_is_really(A) Sign_is_really_implementation(    \
        (A),                                                \
        Sign_is_really,                                     \
        __COUNTER__)
#else
#define Sign_is_really(A) Sign_is_really_macro(A)
#endif // USE_GCC_EXTENSIONS

#ifdef USE_GCC_EXTENSIONS
#define Sign_int_implementation(A,LABEL,COUNTER)        \
    __extension__                                       \
    ({                                                  \
        __typeof__(A) Concat3(__a,LABEL,COUNTER) = (A); \
        Sign_int_macro(Concat3(__a,LABEL,COUNTER));     \
    })
#define Sign_int(A) Sign_int_implementation(A)
#else
#define Sign_int(A) Sign_int_macro(A)
#endif // USE_GCC_EXTENSIONS

#ifdef USE_GCC_EXTENSIONS
#define Sign_is_really_int_implementation(A,LABEL,COUNTER)      \
    __extension__                                               \
    ({                                                          \
        __typeof__(A) Concat3(__a,LABEL,COUNTER) = (A);         \
        Sign_is_really_int_macro(Concat3(__a,                   \
                                         LABEL,                 \
                                         COUNTER));             \
    })
#define Sign_is_really_int(A)                                       \
    Sign_is_really_int_implementation(                              \
        (A),                                                        \
        Sign_int_is_really,                                         \
        __COUNTER__)
#else
#define Sign_is_really_int(A) Sign_is_really_int_macro(A);
#endif // USE_GCC_EXTENSIONS


/* range macros */
#define In_range_macro(A,B,C) ((More_or_equal((A),(B)))&&(Less_or_equal((A),(C))))

#ifdef USE_GCC_EXTENSIONS
#define In_range_implementation(A,B,C,LABEL,COUNTER)    \
    __extension__                                       \
    ({                                                  \
        __typeof__(A) Concat3(__a,LABEL,COUNTER) = (A); \
        __typeof__(B) Concat3(__b,LABEL,COUNTER) = (B); \
        __typeof__(C) Concat3(__c,LABEL,COUNTER) = (C); \
        In_range_macro(Concat3(__a,LABEL,COUNTER),      \
                       Concat3(__b,LABEL,COUNTER),      \
                       Concat3(__c,LABEL,COUNTER));     \
    })
#define In_range(A,B,C) In_range_implementation(    \
        (A),                                        \
        (B),                                        \
        (C),                                        \
        In_range,                                   \
        __COUNTER__)
#else
#define In_range(A) In_range_macro((A),(B),(C));
#endif // USE_GCC_EXTENSIONS

/* Return A limited to be in the range [B,C] */
#define Limit_range(A,B,C) (Max((B),Min((C),(A))))
/* Clamp is as Limit_range but also sets A */
#define Clamp(A,B,C)       ((A)=(Limit_range((A),(B),(C))))

/* Pythagoras in 2D or 3D */
#define Pythag2_squared(X,Y) ((Pow2(X) + Pow2(Y)))
#define Pythag2(X,Y) (sqrt(Pythag2_squared((X),(Y))))
#define Pythag3_squared(X,Y,Z) ((Pow2(X) + Pow2(Y) + Pow2(Z)))
#define Pythag3(X,Y,Z) (sqrt(Pythag3_squared((X),(Y),(Z))))
/* ditto with coordinate structs */
#define Pythag2_coord(C) (sqrt(Pow2((C).x) + Pow2((C).y)))
#define Pythag3_coord(C) (sqrt(Pow2((C).x) + Pow2((C).y) + Pow2((C).z)))

/*
 * Multiply-add : Fma(X,Y,Z) = X*Y+Z
 */
#ifdef FP_FAST_FMA
#define Fma(A,X,B) fma((A),(X),(B))
#else
#define Fma(A,X,B) ((X)*(A)+(B))
#endif // FP_FAST_FMA

/*
 * power macros
 *
 * p means point, e.g. 1p5 = 1.5 = 3/2
 * d means divide, e.g. 1d4 = 1/4 = 0.25
 */
#define _Pow2_macro(X) ((X)*(X))
#define _Pow3_macro(X) ((X)*(X)*(X))
#define _Pow4_macro(X) ((X)*(X)*(X)*(X))
#define _Pow5_macro(X) ((X)*(X)*(X)*(X)*(X))
#define _Pow6_macro(X) ((X)*(X)*(X)*(X)*(X)*(X))
#define _Pow7_macro(X) ((X)*(X)*(X)*(X)*(X)*(X)*(X))
#define _Pow8_macro(X) ((X)*(X)*(X)*(X)*(X)*(X)*(X)*(X))
#define _Pow9_macro(X) ((X)*(X)*(X)*(X)*(X)*(X)*(X)*(X)*(X))
#define _Pow1p5_macro(X) ((X)*sqrt(X))
#define _Pow2p5_macro(X) ((X)*(X)*sqrt(X))
#define _Pow3p5_macro(X) ((X)*(X)*(X)*sqrt(X))
#define _Pow1d4_macro(X) (sqrt(sqrt(X)))
#define _Pow5d4_macro(X) ((X)*sqrt(sqrt(X)))
#define _Pow5p5_macro(X) (_Pow5_macro(X)*sqrt(X))
#define _Pow3d4_macro(X) ((X)/(_Pow1d4_macro(X)))
#define _Pow2d3_macro(X) (_Pow2_macro(cbrt(X)))
#define _Pow4d3_macro(X) (_Pow4_macro(cbrt(X)))


#ifdef POWER_OPERATIONS_ARE_FUNCTIONS
/* use functions for simple power operations */
#define Pow2(A) func_pow2(A)
#define Pow3(A) func_pow3(A)
#define Pow4(A) func_pow4(A)
#define Pow5(A) func_pow5(A)
#define Pow6(A) func_pow6(A)
#define Pow7(A) func_pow7(A)
#define Pow8(A) func_pow8(A)
#define Pow9(A) func_pow9(A)
#define Pow2p5(A) func_pow2p5(A)
#define Pow3p5(A) func_pow3p5(A)
#define Pow1p5(A) func_pow1p5(A)
#define Pow1d4(A) func_pow1d4(A)
#define Pow5d4(A) func_pow5d4(A)
#define Pow1p5(A) func_pow1p5(A)
#define Pow5p5(A) func_pow5p5(A)
#define Pow3d4(A) func_pow3d4(A)
#define Pow2d3(A) func_pow2d3(A)
#define Pow4d3(A) func_pow4d3(A)

#define Power_function static inline double Constant_function

Power_function func_pow2(const double x);
Power_function func_pow3(const double x);
Power_function func_pow4(const double x);
Power_function func_pow5(const double x);
Power_function func_pow6(const double x);
Power_function func_pow7(const double x);
Power_function func_pow8(const double x);
Power_function func_pow9(const double x);
Power_function func_pow2p5(const double x);
Power_function func_pow3p5(const double x);
Power_function func_pow1d4(const double x);
Power_function func_pow5d4(const double x);
Power_function func_pow5p5(const double x);
Power_function func_pow3d4(const double x);
Power_function func_pow2d3(const double x);
Power_function func_pow4d3(const double x);
Power_function func_pow2(const double x) {return _Pow2_macro(x);}
Power_function func_pow3(const double x) {return _Pow3_macro(x);}
Power_function func_pow4(const double x) {return _Pow4_macro(x);}
Power_function func_pow5(const double x) {return _Pow5_macro(x);}
Power_function func_pow6(const double x) {return _Pow6_macro(x);}
Power_function func_pow7(const double x) {return _Pow7_macro(x);}
Power_function func_pow8(const double x) {return _Pow8_macro(x);}
Power_function func_pow9(const double x) {return _Pow8_macro(x);}
Power_function func_pow1p5(const double x) {return _Pow1p5_macro(x);}
Power_function func_pow2p5(const double x) {return _Pow2p5_macro(x);}
Power_function func_pow3p5(const double x) {return _Pow3p5_macro(x);}
Power_function func_pow1d4(const double x) {return _Pow1d4_macro(x);}
Power_function func_pow5d4(const double x) {return _Pow5d4_macro(x);}
Power_function func_pow5p5(const double x) {return _Pow5p5_macro(x);}
Power_function func_pow3d4(const double x) {return _Pow3d4_macro(x);}
Power_function func_pow2d3(const double x) {return _Pow2d3_macro(x);}
Power_function func_pow4d3(const double x) {return _Pow4d3_macro(x);}
#else
/* use macros for simple power operations */

#ifdef USE_GCC_EXTENSIONS
#define Pow2_implementation(A,LABEL,COUNTER)            \
    __extension__                                       \
    ({                                                  \
        __typeof__(A) Concat3(__a,LABEL,COUNTER) = (A); \
        _Pow2_macro(Concat3(__a,LABEL,COUNTER));        \
    })
#define Pow2(A) Pow2_implementation(            \
        (A),                                    \
        Pow2,                                   \
        __COUNTER__)
#else
#define Pow2(A) _Pow2_macro(A);
#endif // USE_GCC_EXTENSIONS

#ifdef USE_GCC_EXTENSIONS
#define Pow3_implementation(A,LABEL,COUNTER)            \
    __extension__                                       \
    ({                                                  \
        __typeof__(A) Concat3(__a,LABEL,COUNTER) = (A); \
        _Pow3_macro(Concat3(__a,LABEL,COUNTER));        \
    })
#define Pow3(A) Pow3_implementation(            \
        (A),                                    \
        Pow3,                                   \
        __COUNTER__)
#else
#define Pow3(A) _Pow3_macro(A);
#endif // USE_GCC_EXTENSIONS

#ifdef USE_GCC_EXTENSIONS
#define Pow4_implementation(A,LABEL,COUNTER)            \
    __extension__                                       \
    ({                                                  \
        __typeof__(A) Concat3(__a,LABEL,COUNTER) = (A); \
        _Pow4_macro(Concat3(__a,LABEL,COUNTER));        \
    })
#define Pow4(A) Pow4_implementation(            \
        (A),                                    \
        Pow4,                                   \
        __COUNTER__)
#else
#define Pow4(A) _Pow4_macro(A);
#endif // USE_GCC_EXTENSIONS

#ifdef USE_GCC_EXTENSIONS
#define Pow5_implementation(A,LABEL,COUNTER)            \
    __extension__                                       \
    ({                                                  \
        __typeof__(A) Concat3(__a,LABEL,COUNTER) = (A); \
        _Pow5_macro(Concat3(__a,LABEL,COUNTER));        \
    })
#define Pow5(A) Pow5_implementation(            \
        (A),                                    \
        Pow5,                                   \
        __COUNTER__)
#else
#define Pow5(A) _Pow5_macro(A);
#endif // USE_GCC_EXTENSIONS

#ifdef USE_GCC_EXTENSIONS
#define Pow6_implementation(A,LABEL,COUNTER)            \
    __extension__                                       \
    ({                                                  \
        __typeof__(A) Concat3(__a,LABEL,COUNTER) = (A); \
        _Pow6_macro(Concat3(__a,LABEL,COUNTER));        \
    })
#define Pow6(A) Pow6_implementation(            \
        (A),                                    \
        Pow6,                                   \
        __COUNTER__)
#else
#define Pow6(A) _Pow6_macro(A);
#endif // USE_GCC_EXTENSIONS

#ifdef USE_GCC_EXTENSIONS
#define Pow7_implementation(A,LABEL,COUNTER)            \
    __extension__                                       \
    ({                                                  \
        __typeof__(A) Concat3(__a,LABEL,COUNTER) = (A); \
        _Pow7_macro(Concat3(__a,LABEL,COUNTER));        \
    })
#define Pow7(A) Pow7_implementation(            \
        (A),                                    \
        Pow7,                                   \
        __COUNTER__)
#else
#define Pow7(A) _Pow7_macro(A);
#endif // USE_GCC_EXTENSIONS

#ifdef USE_GCC_EXTENSIONS
#define Pow8_implementation(A,LABEL,COUNTER)            \
    __extension__                                       \
    ({                                                  \
        __typeof__(A) Concat3(__a,LABEL,COUNTER) = (A); \
        _Pow8_macro(Concat3(__a,LABEL,COUNTER));        \
    })
#define Pow8(A) Pow8_implementation(            \
        (A),                                    \
        Pow8,                                   \
        __COUNTER__)
#else
#define Pow8(A) _Pow8_macro(A);
#endif // USE_GCC_EXTENSIONS

#ifdef USE_GCC_EXTENSIONS
#define Pow9_implementation(A,LABEL,COUNTER)            \
    __extension__                                       \
    ({                                                  \
        __typeof__(A) Concat3(__a,LABEL,COUNTER) = (A); \
        _Pow9_macro(Concat3(__a,LABEL,COUNTER));        \
    })
#define Pow9(A) Pow9_implementation(            \
        (A),                                    \
        Pow9,                                   \
        __COUNTER__)
#else
#define Pow9(A) _Pow9_macro(A);
#endif // USE_GCC_EXTENSIONS

#ifdef USE_GCC_EXTENSIONS
#define Pow2p5_implementation(A,LABEL,COUNTER)            \
    __extension__                                       \
    ({                                                  \
        __typeof__(A) Concat3(__a,LABEL,COUNTER) = (A); \
        _Pow2p5_macro(Concat3(__a,LABEL,COUNTER));        \
    })
#define Pow2p5(A) Pow2p5_implementation(            \
        (A),                                    \
        Pow2p5,                                   \
        __COUNTER__)
#else
#define Pow2p5(A) _Pow2p5_macro(A);
#endif // USE_GCC_EXTENSIONS

#ifdef USE_GCC_EXTENSIONS
#define Pow3p5_implementation(A,LABEL,COUNTER)            \
    __extension__                                       \
    ({                                                  \
        __typeof__(A) Concat3(__a,LABEL,COUNTER) = (A); \
        _Pow3p5_macro(Concat3(__a,LABEL,COUNTER));        \
    })
#define Pow3p5(A) Pow3p5_implementation(            \
        (A),                                    \
        Pow3p5,                                   \
        __COUNTER__)
#else
#define Pow3p5(A) _Pow3p5_macro(A);
#endif // USE_GCC_EXTENSIONS

#ifdef USE_GCC_EXTENSIONS
#define Pow1p5_implementation(A,LABEL,COUNTER)            \
    __extension__                                       \
    ({                                                  \
        __typeof__(A) Concat3(__a,LABEL,COUNTER) = (A); \
        _Pow1p5_macro(Concat3(__a,LABEL,COUNTER));        \
    })
#define Pow1p5(A) Pow1p5_implementation(            \
        (A),                                    \
        Pow1p5,                                   \
        __COUNTER__)
#else
#define Pow1p5(A) _Pow1p5_macro(A);
#endif // USE_GCC_EXTENSIONS

#ifdef USE_GCC_EXTENSIONS
#define Pow1p5_implementation(A,LABEL,COUNTER)            \
    __extension__                                       \
    ({                                                  \
        __typeof__(A) Concat3(__a,LABEL,COUNTER) = (A); \
        _Pow1p5_macro(Concat3(__a,LABEL,COUNTER));        \
    })
#define Pow1p5(A) Pow1p5_implementation(            \
        (A),                                    \
        Pow1p5,                                   \
        __COUNTER__)
#else
#define Pow1p5(A) _Pow1p5_macro(A);
#endif // USE_GCC_EXTENSIONS

#ifdef USE_GCC_EXTENSIONS
#define Pow2p5_implementation(A,LABEL,COUNTER)            \
    __extension__                                       \
    ({                                                  \
        __typeof__(A) Concat3(__a,LABEL,COUNTER) = (A); \
        _Pow2p5_macro(Concat3(__a,LABEL,COUNTER));        \
    })
#define Pow2p5(A) Pow2p5_implementation(            \
        (A),                                    \
        Pow2p5,                                   \
        __COUNTER__)
#else
#define Pow2p5(A) _Pow2p5_macro(A);
#endif // USE_GCC_EXTENSIONS

#ifdef USE_GCC_EXTENSIONS
#define Pow1d4_implementation(A,LABEL,COUNTER)            \
    __extension__                                       \
    ({                                                  \
        __typeof__(A) Concat3(__a,LABEL,COUNTER) = (A); \
        _Pow1d4_macro(Concat3(__a,LABEL,COUNTER));        \
    })
#define Pow1d4(A) Pow1d4_implementation(            \
        (A),                                    \
        Pow1d4,                                   \
        __COUNTER__)
#else
#define Pow1d4(A) _Pow1d4_macro(A);
#endif // USE_GCC_EXTENSIONS

#ifdef USE_GCC_EXTENSIONS
#define Pow5d4_implementation(A,LABEL,COUNTER)            \
    __extension__                                       \
    ({                                                  \
        __typeof__(A) Concat3(__a,LABEL,COUNTER) = (A); \
        _Pow5d4_macro(Concat3(__a,LABEL,COUNTER));        \
    })
#define Pow5d4(A) Pow5d4_implementation(            \
        (A),                                    \
        Pow5d4,                                   \
        __COUNTER__)
#else
#define Pow5d4(A) _Pow5d4_macro(A);
#endif // USE_GCC_EXTENSIONS

#ifdef USE_GCC_EXTENSIONS
#define Pow1p5_implementation(A,LABEL,COUNTER)            \
    __extension__                                       \
    ({                                                  \
        __typeof__(A) Concat3(__a,LABEL,COUNTER) = (A); \
        _Pow1p5_macro(Concat3(__a,LABEL,COUNTER));        \
    })
#define Pow1p5(A) Pow1p5_implementation(            \
        (A),                                    \
        Pow1p5,                                   \
        __COUNTER__)
#else
#define Pow1p5(A) _Pow1p5_macro(A);
#endif // USE_GCC_EXTENSIONS

#ifdef USE_GCC_EXTENSIONS
#define Pow5p5_implementation(A,LABEL,COUNTER)            \
    __extension__                                       \
    ({                                                  \
        __typeof__(A) Concat3(__a,LABEL,COUNTER) = (A); \
        _Pow5p5_macro(Concat3(__a,LABEL,COUNTER));        \
    })
#define Pow5p5(A) Pow5p5_implementation(            \
        (A),                                    \
        Pow5p5,                                   \
        __COUNTER__)
#else
#define Pow5p5(A) _Pow5p5_macro(A);
#endif // USE_GCC_EXTENSIONS

#ifdef USE_GCC_EXTENSIONS
#define Pow3d4_implementation(A,LABEL,COUNTER)            \
    __extension__                                       \
    ({                                                  \
        __typeof__(A) Concat3(__a,LABEL,COUNTER) = (A); \
        _Pow3d4_macro(Concat3(__a,LABEL,COUNTER));        \
    })
#define Pow3d4(A) Pow3d4_implementation(            \
        (A),                                    \
        Pow3d4,                                   \
        __COUNTER__)
#else
#define Pow3d4(A) _Pow3d4_macro(A);
#endif // USE_GCC_EXTENSIONS

#ifdef USE_GCC_EXTENSIONS
#define Pow2d3_implementation(A,LABEL,COUNTER)            \
    __extension__                                       \
    ({                                                  \
        __typeof__(A) Concat3(__a,LABEL,COUNTER) = (A); \
        _Pow2d3_macro(Concat3(__a,LABEL,COUNTER));        \
    })
#define Pow2d3(A) Pow2d3_implementation(            \
        (A),                                    \
        Pow2d3,                                   \
        __COUNTER__)
#else
#define Pow2d3(A) _Pow2d3_macro(A);
#endif // USE_GCC_EXTENSIONS

#ifdef USE_GCC_EXTENSIONS
#define Pow4d3_implementation(A,LABEL,COUNTER)            \
    __extension__                                       \
    ({                                                  \
        __typeof__(A) Concat3(__a,LABEL,COUNTER) = (A); \
        _Pow4d3_macro(Concat3(__a,LABEL,COUNTER));        \
    })
#define Pow4d3(A) Pow4d3_implementation(            \
        (A),                                    \
        Pow4d3,                                   \
        __COUNTER__)
#else
#define Pow4d3(A) _Pow4d3_macro(A);
#endif // USE_GCC_EXTENSIONS

#endif //POWER_OPERATIONS_ARE_FUNCTIONS

/*
 * Compare with DBL_MIN to make sure
 * numbers are non-zero but either positive
 * or negative
 */
#define SMALLEST_POSITIVE_NUMBER (DBL_MIN)
#define Positive_nonzero(A) ((A)>(SMALLEST_POSITIVE_NUMBER))
#define Negative_nonzero(A) ((A)<(-SMALLEST_POSITIVE_NUMBER))
#define Nonzero_macro(A) (Positive_nonzero(A)||Negative_nonzero(A))

#define Positive_or_zero(A) ((A)>-(SMALLEST_POSITIVE_NUMBER))
#define Negative_or_zero(A) ((A)<(SMALLEST_POSITIVE_NUMBER))


#ifdef USE_GCC_EXTENSIONS
#define Nonzero_implementation(A,LABEL,COUNTER)         \
    __extension__                                       \
    ({                                                  \
        __typeof__(A) Concat3(__a,LABEL,COUNTER) = (A); \
        Nonzero_macro(Concat3(__a,LABEL,COUNTER));      \
    })
#define Nonzero(A) Nonzero_implementation(          \
        (A),                                        \
        Nonzero,                                    \
        __COUNTER__)
#else
#define Nonzero(A) Nonzero_macro(A);
#endif // USE_GCC_EXTENSIONS

/*
 * Polynomial forms: X is a double or float here, so
 * we don't have to convert to gcc extensions
 */

/*
 * Horner's method for polynomials
 */
#define _Horner_Quadratic(X,A,B,C)     Fma(Fma(C,X,B),X,A)
#define _Horner_Cubic(X,A,B,C,D)       Fma(Fma(Fma(D,X,C),X,B),X,A)
#define _Horner_Quartic(X,A,B,C,D,E)   Fma(Fma(Fma(Fma(E,X,D),X,C),X,B),X,A)
#define _Horner_Quintic(X,A,B,C,D,E,F) Fma(Fma(Fma(Fma(Fma(F,X,E),X,D),X,C),X,B),X,A)

/*
 * Naive implementation
 */
#define _naive_Quadratic(X,A,B,C) (             \
        (A)+                                    \
        (B)*(X)+                                \
        (C)*Pow2(X)                             \
        )

#define _naive_Cubic(X,A,B,C,D) (               \
        Quadratic((X),(A),(B),(C)) +            \
        (D)*Pow3(X)                             \
        )

#define _naive_Quartic(X,A,B,C,D,E) (           \
        Cubic((X),(A),(B),(C),(D)) +            \
        (E)*Pow4(X)                             \
        )

#define _naive_Quintic(X,A,B,C,D,E,F) (         \
        Quartic((X),(A),(B),(C),(D),(E)) +      \
        (F)*Pow5(X)                             \
        )

/*
 * Choose whether to use Horner's or naive
 * method for polynomial computation.
 */
#ifdef HORNER_POLYNOMIALS
#define Quadratic(...) _Horner_Quadratic(__VA_ARGS__)
#define Cubic(...) _Horner_Cubic(__VA_ARGS__)
#define Quartic(...) _Horner_Quartic(__VA_ARGS__)
#define Quintic(...) _Horner_Quintic(__VA_ARGS__)
#else
#define Quadratic(...) _naive_Quadratic(__VA_ARGS__)
#define Cubic(...) _naive_Cubic(__VA_ARGS__)
#define Quartic(...) _naive_Quartic(__VA_ARGS__)
#define Quintic(...) _naive_Quintic(__VA_ARGS__)
#endif // HORNER_POLYNOMIALS

/* array versions of the polynomial macros */
#define Quadratic_array(X,A) Quadratic((X),(A)[0],(A)[1],(A)[2])
#define Cubic_array(X,A)     Cubic(    (X),(A)[0],(A)[1],(A)[2],(A)[3])
#define Quartic_array(X,A)   Quartic(  (X),(A)[0],(A)[1],(A)[2],(A)[3],(A)[4])
#define Quintic_array(X,A)   Quintic(  (X),(A)[0],(A)[1],(A)[2],(A)[3],(A)[4],(A)[5])
/*
 * Macro to evalutate Y if X is nonzero
 */
#define Eval_if_nonzero(X,Y) (Is_not_zero(X) ? (Y) : 0.0)

/*
 * "Safe" exp10(x) which first checks if x is NaN, if it
 * is, do nothing
 */
#define Safepow10_macro(X) (isnan(X) ? (X) : (exp10(X))))
#ifdef USE_GCC_EXTENSIONS
#define Safepow10_implementation(A,LABEL,COUNTER)       \
    __extension__                                       \
    ({                                                  \
        __typeof__(A) Concat3(__a,LABEL,COUNTER) = (A); \
        Safepow10_macro(Concat3(__a,LABEL,COUNTER));    \
    })
#define Safepow10(A) Safepow10_implementation(  \
        (A),                                    \
        Safepow10,                              \
        __COUNTER__)
#else
#define Safepow10(A) Safepow10_macro(A);
#endif // USE_GCC_EXTENSIONS

/*
 * "Safe(r)" log10(x) : either make the argument
 * larger than TINY or REALLY_TINY.
 */
#define Safelog10(X) (log10(Max((X), (TINY))))
#define Saferlog10(X) (log10(Max((X), (REALLY_TINY))))

/*
 * Macro to check if X is a non-zero number
 */
#define Is_nonzero_number_macro(X)              \
    ((Is_really_not_zero(X) &&                  \
      !isnan(X) && !isinf(X)))
#ifdef USE_GCC_EXTENSIONS
#define Is_nonzero_number_implementation(A,         \
                                         LABEL,     \
                                         COUNTER)   \
    __extension__                                   \
    ({                                              \
        __typeof__(A) Concat3(__a,                  \
                              LABEL,                \
                              COUNTER) = (A);       \
        Is_nonzero_number_macro(Concat3(__a,        \
                                        LABEL,      \
                                        COUNTER));  \
    })
#define Is_nonzero_number(A) Is_nonzero_number_implementation(  \
        (A),                                                    \
        Is_nonzero_number,                                      \
        __COUNTER__)
#else
#define Is_nonzero_number(A) Is_nonzero_number_macro(A);
#endif // USE_GCC_EXTENSIONS


/*
 * States for the differential equation solver:
 * can be forced to be positive
 */
#define SOLUTION_CAN_HAVE_ANY_SIGN (FALSE)
#define SOLUTION_MUST_BE_POSITIVE (TRUE)


#endif // BINARY_MATHS_H
