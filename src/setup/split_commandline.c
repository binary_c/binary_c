#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Code from
 * http://stackoverflow.com/questions/1706551/parse-string-into-argv-argc
 */
#ifdef WINDOWS
#include <windows.h>
#else
#include <wordexp.h>
#endif
#include <stdlib.h>
#include <string.h>
#include <assert.h>

char ** split_commandline(const char * Restrict const cmdline,
                          int * Restrict const argc)
{
    if (!cmdline) return NULL;
    int i;
    char **argv = NULL;
    assert(argc);

    // Posix.
#ifndef WINDOWS
    {
        wordexp_t p;

        // Note! This expands shell variables.
        int r = wordexp(cmdline, &p, 0);
        if(r)
        {
            fprintf(stderr,
                    "wordexp failed %d : %s\n",
                    r,
                    r == WRDE_BADCHAR ? "Illegal character in argument string (check fornewlines or |, &, ;, <, >, (, ), {, })" :
                    r == WRDE_BADVAL ? "Bad value" :
                    r == WRDE_CMDSUB ? "Command sub" :
                    r == WRDE_NOSPACE ? "No space" :
                    r == WRDE_SYNTAX ? "Syntax" :
                    "Unknown"
                );
            fprintf(stderr,"cmdline is %s\n",cmdline);
            return NULL;
        }

        *argc = (int)p.we_wordc;

        if(!(argv = Calloc(*argc, sizeof(char *))))
        {
            printf("fail 1\n");
            goto fail;
        }

        for(i = 0; i < (int)p.we_wordc; i++)
        {
            if(!(argv[i] = strdup(p.we_wordv[i])))
            {
                printf("fail 2\n");
                goto fail;
            }
        }

        wordfree(&p);

        return argv;
    fail:
        wordfree(&p);
    }
#else // WINDOWS
    {
        wchar_t **wargs = NULL;
        size_t needed = 0;
        wchar_t *cmdlinew = NULL;
        size_t len = strlen(cmdline) + 1;

        if(!(cmdlinew = Calloc(len, sizeof(wchar_t))))
            goto fail;

        if(!MultiByteToWideChar(CP_ACP, 0, cmdline, -1, cmdlinew, len))
            goto fail;

        if(!(wargs = CommandLineToArgvW(cmdlinew, argc)))
            goto fail;

        if(!(argv = Calloc(*argc, sizeof(char *))))
            goto fail;

        // Convert from wchar_t * to ANSI char *
        for(i = 0; i < *argc; i++)
        {
            // Get the size needed forthe target buffer.
            // CP_ACP = Ansi Codepage.
            needed = WideCharToMultiByte(CP_ACP, 0, wargs[i], -1,
                                         NULL, 0, NULL, NULL);

            if(!(argv[i] = Malloc(needed)))
                goto fail;

            // Do the conversion.
            needed = WideCharToMultiByte(CP_ACP, 0, wargs[i], -1,
                                         argv[i], needed, NULL, NULL);
        }

        if(wargs) LocalFree(wargs);
        if(cmdlinew) free(cmdlinew);
        return argv;

    fail:
        if(wargs) LocalFree(wargs);
        if(cmdlinew) free(cmdlinew);
    }
#endif// WINDOWS

    if(argv)
    {
        for(i = 0; i < *argc; i++)
        {
            if(argv[i])
            {
                free(argv[i]);
            }
        }

        free(argv);
    }

    return NULL;
}
