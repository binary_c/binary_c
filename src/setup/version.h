#pragma once
#ifndef VERSION_H
#define VERSION_H

#define BOOLEAN_TEST_LIST_TRUE \
    X(    "T")                 \
    X(    "t")                 \
    X( "TRUE")                 \
    X( "True")                 \
    X( "true")                 \
    X( "truE")                 \
    X(   "on")                 \
    X(   "On")                 \
    X(   "ON")                 \
    X(    "1")

#define BOOLEAN_TEST_LIST_FALSE \
    X(     "F")                 \
    X(     "f")                 \
    X( "FALSE")                 \
    X( "False")                 \
    X( "falsE")                 \
    X(   "off")                 \
    X(   "Off")                 \
    X(   "OFF")                 \
    X(     "0")                 \
    X(    "01")                 \
    X(    "10")

#endif //VERSION_H
