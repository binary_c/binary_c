#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef __DEPRECATED
#define _Strides X(4) X(8) X(12) X(16) X(20) X(24) X(32)
#undef X
#define X(N)                                                        \
    static Abundance _strided_Xsum##N (const Abundance * const X);
_Strides
#endif // __DEPRECATED
#include <limits.h>     /* for CHAR_BIT */
#include <sys/resource.h>
#undef __HAVE_LIBBSD__
#ifdef __HAVE_LIBBSD__
#include <bsd/bsd.h>
#endif // __HAVE_LIBBSD__
#include "control/loop_unroller.h"
#include "../setup/defaults_sets.h"
#include "version.h"

#define BITMASK(b) (1 << ((b) % CHAR_BIT))
#define BITSLOT(b) ((b) / CHAR_BIT)
#define BITSET(a, b) ((a)[BITSLOT(b)] |= BITMASK(b))
#define BITCLEAR(a, b) ((a)[BITSLOT(b)] &= ~BITMASK(b))
#define BITTEST(a, b) ((a)[BITSLOT(b)] & BITMASK(b))
#define BITNSLOTS(nb) ((nb + CHAR_BIT - 1) / CHAR_BIT)

static char * bitset;
static unsigned long int __test_milliseconds(void);
static Pure_function inline Boolean is_exponent_char1(const unsigned char c);
static Pure_function inline Boolean is_exponent_char2(const unsigned char c);
static Pure_function inline Boolean is_exponent_char3(const unsigned char c);

void version_speedtests(struct stardata_t * Restrict const stardata)
{
    /*
     * Speed tests
     */
#if defined TIMER && defined CPUFREQ && defined millisecond
    /*
     * nms is the number of milliseconds for which
     * each speed test is carried out.
     *
     * This defaults to 100ms, but can be set using the
     * NMS environment variable.
     */
    const long int nms = __test_milliseconds();
    Printf("Speed tests for %ld milliseconds\n", nms);
    fflush(stdout);


    /*
     * Time function call using counter.
     * Usage:
     * Timeme(n,{...code block...});
     */
#define Time_me_for_ms(counter,_Nms,...)        \
    {                                           \
        ticks t1 = getticks();                  \
        if(t1!=0)                               \
        {                                       \
            t1 += (ticks)(_Nms * millisecond);  \
            while(getticks()<t1)                \
            {                                   \
                {                               \
                    __VA_ARGS__;                \
                }                               \
                (counter)++;                    \
            }                                   \
        }                                       \
    }
#define Timeme(counter,...)                     \
    {                                           \
        ticks t1 = getticks();                  \
        if(t1!=0)                               \
        {                                       \
            t1 += (ticks)(nms * millisecond);   \
            while(getticks()<t1)                \
            {                                   \
                {                               \
                    __VA_ARGS__;                \
                }                               \
                (counter)++;                    \
            }                                   \
        }                                       \
    }
#define Speed_increase_pc(n1,n2)                \
    100.0*(1.0/n1 - 1.0/n2)/(1.0/Max(n1,n2))

    /*
     * Time array operations
     */
#ifdef NUCSYN
    {
        Abundance * X = New_isotope_array;
        Abundance Xsummed[20] = {0.0};
#define _randomX                                    \
        for(Isotope i=0;i<ISOTOPE_ARRAY_SIZE;i++)   \
        {                                           \
            X[i] = random_number(stardata, NULL);   \
        }
        _randomX;

        const size_t ntests = 20;
        int count[20] = {0};
        char * test[20] = {0};
        int testn = 0;

        /*
         * Standard linear-sum test
         */
        test[0] = "Standard linear sum";
        Timeme(count[0])
        {
            Abundance Xsum = 0.0;
            for(Isotope i=0;i<ISOTOPE_ARRAY_SIZE;i++)
            {
                Xsum += X[i];
            }
            Xsummed[testn++] = Xsum;
        }

        /*
         * Strided-sum tests
         */
#define _Stride_test(N)                                                 \
        {                                                               \
            _randomX;                                                   \
            test[testn] = "Unrolled_loop "#N;                           \
            Timeme(count[testn])                                        \
            {                                                           \
                Xsummed[testn] = Unrolled_loop_pointer(X,ISOTOPE_ARRAY_SIZE,N,i,,+=X[i],,+); \
            }                                                           \
            testn++;                                                    \
        }

#define _strides X(1) X(2) X(3) X(4) X(6) X(8) X(10) X(12) X(16) X(20) X(24) X(28) X(32) X(48) X(64) X(128) X(256) X(512)
#undef X
#define X(N) _Stride_test(N)
        _strides;

        /*
         * Output results
         */
        {
            int best = 0;
            printf("%40s : %20s : %s\n","Test","count (high good)","sum value");
            for(size_t i=0;i<ntests;i++)
            {
                best = Max(count[i],best);
            }
            for(size_t i=0;i<ntests;i++)
            {
                if(count[i] > 0)
                {
                    printf("%40s : %20d : % 8.2f : %g\n",test[i],count[i],100.0*(float)count[i]/(float)best,Xsummed[i]);
                }
            }
        }

#ifdef __DEPRECATED
        Abundance Xsummed[ntests] = {0.0};



        {
            const ticks start_tick = getticks();
            test[++testn] = "Stride 4 sum";
            while (Timer_microseconds(getticks() - start_tick) < (nms * 1e3))
            {
                Abundance Xsum[4] = {0.0};
                const Isotope max = ((unsigned int)ISOTOPE_ARRAY_SIZE/4) * 4;
                for(Isotope i=0;i<max;)
                {
                    Xsum[0] += X[i++];
                    Xsum[1] += X[i++];
                    Xsum[2] += X[i++];
                    Xsum[3] += X[i++];
                }
                for(Isotope i=max;i<ISOTOPE_ARRAY_SIZE;)
                {
                    Xsum[0] += X[i++];
                }
                Xsummed[testn] = Xsum[0] + Xsum[1] + Xsum[2] + Xsum[3];
                count[testn]++;
            }
        }

        {
            const ticks start_tick = getticks();
            test[++testn] = "Stride 8 sum";
            while (Timer_microseconds(getticks() - start_tick) < (nms * 1e3))
            {
                Abundance Xsum[8] = {0.0};
                const Isotope max = ((unsigned int)ISOTOPE_ARRAY_SIZE/8) * 8;
                for(Isotope i=0;i<max;)
                {
                    Xsum[0] += X[i++];
                    Xsum[1] += X[i++];
                    Xsum[2] += X[i++];
                    Xsum[3] += X[i++];
                    Xsum[4] += X[i++];
                    Xsum[5] += X[i++];
                    Xsum[6] += X[i++];
                    Xsum[7] += X[i++];
                }
                for(Isotope i=max;i<ISOTOPE_ARRAY_SIZE;i++)
                {
                    Xsum[0] += X[i];
                }
                Xsummed[testn] = Xsum[0] + Xsum[1] + Xsum[2] + Xsum[3] + Xsum[4] + Xsum[5] + Xsum[6] + Xsum[7];
                count[testn]++;
            }
        }

        {
            const ticks start_tick = getticks();
            test[++testn] = "Stride 16 sum";
            while (Timer_microseconds(getticks() - start_tick) < (nms * 1e3))
            {
                Abundance Xsum[16] = {0.0};
                const Isotope max = ((unsigned int)ISOTOPE_ARRAY_SIZE/16) * 16;
                for(Isotope i=0;i<max;)
                {
                    Xsum[0] += X[i++];
                    Xsum[1] += X[i++];
                    Xsum[2] += X[i++];
                    Xsum[3] += X[i++];
                    Xsum[4] += X[i++];
                    Xsum[5] += X[i++];
                    Xsum[6] += X[i++];
                    Xsum[7] += X[i++];
                    Xsum[8] += X[i++];
                    Xsum[9] += X[i++];
                    Xsum[10] += X[i++];
                    Xsum[11] += X[i++];
                    Xsum[12] += X[i++];
                    Xsum[13] += X[i++];
                    Xsum[14] += X[i++];
                    Xsum[15] += X[i++];
                }
                for(Isotope i=max;i<ISOTOPE_ARRAY_SIZE;i++)
                {
                    Xsum[0] += X[i];
                }
                Xsummed[testn] = Xsum[0] + Xsum[1] + Xsum[2] + Xsum[3] + Xsum[4] + Xsum[5] + Xsum[6] + Xsum[7] + Xsum[8] + Xsum[9] + Xsum[10] + Xsum[11] + Xsum[12] + Xsum[13] + Xsum[14] + Xsum[15];
                count[testn]++;
            }
        }


        {
            const ticks start_tick = getticks();
            test[++testn] = "Stride 32 sum";
            while (Timer_microseconds(getticks() - start_tick) < (nms * 1e3))
            {
                Abundance Xsum[32] = {0.0};
                const Isotope max = ((unsigned int)ISOTOPE_ARRAY_SIZE/32) * 32;
                for(Isotope i=0;i<max;)
                {
                    Xsum[0] += X[i++];
                    Xsum[1] += X[i++];
                    Xsum[2] += X[i++];
                    Xsum[3] += X[i++];
                    Xsum[4] += X[i++];
                    Xsum[5] += X[i++];
                    Xsum[6] += X[i++];
                    Xsum[7] += X[i++];
                    Xsum[8] += X[i++];
                    Xsum[9] += X[i++];
                    Xsum[10] += X[i++];
                    Xsum[11] += X[i++];
                    Xsum[12] += X[i++];
                    Xsum[13] += X[i++];
                    Xsum[14] += X[i++];
                    Xsum[15] += X[i++];
                    Xsum[16] += X[i++];
                    Xsum[17] += X[i++];
                    Xsum[18] += X[i++];
                    Xsum[19] += X[i++];
                    Xsum[20] += X[i++];
                    Xsum[21] += X[i++];
                    Xsum[22] += X[i++];
                    Xsum[23] += X[i++];
                    Xsum[24] += X[i++];
                    Xsum[25] += X[i++];
                    Xsum[26] += X[i++];
                    Xsum[27] += X[i++];
                    Xsum[28] += X[i++];
                    Xsum[29] += X[i++];
                    Xsum[30] += X[i++];
                    Xsum[31] += X[i++];
                }
                for(Isotope i=max;i<ISOTOPE_ARRAY_SIZE;i++)
                {
                    Xsum[0] += X[i];
                }
                Xsummed[testn] = Xsum[0] + Xsum[1] + Xsum[2] + Xsum[3] + Xsum[4] + Xsum[5] + Xsum[6] + Xsum[7] + Xsum[8] + Xsum[9] + Xsum[10] + Xsum[11] + Xsum[12] + Xsum[13] + Xsum[14] + Xsum[15] + Xsum[16] + Xsum[17] + Xsum[18] + Xsum[19] + Xsum[20] + Xsum[21] + Xsum[22] + Xsum[23] + Xsum[24] + Xsum[25] + Xsum[26] + Xsum[27] + Xsum[28] + Xsum[29] + Xsum[30] + Xsum[31];
                count[testn]++;
            }
        }

#define _Stride_test(N)                                                 \
        {                                                               \
            const ticks start_tick = getticks();                        \
            test[++testn] = "Stride " #N " sum function";               \
            while (Timer_microseconds(getticks() - start_tick) < (nms * 1e3)) \
            {                                                           \
                Xsummed[testn] = _strided_Xsum##N  (X);                 \
                count[testn]++;                                         \
            }                                                           \
        }
#undef X
#define X(N) _Stride_test(N)
        _Strides;


        {
            const ticks start_tick = getticks();
            test[++testn] = "Standard sum, two var / end ptr";
            while (Timer_microseconds(getticks() - start_tick) < (nms * 1e3))
            {
                Abundance Xsum = 0.0;
                Abundance * Xp = X;
                for(const Abundance * const Xend = X + ISOTOPE_ARRAY_SIZE; Xp!=Xend; Xp++)
                {
                    Xsum += *Xp;
                }
                Xsummed[testn] = Xsum;
                count[testn]++;
            }
        }

        {
            const ticks start_tick = getticks();
            test[++testn] = "Stride 4 sum, two var / end ptr";
            while (Timer_microseconds(getticks() - start_tick) < (nms * 1e3))
            {
                const Isotope max = ((unsigned int)ISOTOPE_ARRAY_SIZE/4) * 4;
                Abundance * Xp = X;
                Abundance Xsum[4] = {0.0};
                for(const Abundance * const Xnearend = X + max; Xp!=Xnearend;)
                {
                    Xsum[0] += *Xp++;
                    Xsum[1] += *Xp++;
                    Xsum[2] += *Xp++;
                    Xsum[3] += *Xp++;
                }
                {
                    const Abundance * Xend = X + ISOTOPE_ARRAY_SIZE;
                    while(Xp != Xend)
                    {
                        Xsum[0] += *Xp++;
                    }
                }
                Xsummed[testn] = Xsum[0] + Xsum[1] + Xsum[2] + Xsum[3];
                count[testn]++;
            }
        }

        printf("%40s : %20s : %s\n","Test","count (high good)","sum value");
        for(int i=0;i<ntests;i++)
        {
            if(count[i] > 0)
            {
                printf("%40s : %20d : %g\n",test[i],count[i],Xsummed[i]);
            }
        }
#endif // _DEPRECATED
    }
#endif//NUCSYN

    /*
     *
     */
    {
        bitset = Calloc((255+1),CHAR_BIT);
        BITSET(bitset,'d');
        BITSET(bitset,'D');
        BITSET(bitset,'e');
        BITSET(bitset,'E');

        Printf("Check speed of exponent char checkers\n");
        for(int _i='a'-2; _i<'f'+2; _i++)
        {
            printf("%c %s %s %s\n",
                   (char)_i,
                   Truefalse(is_exponent_char1(_i)),
                   Truefalse(is_exponent_char2(_i)),
                   Truefalse(is_exponent_char3(_i))
                );
        }
        for(int _i='A'-2; _i<'F'+2; _i++)
        {
            printf("%c %s %s %s\n",
                   (char)_i,
                   Truefalse(is_exponent_char1(_i)),
                   Truefalse(is_exponent_char2(_i)),
                   Truefalse(is_exponent_char3(_i))
                );
        }

        long int is_exponent_char1_count=0;
        long int is_exponent_char2_count=0;
        long int is_exponent_char3_count=0;
        char chars[255];
        int _c = 0;
        for(int _i = (int)'0'; _i <= (int)'9'; _i++)
        {
            chars[_c++] = _i;
        }
        for(int _i = (int)'a'; _i <= (int)'z'; _i++)
        {
            chars[_c++] = _i;
        }
        for(int _i = (int)'A'; _i <= (int)'Z'; _i++)
        {
            chars[_c++] = _i;
        }

        Timeme(is_exponent_char1_count,
               {
                   const char c Maybe_unused = chars[is_exponent_char1_count % _c];
                   Boolean x Maybe_unused  = is_exponent_char1(c);
                   x &= TRUE;
               }
            );
        Timeme(is_exponent_char2_count,
               {
                   const char c Maybe_unused = chars[is_exponent_char2_count % _c];
                   Boolean x Maybe_unused  = is_exponent_char2(c);
                   x &= TRUE;
               }
            );
        Timeme(is_exponent_char3_count,
               {
                   const char c Maybe_unused = chars[is_exponent_char3_count % _c];
                   Boolean x Maybe_unused  = is_exponent_char3(c);
                   x &= TRUE;
               }
            );

        Printf("counters: is_exponent_char1_count %g, is_exponent_char2_count %g, is_exponent_char3_count %g\n",
               (double)is_exponent_char1_count,
               (double)is_exponent_char2_count,
               (double)is_exponent_char3_count
            );
    }


    /*
     * Compare is_integer to isdigit
     */
    {
        Printf("compare is_integer to isdigit\n");

        for(int _i='0'-5; _i<'9'+5; _i++)
        {
            printf("%c %s %s %s %s %s\n",
                   (char)_i,
                   Truefalse(is_integer1(_i)),
                   Truefalse(is_integer2(_i)),
                   Truefalse(is_integer3(_i)),
                   Truefalse(is_integer_or(_i)),
                   Truefalse(is_integer_table(_i)));
        }

        long int is_integer1_counter = 0;
        long int is_integer2_counter = 0;
        long int is_integer3_counter = 0;
        long int is_integer_or_counter = 0;
        long int is_integer_table_counter = 0;
        long int isdigit_counter = 0;
        char chars[255];
        int _c = 0;
        for(int _i = (int)'0'; _i <= (int)'9'; _i++)
        {
            chars[_c++] = _i;
        }
        for(int _i = (int)'a'; _i <= (int)'z'; _i++)
        {
            chars[_c++] = _i;
        }
        for(int _i = (int)'A'; _i <= (int)'Z'; _i++)
        {
            chars[_c++] = _i;
        }
        Timeme(isdigit_counter,
               {
                   const char c Maybe_unused = chars[isdigit_counter % _c];
                   Boolean x Maybe_unused  = isdigit(c);
                   x &= TRUE;
               }
            );
        Timeme(is_integer1_counter,
               {
                   const char c Maybe_unused = chars[is_integer1_counter % _c];
                   Boolean x Maybe_unused  = is_integer1(c);
                   x &= TRUE;
               }
            );
        Timeme(is_integer2_counter,
               {
                   const char c Maybe_unused = chars[is_integer2_counter % _c];
                   Boolean x Maybe_unused  = is_integer2(c);
                   x &= TRUE;
               }
            );
        Timeme(is_integer3_counter,
               {
                   const char c Maybe_unused = chars[is_integer3_counter % _c];
                   Boolean x Maybe_unused  = is_integer3(c);
                   x &= TRUE;
               }
            );
        Timeme(is_integer_or_counter,
               {
                   const char c Maybe_unused = chars[is_integer_or_counter % _c];
                   Boolean x Maybe_unused  = is_integer_or(c);
                   x &= TRUE;
               }
            );
        Timeme(is_integer_table_counter,
               {
                   const char c Maybe_unused = chars[is_integer_table_counter % _c];
                   Boolean x Maybe_unused  = is_integer_table(c);
                   x &= TRUE;
               }
            );

        Printf("counters: isdigit %ld, is_integer1 %ld, is_integer2 %ld, is_integer3 %ld, is_integer_or %ld, is_integer_table %ld\n",
               isdigit_counter,
               is_integer1_counter,
               is_integer2_counter,
               is_integer3_counter,
               is_integer_or_counter,
               is_integer_table_counter
            );
    }

    /*
     * Test polynomial expressions are correct
     */
    {
        size_t _i = 0;
        while(_i++ < 1000)
        {
            const double c[] = {
                random_number(stardata,NULL),
                random_number(stardata,NULL),
                random_number(stardata,NULL),
                random_number(stardata,NULL),
                random_number(stardata,NULL),
                random_number(stardata,NULL)
            };
            const double x = random_number(stardata,NULL);
#define __test(TYPE,X,...)                                      \
            if(!Fequal(_naive_##TYPE((X),__VA_ARGS__),          \
                       _Horner_##TYPE((X),__VA_ARGS__)))        \
            {                                                   \
                Exit_binary_c(2,                                \
                              "Polynomial type %s not equal\n", \
                              #TYPE);                           \
            }

            __test(Quadratic,x,c[0],c[1],c[2]);
            __test(Cubic,x,c[0],c[1],c[2],c[3]);
            __test(Quartic,x,c[0],c[1],c[2],c[3],c[4]);
            __test(Quintic,x,c[0],c[1],c[2],c[3],c[4],c[5]);
        }
    }

    /*
     * fma() vs x*y+z
     */
    {
        size_t nfma = 0;
        Time_me_for_ms(nfma,
                       nms,
                       {
                           double x = random_number(stardata,NULL);
                           double y = random_number(stardata,NULL);
                           double z = random_number(stardata,NULL);
                           double w Maybe_unused = fma(x,y,z);
                       }
            );
        printf("N fma() %zu\n",nfma);
        size_t nraw = 0;
        Time_me_for_ms(nraw,
                       nms,
                       {
                           double x = random_number(stardata,NULL);
                           double y = random_number(stardata,NULL);
                           double z = random_number(stardata,NULL);
                           double w Maybe_unused = x+y*z;
                       }
            );
        printf("N x+y*z() %zu\n",nraw);
        printf("We are using %s\n",Stringof(Fma(x,y,z)));
    }

    /*
     * Time polynomial forms
     */
    {
#define _Time_polynomial(TYPE,...)                                      \
        {                                                               \
            size_t n_Horner=0;                                          \
            size_t n_naive=0;                                           \
            Time_me_for_ms(n_Horner,                                    \
                           nms,                                         \
                           {                                            \
                               const double x = random_number(stardata,NULL); \
                               const double c[] = {                     \
                                   random_number(stardata,NULL),        \
                                   random_number(stardata,NULL),        \
                                   random_number(stardata,NULL),        \
                                   random_number(stardata,NULL),        \
                                   random_number(stardata,NULL),        \
                                   random_number(stardata,NULL)         \
                               };                                       \
                               double w Maybe_unused = _Horner_##TYPE(x, \
                                                                      __VA_ARGS__); \
                           });                                          \
                                                                        \
            Time_me_for_ms(n_naive,                                     \
                           nms,                                         \
                           {                                            \
                               const double x = random_number(stardata,NULL); \
                               const double c[] = {                     \
                                   random_number(stardata,NULL),        \
                                   random_number(stardata,NULL),        \
                                   random_number(stardata,NULL),        \
                                   random_number(stardata,NULL),        \
                                   random_number(stardata,NULL),        \
                                   random_number(stardata,NULL)         \
                               };                                       \
                               double w Maybe_unused = _naive_##TYPE(x, \
                                                                     __VA_ARGS__); \
                           });                                          \
            printf("%s Polynomials in %zdms : Horner %zu, naive %zu : fastest %s\n", \
                   #TYPE,                                               \
                   (ssize_t)nms,                                        \
                   n_Horner,                                            \
                   n_naive,                                             \
                   n_Horner == n_naive ? "Equal" :                      \
                   n_Horner > n_naive ? "Horner" :                      \
                   "naive");                                            \
            fflush(stdout);                                             \
        }
        _Time_polynomial(Quadratic,c[0],c[1],c[2]);
        _Time_polynomial(Cubic,c[0],c[1],c[2],c[3]);
        _Time_polynomial(Quartic,c[0],c[1],c[2],c[3],c[4]);
        _Time_polynomial(Quintic,c[0],c[1],c[2],c[3],c[4],c[5]);
    }

    /*
     * Stardata copy functions
     */
    struct stardata_t * p = new_stardata(stardata->preferences);
    struct stardata_t * q = new_stardata(stardata->preferences);

#define Swap_stardatas_with_copy(STARDATA1,STARDATA2)                   \
    {                                                                   \
        struct stardata_t * _s = new_stardata(stardata->preferences);   \
        copy_stardata((struct stardata_t*)(STARDATA2),                  \
                      (struct stardata_t*)_s,                           \
                      COPY_STARDATA_PREVIOUS_NONE,                      \
                      COPY_STARDATA_PERSISTENT_NONE);                   \
        copy_stardata((struct stardata_t*)(STARDATA1),                  \
                      (struct stardata_t*)(STARDATA2),                  \
                      COPY_STARDATA_PREVIOUS_NONE,                      \
                      COPY_STARDATA_PERSISTENT_NONE);                   \
        copy_stardata((struct stardata_t*)_s,                           \
                      (struct stardata_t*)(STARDATA1),                  \
                      COPY_STARDATA_PREVIOUS_NONE,                      \
                      COPY_STARDATA_PERSISTENT_NONE);                   \
        Safe_free_nocheck(_s);                                          \
    }
#define Swap_stardatas_with_move(STARDATA1,STARDATA2)                   \
    {                                                                   \
        struct stardata_t * _s = new_stardata(stardata->preferences);   \
        struct stardata_t * _dummy_s Maybe_unused;                      \
        _dummy_s = Move_stardata((STARDATA2),                           \
                                 _s);                                   \
        _dummy_s = Move_stardata((STARDATA1),                           \
                                 (STARDATA2));                          \
        _dummy_s = Move_stardata(_s,                                    \
                                 (STARDATA1));                          \
        Safe_free_nocheck(_s);                                          \
    }




#undef X
#define X(STRING) STRING,
char * boolean_strings[] =
{
    BOOLEAN_TEST_LIST_TRUE
    BOOLEAN_TEST_LIST_FALSE
};
#undef X
const size_t n_boolean_strings Maybe_unused = Array_size(boolean_strings);

#define String_is_true_switch(S)                                \
    __extension__                                               \
    ({                                                          \
        Boolean _x;                                             \
        switch((S)[0])                                          \
        {                                                       \
        case '\0':                                              \
            /* empty string*/                                   \
            _x = FALSE;                                         \
            break;                                              \
        case 'T':                                               \
        case 't':                                               \
        case '1':                                               \
        case 'y':                                               \
        case 'Y':                                               \
            switch((S)[1]){                                     \
            case '\0':                                          \
                /* single char: already matched */              \
                _x = TRUE;                                      \
                break;                                          \
            default:                                            \
                /* multiple char */                             \
                _x = Strings_equal_case_insensitive((S),"TRUE") \
                    ||                                          \
                    Strings_equal_case_insensitive((S),"YES")   \
                    ||                                          \
                    Strings_equal_case_insensitive((S),"ON");   \
            }                                                   \
            break;                                              \
        default:                                                \
            _x = FALSE;                                         \
        }                                                       \
        _x;                                                     \
    })

#define String_is_true_switch2(S)                                   \
    __extension__                                                   \
    ({                                                              \
        Boolean _x;                                                 \
        switch((S)[0])                                              \
        {                                                           \
        case '\0':                                                  \
            /* empty string*/                                       \
            _x = FALSE;                                             \
            break;                                                  \
                                                                    \
        case 'T':                                                   \
        case 't':                                                   \
            switch((S)[1]){                                         \
            case '\0':                                              \
                /* single char: already matched */                  \
                _x = TRUE;                                          \
                break;                                              \
            default:                                                \
                /* multiple char */                                 \
                _x = Strings_equal_case_insensitive((S)+1,"RUE");   \
                break;                                              \
            }                                                       \
            break;                                                  \
                                                                    \
        case 'y':                                                   \
        case 'Y':                                                   \
            switch((S)[1]){                                         \
            case '\0':                                              \
                /* single char: already matched */                  \
                _x = TRUE;                                          \
                break;                                              \
            default:                                                \
                /* multiple char */                                 \
                _x = Strings_equal_case_insensitive((S)+1,"ES");    \
                break;                                              \
            };                                                      \
            break;                                                  \
                                                                    \
        case 'o':                                                   \
        case 'O':                                                   \
            switch((S)[1]){                                         \
            case '\0':                                              \
                /* single char: already matched */                  \
                _x = TRUE;                                          \
                break;                                              \
            case 'n':                                               \
            case 'N':                                               \
                _x = TRUE;                                          \
                break;                                              \
            default:                                                \
                _x = FALSE;                                         \
                break;                                              \
            };                                                      \
            break;                                                  \
                                                                    \
        case '1':                                                   \
            switch((S)[1]){                                         \
            case '\0':                                              \
                /* single char: already matched */                  \
                _x = TRUE;                                          \
                break;                                              \
            default:                                                \
                _x = FALSE;                                         \
                break;                                              \
            };                                                      \
            break;                                                  \
                                                                    \
        default:                                                    \
            _x = FALSE;                                             \
            break;                                                  \
        }                                                           \
        _x;                                                         \
    })


/*
 * Compare strtod to fast_strtod
 */
{

    Printf("compare strtod to fast_strtod\n");
#define Random_double                               \
    exp10(random_number(stardata,NULL)*20.0 - 10.0)
    char * strings[1000];
    for (int _i = 0; _i < 1000; _i++)
    {
        if(asprintf(&(strings[_i]), "%g", Random_double) == 0)
        {
            Exit_binary_c(2, "asprintf failed");
        }
    }

    long int overhead_counter = 0;
    Timeme(overhead_counter,

           {
               char * const s Maybe_unused = strings[overhead_counter % 1000];
               double _y Maybe_unused = 1.0;
               _y *= _y;
           }
        );
    long int strtod_counter = 0;
    Timeme(strtod_counter,
           {
               char * const s = strings[strtod_counter % 1000];
               double _y Maybe_unused = strtod(s, NULL);
               _y *= _y;
           }
        );
    long int fast_strtod_counter = 0;
    Timeme(fast_strtod_counter,
           {
               char * const s = strings[fast_strtod_counter % 1000];
               double _y Maybe_unused = fast_strtod(s, NULL);
               _y *= _y;
           }
        );
    Printf("counters: overhead %ld, strtod %ld, fast_strtod %ld\n",
           overhead_counter, strtod_counter, fast_strtod_counter);

    Printf("time in function: strtod %g ms, fast_strtod %g ms\n",
           ((double)overhead_counter / (double)strtod_counter - 1.0) * (double)nms,
           ((double)overhead_counter / (double)fast_strtod_counter - 1.0) * (double)nms);

    Printf("time per run: strtod %g ms, fast_strtod %g ms\n",
           ((double)overhead_counter / (double)strtod_counter - 1.0) * (double)nms / (double)strtod_counter,
           ((double)overhead_counter / (double)fast_strtod_counter - 1.0) * (double)nms / (double)fast_strtod_counter);

    Printf("Speedup %g\n",
           ((double)overhead_counter / (double)strtod_counter - 1.0) /
           ((double)overhead_counter / (double)fast_strtod_counter - 1.0));
}
fflush(stdout);
/*
 * compare speed of memcpy to memmove
 */
{
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
    long int copy = 0, move = 0;
    Timeme(copy, {Swap_stardatas_with_copy(p, q)});
    Timeme(move, {Swap_stardatas_with_move(p, q)});
    Printf("Speed of memcpy/speed of memmove = %g\n",
           (double)move / (double)copy);
#pragma GCC diagnostic pop
}
fflush(stdout);

/*
 * Compare speed of strcat, strlcat, sprintf, asprintf
 */
#undef strcat
#undef strncat
#undef sprintf
{
#define Random_letter  ("ABCDEFGHIJKLMNOPQRSTUVWXYZ"[random () % 26])
#define Reset_strings_12                        \
    {                                           \
        for(i=0;i<n1;i++)                       \
        {                                       \
            s1[i] = Random_letter;              \
            s2[i] = Random_letter;              \
        }                                       \
        s1[n1-2] = '\0';                        \
        s2[n1-2] = '\0';                        \
    }
#define Reset_strings_3                         \
    {                                           \
        for(i=0;i<n1;i++)                       \
        {                                       \
            s3[i] = Random_letter;              \
        }                                       \
        s3[n1-2] = '\0';                        \
    }

#define Alloc_strings_12                        \
    s1 = Calloc(n1+1,sizeof(char));             \
    s2 = Calloc(n1+1,sizeof(char));
#define Alloc_strings_3                         \
    s3 = Calloc((n1+1)*2,sizeof(char));

#define Free_strings                            \
    Safe_free_nocheck(s1);                      \
    Safe_free_nocheck(s2);                      \
    Safe_free_nocheck(s3);

#define String_Timeme(_alloc,_reset,counter,...)    \
    {                                               \
        Timeme(counter,                             \
               {                                    \
                   char * s1;                       \
                   char * s2;                       \
                   char * Maybe_unused s3;          \
                   Alloc_strings_12;                \
                   if(_alloc)                       \
                   {                                \
                       Alloc_strings_3;             \
                   }                                \
                   if(_reset)                       \
                   {                                \
                       Reset_strings_12;            \
                       if(_alloc)                   \
                       {                            \
                           Reset_strings_3;         \
                       }                            \
                   }                                \
                   __VA_ARGS__;                     \
                   Free_strings;                    \
               }                                    \
            );                                      \
    }

    long int _overhead, _strcat, _strncat, _strlcat;
    long int _sprintf, _snprintf, _asprintf;
    _overhead = _strcat = _strlcat = _strncat =
        _sprintf = _snprintf = _asprintf = 0;
    const size_t n1 = 1000;
    unsigned int i;
    String_Timeme(TRUE, TRUE, _overhead,
                  {
                      /* do nothing */
                  });
    String_Timeme(TRUE, TRUE, _strcat,
                  {
                      strcat(s3, s1);
                  });
    String_Timeme(TRUE, TRUE, _strncat,
                  {
                      strncat(s3, s1, n1 * 2 - 1);
                  });
    String_Timeme(TRUE, TRUE, _strlcat,
                  {
                      strlcat(s3, s1, n1 * 2);
                  });
    String_Timeme(TRUE, TRUE, _sprintf,
                  {
                      sprintf(s3, "%s%s", s1, s2);
                  });
    String_Timeme(TRUE, TRUE, _snprintf,
                  {
                      snprintf(s3, n1 * 2 - 1, "%s%s", s1, s2);
                  });

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-result"
    String_Timeme(FALSE, TRUE, _asprintf,
                  {
                      ((void)asprintf(&s3, "%s%s", s1, s2));
                  });
#pragma GCC diagnostic pop

    Printf("String concatenation speed (more is better, n1 = %zu, overhead = %ld) : strcat %ld : strncat %ld : strlcat %ld : sprintf %ld : snprintf %ld : asprintf %ld\n",
           n1,
           _overhead,
           _strcat - 1 * _overhead,
           _strncat - 1 * _overhead,
           _strlcat - 1 * _overhead,
           _sprintf - 1 * _overhead,
           _snprintf - 1 * _overhead,
           _asprintf - 1 * _overhead);
}
fflush(stdout);

/*
 * Compare native memcpy to other versions of memcpy
 * when copying stardatas
 */
{
    /*
     * Define the length of the test in microseconds.
     *
     * Set the environment variable BINARY_C_SPEEDTEST_SECONDS
     * or default to 1 second.
     */

    /*
     * If we have libbsd, use it to set random data,
     * otherwise use /dev/urandom or set to zero.
     */
#ifdef __HAVE_LIBBSD__
#define Reset_buffers                           \
    arc4random_buf(s1, size);                   \
    memcpy(s2,s1,size);
#else
#define Reset_buffers                                   \
    {                                                   \
        if(access("/dev/urandom",F_OK))                 \
        {                                               \
            int __rnd = open("/dev/urandom",O_RDONLY);  \
            if(read(__rnd,s1,size)&&                    \
               read(__rnd,s2,size)){}                   \
            close(__rnd);                               \
        }                                               \
        else                                            \
        {                                               \
            memset(s1,0,size);                          \
            memset(s2,0,size);                          \
        }                                               \
    }
#endif // __HAVE_LIBBSD__

    const size_t size = sizeof(struct stardata_t);
    const size_t malloc_size = size + 10 * sizeof(int);
    struct stardata_t * s1 = Malloc(malloc_size);
    struct stardata_t * s2 = Malloc(malloc_size);
    size_t i;
    Printf("sizeof stardata %zu\n", size);
    Reset_buffers;
    fflush(stdout);

    /*
     * Time native memcpy
     */
    i = 0;
    ticks start_tick = getticks();
    while (Timer_microseconds(getticks() - start_tick) < (nms * 1e3))
    {
        memcpy(s2, s1, size);
        i++;
    }
    Printf("Speed of native memcpy %g GiByte/s\n",
           sizeof(struct stardata_t) * ((float)i) / ( GIGABYTE * ((nms * 1e3) * 1e-6) ));
    fflush(NULL);
    if(!Memory_equal(s1, s2, size))
    {
        Exit_binary_c(1,
                      "s1 and s2 differ :( memcpy failed\n");
    }

    /*
     * Time strided memcpy
     *
     * First, the two areas with identical data
     */
    i = 0;
    Reset_buffers;
    const size_t blocksize = 2048 * sizeof(char);
    start_tick = getticks();
    while (Timer_microseconds(getticks() - start_tick) < (nms * 1e3))
    {
        strided_memcpy(s2, s1, size, blocksize);
        i++;
    }
    Printf("Speed of strided_memcpy (blocksize = %zu, all same) %g GiByte/s\n",
           blocksize,
           sizeof(struct stardata_t) * ((float)i) / ( GIGABYTE * ((nms * 1e3) * 1e-6) ));
    fflush(NULL);
    if(!Memory_equal(s1, s2, size))
    {
        Exit_binary_c(1,
                      "s1 and s2 differ :( memcpy failed\n");
    }

    /*
     * Now the two areas with 10% difference
     */
    Reset_buffers;
    int * s1int = (int*) s1;
    int j = 1;
    for (i = 0; i < size; i += size / 10)
    {
        *( s1int + i / sizeof(int) ) = j;
    }
    i = 0;
    start_tick = getticks();
    while (Timer_microseconds(getticks() - start_tick) < (nms * 1e3))
    {
        strided_memcpy(s2, s1, size, blocksize);
        i++;
    }
    Printf("Speed of strided_memcpy (blocksize = %zu, 10%% different) %g GiByte/s\n",
           blocksize,
           sizeof(struct stardata_t) * ((float)i) / ( GIGABYTE * ((nms * 1e3) * 1e-6) ));
    fflush(NULL);
    if(!Memory_equal(s1, s2, size))
    {
        Exit_binary_c(1,
                      "s1 and s2 differ :( memcpy failed\n");
    }


    Reset_buffers;
    j = 1;
    for (i = 0; i < size; i += size / 5)
    {
        *( s1int + i / sizeof(int) ) = j;
    }
    i = 0;
    start_tick = getticks();
    while (Timer_microseconds(getticks() - start_tick) < (nms * 1e3))
    {
        strided_memcpy(s2, s1, size, blocksize);
        i++;
    }
    Printf("Speed of strided_memcpy (blocksize = %zu, 20%% different) %g GiByte/s\n",
           blocksize,
           sizeof(struct stardata_t) * ((float)i) / ( GIGABYTE * ((nms * 1e3) * 1e-6) ));
    fflush(NULL);

    if(!Memory_equal(s1, s2, size))
    {
        Exit_binary_c(1,
                      "s1 and s2 differ :( memcpy failed\n");
    }

    /*
     * 100% difference
     */
    Reset_buffers;
    j = 1;
    for (i = 0; i < size; i++)
    {
        *( s1int + i / sizeof(int) ) = j;
    }
    i = 0;
    start_tick = getticks();
    while (Timer_microseconds(getticks() - start_tick) < (nms * 1e3))
    {
        strided_memcpy(s2, s1, size, blocksize);
        i++;
    }
    Printf("Speed of strided_memcpy (blocksize = %zu, 100%% different) %g GiByte/s\n",
           blocksize,
           sizeof(struct stardata_t) * ((float)i) / ( GIGABYTE * ((nms * 1e3) * 1e-6) ));
    fflush(NULL);
    if(!Memory_equal(s1, s2, size))
    {
        Exit_binary_c(1,
                      "s1 and s2 differ :( memcpy failed\n");
    }

    /*
     * Now the two areas with 10% difference, but different blocksizes
     */
    Boolean last = FALSE;
    size_t blksize = sizeof(char);
    while (blksize <= size && last == FALSE)
    {
        Reset_buffers;
        j = 1;
        for (i = 0; i < size; i += size / 10)
        {
            *( s1int + i / sizeof(int) ) = j;
        }

        i = 0;
        if(size == blksize) last = TRUE;
        start_tick = getticks();
        while (Timer_microseconds(getticks() - start_tick) < (nms * 1e3))
        {
            strided_memcpy(s2, s1, size, blocksize);
            i++;
        }
        Printf("Speed of strided_memcpy (blocksize = %zu, 10%% different) %g GiByte/s\n",
               blksize,
               sizeof(struct stardata_t) * ((float)i) / ( GIGABYTE * ((nms * 1e3) * 1e-6) ));
        fflush(NULL);
        if(!Memory_equal(s1, s2, size))
        {
            Exit_binary_c(1,
                          "s1 and s2 differ :( memcpy failed\n");
        }

        blksize *= 2;
        blksize = Min(size, blksize);
    }
    fflush(NULL);

    /*
     * Free memory
     */
    Safe_free_nocheck(s1);
    Safe_free_nocheck(s2);
    Safe_free_nocheck(p);
    Safe_free_nocheck(q);
}


/*
 * Compare pow to fastPow
 */
        {
            int n = 1000;
            double lnx, dlnx = (log(DBL_MAX) - log(DBL_MIN)) / (n - 1);
            for (lnx = log(DBL_MIN);
                 lnx < log(DBL_MAX);
                 lnx += dlnx)
            {
                double y = 10 * random_number(stardata, NULL);
                double x = exp(lnx);
                double _pow = pow(x, y);
                double _fastpow = fastPow(x, y);
                Printf("POWCHECK %g %g %g %g %g \n",
                       x,
                       y,
                       _pow,
                       _fastpow,
                       Is_really_not_zero(_fastpow) ? (_pow / _fastpow) : 1.0);
                fflush(NULL);
            }
        }

/*
 * compare multiplication to division
 */
        {
            double x Maybe_unused;
            const double y = 3.346182736, iy = 1.0 / y;
            long int mult = 0, div = 0;
            Timeme(mult, {x = random_number(stardata, NULL); x *= iy;});
            Timeme(div, {x = random_number(stardata, NULL); x /= y;});
            Printf("Speed of mult/div = %g\n", (double)mult / (double)div);
        }
fflush(NULL);

{
    /*
     * Compare log vs log10 operations
     */
    double x Maybe_unused;
    long int __log = 0, __log10 = 0;
    Timeme(__log, {x = log(random_number(stardata, NULL));});
    Timeme(__log10, {x = log10(random_number(stardata, NULL));});
    Printf("Speed of log/speed of log10 = %g\n",
           (double)__log / (double)__log10);
}
fflush(NULL);

{
    /*
     * Compare Pow2,Pow3,... to pow(x,2), pow(x,3), ...
     */



#define powtest(n)                                                      \
    {                                                                   \
        nPOW=npow=0;                                                    \
        Timeme(npow,                                                    \
               {                                                        \
                   double __y = random_number(stardata,NULL);           \
                   double __x Maybe_unused =                            \
                       pow(__y,(double)n);                              \
               }                                                        \
            );                                                          \
                                                                        \
        Timeme(nPOW,                                                    \
               {                                                        \
                   double __y = random_number(stardata,NULL);           \
                   double __x Maybe_unused =                            \
                       Pow##n(__y);                                     \
               }                                                        \
            );                                                          \
        Printf("Speed of POW%d %ld vs pow(x,%d) %ld : speed increase %5.2f %%\n", \
               n,                                                       \
               nPOW,                                                    \
               n,                                                       \
               npow,                                                    \
               Speed_increase_pc(npow,nPOW)                             \
            );                                                          \
        fflush(NULL);                                                   \
    }

    long int nPOW = 0, npow = 0;
    powtest(2);
    powtest(3);
    powtest(4);
    powtest(5);
    powtest(6);
    powtest(7);
    powtest(8);
    powtest(9);
}
#endif //  TIMER && CPUFREQ && millisecond
}



static unsigned long int __test_milliseconds(void)
{
    /* length of speedtests, in milliseconds */
    const char * t = getenv("NMS");
    return t == NULL ? 1 : strtol(t, NULL, 10);
}


static Pure_function inline Boolean is_exponent_char1(const unsigned char c)
{
    /* detect exponent char, e or E in C */
    return (('e' == c) ||
            ('E' == c) ||
            /* allow FORTRAN as well */
            ('d' == c) ||
            ('D' == c));

}

static Pure_function inline Boolean is_exponent_char2(const unsigned char c)
{
    /* detect exponent char, e or E in C */
    return (Boolean[]){FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,TRUE,TRUE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,TRUE,TRUE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE}[c];
}

static Pure_function inline Boolean is_exponent_char3(const unsigned char c)
{
    /* detect exponent char, e or E in C */
    return (Boolean)BITTEST(bitset,c);
}



#ifdef __DEPRECATED
#define _Do_pragma(STRING) _Pragma(#STRING)
#undef _Strided_Xsum
#define _Strided_Xsum(STRIDE)                                           \
    Pure_function Hot_function Nonnull_some_arguments(1)                \
        static Abundance _strided_Xsum##STRIDE (const Abundance * const X) \
    {                                                                   \
        const Isotope max = ((unsigned int)ISOTOPE_ARRAY_SIZE/(STRIDE)) * (STRIDE); \
        Abundance * Xsum = Calloc((STRIDE),sizeof(Abundance));          \
        for(Isotope i=0;i<max;)                                         \
        {                                                               \
            PRAGMA_GCC_IVDEP                                            \
                _Do_pragma(GCC unroll STRIDE)                           \
                for(int j=0; j<(STRIDE); j++)                           \
                {                                                       \
                    Xsum[j] += X[i++];                                  \
                }                                                       \
        }                                                               \
        for(Isotope i=max;i<ISOTOPE_ARRAY_SIZE;i++)                     \
        {                                                               \
            Xsum[0] += X[i];                                            \
        }                                                               \
                                                                        \
        Abundance Xsummed = 0.0;                                        \
        for(int j=0; j<(STRIDE); j++)                                   \
        {                                                               \
            Xsummed += Xsum[j];                                         \
        }                                                               \
        Safe_free(Xsum);                                                \
        return Xsummed;                                                 \
    }


#undef X
#define X(N) _Strided_Xsum(N)
_Strides
#endif
