#include "../binary_c.h"
No_empty_translation_unit_warning;
#include "defaults_sets.h"

void set_default_preferences(struct stardata_t * const stardata)
{
    /*
     * Set the preferences struct to its default values
     * according to the current defaults_set.
     *
     * Also sets stardata->tmpstore->default_preferences
     * if tmpstore is set up.
     */
    const int n =
        (stardata->preferences->defaults_set < 0 ||
         stardata->preferences->defaults_set >= DEFAULTS_SETS_NUMBER)
        ? DEFAULTS_SET_FALLBACK
        : stardata->preferences->defaults_set;
    Dprint("Set default preferences %d : preferences = %p, stardata->tmpstore->preferences = %p \n",
           n,
           (void*)stardata->preferences,
           (void*)stardata->tmpstore->default_preferences
          );
    default_preferences_functions[n](stardata->preferences);
    if(stardata->tmpstore != NULL)
    {
        if(stardata->tmpstore->default_preferences == NULL)
        {
            stardata->tmpstore->default_preferences = Calloc(1, sizeof(struct preferences_t));
        }
        default_preferences_functions[n](stardata->tmpstore->default_preferences);
    }
}
