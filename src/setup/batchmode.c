#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Batchmode control subroutines
 */

#ifdef BATCHMODE
void batchmode_set_echo_on(ARG_SUBROUTINE_DECLARATION)
{
    stardata->preferences->batchmode = BATCHMODE_ON_AND_ECHO;
    printf("set echo on\n");
}

void batchmode_set_echo_off(ARG_SUBROUTINE_DECLARATION)
{
    stardata->preferences->batchmode = BATCHMODE_ON;
    printf("set echo off\n");
}

void No_return batchmode_bye(ARG_SUBROUTINE_DECLARATION)
{
    printf("Rec bye batchmode = %d\n",
           stardata->preferences->batchmode);
    if(stardata->preferences->batchmode==BATCHMODE_ON_AND_ECHO)
        printf("Bye then!\n");
    stardata->preferences->batchmode=BATCHMODE_OFF;
    free_aux_memory();
    Exit_binary_c(BINARY_C_NORMAL_BATCHMODE_EXIT,"batchmode bye");
}

void batchmode_fin(ARG_SUBROUTINE_DECLARATION)
{
    printf("fin\n");
}

void batchmode_go(ARG_SUBROUTINE_DECLARATION)
{
    int evolve_system_return_value = evolve_system(stardata);
    printf("evolve_system returned %d\n",evolve_system_return_value);
    if(stardata->preferences->batch_submode==BATCH_SUBMODE_FIN)
        printf("fin\n");
}

void No_return batchmode_gogo(ARG_SUBROUTINE_DECLARATION)
{
    reset_stars_defaults(ARG_SUBROUTINE_ARGS2);
    stardata->model.max_evolution_time=1.0;
    int evolve_system_return_value = evolve_system(stardata);
    printf("evolve_system returned %d\n",evolve_system_return_value);
    if(stardata->preferences->batch_submode==BATCH_SUBMODE_FIN)
        printf("fin\n");
    batchmode_bye(ARG_SUBROUTINE_ARGS2);
}

void batchmode_status(ARG_SUBROUTINE_DECLARATION)
{
#ifdef STARDATA_STATUS
    stardata_status(stardata,0);
    stardata_status(stardata,1);
    stardata_status(stardata,2);
#endif//STARDATA_STATUS
}

void batchmode_reset_stars_defaults(ARG_SUBROUTINE_DECLARATION)
{
    if(stardata->preferences->batchmode==BATCHMODE_ON_AND_ECHO)
    {
        printf("Reset the stars\n");
        printf("Applying default stellar parameters\n");
    }
    reset_stars_defaults(ARG_SUBROUTINE_ARGS2);
}

void batchmode_reset_stars(ARG_SUBROUTINE_DECLARATION)
{
    /* star reset function for batchmode */
    if(stardata->preferences->batchmode==BATCHMODE_ON_AND_ECHO)
    {
        printf("Reset the stars\n");
    }
    Star_number k;
    Number_of_stars_Starloop(k)
    {
        SETstar(k);
        init_star(stardata,star);
        star->starnum = k;
    }
    init_model(&(stardata->model));
    init_common(stardata);
}


void batchmode_defaults(ARG_SUBROUTINE_DECLARATION)
{
    // set just enough defaults to make the code run
    if(stardata->preferences->batchmode==BATCHMODE_ON_AND_ECHO)
    {
        printf("Applying default stellar parameters\n");
    }
    default_stardata(stardata);
}

void batchmode_reset_prefs(ARG_SUBROUTINE_DECLARATION)
{
    // reset preferences
    if(stardata->preferences->batchmode==BATCHMODE_ON_AND_ECHO)
        printf("Reset preferences\n");
    set_default_preferences(stardata);
}

#endif//BATCHMODE
