#include "../binary_c.h"
No_empty_translation_unit_warning;
#include "cmd_line_function_macros.h"

/*
 * Function to set random systems for testing
 *
 * Arguments have options in them which define whether they
 * should be randomized, and if so in which ranges they should
 * have their random values set.
 */

#ifdef RANDOM_SYSTEMS

/* random double-precision number */
#define RAN ((double)                                   \
             random_number_buffer(                      \
                 stardata,                              \
                 &common->random_systems_buffer.buffer  \
                 ))

/* choose a random number between A and B */
#define Random_double(A,B) (((A)+RAN*((B)-(A))))

/* choose a log-distributed random number between A and B */
#define logRandom_double(A,B) (exp(Random_double(log(A),log(B))))

/* random interger between A and B */
#define Random_int(A,B) ((int)(Random_double((A),(B))+0.5))

#define Random_int_array(N,DATA) ((int)(DATA)[Random_int(0,(N))]._int)
#define Random_double_array(N,DATA) ((double)(DATA)[Random_int(0,(N))]._double)

void set_random_system(struct stardata_t * const Restrict stardata,
                       const int output_format)
{
    const Boolean vb = FALSE;
    struct tmpstore_t * const tmpstore = stardata->tmpstore;
    struct common_t * const common = &stardata->common;
    struct preferences_t * const p = stardata->preferences;

    if(tmpstore->random_system_argstring != NULL)
    {
        Safe_free(tmpstore->random_system_argstring);
    }
    reasprintf(&stardata->tmpstore->random_system_argstring,
               " M_1 0 M_2 0 M_3 0 M_4 0 ");

    /*
     * Seed for generating the random numbers to make the
     * random systems. If set on the command line, use
     * it.
     */
    if(p->cmd_line_random_systems_seed != 0)
    {
        common->random_systems_seed = p->cmd_line_random_systems_seed;
    }
    else
    {
        common->random_systems_seed = random_seed();
    }

    if(common->random_systems_buffer.set == FALSE)
    {
        common->random_systems_buffer.set = TRUE;
        set_random_buffer(common->random_systems_seed,
                          &common->random_systems_buffer.buffer);
    }

    for(unsigned int level=1; level <= (unsigned int)p->random_systems; level++)
    {
        for(int parse_num = 0; parse_num < ARG_NUM_PARSES; parse_num++)
        {
            if(vb) printf("Level %u parse_num %d : ",
                          level,
                          parse_num);

            for(unsigned int i=0; i<stardata->store->argstore->arg_count; i++)
            {
                struct cmd_line_arg_t * const arg = &stardata->store->argstore->cmd_line_args[i];

                if(arg->parse_number == parse_num)
                {
                    if(arg->random_level == level &&
                       arg->pointer != NULL)
                    {
                        if(vb) printf("%s (%u)\n ",
                                      arg->name,
                                      arg->random_arg_type);

                        int n = 1;
                        int offset = 0;
                        if(Argtype_is_scanf(arg->type))
                        {
                            if(Argtype_is_offset_scanf(arg->type))
                            {
                                /* determine the numbers we can use */
                                if(arg->limit_to_multiplicity == TRUE)
                                {
                                    n = stardata->preferences->multiplicity;
                                }
                                else
                                {
                                    const ptrdiff_t diff = (__extension__ (arg->max_pointer - arg->pointer))/
                                        Argtype_scanf_varsize(arg->type);
                                    n = (int)diff;
                                }
                                offset = 1;
                            }
                            else
                            {
                                Exit_binary_c(BINARY_C_ALGORITHM_BRANCH_FAILURE,
                                              "Could not determine the maximum number of a random system scanf argument : you need to set the max pointer for %s\n",
                                              arg->name);
                            }
                        }

                        for(int j=offset; j<offset + n; j++)
                        {
                            char * argname;
                            if(Argtype_is_scanf(arg->type))
                            {
                                if(asprintf(&argname,
                                            arg->name,
                                            j) == -1)
                                {
                                    Exit_binary_c(BINARY_C_MALLOC_FAILED,
                                                  "asprintf failed : check if you have enough memory.");
                                }
                            }
                            else
                            {
                                argname = strdup(arg->name);
                            }

                            /*
                             * Vary this argument
                             */
                            if(arg->random_arg_type == ARG_RANDOM_INT)
                            {
                                *((int*)arg->pointer) = Random_int(arg->random_lower._int,
                                                                   arg->random_upper._int);
                            }
                            else if(arg->random_arg_type == ARG_RANDOM_BOOLEAN)
                            {
                                *((Boolean*)arg->pointer) = (Boolean)Random_int(0,1);
                            }
                            else if(arg->random_arg_type == ARG_RANDOM_DOUBLE)
                            {
                                *((double*)arg->pointer) = Random_double(arg->random_lower._double,
                                                                         arg->random_upper._double);
                            }
                            else if(arg->random_arg_type == ARG_RANDOM_LOGDOUBLE)
                            {
                                *((double*)arg->pointer) = logRandom_double(arg->random_lower._double,
                                                                            arg->random_upper._double);

                            }
                            else if(arg->random_arg_type == ARG_RANDOM_INT_ARRAY)
                            {
                                *((int*)arg->pointer) = Random_int_array(arg->random_array_length,
                                                                         arg->random_array);
                            }
                            else if(arg->random_arg_type == ARG_RANDOM_DOUBLE_ARRAY)
                            {
                                *((double*)arg->pointer) = Random_double_array(arg->random_array_length,
                                                                               arg->random_array);
                            }
                            else if(arg->random_arg_type == ARG_RANDOM_INT_ARRAY_OR_INT_SCALAR)
                            {
                                if(RAN < 0.5)
                                {
                                    *((int*)arg->pointer) = Random_int(arg->random_lower._int,
                                                                       arg->random_upper._int);
                                }
                                else
                                {
                                    *((int*)arg->pointer) = Random_int_array(arg->random_array_length,
                                                                             arg->random_array);
                                }
                            }
                            else if(arg->random_arg_type == ARG_RANDOM_DOUBLE_ARRAY_OR_DOUBLE_SCALAR)
                            {
                                if(RAN < 0.5)
                                {
                                    *((double*)arg->pointer) = Random_double(arg->random_lower._double,
                                                                             arg->random_upper._double);
                                }
                                else
                                {
                                    *((double*)arg->pointer) = Random_double_array(arg->random_array_length,
                                                                                   arg->random_array);
                                }
                            }
                            else if(arg->random_arg_type == ARG_RANDOM_INT_ARRAY_OR_DOUBLE_SCALAR)
                            {
                                /* var type must be double */
                                if(RAN < 0.5)
                                {
                                    *((double*)arg->pointer) = Random_int_array(arg->random_array_length,
                                                                                arg->random_array);
                                }
                                else
                                {
                                    *((double*)arg->pointer) = Random_double(arg->random_lower._double,
                                                                             arg->random_upper._double);

                                }
                            }
                            else
                            {
                                Exit_binary_c(BINARY_C_ALGORITHM_BRANCH_FAILURE,
                                              "Failed to set data into random argument %s : please check that its random_arg_type is handled in set_random_system.c.",
                                              arg->name);
                            }


                            if(Argtype_is_double(arg->type))
                            {
                                reasprintf(&stardata->tmpstore->random_system_argstring,
                                           "%s--%s %g ",
                                           stardata->tmpstore->random_system_argstring,
                                           argname,
                                           (double)*((double*)arg->pointer));
                            }
                            else if(Argtype_is_boolean(arg->type))
                            {
                                reasprintf(&stardata->tmpstore->random_system_argstring,
                                           "%s--%s %s ",
                                           stardata->tmpstore->random_system_argstring,
                                           argname,
                                           Truefalse((Boolean)*((Boolean*)arg->pointer)));
                            }
                            else if(Argtype_is_integer(arg->type))
                            {
                                reasprintf(&stardata->tmpstore->random_system_argstring,
                                           "%s--%s %d ",
                                           stardata->tmpstore->random_system_argstring,
                                           argname,
                                           (int)*((int*)arg->pointer));
                            }
                            else if(Argtype_is_long_integer(arg->type))
                            {
                                reasprintf(&stardata->tmpstore->random_system_argstring,
                                           "%s--%s %ld ",
                                           stardata->tmpstore->random_system_argstring,
                                           argname,
                                           (long int)*((long int*)arg->pointer));
                            }
                            else
                            {
                                Exit_binary_c(BINARY_C_ALGORITHM_BRANCH_FAILURE,
                                              "Failed to set random argument data for argument %s (mapped to %s) : please check that its arg type is handled in set_random_system.c.",
                                              arg->name,
                                              argname);
                            }
                            Safe_free(argname);
                        }
                    }
                }
            }
        }
        if(vb) printf("\n");
    }

    /*
     * Set the random number seed.
     */
    stardata->preferences->cmd_line_random_seed = random_seed();
    reasprintf(&stardata->tmpstore->random_system_argstring,
               "%s--random_seed "Random_seed_format" ",
               stardata->tmpstore->random_system_argstring,
               (Random_seed_cast)stardata->preferences->cmd_line_random_seed);
    stardata->common.random_buffer.set = FALSE;
    stardata->common.random_buffer.count = 0;

#ifdef RANDOM_SYSTEMS_SHOW_ARGS
#ifdef RANDOM_SYSTEMS_SHOW_ARGS_AND_START_TIME
    if(output_format == RANDOM_SYSTEM_OUTPUT_WITH_TIME)
    {
        const time_t t = time(NULL);
        char timestring[100];
        if(strftime(timestring,
                    sizeof(timestring),
                    "At %F %T :   ",
                    localtime(&t)))
        {
            fprintf(RANDOM_SYSTEMS_SHOW_ARGS_STREAM,
                    "%s",
                    timestring);
        }
    }
#endif//RANDOM_SYSTEMS_SHOW_ARGS_AND_START_TIME

    if(output_format == RANDOM_SYSTEM_OUTPUT_ARGS_ONLY)
    {
        Printf("%s\n",
                stardata->tmpstore->random_system_argstring);
        return;
    }

    /*
     * Set arguments from strings, just as we would
     * through the API or command line
     */
    parse_arguments_from_string(stardata->tmpstore->random_system_argstring,
                                stardata);

    /*
     * Show arguments on the screen and flush all
     * buffers, just in case.
     */
    fprintf(RANDOM_SYSTEMS_SHOW_ARGS_STREAM,
            "%s\n",
            stardata->tmpstore->random_system_argstring);
    fflush(NULL);
#endif // RANDOM_SYSTEMS_SHOW_ARGS
}
#endif //RANDOM_SYSTEMS
