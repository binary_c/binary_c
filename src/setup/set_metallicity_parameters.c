/*
 * set_metallicity_parameters : a wrapper
 * to set_metallicity_parameters2 which calculated
 * the various parameters used in the BSE library's
 * interpolation formulae for stellar evolution
 */

#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef BSE

void set_metallicity_parameters (struct stardata_t * Restrict const stardata)
{


/*      zpars:  1; M below which hook doesn't appear on MS, Mhook.
 *              2; M above which He ignition occurs non-degenerately, Mhef.
 *              3; M above which He ignition occurs on the HG, Mfgb.
 *              4; M below which C/O ignition doesn't occur, Mup.
 *              5; M above which C ignites in the centre, Mec.
 *              6; value of log D for M<= zpars[3]
 *              7; value of x for Rgb propto M^(-x)
 *              8; value of x for tMS = Max(tHOOK,x*tBGB);
 *              9; constant for McHeIf when computing Mc,BGB, mchefl.
 *             10; constant for McHeIf when computing Mc,HeI, mchefl.
 *             11; hydrogen abundance.
 *             12; helium abundance.
 *             13; constant x in rmin = rgb*x**y; used by LM CHeB.
 *             14; z**0.4 to be used for WD L formula.
 * NB zpars==metallicity_parameters
 */
    /*
     * old_metallicity is the metallicity that was used last time this routine was
     * called. If old_metallicity=common->metallicity, we don't have to do everything
     * again.
     * First of all, set old_metallicity to -1.0 which is totally unrealistic.
     */

    /*
     * Set these parameters ONLY if they have already not been set
     * or if the metallicity has changed.
     */

    Boolean reset_metallicity_parameters;

#ifdef FORCE_RESET_OF_METALLICITY_PARAMETERS
    reset_metallicity_parameters = TRUE;
#else
    reset_metallicity_parameters =
        Boolean_(!(Fequal(stardata->common.metallicity,
                          stardata->common.parameters_metallicity)));
#endif

    if(reset_metallicity_parameters==TRUE)
    {
        set_metallicity_parameters2(stardata);
        stardata->common.parameters_metallicity=
            stardata->common.metallicity;
    }

    /* else don't set the metallicity parameters, just leave happy... */
}

#endif//BSE
