#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "cmd_line_args.h"
#include "cmd_line_function_macros.h"
#include "cmd_line_macro_pairs.h"
/*
 * Function to refresh cmd-line args:
 * why? some point to functions, these should be renewed
 * because they might change.
 *
 * The argstore should also check that preferences hasn't
 * changed, as pointers to preferences are requirements.
 */

void refresh_arguments(struct stardata_t * const Restrict stardata,
                       const int argc,
                       const Boolean force)
{
    /*
     * Make args table
     */
#undef X
#define X(TYPE,SIZEOF,OPTSTRINGS) #TYPE,
    static const char * const arg_type_strings[] = { ARG_TYPES_LIST };
#undef X
#define X(TYPE,SIZEOF,OPTSTRINGS) (SIZEOF),
    static const size_t arg_type_sizeof[] = { ARG_TYPES_LIST };
#undef X
    Dprint("ARGS setup in store %p\n",
           (void*)stardata->store);

    /*
     * see cmd_line_args.h for definitive argument table
     */
    if(force == TRUE)
    {
        Safe_free(stardata->store->argstore);
    }
    struct argstore_t * argstore = stardata->store->argstore;
    if(!argstore)
    {
        argstore = stardata->store->argstore = Calloc(1,sizeof(struct argstore_t));
    }

    if(force == TRUE ||
       stardata->preferences != argstore->preferences)
    {
        argstore->preferences = stardata->preferences;

        /*
         * Note that this array must be set up
         * every time: it is not possible to define it as
         * const type or in the store because it contains pointers to the star
         * and model structs in stardata, as well as function pointers,
         * which can change from run to run.
         */
        const struct cmd_line_arg_t cmd_line_args2[] =
            {
#include "cmd_line_args_list.h"
            };

        /* size of the cmd_line_args array */
        const size_t size = sizeof(cmd_line_args2);

        /* count the number of arguments (last's name NULL) */
        argstore->arg_count = size / sizeof(struct cmd_line_arg_t);
        Dprint("arg count %u\n",argstore->arg_count);

        /* allocate memory in the store for it */
        if(argstore->cmd_line_args == NULL)
        {
            argstore->cmd_line_args=Calloc(1,size);
            Dprint("Allocated %zu bytes for store->cmd_line_args table at %p\n",
                   size,
                   (void*)argstore->cmd_line_args);
        }

        /*
         * Copy the data
         */
        memcpy(argstore->cmd_line_args,
               cmd_line_args2,
               size);

        for(unsigned int i=0;i<argstore->arg_count;i++)
        {
            /*
             * Check sizes
             */
            if(argstore->cmd_line_args[i].pointer != NULL &&
               arg_type_sizeof[argstore->cmd_line_args[i].type] != 0)
            {
                if(argstore->cmd_line_args[i].varsize !=
                   arg_type_sizeof[argstore->cmd_line_args[i].type])
                {
                    Exit_binary_c(BINARY_C_WRONG_ARGUMENT,
                                  "cmd line arg %s has wrong size, it is declared as type %s with size %zu but points to a variable of size %zu\n",
                                  argstore->cmd_line_args[i].name,
                                  arg_type_strings[argstore->cmd_line_args[i].type],
                                  arg_type_sizeof[argstore->cmd_line_args[i].type],
                                  argstore->cmd_line_args[i].varsize
                        );
                }
            }

            /*
             * clear macro pair counters
             */
            argstore->cmd_line_args[i].pairs = NULL;
            argstore->cmd_line_args[i].npairs = 0;
            argstore->cmd_line_args[i].argpairs = NULL;
            argstore->cmd_line_args[i].nargpairs = 0;
        }

        /*
         * add macro pairs
         */
        set_cmd_line_macro_pairs(stardata,
                                 argstore->cmd_line_args,
                                 argstore->arg_count);


        /*
         * Make parse-number based arg lists
         */
        argstore->cmd_line_args_on_parse = Malloc(sizeof(unsigned int *) * ARG_NUM_PARSES);
        argstore->n_cmd_line_args_on_parse = Malloc(sizeof(unsigned int) * ARG_NUM_PARSES);
        for(int parse_number=0; parse_number<ARG_NUM_PARSES; parse_number++)
        {
            Cprint("parse %d : ",parse_number);
            argstore->cmd_line_args_on_parse[parse_number] = Malloc(sizeof(unsigned int) * argstore->arg_count);
            unsigned int j = 0;
            argstore->n_cmd_line_args_on_parse[parse_number] = 0;
            for(unsigned int i=0; i<argstore->arg_count; i++)
            {
                if(argstore->cmd_line_args[i].parse_number == ARG_PARSE_ALL ||
                   argstore->cmd_line_args[i].parse_number == parse_number)
                {
                    argstore->cmd_line_args_on_parse[parse_number][j] = i;
                    j++;
                    Cprint("%u, ",i);
                    argstore->n_cmd_line_args_on_parse[parse_number]++;
                }
            }
            Cprint("\n");
        }

        Dprint("Arg table built : we have %u args (argc=%d)\n",
               argstore->arg_count,argc);
    }
}
