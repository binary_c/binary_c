#include "../binary_c.h"
No_empty_translation_unit_warning;

char * read_system_list(struct stardata_t * const stardata,
                        const Boolean show)
{
    /*
     * Read a string from the system list.
     *
     * Open the file if necessary.
     *
     * If stardata is NULL, close the list but
     * do not return an error code, just NULL.
     */

    if(stardata->store->system_list == NULL)
    {
        /*
         * First time: open file
         */
        stardata->store->system_list = binary_c_fopen(stardata,
                                                      stardata->preferences->system_list_path,
                                                      "r",
                                                      0);
        Dprint("opened system list %p\n",(void*)stardata->store->system_list);
        if(stardata->store->system_list == NULL)
        {
            Exit_binary_c(BINARY_C_FILE_OPEN_ERROR,
                          "Could not open system list from file \"%s\" : please check its path.",
                          stardata->preferences->system_list_path);
        }
    }

    /*
     * Read next line
     */
    char * const next_arg_string = binary_c_getline(stardata->store->system_list);
    chomp(next_arg_string);
    Dprint("next arg string : %s\n",next_arg_string);

    if(next_arg_string && show)
    {
        const time_t t = time(NULL);
        char timestring[100];
        if(strftime(timestring,
                    sizeof(timestring),
                    "At %F %T system ",
                    localtime(&t)))
        {
            Printf("%s (%u) : %s\n",
                   timestring,
                   ++stardata->store->system_list_counter,
                   next_arg_string);
        }
    }

    return next_arg_string;
}
