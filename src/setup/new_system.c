#include "../binary_c.h"
No_empty_translation_unit_warning;


#define dprint(P,...) Dprint_to_pointer((P),TRUE,__VA_ARGS__);

void new_system(struct stardata_t ** new_stardata,
                struct stardata_t *** previous_stardatas,
                struct preferences_t ** preferences,
                struct store_t **  store,
                struct persistent_data_t ** persistent_data,
                char ** argv,
                const int argc)
{
    /*
     * Make a new binary system in new_stardata.
     *
     * The new stardata struct is allocated and put in *new_stardata
     *
     * If previous_stardata is NULL, it is allocated and put in new_stardata.
     *
     * The preferences struct is set to defaults and filled appropriately.
     *
     * The store struct is set and allocated if NULL, but
     * note that it is best to save this data and call
     * with it already set. Multiple calls to new_system
     * with empty stores means binary_c will be doing a lot of
     * unecessary work.
     *
     * If argc is -1, then argv is a pointer to a string which is split
     * and passed into the parse_arguments function.
     *
     * If argc is 0 or positive, argv and argc are passed
     * directly into parse_arguments.
     *
     */

    /*
     * Do main allocations of new_stardata, previous_stardata
     * and set up pointers. Also reset the preferences struct.
     */
    check_nans_are_signalled();
    struct stardata_t * Maybe_unused stardata = *new_stardata;
    dprint(*new_stardata,
           "call main_allocations : new_stardata=%p *new_stardata=%p persistent_data=%p\n",
           (void*)new_stardata,
           (void*)(*new_stardata),
           (void*)persistent_data);

    main_allocations(new_stardata,
                     previous_stardatas,
                     preferences,
                     store,
                     persistent_data);

    /*
     * Set defaults into stardata
     */
    dprint(*new_stardata,
           "call set defaults\n");
    set_defaults(*new_stardata);

    /*
     * Load in options from command-line arguments
     */
#ifdef DEBUG_FAIL_ON_NAN
    Boolean allow_nan_was Maybe_unused = FALSE;
    if(*new_stardata != NULL)
    {
        allow_nan_was = (*new_stardata)->preferences->allow_debug_nan;
        (*new_stardata)->preferences->allow_debug_nan = TRUE;
    }
#endif//DEBUG_FAIL_ON_NAN
#ifdef DEBUG_FAIL_ON_INF
    Boolean allow_inf_was Maybe_unused = FALSE;
    if(*new_stardata != NULL)
    {
        allow_inf_was = (*new_stardata)->preferences->allow_debug_inf;
        (*new_stardata)->preferences->allow_debug_inf = TRUE;
    }
#endif//DEBUG_FAIL_ON_INF
    dprint(*new_stardata,"Pre-parse argc=%d argv=%p\n",argc,(void*)argv);
    if(argc == -1)
    {
        dprint(*new_stardata,"parse given arg string *argv = %s\n",*argv);
        /*
         * Split given arg string
         */
        int argc_system;
#ifdef USE_SPLIT_COMMANDLINE
        char ** const argv_system = split_commandline(*argv,&argc_system);
#else

        struct string_array_t * string_array = new_string_array(0);
        string_split(stardata,
                     *argv,
                     string_array,
                     ' ',
                     1024,
                     0,
                     TRUE);
        char ** argv_system = string_array->strings;
        argc_system = string_array->n;

        if(argv_system==NULL)
        {
            Exit_binary_c_no_stardata(BINARY_C_WRONG_ARGUMENT,
                                      "Command line split failed: argv_system==NULL from command line string \"%s\"\n",*argv);
        }
#endif // USE_SPLIT_COMMANDLINE
        dprint(*new_stardata,"parse command from command line\n");
        parse_arguments(1,
                        argc_system,
                        argv_system,
                        *new_stardata);
#ifdef USE_SPLIT_COMMANDLINE
        split_commandline_free(argv_system,argc_system);
#else
        free_string_array(&string_array);
#endif
        dprint(*new_stardata,"scanned %d args\n",argc_system);
    }
    else if(argc>=0)
    {
        dprint(*new_stardata,"parse command from command line\n");

        /*
         * Use passed in argv and argc to make the system
         */
        parse_arguments(1,argc,argv,*new_stardata);
    }

#ifdef DEBUG_FAIL_ON_NAN
    if(*new_stardata != NULL)
    {
        (*new_stardata)->preferences->allow_debug_nan = allow_nan_was;
    }
#endif
#ifdef DEBUG_FAIL_ON_INF
    if(*new_stardata != NULL)
    {
        (*new_stardata)->preferences->allow_debug_inf = allow_inf_was;
    }
#endif
    dprint(*new_stardata,"Finished new system\n");
}
