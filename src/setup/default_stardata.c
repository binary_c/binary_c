#include "../binary_c.h"
No_empty_translation_unit_warning;


void default_stardata(struct stardata_t * Restrict const stardata)
{
    stardata->star[0].mass=6.0;
    stardata->star[0].effective_zams_mass=0.0;
    stardata->common.zero_age.mass[0] = 6.0;
    stardata->star[0].phase_start_mass=0.0;
    stardata->star[1].mass=3.0;
    stardata->star[1].effective_zams_mass=0.0;
    stardata->common.zero_age.mass[1] = 3.0;
    stardata->star[1].phase_start_mass=0.0;
#ifdef NUCSYN
    stardata->star[0].hezamsmetallicity=0.0;
    stardata->star[1].hezamsmetallicity=0.0;
    stardata->star[0].num_thermal_pulses=-1.0;
    stardata->star[1].num_thermal_pulses=-1.0;
    stardata->star[0].temp_mult=1.0;
    stardata->star[1].temp_mult=1.0;
#endif //NUCSYN
    stardata->star[0].radius=0.0;
    stardata->star[1].radius=0.0;
    stardata->common.orbit.period=1e3;
    stardata->common.orbit.separation=calculate_orbital_separation(stardata);
    stardata->star[0].stellar_type=MAIN_SEQUENCE;
    stardata->star[1].stellar_type=MAIN_SEQUENCE;
    stardata->common.orbit.eccentricity=0;
    stardata->common.metallicity=0.02;
    stardata->model.max_evolution_time=13000;
}
