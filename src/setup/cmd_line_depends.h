#pragma once
#ifndef CMD_LINE_DEPENDS_H
#define CMD_LINE_DEPENDS_H

/*
 * Based on
 * https://gustedt.wordpress.com/2010/06/08/detect-empty-macro-arguments/
 * but edited for one variable only
 */
#define _ARG16(_0, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, ...) _15
#define HAS_COMMA(...) _ARG16(__VA_ARGS__, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0)
#define _TRIGGER_PARENTHESIS_(...) ,
#define ISEMPTY(VAR)                                                    \
    _ISEMPTY(                                                           \
          /* test if there is just one argument, eventually an empty    \
             one */                                                     \
        HAS_COMMA(VAR),                                                 \
          /* test if _TRIGGER_PARENTHESIS_ together with the argument   \
             adds a comma */                                            \
        HAS_COMMA(_TRIGGER_PARENTHESIS_ VAR),                           \
          /* test if the argument together with a parenthesis           \
             adds a comma */                                            \
        HAS_COMMA(VAR (/*empty*/)),                                     \
          /* test if placing it between _TRIGGER_PARENTHESIS_ and the   \
             parenthesis adds a comma */                                \
        HAS_COMMA(_TRIGGER_PARENTHESIS_ VAR (/*empty*/)),               \
        VAR##VAR                                                        \
          )
#define PASTE5(_0, _1, _2, _3, _4, _5) _0 ## _1 ## _2 ## _3 ## _4 ## _5
#define _ISEMPTY(_0, _1, _2, _3, _4) HAS_COMMA(PASTE5(_IS_EMPTY_CASE_, _0, _1, _2, _3, _4))
#define _IS_EMPTY_CASE_00010 ,



/*
 * Varif and Varif2 test one or two macros
 * and output the variable if both are defined
 * but do not have values set.
 */

    /* one variable */
#define Varif(a,var) _Varif(ISEMPTY(a),var)
#define _Varif(_a,var) __Varif(_a,var)
#define __Varif(_a,var) Varif##_a(var)
#define Varif1(variable) variable
#define Varif0(variable) Not_used_arg

    /* two variables */
#define Varif2(a,b,var) _Varif2(ISEMPTY(a),ISEMPTY(b),var)
#define _Varif2(_a,_b,var) __Varif2(_a,_b,var)
#define __Varif2(_a,_b,var) Varif2##_a##_b(var)
#define Varif211(variable) variable
#define Varif200(variable) Not_used_arg
#define Varif201(variable) Not_used_arg
#define Varif210(variable) Not_used_arg

#endif // CMD_LINE_DEPENDS_H
