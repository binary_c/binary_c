#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Return a new random seed.
 */

Random_seed random_seed(void)
{
    Random_seed ret;

    /*
     * Change random seed every millisecond,nanosecond or second
     * every second
     */
    long int t;

#ifdef NANOSECOND_RANDOMNESS
    /*
     * nanosecond accuracy
     */
    struct timespec ts;
    timespec_get(&ts,
                 TIME_UTC);
    t = 1000*1000*1000*ts.tv_sec + ts.tv_nsec;
#elif defined MILLISECOND_RANDOMNESS
    /*
     * millisecond accuracy
     */
    struct timeb tp;
    ftime(&tp);
    t = 1000 * tp.time + tp.millitm;
#else
    /*
     * 1-second accuracy is sometimes good enough
     */
    t = time(NULL) * rand();
#endif

    /*
     * Hence seed
     */
    ret = (Random_seed)(((long int)(t))%((int)DAY_LENGTH_IN_SECONDS));
    if(ret==0) ret = +1;
    return ret;
}
