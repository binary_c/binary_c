#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "cmd_line_args.h"
#include "cmd_line_function_macros.h"
#include "cmd_line_macro_pairs.h"
static void _arg_action(struct stardata_t * const stardata,
                        struct cmd_line_arg_t * const cmd_line_args,
                        struct cmd_line_arg_t * const cmd_line_arg,
                        int * const cp,
                        size_t * const offsetp,
                        const int argc,
                        char ** const argv,
                        char * const arg,
                        const unsigned int i,
                        Boolean * const success);

void parse_arguments(const int start,
                     const int argc,
                     char ** argv,
                     struct stardata_t * Restrict const stardata)
{
    /*
     * Parse command line arguments and set options into appropriate
     * places inside stardata, especially in the zero_age structure
     *
     * See cmd_line_args.h for the definitive table : that is where
     * they are defined. If you want to add an argument, do it there
     * and rebuild. Do NOT change this function!
     *
     * If argv is non-NULL, we use it as a list of arguments
     * to be parsed.
     *
     * If argv is NULL, we use it to set up the cmd_line_args
     * array in the store.
     *
     * See also argument_setting_functions.c and batchmode.c
     */
    Dprint("PARSE sizeof stardata = %p = %zu, tmpstore = %p = %zu, cmd_line_args = %p, argc = %d\n",
           (void*)stardata,
           sizeof(stardata),
           (void*)(stardata ? stardata->tmpstore : NULL),
           sizeof(stardata->tmpstore),
           (void*)((stardata && stardata->store && stardata->store->argstore) ? stardata->store->argstore->cmd_line_args : NULL),
           argc);

    if(unlikely(argv == NULL))
    {
        /*
         * First-time setup.
         */
        refresh_arguments(stardata,
                          argc,
                          FALSE);

#ifdef HASH_ARGUMENTS
        hash_arguments(stardata,
                       FALSE);
#endif // HASH_ARGUMENTS
    }
    else
    {
        struct store_t * const store = stardata->store;
        struct argstore_t * const argstore = store->argstore;

#undef X
#define X(TYPE,SIZEOF,OPTSTRINGS) #TYPE,
        static const char * const arg_type_strings[] Maybe_unused = { ARG_TYPES_LIST };
#undef X
#define X(TYPE,SIZEOF,OPTSTRINGS) (SIZEOF),
        static const size_t arg_type_sizeof[] Maybe_unused = { ARG_TYPES_LIST };
#undef X
        /*
         * Parse command line arguments
         */
        struct cmd_line_arg_t * const cmd_line_args = argstore->cmd_line_args;

        /*
         * Disable nan and inf checking in here. These are reset later
         * unless set on the command line.
         */
        const Boolean allow_debug_nan_was = stardata->preferences->allow_debug_nan;
        const Boolean allow_debug_inf_was = stardata->preferences->allow_debug_inf;
        stardata->preferences->allow_debug_nan = TRUE;
        stardata->preferences->allow_debug_inf = TRUE;
        Boolean reset_allow_debug_nan = TRUE;
        Boolean reset_allow_debug_inf = TRUE;

        Boolean * success = Calloc((size_t)argc,
                                   sizeof(Boolean));

        for(int parse_number=0; parse_number<ARG_NUM_PARSES; parse_number++)
        {
            /*
             * loop over the arguments, looking for
             * matches and acting accordingly
             */
            for(int c=start; c<argc; c++)
            {
#ifdef DEBUG_FAIL_ON_NAN
                Dprint("CMD LINE ARG %d/%d allow nan ? %d",c,argc,stardata->preferences->allow_debug_nan);
#endif//DEBUG_FAIL_ON_NAN
                Dprint(" is %s : process ",argv[c]);
                char * arg = (char*) argv[c];

                if(arg[0] != '\0')
                {
                    /* ignore leading -- */
                    if(arg[0] == '-' && arg[1] == '-') arg += 2;

                    /*
                     * Special cases : these might be set on the command
                     * line, in which case we shouldn't reset them here.
                     */
                    if(String_startswith(arg, "allow"))
                    {
                        if(Strings_equal(arg,
                                         "allow_debug_nan"))
                        {
                            reset_allow_debug_nan = FALSE;
                        }
                        else if(Strings_equal(arg,
                                              "allow_debug_inf"))
                        {
                            reset_allow_debug_inf = FALSE;
                        }
                    }

#ifdef HASH_ARGUMENTS
                    /*
                     * Try to find the argument in the hash
                     */

                    const struct cdict_entry_t * const e = CDict_nest_get_entry(stardata->store->argcdict,
                                                                                "parse",parse_number,
                                                                                "arguments",arg);
                    if(e != NULL)
                    {
                        /* get parent cdict */
                        unsigned int i;
                        struct cdict_t * cd = e->value.value.cdict_pointer_data;
                        struct cmd_line_arg_t * cmd_line_arg;
                        /* get counter and pointer to the arg details */
                        i = CDict_nest_get_data_value(cd,i,"counter");
                        cmd_line_arg = CDict_nest_get_data_value(cd,
                                                                 (void*)cmd_line_arg,
                                                                 "pointer");
                        /* action the argument */
                        _arg_action(stardata,
                                    cmd_line_args,
                                    cmd_line_arg,
                                    &c,
                                    NULL,
                                    argc,
                                    argv,
                                    arg,
                                    i,
                                    success);
                    }
                    else
                    {
#endif // HASH_ARGUMENTS
                        /*
                         * check this argument against all those in the list
                         */
                        size_t offset = 0;
                        for(unsigned int j = 0; j < argstore->n_cmd_line_args_on_parse[parse_number]; j++)
                        {
                            const unsigned int i = argstore->cmd_line_args_on_parse[parse_number][j];

                            Cprint("CF (parse_number %d) %s %s : is scanf? %d : arg's parse_number %d : parse now? %s\n",
                                   parse_number,
                                   arg,
                                   cmd_line_args[i].name,
                                   Argtype_is_scanf(cmd_line_args[i].type),
                                   cmd_line_args[i].parse_number,
                                   Yesno(cmd_line_args[i].parse_number == parse_number)
                                );

                            if(cmd_line_args[i].parse_number == ARG_PARSE_ALL ||
                               cmd_line_args[i].parse_number == parse_number)
                            {

                                Cprint("check arg %s vs name %s for equal or scanf (parse %d, arg parse number %d)\n",
                                       arg,
                                       cmd_line_args[i].name,
                                       parse_number,
                                       cmd_line_args[i].parse_number);
                                if(
#ifndef HASH_ARGUMENTS
                                    /*
                                     * Constant strings are hashed and match above,
                                     * no check is required here
                                     */
                                    Strings_equal(arg,
                                                  cmd_line_args[i].name)
                                    ||
#endif // HASH_ARGUMENTS
                                    (
                                        Argtype_is_scanf(cmd_line_args[i].type) &&
                                        arg_match_scanf(arg,
                                                        &cmd_line_args[i],
                                                        &offset) == TRUE)
                                    )

                                {
                                    Cprint("match \"%s\" (at arg table %u, type %d %s, pointer %p, function_pointer? %s, size %zu expected %zu (%s) : cf log_filename at %p) repeat = %d, offset = %zu\n",
                                           arg,
                                           i,
                                           cmd_line_args[i].type,
                                           arg_type_strings[cmd_line_args[i].type],
                                           (void*)cmd_line_args[i].pointer,
                                           Yesno(cmd_line_args[i].function_pointer!=NULL),
                                           cmd_line_args[i].varsize,
                                           arg_type_sizeof[cmd_line_args[i].type],
                                           cmd_line_args[i].pointer == NULL ? "Unused" :
                                           arg_type_sizeof[cmd_line_args[i].type] == 0 ? "Unknown size" :
                                           cmd_line_args[i].varsize == arg_type_sizeof[cmd_line_args[i].type] ? "OK" : "NOT OK",
                                           (void*)&(stardata->preferences->log_filename),
                                           stardata->preferences->repeat,
                                           offset
                                        );


                                    _arg_action(stardata,
                                                cmd_line_args,
                                                cmd_line_args+i,
                                                &c,
                                                &offset,
                                                argc,
                                                argv,
                                                arg,
                                                i,
                                                success);

                                    /*
                                     * Matched : break out of i loop
                                     */
                                    break;
                                }
                            }
                        }
                    }
#ifdef HASH_ARGUMENTS
                }
#endif
            } /* loop over cmd line args */
        } /* loo p over parse number */

        /*
         * check all args matched : if one didn't, this is possibly
         * a fatal error
         */
        for(int c=1; c<argc; c++)
        {
            if(success[c] == FALSE)
            {
                if(stardata->preferences->skip_bad_args == SKIP_BAD_ARGS_NONE)
                {
                    Dprint(
                        "Exit because given argument to binary_c, \"%s\" (number %d), failed to match any known argument.",
                        argv[c],
                        c);
                    Exit_binary_c(
                        BINARY_C_SETUP_UNKNOWN_ARGUMENT,
                        "Exit because given argument to binary_c, \"%s\" (number %d), failed to match any known argument (prev args are \"%s\" and \"%s\").",
                        argv[c],
                        c,
                        Batchmode_is_on(stardata->preferences->batchmode) ? "N/A (in batchmode)" : argv[c<=1 ? 0 : (c-2)],
                        Batchmode_is_on(stardata->preferences->batchmode) ? "N/A (in batchmode)" : argv[c<=0 ? 1 : (c-1)]
                        );
                }
                else if(stardata->preferences->skip_bad_args == SKIP_BAD_ARGS_WITH_WARNING)
                {
                    Printf("Skip argument to binary_c, \"%s\" (number %d), which failed to match any known argument (prev args are \"%s\" and \"%s\").\n",
                           argv[c],
                           c,
                           Batchmode_is_on(stardata->preferences->batchmode) ? "N/A (in batchmode)" : argv[c<=1 ? 0 : (c-2)],
                           Batchmode_is_on(stardata->preferences->batchmode) ? "N/A (in batchmode)" : argv[c<=0 ? 1 : (c-1)]
                        );
                    c++;
                }
                else
                {
                    /* skip without warning */
                    c++;
                }
            }
        }
        Safe_free(success);

        Dprint("Finished processing cmd line arguments\n");


        if(reset_allow_debug_nan == TRUE)
        {
            stardata->preferences->allow_debug_nan = allow_debug_nan_was;
        }
        if(reset_allow_debug_inf == TRUE)
        {
            stardata->preferences->allow_debug_inf = allow_debug_inf_was;
        }

        /*
         * Derived variables cases
         */
        derived_arguments(stardata);
    }

    if(stardata->preferences->show_version == TRUE)
    {
        version(stardata);
    }

    Dprint("exit parse_arguments");
}


static void _arg_action(struct stardata_t * const stardata,
                        struct cmd_line_arg_t * const cmd_line_args,
                        struct cmd_line_arg_t * const cmd_line_arg,
                        int * const cp,
                        size_t * const offsetp,
                        const int argc,
                        char ** const argv,
                        char * const arg,
                        const unsigned int i,
                        Boolean * const success)
{
    int c = *cp;
    size_t offset = offsetp ? *offsetp : 0;
    success[c] = TRUE;
    if(cmd_line_arg->pointer == NULL)
    {
        /*
         * If the pointer is NULL, ignore this
         * arg, but skip as many following args
         * as required. This means calling
         * the appropriate subroutine, or
         * simply incrementing the counter (i.e. c++).
         */
        if(Argtype_is_subroutine(cmd_line_arg->type))
        {
            /*
             * Except if it's a subroutine, in which
             * case call the function_pointer
             */
            const int cwas = c;
            cmd_line_arg->function_pointer(ARG_SUBROUTINE_ARGS);
            for(int cc = cwas; cc <= c; cc++)
            {
                success[cc] = TRUE;
            }
        }
        else
        {
            c++;
            success[c] = TRUE;
        }
    }
    else
    {
        /* Act based on argument type */
        switch(cmd_line_arg->type)
        {
        case ARG_NONE:
            /* do nothing */
            break;
        case ARG_DOUBLE:
            Next_arg_check;
            Arg_set_double;
            break;
        case ARG_INTEGER:
            /* integer argument */
            Next_arg_check;
            Arg_set_int;
            break;
        case ARG_UNSIGNED_INTEGER:
            /* unsigned integer argument */
            Next_arg_check;
            Arg_set_unsigned_int;
            break;
        case ARG_INTEGER_SCANF:
            /* integer argument with a scanf for the index */
            Next_arg_check;
            Arg_scanf_set_int;
            break;
        case ARG_BOOLEAN_SCANF:
            /* integer argument with a scanf for the index */
            Next_arg_check;
            Arg_scanf_set_Boolean;
            break;
        case ARG_DOUBLE_SCANF:
            /* integer argument with a scanf for the index */
            Next_arg_check;
            Arg_scanf_set_double;
            break;
        case ARG_INTEGER_OFFSET_SCANF:
            /* integer argument with a scanf for the index */
            Next_arg_check;
            offset--;
            Arg_scanf_set_int;
            break;
        case ARG_BOOLEAN_OFFSET_SCANF:
            /* integer argument with a scanf for the index */
            Next_arg_check;
            offset--;
            Arg_scanf_set_Boolean;
            break;
        case ARG_DOUBLE_OFFSET_SCANF:
            /* integer argument with a scanf for the index */
            Next_arg_check;
            offset--;
            Arg_scanf_set_double;
            break;
        case ARG_LONG_INTEGER:
            /* long integer argument */
            Next_arg_check;
            Arg_set_long_int;
            break;
        case ARG_UNSIGNED_LONG_INTEGER:
            /* long integer argument */
            Next_arg_check;
            Arg_set_unsigned_long_int;
            break;
        case ARG_LONG_LONG_INTEGER:
            /* unsigned long long integer argument */
            Next_arg_check;
            Arg_set_long_long_int;
            break;
        case ARG_UNSIGNED_LONG_LONG_INTEGER:
            /* unsigned long long integer argument */
            Next_arg_check;
            Arg_set_unsigned_long_long_int;
            break;
        case ARG_BOOLEAN:
        case BATCH_ARG_BOOLEAN:
            /* Boolean type set to TRUE */
            Arg_set_boolean_true;
            break;
        case ARG_NOT_BOOLEAN:
        case BATCH_ARG_NOT_BOOLEAN:
            /* Boolean type set to FALSE */
            Arg_set_boolean_false;
            break;
        case ARG_STRING:
            /* string argument : NB no bounds checking! */
            Next_arg_check;
            Arg_set_string;
            break;
        case ARG_UNBOUNDED_STRING:
            /* unbounded string argument : NB no bounds checking! */
            Next_arg_check;
            Arg_set_unbounded_string;
            break;
        case ARG_SUBROUTINE:
        case BATCH_ARG_SUBROUTINE:
            /* subroutine to call */
            Arg_call_subroutine;
            break;
        default:
            /* Matched argument but no idea what to do with it */
            Dprint(
                "Exit because argument type %d is unknown (check cmd_line_args.h at position %u)",
                cmd_line_arg->type,
                i);
            Exit_binary_c(
                BINARY_C_SETUP_UNKNOWN_ARGUMENT,
                "Exit because argument type %d is unknown (check cmd_line_args.h at position %u)",
                cmd_line_arg->type,
                i);
        }
    }
    *cp = c;
    if(offsetp)
    {
        *offsetp = offset;
    }
}
