#include "../binary_c.h"
No_empty_translation_unit_warning;


#define dprint(P,...) Dprint_to_pointer((P),TRUE,__VA_ARGS__);

void set_defaults(struct stardata_t * Restrict const stardata)
{
    /*
     * Set defaults into stardata
     */
    Star_number k;
    Number_of_stars_Starloop(k)
    {
        dprint(stardata,"Call init star for star %d\n",k);
        init_star(stardata,&((stardata)->star[k]));
        dprint(stardata,"Done init star\n");
        (stardata)->star[k].starnum = k;
    }

    dprint(stardata,"call init model\n");
    init_model(&(stardata)->model);

    dprint(stardata,"call init common\n");
    init_common(stardata);

    dprint(stardata,"set default preferences\n");
    set_default_preferences(stardata);

    dprint(stardata,"finished set_defaults\n");
}
