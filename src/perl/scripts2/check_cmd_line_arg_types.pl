#!/usr/bin/env perl
use strict;
use rob_misc;
use Term::ANSIColor;

############################################################
# Script to check the variable types in cmd_line_args.h
#
# Run from in the main binary_c directory. Will exit with an
# error if one is found.
#
############################################################

my $args = {};
my $types = get_types();
my $macros = get_macros();
my $typemap = {
    'double' => 'ARG_DOUBLE',
        'int' => 'ARG_INTEGER',
        'long int' => 'ARG_LONG_INTEGER',
        'unsigned int' => 'ARG_UNSIGNED_INTEGER',
        'Boolean' => 'ARG_BOOLEAN',
};


my $C = slurp('src/setup/cmd_line_args.h');
my $cmd_line_args = ($C=~/\#define\s*CMD_LINE_ARGS.*\s+BATCHMODE_ARGS.*\n((?:.*\n)+)/)[0];

$cmd_line_args=~s/BATCHMODE_ARGS.*//;
$cmd_line_args=~s/\s+\\\n/\n/g;

my @cmd_line_args = split(/\{/,$cmd_line_args);

foreach my $arg (@cmd_line_args)
{
    $arg=~s/\\"//g;
    my @arg_struct_content = split(/,\n/,$arg);
    print "$arg\n";

    map
    {
        s/^\s+//;
        s/\s+$//;
    }@arg_struct_content;

    my $name = $arg_struct_content[1];
    my $var = $arg_struct_content[5];
    my $type = $arg_struct_content[3];

    # there are few subroutines: do them by hand
    next if($type =~/SUBROUTINE/);
    next if($type =~/ARG_STRING/);
    next if($type =~/SCANF/);
    next if($name =~/^\s*$/);
    next if($var =~/\[/);# ignore arrays
    next if($name =~/NULL/);

    $name=~s/^\"//;
    $name=~s/\"$//;

    if($var !~ /^\&/)
    {
        # require a map to a macro
        my $newvar = $macros->{$var};

        if($newvar)
        {
            print "ARG $name of type $type points to $var which is a macro pointing to $newvar\n";
            $var = $newvar;
        }
        else
        {
            print "ARG $name of type $type points to $var which is an undefined macro\n";
            exit;
        }
    }
    next if($var =~/\[/);# ignore arrays

    if($var)
    {
        $var =~s/^\s*\&\s*//;
        $var =~s/^\(//;
        $var =~s/\)$//;
        my $expect_type = $typemap->{$types->{$var}};

        if($expect_type)
        {
            print "ARG $name of type $type points to $var which has type $types->{$var} and we expect $expect_type\n";
            if($type ne $expect_type)
            {
                print color('red'),"FAILED\n",color('reset');
                exit;
            }
        }
        else
        {
            print "ARG $name of type $type points to $var which is of ",color('yellow'),"unknown type\n",color('reset');
            next if($name eq 'phasevol' ||
                    $name eq 'eccentricity' ||
                    $name eq 'orbital_period' ||
                    $name eq 'separation');
            exit;
        }
    }
}

exit;

sub get_types
{
    my $types = {};
    my $f = '/tmp/types.dat';
    `./src/perl/scripts/make_showstruct.pl src/binary_c_structures.h libbinary_c_stardata_t stardata\* expand_pointer_list=common,star,model,preferences,store,tmpstore,discs,thermal_zones outfile=$f`;
    open(my $fp,'<',$f)||die;
    while(<$fp>)
    {
        if(/Printf\ \(\ "STARDATA ([^:]+)\s*\:\s+(\S+)/)
        {
            my $var = $2;
            my $type = $1;
            $type =~s/(unsigned|long|short)int/$1 int/;
            $type=~s/^\s+//;
            $type=~s/\s+$//;

            #print "VAR $var is TYPE $type\n";
            $types->{$var} = $type;
        }
    }
    return $types;
}

sub get_macros
{
    my $macros = {};
    open(my $mfp,'<','src/setup/cmd_line_macros.h')||die;
    while(<$mfp>)
    {
        if(/\#define (\S+) \&\(([^\)]+)\)/ &&
           $2 ne 'NULL')
        {
            $macros->{$1} = $2;
        }
    }
    return $macros;
}
