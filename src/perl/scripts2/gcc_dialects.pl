#!/usr/bin/env perl

use strict;
use LWP::Simple;

my $rooturl = 'https://gcc.gnu.org/onlinedocs/gcc-__VERSION__/gcc/C-Dialect-Options.html';

foreach my $version (
    '8.1.0',
    '9.5.0',
    '10.5.0',
    '11.4.0',
    '12.3.0',
    '13.2.0',
    )
{
    my $url = $rooturl;
    $url =~ s/__VERSION__/$version/;

    #print $url,"\n";
    my $content = get($url);
    my @standards;
    while($content =~ /<dt>&lsquo;<samp(?: class=\"samp\")?>(\S+)<\/samp>&rsquo;<\/dt>/g)
    {
        my $std = $1;
        if($std !~ /\+\+/ && $std !~ /iso/)
        {
            push(@standards,$std);
        }
    }

    printf "%10s : %s\n",
        $version,
        join(' ',sort @standards);
}
