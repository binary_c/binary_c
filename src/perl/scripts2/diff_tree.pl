#!/usr/bin/env perl
use strict;
use warnings 'all';
use File::Find::Rule;
use File::Path;
use File::Basename;
use File::Spec qw/abs2rel/;
use Data::Dumper;
use Cwd;
use Text::Diff;

############################################################
#
# Script to compare to trees and, when a diff is found,
# run kdiff3 to show the differences
#
# If we are given one argument, use the current directory
# as the source, and the argument as the directory with
# which to compare.
#
# If we are given two arguments, use these are the source
# and comparison directories.
#
# If we are given three arguments, use the third to diff.
#
############################################################
my $vb = 0;

my $source_dir = defined $ARGV[1] ? $ARGV[0] : '.';
my $comparison_dir = defined $ARGV[1] ? $ARGV[1] : $ARGV[0];
my $difftool = defined $ARGV[2] ? $ARGV[2] : undef;

if(!defined $source_dir || !defined $comparison_dir)
{
    print "Source or comparison directory not defined\n";
    exit;
}

$source_dir = getcwd if($source_dir eq '.');
$comparison_dir = getcwd if($comparison_dir eq '.');

print "Source     : $source_dir\n";
print "Comparison : $comparison_dir\n";

my $source = filehash($source_dir);
my $comparison = filehash($comparison_dir);
my @fails;


foreach my $fullsourcefile (@{$source->{pathlist}})
{
    print "Full path to sourcefile : $fullsourcefile\n";
    my $relsourcefile = $source->{full_to_rel_map}->{$fullsourcefile};

    # determine the relative path in $comparison that
    # matches $fullsourcefile
    my $match = undef;
    foreach my $file (
        $source->{full_to_rel_map}->{$fullsourcefile},
        $source->{full_to_base_map}->{$fullsourcefile}
        )
    {
        if(defined $file)
        {
            if(defined $comparison->{rel_to_full_map}->{$relsourcefile})
            {
                # first, look for file with matching relative path
                $match = $comparison->{rel_to_full_map}->{$relsourcefile};
                print "Matched full path $match\n" if($vb);
            }
            elsif(defined $comparison->{base_to_full_map}->{$file})
            {
                # match failed, so try to look for files with the same basename
                $match = $comparison->{base_to_full_map}->{$file};
                print "Matched basefile $match\n" if($vb);
            }
        }
        last if defined $match;
    }

    if($match)
    {
        print "Full path to comparison file : $match\n\n\n";
        my $diff = diff($fullsourcefile,
                        $match);
        if($diff)
        {
            if(defined $difftool)
            {
                print "Running $difftool ... \n";
                `$difftool "$fullsourcefile" "$match" 2>&1`;
            }
            else
            {
                print $diff;
            }
        }
        else
        {
            print "No difference\n";
        }
        print "\n############################################################\n\n";
    }
    else
    {
        push(@fails,$fullsourcefile);
    }
}

if(@fails)
{
    print "############################################################\n";
    foreach my $file (@fails)
    {
        print "Could not match $file to any comparison file\n";
    }
    print "############################################################\n";
}


sub filehash
{
    ############################################################
    #
    # given a directory $dir return a hash reference containing:
    #
    # 'pathlist' : a list of full paths
    # 'relpathlist' : a list of paths relative to $dir
    # 'basefilemap' : a map from the base filename (with no dirs) to the full path
    # 'relfilemap' : a map from the full filename path to the relative path
    #
    ############################################################
    my ($dir) = @_;
    my $fullpaths = {};
    my $relpaths = {};
    my $full_to_rel_map = {};
    my $full_to_base_map = {};
    my $rel_to_full_map = {};
    my $base_to_full_map = {};
    my $base_to_rel_map = {};
    my @extensions = ('*.c','*.def','*.h');
    $dir .= '/' if ($dir!~/\/$/);

    for my $fullpath (File::Find::Rule->name(@extensions)->in($dir.'/'))
    {
        $fullpath =~ s!//!/!g;
        my $relpath = File::Spec->abs2rel($fullpath, $dir);
        $relpath =~ s!//!/!g;
        my $base = basename($fullpath);
        $base =~ s!//!/!g;

        if($vb)
        {
            print "FULLPATH $fullpath\n";
            print "RELPATH $relpath\n";
            print "BASE $base\n";
        }
        $fullpaths->{$fullpath} = 1;
        $relpaths->{$relpath} = 1;

        $base_to_full_map->{$base} = $fullpath;
        $base_to_rel_map->{$base} = $relpath;
        $full_to_base_map->{$fullpath} = $base;
        $full_to_rel_map->{$fullpath} = $relpath;
        $rel_to_full_map->{$relpath} = $fullpath;
    }

    return
    {
        pathlist => [sort keys %$fullpaths],
        relpathlist => [sort keys %$relpaths],
        full_to_base_map => $full_to_base_map,
        full_to_rel_map => $full_to_rel_map,
        rel_to_full_map => $rel_to_full_map,
        base_to_rel_map => $base_to_rel_map,
        base_to_full_map => $base_to_full_map,
    };
}
