#!/usr/bin/env perl
use strict; # highly recommended
use 5.16.0; # highly recommended
use distribution_functions;
use binary_grid2; # required
use binary_grid::C; # required (uses C backend )
use rob_misc qw/ncpus/;



############################################################
#
# Example script to demonstrate how to use the
# binary_grid2 module in list mode.
#
# This mean we read from a list of systems, which contains lines like
#
# --M_1 1 --M_2 0.5 --orbital_period 0 --separation 1000 --eccentricity 0.0 --metallicity 0.02 --max_evolution_time 15000 --probability 1
#
# where these are binary_c arguments
# (note they *must* be prefixed with '--').
#
# Change the 'list filename' option (see below)
# to read a different file.
#
# For full documentation, please see binary_grid2.pdf
# in the doc/ directory of binary_c
#
############################################################

# number of computational threads to launch, usually one
# per CPU
my $nthreads = rob_misc::ncpus();

############################################################
# Binary_c should output data that we can understand here.
# There are two ways to do this:
#
# 1) Put output statements, using Printf, in binary_c's
#    log_every_timestep() function. This requires a rebuild
#    of libbinary_c.so and a reinstall of the binary_grid module
#    every time you change the Printf statement.
#
#
# 2) Put a list of hashes in the C_auto_logging grid option
#
#  $population->set(
#    C_auto_logging => {
#        'MY_STELLAR_DATA' =>
#            [
#             'model.time',
#             'star[0].mass',
#             'model.probability',
#             'model.dt'
#            ]
#    });
#
#  where MY_STELLAR_DATA is the key of the hash {...} and is also
#  the header matched in the parse_data function (below). The list [...]
#  contains the names of the variables to be output, which are all
#  assumed to be in stardata.
#
#  This option does not require a rebuild of libbinary_c.so or a
#  reinstall of binary_grid.
#
#
# 3) Put output statements, using Printf, into the C_logging_code
#    grid option
#
#  $population->set( C_logging_code => ' Printf("...\n"); ');
#
#    You have access to the stardata variable, so you can
#    output everything that is available to log_every_timestep();
#
#  This option does not require a rebuild of libbinary_c.so or a
#  reinstall of binary_grid.
#
############################################################
# make a new stellar population
my $population = binary_grid2->new(
    # options can be given now ...
    metallicity => 0.02, # mass fraction of "metals"
    max_evolution_time => 15000, # Myr
    nthreads=>$nthreads, # number of threads,
    check_args=>1, # check that args exist
    );

# ... or options can be set manually later.
$population->set(
    vb=>1, # turn on verbose logging (can be 0,1,2...)
    return_array_refs=>1, # quicker data parsing mode
    log_args=>1,
    sort_args=>1,
    save_args=>1,
    log_args_dir=>'/tmp',
    #log_dt_secs=>1,
    C_auto_logging => {
        'MY_STELLAR_DATA' =>
            [
             'model.time',
             'star[0].mass',
             'model.probability',
             'model.dt'
            ],
    },
## or enter more complicated code yourself:
#
#    C_logging_code => '
#             Printf("MY_STELLAR_DATA %g %g %g %g\n",
#                    stardata->model.time,
#                    stardata->star[0].mass,
#                    stardata->model.probability,
#                    stardata->model.dt);
#                       ',

    );

############################################################
# read systems from a list in the given filename
$population->{_grid_options}->{flexigrid}->{'grid type'} = 'list';
$population->{_grid_options}->{flexigrid}->{'list filename'} = 'src/perl/scripts2/system_list';


############################################################
# scan command line arguments for extra options
$population->parse_args();

############################################################
# link population to custom data parser function
$population->set(
    parse_bse_function_pointer => \&main::parse_data
    );

my %init = $population->initial_abundance_hash('Karakas2002',0.02);
my %isotope_hash = $population->isotope_hash();
my @isotope_list = $population->isotope_list();
my %nuclear_mass_hash = $population->nuclear_mass_hash();
my @nuclear_mass_list = $population->nuclear_mass_list();
my @sources = $population->source_list();
my @ensemble = $population->ensemble_list();

# you can use Data::Dumper to see the contents
# of the above lists and hashes
if(0){
    print Data::Dumper->Dump([
        #\%init,
        #\%isotope_hash,
        #\@isotope_list,
        #\%nuclear_mass_hash,
        \@nuclear_mass_list,
        #\@sources,
        #\@ensemble
                         ]);
}

# uncomment this to show version information
#print $population->evcode_version_string();

# uncomment this to show the evcode's default args list
#print join("\n",@{$population->evcode_args_list()});

# evolution the stellar population (this takes some time)
$population->evolve();

# output the data
output($population);

# done : exit
exit;

############################################################
# subroutines
############################################################

sub parse_data
{
    my ($population, $results) = @_;

    my $progenitor = $population->progenitor();

    # get initial mass and probability for the initial mass function
    my $progenitor_initial_mass =
        $progenitor->{m1} + $progenitor->{m2};

    my $progenitor_probability = $progenitor->{prob};

    # bin initial mass
    my $binned_initial_mass = $population->rebin($progenitor_initial_mass,1.0);

    # initial mass function
    $results->{initial_mass_function}->{$binned_initial_mass} +=
        $progenitor_probability;

    while(1)
    {
        # subsequent calls to tbse_line contain
        # (references to) arrays of data
        my $la = $population->tbse_line();

        #print "DATA @$la\n";

        # first element is the "header" line
        my $header = shift @$la;

        # break out of the loop if this is 'fin'
        last if ($header eq 'fin');

        # check if $header matches one of your
        # expected data lines, if so, act
        if($header eq 'MY_STELLAR_DATA')
        {
            #print "GOT @$la\n";

            # matched MY_STELLAR_DATA header
            #
            # get time, mass, probability etc. as specified above
            #
            # (note that $probability is possibly not the same as
            #  the progenitor's probability!)
            my $time = $la->[0];
            my $mass = $la->[1];
            my $probability = $la->[2];
            my $timestep = $la->[3];

            # bin mass to nearest 1.0 Msun
            $mass = $population->rebin($mass, 1.0);

            # add up the mass distribution
            $results->{mass_distribution}->{$mass} += $probability * $timestep;
        }
    }
}

############################################################

sub output
{
    my ($population) = @_;

    # $results is a hash reference containing
    # the results that were added up in parse_data()
    my $results = $population->results;

    if(0)
    {
        # show results hash in full
        print "############################################################\n";
        print Data::Dumper::Dumper($results);
        print "############################################################\n";
    }

    # output the initial mass function
    foreach my $mass (sort {$a<=>$b} keys %{$results->{initial_mass_function}})
    {
        printf "IMF %g %g\n",$mass,$results->{initial_mass_function}->{$mass};
    }

    # output the mass distribution
    foreach my $mass (sort {$a<=>$b} keys %{$results->{mass_distribution}})
    {
        printf "MASS %g %g\n",$mass,$results->{mass_distribution}->{$mass};
    }
}
