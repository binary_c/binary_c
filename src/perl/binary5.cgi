#!/usr/bin/perl -T
use CGI ":all"; #use the cgi module
use CGI::Carp qw(fatalsToBrowser warningsToBrowser);
use strict;
use subs qw(dprint);
use POSIX qw/log10/;
use List::Util qw(any);

# use the CGI module for our forms

# Perl/CGI script to act as an interface for the binary star evolution program
# 
# version modified to output stellar yields
#
# and now pictures of Roche lobes, coloured stars, surface abundances
# vs time, chemical yields, HRD.
#
# Versions:
#
# 4    : added an HRD, periodic table, timeouts
# 5    : move to Surrey
# 5.01 : bug fix for frozen lock file 

$|=1;
my $command=''; # possible command to execute
my $results=''; # the results of the command executed

# need the colours in here somewhere...
my @colours;
set_up_colours();
my @stellar_types=(
    'Low-Mass Main Sequence (Central H-burning)',
    'Main Sequence (Central H-burning)',
    'Hertzsprung Gap',
    'First Giant Branch',
    'Core Helium Burning',
    'Early Asymptotic Giant Branch',
    'Thermally Pulsing Asymptotic Giant Branch',
    'Main Sequence Naked Helium star',
    'Hertzsprung Gap Naked Helium star',
    'Giant Branch Naked Helium star',
    'Helium White Dwarf',
    'Carbon/Oxygen White Dwarf',
    'Oxygen/Neon White Dwarf',
    'Neutron Star',
    'Black Hole',
    'Massless'
    );

my %stfilter = (
    'Low-Mass Main Sequence'=>1,
    'Main Sequence'=>1,
    'Hertzsprung Gap'=>1,
    'First Giant Branch'=>1,
    'Core Helium Burning'=>1,
    'Early Asymptotic Giant Branch'=>1,
    'Thermally Pulsing Asymptotic Giant Branch'=>1,
    'Main Sequence Naked Helium star'=>1,
    'Hertzsprung Gap Naked Helium star'=>1,
    'Giant Branch Naked Helium star'=>1,
    'Helium White Dwarf'=>0,
    'Carbon/Oxygen White Dwarf'=>0,
    'Oxygen/Neon White Dwarf'=>0,
    'Neutron Star'=>0,
    'Black Hole'=>0,
    'Massless',=>0
    );
$ENV{'PATH'}='/bin:/usr/bin/:/usr/local/bin/';
my $massless = 'γ';

my $initial_separation;

# HTML root directory : this is a kludge to get the CSS
# working because the scripts are not in ~/cgi-bin/
my $htmlroot = 'http://personal.ph.surrey.ac.uk/~ri0005/';

# options hash
my %options=(
    wantimages=>0,
    fixrochesize=>0,
    fixsepscale=>0,
    compress=>0,
    animate=>0,
    abunds=>0,
    vb=>0,
    images_dir=>$htmlroot.'/doc/binary_images/', # location for images
    BINARY_C_DATA => '/user/HS204/ri0005/public/binary_c_data', # binary_c source code
    binary_c_executable=>'/user/HS204/ri0005/public/bin/binary_c-linux-cgi2',
    LD_LIBRARY_PATH=>'/user/HS204/ri0005/lib:/usr/HS204/ri0005/public/lib', # library path
    stacksize=>100000, # require more than the default stack
    lockfile=>'/home/izzard/binary_c.lock', # default /run/lock/binary_c.lock
    lockfile_mode=>0666, # permissions of lockfile 
    lockmaxwait=>15, # max time to wait for the lockfile in seconds (15)
    max_lockfile_age=>600, # max lock file age before reset in seconds (600)
    timeoutcmd=>'/usr/bin/timeout 20', # command timeout (unit is seconds)
    max_command_return=>'25M',

    # for testing:
    #    binary_c_executable=>'/home/izzard/binary_c',
    #    lockmaxwait=>1, # max time to wait for the lockfile in seconds (15)
    #    timeoutcmd=>'/usr/bin/timeout 1', # command timeout (unit is seconds)
    );

# get binary_c isotope number list
my ($version,$isotope_list,$isotope_hash,$element_list,$git_revision) =
    get_code_information();

# output the html header and top bar
htmltop();
#print "Running with lockfile ",`ls -l $options{lockfile}`,"\n";

my $sun='&#x2A00;';
my $subsun='<sub>&#x2A00;</sub>';
my %unit=(
    Msun=>"M$subsun",
    Lsun=>"L$subsun",
    Rsun=>"R$subsun",
    );

#print "Currently offline because of webserver changes\n";
#exit;

my @form=tabs_array();

if(scalar param('state') eq 'Evolve')
{
    ################
    # do evolution #
    ################
    my $args = make_binary_c_argstring();

    # set up the command to run the binary star evolution program
    my $command = external_command("$args legacy_yields 1 colour_log 0 log_arrows 0 log_filename /dev/stdout");
    #print "COMMAND: $command\n<BR>\n";

    # execute the command and store the results
    my $results = runcmd($command);
    dprint "<BR>\$? = $?\n<BR><BR>\$! = $!\n<BR>RESULTS ARE ",$results;

    # need the initial abundances as well
    my %init_abunds;
    my $xx = runcmd("$command --init_abunds_only");
    $init_abunds{$1} = $2 while($xx=~/([A-Zn][a-zA-Z]*\d+)=>([^,]+),/go);

    ################
    # show results #
    ################
    parse_results($command,$results,\%init_abunds);
}
else
{
    ################
    # Options Tabs #
    ################

    # options tabs
    print start_form(),'<ul class="tabs" data-persist="true">';
    for(my $i=0;$i<@form;$i+=2)
    {
        my $tab_name = $form[$i];
        print "<li><a href=\"\#tab$i\">$tab_name</a></li>\n";
    }
    print '<LI>&nbsp;',submit('state','Evolve'),'&nbsp;</LI></UL>';

    for(my $i=0;$i<@form;$i+=2)
    {
        my $tab_id = "tab$i";
        my $tab_name = $form[$i];
        my $tab_contents = $form[$i+1];
        print make_tab($tab_id,$tab_name,$tab_contents);
    }
    print end_form();
}


#the end, always have this!
print "</TD></TR></TABLE></BODY></HTML>\n";

# we're done
exit(0);


################################################################################
################################################################################
############ Subroutines follow this!
################################################################################
################################################################################

sub calc_period_from_sep
{
    # return period from sep,m1,m2 in days
    # as stolen from the actual binary star evolution code
    my $aursun = 214.95;
    my $yeardy = 365.240;
    my ($sep, $m1, $m2) = @_;
    return $yeardy * ($sep/$aursun)*sqrt($sep/($aursun*($m1+$m2)));
}

sub external_command
{
    # construct external binary_c command
    my $command = 'ulimit -s '.$options{stacksize}.'; '.$options{timeoutcmd}.' ';
    foreach my $x (grep {defined $options{$_}} ('LD_LIBRARY_PATH',
                                                'BINARY_C_DATA'))
    {
        $command .= 'env '.$x.'='.$options{$x}.' ';
    }
    $command .= $options{binary_c_executable}.' '.join(' ',@_).' 2>&1';
    return $command;
}

sub get_code_information
{
    # load isotope information
    my @isotope_list;
    my %isotope_hash;
    my @element_list;

    # get version
    my $command = external_command('--version');
    my $x = runcmd($command);

    # make isotope list
    while($x=~/Isotope (\d+) is (\S+) \(mass=\s+\S+ g \(\/amu=\s+\S+, \/MeV=\s+\S+\), Z=(\d+)/g)
    {
        my ($n,$isotope,$atomic_number) = ($1,$2,$3);
        #print "N=$n $isotope $atomic_number\n<BR>";
        $isotope_list[$n] = $isotope;
        $isotope_hash{$isotope} = $n;
        $element_list[$atomic_number] //= {};
        $element_list[$atomic_number]->{isotopes} //= [];
        $element_list[$atomic_number]->{name} = ($isotope=~/^(\D+)/)[0];
        $element_list[$atomic_number]->{atomic_number} = $atomic_number;
        push(@{$element_list[$atomic_number]->{isotopes}}, [$isotope,$n]);
    }

    return(($x=~/Version (.*)/)[0],
           \@isotope_list,
           \%isotope_hash,
           \@element_list,
           ($x=~/git revision \"\"([^\"]+)\"\"/)[0]
        );
}

sub alphanum
{
    # subroutine to sort the isotopes into alphanumerical order
    my $a1;
    my $a2;
    my $b1;
    my $b2;

    if($a=~/(\D+)(\d+)/o)
    {
        $a1=$1; $a2=$2;
    }
    else
    {
        $a1=$a; $a2=2000;
    }

    if($b=~/(\D+)(\d+)/o)
    {
        $b1=$1;$b2=$2;
    }
    else
    {
        $b1=$b;$b2=2000;
    }
    return ($a2!=$b2) ? ($a2 <=> $b2) : ($a1 cmp $b1);
}

sub pretty_firstline
{
    my ($firstline) = @_;

    my @firstline = split(/\s+/,$firstline);
    map
    {
        s/TIME/time\/Myr/;
        s/ M/ <I>M<\/I>/g;
        s/st(\d)/stellar<BR>type $1/g;
        s/SEP/separation/;
        s/PER/period/;
        s/ECC/eccentricity/;
        s/R(\d)\/ROL\d/<i>R<\/i><sub>$1<\/sub>\/<i>R<\/i><sub>L$1<\/sub>/g;
        s/TYPE//;
        $_ = '<TH>'.$_.'</TH>';
    }@firstline;
    $firstline = join(' ',@firstline);

    return $firstline;
}

sub pretty_status
{
    my ($out,$line) = @_;
    if($options{compress})
    {
        $out=~s/INITIAL/t<SUB>0<\/SUB>/go;
        $out=~s/COMENV/Comenv/go;
        $out=~s/BEG_RCHE/RLOF/go;
        $out=~s/KW_CHNGE/Evolve/go;
        $out=~s/MAX_TIME/t<SUB>max<\/SUB>/go;
        $out=~s/END.RCHE/<S>RLOF<\/S>/go;
        $out=~s/BEG_BSS/Blue<BR>Strag/go;
        $out=~s/END_BSS/End Blue<BR>Strag/go;
        $out=~s/BEG_SYMB/Symb/go;
        $out=~s/END_SYMB/<S>Symb<\/S>/go;
        $out=~s/NO_REMNT/Nowt<BR>Left\!/go;
        $out=~s/CO[AE]LESCE/Merge/go;
        $out=~s/CONO_REMNTTACT/Contact/go;
        $out=~s/DISRUPT/Disrupt/go;
        $out=~s/-666\.6/<B>undef<\/B>/go;
        $out=~s/q[-_]inv/Invert <i>q<\/I>/go;
        $out=~s/OFF_MS/Off MS/go;
    }
    else
    {
        $out=~s/INITIAL/Hydrogen ignition/go;
        $out=~s/COMENV/Common Envelope Evolution /go;
        $out=~s/BEG_RCHE/Begin Roche Lobe Overflow /og;
        $out=~s/KW_CHNGE/Stellar Type Change /go;
        $out=~s/TYPE_CHNGE/Stellar Type Change /go;
        $out=~s/MAX_TIME/Maximum Time Reached /og;
        $out=~s/END.RCHE/End of Roche Lobe Overflow /og;
        $out=~s/BEG_BSS/Begin Blue Straggler phase /go;
        $out=~s/END_BSS/End Blue Straggler phase /go;
        $out=~s/BEG_SYMB/Begin Symbiotic /go;
        $out=~s/END_SYMB/End Symbiotic /go;
        $out=~s/NO_REMNT/No Remnant /go;
        $out=~s/CO[AE]LESCE/Stars merge /go;
        $out=~s/CONO_REMNTTACT/Contact Binary /go;
        $out=~s/DISRUPT/Binary Disrupted! /go;
        $out=~s/-666\.6/<B>undef<\/B> /go;
        $out=~s/q[-_]inv/Invert Mass Ratio/go;
    	$out=~s/OFF_MS/Star leaves the main sequence/go;
    	$out=~s/shrinkAGB/Star starts shrinking on the AGB/go;
        $out=~s/SN/Supernova/go;
    }
    $out=~s/\"//g;

    return $out;
}

sub pretty_stellar_type
{
    my ($st) = @_;
    if(!$options{compress})
    {
        my %stellar_types = (
            'LMMS' => 'Convective Low-mass<BR>Main Sequence',
            'MS' => 'Main Sequence',
            'HG' => 'Hertzsprung Gap',
            'GB' => 'First Giant Branch',
            'CHeB' => 'Core Helium Burning',
            'EAGB' => 'Early Asymptotic<BR>Giant Branch',
            'TPAGB' => 'Thermally Pulsing<BR>Asymptotic Giant Branch',
            'HeMS' => 'Main Sequence<BR>Naked Helium star',
            'HeHG' => 'Hertzsprung Gap<BR>Naked Helium star',
            'HeGB' => 'Giant Branch<BR>Naked Helium star',
            'HeWD' => 'Helium<BR>White Dwarf',
            'COWD' => 'Carbon/Oxygen<BR>White Dwarf',
            'ONeWD' => 'Oxygen/Neon<BR>White Dwarf',
            'NS' => 'Neutron Star',
            'BH' => 'Black Hole',
            'γ' => 'Massless'
            );
        $st = $stellar_types{$st};
    }
    return $st;
}

sub rlof_colour
{
    # choose RLOF colour
    my $stellar_type = $_[0];

    return
        ($stellar_type eq 'EAGB' || $stellar_type eq 'TPAGB') ? 'FF0000' : # AGB
        $stellar_type=='GB' ? 'FFAA00' : # GB
        'FFFF00'; # other
}


sub other_formatting
{
    my ($out,$j,$line) = @_;

    #print "<PRE>other $j : $out : $line</PRE><BR>";
    # remove or format SN data, though perhaps it's useful to somebody
    if($options{wantimages})
    {
        # nova and supernova images
        my $snhw=' HEIGHT '.(303*$options{imagescale}).' WIDTH='.(303*$options{imagescale}).' ';
        $snhw=' HEIGHT '.(303*$options{imagescale}).' WIDTH='.(2*303*$options{imagescale}).' ';
        $out=~s/NOVAE/<TR><TD COLSPAN=11><B>Novae<\/B><\/TD><TD ALIGN=CENTER BGCOLOR=\"\#000000\"><IMG $snhw SRC=\"..\/binary_images\/nova_cygni_1992.jpg\" ALT=\"Nova Cygni 1992\"><\/TD><\/TR>/g;

        if($out eq 'kick' &&
           $line=~/SN kick (\S+).*?vk=(\S+).*?dm\(exploder\) = ([^\,]+),?/)
        {
            my $snhw=' HEIGHT '.(303*$options{imagescale}).' WIDTH='.(303*$options{imagescale}).' ';
            $out = "<TR><TD COLSPAN=11><B>Supernova type $1<\/B> - Mass lost to interstellar medium $3 $unit{Msun} - <i>v<\/i><sub>kick<\/sub>=$2<\/TD><TD ALIGN=CENTER BGCOLOR=\"#000000\"><FONT SIZE=10 COLOR=\"\#FFFFFF\"><IMG $snhw SRC=\"$options{images_dir}\/crab_h5m.gif\" ALT=\"The Crab Nebula\"><\/FONT><\/TD>";
        }
    }
    else
    {
        # nova and supernova images
        my $snhw=' HEIGHT '.(303*$options{imagescale}).' WIDTH='.(303*$options{imagescale}).' ';
        $snhw=' HEIGHT '.(303*$options{imagescale}).' WIDTH='.(2*303*$options{imagescale}).' ';
        $out=~s/NOVAE/<TR><TD COLSPAN=12><B>Novae<\/B><\/TD><\/TR>/g;
    }


    # don't care about probability
    $out=~s/Probability.*//g;
    return $out;
}

sub binary_image
{
    my ($suppress_binary_details,@data) = @_;
    my $scale=$options{imagescale}; # overall scaling factor (provided by web form)
    if(!$options{wantimages}) { return; } # no images by default

    # use the xfig generated images to make a nice picture of the binary star

    print '<TD BGCOLOR="#000000"  ALIGN=CENTER><TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0><TR><TD ALIGN=RIGHT>';

    # gap
    if($suppress_binary_details)
    {
        print '<TD>&nbsp;</TD>';
    }

    # left side
    print '<TD>';
    halftable('left',$scale,@data);
    print '</TD>';

    # gap
    if($suppress_binary_details)
    {
        print '<TD WIDTH=33%>&nbsp;</TD>';
    }

    # right side
    print '<TD ALIGN=LEFT>';
    halftable('right',$scale,@data);
    print '</TD>';

    # gap
    if($suppress_binary_details)
    {
        print '<TD>&nbsp;</TD>';
    }
    print '</TR></TABLE></TD>';
}

sub halftable
{
    my $lr = shift @_; # left or right half?
    my $scale = shift @_;
    my @data = @_; # stellar data
    my @ims; # images
    my $offset; # used to determine which star we are
    my $leftscale; # scale width of pointy RLOF
    my $rightscale;


    # choose left or right
    if($lr eq 'left')
    {
        @ims = ('roche_curve_left',
                'rlof_point_right');
        $offset = 0;
        $leftscale = 1;
        $rightscale = 2;
    }
    else
    {
        @ims = ('rlof_point_left',
                'roche_curve_right');
        $offset = 1;
        $leftscale = 2;
        $rightscale = 1;
    }

    print "\n\n<!-- star on the $lr -->\n";

    #  draw half the table
    print "<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0 BGCOLOR=\"#000000\">\n";

    # left : 41x221
    # top : 221x41

    # set the system type
    # gets set to 0 for a single star, or 2 for 
    # a distrupted binary when both survive
    my $system_type = 'binary';
    if($data[3] eq $massless || $data[4] eq $massless || $data[5]<0)
    {
        $system_type = 'single';
    }

    if($data[3] ne $massless && $data[4] ne $massless && $data[5]<=1e-10)
    {
        $system_type = 'disrupted';
    }

    $initial_separation = $data[5] if(!defined $initial_separation);

    if(!$options{fixrochesize} &&
       $system_type == 'binary' &&
       $data[9]!~/COMENV/)
    {
        # scale images to Roche Lobes

        # mass ratio
        my $q = $data[1+$offset]/$data[2-$offset];

        # calculate roche radii
        my ($rol1,$rol2) = (RL($q)*$data[5],RL(1.0/$q)*$data[5]);

        if($rol1<=$rol2)
        {
            # this is the smaller of the lobes - scale down
            $scale*=$rol1/$rol2;
        }
    }


    my $widthfac = ($options{fixsepscale} && $system_type=='binary') ? ($data[5]/$initial_separation) : 1;

    # for comenv special case
    if($data[9] =~/COMENV/o && $lr eq 'left')
    {
        # always a red common envelope
        my $star_image=stellarimage(0.1,3);
        print '<TR><TD  BGCOLOR="#000000" ALIGN=CENTER WIDTH=',$widthfac*2*(221+2*41+41)*$scale,' HEIGHT=',(41+41+221)*$scale,'><IMG  ALT="A common envelope surrounds both stars" WIDTH=',$widthfac*(221+2*41+41)*2*$scale,' HEIGHT=',(41+41+221)*$scale," SRC=\"$star_image\"></TD></TR>\n";
    }
    elsif($data[3] eq $massless && $data[4] eq $massless)
    {
        # nothing left!
    }
    elsif($data[3+$offset] eq $massless)
    {
        # massless remnant, do nothing!
    }
    elsif($system_type ne 'binary')
    {
        # special case - other star no longer exists or not in binary anymore
        # try to make the scale seem realistic
        my $starscale;
        my $stellar_type = $data[3+$offset];

        if($stellar_type =~ /GB$/)
        {
            # giant
            $starscale = $stellar_type ne 'TPAGB' ? 0.75 : 1;
        }
        elsif($stellar_type =~/WD$/ || $stellar_type eq 'BH')
        {
            # WD/BH
            $starscale = 0.1;
        }
        elsif($stellar_type eq 'NS')
        {
            # NS - pretend we're a pulsar
            $starscale = 0.5;
        }
        else
        {
            $starscale = 0.25;
        }

        if($stellar_type == 'NS' || $stellar_type == 'BH' || $stellar_type eq $massless)
        {
            my $star_image = stellarimage($stellar_type,
                                          $data[1+$offset]);
            print "<TR BGCOLOR=\"#000000\" ><TD  ALIGN=$lr BGCOLOR=\"#000000\" ALIGN=CENTER WIDTH=",$scale*$starscale*341,"><IMG  WIDTH=",$scale*$starscale*341," ALT=\"A star\" SRC=\"$star_image\"></TD>";
            if($lr eq 'left')
            {
                print '<TD WIDTH=',$scale*100,'></TD>';
            }
            print '</TR>';
        }
        else
        {
            my $colour=temp_to_rgb($data[10+$offset]);
            print "<TR><TD ALIGN=$lr BGCOLOR=\"$colour\" WIDTH=",$scale*$starscale*341,"><IMG BORDER=0 SRC=\"$options{images_dir}/inverse_circle.gif\" WIDTH=",$scale*$starscale*341," ALT=\"A star\"></TD></TR>";
        }
    }
    else
    {
        my $rlof=0;
        # expand relative to roche lobe
        my $ron = $data[7+$offset];
        my $rlof = $ron >= 1.0;
        if($rlof)
        {
            # R/ROL (max 1.0)
            $ron = 1.0;
        } 

        # make it minimum 0.1 so we can see the star!
        $ron = 0.1 if($ron<0.1);
        my $pix = 221*$scale*$ron;

        # establish colour of the star
        my $colour = temp_to_rgb($data[10+$offset]);
        my $do_images = $system_type eq 'binary' && $rlof==0;

        # if we are RLOFing then have just ONE table cell
        # 303 * 606 in size
        if($ron >= 1.0)
        {
            my $rlofim=$options{images_dir}.'inverse_rlof_point_'.
                ($lr eq 'left' ? 'right' : 'left') . '.gif';

            print "<TR><TD ALIGN=CENTER BGCOLOR=\"$colour\"><IMG SRC=\"$rlofim\"  WIDTH=",344*$scale,' HEIGHT=',303*$scale,'></TD></TR>';
        }
        else
        {
            # top row
            print "<!-- top row -->\n";
            if(!$rlof)
            {
                print '<TR><TD  BGCOLOR="#000000" WIDTH=',$widthfac*41*$scale,' HEIGHT=',41*$scale,'></TD><TD HEIGHT=',41*$scale,' WIDTH=',$widthfac*221*$scale," ALIGN=CENTER BGCOLOR=\"#000000\">\n";
            }

            # if we're binary show Roche lobe (unless it's full!)
            if($do_images)
            {
                print '<IMG HEIGHT=',41*$scale,' WIDTH=',$widthfac*221*$scale," SRC=\"$options{images_dir}/roche_curve_top.gif\" ALT=\"^^^^^\">";
            }
            print '</TD><TD  BGCOLOR="#000000"  WIDTH=',$widthfac*41*$scale,' HEIGHT=',41*$scale,"></TD></TR>\n";

            # middle row
            print "<!-- middle row -->\n<TR>";
            if(!$rlof)
            {
                print ' <TD ALIGN=CENTER BGCOLOR="#000000" HEIGHT=',221*$scale,' WIDTH=',$widthfac*41*$scale*$leftscale,'>';
            }
            if($do_images)
            {
                print "<IMG ALT=\"(\" HEIGHT=",221*$scale," WIDTH=",$widthfac*41*$scale*$leftscale," SRC=\"$options{images_dir}/$ims[0].gif\">\n";
            }
            if(!$rlof)
            {
                print '</TD>';
            }
            print '<TD  BGCOLOR="#000000" ALIGN=CENTER>'; # NB last TD has variable width to fill RLOF

            if($pix<10)
            {
                $pix = $rlof==1 ? 13.71 : 10;
            }

            my $dx=int($scale*0.5*(221-$widthfac*$pix));
            print "<TABLE CELLPADDING=0 BORDER=0 CELLSPACING=0 BORDER=0 ALIGN=CENTER BGCOLOR=\"#000000\"><TR><TD COLSPAN=3></TD></TR><TR><TD></TD><TD ALIGN=CENTER BGCOLOR=\"$colour\" WIDTH=",int($widthfac*$pix)," HEIGHT=",int($pix),"><IMG BORDER=0 SRC=\"$options{images_dir}/inverse_circle.gif\" WIDTH=100%></TD><TD></TD></TR><TR><TD COLSPAN=3></TD></TR></TD></TABLE>";

            if($rlof==0)
            {
                print "</TD><TD  BGCOLOR=\"#000000\"  WIDTH=",$widthfac*41*$scale*$rightscale," HEIGHT=",221*$scale," ALIGN=RIGHT>\n";
            }
            if($do_images)
            {
                print "<IMG ALT=\">\" WIDTH=",$widthfac*41*$scale*$rightscale,' HEIGHT=',221*$scale," SRC=\"$options{images_dir}/$ims[1].gif\">";
            }
            print "</TD></TR>\n";


            # bottom row
            print "<!-- bottom row -->\n";
            if($rlof==0)
            {
                print '<TR><TD  BGCOLOR="#000000" WIDTH=',$widthfac*41*$scale,' HEIGHT=',41*$scale,'></TD><TD WIDTH=',$widthfac*221*$scale," HEIGHT=",41*$scale," ALIGN=CENTER BGCOLOR=\"#000000\">\n";
            }
        }

        # separation and period/single star status
        if($do_images)
        {
            print '<IMG ALT="^^^^^^" HEIGHT=',$scale*41,' WIDTH=',$widthfac*221*$scale," SRC=\"$options{images_dir}/roche_curve_bottom.gif\">\n"
                ,"</TD><TD  BGCOLOR=\"#000000\" HEIGHT=",41*$scale," WIDTH=",$widthfac*41*$scale,"></TD></TR>\n";
        }

    }

    # end the table
    print "</TABLE> <!-- lr=$lr binary=$system_type -->\n\n\n";
    if($lr eq 'right' && $system_type eq 'binary')
    {
        my $fontsize = 5 * $scale;
        print "<TR BGCOLOR=\"\#000000\"><TD  BGCOLOR=\"#000000\" COLSPAN=3 ALIGN=CENTER><FONT SIZE=$fontsize COLOR=\"\#FFFFFF\">&#10229;&nbsp;",
            ($data[5]*214.95>10000000 ? 'a long way' : sprintf('%2.1f AU',$data[5]/214.95)),"&nbsp;&#10230;</TD></TR>\n";

        my $p = calc_period_from_sep(
            $data[5], # sep
            $data[1], # m1
            $data[2]  # m2
            );

        print "<TR BGCOLOR=\"\#000000\"><TD  BGCOLOR=\"#000000\" COLSPAN=3 ALIGN=CENTER><FONT SIZE=$fontsize COLOR=\"\#FFFFFF\">";
        if($p<2.0)
        {
            printf '%2.1f hours',$p*24;
        }
        elsif($p<365.25)
        {
            printf '%2.1f days',$p;
        }
        else
        {
            printf '%2.1f years',$p/365.25;
        }
        print "</TD></TR>\n";
    }
    elsif($lr eq 'right' &&
          $system_type ne 'binary' &&
          $data[9]!~/COMENV/)
    {
        my $fontsize = 5 * $scale;
        print "<TR BGCOLOR=\"\#000000\"><TD BGCOLOR=\"#000000\" COLSPAN=6 ALIGN=CENTER><FONT SIZE=$fontsize COLOR=\"\#FFFFFF\">Single Star";
        print 's' if($system_type eq 'disrupted' && !($data[3] == 'γ' || $data[4] == 'γ'));
        print "</FONT></TD></TR>\n";
    }
    print "<!-- end of $lr star -->\n";
}

sub stellarimage
{
    # map stellar types to images of stars
    my ($stellar_type, $mass, $rlof, $lr) = @_;
    my $im; # return value

    # normal stars
    my %stellar_images = (
        'LMMS' => 'round_red', # MS low-mass
        'MS' => 'round_yellow', # MS normal
        'HG' => 'round_gold', # HG
        'GB' => 'round_red', # GB
        'CHeB' => 'round_cyan', # CHeB (might be blue or red!)
        'EAGB' => 'round_red', # EAGB
        'TPAGB' => 'round_red', # TPAGB
        'HeMS' => 'round_cyan', # HeMS
        'HeHG' => 'round_gold', # HeHG
        'HeGB' => 'round_red', # HeGB,
        'HeWD' => 'round_white', # HeWD
        'COWD' => 'round_white', # COWD
        'ONeWD' => 'round_white', # ONeWD
        'NS' => 'Pulsar', # NS
        'BH' => 'round_black' # BH
        );

    # for pulsars you can choose between options{animate}d gif
    # or non-options{animate}d jpeg
    $stellar_images{NS} = $options{animate}==1 ? 'Pulsar' : 'pulsar';
    $im = $stellar_images{$stellar_type};

    my $tail = ($stellar_type eq 'NS' &&
                !$options{animate}) ? 'jpg' : 'gif';

    if($stellar_type ne 'NS' && $stellar_type ne 'BH' && $rlof==1)
    {
        # flip direction
        $lr = $lr eq 'right' ? 'left' : 'right';

        # require RLOFing stars
        $im=~s/round_(.*)/roche_lobe_/;
        $im .= $lr.'_filled_'.$1;
    }

    return($options{images_dir}.$im.'.'.$tail);
}

sub temp_to_rgb
{
    my ($teff) = @_;
    my $i;
    my $colour;

    for($i=0; $i<$#colours; $i++)
    {
        # loop through colours, search for the next
        # one hotter than our star
        if(
            $colours[$i] > $teff
            ||
            $i == $#colours-1 # off the scale
            )
        {
            $colour = $colours[$i];
            $colour =~ s/^\s*\d+\s+//;
            #$i = $#colours + 1; # break loop
            last;
        }
    }
    chomp($colour);
    return($colour);
}

sub set_up_colours
{
    @colours=('1000 #ff3800','1100 #ff4700','1200 #ff5300','1300 #ff5d00','1400 #ff6500','1500 #ff6d00','1600 #ff7300','1700 #ff7900','1800 #ff7e00','1900 #ff8300','2000 #ff8912','2100 #ff8e21','2200 #ff932c','2300 #ff9836','2400 #ff9d3f','2500 #ffa148','2600 #ffa54f','2700 #ffa957','2800 #ffad5e','2900 #ffb165','3000 #ffb46b','3100 #ffb872','3200 #ffbb78','3300 #ffbe7e','3400 #ffc184','3500 #ffc489','3600 #ffc78f','3700 #ffc994','3800 #ffcc99','3900 #ffce9f','4000 #ffd1a3','4100 #ffd3a8','4200 #ffd5ad','4300 #ffd7b1','4400 #ffd9b6','4500 #ffdbba','4600 #ffddbe','4700 #ffdfc2','4800 #ffe1c6','4900 #ffe3ca','5000 #ffe4ce','5100 #ffe6d2','5200 #ffe8d5','5300 #ffe9d9','5400 #ffebdc','5500 #ffece0','5600 #ffeee3','5700 #ffefe6','5800 #fff0e9','5900 #fff2ec','6000 #fff3ef','6100 #fff4f2','6200 #fff5f5','6300 #fff6f8','6400 #fff8fb','6500 #fff9fd','6600 #fef9ff','6700 #fcf7ff','6800 #f9f6ff','6900 #f7f5ff','7000 #f5f3ff','7100 #f3f2ff','7200 #f0f1ff','7300 #eff0ff','7400 #edefff','7500 #ebeeff','7600 #e9edff','7700 #e7ecff','7800 #e6ebff','7900 #e4eaff','8000 #e3e9ff','8100 #e1e8ff','8200 #e0e7ff','8300 #dee6ff','8400 #dde6ff','8500 #dce5ff','8600 #dae4ff','8700 #d9e3ff','8800 #d8e3ff','8900 #d7e2ff','9000 #d6e1ff','9100 #d4e1ff','9200 #d3e0ff','9300 #d2dfff','9400 #d1dfff','9500 #d0deff','9600 #cfddff','9700 #cfddff','9800 #cedcff','9900 #cddcff','10000 #ccdbff','10100 #cbdbff','10200 #cadaff','10300 #c9daff','10400 #c9d9ff','10500 #c8d9ff','10600 #c7d8ff','10700 #c7d8ff','10800 #c6d8ff','10900 #c5d7ff','11000 #c4d7ff','11100 #c4d6ff','11200 #c3d6ff','11300 #c3d6ff','11400 #c2d5ff','11500 #c1d5ff','11600 #c1d4ff','11700 #c0d4ff','11800 #c0d4ff','11900 #bfd3ff','12000 #bfd3ff','12100 #bed3ff','12200 #bed2ff','12300 #bdd2ff','12400 #bdd2ff','12500 #bcd2ff','12600 #bcd1ff','12700 #bbd1ff','12800 #bbd1ff','12900 #bad0ff','13000 #bad0ff','13100 #b9d0ff','13200 #b9d0ff','13300 #b9cfff','13400 #b8cfff','13500 #b8cfff','13600 #b7cfff','13700 #b7ceff','13800 #b7ceff','13900 #b6ceff','14000 #b6ceff','14100 #b6cdff','14200 #b5cdff','14300 #b5cdff','14400 #b5cdff','14500 #b4cdff','14600 #b4ccff','14700 #b4ccff','14800 #b3ccff','14900 #b3ccff','15000 #b3ccff','15100 #b2cbff','15200 #b2cbff','15300 #b2cbff','15400 #b2cbff','15500 #b1cbff','15600 #b1caff','15700 #b1caff','15800 #b1caff','15900 #b0caff','16000 #b0caff','16100 #b0caff','16200 #afc9ff','16300 #afc9ff','16400 #afc9ff','16500 #afc9ff','16600 #afc9ff','16700 #aec9ff','16800 #aec9ff','16900 #aec8ff','17000 #aec8ff','17100 #adc8ff','17200 #adc8ff','17300 #adc8ff','17400 #adc8ff','17500 #adc8ff','17600 #acc7ff','17700 #acc7ff','17800 #acc7ff','17900 #acc7ff','18000 #acc7ff','18100 #abc7ff','18200 #abc7ff','18300 #abc7ff','18400 #abc6ff','18500 #abc6ff','18600 #aac6ff','18700 #aac6ff','18800 #aac6ff','18900 #aac6ff','19000 #aac6ff','19100 #aac6ff','19200 #a9c6ff','19300 #a9c5ff','19400 #a9c5ff','19500 #a9c5ff','19600 #a9c5ff','19700 #a9c5ff','19800 #a9c5ff','19900 #a8c5ff','20000 #a8c5ff','20100 #a8c5ff','20200 #a8c5ff','20300 #a8c4ff','20400 #a8c4ff','20500 #a8c4ff','20600 #a7c4ff','20700 #a7c4ff','20800 #a7c4ff','20900 #a7c4ff','21000 #a7c4ff','21100 #a7c4ff','21200 #a7c4ff','21300 #a6c4ff','21400 #a6c3ff','21500 #a6c3ff','21600 #a6c3ff','21700 #a6c3ff','21800 #a6c3ff','21900 #a6c3ff','22000 #a6c3ff','22100 #a5c3ff','22200 #a5c3ff','22300 #a5c3ff','22400 #a5c3ff','22500 #a5c3ff','22600 #a5c3ff','22700 #a5c2ff','22800 #a5c2ff','22900 #a5c2ff','23000 #a4c2ff','23100 #a4c2ff','23200 #a4c2ff','23300 #a4c2ff','23400 #a4c2ff','23500 #a4c2ff','23600 #a4c2ff','23700 #a4c2ff','23800 #a4c2ff','23900 #a4c2ff','24000 #a3c2ff','24100 #a3c2ff','24200 #a3c1ff','24300 #a3c1ff','24400 #a3c1ff','24500 #a3c1ff','24600 #a3c1ff','24700 #a3c1ff','24800 #a3c1ff','24900 #a3c1ff','25000 #a3c1ff','25100 #a2c1ff','25200 #a2c1ff','25300 #a2c1ff','25400 #a2c1ff','25500 #a2c1ff','25600 #a2c1ff','25700 #a2c1ff','25800 #a2c1ff','25900 #a2c0ff','26000 #a2c0ff','26100 #a2c0ff','26200 #a2c0ff','26300 #a2c0ff','26400 #a1c0ff','26500 #a1c0ff','26600 #a1c0ff','26700 #a1c0ff','26800 #a1c0ff','26900 #a1c0ff','27000 #a1c0ff','27100 #a1c0ff','27200 #a1c0ff','27300 #a1c0ff','27400 #a1c0ff','27500 #a1c0ff','27600 #a1c0ff','27700 #a1c0ff','27800 #a0c0ff','27900 #a0c0ff','28000 #a0bfff','28100 #a0bfff','28200 #a0bfff','28300 #a0bfff','28400 #a0bfff','28500 #a0bfff','28600 #a0bfff','28700 #a0bfff','28800 #a0bfff','28900 #a0bfff','29000 #a0bfff','29100 #a0bfff','29200 #a0bfff','29300 #9fbfff','29400 #9fbfff','29500 #9fbfff','29600 #9fbfff','29700 #9fbfff','29800 #9fbfff','29900 #9fbfff','30000 #9fbfff','30100 #9fbfff','30200 #9fbfff','30300 #9fbfff','30400 #9fbeff','30500 #9fbeff','30600 #9fbeff','30700 #9fbeff','30800 #9fbeff','30900 #9fbeff','31000 #9fbeff','31100 #9ebeff','31200 #9ebeff','31300 #9ebeff','31400 #9ebeff','31500 #9ebeff','31600 #9ebeff','31700 #9ebeff','31800 #9ebeff','31900 #9ebeff','32000 #9ebeff','32100 #9ebeff','32200 #9ebeff','32300 #9ebeff','32400 #9ebeff','32500 #9ebeff','32600 #9ebeff','32700 #9ebeff','32800 #9ebeff','32900 #9ebeff','33000 #9ebeff','33100 #9ebeff','33200 #9dbeff','33300 #9dbeff','33400 #9dbdff','33500 #9dbdff','33600 #9dbdff','33700 #9dbdff','33800 #9dbdff','33900 #9dbdff','34000 #9dbdff','34100 #9dbdff','34200 #9dbdff','34300 #9dbdff','34400 #9dbdff','34500 #9dbdff','34600 #9dbdff','34700 #9dbdff','34800 #9dbdff','34900 #9dbdff','35000 #9dbdff','35100 #9dbdff','35200 #9dbdff','35300 #9dbdff','35400 #9dbdff','35500 #9dbdff','35600 #9cbdff','35700 #9cbdff','35800 #9cbdff','35900 #9cbdff','36000 #9cbdff','36100 #9cbdff','36200 #9cbdff','36300 #9cbdff','36400 #9cbdff','36500 #9cbdff','36600 #9cbdff','36700 #9cbdff','36800 #9cbdff','36900 #9cbdff','37000 #9cbdff','37100 #9cbdff','37200 #9cbcff');
}

sub make_tab
{
    # tab id
    my $tab_id=$_[0];

    # tab name is text
    my $tab_name=$_[1];

    # tab_contents is an array of hashes, each is a line in the tab's table
    my $tab_contents=$_[2];

    # final output
    my @output;

    # line is a hash containing the details of one line in the table
    foreach my $line (@$tab_contents)
    {
        push(@output,make_tab_line($line));
    }

    return "<div class='optionsTab' id=\"$tab_id\"><TABLE>".
        join('',@output)."</TABLE></div>";
}

sub make_tab_line
{
    my $line = $_[0];

    my @output;

    push(@output,
         "<TD WIDTH=50%>\n",

         # push either the text or another object
         ref($$line{name}) eq 'HASH' ?
         make_tab_object($$line{name}) :
         $$line{name} ,
         "</TD>\n<TD WIDTH=50%>\n",

         make_tab_object($line),

         "</TD>\n",
        );

    return join("\n",'<TR>',@output,"</TR>\n");
}

sub make_tab_object
{
    my $line = $_[0];

    if($$line{type} eq 'text')
    {
        return textfield(-name=>$$line{param},
                         -default=>$$line{default})
            .' '.$$line{unit};
    }
    elsif($$line{type} eq 'dropdown')
    {
        # use the default if it's a key of the values hash
        my $x = defined $$line{values}{$$line{default}} ? $$line{default} : undef;

        # otherwise use the value to find the key
        $x = (grep { $$line{values}{$_} eq $$line{default} } keys %{$$line{values}})[0] if(!(defined $x));

        #my $k = join('+',keys %{$$line{values}});

        # keys of the values hash
        my @vals = sort {
            ($$line{values}{$a} <=> $$line{values}{$b}) ||
                ($a<=>$b) ||
                ($a cmp $b)
        } keys %{$$line{values}};

        #my $v = join('-',@vals);

        if(0){
            # newer CGI bin would take this. sigh.
            return popup_menu(-name=>$$line{param},
                              -default=>$x
                              -values=>\@vals);
        }
        return popup_menu($$line{param},[@vals],$x);
    }
    elsif($$line{type} eq 'tickbox')
    {
        return checkbox(-name=>$$line{param},
                        -value=>'on',
                        -label=>'',
                        -checked=>($$line{default}==1 ||
                                   $$line{default}=~/on/i ||
                                   $$line{default}=~/true/i ||
                                   $$line{default}=~/ticked/i)
            );
    }
}


sub htmltop
{
    print header(),
        '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<HTML><HEAD>
<link rel="stylesheet" type="text/css" media="screen,print" href="'.$htmlroot.'/css/rgi-ioa.css" />
<script src="'.$htmlroot.'/jscript/tabcontent.js" type="text/javascript"></script>
<script src="'.$htmlroot.'/jscript/Chart.bundle.js"></script>
<script src="'.$htmlroot.'/jscript/hammer.min.js"></script>
<script src="'.$htmlroot.'/jscript/chart.js/chart.zoom.js"></script>
    <style>
    canvas {
        -moz-user-select: none;
        -webkit-user-select: none;
        -ms-user-select: none;
    }
    </style>
<script src="'.$htmlroot.'/jscript/tabcontent.js" type="text/javascript"></script>
<link href="'.$htmlroot.'/css/tabs-binary_c.css" rel="stylesheet" type="text/css" />
<TITLE>binary_c frontend</TITLE></HEAD>
<BODY>
<TABLE width="100%">
  <TR>
    <TD align="center" id="header_gradient">
      <H1>Binary_c Online</H1>
      A frontend to the <A HREF="'.$htmlroot.'/binary_c.html"><i>binary_c</I></A> code, version '.$version.', git rev '.$git_revision.'.
    </TD>
  </TR>
<TR><TD align="center" id="header_gradient">If you use the results of <i>binary_c</i> for commercial, academic or published work, please read the <A HREF="https://gitlab.com/binary_c/binary_c/-/blob/master/LICENCE">LICENCE</A> file.<BR>
<a href="https://binary_c.gitlab.io/binary_c.html">Documentation</a> - <A HREF="http://www.gitlab.com/binary_c">code at gitlab</A></TD></TR>
</TABLE>
<TABLE width="100%" id="main_table" VALIGN="top">
  <TR>
    <TD VALIGN=TOP>
    <HR>
';
}

sub tabs_array
{
    my %stellar_types;
    {
        my $i=0;
        %stellar_types = map{ $_=>$i++ }@stellar_types 
    }

    # define the options tabs
    (
     'Basic'=>[
         {
             name=>'Mass 1', # onscreen name
             unit=>$unit{Msun}, # unit
             param=>'M_1', # parameter name
             type=>'text', # type, can be: text, dropdown, tickbox
             default=>'16.0', # default value
             min=>0.0, # minimum value
             max=>100.0, # maximum value
         },
         {
             name=>'Mass 2', # onscreen name
             unit=>$unit{Msun}, # unit
             param=>'M_2', # parameter name
             type=>'text', # type, can be: text, dropdown, tickbox
             default=>'12.0', # default value
             min=>0.0,
             max=>100.0,
         },
         {
             name=>'Metallicity', # onscreen name
             param=>'metallicity', # parameter name
             type=>'text', # type, can be: text, dropdown, tickbox
             default=>'0.016', # default value
             min=>1e-4,
             max=>0.03,
         },
         {
             name=>'Stellar type 1', # onscreen name
             param=>'stellar_type_1', # parameter name
             type=>'dropdown', # type, can be: text, dropdown, tickbox
             default=>1, # default value
             values=>{%stellar_types},
         },
         {
             name=>'Stellar type 2', # onscreen name
             param=>'stellar_type_2', # parameter name
             type=>'dropdown', # type, can be: text, dropdown, tickbox
             default=>1, # default value
             values=>{%stellar_types},
         },

         {
             name=>'Rotational velocity of star 1', # onscreen name
             unit=>'km/s (0=Hurley et al 2002, use 0.1 for zero)', # unit
             param=>'vrot1', # parameter name
             type=>'text', # type, can be: text, dropdown, tickbox
             default=>'0.1', # default value
             min=>-0.1,
             max=>1e10,
         },
         {
             name=>'Rotational velocity of star 2', # onscreen name
             unit=>'km/s (0=Hurley et al 2002, use 0.1 for zero)', # unit
             param=>'vrot2', # parameter name
             type=>'text', # type, can be: text, dropdown, tickbox
             default=>'0.1', # default value
             min=>-0.1,
             max=>1e10,
         },
         {
             name=>'Max. Evolution Time', # onscreen name
             unit=>'Myr',
             param=>'max_evolution_time', # parameter name
             type=>'text', # type, can be: text, dropdown, tickbox
             default=>'13700', # default value
             min=>0.1,
             max=>50000,
         },
     ],

     'Orbit'=>[
         {
             name=>{
                 name=>'Period or Separation', # onscreen name
                 param=>'per-or-sep-choice', # parameter name
                 type=>'dropdown', # type, can be: text, dropdown, tickbox
                 values=>{
                     'Period/days'=>0,
                         "Separation/Rsun"=>1
                 },
                 default=>1,
                 dummy=>1,
             },
             param=>'orbital_period',
             type=>'text',
             default=>'1000.0',
             min=>0.1,
             max=>1e8,
             evalcode=>'
    if( scalar param("per-or-sep-choice") !~/period/i)
    {
        # convert separation to period
        $value = calc_period_from_sep($value,scalar param("M_1"),scalar param("M_2"));
    }
',
	},
	{
	    name=>'Eccentricity', # onscreen name
	    param=>'eccentricity', # parameter name
	    type=>'text', # type, can be: text, dropdown, tickbox
	    default=>'0.0', # default value
	    min=>0.0,
	    max=>0.9999999,
	},
     ],


     'Explosions'=>[
         {
             name=>'SN Remnant Mass',
             param=>'BH_prescription',
             type=>'dropdown',
             default=>'BH_HURLEY2002',
             values=>
             {
                 'Hurley et al. 2002'=>'BH_HURLEY2002',
                     'Belczynski'=>'BH_BELCZYNSKI',
                     'Spera 2015'=>'BH_SPERA2015',
                     'Fryer 2012 delayed'=>'BH_FRYER12_DELAYED',
                     'Fryer 2012 rapid'=>'BH_FRYER12_RAPID',
                     'Fryer 2012 startrack'=>'BH_FRYER12_STARTRACK',
             }
         },
         {
             name=>'SN kick distribution for SN IBC',
             param=>'sn_kick_distribution_IBC',
             type=>'dropdown',
             default=>'KICK_VELOCITY_MAXWELLIAN',
             values=>
             {
                 'Fixed'=>'KICK_VELOCITY_FIXED',
                     'Maxwellian'=>'KICK_VELOCITY_MAXWELLIAN',
             }
         },
         {
             name=>'SN kick distribution for SN II',
             param=>'sn_kick_distribution_II',
             type=>'dropdown',
             default=>'KICK_VELOCITY_MAXWELLIAN',
             values=>
             {
                 'Fixed'=>'KICK_VELOCITY_FIXED',
                     'Maxwellian'=>'KICK_VELOCITY_MAXWELLIAN',
             }
         },
         {
             name=>'SN kick speed dispersion for SN IBC', # onscreen name
             unit=>'km/s',
             param=>'sn_kick_dispersion_IBC', # parameter name
             type=>'text', # type, can be: text, dropdown, tickbox
             default=>'190', # default value
             min=>0.0,
             max=>1e10.0,
         },
         {
             name=>'SN kick speed dispersion for SN II', # onscreen name
             unit=>'km/s',
             param=>'sn_kick_dispersion_II', # parameter name
             type=>'text', # type, can be: text, dropdown, tickbox
             default=>'190', # default value
             min=>0.0,
             max=>1e10.0,
         },
         {
             name=>'Nova retention fraction (hydrogen novae)', # onscreen name
             param=>'nova_retention_fraction_H', # parameter name
             type=>'text', # type, can be: text, dropdown, tickbox
             default=>'1e-3', # default value
             min=>0.0,
             max=>1.0,
         },
         {
             name=>'Nova retention fraction (helium novae)', # onscreen name
             param=>'nova_retention_fraction_He', # parameter name
             type=>'text', # type, can be: text, dropdown, tickbox
             default=>'1e-3', # default value
             min=>0.0,
             max=>1.0,
         },
     ],

     'Binary'=>[
         {
             name=>"RLOF non-conservative \&gamma", # onscreen name
             param=>'nonconservative_angmom_gamma', #     parameter name
             type=>'dropdown', # type, can be: text, dropdown, tickbox
             default=>'RLOF_NONCONSERVATIVE_GAMMA_ISOTROPIC', # default value
             values=>{
                 'Donor'=>'RLOF_NONCONSERVATIVE_GAMMA_DONOR',
                     'Isotropic'=>'RLOF_NONCONSERVATIVE_GAMMA_ISOTROPIC',
             }
         },
         {
             name=>'RLOF rate multiplier', # onscreen name
             param=>'RLOF_mdot_factor', # parameter name
             type=>'text', # type, can be: text, dropdown, tickbox
             default=>'0.0', # default value
             min=>0.0,
             max=>1e20,
         },
         {
             name=>'RLOF Method',
             param=>'RLOF_method',
             type=>'dropdown',
             default=>'RLOF_METHOD_CLAEYS',
             values=>{
                 'Hurley et al. 2002'=>'RLOF_METHOD_BSE',
                     'Claeys et al 2013'=>'RLOF_METHOD_CLAEYS',
             }
         },
         {
             name=>'<FONT FACE="Helvetica">&alpha;</FONT> CE', # onscreen name
                                param=>'alpha_ce', # parameter name
                                type=>'text', # type, can be: text, dropdown, tickbox
                                default=>'0.2', # default value
                                min=>0.0,
                                max=>10000000.0,
         },
         {
             name=>'<FONT FACE="Helvetica">&lambda;</FONT> CE', # onscreen name
                                 param=>'lambda_ce', # parameter name
                                 type=>'text', # type, can be: text, dropdown, tickbox
                                 default=>'-1.0', # default value
                                 min=>-2,
                                 max=>10000000.0,
         },
         {
             name=>'<FONT FACE="Helvetica">&lambda;</FONT> ion', # onscreen name
                                 param=>'lambda_ionisation', # parameter name
                                 type=>'text', # type, can be: text, dropdown, tickbox
                                 default=>'0.0', # default value
                                 min=>0.0,
                                 max=>10000.0,
         },


         #{
         #    name=>'<FONT FACE="Helvetica">&gamma;</FONT> CE', # onscreen name
         #    param=>'nelemans_gamma', # parameter name
         #    type=>'text', # type, can be: text, dropdown, tickbox
         #    default=>'1.5', # default value
         #    min=>-1000000.0,
         #    max=>10000000.0,
         #},

         {
             name=>'Merger Angular Momentum Factor', # onscreen name
             param=>'merger_angular_momentum_factor', # parameter name
             type=>'text', # type, can be: text, dropdown, tickbox
             default=>'1.0', # default value
             min=>0.0,
             max=>1e20,
         },

         {
             name=>'Eddington limit multiplier', # onscreen name
             param=>'accretion_limit_eddington_steady_multiplier', # parameter name
             type=>'text', # type, can be: text, dropdown, tickbox
             default=>'1.0', # default value
             min=>-0.01,
             max=>1e20,
         },

         {
             name=>'Tidal strength factor', # onscreen name
             param=>'tidal_strength_factor', # parameter name
             type=>'text', # type, can be: text, dropdown, tickbox
             default=>'1.0', # default value
             min=>0.0,
             max=>1e20,
         },
         {
             name=>'Tides E<SUB>2</SUB> prescription',
             param=>'E2_prescription',
             type=>'dropdown',
             values=>{
                 'Hurley et al. 2002 (based on Zahn ZAMS)'=>'E2_HURLEY_2002',
                     'Siess, Izzard et al. 2013'=>'E2_IZZARD'
             }
         },
     ],
     'Wind'=>[
         {
             name=>'Giant Branch Wind Loss Prescription',
             param=>'gbwind',
             default=>0,
             type=>'dropdown',
             values=>{
                 'Reimers'=> 'GB_WIND_REIMERS',
                     'Schroeder and Cuntz 2005'=> 'GB_WIND_SCHROEDER_CUNTZ_2005',
                     'Goldman et al. (2017)'=> 'GB_WIND_GOLDMAN_ETAL_2017',
                     'Beasor et al. (2020)' => 'GB_WIND_BEASOR_ETAL_2020'
             }
         },
         {
             name=>'Giant Branch Wind Multiplier', # onscreen name
             param=>'gb_reimers_eta', # parameter name
             type=>'text', # type, can be: text, dropdown, tickbox
             default=>'0.5', # default value
             min=>0.0,
             max=>1e20,
         },
         {
             name=>'TPAGB Wind Loss Prescription',
             param=>'tpagbwind',
             type=>'dropdown',
             values=>{
                 'Vassiliadis and Wood 1993 (as in Karakas et al. 2002)'=> 'TPAGB_WIND_VW93_KARAKAS',
                     'Vassiliadis and Wood 1993 (as in Hurley et al. 2002)'=> 'TPAGB_WIND_VW93_ORIG',
                     'Reimers'=> 'TPAGB_WIND_REIMERS',
                     'Bloecker'=> 'TPAGB_WIND_BLOECKER',
                     'Van Loon'=> 'TPAGB_WIND_VAN_LOON',
                     'Goldman et al. (2017)' => 'TPAGB_WIND_GOLDMAN_ETAL_2017',
                     'Beasor et al. (2020)' => 'TPAGB_WIND_BEASOR_ETAL_2020',
             }
         },

         {
             name=>'TPAGB VW93 Multiplier', # onscreen name
             param=>'vw93_multiplier', # parameter name
             type=>'text', # type, can be: text, dropdown, tickbox
             default=>'1.0', # default value
             min=>0.0,
             max=>1e20,
         },
         {
             name=>'TPAGB VW93 Mira Period Shift', # onscreen name
             unit=>'days',
             param=>'vw93_mira_shift', # parameter name
             type=>'text', # type, can be: text, dropdown, tickbox
             default=>'0.0', # default value
             min=>-1e20,
             max=>1e20,
         },
         {
             name=>'TPAGB Reimers Multiplier', # onscreen name
             param=>'tpagb_reimers_eta', # parameter name
             type=>'text', # type, can be: text, dropdown, tickbox
             default=>'1.0', # default value
             min=>0.0,
             max=>1e20,
         },
         {
             name=>'TPAGB Superwind Mira Period', # onscreen name
             unit=>'days',
             param=>'superwind_mira_switchon', # parameter name
             type=>'text', # type, can be: text, dropdown, tickbox
             default=>'500.0', # default value
             min=>0.0,
             max=>1e20,
         },
         {
             name=>'post-AGB Wind Loss Prescription',
             param=>'postagbwind',
             type=>'dropdown',
             values=>{
                 'As on AGB'=> 'POSTAGB_WIND_GIANT',
                     'None'=> 'POSTAGB_WIND_NONE',
                     'Krticka, Kubát and Krticková (2020)' => 'POSTAGB_WIND_KRTICKA2020'
             }
         },

         {
             name=>'Massive Star Wind Loss Prescription',
             param=>'wr_wind',
             type=>'dropdown',
             values=>{
                 'Hurley2002'=>0,
                     'Meynet+Maeder'=>1,
                     'Nugis+Lamers'=>2
             }
         },
         {
             name=>'Massive Star Wind Multiplier', # onscreen name
             param=>'wr_wind_fac', # parameter name
             type=>'text', # type, can be: text, dropdown, tickbox
             default=>'1.0', # default value
             min=>0.0,
             max=>1e20,
         },

         {
             name=>'Bondi-Hoyle wind accretion multiplier', # onscreen name
             param=>'Bondi_Hoyle_accretion_factor', # parameter name
             type=>'text', # type, can be: text, dropdown, tickbox
             default=>'1.5', # default value
             min=>0.0,
             max=>1e20,
         },

         {
             name=>'Wind RLOF',
             param=>'WRLOF_method',
             type=>'dropdown',
             values=>{
                 'Off'=> 'WRLOF_NONE',
                     'q-dependent'=> 'WRLOF_Q_DEPENDENT',
                     'quadratic'=> 'WRLOF_QUADRATIC'
             }
         },

         { 
             name=>'Tidal wind enhancement factor (CRAP)', # onscreen name
             param=>'CRAP_parameter', # parameter name
             type=>'text', # type, can be: text, dropdown, tickbox
             default=>'0.0', # default value
             min=>0.0,
             max=>1e20,
         },

         {
             name=>'Wind Jorbdot multiplier', # onscreen name
             param=>'lw', # parameter name
             type=>'text', # type, can be: text, dropdown, tickbox
             default=>'1.0', # default value
             min=>0.0,
             max=>1000000.0,
         },

     ],

     
     'Nucleosynthesis'=>[
         {
             name=>'Initial abundance mixture',
             param=>'initial_abundance_mix',
             type=>'dropdown',
             values=>{
                 'Anders and Grevesse 1989'=> 'NUCSYN_INIT_ABUND_MIX_AG89',
                     'Grevesse and Noels 1993'=> 'NUCSYN_INIT_ABUND_MIX_GREVESSE_NOELS_1993',
                     'Karakas et al. 2002'=> 'NUCSYN_INIT_ABUND_MIX_KARAKAS2002',
                     'Lodders 2003'=> 'NUCSYN_INIT_ABUND_MIX_LODDERS2003',
                     'Asplund 2009' => 'NUCSYN_INIT_ABUND_MIX_ASPLUND2009',
                     'Lodders 2010' => 'NUCSYN_INIT_ABUND_MIX_LODDERS2010',
                     'Kobayashi 2011 / Asplund 2009' => 'NUCSYN_INIT_ABUND_MIX_KOBAYASHI2011_ASPLUND2009'
             }
         },
         {
             name=>'Third Dredge up <font face="Helvetica">&Delta;</font>M<SUB>c,min</SUB> for third dredge up (Izzard and Tout 2004)',
                                               param=>'delta_mcmin',
                                               type=>'text',
                                               default=>0.0,
                                               min=>-100.0,
                                               max=>100.0,
                                               unit=>$unit{Msun},
         },
         {
             name=>'<font face="Helvetica">&lambda;</font><SUB>min</SUB> for third dredge up (Izzard and Tout 2004)',
                                param=>'lambda_min',
                                type=>'text',
                                default=>0.0,
                                min=>0.0,
                                max=>1000.0,
         },
         {
             name=>'Minimum M<SUB>env</SUB> for third DUP',
             param=>'minimum_envelope_mass_for_third_dredgeup',
             type=>'text',
             default=>0.5,
             min=>0.0,
             max=>1000.0,
             unit=>$unit{Msun},
         },
         {
             name=>'<SUP>13</SUP>C efficiency (old models)',
             param=>'c13_eff',
             type=>'text',
             default=>1.0,
             min=>0.0,
             max=>10.0,
         },
         {
             name=>'s-process partial mixing zone mass (new models, <I>Z</I>=1e-4 only)',
             param=>'mass_of_pmz',
             type=>'text',
             unit=>$unit{Msun},
             default=>0.0,
             min=>0.0,
             max=>1.0,
         },

         {
             name=>'Disable thermohaline mixing?',
             param=>'no_thermohaline_mixing',
             type=>'dropdown',
             values=>{
                 'No'=>'False',
                     'Yes'=>'True',
             },
         },

         {
             name=>'Core-collapse SN yields',
             param=>'core_collapse_supernova_algorithm',
             type=>'dropdown',
             default=>'NUCSYN_CCSN_LIMONGI_CHIEFFI_2018',
             values=>{
                 'Limongi and Chieffi 2018' => 'NUCSYN_CCSN_LIMONGI_CHIEFFI_2018',
                     'Chieffi and Limongi 2004' => 'NUCSYN_CCSN_CHIEFFI_LIMONGI_2004',
                     'Woosley Weaver 1995 (set A)' => 'NUCSYN_CCSN_WOOSLEY_WEAVER_1995A',
                     'Woosley Weaver 1995 (set B)' => 'NUCSYN_CCSN_WOOSLEY_WEAVER_1995B',
                     'Woosley Weaver 1995 (set C)' => 'NUCSYN_CCSN_WOOSLEY_WEAVER_1995C',
                     'None' => 'NUCSYN_CCSN_NONE',
             }
         },

         {
             name=>'Core-collapse SN r-process',
             param=>'core_collapse_rprocess_algorithm',
             type=>'dropdown',
             default=>'NUCSYN_CCSN_RPROCESS_SIMMERER2004',
             values=>{
                 'Simmerer 2004' => 'NUCSYN_CCSN_RPROCESS_SIMMERER2004',
                     'Arlandini 1999' => 'NUCSYN_CCSN_RPROCESS_ARLANDINI1999',
                     'None' => 'NUCSYN_CCSN_RPROCESS_NONE',
             }
         },
         {
             name=>'Core-collapse SN r-process mass',
             param=>'core_collapse_rprocess_mass',
             type=>'text',
             default=>1e-6,
             min=>0.0,
             max=>10.0,
         },

         {
             name=>'Type Ia MCh SN yields',
             param=>'type_Ia_MCh_supernova_algorithm',
             type=>'dropdown',
             default=>'TYPE_IA_SUPERNOVA_ALGORITHM_DD2',
             values=>{
                 'Iwamoto 1999 DD2' => 'TYPE_IA_SUPERNOVA_ALGORITHM_DD2',
                     'Seitenzahl 2013 N100' => 'TYPE_IA_SUPERNOVA_ALGORITHM_SEITENZAHL2013_AUTOMATIC',
             }
         },

         {
             name=>'Type Ia sub-MCh SN yields',
             param=>'type_Ia_sub_MCh_supernova_algorithm',
             type=>'dropdown',
             default=>'TYPE_IA_SUB_MCH_SUPERNOVA_ALGORITHM',
             values=>{
                 'Livne and Arnett 1995' => 'TYPE_IA_SUPERNOVA_ALGORITHM_LIVNE_ARNETT_1995',
             }
         },
         {
             name=>'WD Mass accretion for Detonation',
             param=>'mass_accretion_for_eld',
             type=>'text',
             default=>0.15,
             min=>0.0,
             max=>1.0,
         },
     ],

     'Display'=>[
         {
             name=>'Roche lobe images?',
             param=>'wantimages',
             cgi_option=>1,
             type=>'tickbox',
             default=>1
         },
         {
             name=>'Roche lobe images scale factor',
             param=>'imagescale',
             type=>'text',
             cgi_option=>1,
             default=>0.25,
             min=>0.01,
             max=>100.0,
         },
         {
             name=>'Fix size of Roche lobes?',
             param=>'fixrochesize',
             cgi_option=>1,
             type=>'tickbox',
             default=>1
         },
         {
             name=>'Fix separations relative to initial separation?',
             param=>'fixsepscale',
             cgi_option=>1,
             type=>'tickbox',
             default=>0
         },
         {
             name=>'Compress text',
             param=>'compress',
             cgi_option=>1,
             type=>'tickbox',
             default=>0
         },
         {
             name=>'Animated gifs?',
             param=>'animate',
             cgi_option=>1,
             type=>'tickbox',
             default=>0
         },
         {
             name=>'Show isotopes?',
             param=>'abunds',
             cgi_option=>1,
             type=>'tickbox',
             default=>1
         },
         map
         {
             {
                 name=>'Show stellar type '.$_,
                 param=>'show stellar type '.$_,
                 cgi_option=>1,
                 type=>'tickbox',
                 default=>$stfilter{$_},	     
             }
         }@stellar_types,
     ],

    );
}

sub make_binary_c_argstring
{
    my @args;
    for(my $i=0;$i<@form;$i+=2)
    {
        my $tab_name = $form[$i];
        my $tab_contents = $form[$i+1];
        foreach my $line (@$tab_contents)
        {
            push(@args, parse_tab_line($line));
        }
    }
    my $c = join(' ',@args);

    # make sure M2 > M1
    my $m1 = ($c=~/M_1\s+(\S+)/)[0];
    my $m2 = ($c=~/M_2\s+(\S+)/)[0];
    if(!defined $m1)
    {
        if(!defined $m2)
        {
            print "Neither mass is defined : this is an error\n<BR>";
            exit;
        }
        else
        {
            $m1 = 1.0*$m2;
            $m2 = 0;
            $c=~s/M_1\s+(\S+)/M_1 $m1/;
            $c=~s/M_2\s+(\S+)/M_2 $m2/;
        }
    }
    else
    {
        if(defined $m2 && $m2 > $m1)
        {
            # swap masses
            my $tmp = 1.0*$m1;
            $m1 = 1.0*$m2;
            $m2 = $tmp;
            $c=~s/M_1\s+(\S+)/M_1 $m1/;
            $c=~s/M_2\s+(\S+)/M_2 $m2/;
        }
    }

    return $c;
}

sub parse_tab_line
{
    my ($line) = @_;

    next if(defined $$line{dummy} && $$line{dummy}==1);
    next if(defined $$line{cgi_option} && $$line{cgi_option}==1);

    # loop through the options tab lines, looking for param values
    # This is much safer than looping over param(), which we should
    # never do, just in case params are introduced.

    my $arg = $$line{param};  # binary_c arg (also in the web form)
    my $value = scalar param($arg); # can be text

    if(defined $value)
    {
        if(defined $$line{evalcode})
        {
            # use special eval code
            dprint "EVAL $$line{evalcode}\n<BR>arg now $arg";
            eval $$line{evalcode};
            dprint "EVAL error \"$@\"\n";
        }

        if($$line{type} eq 'dropdown')
        {
            # convert string to value (or use default)
            $value = $$line{values}{$value};
            if(!defined $value)
            {
                $value = $$line{default} 
            }
        }
        elsif($$line{type} eq 'tickbox')
        {
            # should be 'on' by default if checked
            $value = ($value eq 'on' ||  $value eq 'On' ||
                      $value eq 'True' || $value eq 'true' ||
                      $value eq 'Ticked' || $value eq 'ticked' ||
                      $value eq '1' || $value == 1) ? 1 : 0;
        }

        # value must be numeric unless otherwise told
        # or unless it's a dropdown option
        if($line->{type} eq 'dropdown')
        {
            if(! any {$_ eq $value} values %{$line->{values}})
            {
                print "<FONT COLOR=RED> parameter $arg (value $value) is not a valid option.";
                exit;
            }
        }
        elsif(!(defined $line->{non_numeric} && $line->{non_numeric}==1))
        {
            $value = is_numeric_scalar($value);
            if(!defined is_numeric_scalar($value))
            {
                print "<FONT COLOR=RED> parameter $arg (value $value) is not numeric or a designated value.";
                exit;
            }
        }
    }
    else
    {
        print "<FONT COLOR=RED>Warning : no value for parameter $arg : using default</FONT>\n<BR>";
        $value = $$line{default};
    }

    dprint "Parse $$line{name} : ",scalar param($arg)," -> $value<BR>\n";

    $value = $$line{min} if defined($$line{min}) && $value < $$line{min};
    $value = $$line{max} if defined($$line{max}) && $value > $$line{max};

    dprint "Return --$arg $value\n<BR>";

    return " --$arg $value ";
}


sub is_numeric_scalar
{
    # tests if $s is a numeric scalar, and if so return it
    # (via a regexp for taint mode) or return undef on failure
    my ($s) = @_;

    # first test for defined, not empty and at least one number or symbol
    return undef if ((!defined($s))||
		     ($s eq '')||
		     ($s=~/[^0-9efEF\.\+\-]/o))
	;

    # if we start with a . prepend a zero
    $s = '0'.$s if($s=~/^\./o);

    # x or +x or -x or x.y or +x.y or -x.y
    my $r = ($s=~/^([\+\-]?\d+\.?(?:\d+)?)$/o)[0];

    # x.yez etc.
    $r = ($s=~/^([\+\-]?\d+\.?(?:\d+)?[eEdD][\+\-]?\d+)$/o)[0] if(!defined $r);

    return $r;
}


sub dprint
{
    if($options{vb}>=1)
    {
        foreach my $l (@_)
        {
            my $x = $l;
            $x=~s/\n/<BR>\n/g;
            print $x;
        }
    }
}


sub extarg
{
    my ($arg, $regexp) = @_;
    return ($arg=~/--$regexp (\S+)/)[0];
}

sub parse_results
{
    my ($args,$results,$init_abunds)=@_;

    $results=~s/M(\d)\/M\S+/M<sub>$1<\/sub>\/$unit{Msun}/gom;
    $results=~s/SEP\/R\S+/SEP\/$unit{Rsun}/gom;
    $results=~s/^ABUNDS.*\n*//gom;
    # unsupported or disabled features
    $results=~s/^SUPERNOVA.*\n*//gom;
    $results=~s/^SINGLE_STAR_LIFETIME.*\n*//gom;
    $results=~s/^.*COMENVin.*\n*//gom;
    $results=~s/^IDUM.*\n*//gom;
    $results=~s/^KICK.*\n*//gom;
    $results=~s/^LAMBDA.*\n*//gom;
    $results=~s/^Probability.*\n*//gom;
    $results=~s/^Tick count.*\n*//gom;
    $results=~s/.*PreMS.*\n//gom;
    $results=~s/.*ANTI_TZ.*\n//gom;
    $results=~s/RANDOM_SEED.*//gom;
    $results=~s/.*Randbuf.*//gom;
    $results=~s/.*tidal lock.*\n//gom;
    $results=~s/.*post-COMENV.*//gom;
    $results=~s/.*Unstable RLOF.*//gom;
    $results=~s/.*\"SN\".*//gom;
    $results=~s/.*Mers.*//gom;
    #$results=~s/.*q-inv.*//gom;

    if(0)
    {
        my $r = $results;
        $r=~s/CGIHRD.*\n//g;
        print '<TT><PRE>'.$r.'</PRE></TT><HR>';
    }

    print '<CENTER>Jump to: <A HREF="#HRD">HRD</A> - <A HREF="#yields">Yields</A> - <A HREF="#periodic">Periodic table</A></CENTER><BR>';

    my $massinstars=extarg($args,'M_1')+extarg($args,'M_2');

    foreach my $x (
        'wantimages',
        'fixrochesize',
        'fixsepscale',
        'compress',
        'animate',
        map
        {
            'show stellar type '.$_
        }@stellar_types,
        )
    {
        $options{$x} = scalar param($x) eq 'on' ? 1 : 0;
    }

    $options{imagescale} = is_numeric_scalar(scalar param('imagescale'));

    if($options{vb})
    {
        print "<HR>Command    :<PRE style=\"white-space: pre-wrap;\">$args</PRE>\n<BR>";
        print "<HR>RAW results:<PRE style=\"white-space: pre-wrap;\">$results</PRE>\n<BR>";

        my $r2 = $results;
        $r2 = ~s/\n/<BR>/g;
        print '<PRE>',$r2,'</PRE>';
    }

    # save the results
    my $saved_results = $results;

    # split results and loop
    my @results = split(/\n+/o,$results);
    my @bin_yields;
    my @hrddata;
    my $starts_single;
    my $firstline;

    for my $line (@results)
    {
        $line =~ s/^\s+//o;
        my @data = split(/\s+/o,$line);

        # skip some lines in newer binary_c
        next if($line=~/post-COMENV/);
        if($line =~ s/^CGIHRD\s+//)
        {
            push(@hrddata,$line);
            next;
        }

        if($data[0]=~/DXYIELDbin(\d+)__/)
        {
            # yields
            my $source = $1; # maybe use this later?
            for(my $j=1; $j <= $#data; $j++)
            {
                $bin_yields[$j-1] += $data[$j];
            }
            next;
        }

        if(!defined $firstline)
        {
            # first line of data
            $firstline = pretty_firstline($line);
            print "<TABLE WIDTH=100% BORDER=1 style=\"border-style:solid;border-color:gray;\">\n<TR>$firstline</TR>\n<TR>";
        }
        elsif($#data>=25 || $#data==9)
        {
            # proper data line, make it nice
            # colours for each column
            my @cell_colours=(
                'EEEEFF',
                'FFDDDD',
                'DDFFDD',
                'FFDDDD',
                'DDFFDD',
                'FFFFEE',
                'FFFFEE',
                'FFDDDD',
                'DDFFDD',
                'FFEEFF',
                '000000',
                '000000'
                );
            
            if(!defined $starts_single)
            {
                # set starts_single to the massless star number (+1)
                $starts_single =
                    $data[3] eq $massless ? 1 :
                    $data[4] eq $massless ? 2 :
                    0;
            }
            # BUG! we should skip the isotopes
            my $skipiso=0;
            if($#data==9)
            {
                $skipiso=1;
                $#data=25;
            }

            # if RLOFing, blue straggler or comenv change the
            # colour again
            if($data[7]>=1.0)
            {
                $cell_colours[7] = rlof_colour($data[3]);
            }
            if($data[8]>=1.01)
            {
                $cell_colours[8] = rlof_colour($data[4]);
            }
            if($data[9] eq 'BEG_BSS')
            {
                $cell_colours[9]='8888FF';
            } # Blue straggler
            elsif($data[9] eq 'COMENV')
            {
                $cell_colours[9]='FF8888';
            } # Comenv

            # output
            my $j;
            print '<TR>';
            my $suppress_binary_details = 0;
            for($j=0; $j<=$#data; $j++)
            {
                # style : odd/even
                my $style='border-style:solid;border-color:gray;background-color:#'.
                                                                    (($j%2)==0 ? 'c3bbDD' : 'b3aaDD') . ';';  

                # the output : change this, not @data
                my $out = $data[$j];

                if($j==0 || $j==1 || $j==2)
                {
                    # Time, Masses
                    $out = sprintf "%g",$out;
                }
                if($j==3 || $j==4)
                {
                    # stellar type
                    $out = pretty_stellar_type($out);
                }
                elsif($j==5)
                {
                    # separation / Rsun
                    if($out eq '-1')
                    {
                        $out =  '';
                        $suppress_binary_details = 1;
                    }
                }
                elsif($j==6)
                {
                    # orbital period
                    if($out eq '-1')
                    {
                        $out = '';
                        $suppress_binary_details = 1;
                    }
                    else
                    {
                        # remove _ before unit and add half-space &#8239;
                        $out =~ s/_//;
                        $out =~ s/(.*\d)/$1\&#8239;/;
                    }
                }
                elsif($j==7)
                {
                    # eccentricity
                    if($suppress_binary_details == 1)
                    {
                        $out = '';
                    }
                }
                elsif($j==8 || $j==9)
                {
                    # R/RL1, R/RL2
                    if($suppress_binary_details == 1)
                    {
                        $out = '';
                    }
                    else
                    {
                        $out = $out > 0 ? $out : '-';
                    }
                }
                elsif($j==10)
                {
                    # status
                    $out = pretty_status($out,$line);
                }
                elsif($j==11)
                {
                    if($data[10] =~ /SN/)
                    {
                        # SN image : special case 
                        $out = other_formatting($out,$j,$line);
                    }
                    else
                    {
                        $out = '';
                    }
                }

                if($j<=11)
                {
                    my $cell_colour= $j>$#cell_colours ? 'FFFFFF' : $cell_colours[$j];
                    print "<TD ALIGN=LEFT BGCOLOR=\"\#$cell_colour\" style=\"$style\">$out</TD>\n";
                }
                elsif($j==12)
                {
                    if($skipiso)
                    {
                        print "<TD></TD>";
                    }
                    elsif($options{abunds})
                    {
                        # abundances vs time!
                        print '<TD BGCOLOR="#FFFFFF" style=\"$style\">';

                        # make as a table with two columns: one for each star
                        my @isos=('<SUP>1</SUP>H','<SUP>4</SUP>He','<SUP>12</SUP>C','<SUP>14</SUP>N','<SUP>16</SUP>O','<SUP>56</SUP>Fe','Ba');
                        my @cell_colours=('FFDDDD','FFDDDD', # X,Y
                                          'DDFFDD','DDFFDD','DDFFDD', #CNO
                                          'DDDDFF','DDDDFF'); # Fe56, Ba 
                        my $i;
                        print "<TABLE CELLPADDING=0  width=100% CELLSPACING=0 BORDER=0>\n";
                        for($i=0;$i<=$#isos;$i++)
                        {

                            # format
                            $data[$i+12]=sprintf('%2.2e',$data[$i+12]);
                            $data[$i+13+$#isos]=sprintf('%2.2e',$data[$i+13+$#isos]);

                            # make very small abundances zero
                            $data[$i+12]='0' if($data[$i+12]<1e-14);

                            $data[$i+$#isos+13]='0' if($data[$i+$#isos+13]<1e-14);


                            print "<TR BGCOLOR=\"$cell_colours[$i]\" style=\"$style\"><TD ALIGN=LEFT><FONT SIZE=-3>$isos[$i]</FONT></TD><TD ALIGN=RIGHT><FONT SIZE=-3>$data[$i+12]</FONT></TD><TD WIDTH=10>&nbsp;&nbsp;</TD><TD ALIGN=RIGHT><FONT SIZE=-3>",$data[$i+$#isos+13],"</FONT></TD></TR>\n";
                        }
                        print "</TABLE>\n</TD>";
                    }
                }
            }
            binary_image($suppress_binary_details,@data);
            print "</TR>\n";
        }
        else
        {


            # some other line of data (supernova? novae?)
            print '<TR><TD COLSPAN=11>',other_formatting($line),"</TD></TR>\n";
        }
    }
    print "</TABLE></TR></TD>\n";

    # HRD
    print '<TR><TD><FONT SIZE=+5><A NAME="HRD"></A>Hertzsprung-Russell</FONT><BR><BR>';
    my %cols = (
        'ST1'=>0,
        'Teff1'=>1,
        'L1'=>2,
        'ST2'=>3,
        'Teff2'=>4,
        'L2'=>5
        );

    my %units = (
        'ST1'=>'',
        'Teff1'=>'K',
        'L1'=>'Lsun',
        'ST2'=>'',
        'Teff2'=>'K',
        'L2'=>'Lsun'
        );

    my $obj='HRD'.randstring();
    my $fontsize=15;
    my %dim=(width=>1000,
             height=>400);

    print "
<div style=\"width:".$dim{width}."px; height:".$dim{height}."px;\">
   <canvas id=\"$obj\" width=\"".$dim{width}."px\" height=\"".$dim{height}."px\" style=\"background-color:white; width:".$dim{width}."px; height:".$dim{height}."px\"></canvas>
   <script>
var ctx = document.getElementById(\"$obj\");
var $obj = new Chart(ctx, {
    type: 'line',
    data: {
        datasets: [ ";
    my %axislabels;
    my $n = $#hrddata;
    my @colour = (
        '255,0,0',
        '0,0,255'
        );
    my @stars = $starts_single==1 ? (0) :
        $starts_single==2 ? (1) :
        (0,1);
    foreach my $star (@stars)
    {
        print "\{
                label: '',
                fontSize: $fontsize,
                fill: false,
                lineTension: 0,
                pointRadius: 0,
                showLine: true,
                borderColor: 'rgb($colour[$star])',
                spanGaps: false,
                data: \[
";
        my $xcol = 'Teff'.$star;
        my $ycol = 'L'.$star;
        my $st = 'ST'.$star;
        my $stcol = $cols{$st};

        for(my $i=0;$i<=$n;$i++)
        {
            my @x = split(/\s+/,$hrddata[$i]);
            if($options{'show stellar type '.$stellar_types[$x[$stcol]] })
            {
                print '{ x: '.log10($x[$cols{$xcol}]).', y: '.log10($x[$cols{$ycol}]).' } ';
                print ',' if($i!=$n);
                print "\n";
            }
        }
        print "   \]
         \} ,
";
        if($star==0)
        {
            %axislabels = (x=>$xcol.((defined $units{$ycol} && $units{$ycol}) ? (' / '.$units{$xcol}) : ''),
                           y=>$ycol.((defined $units{$xcol} && $units{$xcol}) ? (' / '.$units{$ycol}) : ''));
            map
            {
                $axislabels{$_}=~s/\/\s*$//;
                $axislabels{$_} = 'log10('.$axislabels{$_}.')';
            }keys %axislabels;
        }
    }


    print "
       \]\},

    options: {
         legend: {
              display: false
         },
         title: {
              display: true,
              text: '',
              fontSize: $fontsize
         },
          scales: {
            xAxes: [{    
                type: 'linear',
                position: 'bottom',
                beginAtZero:true,
                scaleLabel: {
                display: true,
                labelString: 'log(Teff/K)',
                fontSize: $fontsize,
                },
                ticks: {
                    reverse: true,
                },
            }],
            yAxes: [{
                type: 'linear',";
    
    # stellar type has special labels
    if($axislabels{y} =~/Stellar type/i)
    {
        print "ticks: {
                  callback: function(value, index, values){
                    var binary_c_stellar_types = ['LMMS','MS','HG','GB','CHeB','EAGB','TPAGB','HeMS','HeHG','HeGB','HeWD','COWD','ONeWD','NS','BH','Massless'];
                    return binary_c_stellar_types[value];
                          }
                  },
";
    }

    print "
                beginAtZero:true,
                scaleLabel: {
                display: true,
                labelString: 'log(L/Lsun)',
                fontSize: $fontsize
                }
            }],
        },
        pan: {
                enabled: true,
                mode: 'xy'
             },
        zoom: {
                        enabled: true,
                        mode: 'x',
                        limits: {
                            max: 10,
                            min: 0.5
                        }
             }
    }
});

";

    print "
</script>
</div>
</TD></TR>
                    ";


    my $massoutofstars=1e-30;
    map
    {
        $massoutofstars+=$_;
    }grep {!/x0/} @bin_yields;

    my %ycolours = (
        'isotope'=>'#000000',
        'expelled'=>'#000000',
        'ei'=>'#ff0000',
        'eo'=>'#0000ff',
        'ym'=>'#ff00aa',
        );

    print "<TR><TD COLSPAN=1><A NAME=\"yields\"></A><FONT SIZE=+5>Chemical Yields</FONT><BR><BR><TABLE BORDER=1><TR><TD COLSPAN=1><TABLE BORDER=0><TR><TD HEIGHT=100% id=\"td_menu\"><B>Isotope</B><BR><BR><FONT SIZE=-2>Initial Abundance</FONT></TD></TR></TABLE></TD><TD id=\"td_menu\"><FONT COLOR=\"$ycolours{'mass expelled'}\">Mass Expelled (in M$subsun)</FONT><BR><FONT COLOR=\"$ycolours{'ei'}\">Mass expelled/Mass input</FONT><BR><FONT COLOR=\"$ycolours{'eo'}\">Mass expelled/Mass output</FONT><BR><FONT COLOR=\"$ycolours{'ym'}\">(Mass expelled as isotope - Equivalent mass x initial abundance)/(Mass In)</FONT></TD></TR><TR><TD align=\"center\" COLSPAN=2><FONT SIZE=+1>Total mass into stars : $massinstars M$subsun<BR>Total mass out of stars: ",sprintf('%2.2f',$massoutofstars)," M$subsun\n<BR></FONT></TD></TR></TABLE><BR><TABLE BORDER=1 style=\"border-style:solid;border-color:gray;\">\n";

    my @shead;

    if(defined $isotope_list && ref $isotope_list eq 'ARRAY')
    {
        # sort head into non-silly order
        @shead = grep {$_ ne 'n' && $_ ne 'e'} sort alphanum (@$isotope_list);

        shift @bin_yields; # remove first to get offset right (not sure why!)

        for(my $j=0; $j<=$#shead; $j++)
        {
            my $isotope = $shead[$j];
            my $k = $isotope_hash->{$isotope}; # binary_c isotope number
            my $s = sprintf('%5s',$isotope);
            $s =~ s/\s/\&nbsp;/g;

            my $init = $init_abunds->{$isotope}; # initial abundance

            print "<TD><TABLE BORDER=0><TR>",

                # left cell : isotope name
                "<TD><B>$s</B><BR><BR><FONT SIZE=-2 COLOR=\"$ycolours{'isotope'}\">",
                sprintf('%3.3e',$init),
                "</FONT></TD>",

                # right cell : various yields
                "<TD ALIGN=RIGHT><FONT COLOR=\"$ycolours{'expelled'}\">",
                sprintf('%3.3e',$bin_yields[$k]),
                "<BR><FONT COLOR=\"$ycolours{'ei'}\">",
                sprintf('%3.3e',$bin_yields[$k]/$massinstars),
                "</FONT><BR><FONT COLOR=\"$ycolours{'eo'}\">",
                sprintf('%3.3e',$bin_yields[$k]/$massoutofstars),
                "</FONT><BR><FONT COLOR=\"$ycolours{'ym'}\">",
                sprintf('%3.3e',($bin_yields[$k]-$massoutofstars*$init)/$massinstars),
                "</FONT></TD></TABLE></TD>\n";

            # newline every 10 isotopes
            if(($j+1)%10 == 0) 
            {
                print "</TR><TR>";
            }
        }

    }
    print '</TABLE></TD></TR>';
    
    print "<TR><TD><A NAME=\"periodic\"></A><FONT SIZE=+5>Periodic Table of Ejecta</FONT><BR><TABLE BORDER=1>";

    # make periodic table
    my $table = []; # NB $table->{$y}->{$x}
    my $width = 18;
    my $nrows;
    
    foreach my $element (@$element_list)
    {
        next if($element->{name} eq '' || $element->{name} eq 'e');

        # make x,y co-ordinates
        my $Z = $element->{atomic_number};
        my $x;
        my $y;
        if($Z<=2)
        {
            $y = 1;
            $x = $Z==1 ? 1 : $width;
        }
        elsif($Z<=10)
        {
            $y = 2;
            $x = $Z <= 4 ? ($Z - 2) : ($Z + 8)
        }
        elsif($Z<=18)
        {
            $y = 3;
            $x = $Z <= 12 ? ($Z - 10) : $Z;
        }
        else
        {
            $y = int(($Z-$width-1) /$width) + 4;
            $x = 1 + ($Z-$width-1) % $width;
        }
        $nrows = $y>$nrows ? $y : $nrows;

        # make table contents
        $table->[$y] //= [];
        $table->[$y]->[$x] = $element->{name}." <FONT SIZE=-1><I>Z</I>&nbsp;=&nbsp;$element->{atomic_number}</FONT><BR><FONT SIZE=-1>";

        # get total yield for this element
        my $tot = 0.0;
        foreach my $i (@{$element->{isotopes}})
        {
            my ($isotope,$n) = @$i;
            $tot += $bin_yields[$n];
        }

        if($tot>0.0)
        {
            foreach my $i (sort {
                ($a->[0]=~/(\d+)/)[0] <=> ($b->[0]=~/(\d+)/)[0]
                           }@{$element->{isotopes}})
            {
                
                my ($isotope,$n) = @$i;
                if($bin_yields[$n]>0.0)
                {
                    my $A = ($isotope=~/(\d+)/)[0];
                    my $frac = format_number(sprintf '%g',100.0*$bin_yields[$n]/$tot);
                    if($frac > 0.0)
                    {
                        $table->[$y]->[$x] .= '<B>'.$A.'</B>&nbsp;'.$frac."%<BR>";
                    }
                }
            }
        }
    }

    
    for(my $y=1; $y<=$nrows; $y++)
    {
        print "<TR>";
        for(my $x=1; $x<=$width; $x++)
        {
            print "<TD>$table->[$y]->[$x]</TD>";
        }
    }
    if(defined $isotope_list)
    {
        for(my $j=0;$j<=$#shead;$j++)
        {
            my $isotope = $shead[$j];
            my $k=$$isotope_hash{$isotope}; # binary_c isotope number

            my $s=sprintf('%5s',$isotope);
            $s=~s/\s/\&nbsp;/g;
            
            my $init=$$init_abunds{$isotope}; # initial abundance
            
        }
    }
}

sub RL
{
    my $q=$_[0];
    my $p=$q**(1.0/3.0);
    return 0.49*$p*$p/(0.6*$p*$p + log(1.0+$p));
}



sub randstring
{
    #  return random string
    my @chars = ("A".."Z", "a".."z");
    my $string;
    $string .= $chars[rand @chars] for 1..8;
    return $string;
}

sub format_number
{
    my $x = shift;
    return '10' if($x == '1e1');
    return '100' if($x == '1e2');
    return '1000' if($x == '1e3');
    $x = sprintf '%.3f',$x;
    $x=~s/(\d+\.\d+)e([\+-]\d+)/sprintf'%5.2fx10<SUP>%d<\/SUP>',$1,$2/;
    $x=~s/SUP>\+0+/SUP>/g;
    return $x;
}

# use wait_for_cpu prior to running an external command
# to prevent denial-of-service type attack

sub wait_for_cpu
{
    my $n=0;
    while(-f $options{lockfile} &&
          -s $options{lockfile} > 1)
    {
        if($options{vb})
        {
            print "wait_for_cpu $n (lockfile $options{lockfile} in place)<BR>\n\n\n\n";
        }
        
        if($n++>$options{lockmaxwait})
        {
            # V5.01:
            # We've hit the timeout, now check that the
            # lock file is not > max_lockfile_age. 
            #
            # If it is older, assume that something crashed
            # which prevented the lock file from being reset.
            #
            # Delete the lock file and assume all is ok.
            #
            # The max_lockfile_age is usually 10 minutes, which
            # is so infrequent that a denial-of-service attack
            # is still restricted.

            my $lockfile_age = time() - (stat($options{lockfile}))[9];
            
            if($options{vb})
            {
                printf "AGE %g vs %g\n",$lockfile_age,$options{max_lockfile_age};
            }
            if($lockfile_age >= $options{max_lockfile_age})
            {
                # lockfile is too old : remove it
                unlink $options{lockfile};
                return;
            }
            else
            {
                # lockfile is young, so CPU is genuinely busy
                print "Timed out while for a CPU<BR>\n";
                exit;
            }
        }
        sleep 1;
    }
}

sub runcmd
{
    # wait for a CPU to be available
    wait_for_cpu();

    # get the command
    my ($command) = @_;

    # open lock file: prevents other commands
    # from running while this one executes
    open(my $lock,'>',$options{lockfile});
    print {$lock} $$,"\n";
    close $lock;

    # allow others to delete the lock file in
    # case of malfunctions
    chmod $options{lockfile_mode}, $options{lockfile};

    # run command
    my $x = `$command | head -c $options{max_command_return}`;

    # clean up the lockfile
    unlink $options{lockfile};
    
    return $x;
}
