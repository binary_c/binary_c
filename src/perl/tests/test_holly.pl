#!/usr/bin/env perl
use strict;
use rob_misc;
use robqueue;
use threads::shared;
use Sort::Key qw/nsort/;
use Data::Dumper;

if(!$ARGV[0])
{
    print "Args must be a single metallicity or the first arg must be 'all'\n";
    exit;
}

#
# change this to use masses from the interpolation table, not
# the 0.25Msun spaced masses given here.
#
  
my @Z = ($ARGV[0] eq 'all') ? ('0.02','0.004','0.001','0.0001') : (@ARGV) ;
my $dm = 0.25;
my %holly : shared;
my %mine_AG89 : shared;
my %mine_Lodders2003 : shared;
my %mine_GN93 : shared;
my %init_abunds;
get_init_abunds();

foreach my $Z (@Z)
{
    my $q = robqueue->new(
        nthreads=>16,
        subr=>\&runstar
        );

    foreach my $m (grep {
        1
                   } qw/0.8 0.8449 0.8922 0.9423 0.9951 1.051 1.11 1.172 1.238 1.307 1.381 1.458 1.54 1.626 1.717 1.814 1.915 2.023 2.136 2.256 2.382 2.516 2.657 2.806 2.963 3.129 3.305 3.49 3.686 3.893 4.111 4.342 4.585 4.842 5.114 5.4 5.703 6.023 6.361 6.717 7.094 7.492 7.912 8.356 8.824 9.319 9.842 10.39 10.98 11.59 12.24 12.93 13.65 14.42 15.23 16.08 16.98 17.94 18.94 20/)
    #for(my $m=0.8;$m<20;$m+=$dm)
    {
        $q->q([$m,$Z]);
    }
    $q->end;
    output($Z);
}

# rerun 
print `gnuplot test_holly.plt`;
print "See test_holly.pdf\n";

exit;

############################################################

sub output
{
    my ($Z) = @_;
    my $fp;

    open(my $fp,'>',"holly_test_output-Z$Z.dat")||die("cannot open output data file");
    map
    {
        printf {$fp} "%g %g %g %g %g\n",
        $_,
        $holly{$_},
        $mine_AG89{$_},
        $mine_GN93{$_},
        $mine_Lodders2003{$_};
    }nsort keys %holly;
    close $fp;
    
    %holly=();
    %mine_GN93=();
    %mine_Lodders2003=();
    %mine_AG89=();
}

############################################################

sub runstar
{
    my ($m,$Z) = @{$_[0]};
    $m = sprintf '%g',$m;
    mkdir '/tmp/holly';
    my $outf = "/tmp/holly/tbse.$m.out";
    print STDERR "Run M=$m Z=$Z > $outf \n";

    # H = 0.76 - 3 * Z
    # He = 1 - everything
    my %STARS_abunds = (
        H=>0.76 - 3.0 * $Z,
        # 6.98485924E-01 
        # 2.81397579E-01 
        C=>3.47953321E-03,
        N=>1.12607563E-03,
        O=>9.69750424E-03,
        Ne=>1.99193133E-03,
        
        );
    {
        my $total=0.0;
        map
        {
            $total += $STARS_abunds{$_}; 
        }keys %STARS_abunds;
        $STARS_abunds{He} = 1.0 - $total;
    } 

    my $maxt = $m < 1.0 ? 200000 : 13700;
    my $wind = $Z < 0.005 ? 1 : 0;
    my $args = "--wind_mass_loss $wind --M_1 $m --M_2 0.1 --separation 1e9 --metallicity $Z --gb_reimers_eta 0 --max_evolution_time $maxt --initial_abundance_mix 0 --orbital_period 1e9 ";

    # modify initial abunds to match Holly's models
    $args .= " --init_abund 2 $init_abunds{$Z}{H} --init_abund 5 $init_abunds{$Z}{He} --init_abund 19 $init_abunds{$Z}{O} --init_abund 12 $init_abunds{$Z}{C} --init_abund 15 $init_abunds{$Z}{N} ";
    

    print "$args\n";
    # run
    `cd $ENV{HOME}/progs/stars/binary_c/; tbse $args | grep -E 'HOLLY|1DUP' | tail > $outf`;
    $holly{$m} = (`grep HOLLY $outf | tail -1`=~/\[C\/N\]=(\S+)/)[0];
    #print "HOLLY $holly{$m}\n";
    $mine_AG89{$m} = (`grep 1DUP $outf | tail -1`=~/\[C\/N\]=(\S+)/)[0];
    #print "MINE $mine_AG89{$m}\n";
}



sub get_init_abunds
{
    open(my $init,'<',"$ENV{HOME}/svn/texyears/2016/notes/1dup/Holly/initial_compositions.dat")||die('cannot open initial compositions');
    while(my $x = <$init>)
    {
        chomp $x;
        my @x = split(/\s+/,$x);
        my $z = sprintf '%g',$x[0];
        $init_abunds{$z}{H} = $x[3];
        $init_abunds{$z}{He} = $x[4];
        $init_abunds{$z}{C} = $x[5];
        $init_abunds{$z}{N} = $x[6];
        $init_abunds{$z}{O} = $x[7];
    }
    close $init;
}
