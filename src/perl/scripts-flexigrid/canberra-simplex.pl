#!/usr/bin/env perl 
$|=1; # enable this line for auto-flushed output
use strict;

############################################################
#
# Script to run the Amoeba for the Canberra WD project using Condor 
#
# Should not be called directly, use canberra-runall.pl to make all 
# the project files.
#
############################################################

use rob_misc;
use binary_grid;
use threads;
use Term::ANSIColor;
use IO::Handle;
use Math::Amoeba qw(MinimiseND);
$|=1;

renice_me();
my %number_counts;
my %obsdata;
my $obstot;
my %chisqhash;# cache results

# how to deal with edges:
# bound : caps at max,min
# rand  : random small offsets from the boundaries
# reflect : simply reflect from the boundaries
# huge : return a huge CHISQ for points over the boundaries (default)
my $boundary_treatment='huge';

my $test=0; # 1 = test func only
my $out=(!$test); # 1 = output
my %prev_space_location;
my $tiny=1e-8;
my $huge_chisq=1e10;
my @fixed_parameters;
my @variable_parameters;
my @parameters;
my $batch=0;
my $maxiterations=100;
my $runcount=0;
my $tol=0.05; # relative tolerance
my $chisqmin=0.01; # minimum CHISQ : used to reduce the number of iterations if we're sure things are ok
my $best_chisq=1e99;
my $best_runcount;
my %best_param;
my $worst_chisq=0.0;
my $varformat='%g'; # format for variables
# range for chi-sq comparison
my $vb=2;

############################################################
# parameter space: keys are
# init : initial value
# min : minimum value
# max : maximum value
# vary : should we vary it? if 0 then min and max are set to the init value

my %paramspace=(
    alpha_ce=>{min=>0.0, max=>1.0, init=>0.1, vary=>0}, 
    lambda_ionisation=>{min=>0.0, max=>1.0, init=>0.5, vary=>0}, 
#    qbreak=>{min=>0.01, max=>0.99, init=>0.8, vary=>1}, 
#    qpeak=>{min=>0.0, max=>10.0, init=>1.0, vary=>1}, 
#    qbeta=>{min=>-2.0, max=>1.0, init=>0, vary=>0}, #-0.44 best fit
    wd_sigma=>{min=>0.0, max=>10.0, init=>0.0, vary=>0},
#    binary_fraction_shift=>{min=>-1.0,max=>1.0,init=>0.0,vary=>0},
    z=>{min=>1e-4, max=>0.03, init=>0.02, vary=>0},
    sirius_teff_cutoff=>{min=>0, max=>1e4, init=>0, vary=>0},
    wdwd_teff_min_diff=>{min=>0, max=>1e4, init=>0, vary=>0},

    );

# current log format
my $logformat=2;

############################################################

# Output directory
my $rundir=$ENV{VOLDISK}.'/data/canberra/outdir';

############################################################

# perhaps override 
parse_args();

# ordered, sorted list of parameters (for log file)
my @parameters=sort keys %paramspace;
set_variable_parameters();
set_fixed_parameters();

map
{
    printf "Parameter $_ : min %g max %g init %g vary? %g\n",
    $paramspace{$_}{min},
    $paramspace{$_}{max},
    $paramspace{$_}{init},
    $paramspace{$_}{vary};
}keys %paramspace;

print "Rundir: $rundir\n";
if(-d $rundir)
{
    if($batch)
    {
	print "Already run : exiting\n"; 
	exit;
    }

    print "Directory already exists, would you like to restart it?\n";
    (<STDIN>=~/^y/i) ? 1 : exit 0;  # exit on anything else but y/Y/yes/Yes etc.
    load_prev_results($rundir);    
}

mkdirhier($rundir);

# capture ctrl-c to stop all threads
$SIG{INT}=\&ctrlc_capture;

# open log file and set to autoflush
my $chisqlog=open_chisqlog();

# first guess point and scale variation
my @guess;
my @scale;

set_guess_and_scale();

# for details about MinimiseND:
# http://search.cpan.org/~tom/Math-Amoeba-0.04/lib/Math/Amoeba.pm
my ($p,$y)=MinimiseND(\@guess,
		      \@scale,
		      \&run_model_and_return_chisq,
		      $tol, # tolerance
		      $maxiterations # max nsteps
    );

save_best();

{
    my $c=0;
    for(my $i=0;$i<=$#variable_parameters;$i++)
    {
	printf "% 40s (% 2d) = %g\n",$variable_parameters[$i],$c++,$$p[$i];
    }
    for(my $i=0;$i<=$#fixed_parameters;$i++)
    {
	printf "% 40s (% 2d) = %g\n",$fixed_parameters[$i],$c++,$paramspace{$fixed_parameters[$i]}{init};
    }
}

print "(",join(',',@{$p}),")=$y in $runcount iterations\n";

exit(0);

############################################################
############################################################
############################################################

sub dprint
{
    map
    {
	print $_;
	print OUT $_;
    }@_;
}

sub save_best
{
    # save best data 
}

sub run_model_and_return_chisq
{
    # run model and calculate CHISQ difference to observations
    $runcount++;

    # enforce variable formatting
    map
    {
	$_=varf($_);
    }@_;

    print "RUN MODEL $runcount: '@_'\n"if($vb>=2);

    # make opts
    my $opts=set_bse_parameters(@_);
    my $chisq;
    my $logg=1;

    if($opts=~s/huge//o)
    {
	$logg=0;
	$chisq=$huge_chisq;
	$runcount--; # not really a run
    }
    elsif($chisqhash{$opts})
    {
	$chisq=$chisqhash{$opts};
    }
    elsif($test)
    {
	# for testing : an artificial space
	$chisq=0.0;
	my %target=(alpha_ce=>0.1,
		    lambda_ionisation=>1.0,
		    qpeak=>20.5,
		    wd_sigma=>3.33,
		    z=>0.016
	    );

	my $i=0;
	map
	{
	    my $d=($target{$_}-$_[$i])/$target{$_};
	    printf "D^2=%g from %g - %g\n",$d*$d,$target{$_},$_[$i] if($vb>=2);
	    $chisq+=$d*$d;
	    $i++;
	}@variable_parameters;
    }
    else
    {
	# call grid-canberra-condor
	my $outdir=$rundir.'/'.$runcount;
	my $logfile="$outdir/log";
	mkdirhier ($outdir);
	#unlink $logfile; # remove log!
	
	my $cmd="./src/perl/scripts-flexigrid/grid-canberra-condor2.pl $opts outdir=$outdir  2>/dev/null";
	print color('cyan bold'),scalar(localtime),color('green bold'),": Run $cmd\n";
	`$cmd`;
	print color('cyan bold'),scalar(localtime),color('green bold'),": Finished run of $cmd\n";
	
	# wait for grid to finish
	print "Wait for grid...\n";
	while(!(-f $logfile))
	{
	    #print "Waiting for grid to finish...\n";
	    sleep 1;
	}
	sleep 5; # wait for data to be written (NFS lags etc)
	
	# get CHISQ error (we want to minimize this!)
	$chisq=(slurp($logfile)=~/CHISQ (\S+)/)[0];
    }

    print "Got CHISQ = $chisq\n"if($vb);

    $chisq=~s/\s+//g;
    if(defined($chisq) && $chisq ne '')
    {
	# logging
	write_log(@_,$chisq) if($logg);

	# save for next time
	{
	    my $i=0;
	    map
	    {
		$prev_space_location{$_}=$_[$i];
		$i++;
	    }@variable_parameters;
	}

	# compare to previous
	$worst_chisq=MAX($chisq,$worst_chisq);
	
	my $newbest=0;
	if($chisq<$best_chisq)
	{
	    # found new best fit
	    print color('red bold'),"An improvement!\n",color('reset')if($vb);
	    save_best();	
	    $best_chisq=$chisq;
	    $best_runcount=$runcount;
	    my $i=0;
	    map
	    {
		$best_param{$_}=$_[$i];
		$i++;
	    }@variable_parameters;
	    $newbest=1;
	    print "LINK $rundir/$best_runcount to $rundir/best";
 	    `cd $rundir ; rm best ; ln -sf $best_runcount best`;
	}

	if($vb || $newbest)
	{
	    print color('red bold'),"\nBest so far (run $best_runcount with chisq $best_chisq):\n";
	    map
	    {
		printf "$_ = %g\n",$best_param{$_};
	    }@variable_parameters;
	    print color('reset');
	}
    }
    
    return MAX($chisqmin,$chisq);
}


sub set_guess_and_scale
{
    # set guess and scale arrays for Amoeba
    map
    {
	if($paramspace{$_}{vary})
	{
	    push(@guess,$paramspace{$_}{init});
	    push(@scale,($paramspace{$_}{max}-$paramspace{$_}{min})*0.1);
	}
    }@parameters;
}

sub set_fixed_parameters
{
    map
    {
	if(!$paramspace{$_}{vary})
	{
	    push(@fixed_parameters,$_);
	}
    }@parameters;
    print "Fixed parameters are : @fixed_parameters\n";
}

sub set_variable_parameters
{
    map
    {
	push(@variable_parameters,$_) if($paramspace{$_}{vary});
    }@parameters;
    print "Variable parameters are : @variable_parameters\n";
}

sub set_bse_parameters
{
    my $i=0;
    my $opts;
    my $huge=0;
    map
    {
	my $x=$_[$i]; # the value the simplex routine would like to use
	my $y=$_[$i]; 
	my $was = $prev_space_location{$_};

	if(!$paramspace{$_}{vary})
	{
	    $y = $paramspace{$_}{init};
	}
	else
	{

	    # check if over boundary
	    if($boundary_treatment eq 'rand')
	    {
		# random-reflective hybrid algorithm
		print "Reflective bound $y vs $paramspace{$_}{min},$paramspace{$_}{max} (was $was)\n"if($vb>=2);
		
		# this fails : should just choose a randomly selected
		# point just inside the boundary?

		# parameter range
		my $d=$paramspace{$_}{max}-$paramspace{$_}{min}; 

		# above the maximum
		if($y>=$paramspace{$_}{max})
		{
		    print "New guess $y > parameter space maximum (was $was), reducing to "if($vb>=2);
		    $y = $paramspace{$_}{max} - rand()*0.05*$d;
		    print "$y\n"if($vb>=2);
		}

		# below the minimum
		if($y<=$paramspace{$_}{min})
		{
		    print "New guess $y < parameter space minimum (was $was), reducing to "if($vb>=2);
		    #$y= 0.5*($was-$paramspace{$_}{min})+$paramspace{$_}{min};
		    $y = $paramspace{$_}{min} + rand()*0.05*$d;
		    print "$y\n"if($vb>=2);
		}
	    }
	    elsif($boundary_treatment eq 'reflect')
	    {
		# reflective boundaries 

		print "Reflective bound $y vs $paramspace{$_}{min},$paramspace{$_}{max} (was $was)\n"if($vb>=2);
		
		# above the maximum
		if($y>=$paramspace{$_}{max}-$tiny)
		{
		    print "New guess $y > parameter space maximum (was $was), reducing to "if($vb>=2);
		    $y= $paramspace{$_}{max} - 0.5*($paramspace{$_}{max}-$was);
		    print "$y\n"if($vb>=2);
		}

		# below the minimum
		if($y<=$paramspace{$_}{min}+$tiny)
		{
		    print "New guess $y < parameter space minimum (was $was), reducing to "if($vb>=2);
		    $y= 0.5*($was-$paramspace{$_}{min}) + $paramspace{$_}{min};
		    print "$y\n"if($vb>=2);
		}
	    }
	    elsif($boundary_treatment eq 'huge')
	    {
		# force return of a huge chisq if over the boundaries 
		$huge = ($y>$paramspace{$_}{max} || $y<$paramspace{$_}{min});		
	    }
	    else
	    {
		# bound : use just the bounded value
		$y = MAX($paramspace{$_}{min},MIN($paramspace{$_}{max},$y));
		print "Bounded $y vs $paramspace{$_}{min},$paramspace{$_}{max} (was $was, arrayval $_[$i])\n"if($vb>=2);
	    }
	}

	# format
	$y=varf($y);
	
	# set simplex value to be bounded
	$_[$i]=$y;

	# set grid option
	$opts .= $_.'='.$y.' '.($huge ? 'huge' : '');

	print color('yellow bold'),"Set $_ = $y (simplex wants $_[$i])\n",color('reset')if($vb);
	$i++;
    }@variable_parameters;

    # append fixed parameters
    map
    {
	$opts .= $_.'='.varf($paramspace{$_}{init}).' ';
    }@fixed_parameters;

    print "OPTS '$opts' hashed? ",defined($chisqhash{$opts}) ? $chisqhash{$opts} : "no","\n";

    return $opts;
}

sub write_log
{
    # write log file
    
    return if(!$out);
    
    # 0) run number 
    printf $chisqlog "%d ",$runcount;

    # 1) grid resolution
    if($logformat<=1)
    {
	map
	{
	    printf $chisqlog "%g ",$binary_grid::grid_options{$_}; 
	}('nm1','nm2','nper','necc');
    }

    # 2) parameters and CHISQ error
    map
    {
	printf $chisqlog "$varformat ",$_;
    }@_;
    print $chisqlog "\n";

    if(!$test)
    {
	# 3) sync and flush
	$chisqlog->sync();
	$chisqlog->flush();
    }
}

sub ctrlc_capture
{
    if(!$test)
    {
	print "CTRL-C caught! shutting down condor jobs\n";
	
	my @jobs=grep {/canberra-condor/ && s/(\S+).*/$1/ && chomp} `condor_q -wide`;
	foreach my $job (@jobs)
	{
	    print "KILL $job ...";
	    `condor_rm $job`;
	}
	print "\nSleep 5...\n";
	sleep 5; # wait for death
	@jobs=grep {/canberra-condor/ && s/(\S+).*//} split(/\n/,`condor_q -wide`);
	foreach my $job (@jobs)
	{
	    print "KILL $job...";
	    `condor_rm $job`;
	}
    }
    print "\nDone\n";
    exit;
}

sub parse_args
{
    map
    {
	print "Parse $_\n";
	my $ev;
	
	if(/(\S+)\{(\S+)\}=(\S+)/)
	{
	    $ev="\$$1\{$2\}=\"$3\";";
	}
	elsif(/(\S+)=(\S+)/)
	{ 
	    $ev="\$$1=\"$2\";";
	}
	print "EVAL $ev\n";
	eval $ev if(defined($ev));
    }@ARGV;
}

sub logfile_header
{
    # log file header 
    print {$chisqlog} "# log format $logformat\n# fixed parameters: ";
    map
    {
	print {$chisqlog} "$_=$paramspace{$_}{init} ";
    }@fixed_parameters;
    print {$chisqlog} "\n# 1=run_number ";

    for(my $i=0;$i<=$#variable_parameters;$i++)
    {
	printf {$chisqlog} "%d=%s ",$i+2,varf($variable_parameters[$i]); 
    }
    print {$chisqlog} "\n";
}

sub load_prev_results
{
    open(FP,"<$rundir/chisqlog.dat")||die("cannot open chisqlog.dat in $rundir");
    my $format=1; # log format : default to format 1 (newer files will 
    # override)

    while(<FP>)
    {
	if(/^\#/)
	{ 
	    if(/log format (\S+)/)
	    {
		print "Using log format $1\n" if($1!=$format);
		$format=$1;
	    }
	}
	else
	{
	    chomp;
	    my @x=split;
	    my $chisq = pop @x;
	    my $n=shift @x; # run number (ignore)
	    if($format==1)
	    {
		# old format files had nm1,nm2,nsep,necc here: 
		# not required (always zero!)
		foreach (0..3) {shift @x};
	    }

	    my $opts;

	    if($#x != $#variable_parameters)
	    {
		print "Error loading old data : number of variable_parameters ($#variable_parameters) is not the same as the number of data points ($#x) : something has changed?\n"; 
		for(my $i=0;$i<=MAX($#x,$#variable_parameters);$i++)
		{
		    printf "%d : %s vs %s\n",$i,$variable_parameters[$i],$x[$i];
		}
		
		exit;
	    }
	    
	    # set opts string
	    for(my $i=0;$i<=$#x;$i++)
	    {
		$opts .= $variable_parameters[$i].'='.varf($x[$i]).' ';
	    }
	    map
	    {
		$opts .= $_.'='.varf($paramspace{$_}{init}).' ';
	    }@fixed_parameters;

	    print "$n : Set '$opts' (from @x) with chisq $chisq\n";
	
	    # save in hash to avoid rerun
	    $chisqhash{$opts} = $chisq;
	}
    }
    close FP;
    print "Finished loading in previous run data\n";
}

sub varf
{
    return sprintf $varformat, @_;
}

sub open_chisqlog
{
    # open log to APPEND, otherwise make a new directory
    new IO::Handle;
    open($chisqlog,">>$rundir/chisqlog.dat")||
	die("Cannot open logfile $rundir/chisqlog.dat (for append)");
    $chisqlog->autoflush(1) if(!$test);

    # header for logfile
    logfile_header();

    return $chisqlog;
}
