#!/usr/bin/env perl

# script to DO NOTHING except pretend to be binary_c
use Time::HiRes qw(sleep gettimeofday tv_interval); # for timers

$|=1;
print "set batchmode to 1\n";

# default to no delay, no output lines
my $nlines=0;
my $runtime=0;
my $mode='sleep';

while(my $l=<STDIN>)
{
    next if($l =~ /no_arg_checking/o);
    chomp $l;
    next if($l eq 'reset_stars' || $l eq 'defaults');
    if($l eq 'bye')
    {
	print "STATUS Safe exit with code 0\n";
	exit;
    }    
    elsif($l=~/binary_c_lines (\d+)/)
    {
	$nlines = $1;
    }
    elsif($l=~/binary_c_runtime (\S+)/)
    {
	$runtime=$1;
    }
    elsif($l=~/binary_c_mode (\S+)/)
    {
	$mode=$1;
    }
    elsif($l eq 'go')
    {
	#print "STATUS star go (l=\"$l\" = go? ",($l eq 'go'),"; runtime=$runtime, nlines=$nlines)\n";
	# simulate a star's output: 
	if($runtime)
	{
	    if($mode eq 'sleep')
	    {
		sleep $runtime;
	    }
	    elsif($mode eq 'spin')
	    {
		my $t0=[gettimeofday];
		# CPU bound loop
		while(tv_interval($t0,[gettimeofday])<$runtime){
		    my $i=0;
		    # do something
		    foreach (0..100){$i+=rand()}
		}
	    }
	    else
	    {
		print "mode \"$mode\" unknown : use 'sleep' or 'spin'\n";
		exit;
	    }
	}
	if($nlines)
	{ 
	    my $r=rand();
	    foreach (0..2)
	    {
		print STDERR "pre star $r $_ ",scalar localtime(),"\n";
	    }
	    foreach (0..$nlines)
	    {
		print 'BINARY_C '.$r.' '.$_,"\n";
	    }
	    foreach (0..2)
	    {
		print STDERR "done star $r $_ ",scalar localtime(),"\n";
	    }
	}
	print "fin\n";
    }
}

# should never get here
exit;

