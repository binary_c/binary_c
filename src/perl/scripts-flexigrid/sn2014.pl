#!/usr/bin/env perl
use strict;
use Term::ANSIColor;
use rob_misc;
use 5.16.0;

# wrapper to run multiple populations for Takashi's project

# first arg is the population : 'single','binary' or 'mixed'
my $population = $ARGV[0];

if($population!~/^(?:single|binary|mixed|binary-lna)$/)
{
    print "First arg should be 'single', 'binary', 'binary-lna' or 'mixed'\n";
    exit;
}

rungrid(undef); # defaults
exit if($population eq 'binary-lna');

foreach my $z (0.0001,0.001,0.004,0.008,0.01,0.02) # not 0.004
{
    next if ($z==0.02); # skip defaults
    rungrid("metallicity=$z");
}

foreach my $BH_prescription (0,1)
{
    next if ($BH_prescription==0); # skip defaults
    rungrid("BH_prescription=$BH_prescription");
}

foreach my $wr_wind (0,1,2,3)
{
    next if ($wr_wind==0); # skip defaults
    rungrid("wr_wind=$wr_wind");
}

if($population ne 'single')
{
    # binary-related grids
    foreach my $alpha_ce (0.1,0.5,1.0,3.0)
    {
	foreach my $lambda_ce (-1,0.5)
	{
	    foreach my $lambda_ion (0.0,0.1,0.5)
	    {
		# skip defaults
		next if($lambda_ce==-1 && $alpha_ce==1.0 && $lambda_ion==0.0);
		rungrid("alpha_ce=$alpha_ce lambda_ce=$lambda_ce lambda_ion=$lambda_ion");
	    }
	}
    }

    foreach my $sn_sigma (0,50,500)
    {
	rungrid("sn_sigma=$sn_sigma");
    }

    foreach my $tidal_strength_factor (0.1,1.0,100)
    {
	next if($tidal_strength_factor==1.0);# skip default
	rungrid("tidal_strength_factor=$tidal_strength_factor");
    }
}
exit;

##################################################

sub rungrid
{
    # run grid with extra options
    my $opts=$_[0];

    $opts//='defaults';

    my $outdir=$opts;
    $outdir=~s/\s+/_/go;

    #$outdir = $ENV{VOLDISK}.'/data/sn2014-rerun/IMF_-2.2/'.$outdir;
    #$outdir = $ENV{VOLDISK}.'/data/sn2014-rerun/'.$outdir;
    
    $outdir = $ENV{VOLDISK}.'/data/sn2014-'.$population.'/'.$outdir;
    
    #skip existing
    return if(-d $outdir);

    print "Run grid with extra opts ",color('yellow'),$opts,color('reset')," to outdir $outdir\n";
    mkdirhier($outdir);
    
    my $cmd ="./src/perl/scripts-flexigrid/grid-sn2014-flexigrid.pl population=$population $opts outdir=$outdir vb=3";
    print "CMD = $cmd\n";
    open(my $cmd,"$cmd vb=1 |")||die("cannot open script");
    while(<$cmd>)
    {
	print $_;
    }
    close $cmd;

}
