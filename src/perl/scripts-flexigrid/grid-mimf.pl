#!/usr/bin/env perl

############################################################
#                                                          #
# Example of how to use the binary_grid flexigrid.         #
#                                                          #
############################################################

modules_etc();# load modules

my %results; # hash to store results
my @gpfiles;

# first set some defaults for the grid
defaults();

# then parse the command line to override the defaults
parse_grid_args(@ARGV);

foreach my $tpagbwind (0..8)
{
    print "Run for tpagbwind = $tpagbwind\n";
    # clear results hash
    %results=();

    # now set up the grid
    setup_binary_grid();

    $binary_grid::bse_options{tpagbwind}=$tpagbwind;

    # run flexigrid with two threads
    flexigrid(ncpus());

    # output results
    output(); 
}

gnuplot();

exit(0); # done!

############################################################
############################################################
############################################################

sub gnuplot
{
    my $gpfile="/tmp/MiMf.plt";
    my $psfile="/tmp/MiMf.ps";
    open(my $gp,">$gpfile")||die;
    print {$gp} "set terminal postscript colour solid \"Times-Roman\" 22 enhanced
set output \"$psfile\"
set xrange[0.8:2.0]
";
    my @xlabels=(undef,undef,"Initial Mass");
    my @ylabels=(undef,undef,"Final Mass");

    $labels[2]=['amanda','hurley','reimers','blocker','Van Loon','Rob C-wind',
		'VW93 (karakas) when C/O>1','VW93 (hurley) when C/O>1','Mattsson'];

    foreach my $col (2)
    {
	print {$gp} "set xlabel \"$xlabels[$col]\nset ylabel \"$ylabels[$col]\n";

	print {$gp} "plot \"/tmp/Mi.dat\" u 1:$col w lp lw 2 title \"Initial\" , (0.551-0.005) w l lt -1 title \"\", (0.551+0.005) w l lt -1 title \"Kalirai halo limits\" ";

	# 

	foreach my $f (@gpfiles)
	{
	    $f=~/\.(\d+)\.dat/;
	    print {$gp} ", \"$f\" u 1:$col w lp title \"{/=15 $labels[$col][$1]}\" ";
	    $plotcomma=',';
	}
	print {$gp} "\n\n";
    } 
    close $gp;
    `gnuplot $gpfile`;
    print "See $psfile\n";
}

sub output
{
    # output results
    print "OUTPUT\n";
    
    my $f='/tmp/Mf.'.$binary_grid::bse_options{tpagbwind}.'.dat';

    open(my $fp0,'>/tmp/Mi.dat')||die("cannot open /tmp/Mi.dat");
    open(my $fp1,'>'.$f)||die("cannot open $f");

    map
    {
	print {$fp0} $_,' ',${$results{MiMf0}}{$_},"\n";
	print {$fp1} $_,' ',${$results{MiMf1}}{$_},"\n";
    }sort keys %{$results{MiMf0}};

    close $fp0;
    close $fp1;

    push(@gpfiles,$f);
}

sub parse_bse
{
    # this gets called by the binary_grid module, it parses the 
    # lines of output from binary_c/nucsyn
    my $brk=0; 

    # you should set data in the $h hash pointer, which is 
    # then passed to the join_thread function (below) for you
    # to add up. (Note: $h is local to each thread.)
    my $h=$_[0];

    my $nthread=$binary_grid::grid_options{'thread_num'};

    while($brk==0)
    {
	$_=tbse_line();
	#print $_;
	chomp;
	if($_ eq 'fin')
	{
	    $brk=1; # the end of output
	}
	elsif(s/^MCWD\s+//)
	{
	    my @x=split(' ',$_);
	    $$h{MiMf0}{$x[0]}=$x[1] if(!defined($$h{MiMf0}{$x[0]}));
	    $$h{MiMf1}{$x[0]}=$x[1];
	}
    }
}


sub defaults
{
    grid_defaults();
  
    # physics options
    $binary_grid::bse_options{'z'}=0.0001;
    $binary_grid::bse_options{'wd_sigma'}=0.0;
    $binary_grid::bse_options{'c13_eff'}=1.0;
    $binary_grid::bse_options{'acc2'}=1.5;
    $binary_grid::bse_options{'tidal_strength_factor'}=1.0;
    $binary_grid::bse_options{'alpha_ce'}=1.0; # 1
    $binary_grid::bse_options{'lambda_ce'}=-1; # -1 = automatically set
    $binary_grid::bse_options{'lambda_ionisation'}=0.0; # 0.0
    $binary_grid::bse_options{'delta_mcmin'}=-1.0;
    $binary_grid::bse_options{'lambda_min'}=1.0;
    $binary_grid::bse_options{'minimum_envelope_mass_for_third_dredgeup'}=0.0;

# Wind on TPAGB
# 0=amanda, 1=hurley, 2=reimers, 3=blocker, 4=Van Loon, 5=Rob's C-wind
# 6=VW93 (karakas) when C/O>1, 7=VW93 (hurley) when C/O>1, 8=Mattsson's

    $binary_grid::bse_options{'tpagbwind'}=0;
    $binary_grid::bse_options{'max_evolution_time'}=13700;

# Wind on GB: 0=Reimers (original, choose eta~0.5), 
# 1=Schroeder+Cuntz 2005 (choose eta=1.0 for their Mdot)
    $binary_grid::bse_options{'gbwind'}=0;
    $binary_grid::bse_options{'gb_reimers_eta'}=1.0;
   
    # grid options
    $binary_grid::grid_options{'results_hash'}=\%results;
    $binary_grid::grid_options{'parse_bse_function_pointer'}=\&parse_bse;
    $binary_grid::grid_options{'threads_stack_size'}=32; # MBytes
    $binary_grid::grid_options{'nice'}='nice -n +10'; # nice command e.g. 'nice -n +10' or '' 
    $binary_grid::grid_options{'binary'}=0; # single stars -> 0, binary stars -> 1
    $binary_grid::grid_options{'timeout'}=30; # seconds until timeout
    $binary_grid::grid_options{'log_args'}=1;
    $binary_grid::grid_options{'vb'}=1;

    # Mass 1
    my $nvar=0;
    my $mmin=0.8;
    my $mmax=2.0;
    my $n=100; # resolution
    $binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
    {
	'name'=> 'lnm1', 
	'longname'=>'Primary mass', 
	'range'=>["log($mmin)","log($mmax)"],
	'resolution'=> $n,
	'spacingfunc'=>"const(log($mmin),log($mmax),$n)",
	'precode'=>'my $m1=exp($lnm1); my $eccentricity=0.0;',
	'probdist'=>"ktg93(\$m1)*\$m1",
	'dphasevol'=>'$dlnm1'
    };

    # Binary stars: Mass 2 and Separation
    if($binary_grid::grid_options{'binary'})
    {
	my $m2min=0.1;
	$binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
	{
	    # name of the variable
	    'name'=>'m2',
	    # long name (for verbose logging)
	    'longname'=>'Secondary mass',
	    # range of the parameter space
	    'range'=>[$m2min,'$m1'],
	    # resolution 
	    'resolution'=>$n,
	    # constant grid spacing function
	    'spacingfunc'=>"const($m2min,\$m1,$n)",
	    # flat-q distribution between m2min and m1
	    'probdist'=>"const($m2min,\$m1)",
	    # phase volume contribution
	    'dphasevol','$dm2'
	};

	
	$binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
	{
	    # name of the variable
	    'name'=>'lnsep',
	    # long name (for verbose logging)
	    'longname'=>'ln(Orbital_Separation)',
	    # range of the separation space
	    'range'=>['log(3.0)','log(1e4)'],
	    # resolution
	    'resolution'=>$n,
	    # constant spacing in ln-separation
	    'spacingfunc'=>"const(log(3.0),log(1e4),$n)",
	    # precode has to calculation the period (required for binary_c)
	    'precode'=>"my\$sep=exp(\$lnsep);my\$per=calc_period_from_sep(\$m1,\$m2,\$sep);",
	    # flat in log-separation (dN/da~1/a) distribution (Opik law0
	    'probdist'=>'const(log(3.0),log(1e4))',
	    # phase volume contribution
	    'dphasevol'=>'$dlnsep'
	}
    }
}


sub modules_etc
{
    $|=1; # enable this line for auto-flushed output
    use strict;
    use rob_misc;
    use threads;
    use Sort::Key qw(nsort);
    use 5.16.0;
    use binary_grid;
    use threads::shared;
    use Carp qw(confess);
    use Proc::ProcessTable;
}
