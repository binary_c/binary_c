#!/usr/bin/env perl
$|=1; # enable this line for auto-flushed output
use strict;
use Sort::Key qw(nsort);
use rob_misc;
use binary_grid;
use binary_grid::condor;
use threads;
use threads::shared;
use Fcntl ':flock'; # import LOCK_* constants
use subs qw(logprint logprintf);
use Carp qw(confess);
use File::Path qw(make_path);
#
# Flexigrid-condor script for the Yong Mg24/Mg26 project
#

my $nm; my $nm1; my $nm2; my $nper; my $necc;

# grid resolution : full recommended resolution after #s
my $nm=100; # 1000
my $nm1=100; # 100
my $nm2=40; # 40
my $nper=40; # 40
my $necc=20;
my $useecc=0;

# lores
#$nm=10; $nm1=10; $nm2=5; $nper=5;

# midres
#$nm=20; $nm1=20; $nm2=40; $nper=20;

# M1 (and M2)
my $mmin=0.1; # should be 0.01 to include planets (?) or 0.1 for stars only
my $mmax=8.0; 

# period distribution D+M91 options
my $logpermin=-2.0;
my $logpermax=12.0;
my $gauss_logmean=4.8;
my $gauss_sigma=2.3;

# q distribution
my $qbeta=0.0; # use qbeta by default
my $binary_fraction_shift=0.0;

# period distribution
my $period_distribution='bastard'; # 'DM91' or 'bastard'

my $outlock : shared;
my @spectral_type_strings=('O','B','A','F','G','K','M');
my %spectral_type;  revspechash();
my @stellar_type_strings;
my @short_stellar_type_strings;
my %short_stellar_type_strings;
my %binwidth=('period'=>0.1, # log period
	      'eccentricity'=>0.05);
my $outdir;
my $log_fp;

# fix aifa environment/paths
fix_aifa_environment();
my $results={}; # reset results
defaults();
condor_grid();
output();
exit(0);

############################################################
############################################################
############################################################

sub defaults
{
    binary_grid::parse_args();
    grid_defaults();
    renice_me(19);
    binary_grid::parse_args();

    my $args="@ARGV";
    $outdir=($args=~/outdir=(\S+)/)[0];
    
    # optional parameters
    $qbeta=($args=~/qbeta=(\S+)/)[0] // $qbeta;
    $binary_fraction_shift=($args=~/binary_fraction_shift=(\S+)/)[0]//$binary_fraction_shift;

    # condor directory : this stores the temp files and the final 
    # output : you should make sure it is read/writeable on all
    # condor notes (e.g. via an NFS link)
    $binary_grid::grid_options{condor_dir}='/users/izzard/data/condor/yong_tmp';
    $binary_grid::grid_options{condor_dir}=$ENV{HOME}.'/data/condor/yong_tmp' if(`hostname`=~/buntspecht/);

    # condor requirements
    $binary_grid::grid_options{condor_options}{Requirements}=
	'Machine!="aibn73.astro.uni-bonn.de" && Machine!="aibn79.astro.uni-bonn.de"';
#	'Machine=="aibn36.astro.uni-bonn.de"';	

    $binary_grid::grid_options{condor_njobs}=4;#condor_free();
    print "Use $binary_grid::grid_options{condor_njobs} condor slots\n"; 
 
    # always link binary_grid to our local results hash
    $binary_grid::grid_options{results_hash}=$results;

    # use which parser function?    
    $binary_grid::grid_options{parse_bse_function}='parse_bse';
    
    $binary_grid::grid_options{threads_stack_size}=50; # MBytes
    
    $binary_grid::grid_options{prog}='binary_c-yong';
    $binary_grid::grid_options{'gdb wait'}=undef;

    $binary_grid::bse_options{z}=1e-4;
    $binary_grid::bse_options{wd_sigma}=0.0;

    # tides: 1.0=normal 0=no tides
    $binary_grid::bse_options{tidal_strength_factor}=1.0;

    $binary_grid::bse_options{alpha_ce}=0.1; # 1
    $binary_grid::bse_options{lambda_ce}=-1; # -1 = automatically set
    $binary_grid::bse_options{lambda_ionisation}=0.5; # 0.0
    $binary_grid::bse_options{lw}=1.0;

    # star formation stuff
    $binary_grid::bse_options{max_evolution_time}=15000; # Myr
    
    # time resolution
    $binary_grid::bse_options{'maximum_timestep'}=10;

    # code options
    $binary_grid::grid_options{'nice'}='nice -n +10'; # nice command e.g. 'nice -n +10' or '' 
    
    # use C routines when available (should be faster)
    $binary_grid::grid_options{'code flavour'}='C';
    
    $binary_grid::grid_options{'timeout'}=0; # seconds until timeout
    
    $binary_grid::grid_options{'log_args'}=1;
    $binary_grid::grid_options{'vb'}=1; # make verbose
    $binary_grid::grid_options{'tvb'}=1; # thread logfile

    # activate flexigrid
    # build flexigrid
    
    # maximum stack size before we pause to allow stars to finish
    $binary_grid::grid_options{'flexigrid'}{'maxstack'}=10;

    # use local threads or a daemon
    $binary_grid::grid_options{'flexigrid'}{'method'}='local threads';

    # binary_c approximate runtime in s
    $binary_grid::grid_options{'binary_c_runtime'}=1e-3;
    
    # first attempt at an MC grid
    #$binary_grid::grid_options{'flexigrid'}{'grid type'}='monte carlo';

    my $nvar=0; 
 


    # duplicity
    $binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
    {
	'name'=>'duplicity',
	'longname'=>'Duplicity',
	'range'=>[0,1],
	'resolution'=>1,
	'spacingfunc'=>'number(1.0)',
	'precode'=>'$binary_grid::grid_options{binary}=$duplicity;binary_grid::flexigrid_grid_resolution_shift();',
	'gridtype'=>'edge',
	'noprobdist'=>1,
    };
    
    
    # Mass 1
    $binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
    {
	'name'=> 'lnm1', 
	'longname'=>'Primary mass', 
	'range'=>["log($mmin)","log($mmax)"],
	'resolution'=> "$nm1", # ignore single stars in approximate resolution calculation
	'spacingfunc'=>"const(log($mmin),log($mmax),\$binary_grid::grid_options{binary} ? $nm1 : $nm)",
	'precode'=>'$m1=exp($lnm1);  $eccentricity=0.0; my $binary_fraction=MAX(0.0,MIN(1.0,distribution_functions::raghavan2010_binary_fraction($m1)+$main::binary_fraction_shift));  $binary_grid::grid_options{weight}= $binary_grid::grid_options{binary}==0 ? 1.0-$binary_fraction : $binary_fraction;   ',
	#'probdist'=>"ktg93(\$m1)*\$m1", # KTG93 (all masses)
	'probdist'=>"Kroupa2001(\$m1)*\$m1",
	#'probdist'=>'powerlaw(0.8,100.0,-2.35,$m1)*$m1', # salpeter (0.8-100)
	'dphasevol'=>'$dlnm1',
    };
    
    # Binary stars: Mass 2 and Separation
    my $m2min=$mmin;

    if(defined($qbeta))
    {
	# use a q powerlaw with slope $qbeta
	$binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
	{
	    'condition'=>'$binary_grid::grid_options{binary}==1',
	    'name'=>'q',
	    'longname'=>'Mass ratio',
	    'range'=>[$m2min.'/$m1',1.0],
	    'resolution'=>$nm2,
	    'spacingfunc'=>'const('.$m2min.'/$m1,1.0,'.$nm2.')',
	    'probdist'=>"powerlaw(0.0,1.0,$qbeta,\$q)",
	    'precode'=>'$m2=$q*$m1;',
	    'dphasevol'=>'$dq',
	};
    }
    else
    {
	print STDERR "Mass ratio distribution unknown\n";
	print "Mass ratio distribution unknown\n";
	exit;
    }

    
    if($period_distribution eq 'DM91')
    {
	# old code
	$binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
	{  
	    'name'=>'logper',
	    'longname'=>'log(Orbital_Period)',
	    'range'=>[$logpermin,$logpermax],
	    'resolution',$nper,
	    'spacingfunc',"const($logpermin,$logpermax,$nper)",
	    'precode'=>"\$per=10.0**\$logper;\$sep=calc_sep_from_period(\$m1,\$m2,\$per)if(defined(\$m1)&&defined(\$m2));",
	    'probdist'=>"gaussian(\$logper,$gauss_logmean,$gauss_sigma,$logpermin,$logpermax)",
	    'dphasevol'=>'$dln10per'
	}
    }
    elsif($period_distribution eq 'bastard')
    {
	# period distribution : rob's bastard
	$binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
	{
	    'name'=>'log10per',
	    'longname'=>'log10(Orbital_Period)',
	    'range'=>['-1.0','10.0'],
	    'resolution',$nper,
	    'spacingfunc',"const(-1.0,10.0,$nper)",
	    'precode'=>"my \$per=10.0**\$log10per;my\$sep=calc_sep_from_period(\$m1,\$m2,\$per);",
	    'probdist'=>'Izzard2012_period_distribution($per,$m1)',
	    'dphasevol'=>'$dlog10per'
	};
    }
    else
    {
	print "Unknown period distribution $period_distribution\n";
	exit;
    }


    if($useecc){
    # eccentricity
    $binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
    {
	'name'=>'ecc',
	'longname'=>'log10(Eccentricity)',
	'range'=>[0,0.999],
	'resolution',$necc,
	'spacingfunc',"const(0.0,0.999,$necc)",
	'precode','$eccentricity=$ecc;',
	# gaussian as in Meiborn & Mathieu 2005
	'probdist'=>'gaussian($ecc,0.38,0.23,0.0,0.999)',
	'dphasevol'=>'$decc'
    };
    }

    printf "Flexigrid modulo = %d, offset = %d\n",
    $binary_grid::grid_options{modulo},
    $binary_grid::grid_options{offset};

    binary_grid::parse_args();
    secondary_variables();
    setup_binary_grid();

    @stellar_type_strings=@{(stellar_type_strings())[2]};
    @short_stellar_type_strings=@{(stellar_type_strings())[3]};
    %short_stellar_type_strings=%{(stellar_type_strings())[4]};
    #print "Short @short_stellar_type_strings\n";
}

############################################################

sub output
{
    # if we're running condor, do not output unless for the final call
    return if($binary_grid::grid_options{condor_command} eq 'run_flexigrid');

    # open log file
    $outdir = $ENV{VOLDISK}.'/data/yong/' 
	if(!defined($outdir));
    make_path($outdir); # nb we assume this works...

    # output binary_grid::grid_options/bse_options
    open($log_fp,">$outdir/grid_options")||
	confess("cannot open $outdir/grid_options for output");
    print $log_fp "grid_options\n\n";
    map
    {
	print {$log_fp} $_.' : '.$binary_grid::grid_options{$_}."\n";
    }keys %binary_grid::grid_options;
    close $log_fp;
    open($log_fp,">$outdir/bse_options")||
	confess("cannot open $outdir/bse_options for output");
    print $log_fp "bse_options\n\n";
    map
    {
	print {$log_fp} $_.' : '.$binary_grid::bse_options{$_}."\n";
    }keys %binary_grid::bse_options;
    close $log_fp;
    
    # output grid results
    open ($log_fp,">$outdir/log") || 
	confess ("cannot open $outdir/log for output");
    logprint "\n\n\nYONG PROJECT Grid results\n============================================================\n\n";
    logprint "SVN Revision ",`svn info|grep Revision|gawk \"\{print \\\$2\}\"`;
    logprint "Run at ",scalar(localtime),"\n";
    logprint "Resolution: nm=$nm nm1=$nm1 nm2=$nm2 nper=$nper\n";
    logprint "$mmin < M < $mmax\n";
    logprint "Period distribution : $period_distribution : $logpermin < logP < $logpermax : guassian mu=$gauss_logmean sigma=$gauss_sigma\n";
    logprint "\n\nParameters:\n";
    logprint "Initial binary fraction : Raghavan 2010\n";
    
    
    foreach my $k (sort keys %$results)
    {
	if($k eq 'Mg26/Mg24')
	{
	    foreach my $duplicity (keys %{$results->{$k}})
	    { 
		my $dtptot=0.0;
		foreach my $mgratio (keys %{$results->{$k}->{$duplicity}})
		{ 
		    $dtptot += $$results{$k}{$duplicity}{$mgratio};
		}
		
		logprint "Mg26/Mg24 ratios for $duplicity stars\n";
		my $f="$outdir/Mg26_24-$duplicity.dat";

		open(FP,">$f")||confess("can't open $f");
		foreach my $mgratio (nsort keys %{$results->{$k}->{$duplicity}})
		{ 
		    logprint sprintf "%g %g\n",$mgratio,$$results{$k}{$duplicity}{$mgratio}/$dtptot; 
		    printf FP "%g %g\n",$mgratio,$$results{$k}{$duplicity}{$mgratio}/$dtptot; 
		}
		close FP;

	    }
	}
    }

    close $log_fp;

}

sub parse_bse
{
    # this gets called by the binary_grid module, it parses the 
    # lines of output from binary_c/nucsyn
    my $brk=0; 

    # you should set data in the $h hash pointer, which is 
    # then passed to the join_thread function (below) for you
    # to add up. (Note: $h is local to each thread.)
    my $h=shift;

    my $nthread=$binary_grid::grid_options{'thread_num'};
    my @init_spectypes;
    
    while($brk==0)
    {
	$_=tbse_line();
	#print "PARSE $_\n";
	chomp;
	if($_ eq 'fin')
	{
	    $brk=1; # the end of output
 	}
	elsif(/YONG([sb]) (\S+) (\S+)/)
	{
	    # test MS star
	    my $duplicity= $1 eq 's' ? 'Single' : 'Binary';
	    my $Mgratio = $2 ;
	    my $dtp=$3;
	    #print "DATA : $_\n";
	    #print "Add Yong $duplicity star with Mg26/Mg24=$Mgratio dtp=$dtp\n";
	
	    $$h{'Mg26/Mg24'}{$duplicity}{bin_data(log10($Mgratio+1e-14),0.1)}+=$dtp;
	}
    }
}


sub secondary_variables
{
    # from the defaults and/or passed in variables,
    # calculate useful things

    # use colour?
    if($binary_grid::grid_options{'colour'}==1)
    {
        # use ANSI color for output to xterms/rxvt etc.
        use Term::ANSIColor;
        # set to white on black
        print color("white on_black");
    }
    else
    {
        # dummy function to replace the ANSI color() function
        eval("sub\ color\{ \}");
    }
}

sub logprint
{
    print {$log_fp} "@_";
    print "@_";
}
sub logprintf
{
    my $format=shift @_;
    printf {$log_fp} $format,@_;
    printf $format,@_;
}

sub revspechash
{
    for(my $i=0;$i<=$#spectral_type_strings;$i++)
    {
	$spectral_type{$spectral_type_strings[$i]}=$i;
    }
}

sub spec_to_float
{
    my $s=$_[0]; # spectral type to check
    if($s=~/([A-Z])(\d)/)
    {
	return $spectral_type{$1}+0.1*$2;
    }
    else
    {
	print "Failed to convert spec type $_[1] to float\n";
	exit;
    }
}

sub spec_type_sorter
{
    return spec_to_float($a) <=> spec_to_float($b);
}

sub pad_datahash
{
    # pad datahash $h so it has zeros where there's no data
    my $h=shift;
    my $d=shift;
    my @k=nsort keys %$h;
    my $xmax=$k[$#k]; # max value
    my $x=$k[0]; # start (min) value
    while($x<$xmax)
    {
	$$h{$x}=0.0 if(!defined($$h{$x}));
	$x=sprintf '%g',$x+$d;
    }
}

sub SFR
{
    # SFR as a function of Galactic age in Myr
    my $t=$_[0];

    return 1.0; # const SFR
    #return $t<1.0e3 ? 1.0 : 0.0; # const for a given time period
    #return exp(-$t/$SF_timescale}); # exponential
}

sub fix_aifa_environment
{
    if(`hostname`=~/aibn/)
    {
	map
	{
	    print "CHECK $_\n";
	    if($ENV{$_}=~s!/export/aibn(\d+)_(\d+)/!/vol/aibn$1/aibn$1\_$2/!g)
	    {
		print "CONV $_=$ENV{$_}\n";
	    }
	}%ENV;
    }
}

sub condor_free
{
    # return number of free condor slots
    my $cmd='condor_status|grep -A2 Claimed\ Unclaimed|tail -1|gawk "{print \$5}"';
    my $n=`$cmd`;
    chomp $n;
    return $n;
}
