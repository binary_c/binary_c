#!/usr/bin/env perl
use strict;
use rob_misc;
use List::MoreUtils qw/uniq/;

my @feh=qw/-2.80 -2.60 -2.40 -2.20 -2.00 -1.80 -1.60 -1.40 -1.20 -1.00 -0.80 -0.60 -0.40 -0.20  0.00  0.20/;

my @Zs=uniq(sort (map{MAX(1e-4,MIN(0.03,sprintf "%.5f",10.0**$_*0.02))}@feh,0.02,0.0001));

print "Metallicities to run : @Zs\n";

foreach my $Z (@Zs)
{
    print "Launch $Z\n";
    runcmd("./src/perl/scripts-flexigrid/grid-hrd-norberto.pl Z=$Z testing=1");
    exit;
}

