#!/usr/bin/env perl
use strict;
use Data::Dumper;
use Time::HiRes qw(sleep gettimeofday tv_interval); # for timers

############################################################
#                                                          #
# Example of how to use the binary_grid flexigrid.         #
#                                                          #
############################################################

modules_etc();# load modules

my $results={}; # hash to store results

# time window for star formation : select only stars 
# between these ages (nb max ev time is 15000)
my @time_windows = ({min=>14000,max=>14001}, # m~0.825
		    {min=>8000,max=>8001} # m~0.910
    );
my %timers;

# first set some defaults for the grid
defaults();

# then parse the command line to override the defaults
my $ncpus=$ARGV[0] if($ARGV[0] =~/^\d+$/);
parse_grid_args(@ARGV);

# now set up the grid
setup_binary_grid();

# run flexigrid with two threads
flexigrid($ncpus//4); 

# output results
output(); 

exit(0); # done!

############################################################
############################################################
############################################################

sub output
{
    my $h=$$results{HRD};
     
    foreach my $key (keys %$h)
    {
	
	# loop over Teff, Lum
	my $out=0; 
	my $hrd = $$h{$key};

	foreach my $st (keys %{$$h{$key}})
	{
	    $hrd = $$h{$key}{$st};

	    my $keyst = $key.'_'.$st;
	    my $file = "/tmp/hrd_$keyst.dat";
	    my $surffile = "/tmp/hrd_$keyst.surf.dat";
	    
	    open(FP,'>',$file) || die;

	    foreach my $logteff (nsort (keys (%$hrd)))
	    { 
		foreach my $logL (nsort(keys %{$hrd->{$logteff}}))
		{
		    my $norm = $$h{dtp}{$st}{$logteff}{$logL};

		    if($norm > 1e-50)
		    {		
			if($key ne 'dtp')
			{
			    # re-normalize to find mean 
			    $$hrd{$logteff}{$logL} /= $norm;
			}
			
			printf FP "%g %g %g\n",
			$logteff,
			$logL,
			$$hrd{$logteff}{$logL};
			
			$out=1;
		    }
		}
		print FP "\n" if($out);;
	    }
	    close FP;

	    # gnuplot surface conversion
	    if(-s $file)
	    {
		`gnuplot_surface3.pl $file > $surffile`;
	    }
	}
    }

    output_binary_grid_timers();
    output_binary_grid_timers(\%timers);
    
}

sub parse_bse
{
    my $t0 = scalar gettimeofday;

    # this gets called by the binary_grid module, it parses the 
    # lines of output from binary_c/nucsyn
    my $brk=0; 

    # you should set data in the $h hash pointer, which is 
    # then passed to the join_thread function (below) for you
    # to add up. (Note: $h is local to each thread.)
    my $h=$_[0];

    my $nthread=$binary_grid::grid_options{'thread_num'};

    while($brk==0)
    {
	$_=tbse_line();
	#print $_;
	chomp;
	if($_ eq 'fin')
	{
	    $brk=1; # the end of output
	}
	elsif(s/^HRD2 //)
	{
	    # found HRD data
	    my @x=split(' ',$_);

	    # populate HRD
	    my $time = $x[19];
	    
	    # select time : is it in a window?
	    my $in_window=0;
	    foreach my $time_window (@time_windows)
	    {
		#print "Check $$time_window{min} < $time < $$time_window{max}\n"; 
		if($time>$$time_window{min} &&
		   $time<$$time_window{max})
		{
		    $in_window=1;
		    last;
		}
	    }
	    	    
	    if($in_window==1)
	    {
		my $binwidth = 0.02;
		my $logteff = bin_data(log10($x[10]),$binwidth);
		my $logL = bin_data(log10($x[11]),$binwidth);
		my $st = $x[9];
		my $CN=$x[18];
		my $m = $x[20];
		my $dtp = $x[1];

		# C12=15, C13=16, N14=17, [C/N]=18, binary_c time=19,
		# mass=20

		#next if($logL!=-0.27 || $logteff!=3.71);

		# the main HRD
		$$h{HRD}{dtp}{all}{$logteff}{$logL} += $dtp;
		$$h{HRD}{dtp}{$st}{$logteff}{$logL} += $dtp;
		
		# bin [C/N]
		my $cndtp = $dtp * $CN;
		$$h{HRD}{CN}{all}{$logteff}{$logL} += $cndtp;
		$$h{HRD}{CN}{$st}{$logteff}{$logL} += $cndtp;
		
		# bin masses
		my $mdtp = $dtp * $m;
		$$h{HRD}{m}{all}{$logteff}{$logL} += $mdtp;
		$$h{HRD}{m}{$st}{$logteff}{$logL} += $mdtp;
	    }
	}
    }

    $timers{'main::parse_bse'} += scalar gettimeofday - $t0;
}


sub defaults
{
    grid_defaults();
  
    # physics options
    $binary_grid::bse_options{'z'}=0.003;
    $binary_grid::bse_options{'wd_sigma'}=0.0;
    $binary_grid::bse_options{'c13_eff'}=1.0;
    $binary_grid::bse_options{'acc2'}=1.5;
    $binary_grid::bse_options{'tidal_strength_factor'}=1.0;
    $binary_grid::bse_options{'alpha_ce'}=1.0; # 1
    $binary_grid::bse_options{'lambda_ce'}=-1; # -1 = automatically set
    $binary_grid::bse_options{'lambda_ionisation'}=0.0; # 0.0
    $binary_grid::bse_options{'delta_mcmin'}=0.0;
    $binary_grid::bse_options{'lambda_min'}=0.0;
    $binary_grid::bse_options{'minimum_envelope_mass_for_third_dredgeup'}=0.5;
    $binary_grid::bse_options{'max_evolution_time'}=15000;
    $binary_grid::bse_options{'gbwind'}=0;
    $binary_grid::bse_options{'gb_reimers_eta'}=0.0;

    $binary_grid::bse_options{'hrdiag_output'}=1;
    $binary_grid::bse_options{'dup1mult'}=1.0;
    #$binary_grid::bse_options{'maximum_timestep'}=1.0;
   
    # grid options
    $binary_grid::grid_options{'results_hash'}=$results;
    $binary_grid::grid_options{'parse_bse_function_pointer'}=\&parse_bse;
    $binary_grid::grid_options{'threads_stack_size'}=32; # MBytes
    $binary_grid::grid_options{'nice'}='nice -n +10'; # nice command e.g. 'nice -n +10' or '' 
    $binary_grid::grid_options{'prog'}='binary_c-thickdisk';

    $binary_grid::grid_options{'binary'}=0; # single stars -> 0, binary stars -> 1
    $binary_grid::grid_options{'timeout'}=30; # seconds until timeout
    $binary_grid::grid_options{'log_args'}=1;
    $binary_grid::grid_options{'vb'}=0;
    $binary_grid::grid_options{'timers'}=1;
    $binary_grid::grid_options{'threadcomms'}=0;

    $binary_grid::grid_options{pre_filter_file} = '/tmp/pre';
    $binary_grid::grid_options{post_filter_file} = '/tmp/post';

    
    # Mass 1
    my $nvar=0;
    my $mmin=0.8;
    my $mmax=1.2;
    my $n=100; # resolution
    $binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
    {
	'name'=> 'lnm1', 
	'longname'=>'Primary mass', 
	'range'=>["log($mmin)","log($mmax)"],
	'resolution'=> $n,
	'spacingfunc'=>"const(log($mmin),log($mmax),$n)",
	'precode'=>'my $m1=exp($lnm1); my $eccentricity=0.0;',
	'probdist'=>"ktg93(\$m1)*\$m1",
	'dphasevol'=>'$dlnm1'
    };

    # Binary stars: Mass 2 and Separation
    if($binary_grid::grid_options{'binary'})
    {
	my $m2min=0.1;
	$binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
	{
	    # name of the variable
	    'name'=>'m2',
	    # long name (for verbose logging)
	    'longname'=>'Secondary mass',
	    # range of the parameter space
	    'range'=>[$m2min,'$m1'],
	    # resolution 
	    'resolution'=>$n,
	    # constant grid spacing function
	    'spacingfunc'=>"const($m2min,\$m1,$n)",
	    # flat-q distribution between m2min and m1
	    'probdist'=>"const($m2min,\$m1)",
	    # phase volume contribution
	    'dphasevol','$dm2'
	};

	
	$binary_grid::grid_options{'flexigrid'}{'grid variable '.$nvar++}=
	{
	    # name of the variable
	    'name'=>'lnsep',
	    # long name (for verbose logging)
	    'longname'=>'ln(Orbital_Separation)',
	    # range of the separation space
	    'range'=>['log(3.0)','log(1e4)'],
	    # resolution
	    'resolution'=>$n*10,
	    # constant spacing in ln-separation
	    'spacingfunc'=>"const(log(3.0),log(1e4),$n)",
	    # precode has to calculation the period (required for binary_c)
	    'precode'=>"my\$sep=exp(\$lnsep);my\$per=calc_period_from_sep(\$m1,\$m2,\$sep);",
	    # flat in log-separation (dN/da~1/a) distribution (Opik law0
	    #'probdist'=>'const(log(3.0),log(1e4))',
	    'probdist'=>'powerlaw(log(3.0),log(1e4),-1.0,$lnsep)',
	    # phase volume contribution
	    'dphasevol'=>'$dlnsep'
	}
    }
}


sub modules_etc
{
    $|=1; # enable this line for auto-flushed output
    use strict;
    use rob_misc;
    use threads;
    use Sort::Key qw(nsort);
    use 5.16.0;
    use binary_grid;
    use threads::shared;
    use Carp qw(confess);
    use Proc::ProcessTable;
}
