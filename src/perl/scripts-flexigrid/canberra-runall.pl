#!/usr/bin/env perl
use strict;
use rob_misc;
# wrapper script to run ALL the canberra grids
$|=1;

my $gridscript = "./src/perl/scripts-flexigrid/canberra-simplex.pl";

my $rundirroot = $ENV{VOLDISK}."/data/canberra/paramspace";

if(1)
{
    # parameter space exploration 

    # vary WD kick
    foreach my $wd_sigma (1,1.5,2,3,4,10)
    {
	run_grid({rundir=>"wd_sigma/wd_sigma=$wd_sigma",
		  args=>"paramspace{wd_sigma}{init}=$wd_sigma"});
    }

# vary Z
    foreach my $z (0.0001, 0.001, 0.01, 0.02, 0.03)
    {
	run_grid({rundir=>"z/z=$z",
		  args=>"paramspace{z}{init}=$z"});
    }

# vary alpha_CE
    foreach my $alpha_ce (0.0,0.1,0.3,0.5,0.7,1.0)
    {
	run_grid({rundir=>"alpha_ce/alpha_ce=$alpha_ce",
		  args=>"paramspace{alpha_ce}{init}=$alpha_ce"});
    }
    
    if(0)
    {

# NO LONGER REQUIRED with the newbastard distribution

# vary qbeta
	foreach my $qbeta (-1.3,-1.25,-1.1,-0.99,-0.9,-0.825,-0.75,-0.5,-0.25,0,0.25)
	{
	    run_grid({rundir=>"qbeta/qbeta=$qbeta",
		      args=>"paramspace{qbeta}{init}=$qbeta"});
	}

# vary binary fraction shift
	foreach my $binary_fraction_shift (-0.5,-0.25,0.0,0.25)
	{
	    run_grid({rundir=>"binary_fraction_shift/binary_fraction_shift=$binary_fraction_shift",
		      args=>"paramspace{binary_fraction_shift}{init}=$binary_fraction_shift"});
	}
    }

}
exit;

if(0){
############################################################
# best fits
############################################################
    run_grid({rundir=>"fit_qbeta/",
	      args=>"paramspace{qbeta}{vary}=1"});

run_grid({rundir=>"fit_binary_fraction_shift/",
	  args=>"paramspace{binary_fraction_shift}{vary}=1"});

# best fit with alpha_ce = 0 
run_grid({rundir=>"fit_qbeta_and_binary_fraction_shift/",
	  args=>"paramspace{alpha_ce}{init}=0.0 paramspace{qbeta}{vary}=1 paramspace{binary_fraction_shift}{vary}=1"});

run_grid({rundir=>"fit_qbeta_and_binary_fraction_shift/",
	  args=>"paramspace{qbeta}{vary}=1 paramspace{binary_fraction_shift}{vary}=1"});

# the more dubious parametebrs!
run_grid({rundir=>"fit_wd_sigma/",
	  args=>"paramspace{wd_sigma}{vary}=1"});
run_grid({rundir=>"fit_z/",
	  args=>"paramspace{z}{vary}=1"});
run_grid({rundir=>"fit_alpha_ce/",
	  args=>"paramspace{alpha_ce}{vary}=1"});
}

exit;

sub run_grid
{
    my $opts=$_[0];

    my $rundir="$rundirroot/$opts->{rundir}";
    $rundir=~s/=/_/g;
    
    my $cmd="$gridscript batch=1 rundir=$rundir $opts->{args}";
    print "CMD $cmd\n";
    open(CMD,"$cmd |")||die("cannot open cmd \"$cmd\"");
    while(<CMD>)
    {
	print $_;
    }
    close CMD; 
}
