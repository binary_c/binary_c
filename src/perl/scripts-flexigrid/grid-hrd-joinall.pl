#!/usr/bin/env perl
use strict;
use rob_misc;

foreach my $d (grep{chomp && -d $_} `ls|grep ^hrd`)
{
    print "Process $d\n";

    foreach my $f (grep{chomp}`find $d/stdout/`)
    {
        #print "Check $f\n";
        my $stdout=slurp($f);
        #print $stdout;
        if($stdout=~/Join postponed/ &&
	   $stdout=~/Join script which you should execute\:\s+(.*)/)
        {
            my $cmd=$1.'cmd';
	    
	    if(-s -e -f $cmd)
	    {
		$cmd = "cd /users/izzard/progs/stars/binary_c/; ".$cmd;
		print "Running $cmd\n";
		
		open(CMD,"-|",$cmd)
		    or die("cannot run $cmd");
		while(<CMD>) { print $_ } 
		close CMD;
	    }
        }
    }
}
