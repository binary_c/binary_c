#!/usr/bin/env perl
use strict;

my $defaults = "testing=0 maxlogt=10.25 sn_sigma=0 wd_sigma=0";

#foreach my $nres (3,10,20,40,60)#,80,100)
foreach my $nres (100)
{
    #foreach my $fshannon (0.1,0.25,0.5,1.0,2.0)
    #foreach my $fshannon (0.01,0.05)

    foreach my $fshannon (0.25,0.1)
    {
	my $args = "$defaults nres=$nres fshannon=$fshannon";

	open(CMD,"-|","src/perl/scripts-flexigrid/grid-hrd-condor.pl $args")||die("cannot open grid-hrd-condor.pl script");
	#open(CMD,"-|","src/perl/scripts-flexigrid/grid-hrd-flexigrid.pl $args")||die("cannot open grid-hrd-condor.pl script");
	while(<CMD>) { print $_ } 
	close CMD;
    }
}
 
