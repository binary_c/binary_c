#!/usr/bin/env perl
use strict;
use Term::ANSIColor;

# wrapper to launch many yields_vs_time scripts on condor

my $yvt="/users/izzard/progs/stars/binary_c/src/perl/scripts-flexigrid/yield_vs_time-condor.pl dt=1 max_evolution_time=14000 sources=1 ensemble=0 ";

foreach my $binary (0,1)
{
    foreach my $Z (1e-4,1e-3,0.004,0.008,0.02,0.03)
#foreach my $Z (1e-4,4e-4,1e-3,4e-3,0.008,0.01,0.015,0.02,0.025,0.03)
    {
	foreach my $wind ('tpagbwind=0', # karakas wind
			  'tpagbwind=3 tpagb_reimers_eta=0.1', # Bloecker
			  'tpagbwind=3 tpagb_reimers_eta=0.02', # Bloecker via Ventura
	    )
	{
	    my $x="z=$Z $wind binary=$binary";
	    my $gridstring = $x;
	    $gridstring=~s/ /_/g;

	    my $cmd="$yvt $x gridstring=$gridstring";

	    if( `ls /users/izzard/data/condor/yvt2/$gridstring`=~/yields_vs_time/)
	    {
		print color('green'),"Already run\n",color('reset');
	    }
	    else
	    {

		print color('red'),"$cmd\n",color('reset');
		`$cmd`;
		#sleep 5;
		#exit;
	    }
	}
    }
}
