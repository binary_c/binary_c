#!/usr/bin/env perl
$|=1; # enable this line for auto-flushed output
use strict;
use Sort::Key qw(nsort);
use rob_misc;
use binary_grid;
use binary_grid::condor;
use threads;
use threads::shared;
use Fcntl ':flock'; # import LOCK_* constants
use Carp qw(confess);
use File::Path qw(make_path);
use File::Basename;
use Cwd;

my $testing=0; # 1 = low resolution, few jobs

# script to make stellar population ensembles with condor 

# Version 1.20pre68 : to match binary_c version number

my $results={}; # hash to store results
my $outdir='/users/izzard/data/condor/ensemble';

use vars qw($dt $differential $ensemble @ensemble_label $exacttimes $gnuplot $gridstring @isotopes $idt $macrosfile $meantimes $nbins $nensemble $nisotopes $nisotopes1 $norm_output $nsources $sources @source_label $version_string $yieldoutfile );

#
# memory use: with dt=10 about 1GB is required to join the hashes
# ... so expect memory use to be a pain at dt=1

# fix aifa environment/paths
fix_aifa_environment();

# combined single/binary star grid
defaults();

condor_grid();
#flexigrid(1);

all_output();

output_binary_grid_timers();
exit(0);

############################################################
############################################################
############################################################

sub all_output
{
    # if we're running condor, do not output unless for the final call
    return if($binary_grid::grid_options{condor_command} eq 'run_flexigrid');

    make_path($outdir); # nb we assume this works...

     # output
    print "\n\nOutputting...\n" if($binary_grid::grid_options{vb});

    $$results{total_mass_ejected}=total_mass_ejected();
    printf "Total mass ejected: %g\n",$$results{total_mass_ejected};
    printf "Total mass into stars: %g\n",$$results{mass_into_stars};
    printf "Return fraction %3.2f %%\n",$$results{total_mass_ejected}/$$results{mass_into_stars}*100.0;

    if($sources)
    {
	for(my $i=0;$i<$nsources;$i++)
	{
	    output($i); # output for every source
	}
    }
    else
    {
	output(0); # default to source 0
    }
    
    print "\n";  

############################################################
# The end, output results
# perhaps plot stuff (could be slow!)
    output_gp() if($gnuplot);

    output_ensemble() if($ensemble);

    print color('reset');
}

sub parse_bse
{ 
    my $h=$_[0]; # data hash to be populated
    my $brk=0;
    my $m=$binary_grid::grid_options{progenitor_hash}{m1};
    my $mp=$binary_grid::grid_options{progenitor_hash}{m1} * $binary_grid::grid_options{progenitor_hash}{prob};
    my $p=$binary_grid::grid_options{progenitor_hash}{prob};
    
    # add up mass into stars and notstars (planets/brown dwarfs)
    ($binary_grid::grid_options{progenitor_hash}{m1}>0.1) ? $$h{mass_into_stars} : $$h{mass_into_notstars} += $mp;
    ($binary_grid::grid_options{progenitor_hash}{m2}>0.1) ? $$h{mass_into_stars} : $$h{mass_into_notstars} += $binary_grid::grid_options{progenitor_hash}{m2} * $binary_grid::grid_options{progenitor_hash}{prob};

    while($brk==0)
    {
	$_=tbse_line();

	if(/nan/io)
	{
	    print "NAN ERROR in thread $binary_grid::grid_options{thread_num}\n";
	    print "tbse_line is $_\n";
	    print "ARGS : $binary_grid::grid_options{args}\n";
	    exit; # quit everything!
	}

	next if (/^\#/o);

	if($_ eq 'fin')
	{
	    $brk=1;
	}
	else
	{
	    if(($ensemble)&&(s/^STELLARPOPENSEMBLE\d+__ //o))
	    {
		process_ensemble_line($h,$_);
	    }
	    elsif(s/^DXYIELDbin__ //o)
	    {
		# no sources
		process_yield_line($h,0,$_);
	    }
	    elsif(s/^DXYIELDbin(\d+)?__ //o)
	    {
		# sources
		process_yield_line($h,$1,$_);
	    }
	}
    }
}



sub test_parse_bse
{ 
    my $h=$_[0]; # data hash to be populated
    my $brk=0;
    my $m=$binary_grid::grid_options{progenitor_hash}{m1};
    my $mp=$binary_grid::grid_options{progenitor_hash}{m1} * $binary_grid::grid_options{progenitor_hash}{prob};
    my $p=$binary_grid::grid_options{progenitor_hash}{prob};

    # add up mass into stars and notstars (planets/brown dwarfs)
    ($binary_grid::grid_options{progenitor_hash}{m1}>0.1) ? $$h{mass_into_stars} : $$h{mass_into_notstars} += $mp;
    ($binary_grid::grid_options{progenitor_hash}{m2}>0.1) ? $$h{mass_into_stars} : $$h{mass_into_notstars} += $binary_grid::grid_options{progenitor_hash}{m2} * $binary_grid::grid_options{progenitor_hash}{prob};

    while($brk==0)
    {
	if(tbse_line() eq 'fin')
	{
	    $brk=1;
	}
    }

    for(my $t=0; $t < $binary_grid::bse_options{max_evolution_time}; $t += $dt)
    {
	# yield - source - timebin - isotope += amount
	process_yield_line($h,1,$t." 10.0 1.0 0.5");
    } 
}

sub defaults
{
    binary_grid::parse_args();
    grid_defaults();
    renice_me(0);

    # local options
    
    my $host=`hostname`; chomp $host;

    if($host eq 'buntspecht')
    {
	# laptop : localhost, ignore memory restrictions
	$binary_grid::grid_options{condor_memory}=0;
	$binary_grid::grid_options{condor_njobs}=4;
    }
    elsif($host =~/klaipeda/)
    {
	# remove trailing material
	$host=~s/\.astro\.uni-bonn\.de//;

	# 2,3,4 are always good machines
	my @good_machines=('klaipeda2',
			   #'klaipeda3',
			   'klaipeda4');

	# klaipeda1 cannot submit to itself... odd!
	push(@good_machines,'klaipeda1') if($host ne 'klaipeda1');
	
	$binary_grid::grid_options{condor_options}{Requirements}=
	    join(' || ',map {$_='Machine=="'.$_.'.astro.uni-bonn.de"'}
		 @good_machines);


	#$binary_grid::grid_options{condor_njobs}=MIN(48,MAX(12,condor_free()-16));

	# queue = 64 slots, want 4x this for ~ fair queueing
	$binary_grid::grid_options{condor_njobs}=256;
    }
    else
    {
	# aibn condor cluster

	# perhaps don't use the slower machines?
	#$binary_grid::grid_options{condor_options}{Requirements} =
	#'Machine!="aibn73.astro.uni-bonn.de" && Machine!="aibn79.astro.uni-bonn.de"';
	
	# number of jobs
	$binary_grid::grid_options{condor_njobs}=MAX(10,condor_free()-2);
    }

    # override njobs manually
    #$binary_grid::grid_options{condor_njobs}=10;# if($testing);

    print "Use $binary_grid::grid_options{condor_njobs} condor slots\n"; 
    
    # postpone joining : you must do this manually - why? RAM!
    $binary_grid::grid_options{condor_postpone_join}=1;

    # always link binary_grid to our local results hash
    $binary_grid::grid_options{results_hash}=$results;
    $binary_grid::grid_options{vb}=0;
    $binary_grid::grid_options{tvb}=0;
    
    # set the rootpath and the binary_c executable
    $binary_grid::grid_options{rootpath}='/users/izzard/progs/stars/binary_c';
    $binary_grid::grid_options{srcpath}='/users/izzard/progs/stars/binary_c/src';
    $binary_grid::grid_options{prog}='binary_c-ensemble';

    # no timeouts, no logging
    $binary_grid::grid_options{timeout}=0;
    $binary_grid::bse_options{log_filename}='/dev/null';
    
    # use which parser function?    
    $binary_grid::grid_options{parse_bse_function}='parse_bse';
    
    $binary_grid::grid_options{threads_stack_size}=50; # MBytes
    
    $binary_grid::grid_options{'gdb wait'}=undef;

    $binary_grid::bse_options{metallicity}=0.019;
    $binary_grid::bse_options{wd_sigma}=0.0;
    $binary_grid::bse_options{sn_sigma}=190.0;
    
    $binary_grid::bse_options{alpha_ce}=1.0; # 1
    $binary_grid::bse_options{lambda_ce}=-1.0; # -1 = automatically set
    $binary_grid::bse_options{lambda_ionisation}=0.0; # 0.0

    $binary_grid::bse_options{max_evolution_time}= 14000;

    # time resolution
    $binary_grid::bse_options{maximum_timestep}=10;

    # code options
    $binary_grid::grid_options{nice}='nice -n +10'; # nice command e.g. 'nice -n +10' or '' 
    
    $binary_grid::grid_options{timeout}=0; # seconds until timeout
    
    $binary_grid::grid_options{log_args}=0;
    $binary_grid::grid_options{save_args}=0;
    $binary_grid::grid_options{tvb}=0; # thread logfile

    # activate flexigrid
    # build flexigrid
    
    # maximum stack size before we pause to allow stars to finish
    $binary_grid::grid_options{flexigrid}{maxstack}=10;

    # use local threads or a daemon
    $binary_grid::grid_options{flexigrid}{method}='local threads';

    # binary_c approximate runtime in s
    $binary_grid::grid_options{binary_c_runtime}=1e-3;
    
    # yvt options
    # follow different sources of matter?
    $sources=1; # 0 : no, 1 : yes (requires appropriate switches
    # in binary_c header files)
    $differential=1; # if set to 1 output the time derivative 
    $ensemble=1; #  do ensembles by default

    # NB as of V1.20pre68 (SVN 1979+) $sources, $ensemble and $differential
    # are set based on binary_c --version
    

    
    $yieldoutfile=''; # if blank use default (compatible with gce.pl)

    $norm_output='mass_into_stars'; # how to normalize output
    # can be 'mass_into_stars' (default), 'total_mass_ejected','none'

    $macrosfile=$binary_grid::grid_options{srcpath}.'/nucsyn/nucsyn_macros.h';

    $gnuplot=0; # plot or not

    print "Getting code data\n";

    print "Getting isotope_list\n";
    @isotopes=isotope_list(); # uses binary_c --version
    $nisotopes //= $#isotopes; 
    $nisotopes1 = $nisotopes+1;

    # get number of ensemble elements
    print "Getting nelements\n";
    my $version_string = evcode_version_string();
   
    $nensemble = ($version_string=~/GCE population ensemble is enabled with N=(\d+)/)[0];

    if(!defined $nensemble)
    {
	print "Failed to get nensemble from evolution code, trying headers\n";
	$nensemble = get_from_header('NUCSYN_ENSEMBLE_N',
				     'nucsyn/nucsyn_macros.h',
				     '/users/izzard/progs/stars/binary_c')
	    || confess ("Cannot determine NUCSYN_ENSEMBLE_N from $macrosfile or --version");
    }

    # get number of sources
    print "Getting nsources\n";
    $nsources= ($version_string=~/NUCSYN_SOURCE_NUM=(\d+)/)[0];

    if(!defined $nsources)
    {
	print "Failed to get nsources from evolution code, trying headers\n";
	$nsources = get_from_header('NUCSYN_SOURCE_NUM',
				    'nucsyn/nucsyn_macros.h',
				    '/users/izzard/progs/stars/binary_c')
	    || confess("Cannot determine NUCSYN_SOURCE_NUM from $macrosfile");
    }

    print "N : ens $nensemble ; iso $nisotopes ; sources $nsources\n";

    $meantimes=0; # if 1 then output at time bin midpoints
    # otherwise set the time to the end of the time bin

    # set this to 1 if the stellar code outputs at exact intervals
    $exacttimes=1;
        
    # grid resolution
    my $sampling_factor=1.0/4.0; # over-resolution (Nyquist/Shannon)

    # grid type :
    # 'bastard' is the mixed grid, with a mass dependent binary fraction
    # otherwise just select single or binary stars
    my $distribution_types = ' ';# 'bastard';

    my %resolution=(
	m=>$testing ? 10 : 1000, # ignored if automatically set
	m1=>$testing ? 10 : 100, # ignored if automatically set
	m2=>$testing ? 10 : 100,
	per=>$testing ? 10 : 100,
	);

    my $period_distribution='bastard'; # 'DM91' or 'bastard'

    # M1 and M2
    my $mmin=0.1;
    my $mmax=80.0;
    my $m2min=0.1;
    my $m1hbb=3.0; # HBB above this mass (for period distribution)

    # period distribution D+M91 options
    my $logpermin=-2.0;
    my $logpermax=12.0;
    my $gauss_logmean=4.8;
    my $gauss_sigma=2.3;

    my $nvar=0;

    if($distribution_types eq 'bastard')
    {
	# new bastard distribution of single and binary stars
	distribution_functions::bastard_distribution({
	    mmin=>$mmin,
	    mmax=>$mmax,

	    # use time-adaptive mass resolution for M1
	    time_adaptive=>{
		mass_grid_log10_time=>0,
		mass_grid_step=>$dt,
		mass_grid_log10_step=>$dt, # ignored
		extra_flash_resolution=>0,
		mass_grid_nlow_mass_stars=>10,
		debugging_output_directory=>undef,
		max_delta_m=>2.0
	    },	    

		    m2min=>$m2min,
		    nm2=>$resolution{m2},
		    nper=>$resolution{per},
		    necc=>undef,
		    qmin=>0.0,
		    qmax=>1.0,
		    useecc=>undef	    
						     });
    }
    else
    {
	# a population of either single or binary stars

	if(0)
	{
	    # NOT NORMAL:
	    # enforce a mixed population?
	    # mixture of single and binary stars
	    $binary_grid::grid_options{flexigrid}{'grid variable '.$nvar++}=
	    {
		'name'=>'duplicity',
		'longname'=>'Duplicity',
		'range'=>[0,1], 
		'resolution'=>1,
		'spacingfunc'=>'number(1.0)',
		'precode'=>'$binary_grid::grid_options{binary}=$duplicity;binary_grid::flexigrid_grid_resolution_shift();',
		'gridtype'=>'edge',
		'noprobdist'=>1,
	    };
	}
	else
	{
	    # only single stars (if not defined)
	    $binary_grid::grid_options{binary}//=0;
	}	
	
	# Mass 1
	if($testing)
	{
	    # log mass spacing
	    $binary_grid::grid_options{flexigrid}{'grid variable '.$nvar++}=
	    {
		'name'=> 'lnm1', 
		'longname'=>'Primary mass', 
		'range'=>["log($mmin)","log($mmax)"],
		'resolution'=> "$resolution{m1}", # ignore single stars in approximate resolution calculation
		'spacingfunc'=>"const(log($mmin),log($mmax),\$binary_grid::grid_options{binary} ? $resolution{m1} : $resolution{m})",
		'precode'=>'$m1=exp($lnm1);  $eccentricity=0.0; my $binary_fraction=main::binary_fraction($m1);  $binary_grid::grid_options{weight}= $binary_grid::grid_options{binary}==0 ? 1.0-$binary_fraction : $binary_fraction;   ',
		#'probdist'=>"Kroupa2001(\$m1)*\$m1",
		'probdist'=>"ktg93(\$m1)*\$m1",

		'dphasevol'=>'$dlnm1',
	    };
	}
	else
	{
	    # fixed-dt mass spacing
	    $binary_grid::grid_options{flexigrid}{'grid variable '.$nvar++}=
	    {
		'name'=> 'lnm1', 
		'longname'=>'Primary mass', 
		'range'=>["log($mmin)","log($mmax)"],

		# const_dt spacing function options
		'preloopcode'=>"
my \$const_dt_opts=\{
    max_evolution_time=>\$binary_grid::bse_options{max_evolution_time},
    stellar_lifetime_table_nm=>200,
    nthreads=>1,
    thread_sleep=>1,
    mmin=>$mmin,
    mmax=>$mmax,
    time_adaptive_mass_grid_log10_time=>0,
    time_adaptive_mass_grid_step=>".($dt*$sampling_factor).", # over-resolve 
    extra_flash_resolution=>1,
    time_adaptive_mass_grid_nlow_mass_stars=>10,
    debugging_output_directory=>'/tmp',
    max_delta_m=>1.0,
    savegrid=>1,
    vb=>0,
\};
spacing_functions::const_dt(\$const_dt_opts,'reset');
",
		# use const_dt function
		'spacingfunc'=>"const_dt(\$const_dt_opts,'next')",

		# and its resolution
		'resolution'=> "spacing_functions::const_dt(\$const_dt_opts,'resolution');",

		#'precode'=>'$m1=exp($lnm1);  $eccentricity=0.0; my $binary_fraction=main::binary_fraction($m1);  $binary_grid::grid_options{weight}= $binary_grid::grid_options{binary}==0 ? 1.0-$binary_fraction : $binary_fraction;   ',

		'precode'=>'$m1=exp($lnm1);  $eccentricity=0.0; my $binary_fraction=$binary_grid::grid_options{binary}==0;   ',

		#'probdist'=>"Kroupa2001(\$m1)*\$m1",
		'probdist'=>"ktg93(\$m1)*\$m1",

		'dphasevol'=>'$dlnm1',
	    };
	}
	
	# Binary stars: Mass 2 and Separation/Period
	$binary_grid::grid_options{flexigrid}{'grid variable '.$nvar++}=
	{
	    'condition'=>'$binary_grid::grid_options{binary}==1',
	    'name'=>'q',
	    'longname'=>'Mass ratio',
	    'range'=>[$m2min.'/$m1',1.0],
	    'resolution'=>$resolution{m2},
	    'spacingfunc'=>'const('.$m2min.'/$m1,1.0,'.$resolution{m2}.')',
	    'probdist'=>"flatsections\(\$q,\[
\{min=>$m2min/\$m1,max=>0.8,height=>1.0\},
\{min=>0.8,max=>1.0,height=>1.0\},
\]\)",
		#postcode=>'exit;',
		precode=>'$m2=$q*$m1;',
		dphasevol=>'$dq',
	};
	
	if($period_distribution eq 'DM91')
	{
	    # old code
	    $binary_grid::grid_options{flexigrid}{'grid variable '.$nvar++}=
	    {  
		'name'=>'logper',
		'longname'=>'log(Orbital_Period)',
		'range'=>[$logpermin,$logpermax],
		'resolution',$resolution{per},
		'spacingfunc',"const($logpermin,$logpermax,$resolution{per})",
		'precode'=>"\$per=10.0**\$logper;\$sep=calc_sep_from_period(\$m1,\$m2,\$per)if(defined(\$m1)&&defined(\$m2));",
		'probdist'=>"gaussian(\$logper,$gauss_logmean,$gauss_sigma,$logpermin,$logpermax)",
		'dphasevol'=>'$dln10per'
	    }
	}
	elsif($period_distribution eq 'bastard')
	{
	    # period distribution : rob's bastard
	    $binary_grid::grid_options{flexigrid}{'grid variable '.$nvar++}=
	    {
		'name'=>'log10per',
		'longname'=>'log10(Orbital_Period)',
		'range'=>['-1.0','10.0'],
		'resolution'=>int($resolution{per}),
	
		# fixed resolution
		'spacingfunc',"const(-1.0,10.0,$resolution{per})",

		precode=>"my \$per=10.0**\$log10per;my\$sep=calc_sep_from_period(\$m1,\$m2,\$per);",
		probdist=>'Izzard2012_period_distribution($per,$m1)',
		dphasevol=>'$dlog10per'
	    }
	}
	else
	{
	    print "Unknown period distribution $period_distribution\n";
	    exit;
	}
    }

    # maximum stack size before we pause to allow stars to finish
    $binary_grid::grid_options{flexigrid}{maxstack}=10;

    printf "Flexigrid modulo = %d, offset = %d\n",
    $binary_grid::grid_options{modulo},
    $binary_grid::grid_options{offset};

    binary_grid::parse_args();
    secondary_variables();
    setup_binary_grid();
}


sub cumulate_yields
{
    my $source=$_[0];
    if($sources)
    {
	printf "Cumulating source %d/%d\n",$source,$nsources-1;
    }
    else
    {
	print "Cumulating...\n";
    }
    
    my @sum;
    for(my $tbin=0;$tbin<$nbins;$tbin++)
    {
	my $t2=$tbin-1;
	for(my $j=0;$j<$nisotopes;$j++)
	{
	    $$results{yield}{$source}{$tbin}{$j}+=$$results{yield}{$source}{$t2}{$j};
	}
    }
}

sub secondary_variables
{
    # from the defaults and/or passed in variables,
    # calculate useful things

    my $executable = binary_grid::evcode_program();
    $version_string = `$executable --version`;

    # use colour?
    if($binary_grid::grid_options{colour})
    {
        # use ANSI color for output to xterms/rxvt etc.
        use Term::ANSIColor;
        # set to white on black
        print color("white on_black");
    }
    else
    {
        # dummy function to replace the ANSI color() function
        eval("sub\ color\{ \}");
    }

    if($dt == 0.0)
    {
	print "dt=0 : you must specify dt on the command line!\n";
	exit;
    }

    # used lots, calculate once (* is faster than /, or at least used to be ...)
    $idt=1.0/$dt;

    # calculate number of bins (+1, so we can use < instead of <= in fors)
    $nbins=int($binary_grid::bse_options{max_evolution_time}/$dt)+1;

    # check if differential is correct
    if($version_string=~/NUCSYN_LOG_BINARY_DX_YIELD is defined/)
    {
	if(!$differential)
	{
	    $differential=1;
	    print "Overriding differential to $differential based on binary_c --version\n";
	}
    }
    else
    {
	if($differential)
	{
	    $differential=0;
	    print "Overriding differential to $differential based on binary_c --version\n";
	}
    }

    # check if ensemble is correct
    if($version_string=~/GCE population ensemble is disabled/)
    {
	if($ensemble)
	{
	    $ensemble=0;
	    print "Overriding ensemble to $ensemble based on binary_c --version\n";
	}
    }
    else
    {
	if(!$ensemble)
	{
	    $ensemble=1;
	    print "Overriding ensemble to $ensemble based on binary_c --version\n";
	}
    }

    # check if sources is correct
    if($version_string=~/NUCSYN_ID_SOURCES is defined/)
    {
	if(!$sources)
	{
	    $sources=1;
	    print "Overriding sources to $sources based on binary_c --version\n";	}
    }
    else
    {
	if($sources)
	{
	    $sources=0;
	    print "Overriding sources to $sources based on binary_c --version\n";
	}
    }

    # get the source and ensemble labels
    @source_label = binary_grid::source_list() if($sources); 
    @ensemble_label = binary_grid::ensemble_list()if($ensemble);

    #if($binary_grid::bse_options{yields_dt} eq '')
    {
	# yields_dt controls how often yields are output from binary_c
	# set it : small for greater time accuracy
	#        : large for higher speed (fewer data lines)
	# really you want a happy medium, e.g. 0.1*dt
	$binary_grid::bse_options{yields_dt} = $exacttimes!=1 ? 0.1 * $dt : $dt;
    }

    # use time-adaptive mass grid to get yields smooth as f(time)
    $binary_grid::grid_options{time_adaptive_mass_grid_step}=$dt;

    # unique string for this model grid
    $gridstring //= 
	'dt='.$dt.
	'_Z='.$binary_grid::bse_options{metallicity}.
	'_binary='.$binary_grid::grid_options{binary}.
	'_alpha_ce='.$binary_grid::bse_options{alpha_ce}.
	'_lambda_ce='.$binary_grid::bse_options{lambda_ce}.
	'_lambda_ionisation='.$binary_grid::bse_options{lambda_ionisation}.
	'_epsnov='.$binary_grid::bse_options{epsnov};

    # condor and output directory : this stores the temp files and the final 
    # output : you should make sure it is read/writeable on all
    # condor notes (e.g. via an NFS link)
    $outdir .= '/'.$gridstring;
    $binary_grid::grid_options{condor_dir}=$outdir;

    print "Set condor dir $binary_grid::grid_options{condor_dir}\n";
    mkdirhier($outdir);
    mkdirhier($binary_grid::grid_options{condor_dir});

    # make output filename IF not already set
    $yieldoutfile=output_filename('yields_vs_time') if($yieldoutfile eq '');
 

    print "Set output dir $outdir\n";
}

sub pad_datahash
{
    # pad datahash $h so it has zeros where there's no data
    my $h=$_[0];
    my $d=$_[1];
    my @k=nsort keys %$h;
    my $xmax=$k[$#k]; # max value
    my $x=$k[0]; # start (min) value
    while($x<$xmax)
    {
	$$h{$x}//=0.0;
	$x=sprintf'%g',$x+$d;
    }
}


sub fix_aifa_environment
{
    if(`hostname`=~/aibn/)
    {
	# remove MESA references
	foreach ('PATH','MANPATH')
	{
	    $ENV{$_} = join(':',grep {!/mesa/} (split(/\:/,$ENV{$_})));
	}

	map
	{
	    print "CHECK $_\n";
	    if($ENV{$_}=~s!/export/aibn(\d+)_(\d+)/!/vol/aibn$1/aibn$1\_$2/!g)
	    {
		print "CONV $_=$ENV{$_}\n";
	    }

	    # remove MESA references
	    if(/MESA/)
	    {
		$ENV{$_}=undef;
	    }	    
	}%ENV;
    }
}

sub process_yield_line
{
    my $h=$_[0];
    my $source=$_[1]//0;
    my $line=$_[2];
    
    # expand compressed output
    $line=~s/(\d+)x0/('0 ')x$1/eog;

    # split line into array
    my @ry=split(' ',$line);

    # shift off the time
    my $t=shift @ry;

    # set isotope number if not already defined
    $nisotopes //= $#ry; 

    # time bin = $t*$idt-1
    my $timebin=$t*$idt-1;

    foreach my $j (0..$nisotopes)
    {
	my $was = $$h{yield}{$source}{$timebin}{$j};
	$$h{yield}{$source}{$timebin}{$j}+=$ry[$j];
	#printf "Add yield %g to {yield}{$source}{$timebin}{$j} was %g now %g\n",
	#$ry[$j],$was,
	#$$h{yield}{$source}{$timebin}{$j}+=$ry[$j] if($j==2);
    }
}

sub process_ensemble_line
{
    # process a line of ensemble data
    my $h=$_[0]; # data hash (add to this)
    my $line = $_[1]; # data line (take from this)

    # expand compressed output
    $line=~s/(\d+)x0/('0 ')x$1/eog;

    # might be quicker to do it in the array? not sure, splicing is
    # expensive...

    my @ry=split(' ',$line);

    $nensemble//=$#ry+1;
 
    # get the time
    my $t=shift @ry;

    # time bin = $t*$idt-1 
    my $timebin=$t*$idt-1;

    # save data
    foreach my $j (0..$nensemble)
    {
	$$h{ensemble}{$timebin}{$j} += $ry[$j] if($ry[$j]);
    }
}


sub total_mass_ejected
{
    # calculate total ejected mass (for each source as well, if appropriate)
    # and return the total
    my $m=0.0;
    my $nbins1=$nbins+1;

    if($sources)
    {
	for(my $source=0;$source<$nsources;$source++)
	{
	    for(my $tbin=0;$tbin<$nbins1;$tbin++)
	    {
		for(my $i=0;$i<$nisotopes1;$i++)
		{
		    $m += $$results{yield}{$source}{$tbin}{$i};
		}
	    }
	}
    }
    else
    {
	for(my $tbin=0;$tbin<$nbins1;$tbin++)
	{
	    for(my $i=0;$i<$nisotopes1;$i++)
	    {
		$m += $$results{yield}{0}{$tbin}{$i};
	    }
	}	
    }
    return $m;
}

sub get_colranges
{
    my $f=$_[0];
    my @colranges;
    my @datamax;
    my @datamin;

    my @x=`check_col_ranges.pl $f`;

    chomp @x;
    @colranges=split(/\s+/o,$x[0]);
    shift @colranges; # first is time
    
    @datamax=split(/\s+/o,$x[1]);
    shift @datamax; # MAX
    shift @datamax; # :
    shift @datamax; # the time
    #print "Datamax @datamax\n";

    @datamin=split(/\s+/o,$x[2]);
    shift @datamin; # MIN
    shift @datamin; # :
    shift @datamin; # the time
    #print "Datamin @datamin\n";

    return (\@colranges,\@datamax,\@datamin);
}


sub output_gp
{
    my @colranges;
    my @datamin;
    my @datamax;
    my @source_colranges;
    my $ymin=1e-10;

    if($sources==0)
    {
	my @x;
	@x=get_colranges($yieldoutfile);
	@colranges=@{$x[0]};
	@datamax=@{$x[1]};
	@datamin=@{$x[2]};
    }
    else
    {
	for(my $j=0;$j<$nsources;$j++)
	{  
	    my $f=$yieldoutfile;
	    $f=~s/\.dat/\.source$j\.dat/;
	    $source_colranges[$j]=[]; # create anon array
	    my $x=$source_colranges[$j];
	    #@x=get_colranges($f);
	}
    }
    # set gnuplot output files
    my $gpfile="$outdir/cyields.plt"; # gnuplot plot file
    my $psfile="$outdir/cyields.ps"; # gnuplot postscript file
 
    my @gp;
    my $fontsize=18;
    open(GP,'>',$gpfile)||confess("cannot open gnuplot file");
    print GP "set terminal postscript solid enhanced colour \"Times-Roman\" $fontsize linewidth 1\n";
    
    print GP "set output \"$psfile\"\nset zero 1e-50\n";
    print GP "set xlabel \"Time\"\n";
    print GP "set key below\n";
    if($differential==1)
    {
	print GP "set logscale x\n";
	print GP "set logscale y\n";
	print GP "set yrange [$ymin:*]\n";
    }
    else
    {
	print GP "set yrange[$ymin:*]\n";
    }
    my $z=$binary_grid::bse_options{metallicity}; 
    my $bin=$binary_grid::grid_options{binary}==0?'Single':'Binary' ;
    my @linetypes=(1..21);
    
    for(my $i=0;$i<=$nisotopes;$i++)
    {
	my $comma='plot ';
	if($sources)
	{
	    for(my $j=0;$j<$nsources;$j++)
	    {
		my $lt=$linetypes[$j]//1;
		my $f=$yieldoutfile;
		$f=~s/\.dat/\.source$j\.dat/;
		my $x=$source_colranges[$j];
		@colranges=@$x;

		if(($colranges[$i]>$ymin)&&
		   ($datamax[$i]>$ymin))
		{
		    # save output prior to gnuplotting
		    $gp[$i]=$gp[$i]." $comma \"$f\" using 1:".($i+2)." with lp lt $lt pt $lt title \"$source_label[$j]\" ";
		    $comma=', ';
		}
	    }
	}
	else
	{
	    print "Isotope $i = $isotopes[$i] colrange = $colranges[$i], data range $datamin[$i] to $datamax[$i]\n"; 
	    $gp[$i] = (($colranges[$i]==0.0)||($datamax[$i]<1e-20)) ?
		"plot (100),(100) title \"No Yield\"\n":
		# intelligent y range e.g. for log scale
		"set yrange[".($datamin[$i]==0.0 ? ($differential ? $datamax[$i]*1e-5 : 0.0) : '*').":*]\n".$gp[$i]."plot \"$yieldoutfile\" using 1:".($i+2)." with lp notitle ";
	    
	}
	
	print GP "set nologscale x\nset title \"";
	print GP "Cumulative "if(!$differential);
	my $isotope=$isotopes[$i];
	my $superfontsize=int($fontsize*0.8);
	$isotope=~s/(\D+)(\d+)/^\{\/=$superfontsize $2\}$1/;
	print GP "$isotope yield ";
	print GP 'rate 'if($differential);
	print GP "{/Italic Z}=$z; $bin stars\"\nset xrange[0:*]\n",$gp[$i]," \# $isotopes[$i] $colranges[$i]\nset xrange[",0.5*$dt,":*]\nset logscale x\n",$gp[$i],"\n";
    }
    
    close GP;

    # run gnuplot
    `gnuplot $gpfile`;
    
    # convert colours
    my $ps=slurp($psfile);
    $ps=~s/1 1 0/0.5 0.5 0/o;
    $ps=~s/0 0 1/0 0 0.7/o;
    dumpfile($psfile,$ps);

    print "See $psfile\n";

}


sub output_ensemble
{
    # process, and output information about, the stellar ensemble
    # data we have saved
    my $t;
    
    print "Process ensemble: $nensemble observations\n" if ($binary_grid::grid_options{vb});

    my $fac=1.0/$$results{mass_into_stars};

    # output the large file with zillions of columns
    my $file=output_filename('ensemble');
    open(ENSEMBLE,'>',$file)||confess("cannot open ensemble output file $file");
    
    print ENSEMBLE "# Stellar ensemble normalized with IMF, divided by mass input to stars = $$results{mass_into_stars}\n";
    for(my $o=0;$o<$nensemble;$o++)
    {
	printf ENSEMBLE "# Column %d is %s\n",$o+2,$ensemble_label[$o];
    }

    
    for(my $tbin=0;$tbin<$nbins;$tbin++)
    {
	my @output;
	push(@output,$dt*($meantimes ? ($tbin+0.5) : ($tbin+1)));
	for(my $o=0;$o<$nensemble;$o++)
	{
	    push(@output,$fac*$$results{ensemble}{$tbin}{$o});
	}
	print ENSEMBLE join(' ',@output,"\n");
    }
    close ENSEMBLE;

}

sub output_filename
{
    # standard output filename
    return $outdir.'/'.$_[0].'.z='.$binary_grid::bse_options{metallicity}.'.binary='.$binary_grid::grid_options{binary}.'.dat';
}


sub binary_fraction
{ 
    my $m1=$_[0];
    return $binary_grid::grid_options{binary} ; # force single or binary stars only
    return distribution_functions::raghavan2010_binary_fraction($m1);
}


sub output
{
    my $t;
    my $bin;
    my $file=$yieldoutfile;
    my $source=$_[0];

    cumulate_yields($source) if(!$differential);
    
    # convert the yieldoutfile to account for the fact that 
    # we have multiple sources of stuff
    $file=~s/\.dat/\.source$source\.dat/ if($sources);

    # new output file
    open(FP,'>',$file)||
	confess("cannot open output file $file (source=$source)");
    
    # make human-readable symlink, or warn if we cannot (not a fatal error)
    my $symlink = $file;  # full filename
    $symlink =~s/source(\d+)/source$source_label[$source]/;

    # get dir and relative filename
    my $dirname = dirname($symlink);
    my $relfile = basename($file);
    my $rellink = basename($symlink); 

    # save current dir
    my $cwd = getcwd;

    if(chdir $dirname && !-l $symlink && !symlink $relfile,$rellink)
    {
	print "Warning: Unable to create symlink ($relfile > $rellink in $dirname) for this source (are symlinks available?)\n";	
    }

    # go back to cwd
    chdir $cwd;

    # output version information
    {
	my @v=split(/\n/o,$version_string);
	map
	{  
	    if(/^Isotope (\d+)/ && ($1>=$nisotopes))
	    {
		$_ .= " (not output because nisotopes=$nisotopes < number of isotopes in binary_c)";
	    }
	    print FP "# $_\n";
	}@v;
    }    

    # dump options for 
    map
    {
	print FP "# grid_option $_ = $binary_grid::grid_options{$_}\n";
    }sort keys %binary_grid::grid_options;
    map
    {
	print FP "# bse_option $_ = $binary_grid::bse_options{$_}\n";
    }sort keys %binary_grid::bse_options;
    
    print FP '# yields (mass ejected, normalized with IMF, divided by mass input to stars = ',$$results{mass_into_stars};    
    print FP " per unit time"if($differential);    
    print FP ") vs time\n",join(' ','# time',@isotopes,"\n");
  
    my $denominator=denominator();
    print "Source $source ($source_label[$source]) : Denominator = $denominator ($$results{mass_into_stars}) dt=$dt\n";

    for(my $tbin=0;$tbin<=$nbins;$tbin++)
    {
	#print "Output tbin $tbin\n";
        # calculated just once
	my $sum=0.0; # sum of yields
        my @dyield; # yield time derivative

        if($differential)
        { 
	    my $i2=1.0/($denominator*$dt); # inverse denominator

	    # yields are already stored as differential yields
	    # but we want per unit mass per unit time
	    for(my $i=0;$i<$nisotopes1;$i++)
	    {
		#printf "Output results{yield}{$source}{$tbin}{$i} = %g * $i2\n",$$results{yield}{$source}{$tbin}{$i};
		$dyield[$i]=$$results{yield}{$source}{$tbin}{$i}*$i2;
	    }
	}
        else
        {
            # set sum to something >0 so output is forced when
            # we want cumulative yields
            $sum=1.0;
	}
	
        # now only output yields if the sum of the yields is > 0
        # i.e. if anything happens, otherwise nothing happens and we do not care!
        # so don't output lots of lines of zeros!
        #if($sum>0.0) # (NB comment out this line for debugging) 
        {
	    my $idenom=1.0/$denominator;
	    my $tt = $meantimes ? ($tbin+0.5)*$dt # midpoint of the bin
		: ($tbin+1)*$dt; # end of the bin
	   
	    print FP "$tt ";

	    for(my $i=0;$i<$nisotopes1;$i++)
            {
                #print "T=$t $isotopes[$i] $yield[$source}{$t}{$i]\n";
                # normalize to mass put into stars
                if($differential)
                {
		    # already normalized (see above)
		    print FP $dyield[$i]==0 ? '0 ' : (sprintf '%5.5e ',$dyield[$i])
			    if($t<=$binary_grid::bse_options{max_evolution_time});
                }
                else
                {
                    print FP (sprintf  '%5.5e ',$$results{yield}{$source}{$tbin}{$i}*$idenom);
                }
	    }
            print FP "\n";
        }
	
    }
    print FP "\n";
    close FP;
}

sub denominator
{
    my $denominator;
    if($norm_output eq 'none')
    {
	$denominator=1.0;
	#print "Denominator = 1.0 (i.e. none)\n";
    }
    elsif($norm_output eq 'total_mass_ejected')
    {
	$denominator=1e-40+$$results{total_mass_ejected};
	#print "Denominator = mass ejected = $$results{total_mass_ejected}\n";
    }
    else
    {
	$denominator=1e-40+$$results{mass_into_stars};
	#print "Denominator = mass into stars = $results{mass_into_stars}\n";
    }
    return $denominator;
}


sub condor_free
{
    # return number of free condor slots
    my $cmd='condor_status|grep -A2 Claimed\ Unclaimed|tail -1|gawk "{print \$5}"';
    my $n=`$cmd`;
    chomp $n;
    return $n;
}
