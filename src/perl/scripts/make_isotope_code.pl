#!/usr/bin/env perl
use strict;

open(my $fp,'<','isotope_list')||die;
my @isotopes = split(/,/,<$fp>);

my $prev;

print "// \#define ALL_ISOTOPES\n\n\n";

foreach my $isotope (@isotopes)
{
    my $macro;
    
    # special cases
    if($isotope eq 'n')
    {
        $macro = '0';
    }
    elsif($isotope eq 'n1')
    {
        $macro = '(Xn)';
    }
    elsif($isotope eq 'e')
    {
        $macro = '(Xn+1)';
    }
    else
    {
        $macro = "(X$prev+1)";
    }

    if($isotope eq 'P30')
    {
        print "\n\#ifdef ALL_ISOTOPES\n\n";
    }

    print "\#define X$isotope $macro\n";
    $prev = $isotope;
}

print "\#define ISOTOPE_ARRAY_SIZE (X$prev+1)

\#else

\#define Xelse (XSi30+1)

";

my $out=0;
foreach my $isotope (@isotopes)
{
    if($out)
    {
        print "\#define X$isotope Xelse\n";
    }
    else
    {
        $out = $isotope eq 'Si30';
    }
}

print "\n\#define ISOTOPE_ARRAY_SIZE (Xelse+1)\n\n\#endif\n\n\n";
