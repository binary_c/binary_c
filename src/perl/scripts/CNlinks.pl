#!/usr/bin/env perl
use strict;

#
# Script to make symlinks of the CN model sets
#

my %links = (

    'CN-Z0.008,N_RES_,TDend5e3,TDstart1e4,single' => 'S1',
    'CN-Z0.0001,N_RES_,TDend5e3,TDstart1e4,single' => 'S2',
    'CN-Z0.001,N_RES_,TDend5e3,TDstart1e4,single' => 'S3',
    'CN-Z0.02,N_RES_,TDend5e3,TDstart1e4,single' => 'S4',
    'CN-Z0.008,N_RES_,TDend8e3,TDstart13000,single' => 'S5',

    'CN-CRAP0,RLOF3,WRLOF1,alpha_ce0.2,Z0.008,N_RES_,padist=Izzard2012qcrit_MS1.6,rlof_gamma-2,TDend5e3,TDstart1e4,binary' => 'B1',
    'CN-CRAP0,RLOF3,WRLOF1,alpha_ce0.2,Z0.0001,N_RES_,padist=Izzard2012qcrit_MS1.6,rlof_gamma-2,TDend5e3,TDstart1e4,binary'=>'B2',
    'CN-CRAP0,RLOF3,WRLOF1,alpha_ce0.2,Z0.001,N_RES_,padist=Izzard2012qcrit_MS1.6,rlof_gamma-2,TDend5e3,TDstart1e4,binary'=>'B3',
    'CN-CRAP0,RLOF3,WRLOF1,alpha_ce0.2,Z0.02,N_RES_,padist=Izzard2012qcrit_MS1.6,rlof_gamma-2,TDend5e3,TDstart1e4,binary'=>'B4',

    # vary qcrit
    'CN-CRAP0,RLOF3,WRLOF1,alpha_ce0.2,Z0.008,N_RES_,padist=Izzard2012qcrit_MS1.8,rlof_gamma-2,TDend5e3,TDstart1e4,binary'=>'B5',
    'CN-CRAP0,RLOF3,WRLOF1,alpha_ce0.2,Z0.008,N_RES_,padist=Izzard2012qcrit_MS3,rlof_gamma-2,TDend5e3,TDstart1e4,binary'=>'B6',

    # vary alpha_ce
    'CN-CRAP0,RLOF3,WRLOF1,alpha_ce0.5,Z0.008,N_RES_,padist=Izzard2012qcrit_MS1.6,rlof_gamma-2,TDend5e3,TDstart1e4,binary'=>'B7',
    'CN-CRAP0,RLOF3,WRLOF1,alpha_ce1,Z0.008,N_RES_,padist=Izzard2012qcrit_MS1.6,rlof_gamma-2,TDend5e3,TDstart1e4,binary'=>'B8',

    # vary WRLOF
    'CN-CRAP0,RLOF3,WRLOF0,alpha_ce0.2,Z0.008,N_RES_,padist=Izzard2012qcrit_MS1.6,rlof_gamma-2,TDend5e3,TDstart1e4,binary'=>'B9',
    'CN-CRAP0,RLOF3,WRLOF2,alpha_ce0.2,Z0.008,N_RES_,padist=Izzard2012qcrit_MS1.6,rlof_gamma-2,TDend5e3,TDstart1e4,binary'=>'B10',

    # vary RLOF
    'CN-CRAP0,RLOF0,WRLOF1,alpha_ce0.2,Z0.008,N_RES_,padist=Izzard2012qcrit_MS1.6,rlof_gamma-2,TDend5e3,TDstart1e4,binary'=>'B11',
    'CN-CRAP0,RLOF1,WRLOF1,alpha_ce0.2,Z0.008,N_RES_,padist=Izzard2012qcrit_MS1.6,rlof_gamma-2,TDend5e3,TDstart1e4,binary'=>'B12',

    # vary CRAP
    'CN-CRAP1e3,RLOF3,WRLOF1,alpha_ce0.2,Z0.008,N_RES_,padist=Izzard2012qcrit_MS1.6,rlof_gamma-2,TDend5e3,TDstart1e4,binary'=>'B13',
    'CN-CRAP1e4,RLOF3,WRLOF1,alpha_ce0.2,Z0.008,N_RES_,padist=Izzard2012qcrit_MS1.6,rlof_gamma-2,TDend5e3,TDstart1e4,binary'=>'B14',

    # vary gamma
    'CN-CRAP0,RLOF3,WRLOF1,alpha_ce0.2,Z0.008,N_RES_,padist=Izzard2012qcrit_MS1.6,rlof_gamma-1,TDend5e3,TDstart1e4,binary'=>'B15',
    'CN-CRAP0,RLOF3,WRLOF1,alpha_ce0.2,Z0.008,N_RES_,padist=Izzard2012qcrit_MS1.6,rlof_gamma0,TDend5e3,TDstart1e4,binary'=>'B16',
    'CN-CRAP0,RLOF3,WRLOF1,alpha_ce0.2,Z0.008,N_RES_,padist=Izzard2012qcrit_MS1.6,rlof_gamma1,TDend5e3,TDstart1e4,binary'=>'B17',
    'CN-CRAP0,RLOF3,WRLOF1,alpha_ce0.2,Z0.008,N_RES_,padist=Izzard2012qcrit_MS1.6,rlof_gamma2,TDend5e3,TDstart1e4,binary'=>'B18',

    # Opik period distribution
    'CN-CRAP0,RLOF3,WRLOF1,alpha_ce0.2,Z0.008,N_RES_,padist=Opikqcrit_MS1.6,rlof_gamma-2,TDend5e3,TDstart1e4,binary'=>'B19',
    
    # vary ages
    'CN-CRAP0,RLOF3,WRLOF1,alpha_ce0.2,Z0.008,N_RES_,padist=Izzard2012qcrit_MS1.6,rlof_gamma-2,TDend8e3,TDstart13000,binary'=>'B20',
    
    # limit logg
    'CN-Z0.008,N1e4,TDend5e3,thick_disc_logg_max3,thick_disc_logg_min2,TDstart1e4,single' => 'S6',
    'CN-CRAP0,RLOF3,WRLOF1,alpha_ce0.2,Z0.008,N100,padist=Izzard2012qcrit_MS1.6,rlof_gamma-2,TDend5e3,logg_max3,logg_min2,TDstart1e4,binary'=>'B21',
   
    
    );


# available resolutions, largest first
my @resolutions = ('1e4','1e3','171','100','40','10');

# remove old links
foreach my $x (keys %links)
{
    my $link = $links{$x};
    unlink $link;
}

foreach my $x (keys %links)
{
    my $link = $links{$x};
    my $ok=0;
    print "X $x LINK $link\n";
    # no link : try to make one
    OUTER: foreach my $resolution (@resolutions)
    {
        my $d = $x;
        $d=~s/_RES_/$resolution/;

        my @dirs = ($d);
        
        if($d!~s/(alpha_ce[^,]+)/$1,maximum_timestep0.1/)
        {
            $d=~s/(^CN)-/$1-maximum_timestep0.1,/;
        }
        push(@dirs,$d);
        
        foreach my $dir (@dirs)
        {
            print "Look for $dir\n";
            if(-d $dir)
            {
                print "Found $dir, link to $links{$x}\n";
                symlink $dir,$links{$x};
                $ok=1;
                last OUTER;
            }
        }
    }

    # check it exists
    if(!$ok || !-l $link)
    {
        print "Failed to make link $x to $links{$x}\n";
        #exit;
    }
}
    

print "All symlinks made ok\n";
