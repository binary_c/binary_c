#!/usr/bin/env perl
use strict;
use rob_misc qw/is_numeric MAX/;
use robqueue;

#
# Wrapper for CNstats to test population statistics
#

my $datadir = $ARGV[0] // '.';
my $cnstats = $ENV{HOME}.'/progs/stars/binary_c/src/perl/scripts/CNstats2.pl';
my %format = (
    'g'=>'% 31.5g',
    'gpc'=>'% 30.5g%%',
    's'=>'% 31s'
    );
my $hashline = "########################################################################################################################\n";
my %result :shared;
my %parameters :shared;
my $default_parameter_name = 'default';

#
# Call the process() function with the following options:
#
# title => 'string' : the title of this comparison (e.g. 'Single Stars')
#
# single_regexps => ['string1','string2'...] : a series of regular expressions that should match the
#                                              single-star directories. (ALL must match)
#
# binary_regexps => ['string1','string2'...] : as single_regexps for binary stars. 
#
# NOTE if NO regexps match (e.g. if the string is empty) then only one of the sets will be used
#      -> this is good for a purely single or binary population
#
# NOTE if ONE regexp matches, this will be used for ALL the other sets. E.g. if only
# one single-star set matches, but many binary sets match, then the single-star set will
# be matched to all the binary sets. 
#
# negregexps=>['string1','string2'...] : a list of negative regexps. If any of these match
#                                        either list, the directory will be ignored
#
# paramregexp=>'string' : a regular expression to extract the parameter value from the directory name
#
# param=>'string' : a string containing a (human-readable) name for the parameter
#
# test=>{0,1} : if 0 a directry is skipped, if 1 it is processed

# single stars only
process(
    title=>'Single Stars',
    single_regexps=>['CN-Z(\d+\.\d+).*single$'],
    negregexps=>['TD'],
    paramregexp=>'CN-Z(\d+\.\d+).*single$',
    param=>'Z',
    test=>0
    );

# binary stars only
process(
    title=>'Binary Stars',
    binary_regexps=>['CN-CRAP0,RLOF3,WRLOF1,alpha_ce0.2,Z(\d+\.\d+),N\d+,padist=Izzard2012qcrit_MS1.6,rlof_gamma-2,binary'],
    negregexps=>['TD'],
    paramregexp=>'Z(\d+\.\d+)',
    param=>'Z',
    test=>0
    );

# binary stars only
process(
    title=>'50:50 Population',
    single_regexps=>['^CN-Z(\d+\.\d+).*single$'],
    binary_regexps=>['^CN-CRAP0,RLOF3,WRLOF1,alpha_ce0.2,Z(\d+\.\d+),N\d+,padist=Izzard2012qcrit_MS1.6,rlof_gamma-2,binary'],
    negregexps=>['TD'],
    paramregexp=>'Z(\d+\.\d+)',
    param=>'Z',
    test=>0
    );

# alpha_ce
process(
    title=>'Common Envelope Alpha', 
    single_regexps=>['CN-Z0.004.*single$'],
    binary_regexps=>['CN-CRAP0,RLOF3,WRLOF1,alpha_ce(\d+(?:\.\d+)?),Z0.004,N\d+,padist=Izzard2012qcrit_MS1.6,rlof_gamma-2,binary'],
    negregexps=>['TD'],
    paramregexp=>'alpha_ce(\d+(?:\.\d+)?)',
    param=>'alpha_ce',
    test=>0
    );


# WRLOF
process(
    title=>'WRLOF',
    single_regexps=>['CN-Z0.004.*single$'],
    binary_regexps=>['CN-CRAP0,RLOF3,WRLOF(\d),alpha_ce0.2,Z0.004,N\d+,padist=Izzard2012qcrit_MS1.6,rlof_gamma-2,binary'],
    negregexps=>['TD'],
    paramregexp=>'WRLOF(\d)',
    param=>'WRLOF',
    test=>0,
    );

# RLOF interpolation method
process(
    title=>'RLOF method',
    single_regexps=>['CN-Z0.004.*single$'],
    binary_regexps=>['CN-CRAP0,RLOF(\d),WRLOF1,alpha_ce0.2,Z0.004,N\d+,padist=Izzard2012qcrit_MS1.6,rlof_gamma-2,binary'],
    negregexps=>['TD'],
    paramregexp=>'RLOF(\d)',
    param=>'RLOF',
    test=>0
    );    

# CRAP
process(
    title=>'Companion Reinforced Attrition Process',
    single_regexps=>['CN-Z0.004.*single$'],
    binary_regexps=>['CN-CRAP([^,]+),RLOF3,WRLOF1,alpha_ce0.2,Z0.004,N\d+,padist=Izzard2012qcrit_MS1.6,rlof_gamma-2,binary'],
    negregexps=>['TD'],
    paramregexp=>'CRAP([^,]+)',
    param=>'CRAP',
    test=>0
    );

# RLOF gamma
process(
    title=>'RLOF gamma',
    single_regexps=>['CN-Z0.004.*single$'],
    binary_regexps=>['CN-CRAP0,RLOF3,WRLOF1,alpha_ce0.2,Z0.004,N\d+,padist=Izzard2012qcrit_MS1.6,rlof_gamma(-?\d),binary'],
    paramregexp=>'rlof_gamma(-?\d)',
    param=>'rlof_gamma',
    negregexps=>['TD'],
    test=>0
    );

# qcrit on main sequence
process(
    title=>'qcrit MS',
    single_regexps=>['CN-Z0.004.*single$'],
    binary_regexps=>['CN-CRAP0,RLOF3,WRLOF1,alpha_ce0.2,Z0.004,N\d+,padist=Izzard2012qcrit_MS([^,]+),rlof_gamma-2,binary'],
    paramregexp=>'qcrit_MS([^,]+)',
    param=>'qcrit_MS',
    negregexps=>['TD'],
    test=>0
    );

# Period distribution
process(
    title=>'Period distribution',
    single_regexps=>['CN-Z0.004.*single$'],
    binary_regexps=>['CN-CRAP0,RLOF3,WRLOF1,alpha_ce0.2,Z0.004,N\d+,padist=([^q]+)qcrit_MS1.6,rlof_gamma-2,binary'],
    paramregexp=>'padist=([^q]+)',
    param=>'padist',
    negregexps=>['TD'],
    test=>0
    );

# Thick disc age distribution
process(
    title=>'Thick disc ages',
    single_regexps=>['CN-Z0.004.*(TDend[^,]+,TDstart[^,]+)?,single$'],
    binary_regexps=>['CN-CRAP0,RLOF3,WRLOF1,alpha_ce0.2,Z0.004,N\d+,padist=Izzard2012qcrit_MS1.6,rlof_gamma-2,(TDend[^,]+,TDstart[^,]+,)?binary'],
    paramregexp=>'(TDend[^,]+,TDstart[^,]+)',
    param=>'TDage',
    test=>0
    );

exit;

############################################################

sub process
{
    my %opts = @_;
    my $q = robqueue->new(
        subr=>\&runCN
        );

    printf STDERR "Process \"%s\"\n",$opts{title};
    my @singledirs = sets($opts{single_regexps},$opts{negregexps},$opts{paramregexp});
    my @binarydirs = sets($opts{binary_regexps},$opts{negregexps},$opts{paramregexp});

    # construct directory list
    my @dirs;
    for(my $i=0;$i<=MAX($#singledirs,$#binarydirs);$i++)
    {
        $dirs[$i] = ($#singledirs==0 ? $singledirs[0] : $singledirs[$i]).' '.
            ($#binarydirs==0 ? $binarydirs[0] : $binarydirs[$i]);
        $dirs[$i]=~s/^\s+//;
        $dirs[$i]=~s/\s+$//;
        print STDERR "Made $i : $dirs[$i]\n";
    }

    my @params = params(\@dirs,$opts{paramregexp});
    %result=();
    %parameters=();

    #print STDERR "singledirs @singledirs\n";
    #print STDERR "binarydirs @singledirs\n";
    print STDERR "dirs ",join('::',@dirs),"\n";
    print STDERR "params @params\n";

    for(my $i=0;$i<=$#dirs;$i++)
    {
        print STDERR "Run $dirs[$i] : $params[$i]\n";

        if(defined $opts{test} && $opts{test})
        {
            print STDERR "Test run only : skipping\n";
        }
        else
        {
            my $param = $params[$i];
            $result{$param} = &threads::shared::share( {} );
            $q->q([$dirs[$i],'',$param]);
        }
    }

    $q->end();

    sub runCN
    {
        my ($dirS,$dirB,$param) = @{$_[0]};
        
        # run stats script
        my $results = `$cnstats $dirS $dirB nognuplot 2>/dev/null`;

        # get extra results
        my $extra = ($results=~/(Extra-massive giants relative to the total number of extra massive giants.*)/s)[0];
        
        # strip results relative to extra massive giants
        $results=~s/(Extra-massive giants relative to the total number of extra massive giants.*)//s;
        
        while($results=~/\n\s*(\S+)\s*\:\s*(\S+(?: \%)?)\s*\:\s*(\S+(?: \%)?)/g)
        {
            # ignore $2 : the absolute number, just deal with the fraction of giants
            $1//=$default_parameter_name;
            $result{$param}->{$1}=$3;
            $parameters{$1}=1;
        }
        
        while($extra=~/\n\s*(\S+)\s*\:\s*(\S+(?: \%)?)\s*\:\s*(\S+(?: \%)?)/g)
        {
            # again ignore $2 : the absolute number, just deal with the fraction of massive giants
            $1//=$default_parameter_name;
            $result{$param}->{$1.'(OM)'}=$3;
            $parameters{$1.'(OM)'}=1;
        }
    }

    # sorted list of parameters
    my @parameters = sort keys %parameters;
    
    # output headers
    print $hashline;
    print "$opts{title}\n";
    printf $format{s},$opts{param};
    foreach my $param (@parameters)
    {
        printf $format{s},$param;
    }
    print "\n",$hashline;

    # output results
    foreach my $param (sort keys %result)
    { 
        my $format = is_numeric($param) ? $format{g} : $format{s};
        printf $format,$param;
                
        foreach my $k (@parameters)
        {
            my $v = $result{$param}->{$k};
            my $format = $v=~s/\%// ? $format{gpc} : $format{g};
            printf $format,$v;
        }
        print "\n";
    }
    print $hashline;

}

sub sets
{
    # return dirs matching a set of regexps
    my ($regexps,$negregexps,$paramregexp) = @_;
    return if(!$regexps);
    opendir(my $dh,$datadir)||die;
    my @dirs = readdir($dh);
    my @sets;
    foreach my $dir (@dirs)
    {
        next if($dir eq '.' || $dir eq '..');
        next if(!-d $dir) ;
        my $ok = 1;
        foreach my $regexp (@$regexps)
        {
            $ok = 0 if($dir!~/$regexp/);
        }
        foreach my $regexp (@$negregexps)
        {
            $ok = 0 if($dir=~/$regexp/);
        }
        push(@sets,$dir)if($ok);
    }
    return sort {
        ($a=~/$paramregexp/)[0] <=> ($b=~/$paramregexp/)[0]
    }@sets;
}

sub params
{
    # return list of parameters correspoding to chosen dirs
    my ($dirs,$regexp) = @_;
    my @params;
    foreach my $dir (@$dirs)
    {
        if($dir=~/$regexp/)
        {
            push(@params,$1);
        }
        else
        {
            push(@params,$default_parameter_name);
        }
    }
    return @params;
}

