#!/usr/bin/env perl
use rob_misc;
use strict;

# script to rename a variable in the binary_c source tree
#
# ARGV[0] contains the old name, ARGV[1] the new name
# ARGV[2] contains the options
#

my $oldname = shift @ARGV;
my $newname = shift @ARGV;
$newname = qq{"$newname"};
my $options = "@ARGV";

if(!defined $oldname || !defined $newname)
{
    print "Please give the old and new name as arguments\n";
    exit;
}

# default options
my %opts =
    (
     bounds => 1,
     vb=>1,
     dryrun=>0,
    );

# set options on command line
if($options =~ /nobounds/)
{
    $opts{bounds} = 0;
    print "Turned off bounds\n";
}
if($options =~/-N/ || $options=~/dry/)
{
    $opts{dryrun} = 1;
    print "Set dry run to on\n";
}

if($opts{vb})
{
    print STDERR "REGEXP old : $oldname\n";
    print STDERR "   replace : $newname\n";
    map
    {
        print STDERR " opt\{$_\} : $opts{$_}\n";
    }sort keys %opts;
}

# paths are either src/whatever, if we're in the binary_c root,
# or just local .c, .h and .def files
my $paths =
    -f './binary_c-config' ?
    'src/*.c src/*.h src/*/*.c src/*/*.h src/*.def src/*/*.def' :
    '*.c *.h *.def';

foreach my $file (`ls $paths`)
{
    chomp $file;
    my $x = slurp($file);

    if($opts{bounds})
    {
        if($x=~s/\b{wb}$oldname\b{wb}/$newname/eeg)
        {
            if(!$opts{dryrun})
            {
                print "Renamed in $file\n";
                dumpfile($file,$x);
            }
            else
            {
                print "Would have renamed in $file (but didn't)\n";
                print "############################################################\n";
                print $x;
                print "############################################################\n";
            }
        }
    }
    elsif($x=~s/$oldname/$newname/g)
    {
        if(!$opts{dryrun})
        {
            print "Renamed in $file\n";
            dumpfile($file,$x);
        }
        else
        {
            print "Would have renamed in $file (but didn't)\n";
            print "############################################################\n";
            print $x;
            print "############################################################\n";
        }
    }
}
