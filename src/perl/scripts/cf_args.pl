#!/usr/bin/env perl
use strict;
use 5.16.0;
use rob_misc;
use Data::Dumper;
use List::MoreUtils qw/uniq/;
use Term::ANSIColor;

my ($args1,$args2) = @ARGV;
my $defaults = get_defaults();
$args1 = parse_args($args1);
$args2 = parse_args($args2);
my @k = sort uniq (keys %$args1,keys %$args2);

my $n = 0;
my $lists = {
    ok => [],
    diff => [],
    NULL => [],
    Function => [],
};

# get max required parameter string length
my $maxlen = 0;
foreach my $k (sort @k)
{
    $maxlen=MAX(length($k),$maxlen);
}
my $format ="%".($maxlen+2)."s %35s %35s\n";

# loop to detect diffs
foreach my $k (sort @k)
{
    my $s = sprintf $format,
        $k,
        $args1->{$k},
        $args2->{$k};

    if($s=~/NULL/)
    {
        push(@{$lists->{NULL}},$s);
    }
    elsif($s=~/Function/)
    {
        push(@{$lists->{Function}},$s);
    }
    elsif(
        (
         # numeric comparison
         is_numeric($args1->{$k}) &&
         is_numeric($args2->{$k}) &&
         $args1->{$k} == $args2->{$k}
        )
        ||
        (
         # char comparison
         $args1->{$k} eq $args2->{$k}
        )
        )
    {
        push(@{$lists->{ok}},$s);
    }
    else
    {
        $s=~s/^ /\*/;
        push(@{$lists->{diff}},$s);
    }
}

# output
foreach my $x (
    ['ok','reset'],
    ['Function','cyan'],
    ['NULL','yellow'],
    ['diff','red'],
    )
{
    print color($x->[1]);
    if(defined $lists->{$x->[0]} &&
       ref $lists->{$x->[0]} eq 'ARRAY')
    {
        print join('',@{$lists->{$x->[0]}});
    }
    print color('reset');
}


sub parse_args
{
    my ($args) = @_;
#    print "\n\n\n############################################################\n\n\n",$args;
    $args=~s/^\s+//;
    $args=~s/\s+$//;
    my $h = {%$defaults};
    map
    {
        $h->{$_}=~s/^\s+//;
        $h->{$_}=~s/\s+$//;
    }keys %$h;

    my %args = map { s/^\-+//;$_; } split(/\s+/,$args);
    # we must loop in order of the args
    while(my ($key,$value) = each (%args))
    {
        $value=~s/^\s+//;
        $value=~s/\s+$//;
        $h->{$key} = $value;
    };
    #print Data::Dumper::Dumper($h);
    map
    {
        if($h->{$_}=~/false/i ||
           $h->{$_} eq 'f' ||
           $h->{$_} eq 'F')
        {
            $h->{$_} = '0';
        }
        elsif($h->{$_}=~/true/i ||
              $h->{$_} eq 't' ||
            $h->{$_} eq 'T')
        {
            $h->{$_} = '1';
        }

    }keys %$h;

    return map_argpairs($h);
}


sub get_defaults
{
    my %d = map{
        /^(\S+) = (.*)/; $1=>$2
    } split(/\n/,`$ENV{BINARY_C}/binary_c-config defaults`);
    return \%d;
}

sub map_argpairs
{
    # given a hash (reference) of $args, replace
    # macros with actual numbers
    state $argpairs = {map{/(\S+) (.*)/;$1=>$2}`$ENV{BINARY_C}/binary_c-config argpairs`};
    my ($args) = @_;
    foreach my $k (keys %$args)
    {
        if(exists $argpairs->{$args->{$k}})
        {
            $args->{$k} = $argpairs->{$args->{$k}};
        }
    }
    return $args;
}
