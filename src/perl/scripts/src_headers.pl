#!/usr/bin/env perl
use rob_misc;
use strict;

# script to make sure source files cannot be empty
# transition units

# paths are either src/whatever, if we're in the binary_c root,
# or just local .c and .h files
my $paths =
    -f './binary_c-config' ?
    'src/*.c src/*.h src/*/*.c src/*/*.h' :
    '*.c *.h';

foreach my $file (`ls $paths`)
{
    chomp $file;
    my $x = slurp($file);

    if($x !~ /No_empty_translation_unit_warning/)
    {
        $x =~ s!(\#include \"../binary_c.h\")!$1\nNo_empty_translation_unit_warning;\n!;
        print "Patched $file\n";
        #print $x;
        dumpfile($file,$x);
    }
}
