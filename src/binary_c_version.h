#pragma once
#ifndef BINARY_C_VERSION_H
#define BINARY_C_VERSION_H

/*
 * The binary_c stellar population nucleosynthesis framework.
 *
 * Copyright Robert Izzard 2000-2025
 *
 * Contact rob.izzard@gmail.com
 *
 * Code repository:
 * https://gitlab.com/binary_c
 *
 * Documentation is in doc/
 * Code is in src/
 *
 * To build, run
 *
 * ---
 *
 * meson setup builddir
 * cd builddir
 * ninja binary_c_install
 *
 * ---
 *
 * Please see the files README, LICENCE and CHANGES,
 * and the doc/ directory for documentation which is mirrored
 * at https://binary_c.gitlab.io
 *
 **********************
 *
 * Purpose:
 *
 * This file holds the version number for binary_c.
 *
 * BINARY_C_VERSION is this branch's version.
 *
 * It is NOT necessary the last stable release.
 *
 * Note: BINARY_C_VERSION should have no spaces in it and
 *       should be in the format:
 *
 *       <major>.<minor>.<patch>
 *
 * where major, minor and patch are integers.
 *
 * Patch is zero for a major release.
 */
#define BINARY_C_VERSION "2.2.4"

/*
 * BINARY_C_STABLE_VERSION is the last stable release
 */
#define BINARY_C_STABLE_VERSION "2.2.4"

/*
 * Matching version of binary_c-python
 */
#define BINARY_C_PYTHON_VERSION "1.0.0"

/*
 * Code development start and end years
 */
#define BINARY_C_START_YEAR 2000
#define BINARY_C_END_YEAR 2023

/* and a CEMP run version */
#define CEMP_VERSION "prelim10-O1-noCNEMPs"

#endif// BINARY_C_VERSION_H
