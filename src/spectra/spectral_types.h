#pragma once
#ifndef SPECTRAL_TYPES_H
#define SPECTRAL_TYPES_H

#define SPECTRAL_TYPE_UNDEFINED -1
#include "spectral_types.def"

#undef X
#define X(CODE) SPECTRAL_TYPE_##CODE,
enum { SPECTRAL_TYPE_LIST };

#undef X
#define X(CODE) #CODE,
static const char * const spectral_type_strings[] = { SPECTRAL_TYPE_LIST };
#undef X

#define NUM_SPECTRAL_TYPES 7
#define NUM_LUMINOSITY_SUBCLASSES 10

#endif // SPECTRAL_TYPES_H
