/*
 * C port of Paczyski's envelope integration code
 *
 * Based on gob84.for
 *
 * See
 * "Envelopes of Red Supergiants", by Paczynski
 * ACTA Astronomica Vol. 19, 1 (1969)
 */

#include "../binary_c.h"
No_empty_translation_unit_warning;



#undef _exit
#undef exit
#define _exit(X) exit(X)
#ifdef ENVELOPE_INTEGRATIONS

#define XX_LABEL(I) (                           \
            (I)==(X_M) ? "M" :                  \
            (I)==(X_R) ? "R" :                  \
            (I)==(X_TAU) ? "TAU" :              \
            (I)==(X_RHO) ? "RHO" :              \
            (I)==(X_T) ? "T" :                  \
            (I)==(6) ? "6" :                    \
            "unknown" )

#define UNDEF -666.666

const Boolean vb_env = FALSE;

#define MAX_INTEGRATIONS 30000

// max denisty is 1e3 in the GOB code
#define MAX_DENSITY 1e6
// max temperature is 1.5e8 in GOB
#define MAX_TEMPERATURE 3e8
// surface density is 1.1e-12 in GOB
#define INITIAL_SURFACE_DENSITY 1e-8
// density of the photosphere is 1e-12 in GOB
#define MAXIMUM_PHOTOSPHERIC_DENSITY 1e-12
// tauf is 2.0/3.0 in GOB
#define TAUF (2.0/3.0)
// tau at the surface (1e-30 in the original code, 2/3 to match given radius)
#define INITIAL_TAU (2.0/3.0)
// mixing length alpha
#define ALPHA_MLT 1.0


struct xion_t {
    double xh1;
    double xhe1;
    double xhe2;
};

struct grad_t {
    double vad;
    double grad;
    double gradad;
    double gradrad;
    double vt;
    //double delt; // unused?
};

struct abund_t
{
    double X;
    double Y;
    double Z;
};

/* equation numbers */
#define X_M 1
#define X_T 2
#define X_RHO 3
#define X_R 4
#define X_TAU 5
#define X_P 6

static void calc_envelope_structure(struct stardata_t * Restrict stardata,
                                    struct star_t * star,
                                    struct envelope_t * Restrict envelope,
                                    const double mass_resolution,
                                    int * iprint,
                                    int * ipr,
                                    int * iend);


static void pso(struct stardata_t * stardata,
                struct envelope_t * envelope,
                struct abund_t * abund,
                struct equation_of_state_t * eq,
                const double xx[7],
                const double lr,
                struct xion_t * xion,
                struct grad_t * grad,
                const double rp,
                const double tp,
                double * ya,
                double * yy,
                double * error);

int calculate_stellar_envelope_structure(struct stardata_t * stardata,
                                         struct star_t * star,
                                         struct envelope_t * envelope,
                                         const double depth,
                                         const double mass_resolution)
{
    /* set up envelope struct */
#ifdef NUCSYN
    envelope->x = star->Xenv[XH1];
    envelope->z = stardata->common.nucsyn_metallicity;
#else
    envelope->x = 0.700;
    envelope->z = stardata->common.metallicity;
#endif
    envelope->y = 1.0 - envelope->x - envelope->z;
    envelope->surface_density = INITIAL_SURFACE_DENSITY;
    envelope->surface_tau = INITIAL_TAU;
    envelope->tmax = MAX_TEMPERATURE;
    envelope->tauf = TAUF;
    envelope->alpha = 10*ALPHA_MLT;
    envelope->nshells = 0;
    envelope->shells = NULL;

    printf("  x =%9.6f     y = %g z =%9.6f\n",
           envelope->x,
           envelope->y,
           envelope->z);
    printf("  m, fm, flp1, tp1, dflp, dtp, nflp, ntp, iprint, alpha = ?\n");

    /************************************************************/

    /* mass in Msun */
    envelope->m = star->mass;

    /* depth (by mass) to be integrated */
    envelope->fm = star->mass - depth;

    /* log10 (L/Lsun) and log10 (T/K) */
    const double teff = Teff_from_star_struct(star);
    envelope->logl = log10(star->luminosity);
    envelope->logteff = log10(teff);

    /* test star */
    if(0)
    {
        envelope->m = 2.0;
        envelope->fm = 1.5;
        envelope->logl = log10(1e3);
        envelope->logteff = log10(4000.0);
        envelope->x = 0.700; // fixed by P's opacity table
        envelope->z = 0.03; // fixed by P's opacity table
        envelope->y = 1.0 - envelope->x - envelope->z;
    }
    /************************************************************/

    /*
     * convert Teff^4 = L / (4pi sigma R^2) to T0^4 = L/(8pi sigma R^2)
     * (Eq. 3)
     */
    envelope->logt0 = log10(teff / Pow1d4(2.0));

    /* output options */
    int iprint = 20;
    int ipr,iend;
    ipr = iprint;

    /************************************************************/

    calc_envelope_structure(stardata,
                            star,
                            envelope,
                            mass_resolution,
                            &iprint,
                            &ipr,
                            &iend);

    if(1)
    {
        int i;
        for(i=0; i<envelope->nshells; i++)
        {
            struct envelope_shell_t * shell =
                & envelope->shells[i];
            if(i%10==0)
                printf("ENV#nshell%12s%12s%12s%12s%12s\n",
                       "m(r)",
                       "r",
                       "T(r)",
                       "rho(r)",
                       "tau(r)");
            printf("ENV % 6d % 11g % 11g %11g % 11g % 11g\n",
                   i,
                   shell->m,
                   shell->r,
                   shell->temperature,
                   shell->density,
                   shell->tau);
        }
    }

    return 0;
}

static void pso(struct stardata_t * stardata,
                struct envelope_t * envelope,
                struct abund_t * abund,
                struct equation_of_state_t * eq,
                const double xx[7],
                const double lr,
                struct xion_t * xion,
                struct grad_t * grad,
                double rp,
                const double tp,
                double * ya,
                double * yy,
                double * error)
{
    /*
     * Calculate structure at a position in the envelope
     * e.g. call the equation of state and opacity
     * routines, and calculate the thermal gradient.
     */

    double mr,t,ro,r,q,cp,p,dpro,dpt,kp,dp_dm,g,hp,
        lt,u,omega,r2,t2,a,gamma,sqc,sqg,v,grop,cv;

    mr = envelope->m - xx[X_M];
    t = xx[X_T];
    ro = xx[X_RHO];
    r = xx[X_R];

    r2 = Pow2(r);
    t2 = Pow2(t);

    /* set up equation of state struct */
    eq->X = abund->X;
    eq->Y = abund->Y;
    eq->Z = abund->Z;

    eq->xh1 = xion->xh1;
    eq->xhe1 = xion->xhe1;
    eq->xhe2 = xion->xhe2;
    eq->density = ro;
    eq->temperature = t;
    eq->error = *error;

    /* calculate equation of state */
    equation_of_state(stardata,
                      eq,
                      stardata->preferences->equation_of_state_algorithm);

    /* set local variables */
    xion->xh1 = eq->xh1;
    xion->xhe1 = eq->xhe1;
    xion->xhe2 = eq->xhe2;

    p = eq->P;
    u = eq->U;
    cp = eq->Cp;
    cv = eq->Cv;
    q = -eq->dlnrho_dlnT_P;
    dpro = eq->dP_drho_T;
    dpt = eq->dP_dT_rho;

    grad->gradad = eq->gradad;
    grad->vad = eq->cs_ad;

    if(isnan(grad->vad))
    {
        printf("vad nan\n");
        _exit(0);
    }

    *error = eq->error;

    if(vb_env == TRUE) printf("thermodynamics: q=%12.3E cp=%12.3E cv=%12.3E gradad=%12.3E P=%12.3E dp/drho=%12.3E dp/dt=%12.3E u=%12.3E cv=%12.3E grad_vad=%12.3E error=%12.3E\n",
                     q,cp,cv,grad->gradad,p,dpro,dpt,u,cv,grad->vad,*error);

    /* set up opacity struct */
    struct opacity_t op;
    op.temperature = t;
    op.density = ro;
    op.H = envelope->x;
    op.Z = envelope->z;

    /* calculate opacity */
    kp = opacity(stardata,
                 &op,
                 stardata->preferences->opacity_algorithm);


    if(vb_env == TRUE) printf("opacity: rho = %12.3E T = %12.3E -> kappa = %12.3E\n",ro,t,kp);

    /* remaining calculations */
    dp_dm = -8.97e14 * mr / Pow2(r2);

    /*
     * yy = d/dM : Eq. 32
     */
    yy[X_R] = 0.470 / (r2 * ro);
    yy[X_TAU] = -3.27e10 / r2 * kp;
    yy[X_P] = mr / r - 5.24e-16 * u;
    yy[X_M] = -1.0;

    grad->gradrad = 7.65e9 * kp * p/ Pow2(t2) * lr / mr;

    if(xx[X_TAU] <= envelope->tauf)
    {
        /*
         * envelope correction
         */
        a = 1.285e-26 * Pow1d4(lr * r2)
            * (1.0 - xx[X_TAU]/envelope->tauf)/(ro*mr) * (t2*t);
        dp_dm *= 1.0 + a;
        grad->gradrad *= (1.0 + 1.296e4 * mr / ( lr * kp ) * a) / ( 1.0 + a );
    }

    if(grad->gradrad <= grad->gradad)
    {
        /*
         * Radiative
         */
        grad->grad = grad->gradrad;
        grad->vt = 0.0;
        //grad->delt = 0.0;
    }
    else
    {
        /*
         * Convective
         */
        g = 2.74e4 * mr / r2;
        hp = p / ( ro * g );
        lt = envelope->alpha * hp;
        omega = kp * ro * lt;
        a = 1.125 * Pow2(omega) / ( 3.0 + Pow2(omega) );
        gamma  = 826.7 * cp * ro / (t2 * t) * omega / a;
        sqc = lt * sqrt( g * q / (8.0*hp) );
        sqg = sqrt( grad->gradrad - grad->gradad );
        v = 1.0 / (gamma * sqc * sqg);
        a *= 2.0 / v;

        /* Newton Raphson */
        Boolean first = TRUE;
        while(first == TRUE ||
              fabs(grad->vt/ *ya) > 1e-3)
        {
            grad->vt = (1.0 - *ya * ( v + *ya * ( 1.0 + *ya * a )))
                /( v + *ya * ( 2.0 + *ya * 3.0 * a) );
            *ya += grad->vt;
            first = FALSE;
        }

        grad->vt = sqc * sqg * *ya;
        grad->grad = grad->gradad + ( grad->gradrad - grad->gradad ) * *ya * ( *ya + v );
        //grad->delt = 4.0 * t * grad->vt * grad->vt / (g * lt * q);
    }

    /* Eq 30 : grop = nabla_rho */
    grop = ( p - grad->grad * t * dpt ) / (ro * dpro);

    /* Eq 32 */
    yy[X_T] = grad->grad * t / p * dp_dm;
    yy[X_RHO] = grop * ro / p * dp_dm;

    printf("YY 3 = drho/dm = from grop=%g * ro=%g / p=%g * dp_dm=%g = %g\n",
           grop,ro,p,dp_dm,yy[X_RHO]);

    if(vb_env == TRUE) printf("yyret 1=%12.3E 2=%12.3E 3=%12.3E 4=%12.3E 5=%12.3E 6=%12.3E\n",
                     yy[X_M],yy[X_T],yy[X_RHO],yy[X_R],yy[X_TAU],yy[X_P]);
}


static void calc_envelope_structure(struct stardata_t * stardata,
                                    struct star_t * star,
                                    struct envelope_t * Restrict envelope,
                                    const double mass_resolution,
                                    int * iprint,
                                    int * ipr,
                                    int * iend)
{
    //envelope->logl = 3.0; // logL > 2 gives an inversion
    *iprint = *ipr;
    const double l = exp10( envelope->logl);
    const double tp = exp10( envelope->logt0);
    const double rp = sqrt( 5.6e14 * l ) / Pow2( tp );
    const double mbol = 4.74 - 1.0857 * log(l);
    const double rplog = log10(rp);
    double ya = 1.0;
    double dt = 0.0;
    double ermax = -10.0;
    struct grad_t grad;
    struct xion_t xion = {
        .xh1 = 0.0,
        .xhe1 = 0.0,
        .xhe2 = 0.0
    };
    const double acc[6] = {
        UNDEF,
        0.2,
        0.05,
        0.15,
        0.05,
        0.1
    };

    /* initial state */
    double xx[7];
    xx[0] = UNDEF; // unused (Fortran array indexing!)
    xx[X_M] = 1.0e-30;
    xx[X_T] = tp;
    xx[X_RHO] = envelope->surface_density;
    xx[X_R] = rp;
    xx[X_TAU] = envelope->surface_tau;
    xx[X_P] = 0.0;

    /* other variables */
    double error=-100.0,tef=0.0,fac=0.0;
    int i,k=0;

    printf("  log l/lsun =%6.3f    m bol =%6.3f    log r/rsun =%6.3f    log t0 =%6.3f\n",
           envelope->logl,mbol,rplog,envelope->logt0);
    printf("    l dm    l t  l rho    l r  l tau   l gr   l ga   l ra  xh1 xhe1 xhe2  err\n");

    double w[7];
    memset(w,0,sizeof(int)*7);

    struct abund_t * abund = Malloc(sizeof(struct abund_t));
    int nan = 0;

    while(k++ < MAX_INTEGRATIONS)
    {
        if(vb_env == TRUE)  printf("Iteration %4d\n",k);


        memcpy(w,xx,sizeof(double)*7);

        if(vb_env == TRUE) printf("w 1=%12.3E 2=%12.3E 3=%12.3E 4=%12.3E 5=%12.3E 6=%12.3E\n",
                     w[1],w[2],w[3],w[4],w[5],w[6]);

        /*
         * Current mass co-ordinate
         */
        double Mr = envelope->m - xx[X_M];

        /*
         * Abundances are either taken from the
         * envelope, or depend on mass co-ordinate
         * in some way.
         */
        abund->X = envelope->x;
        abund->Y = envelope->y;
        abund->Z = envelope->z;

        /*
         * Enhance helium near the core
         */
        if(0 && Mr < star->core_mass[CORE_He] * 1.3)
        {
            abund->X = 0.01;
            abund->Y = 1.0 - abund->X - abund->Z;
        }

        double yy[7];
        struct equation_of_state_t eq;
        pso(stardata,
            envelope,
            abund,
            &eq,
            xx,
            l,
            &xion,
            &grad,
            rp,
            tp,
            &ya,
            yy,
            &error);

        if(vb_env == TRUE)
        {
            printf("xx: M=%12.3E T=%12.3E RHO=%12.3E R=%12.3E TAU=%12.3E P=%12.3E\n",
                   xx[X_M],xx[X_T],xx[X_RHO],xx[X_R],xx[X_TAU],xx[X_P]);
            printf("grad: vad=%12.3E grad=%12.3E gradad=%12.3E gradrad=%12.3E\n",
                   grad.vad,grad.grad,grad.gradad,grad.gradrad);
        }

        if(error > ermax) ermax = error;

        (*iprint) ++;
        if(*iprint >= 0 && *ipr >= 0)
        {
            *iprint = - *ipr;
            double dmlog = log10(xx[X_M]);
            double tlog = log10(xx[X_T]);
            double rhlog = log10(xx[X_RHO]);
            double rlog = log10(xx[X_R]);
            double taulog = log10(xx[X_TAU]);
            double grl = log10(grad.grad);
            double gal = log10(grad.gradad);
            double gral = log10(grad.gradrad);
            printf(" %7.2f%7.2f%7.2f%7.2f%7.2f%7.2f%7.2f%7.2f%5.2f%5.2f%5.2f%6.2f\n",
                   dmlog,
                   tlog,
                   rhlog,
                   rlog,
                   taulog,
                   grl,
                   gal,
                   gral,
                   xion.xh1,
                   xion.xhe1,
                   xion.xhe2,
                   error);
        }

        /*
         * choose an integration step = h
         */
        double h;

        if(xx[X_TAU] < envelope->tauf)
        {
            h = fabs( yy[X_TAU] / acc[X_TAU] );
        }
        else
        {
            h = 0.0;
        }

        {
            double Y = 1.0 / (acc[X_T] * Mr );
            if(h < Y)
            {
                h = Y;
            }
        }


        for(i=2;i<5;i++)
        {
            double Y = fabs( yy[i] / ( acc[i]*xx[i] ) );
            if(h < Y)
            {
                h = Y;
            }
        }
        h = 1.0 / h;

        /* enforce maximum mass resolution */
        h = Min(h,mass_resolution);

        /* go down into the star */
        h = -h;

        /*
         * integration step = h   has been chosen
         */
        *iend = -1;
        if(xx[X_M] - h >= envelope->fm)
        {
            h = xx[X_M] - envelope->fm;
            *iend = 1;
        }

        double xp[7];
        for(i=1;i<7;i++)
        {
            xp[i] = xx[i] + 0.5 * h * yy[i];
            if(isnan(xp[i])) nan = 1;
        }

        pso(stardata,
            envelope,
            abund,
            &eq,
            xp,
            l,
            &xion,
            &grad,
            rp,
            tp,
            &ya,
            yy,
            &error);

        for(i=1;i<7;i++)
        {
            printf("D XX %d (%s) was %g : += (h=%g) * (yy[%d]=%g) -> %g\n",
                   i,
                   XX_LABEL(i),
                   xx[i],
                   h,
                   i,
                   yy[i],
                   xx[i] + h * yy[i]);
            fflush(stdout);
            xx[i] += h * yy[i];

            if(isnan(xx[i])) nan = 2;

            if(i==X_RHO)
            {
                if(h*yy[i]<0.0)
                {
                    printf("RHO DROP!\n") ;
                    //_exit(0);
                }
            }
        }

        /*
         * an integration step has been completed
         */
        dt = -6.96e10 * h * yy[X_R] / grad.vad;
        printf("dt = %g * %g / %g\n",h,yy[X_R],grad.vad);


        if(isnan(dt)) nan = 3;

        {
            /*
             * Save envelope structure
             */
            envelope->nshells ++;
            envelope->shells = Realloc(envelope->shells,
                                       sizeof(struct envelope_shell_t)*
                                       envelope->nshells);
            struct envelope_shell_t * shell =
                & envelope->shells[envelope->nshells-1];
            shell->m = envelope->m - xx[X_M];
            shell->dm = -h;
            shell->r = xx[X_R];
            shell->tau = xx[X_TAU];
            shell->temperature = xx[X_T];
            shell->density = xx[X_RHO];
            shell->pressure = eq.P;

            shell->grad = grad.grad;
            shell->gradad = grad.gradad;
            shell->gradrad = grad.gradrad;
            shell->xion_h1 = xion.xh1;
            shell->xion_he1 = xion.xhe1;
            shell->xion_he2 = xion.xhe2;
            shell->X = abund->X;
            shell->Y = abund->Y;
            shell->Z = abund->Z;
        }

        if(vb_env == TRUE) printf("endstep iend=%4d ipr=%4d xx: M=%12.3E T=%12.3E RHO=%12.3E R=%12.3E TAU=%12.3E P=%12.3E dt=%12.3E\n",
                     *iend,*ipr,xx[X_M],xx[X_T],xx[X_RHO],xx[X_R],xx[X_TAU],xx[X_P],dt);
        if(*iend < 0)
        {
            if(vb_env == TRUE) printf(" iend < 0\n");
        }
        if(*ipr<0)
        {
            if(vb_env == TRUE) printf(" ipr < 0\n");
        }

        if(*iend >= 0)
        {
            if(*ipr > 0)
            {
                double dmlog = log10(xx[X_M]);
                double tlog = log10(xx[X_T]);
                double rhlog = log10(xx[X_RHO]);
                double rlog = log10(xx[X_R]);
                double taulog = log10(xx[X_TAU]);
                double grl = log10(grad.grad);
                double gal = log10(grad.gradad);
                double gral = log10(grad.gradrad);
                //printf("%5.2f %5.2f %5.2f %5.2f %5.2f %5.2f %5.2f %5.2f %5.2f\n",
                //dmlog,tlog,rhlog,rlog,taulog,grl,gal,gral,error);
                printf(" %7.2f%7.2f%7.2f%7.2f%7.2f%7.2f%7.2f%7.2f%5.2f%5.2f%5.2f%6.2f\n",
                       dmlog,tlog,rhlog,rlog,taulog,grl,gal,gral,xion.xh1,xion.xhe1,xion.xhe2,error);
            }
            break;
        }

        double tauc = -1.0;
        if(xx[X_TAU] > 0.05 && w[5] < 0.05) tauc = 0.05;
        if(xx[X_TAU] > envelope->tauf && w[5] < envelope->tauf) tauc = envelope->tauf;

        if(tauc>=0.0)
        {
            fac = ( xx[X_TAU] - tauc ) / ( xx[X_TAU] - w[5] );
            for(i=1; i<7; i++)
            {
                w[i] = fac * w[i] + (1.0 - fac) * xx[i];
            }
            if(*ipr>=0)
            {
                /*
                 * printing at optical depth = 0.05, 0.66667
                 */
                double dmlog = log10(w[1]);
                double tlog = log10(w[2]);
                double rhlog = log10(w[3]);
                double rlog = log10(w[4]);
                double taulog = log10(w[5]);
                printf(" %7.2f%7.2f%7.2f%7.2f%7.2f\n",
                       dmlog,tlog,rhlog,rlog,taulog);
            }
            if(fabs(1.0 - tauc / envelope->tauf) < 0.01) tef = log10( w[2] );
        }

        if(vb_env == TRUE)
        {
            printf("xx : M=%12.3E T=%12.3E RHO=%12.3E TAU=%12.3E tmax=%12.3E 0.9m=%12.3E tauf=%12.3E\n",
                   xx[X_M],xx[X_T],xx[X_RHO],xx[X_TAU],envelope->tmax,0.9*envelope->m,envelope->tauf);
            if(xx[X_T] > envelope->tmax) printf("test1\n");
            if(xx[X_M] > 0.9*envelope->m) printf("test2\n");
            if(xx[X_RHO] > 1000.0) printf("test3\n");

            if(xx[X_RHO] < MAXIMUM_PHOTOSPHERIC_DENSITY && xx[X_TAU] > envelope->tauf) printf("test4\n");
        }

#define CHECK(...) { brk = 1; printf(__VA_ARGS__); break; }

        int brk = 0;
        if(xx[X_T] > envelope->tmax) CHECK("T > Tmax = %g\n",envelope->tmax);
        if(xx[X_M] > 0.9*envelope->m) CHECK("M < 0.1 * M\n");
        if(xx[X_RHO] > MAX_DENSITY) CHECK("rho > 1e3\n");
        //if(xx[X_RHO] < 1e-12 && xx[X_TAU] > envelope->tauf)
        //CHECK("rho < 1e-12 with tau > tauf = %g\n",envelope->tauf);

        if(brk==1)
        {
            printf("exiting because failure condition detected\n");
            _exit(0);
        }

        if(nan)
        {
            printf("nan detected at %d\n",nan);
            _exit(0);
        }

    }

    if(vb_env == TRUE) printf("115%12.3E%12.3E\n",xx[X_T],envelope->tmax);

    if(xx[X_T] >= envelope->tmax)
    {
        fac = ( xx[X_T] - envelope->tmax ) / ( xx[X_T] - w[2] );
        double fac1 = 1.0 - fac;
        for(i=1; i<7; i++)
        {
            w[i] = fac * w[i] + fac1 * xx[i];
        }
        fac = w[6] / w[1];
    }

    fac = 617.0 * sqrt( fabs(fac) ) * copysign(1.0,fac);
    double ref = 7.5246 + 0.5 * envelope->logl - 2.0 * tef;
    double ti = log10(xx[X_T]);
    double rhoi = log10(xx[X_RHO]);
    double ri = log10(xx[X_R]);
    printf(" %8.4f%8.4f%8.4f%8.4f%8.4f%8.4f%8.4f%8.4f%8.4f%6.2f\n",
           envelope->m,envelope->fm,envelope->logl,envelope->logt0,
           tef,ref,ti,rhoi,ri,ermax);


    if(nan)
    {
        printf("nan detected at %d\n",nan);
        _exit(0);
    }

    Safe_free(abund);
}



#endif // ENVELOPE_INTEGRATIONS
