#pragma once
#ifndef BINARY_C_UNIT_FUNCTION_MACROS_H
#define BINARY_C_UNIT_FUNCTION_MACROS_H

/*
 * The binary_c stellar population nucleosynthesis framework.
 *
 * Copyright Robert Izzard 2000-2025
 *
 * Contact rob.izzard@gmail.com
 *
 * Code repository:
 * https://gitlab.com/binary_c
 *
 * Documentation is in doc/
 * Code is in src/
 *
 * To build, run
 *
 * ---
 *
 * meson setup builddir
 * cd builddir
 * ninja binary_c_install
 *
 * ---
 *
 * Please see the files README, LICENCE and CHANGES,
 * and the doc/ directory for documentation which is mirrored
 * at https://binary_c.gitlab.io
 *
 **********************
 *
 * Purpose:
 *
 * Macros to convert units from one type to another,
 * e.g. cgs to solar units, or to/from code units.
 *
 **********************
 */

/*
 * Convert X (in cgs) to the appropriate solar unit,
 * where TYPE is L, M or R
 */
#define Solarunit(X,TYPE) ((X) / TYPE##_SUN)

/*
 * Convert X (in cgs) to the appropriate solar unit,
 * and return a pair of variables:
 *
 * 1) the variable X (in cgs) divided by the appropriate solar constant
 * 2) the solar constant string
 *
 * Type is L, M or R
 *
 * You can thus do something like:
 *
 * r = 123.45 * R_SUN;
 * printf("Radius is %g %s",Solar(r,R));
 *
 * to output something like
 *
 * "Radius is 123.45 R☉"
 */
#define Solar(X,TYPE) Solarunit((X),TYPE) , TYPE##_SOLAR



/*
 * Convert (specific) angular momentum from cgs to
 * code units, and vice versa
 */
#define Angular_momentum_cgs_to_code(J)         \
    ((J) / ANGULAR_MOMENTUM_CGS)
#define Angular_momentum_code_to_cgs(J)         \
    ((J) * ANGULAR_MOMENTUM_CGS)
#define Angular_momentum_derivative_cgs_to_code(Jdot)               \
    (Angular_momentum_cgs_to_code(Jdot) * YEAR_LENGTH_IN_SECONDS)
#define Angular_momentum_derivative_code_to_cgs(Jdot)               \
    (Angular_momentum_code_to_cgs(Jdot) / YEAR_LENGTH_IN_SECONDS)

#define Specific_Angular_momentum_cgs_to_code(L)    \
        ((L) / SPECIFIC_ANGULAR_MOMENTUM_CGS)
#define Specific_Angular_momentum_code_to_cgs(L)    \
    ((L) * SPECIFIC_ANGULAR_MOMENTUM_CGS)
#define Specific_Angular_momentum_derivative_cgs_to_code(Ldot)          \
    (Specific_Angular_momentum_cgs_to_code(Ldot) * YEAR_LENGTH_IN_SECONDS)
#define Specific_Angular_momentum_derivative_code_to_cgs(Ldot)          \
    (Specific_Angular_momentum_code_to_cgs(Ldot) / YEAR_LENGTH_IN_SECONDS)


#endif // BINARY_C_UNIT_FUNCTION_MACROS_H
