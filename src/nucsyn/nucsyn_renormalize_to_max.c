#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN

/*
 * Given an array of abundances X, renormalize
 * the total to 1.0 by finding the dominant isotope
 * and setting any error into that.
 *
 * Note that if the sum is zero, do nothing.
 *
 * X must not be NULL.
 */

void Hot_function Nonnull_some_arguments(1)
    nucsyn_renormalize_to_max(Abundance * Restrict const X)
{
    /*
     * Find the most abundant isotope:
     * we do this by trying a few commonly-abundant
     * isotopes, if they exceed 50% they must
     * be the most abundant.
     *
     * If none is >50% we have to check all the isotopes.
     */
    double max;
    Isotope imax;

    if(X[XH1] > 0.5)
    {
        imax = XH1;
        max = X[XH1];
    }
    else if(X[XHe4] > 0.5)
    {
        imax = XHe4;
        max = X[XHe4];
    }
    else if(X[XC12] > 0.5)
    {
        imax = XC12;
        max = X[XC12];
    }
    else if(X[Xn] > 0.5)
    {
        imax = Xn;
        max = X[Xn];
    }
    else if(X[XO16] > 0.5)
    {
        imax = XO16;
        max = X[XO16];
    }
    else
    {
        /*
         * Assume hydrogen is the max: it usually is,
         * then check the other isotopes.
         */
        max = X[XH1];
        imax = XH1;
        Nucsyn_isotope_stride_loop(
            if(unlikely(X[i] > max))
            {
                imax = i;
                max = X[i];
            }
            );
    }

    const double tot = nucsyn_totalX(X);
    if(Is_not_zero(tot))
    {
        /*
         * The following line is equivalent to:
         * a) Set X[imax] = 0
         * b) Sum up X
         * c) Set X[imax] to 1 - the sum
         *
         * hence we want
         * X[imax] = 1.0 - (tot - X[imax])
         *         = 1.0 - tot + X[imax]
         *
         * By saving the total above in tot
         * we save a call to nucsyn_totalX()
         */
        X[imax] = 1.0 - tot + X[imax];
    }
}
#endif //NUCSYN
