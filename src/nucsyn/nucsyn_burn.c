#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef NUCSYN

#include <string.h>
#include "nucsyn_isotopes.h"

#define NORMAL_BURN TRUE


// enable this debug flag for extra messages
#if (DEBUG==1)
#define HBB_DEBUG
#endif

/*
 * dt is the timestep (years), N is the number density (cgs), passed in is the
 * old number density, passed out is the new. oN stores the old values of the
 * number density. logT = log10(temperature/K), density is in g/cm^3.
 */
void nucsyn_burn(const double logT,
                 const double density,
                 const double dt,
                 Number_density * Restrict const N,
                 struct star_t * const star Maybe_unused,
                 struct stardata_t *  const stardata
    )
{
#ifdef NUCSYN_NUMERICAL_BURN
    /*
     * Numerical burn: pass the hard work to the
     * nucsyn_network_burn function
     */
    double sigmav[SIGMAV_SIZE]; /* NB we ignore sigmav[0] */
    Dprint("nucsyn_burn: call nucsyn_set_sigmav\n");

     /* electron density required for Be7 e-capture */
    N[Xe] = nucsyn_free_electron_density(N,stardata->store->atomic_number);
    nucsyn_set_sigmav(stardata,
                      stardata->store,
                      logT,
                      sigmav,
                      stardata->preferences->reaction_rate_multipliers
#ifdef RATES_OF_AMANDA_NE22PG_FIX
                      ,cf88_fix
#endif
        );

    Dprint("nucsyn_burn: call burning cycles %g %g\n",N[0],N[1]);


#ifdef NANCHECKS
    Nucsyn_isotope_stride_loop(
        if(isnan(N[i])!=0)
        {
            Exit_binary_c(BINARY_C_EXIT_NAN,"nucsyn_burn (preburn) isotope %u/%u () is %g\n",
                          i,
                          ISOTOPE_ARRAY_SIZE,
                          N[i]);
        }
        );
#endif // NANCHECKS

    /* call the burning cycles (dt in years) */
    nucsyn_burning_cycles(N,sigmav,dt,exp10(logT),density,stardata);

    Dprint("nucsyn_burn: post-burning cycles %g %g\n",N[0],N[1]);

    return;

#endif // NUCSYN_NUMERICAL_BURN

#ifdef NUCSYN_ANAL_BURN
#define MEAN_ABUND(A) (0.5*(Nin[(A)]+N[(A)]))
/*
 * Function to actually do the (numerical) burning at temperature t and for
 * time dt.
 *
 * Now includes: CNO, NeNa and MgAl cycles.
 *
 * Note: this function does not calculate hydrogen or helium abundances
 * properly!
 */


/* Nuclear Reaction Rates (sigmav) */

    double sigmav[SIGMAV_SIZE]; /* NB we ignore sigmav[0] */
    Boolean sigmavset=FALSE;
    double dtseconds;


/*     analytic solution stuff */
#ifdef HBB_DEBUG
    double oN[3];
#endif
    double timescales[7],Nin[ISOTOPE_ARRAY_SIZE],ddN,dH1=0.0,invNH1;

    // F19
    double dF1,dF2;
    double dO1;//,dO2,dO3;
    // convective turnover time
    double tc,tcsecs;

    //useful eq abunds output
    //
    //#define TTEST
#ifdef TTEST
    double n1,n2,n3;
#endif

#ifdef RATES_OF_AMANDA_NE22PG_FIX
    Boolean cf88_fix;
    if((star->effective_zams_mass>4.5)||(star->effective_zams_mass<6.6))
    {
        cf88_fix=TRUE;
    }
    else
    {
        cf88_fix=FALSE;
    }
#endif


#ifdef HBB_DEBUG
    int i;
#endif
    /* calculate dt in seconds and return if<0 */
    dtseconds=dt*YEAR_LENGTH_IN_SECONDS;
    if(dtseconds<TINY) return;

#ifdef TTEST
    n1=N[1]; n2=N[2]; n3=N[3];
    dtseconds=1e20; // burn to eq

    for(T=7.2;T<=8.2;T+=0.01)
    {
        N[1]=n1; N[2]=n2; N[3]=n3;

#endif // TTEST



        nucsyn_set_sigmav(stardata,
                          stardata->store,
                          logT,
                          sigmav,
                          stardata->preferences->reaction_rate_multipliers
#ifdef RATES_OF_AMANDA_NE22PG_FIX
                          ,cf88_fix
#endif
            );
        sigmavset=TRUE;


        // perhaps we need the convective turnover time?
#ifdef NUCSYN_Na22_EQUILIBRIUM
        tc = MR23YR*pow((star->menv*0.5*Pow2(star->radius)*
                         (3.0*star->luminosity)),(1.0/3.0));
#endif
        /*
         * do analytic burn a la Clayton CN/ON cycles
         *     Burn CN cycle:
         */
        /* Save number densities for C12,C13,N14 */
#ifdef HBB_DEBUG
        oN[0]=N[XC12];
        oN[1]=N[XC13];
        oN[2]=N[XN14];
#endif

        /********************* start the burn ! ********************/

        invNH1=1.00/N[XH1]; // used MANY times, in all the cycles

#ifdef NUCSYN_TPAGB_HBB_CN_CYCLE
        timescales[1]=Nuclear_burning_timescale(SIGMAV_T12);
        timescales[2]=Nuclear_burning_timescale(SIGMAV_T13);
        timescales[3]=sigmav[SIGMAV_BRANCH_ALPHA]*Nuclear_burning_timescale(SIGMAV_T14);

        Nin[1]=N[XC12];            // C12
        Nin[2]=N[XC13];            // C13
        Nin[3]=N[XN14];            // N14

#ifdef HBB_DEBUG
        printf("\nPre-CN numbers : C12=%g C13=%g N14=%g sum=%g\n",
               N[XC12],N[XC13],N[XN14],Nin[1]+Nin[2]+Nin[3]);
#endif
        /*
         * Call the analytic burn function
         */
        nucsyn_anal_CNO_burn(timescales,Nin,dtseconds,NORMAL_BURN);

#ifdef HBB_DEBUG
        printf("PostCN numbers : C12=%g C13=%g N14=%g sum=%g\n",
               Nin[1],Nin[2],Nin[3],Nin[1]+Nin[2]+Nin[3]);
#endif

        if((isnan(Nin[1])==0)&&
           (isnan(Nin[2])==0)&&
           (isnan(Nin[3])==0))
        {

            N[XC12]=Nin[1];
            N[XC13]=Nin[2];
            N[XN14]=Nin[3];
        }

#ifdef HBB_DEBUG
        printf("\n\nCN 12 t=%g N=%g->%g 13 t=%g N=%g->%g 14 %g N=%g->%g\n",
               timescales[1],oN[0],N[XC12],
               timescales[2],oN[1],N[XC13],
               timescales[3],oN[2],N[XN14]);
#endif

        dH1-=(MEAN_ABUND(XC12)/Nuclear_burning_timescale(SIGMAV_T12)+
              MEAN_ABUND(XC13)/Nuclear_burning_timescale(SIGMAV_T13)+
              MEAN_ABUND(XN14)/Nuclear_burning_timescale(SIGMAV_T14)+
              MEAN_ABUND(XN15)/Nuclear_burning_timescale(SIGMAV_T15)
            );

        Dprint("NNOF 13,15,15,17=%g,%g,%g,%g\n",
               sigmav[SIGMAV_TBETA13]/(invNH1/sigmav[SIGMAV_T12]),
               sigmav[SIGMAV_TBETA15]/(invNH1/sigmav[SIGMAV_T14]),
               (invNH1/sigmav[SIGMAV_T15])/sigmav[SIGMAV_TBETA15],
               sigmav[SIGMAV_TBETA17]/(invNH1/sigmav[SIGMAV_T16]));
#endif /* NUCSYN_TPAGB_HBB_CN_CYCLE */


#ifdef NUCSYN_TPAGB_HBB_ON_CYCLE
        /* ON cycle burning */

#define NINS Nin[1],Nin[2],Nin[3]
#define TIMESCALES timescales[1],timescales[2],timescales[3]

        /* time for ON cycle */
        if(sigmavset==FALSE) nucsyn_set_sigmav(stardata,
                                               stardata->store,
                                               T,
                                               sigmav,
                                               stardata->preferences->reaction_rate_multipliers
#ifdef RATES_OF_AMANDA_NE22PG_FIX
                                               ,cf88_fix
#endif
            );


        /* Now let the ON cycle burn! */
        timescales[1]=Nuclear_burning_timescale(SIGMAV_T14)/sigmav[SIGMAV_BRANCH_GAMMA];
        timescales[2]=Nuclear_burning_timescale(SIGMAV_T16);
        timescales[3]=Nuclear_burning_timescale(SIGMAV_T17);

#ifdef HBB_DEBUG
        oN[0]=N[XN14];
        oN[1]=N[XO16];
        oN[2]=N[XO17];
#endif
        Nin[1]=N[XN14]+N[XC12]+N[XC13];
        Nin[2]=N[XO16];
        Nin[3]=N[XO17];

#ifdef HBB_DEBUG
        printf("\nPre-ON numbers : N14(+C12+C13)=%g O16=%g O17=%g \n",
               Nin[1],Nin[2],Nin[3]);
        printf("ON timescales %g %g %g\n",timescales[1],timescales[2],timescales[3]);
#endif
        /*
         * Call the burn function
         */
        nucsyn_anal_CNO_burn(timescales,Nin,dtseconds,NORMAL_BURN);

#ifdef HBB_DEBUG
        printf("PostON numbers : N14(+C12+C13)=%g O16=%g O17=%g \n",
               Nin[1],Nin[2],Nin[3]);
#endif


        if((isnan(Nin[1])==0)&&
           (isnan(Nin[2])==0)&&
           (isnan(Nin[3])==0))
        {
            N[XN14]=Nin[1]-N[XC12]-N[XC13];
            N[XO16]=Nin[2];
            N[XO17]=Max(0.0,Nin[3]); // sometimes gets <0 (when ~0)
        }
#ifdef HBB_DEBUG
        printf("Proton Diffs: N14 = %g-%g=%g , O16 = %g-%g=%g, O17 = %g-%g=%g\n",
               14.0*(N[XN14]),
               14.0*(oN[0]),
               14.0*(N[XN14]-oN[0]),
               16.0*(N[XO16]),
               16.0*(oN[1]),
               16.0*(N[XO16]-oN[1]),
               17.0*(N[XO17]),
               17.0*(oN[XO17]),
               17.0*(N[XO17]-oN[XO17])
            );
        printf("Estimate dHe4=%g (c.f. old est %g)\n",
               dtseconds*N[XO17]/(invNH1/sigmav[SIGMAV_T17]),
               ddN);
        printf("Better Estimate dHe4=%g (c.f. old est %g)\n",
               dtseconds*0.5*(N[XO17]+oN[XO17])/(invNH1/sigmav[SIGMAV_T17]),
               ddN);
#endif
#endif


        // Assume eq values for N13, O15, N15 and F17
        // the timescales are invNH1/sigmav[...] except the
        // beta decays which are the actual times
        //tc=1.0;

        // N13 is broken! it feeds back into C13 by beta decay :(
        //N[XN13]=N[XC12]*(sigmav[SIGMAV_TBETA13]/(invNH1/sigmav[SIGMAV_T12]));
        N[XO15]=N[XN14]*(sigmav[SIGMAV_TBETA15]/(invNH1/sigmav[SIGMAV_T14]));

        // N15 can be created from O18(p,alpha)N15 and O15 beta decay
        // it is destroyed by proton capture
        N[XN15]=((invNH1/sigmav[SIGMAV_T15])
            )
            *// t15
            (N[XO15]/(sigmav[SIGMAV_TBETA15])+ // O15/tb15
             N[XO18]/(invNH1/sigmav[SIGMAV_O18A]) // O18/O18A
                );

        dH1-=(MEAN_ABUND(XO16)/Nuclear_burning_timescale(SIGMAV_T16)+
              MEAN_ABUND(XO17)/Nuclear_burning_timescale(SIGMAV_T17)+
              MEAN_ABUND(XN15)/Nuclear_burning_timescale(SIGMAV_T15_BRANCH));

        N[XF17]=N[XO17]*(sigmav[SIGMAV_TBETA17]/(invNH1/sigmav[SIGMAV_T16]));

        // O18 is created from O17
        // equilibrium?
        N[XO18]=N[XO17]/(invNH1/sigmav[SIGMAV_O17G])*
            1.0/( 1.0/(invNH1/sigmav[SIGMAV_O18A]) +
                  1.0/(invNH1/sigmav[SIGMAV_O18G])
                );

        dO1=N[XO17]*(1.0-exp(-dtseconds/(invNH1/sigmav[SIGMAV_O17G])));
        //dO2=N[XO18]*(1.0-exp(-dtseconds/(invNH1/sigmav[SIGMAV_O18A])));

        /* explicit fluorine burning */
        dF1=N[XF19]*(1.0-exp(-dtseconds/(invNH1/sigmav[SIGMAV_F19A]))); // F19(p,alpha)O16
        dF2=N[XO18]*(1.0-exp(-dtseconds/(invNH1/sigmav[SIGMAV_O18G]))); // O18(p,g)F19

        //N[XN15]+=dO2;
        N[XO16] += dF1;
        N[XO17] -=dO1;
        //N[XO18] +=dO1-dO2-dF2;
        N[XF19] += dF2 - dF1;

        // and create some F19 from O18
        //dF=N[XF19]*(1.0-exp(-dtseconds*invNH1/sigmav[SIGMAV_O18G]));
        /*printf("F19 =%g dF=%g O18pgF19 t=%g\n",N[XF19],dF,
          invNH1/sigmav[SIGMAV_O18G]
          );
        */


#ifdef NUCSYN_ALLOW_COOL_CNO_BURNING
        /*
         * Check the temperature here, if it's too low (<1e6K)
         * then do NOT activate the NeNa and MgAl cycles
         */
        if(logT<6)
        {
            return;
        }

#endif


        if(stardata->preferences->NeNaMgAl==TRUE)
        {
#ifdef NUCSYN_TPAGB_HBB_NeNa_CYCLE
            /* Analytic form of the NeNa cycle */
            /* Set up tau_20,21,22,23 */
            //  if(logT>T_MIN_NeNa_HBB)
            if(1==1)
            {
                timescales[0]=Nuclear_burning_timescale(SIGMAV_F19A);
                timescales[1]=Nuclear_burning_timescale(SIGMAV_T20);
                timescales[2]=Nuclear_burning_timescale(SIGMAV_T21);
                timescales[3]=Nuclear_burning_timescale(SIGMAV_T22);
                timescales[4]=Nuclear_burning_timescale(SIGMAV_T23);
                timescales[5]=Nuclear_burning_timescale(SIGMAV_T23P);

                Dprint("NenaT T9=%g (T=%g) tscls %g %g %g %g leak %g\n",
                       exp10(T)/1e9,logT,
                       timescales[1],
                       timescales[2],
                       timescales[3],
                       timescales[4],
                       timescales[5]);
                /* Set up burn time */
#ifndef TTEST
                dtseconds=dt*YEAR_LENGTH_IN_SECONDS;
#endif
                /* Set up number densities */
                Nin[0]=N[XF19]; // entry in the cycle
                Nin[1]=N[XNe20]; // cycle
                Nin[2]=N[XNe21];
                Nin[3]=N[XNe22];
                Nin[4]=N[XNa23];

                // Call old version of the code
                //nucsyn_anal_NeNa_burn(timescales,Nin,dtseconds,NORMAL_BURN);

                // Call new version which is far better for you
                nucsyn_anal_NeNa_burn1(stardata,
                                       timescales,
                                       Nin,dtseconds);

                if((isnan(Nin[1])==0)&&
                   (isnan(Nin[2])==0)&&
                   (isnan(Nin[3])==0)&&
                   (isnan(Nin[4])==0)&&
                   (isnan(Nin[0])==0))
                {
                    N[XF19]=Nin[0];
                    N[XNe20]=Nin[1];
                    N[XNe21]=Nin[2];
                    N[XNe22]=Nin[3];
                    N[XNa23]=Nin[4];
                }


#ifdef NUCSYN_Na22_EQUILIBRIUM
                // set Na22 to be in equilibrium, remember to increase
                // beta decay time by convective turnover time
                N[XNa22]=((3.74/tc)/timescales[2])*N[XNe21];
#endif
            }
#endif //NUCSYN_TPAGB_HBB_NeNa_CYCLE

#ifdef NUCSYN_NENA_LEAK
            // proton capture on Na23 links the two cycles
            ddN=N[XNa23]*(1.0-exp(-dtseconds/Nuclear_burning_timescale(SIGMAV_T23P)));
            N[XNa23] -= ddN;
            N[XMg24] += ddN;
            dH1-=ddN;

            double nena_branching_ratio=sigmav[SIGMAV_T23P]/sigmav[SIGMAV_T23];

            //#define NUCSYN_NENA_LEAK_DEBUG
#ifdef NUCSYN_NENA_LEAK_DEBUG
            if(nena_branching_ratio>0.5)
            {
                /*
                 * SIGMAV_T23P = Na23(pg)Mg24
                 * SIGMAV_T23 = Na23(pa)Ne20
                 *
                 * With NACRE rates, Na23(pa)Ne20 >> Na23(pg)Mg24
                 * But with new Iliadis rates this is not the case?
                 */
                fprintf(stderr,"NeNa branch %g > 0.5! (%g/%g) log(T)=%g\n",
                        sigmav[SIGMAV_T23P]/sigmav[SIGMAV_T23],
                        sigmav[SIGMAV_T23P],sigmav[SIGMAV_T23],
                        T
                    );
            }
#endif
#endif

#ifdef NUCSYN_TPAGB_HBB_MgAl_CYCLE
            if(T>T_MIN_MgAl_HBB)
                //if(1==1)
            {
                if(sigmavset==FALSE) nucsyn_set_sigmav(stardata,
                                                       stardata->store,
                                                       logT,
                                                       sigmav,
                                                       stardata->preferences->reaction_rate_multipliers
#ifdef RATES_OF_AMANDA_NE22PG_FIX
                                                       ,cf88_fix
#endif
                    );

                timescales[0]=Nuclear_burning_timescale(SIGMAV_T24);

#ifdef NUCSYN_Al26m
                /*
                 * Allow for metastable state : change t25 to include both (for
                 * destruction of Mg25, and for later use of the Mg25 expression)
                 * and separately include Al26g and extra Mg26 from the decay
                 * of Al26m
                 */
                timescales[5]=Nuclear_burning_timescale(SIGMAV_T25);
                timescales[6]=Nuclear_burning_timescale(SIGMAV_T25m);
                timescales[1]=1.0/(1.0/timescales[5]+1.0/timescales[6]);
#else
                timescales[1]=Nuclear_burning_timescale(SIGMAV_T25);
#endif

                timescales[2]=Nuclear_burning_timescale(SIGMAV_T26P);
                timescales[3]=sigmav[SIGMAV_TBETA26];
                timescales[4]=Nuclear_burning_timescale(SIGMAV_T26);

                Dprint("MGAL timescales (1/X=%g) %g %g %g %g %g\n",
                       invNH1,
                       timescales[0],
                       timescales[1],
                       timescales[2],
                       timescales[3],
                       timescales[4]);

#ifndef TTEST
                dtseconds=dt*YEAR_LENGTH_IN_SECONDS;
#endif

                /* set up number densities */
                Nin[0]=0.0; // source term from NeNa - neglect
                Nin[1]=N[XMg24];
                Nin[2]=N[XMg25];
                Nin[3]=N[XMg26];
                Nin[4]=N[XAl26];
                Nin[5]=N[XAl27];

                nucsyn_anal_MgAl_burn(stardata,timescales,Nin,dtseconds);

                if((isnan(Nin[1])==0)&&(isnan(Nin[2])==0)&&(isnan(Nin[3])==0)&&
                   (isnan(Nin[4])==0)&&(isnan(Nin[5])==0))
                {
                    N[XMg24]=Nin[1];
                    N[XMg25]=Nin[2];
                    N[XMg26]=Nin[3];
                    N[XAl26]=Nin[4];
                    N[XAl27]=Nin[5];
                }

#ifdef NUCSYN_MGAL_LEAKBACK
                // proton capture on Al27 leads to Mg24 SIGMAV_T27a
                ddN=N[XAl27]*(1.0-exp(-dtseconds/(invNH1/sigmav[SIGMAV_T27a])));
                N[XAl27] -= ddN;
                N[XMg24] += ddN;
                dH1-=ddN;
#endif

                dH1-=(MEAN_ABUND(XMg24)/Nuclear_burning_timescale(SIGMAV_T24)+
                      MEAN_ABUND(XMg25)/Nuclear_burning_timescale(SIGMAV_T25)+
                      MEAN_ABUND(XMg26)/Nuclear_burning_timescale(SIGMAV_T26)+
                      MEAN_ABUND(XAl26)/Nuclear_burning_timescale(SIGMAV_T26P));
            }


#endif // NUCSYN_TPAGB_HBB_MgAl_CYCLE

        }

        /*
         * Burn hydrogen
         */
        /*
          dH1*=dtseconds;
          N[XH1]+=dH1;
          N[XHe4]-=dH1*0.25;
        */


        // fudge! if N[XH1] gets very small we get 1/0 errors - don't let it happen!
        if(N[XH1]<0.0)
        {
            N[XH1]=1e10;
        }

#ifdef TTEST
        ddN=N[XC12]+N[XC13]+N[XN14]+N[XO16]+N[XO17];
        A=N[XNe20]+N[XNe21]+N[XNe22]+N[XNa23];
        B=N[XMg24]+N[XMg25]+N[XMg26]+N[XAl26]+N[XAl27];
        printf("TTEST %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g\n",
               T,
               N[XC12]/ddN,
               10*N[XC13]/ddN,
               N[XN14]/ddN,
               10*N[XN15]/ddN,
               N[XO16]/ddN,
               10*N[XO17]/ddN,
               (N[XC12]+N[XC13])/(N[XO16]+N[XO17]),
               N[XNe20]/A,
               1e2*N[XNe21]/A,
               N[XNe22]/A,
               1e3*N[XNa23]/A,
               N[XMg24]/B,
               N[XMg25]/B,
               N[XMg26]/B,
               N[XAl26]/B,
               N[XAl27]/B
            );
    }
    Exit_binary_c(BINARY_C_NORMAL_EXIT,"");
#endif

#endif // NUCSYN_ANAL_BURN

}



#endif // NUCSYN



#ifdef NUCSYN_TPAGB_HBB_CLAYTON_TABLE
/* The table in Clayton P393 */

void nucsyn_clayton_table(double *mnuc)
{

/* Uncomment this block of code in order to run the hbb routine through a
 * loop over different temperatures. Use this in conjuction with the
 * "end do" at the end of this function and the "CLAYTON" section below
 * to make a table just like Clayton's (== C+F's) table on page 393
 * of Clayton's nice book.
 */
    double t6=4.00,t;
    double sigmav[15];
//      loop over t6
    while(t6<100.00)
    {
        if(t6<20)
        {
            t6++;
        }
        else if(t6<30.0)
        {
            t6+=2.0;
        }
        else
        {
            t6+=5.0;
        }
        t=log10(t6*1.0e6); // log10 of T (in kelvin!)
        nucsyn_set_sigmav(stardata,
                          stardata->store,
                          t,
                          sigmav,
                          stardata->preferences->reaction_rate_multipliers
#ifdef RATES_OF_AMANDA_NE22PG_FIX
                          ,cf88_fix
#endif

            );

/* p393 table, just uncomment this and the "t6 testing loop" below
 * DO NOT DELETE - I need this code to prove that what I am doing
 * is ok! ;-)
 */

        printf("% 3.2f % 3.2f % 3.2f % 3.2f % 3.2f % 3.2f % 3.2f\n",
               t6,
               log10(mnuc[XH1]/(100.0*sigmav[1]*YEAR_LENGTH_IN_SECONDS)),
               log10(mnuc[XH1]/(100.0*sigmav[5]*YEAR_LENGTH_IN_SECONDS)),
               log10(mnuc[XH1]/(100.0*sigmav[6]*YEAR_LENGTH_IN_SECONDS)),
               log10(mnuc[XH1]/(100.0*sigmav[2]*YEAR_LENGTH_IN_SECONDS)),
               log10(mnuc[XH1]/(100.0*sigmav[10]*YEAR_LENGTH_IN_SECONDS)),
               log10(mnuc[XH1]/(100.0*sigmav[7]*YEAR_LENGTH_IN_SECONDS)));
    }
    Exit_binary_c(BINARY_C_NORMAL_EXIT,"");
}


#endif
