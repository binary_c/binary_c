#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN
/*
 * A logging function which is to be output every timestep so should be
 * *short*! Best to just include XH,XHe,XC12,XC13,XN14,XO16,NC/NO
 * The header for the log is NUCSYNs__ (s for short)
 */

/*
 * There are several arrays you can use: Xenv the
 * current values (which might be wrong!) and Xsurfs which are always right
 */
#define XARRAY Xobs
#define XARRAY2 Xobs2
#undef USE_OLD_R_STAR_LOG

/* C/O number ratio */
#define NCORATIO Max(-10.0,\
                     ((XARRAY[XC12]/stardata->store->mnuc[XC12]+\
                       XARRAY[XC13]/stardata->store->mnuc[XC13])/\
                      (TINY+                                     \
                       XARRAY[XO15]/stardata->store->mnuc[XO15]+ \
                       XARRAY[XO16]/stardata->store->mnuc[XO16]+ \
                       XARRAY[XO17]/stardata->store->mnuc[XO17])))

/* do you want to log10 them? */
//#define LOGG10 log10

/* A macro to calculate the effective temperature */
//#define TEFF_LOG(A,B) (1000.0*pow((1130.0*stardata->star[(A)].luminosity/Pow2((B))),0.25))

#define TEFF_LOG(A,B) (5800.0*pow((stardata->star[(A)].luminosity/((B)*(B))),0.25))

#define LOGG10 log10

#if (DEBUG==1)
/* #define all the useful things for debugging */
#ifndef NUCSYN_SHORT_LOG
#define CHECK_DEBUG_EXP
#endif
#define NUCSYN_SHORT_LOG
#define NUCSYN_STRUCTURE_LOG
#define NUCSYN_S_PROCESS_LOG
#define NUCSYN_XTOT_CHECKS
#endif /* DEBUG */

#if defined NUCSYN_SHORT_LOG || defined USE_OLD_R_STAR_LOG
static inline double Maybe_unused nucsyn_teff(struct stardata_t * Restrict const stardata,
                                              struct star_t * const star);

#endif

void nucsyn_short_log(struct stardata_t * Restrict const stardata)
{
#ifdef CHECK_DEBUG_EXP
    if(!(Debug_expression)) return;
#endif

#ifdef NUCSYN_R_STAR_LOG
    int o;
    double teff;
    Boolean already_logged_k_star=FALSE;
    Boolean already_logged_r_star=FALSE;
    Boolean already_logged_c_star=FALSE;

#endif
#if defined NUCSYN_R_STAR_LOG || DEBUG==1
    double *Xobs,*Xobs2;
#endif // NUCSYN__R_STAR_LOG
#ifdef NUCSYN_XTOT_CHECKS
    Abundance Xtot;
#endif /* NUCSYN_XTOT_CHECKS */

    Dprint("in nucsyn_short_log\n");

    if(stardata->common.nucsyn_short_log_count==0)
    {
        stardata->common.nucsyn_short_log_count=1;
#ifdef NUCSYN_SHORT_LOG
        printf("NUCSYNs__#Time star1: type,XH,XHe,XC12,XC13,XN14,XO16,NC/NO,Teff,Lum,Mass star2: type,XH,XHe,XC12,XC13,XN14,XO16,NC/NO,Teff,Lum,Mass <s-proc>\n");
#endif
    }

#ifdef NUCSYN_R_STAR_LOG

    o=0;
    Foreach_star(star)
    {
        /* R-Stars */
        /* Simulate the observations of Dominy 1984 */

        Xobs = (Abundance*) nucsyn_observed_surface_abundances(star);


#ifdef USE_OLD_R_STAR_LOG

        /* Choose only HG, GB and HeHG, HeGB stars */
        /* because we require the luminosity to be high (~100Lsun) */
        /* but not as high as AGB stars (~1000Lsun) */
        /* perhaps CHeB stars? */
        if((star->stellar_type==HG)||
           (star->stellar_type==GIANT_BRANCH)||
           (star->stellar_type==CHeB)||
           (star->stellar_type==HeHG)||
           (star->stellar_type==HeGB))
        {

            if(o==0)
            {
                printf("NUCSYNR__%d %g ",
                       stardata->model.sgl,
                       stardata->model.time-stardata->common.lastcallrlog);
                o=1;
            }


            printf("%d %8.8g %8.8g %8.8g %8.8g %8.8g %8.8g %8.8g %8.8g %8.8g %8.8g %8.8g %8.8g %8.8g %8.8g ",
                   //printf("kw=%d nco=%8.8g teff=%8.8g L=%8.8g [Ba]=%8.8g X=%8.8g Y=%8.8g C12/C13=%8.8g [C12]=%8.8g [N14]=%8.8g [O16]=%8.8g O16/O17=%8.8g (Mg25+Mg26)/Mg24=%8.8g [Ti]=%8.8g g=%8.8g ",
                   star->stellar_type,
                   NCORATIO,
                   nucsyn_teff(stardata,star),
                   star->spiky_luminosity,
                   log10(nucsyn_elemental_abundance("Ba",XARRAY,stardata,stardata->store)/
                         nucsyn_elemental_abundance("Ba",stardata->preferences->zero_age.XZAMS,stardata,stardata->store)),
                   XARRAY[XH1],
                   XARRAY[XHe4],
                   XARRAY[XC12]/(XARRAY[XC13]+TINY),
                   log10(XARRAY[XC12]/(stardata->preferences->zero_age.XZAMS[XC12])),
                   log10(XARRAY[XN14]/(stardata->preferences->zero_age.XZAMS[XN14])),
                   log10(XARRAY[XO16]/(stardata->preferences->zero_age.XZAMS[XO16])),
                   XARRAY[XO16]/XARRAY[XO17],
                   (XARRAY[XMg25]+XARRAY[XMg26])/(XARRAY[XMg24]),
                   log10((XARRAY[XTi48]+XARRAY[XTi50])/(stardata->preferences->zero_age.XZAMS[XTi48]+stardata->preferences->zero_age.XZAMS[XTi50])),
                   //GRAVITATIONAL_CONSTANT*M_SUN*star->mass/Pow2(star->radius*R_SUN)
                   star->mass/Pow2(star->radius)
                );

        }

        if(o==1)
        {
            printf("\n");
        }


#endif //USE_OLD_R_STAR_LOG

        // New R-star log

        // first, selection K-type stars
        // calculate effective temperature
        teff = 1000.0*pow((1130.0*star->luminosity/
                           Pow2(star->radius)),
                          0.25);
        teff=log10(teff);

        if(stardata->model.time<TINY)
        {
            // reset the last called
            stardata->common.lastcallrlog=0.0;
        }

        // Check for R-stars, observations from JL's web page
        // http://www.maths.monash.edu.au/~johnl/sins/Rstars/index.html
        /*
          printf("RSTAR check %d %g kw1=%d kw2=%d L1=%g L2=%g\n",i,(star->Xenv[XC12]/12.0+
          star->Xenv[XC13]/13.0)
          /
          (star->Xenv[XO16]/16.0+
          star->Xenv[XO17]/17.0+
          star->Xenv[XO18]/18.0),
          star->stellar_type,
          stardata->star[Other_star(i)].stellar_type,
          star->luminosity,
          stardata->star[Other_star(i)].luminosity
          );
        */
        // star must be evolved but NOT a white dwarf
        if(star->stellar_type<10)
        {

            if(
                // Teff 4000-5000 K
                ((teff<log10(5500))&&
                 (teff>log10(3800)))
                // carbon star
                &&((star->Xenv[XC12]/12.0+
                    star->Xenv[XC13]/13.0)
                   /
                   (star->Xenv[XO16]/16.0+
                    star->Xenv[XO17]/17.0+
                    star->Xenv[XO18]/18.0)>1.0)
                // L ~ 100 Lsun ?
                &&((star->luminosity>10)&&
                   (star->luminosity<300))
                &&(already_logged_r_star==FALSE)
                )
            {
                already_logged_r_star=TRUE;
                printf("RSTAR__%d %g %d %d %g %g\n",i,
                       stardata->model.time-stardata->common.lastcallrlog,
                       stardata->model.sgl,
                       star->stellar_type,
                       star->mass,
                       ((star->Xenv[XC12]/12.0+
                         star->Xenv[XC13]/13.0)
                        /
                        (star->Xenv[XO16]/16.0+
                         star->Xenv[XO17]/17.0+
                         star->Xenv[XO18]/18.0))
                    );
            }

            // Carbon giant (N-star)
            if((GIANT_LIKE_STAR(star->stellar_type))&&

               ((star->Xenv[XC12]/12.0+
                 star->Xenv[XC13]/13.0)
                /
                (star->Xenv[XO16]/16.0+
                 star->Xenv[XO17]/17.0+
                 star->Xenv[XO18]/18.0)>1.0)
               &&(already_logged_c_star==FALSE)
                )
            {
                already_logged_c_star=TRUE;
                printf("CSTAR__%d %g\n",i,
                       stardata->model.time-stardata->common.lastcallrlog);
            }

            // log G and K giants
            if(
                // don't log twice
                (already_logged_k_star==FALSE)&&
                // giant-like star
                (GIANT_LIKE_STAR(star->stellar_type))
                // G or K type i.e. Teff in the range 5150 to 3950
                &&((teff<log10(5150))&&(teff>log10(3950)))
                // impose a luminosity limit?
                &&(star->luminosity>100) // this is really nice!
                )
            {
                printf("KSTAR__%d %g\n",i,
                       stardata->model.time-stardata->common.lastcallrlog);
                /*
                 * we set already_logged_k_star to TRUE in order that
                 * we do not log each member of the binary system > once
                 * and get twice the contribution to the binary parameter space
                 */
                already_logged_k_star=TRUE;
            }

        }
    }
    stardata->common.lastcallrlog=stardata->model.time;

#endif /* NUCSYN_R_STAR_LOG */



#ifdef NUCSYN_SHORT_LOG
    printf("NUCSYNs__%8.8g ",stardata->model.time);
#endif /* NUCSYN_SHORT_LOG */

    Dprint("About to loop over stars...\n");

#ifdef NUCSYN_SHORT_LOG
    Foreach_star(star)
    {
        Xobs = (Abundance*) nucsyn_observed_surface_abundances(star);
        Dprint("\ni=%d XH1=%g\n",star->starnum,XARRAY[XH1]);
        printf("%d %8.8g %8.8g %8.8g %8.8g %8.8g %8.8g %8.8g %8.8g %8.8g %8.8g ",
               star->stellar_type,
               LOGG10(TINY+XARRAY[XH1]),
               LOGG10(TINY+XARRAY[XHe4]),
               LOGG10(TINY+XARRAY[XC12]),
               LOGG10(TINY+XARRAY[XC13]),
               LOGG10(TINY+XARRAY[XN14]),
               LOGG10(TINY+XARRAY[XO16]),
               NCORATIO,
               nucsyn_teff(stardata,star), /* effective temperature */
               star->luminosity,
               //star->spiky_luminosity,
               star->mass
            );


    }

/* s-process */
    /* Ba 24, 25 */
    Xobs = (Abundance*) nucsyn_observed_surface_abundances(&(stardata->star[0]));
    Xobs2 = (Abundance*) nucsyn_observed_surface_abundances(&(stardata->star[1]));

    printf("%8.8g %8.8g ",
           LOGG10(TINY+nucsyn_elemental_abundance("Ba",XARRAY,stardata,stardata->store)),
           LOGG10(TINY+nucsyn_elemental_abundance("Ba",XARRAY2,stardata,stardata->store)));

    /* Tc 26,27 */
    printf("%8.8g %8.8g ",
           LOGG10(TINY+nucsyn_elemental_abundance("Tc",XARRAY,stardata,stardata->store)),
           LOGG10(TINY+nucsyn_elemental_abundance("Tc",XARRAY2,stardata,stardata->store)));

    /* Zr 28,29 */
    printf("%8.8g %8.8g ",
           LOGG10(TINY+nucsyn_elemental_abundance("Zr",XARRAY,stardata,stardata->store)),
           LOGG10(TINY+nucsyn_elemental_abundance("Zr",XARRAY2,stardata,stardata->store)));

    /* Fe 30, 31*/
    printf("%8.8g %8.8g ",
           LOGG10(TINY+nucsyn_elemental_abundance("Fe",XARRAY,stardata,stardata->store)),
           LOGG10(TINY+nucsyn_elemental_abundance("Fe",XARRAY2,stardata,stardata->store)));

    // Separation 32
    printf("%8.8g ",stardata->common.orbit.separation);

    printf("\n"); /* end of short log */
#endif /* NUCSYN_SHORT_LOG */

    /* ************************************************************ */
#ifdef NUCSYN_XTOT_CHECKS
    /* Do some abundance checking */
    Foreach_star(star)
    {
        Xtot=0.0;
        Xobs = (Abundance*) nucsyn_observed_surface_abundances(star);
        Forward_isotope_loop(i)
        {
            if(Xobs[i]<0)
            {
                _printf("Star %d : Element %u has abundance %g which is <0!\n",
                        star->starnum,i,Xobs[i]);
                _printf("Full array listing:\n");
                Forward_isotope_loop(j)
                {
                    _printf("isotope %u : X = %g\n",j,
                           Xobs[j]);
                }
                Exit_binary_c(BINARY_C_NORMAL_EXIT,"nucsyn short log");
            }
            Xtot+=Xobs[i];
        }
        _printf("\nXtot%d=%12.12g ",star->starnum,Xtot);
        if(star->stellar_type < NS)
        {
            if(Xtot>1.1 || Xtot<0.9)
            {
                _printf("Xtot for star %d (stellar type %d) is more than 10%% in error\n",
                        star->starnum,
                        star->stellar_type);
#define COLUMN_OUTPUT

#ifdef COLUMN_OUTPUT
                _printf("Element : Mass fraction : Running total\n");
                Xtot=0.0;
                Forward_isotope_loop(i)
                {
                    Xtot+=Xobs[i];
                    _printf("%u : %g : %g\n",i,Xobs[i],Xtot);
                }
#endif
                Exit_binary_c(BINARY_C_NORMAL_EXIT,"nucsyn short log 2");
            }
        }
    }
    printf("\n");

#endif //NUCSYN_XTOT_CHECKS


#ifdef NUCSYN_STRUCTURE_LOG
    /* Structure */
    Foreach_star(star)
    {
        printf("NUCSYNstruct %d %d %g %g %g %g %g %g %g\n",
               star->starnum,
               star->stellar_type,
               stardata->model.time,
               star->mass,
               star->core_mass[CORE_He],
               star->radius,
               star->core_radius,
               //star->luminosity,
               star->spiky_luminosity,
               Teff(star->starnum)
            );
    }
#endif


#ifdef NUCSYN_HS_LS_LOG
    Foreach_star(star)
    {
        printf("NUCSYNHSLS %d %d %2.2f %2.2f %2.2f %2.2f\n",
               star->starnum,
               star->stellar_type,
               log10(TINY+S_process_overabundance(star->Xenv) ),
               S_process_hs_abundance(star->Xenv),
               S_process_ls_abundance(star->Xenv),
               S_process_hs_minus_ls(star->Xenv));
    }

#endif
    Dprint("Exiting nucsyn short log\n");
}

static inline double Maybe_unused nucsyn_teff(struct stardata_t * Restrict const stardata,
                                              struct star_t * const star)
{
    /*
     * Function to calculate the effective temperature
     */
    return(TEFF_LOG(star->starnum,star->radius));
}

#endif /* NUCSYN */
