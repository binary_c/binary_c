#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef NUCSYN

/*
 * Burn a nuclear network with given
 * cross sections (sigmav) and input abundances (Nin)
 * for a given time maxt (years).
 * The network code is given by *burnfunc, a pointer to
 * a function automatically generated by the
 * make_nuclear_network_code.pl script.
 *
 * If logfunc is non-NULL it is called with args N,t
 *
 */

//#define  NUCSYN_NETWORK_STATS
//#define NETWORK_DEBUG

/*
 * These factors determine how much you change the
 * timestep on success or failure. These have been
 * found by trial and error to reduce the number
 * of integration steps. They are significant when
 * it comes to the speed of the network, so keep
 * that in mind when changing them!
 */
#define TIMESTEP_RAISE_MULTIPLIER 1.2
#define TIMESTEP_LOWER_MULTIPLIER 0.8

/*
 * Intelligent timestep algorithm: these were chosen by
 * experiment on 1-6Msun stars with Z=0.02. They reduced
 * the total number of integration steps to a minimum
 * (although not the total number of error steps, which
 * of course can be zero or very small if only small timesteps
 * are used - in reality you want a compromise between the two).
 */
#define INTELLIGENT_RAISE_MULTIPLIER 2.2
#define INTELLIGENT_LOWER_MULTIPLIER 0.9

/* however, limit to some sensible changes */
#define MAX_RAISE 100.0
#define MIN_LOWER 0.01

/* Multiplier for dtguess, experiments show that 2.0 is fastest */
#define DTGUESS_MULTIPLIER 2.0

double nucsyn_network_burn(double * Restrict const Nin,
                           const double * Restrict const sigmav,
                           const double maxt,
                           nucsyn_burnfunc burnfunc,
                           nucsyn_logfunc logfunc,
                           const double dtguess,
                           struct stardata_t * const Restrict stardata,
                           const Reaction_network network_id,
                           const Boolean vb
    )
{
    /*
     * The initial timestep should probably be small, and will
     * be increased rapidly if there is a successful integration.
     *
     * If this routine has already been called for the relevant burning
     * cycle, the timestep on the previous call is used (this is dtguess),
     * up to a maximum of half the burning time (maxt - Shannon!).
     *
     * Otherwise, dt is one year or one tenth of maxt.
     *
     */
    double dt =
        dtguess > 1e-200
        ?
        Min(DTGUESS_MULTIPLIER*dtguess,0.5*maxt)
        :
        Min(0.1*maxt,1.0*YEAR_LENGTH_IN_SECONDS);

    const double mindt = 1e-100; // error if dt < mindt
    const double maxt2 = maxt + dt * 1e-8; // force correct time loop
    double t = 0.0,err,return_dt = dt;
    unsigned int success_count Maybe_unused = 0;
    unsigned int failure_count = 0;
    double dtr = dt;

    /* maybe we should output */
    if(logfunc!=NULL)
    {
        logfunc(stardata,Nin,t);
    }

    for(;(t<maxt2 && dt>0);) // NB dt=small at input, update dealt with below
    {
        /*
         * call the given burning function:
         * this returns the error relative
         * to the required threshold
         */
        err = burnfunc(stardata,
                       Nin,
                       sigmav,
                       Min(dt,dtr));

        if(err < 1.0)
        {
            /* error < threshold */
            success_count++;
            t += dt;

            /* choose a timestep for next time which is the minimum we require */
            return_dt = Min(return_dt,dt);

            /* maybe we should output */
            if(logfunc!=NULL)
            {
                logfunc(stardata,
                        Nin,
                        t);
            }

            /* adjust timestep */
            dt *= Min(MAX_RAISE,
                      INTELLIGENT_RAISE_MULTIPLIER  * (-log10(err)));
        }
        else
        {
            /* err > threshold */

            /* failed to find solution, reduce timestep */
            failure_count++;

            /* failure : reduce dt */
            dt *=  Max(MIN_LOWER,INTELLIGENT_LOWER_MULTIPLIER / err);
        }

        /*
         * Should we allow for really, really small timesteps? Probably not.
         */
        if(t+dt < maxt2 &&
           dt < mindt)
        {
            Backtrace;
            Exit_binary_c(BINARY_C_OUT_OF_RANGE,
                          "nucsyn_network_burn: network %u = \"%s\" dt=%g too small error\n",
                          network_id,
                          nuclear_network_strings[network_id],
                          dt);
        }

        /*
         * Make sure we don't overrun the end of the burn time
         */
        dtr = Min(dt,
                  maxt2-t);

        if(vb)Dprint("dt=%12.12e -> err=%g\n",dt,err);

    }

#ifdef NUCSYN_NETWORK_STATS
    stardata->common.nucsyn_network_total_failure_count+=failure_count;
    stardata->common.nucsyn_network_total_success_count+=success_count;
    fprintf(stderr,"Successes %d (%d), failures %d (%d), total steps %d (%d), return_dt=%g\n",success_count,stardata->common.nucsyn_network_total_success_count,failure_count, stardata->common.nucsyn_network_total_failure_count,success_count+failure_count,stardata->common.nucsyn_network_total_success_count+stardata->common.nucsyn_network_total_failure_count,return_dt );
#endif


    if(failure_count>NUCSYN_NETWORK_BURN_MAX_FAILURE_COUNT)
    {
        fprintf(stderr,"warning: failure count %u > recommended max %u in nucsyn_network_burn\n",
                failure_count,
                (unsigned int)NUCSYN_NETWORK_BURN_MAX_FAILURE_COUNT);
    }

    return return_dt;
}

#endif
