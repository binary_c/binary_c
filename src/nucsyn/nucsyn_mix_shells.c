#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN

void nucsyn_mix_shells(const double m1,
                       Abundance * const Restrict X1,
                       const double m2,
                       Abundance * const Restrict X2)
{
    /* if shells are the same, just return */
    if(X1==X2) return;

    /* Mix the contents into shell 1 */
    nucsyn_dilute_shell(m1,X1,m2,X2);
  
    /* copy shell 1 abunds into shell 2 */
    Copy_abundances(X1,X2);
}

#endif // NUCSYN
