#include "../binary_c.h"
No_empty_translation_unit_warning;
#if defined NUCSYN && \
    defined NUCSYN_NETOWRK_HOTCNO && \
    defined NUCSYN_NETWORK_COLDCNO

#ifdef __THIS_CODE_IS_NOT_THREAD_SAFE
static void output_function_hotCNO(struct stardata_t * stardata,
                                   const double * Restrict Nin,
                                   const double t);
static void output_function_coldCNO(struct stardata_t * stardata,
                                    const double * Restrict Nin,
                                    const double t);
//static void output_function_NeNaMgAl(struct stardata_t * stardata,const double * Restrict Nin,const double t);

void nucsyn_network_test(struct stardata_t *stardata)
{
    /*
     *  Burning network experiments
     */
    double t9=0.125; // T/10^9
    double rho=1.0; // density
    static double THREAD_LOCAL Nin[ISOTOPE_MEMSIZE];
    static double THREAD_LOCAL Norig[ISOTOPE_MEMSIZE];
    unsigned int i;

    /* Initial abundances */
    X_to_N(stardata->store->imnuc,
           rho,
           Nin,
           stardata->star[0].Xenv,
           ISOTOPE_ARRAY_SIZE);

    Isotope_loop(i)
    {
        /* Make sure there is something in the minor species:
         * This reduces the stiffness of the equations but hardly
         * changes the results.
         */
        Nin[i]=Max(Nin[i],1e5*rho);
        Norig[i]=Nin[i];
    }

    /*
     * Calculate nuclear reaction rates
     */
    static double THREAD_LOCAL sigmav[SIGMAV_SIZE];
    nucsyn_set_sigmav(stardata,
                      log10(t9)+9,
                      sigmav,
                      stardata->preferences->reaction_rate_multipliers
#ifdef RATES_OF_AMANDA_NE22PG_FIX
                      ,FALSE
#endif
        );

    double maxt=1.0;

    /* (cold) CNO cycle */
    nucsyn_network_burn(Nin,sigmav,maxt,&nucsyn_burn_kaps_rentrop_coldCNO,
                        &output_function_coldCNO,1,stardata);


    Copy_abundances(Norig,Nin);

    /* (hot) CNO cycle */
    nucsyn_network_burn(Nin,sigmav,maxt,&nucsyn_burn_hotCNO,
                        //NULL);
                        &output_function_hotCNO,1,stardata);

    /* NeNa and MgAl chains */

    // only NeNa
    //nucsyn_network_burn(Nin,sigmav,maxt,&nucsyn_burn_NeNa,&output_function_NeNaMgAl,1,stardata);

    // old-fashioned NeNa / MgAl separately
    //nucsyn_network_burn(Nin,sigmav,maxt,&nucsyn_burn_NeNaMgAlnoleak,&output_function_NeNaMgAl,1,stardata);

    // high-tech NeNa and MgAl combined
    //nucsyn_network_burn(Nin,sigmav,maxt,&nucsyn_burn_kaps_rentrop_NeNaMgAl,&output_function_NeNaMgAl);

    Exit_binary_c(BINARY_C_NORMAL_EXIT,"Network test finished\n");
}


static void output_function_coldCNO(struct stardata_t * stardata,
                                    const double *Nin,
                                    const double t)
{
#define LOGISO log10
    //#define LOGISO /**/
    static double THREAD_LOCAL prevt=0.0;
    if(t>prevt*1.05)
    {
        Printf("coldCNO %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g\n",
               Max(log10(t),-12),
               LOGISO(Nin[XH1]),
               LOGISO(Nin[XHe4]),
               LOGISO(Nin[XC12]),
               LOGISO(Nin[XC13]),
               LOGISO(Nin[XN13]),
               LOGISO(Nin[XN14]),
               LOGISO(Nin[XN15]),
               LOGISO(Nin[XO14]),
               LOGISO(Nin[XO15]),
               LOGISO(Nin[XO16]),
               LOGISO(Nin[XO17]),
               LOGISO(Nin[XO18]),
               LOGISO(Nin[XF17]),
               LOGISO(Nin[XF18]),
               LOGISO(Nin[XF19])
            );
        prevt=t;
        fflush(stdout);
    }
}

static void output_function_hotCNO(struct stardata_t * stardata,
                                   const double *Nin,
                                   const double t)
{
#define LOGISO log10
    //#define LOGISO /**/
    static double THREAD_LOCAL prevt=0.0;
    if(t>prevt*1.05)
    {
        Printf("hotCNO %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g\n",
               Max(log10(t),-12),
               LOGISO(Nin[XH1]),
               LOGISO(Nin[XHe4]),
               LOGISO(Nin[XC12]),
               LOGISO(Nin[XC13]),
               LOGISO(Nin[XN13]),
               LOGISO(Nin[XN14]),
               LOGISO(Nin[XN15]),
               LOGISO(Nin[XO14]),
               LOGISO(Nin[XO15]),
               LOGISO(Nin[XO16]),
               LOGISO(Nin[XO17]),
               LOGISO(Nin[XO18]),
               LOGISO(Nin[XF17]),
               LOGISO(Nin[XF18]),
               LOGISO(Nin[XF19])
            );
        prevt=t;
        fflush(stdout);
    }
}

/*
  static void output_function_NeNaMgAl(const double *Nin,const double t)
  {
  #define LOGISO log10
  //#define LOGISO
  printf("NeNa %g %g %g %g %g %g %g %g %g %g %g %g %g\n",
  Max(log10(t),-12),
  LOGISO(Nin[XH1]), //2
  LOGISO(Nin[XHe4]),
  LOGISO(Nin[XNe20]),//4
  LOGISO(Nin[XNe21]),
  LOGISO(Nin[XNe22]),
  LOGISO(Nin[XNa23]),
  LOGISO(Nin[XMg24]),//8
  LOGISO(Nin[XMg25]),
  LOGISO(Nin[XMg26]),
  LOGISO(Nin[XAl26]),//11
  LOGISO(Nin[XAl27]),//12
  LOGISO(Nin[XSi28]//13
  )
  );
  }
*/
#endif
#endif
