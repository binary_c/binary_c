#pragma once
#ifndef NUCSYN_WR_TYPES_CODES_H
#define NUCSYN_WR_TYPES_CODES_H

#include "nucsyn_WR_types.def"

/*
 * Construct WR type codes
 */
#undef X
#define X(CODE) CODE,
enum { NUCSYN_WR_TYPES_CODES };

#undef X
#define X(CODE) #CODE,
static char * NUCSYN_WR_TYPES_code_macros[] Maybe_unused = { NUCSYN_WR_TYPES_CODES };

#undef X
#define X(CODE) #CODE
static char * NUCSYN_WR_TYPES_code_strings[] Maybe_unused = { NUCSYN_WR_TYPES_CODES };

#define NUCSYN_WR_TYPES_string(N) Array_string(NUCSYN_WR_TYPES_code_strings,(N))

#undef X

#endif // NUCSYN_WR_TYPES_CODES_H
