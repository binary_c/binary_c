#ifndef NUCSYN_LOG_H
#define  NUCSYN_LOG_H

#define OUTPUT stdout
#define HEADER headstring
#define HEAD_STRING "NUCSYN%d__",star->starnum
/*
 * This is the definition of what is output in the log, you may want to put a
 * log10 in here
 */
#define ABUND(A) (log10(Max(Xabund[A],1e-30)))
#define XRATIO(A,B) (log10((Xabund[(A)])/(Xabund[(B)])))
#define XCORATIO (log10((Xabund[XC12]+Xabund[XC13])/\
                        (Xabund[XO15]+Xabund[XO16]+Xabund[XO17])))

/* number ratios! */
#define NRATIO(A,B) ((Xabund[(A)]/stardata->store->mnuc[(A)])/\
                     (TINY+Xabund[(B)]/stardata->store->mnuc[(B)]))

#define NCORATIO ((Xabund[XC12]/stardata->store->mnuc[XC12]+\
                   Xabund[XC13]/stardata->store->mnuc[XC13])/   \
                  (TINY+                                        \
                   Xabund[XO15]/stardata->store->mnuc[XO15]+    \
                   Xabund[XO16]/stardata->store->mnuc[XO16]+    \
                   Xabund[XO17]/stardata->store->mnuc[XO17]))

#define NUM_INDICES 13
/* only log STAR_TYPE */
#define STAR_TYPE TPAGB

#define FLUSH /* fflush(OUTPUT); */

#endif // NUCSYN_LOG_H
