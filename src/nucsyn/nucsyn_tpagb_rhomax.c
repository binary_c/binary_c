#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN

Constant_function double nucsyn_tpagb_rhomax(const double m0,
                                      const Abundance z)
{
    /* Maximum value of the density on the TPAGB */
    double rhomax;
    rhomax = (3.05860+9.51050*pow(m0,-7.20180e-01)-3.65330*log10(z/0.02));
    rhomax = Max(0.0,rhomax);
    return rhomax;
}

#endif // NUCSYN

