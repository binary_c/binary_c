#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN

/*
 * Given an array of abundances X, renormalize
 * the total to 1.0 by scaling them.
 *
 * Note that if the sum is zero, do nothing, as we
 * cannot normalize (this is better than NaN).
 */
void nucsyn_renormalize_abundance(Abundance * Restrict const X)
{
    if(X != NULL)
    {
        const double Xsum = nucsyn_totalX((const Abundance * const)X);
        if(Is_really_not_zero(Xsum) && !Fequal(1.0,Xsum))
        {
            Xmult(X,1.0/Xsum);
        }
    }
}

#endif //NUCSYN
