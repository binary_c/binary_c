#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Surface chemistry of high mass O/B and Wolf-Rayet stars in the range
 * 10<M/Msun<100. Fits based on Z=0.02, MM mass loss models of Lynnette Dray
 * (see Lynnette Dray's thesis or Dray et al 2003) but with corrections to deal
 * with NL mass loss and approximate other metallicities.
 *
 * The fits are phenomenological and while reasonably accurate for
 * X,Y,C12,N14,O16 and Ne22 (which Lynnette called "Ne20") are only approximate
 * for the minor isotopes. This is generally ok because GB/AGB stars are the
 * main contributors... but (e.g. F19) might not be in reality. Also, the fits
 * are to the "ZAMS mass" which can be redefined if mass is accreted on the
 * main sequence... The phenomenological nature of the fits makes them rather
 * complicated - but the computer does not care. You might and if you can, for
 * example, get the HBB code to work for WR stars then that would be *much*
 * better. However, I cannot, at present, so this approximation will have to
 * do. NUCSYN_WR must be defined for this function to be activated.
 * NUCSYN_WR_METALLICITY_CORRECTIONS must be defined for the Z!=0.02 code to be
 * activated.
 */

#ifdef NUCSYN
#include "nucsyn_WR.h"

/************************************************************/

void nucsyn_WR(struct star_t * const star,
               struct stardata_t * Restrict const stardata,
               const double stellar_mass Maybe_unused)
{
#ifdef NUCSYN_WR
#ifdef NUCSYN_WR_TABLES
    nucsyn_WR_table(stardata,star);
    return;
#else
    /*
     * x,y,zc,zn,zo are calculated surface abundances of H1, He4, C12, N14
     * and O16 prior to the values being set (i.e. they are temporary) zcno
     * is the sum of the CNO isotopes zc13,zo17,zo18,ne20,ne22,mg are also
     * isotopic abundances prior to being set. "metallicity" is the current
     * metallicity rather than the ZAMS metallicity. Other variables are
     * explained below...
     */
    Abundance x=0,y=0,zc=0,zn=0,zo=0,zcno=0,zc13=0,dzn=0,dzc=0,dzo=0,zo17=0,zo18=0,ne20,ne22,mg;
    Abundance dx1,dx2=0.0;
    double dmd1,dmd2=0.0,altdmd1;
    double f,s1,s1alt,s2=0.0;
    double f_z; // metallicity correction factor
    double a;
    double b;
    double c;
    double d;
    double e_c;
    double mlow;
    Abundance metallicity=1.0-star->Xenv[XH1]-star->Xenv[XH2]-star->Xenv[XHe4]-star->Xenv[XHe3];
    // NOT the ZAMS metallicity but the metallicity NOW!
    double gamma13;
    double f54;
#if (DEBUG==1)
    double dt;
#endif
    double dtfrac;
    double tHeMS; // helium star main sequence lifetime
    Abundance xsol; // x scaled to solar
    double ddx1,ddmd1;
    /* the ZAMS mass, or the current mass if accretion has occured */
    double effective_zams_mass=Max(stellar_mass,star->effective_zams_mass);
    double mcoterm; // terminal core mass for He stars
    Abundance yterm,zcterm,zoterm; // terminal abundances
    double m3,dum1;
    double Heaj;

    int ftype;
#if (DEBUG==1)
    //char *wr_type_strings[]={WR_TYPE_STRINGS};
#endif
    Dprint("\nWRWR star %d mass %g (mc=%g), type %d, depth=%g, age=%g (was X=%g Y=%g C=%g)\n",
           star->starnum,
           stellar_mass,
           Outermost_core_mass(star),
           star->stellar_type,
           DEPTH,star->age,
           star->Xenv[XH1],
           star->Xenv[XHe4],
           star->Xenv[XC12]
        );

    if(More_or_equal(star->Xenv[XC12],0.7))
    {
        /*
         * Problems with hugely stripped objects pretending to be white dwarfs
         */
        return;
    }


    Dprint("XLOG%d (%d m=%g DEPTH=%g) %g\n",
           star->starnum,
           star->stellar_type,
           star->mass,
           DEPTH,
           star->Xenv[XH1]
        );
    /* first set the fitted abundances to their current values */
    x=star->Xenv[XH1];
    y=star->Xenv[XHe4];
    zc=star->Xenv[XC12];
    zc13=star->Xenv[XC13];
    zn=star->Xenv[XN14];
    zo=star->Xenv[XO16];
    zo17=star->Xenv[XO17];
    ne20=star->Xenv[XNe20];
    ne22=star->Xenv[XNe22];
    mg=star->Xenv[XMg24];
    zcno=zc+zn+zo+zc13+zo17+star->Xenv[XO18]; // total CNO elements (by mass)

/*
 * Fits to mass co-ordinate for hydrogen stars which are not TPAGB stars, have
 * some stellar envelope and mass > 8.0 Msun
 */
    if(
        (star->stellar_type<7)&&(star->stellar_type!=6)
        &&(DEPTH>TINY)
        &&(star->mass>NUCSYN_WR_MASS_BREAK)
        )
    {
        Dprint("X>0.01, stellar type < 7 treatment \n");


        /*
         * The change in surface X is a Fermi function of magnitude dmd1 which
         * occurs at a depth dx1 - See Rob Izzard's thesis for details.
         */

        // f_M75
        f=fermi(effective_zams_mass,1.0,1.0,0.2,75); // f=0 for low mass, f=1 for high mass

        /*
         * calculate dM_RLOF_lose(0) - a contribution from low
         * mass (M<75) stars and a
         * contribution from high mass (M>75) stars
         */
        m3=Pow3(effective_zams_mass);
        dmd1=(1.0-f)*(2.5977-3.053e-1*effective_zams_mass+ // low mass
                      1.1911e-2*effective_zams_mass*effective_zams_mass+1.40440e-5*m3)+
            f*(1.6137e2-2.3087*effective_zams_mass+1.13350e-4*m3); // high mass

        // calculate dM_RLOF_lose(1)
        if(effective_zams_mass<20.0)
        {
            dmd1=Min(dmd1,
                     (2.86550e-01)+(-1.10370e-01)*effective_zams_mass+(8.88830e-03)*effective_zams_mass*effective_zams_mass);
            dmd1=Max(0.2,dmd1);
        }

        // set first dredge up though it's not really first dredge up
        if(DEPTH>dmd1) star->first_dredge_up=TRUE;

        // calculate dx1(0)
        if(effective_zams_mass<31)
        {
            dx1=2.6749e-2 +3.5589e-3*effective_zams_mass;

        }
        else
        {
            dx1=(1-f)*(2.7291e-1 -6.4511e-3*effective_zams_mass+7.45060e-8*pow(effective_zams_mass,4.0))+
                f*Max(0.74,6.9329e-1 -  0.020623242/effective_zams_mass);
#ifdef NUCSYN_WR_METALLICITY_CORRECTIONS
            // this is the factor f_Z(eq:dX1-Mlt31)
            dmd1*=1+0.3*Max(0.0,(metallicity-0.02)/0.02);
#endif
        }

        // Function type (ftype) and other factors (e.g. slope)
        if(effective_zams_mass<25)
        {
            ftype=-1; // single fermi function
            s1=1e-100; // s1(0)
            dx2=0.0;
        }
        else if(effective_zams_mass<38)
        {
            ftype=-2; // double fermi function
            s1=exp10((Max(-10.0, Min(-7.543e1+2.0927*effective_zams_mass,-2)))); // s1(0)
        }
        else
        {
            // s1(0)
            s1=(-2.93130e+01)+(2.76940e+02)/(effective_zams_mass-(2.71420e+01))+
                (5.27890e-01)*effective_zams_mass+(-2.59710e-03)*effective_zams_mass*effective_zams_mass;
            s1=exp10(-s1);
            ftype=2;

            if(effective_zams_mass<55)
            {
                ftype=2; // double log fermi function
            }
            else
            {
                ftype=1; // single log fermi function
                dx2=0.0;
            }
        }

        if(abs(ftype)==2)
        {
            // DX2, DMD2, S2
            // dx2(0)
            dx2=0.32+0.18/(1.0+pow(0.3,effective_zams_mass-42.5));
            // s2(0)
            s2=exp10(Max(-10.0,-7.5316e1+1.3218*effective_zams_mass));
            dmd2=1.4174e-1+5.5388e-1*effective_zams_mass;
        }


        if((effective_zams_mass>55)&&(effective_zams_mass<62))
        {
            // IS THIS NEEDED???
/* It would seem not - but if things break... please reactivate! */
//f=fermi(effective_zams_mass,2.0,1.0,0.2,57); // f=0 for low mass, f=1 for high mass
            // fM55 factor
            dx1*=1.0+(62.0-effective_zams_mass)/12.0;
        }

        // from the above calculate X as a function of DEPTH
        x=star->Xinit[XH1];

        // wind loss effects
        /* M<54 */
        // dM1(a)
        altdmd1=effective_zams_mass - star->start_HG_mass+
            Max(Max(0.0,(-7.09180e-01)+(5.61220e-02)*effective_zams_mass),
                (-1.25000e+01)+(3.50000e-01)*effective_zams_mass);
        // f_54
        f54=fermi(effective_zams_mass,
                  1.0,1.0,0.01,54.0);
        // M<54
        // dx1(1)
        dum1=1.0+(1.0-f54)*(altdmd1-dmd1)/20.0;
        dx1 *= dum1;
        // dx2(1)
        dx2 *= dum1;
        // dM_RLOF_lose(2)
        dmd1=(1-f54)*altdmd1+f54*dmd1;
        /* M>54 */
        // dx1(1) as well (one equation in thesis)
        dx1 += f54 * 2.0/(stellar_mass - Outermost_core_mass(star)+TINY);

        Dprint("Surface change : ftype=%d dmd1=%g dx1=%g s1=%g dmd2=%g dx2=%g s2=%g\n",
               ftype,dmd1,dx1,s1,dmd2,dx2,s2);

        // for small XH1 such that XH1-dx1(1)<0.1
        if(x-dx1<0.1)
        {
            // already defined?
            dmd2=1.4174e-1+5.5388e-1*effective_zams_mass;
            // DeltaXH1
            x +=
#ifdef NUCSYN_WR_METALLICITY_CORRECTIONS
                Max(1.0,(metallicity/0.02))*
#endif
                Max(0.0,(DEPTH-dmd2)*0.005*x);
        }

#ifdef NUCSYN_WR_METALLICITY_CORRECTIONS
        // metallicity corrections
        // factor which is 1 below 0.01 and zero above 0.02
        // f_Z
        f_z=1.0/(1+pow(0.1,(1e3*(0.015-metallicity))));
        // s1(a) gaussian
        s1alt=(f_z*6.357e-1*exp(-Pow2(effective_zams_mass-30.479)/109.61));
        // M_low
        mlow= 9.44010+514.990*metallicity;

        if(effective_zams_mass > mlow)
        {
            if(s1alt>0.1)s1=s1alt;

            if(effective_zams_mass<45)
            {
                // intermediate mass
                // \delta X1
                ddx1 = 0.75+0.05*effective_zams_mass;
                // \delta M1
                ddmd1 = -3.08+0.35*effective_zams_mass;
            }
            else
            {
                // high mass
                // \delta X1
                ddx1=-0.1;
                // \delta M1
                ddmd1=0.0;
                // \delta X2 =  - 0.5*f_z*dx2
                dx2 = dx2 - 0.5*f_z*dx2;
                // \delta M2 = - 9.515-472*metallicity;
                dmd2 -= 9.515-472.0*metallicity;
            }
        }
        else
        {
            // low mass stars M<mlow

            if(DEPTH>1.5)
            {
                // \delta X1
                ddx1 = 0.2 * (0.75 + 0.05*effective_zams_mass);
                // \delta M1
                ddmd1 =-3.08+0.35*effective_zams_mass;
                // s1(a)
                s1=s1alt;
            }
            else
            {
                // \delta M1
                ddmd1=+0.0;
                // \delta X1
                ddx1 = - 2.0*Pow3((metallicity/0.01))
                    /Pow2(mlow - effective_zams_mass);
            }
        }

        // dX1 from dX1(1) and others
        dx1 = dx1 *(1.0 + ddx1 * f_z);
        // dM1 from dM1(2) and others
        dmd1=dmd1 + ddmd1*f_z;
        // end of metallicity corrections
#endif // NUCSYN_WR_METALLICITY_CORRECTIONS
        if(ftype<0)
        {
            // linear fermi function
            x -= dx1/(1.0+pow(s1,DEPTH-dmd1));
            if(ftype==-2)
            {
                x -= dx2/(1.0+pow(s2,DEPTH-dmd2));
            }
        }
        else
        {
            // (natural) log fermi function
            x -= dx1/(1.0+pow(s1,log(DEPTH)-log(dmd1)));
            if(ftype==2)
            {
                x -= dx2/(1.0+pow(s2,log(DEPTH)-log(dmd2)));
            }
        }

#ifdef NUCSYN_WR_METALLICITY_CORRECTIONS
        // another metallicity correction
        x=Max(
            Min(0.2,
                (1+9.52700e-03*effective_zams_mass-1.11420e-04*Pow2(effective_zams_mass))*
                (1+-1.39250e+01*metallicity)*0.16*f_z
                ),
            x);

#endif // NUCSYN_WR_METALLICITY_CORRECTIONS

        if((dmd2>TINY)&&(DEPTH>dmd2)
           &&(effective_zams_mass<45
#ifdef NUCSYN_WR_METALLICITY_CORRECTION
              // metallicity correction to force change at lower mass
              *metallicity/0.02
#endif
               )
            ) //45
        {
            // final bump
            x -= Max(0.0,(DEPTH-dmd2)*0.5*x);
        }

        x=Max(0.0,Min(star->Xinit[XH1],Min(Max(0.0,x),star->Xenv[XH1]))); // MUST decrease


        metallicity=1.0-star->Xenv[XH1]-star->Xenv[XHe4];
        y = 1.0 - x - metallicity;
        Dprint("XLOG%d xinit=%g dx1=%g x=%g from effective_zams_mass=%g\n",
               star->starnum,
               star->Xinit[XH1],
               dx1,
               x,
               effective_zams_mass);

        // need a correction as we approach the He core - this is important
        // to give a smooth transition from an HG star to a He star in binaries
        // NB the 1.0 is quite arbitrary
        if(DELTA_CORE<1.0)
        {
            y=Min(1.0-metallicity,Max(1.0-metallicity-DELTA_CORE,y));
        }

        //if(fabs(star->Xinit[XH1] - x) > 0.01)
        if(Abs_more_than(star->Xinit[XH1]-x,0.01))
        {
            // fit Oxygen to hydrogen

            // coeffs
            a=(-9.4e-2+1.1468e-1/(1.0+ pow(0.01,0.19394*effective_zams_mass-6.8227)) );
            b=(3.2732-4.8939e-2*effective_zams_mass+3.7616e-4*effective_zams_mass*effective_zams_mass);
            c=(18.88-1.1831*exp(-Pow2((log(effective_zams_mass)-log(17.8)))/0.5076));
            d=(-13.936-0.68025*exp(-Pow2((log(effective_zams_mass)-log(16.0)))/0.97626));
            e_c=(0.8064-0.81104/(1.0+pow(1.4638,-1.1967*effective_zams_mass+55.336))+1.7509e-7*effective_zams_mass*effective_zams_mass*effective_zams_mass);
            f=(0.70766+exp(3.7462e-2*(effective_zams_mass-82.122)));

            //oxygen is now a smooth function of hydrogen (scaled to solar value 0.7)
            xsol=0.7*(x/star->Xinit[XH1]);
            zo=a+pow(b,c*xsol+d)+e_c*pow(xsol,f);

            // correction factor : cubic + gaussian
            zo *= 8.48070e-01+1.97410e-02*effective_zams_mass-4.32430e-04*effective_zams_mass*effective_zams_mass
                +2.55740e-06*effective_zams_mass*effective_zams_mass*effective_zams_mass
                -4.42660e-01*exp(-Pow2(effective_zams_mass-
                                       40) /1.14160e+02);
            /* might dip below zero - prevent this! */
            zo = Max(0,zo);

            // fit carbon to oxygen
            if(effective_zams_mass < 25.19)
            {
                a=3.8576 -3.0414e-1*effective_zams_mass+6.295e-3*effective_zams_mass*effective_zams_mass;
                b=-1.2725e1+1.0165*effective_zams_mass-2.1193e-2*effective_zams_mass*effective_zams_mass;
            }
            else if(effective_zams_mass < 46.21)
            {
                a=-1.4495e-2+6.7851e-4*effective_zams_mass;
                b=-6.3494e-1+5.5562e-2*effective_zams_mass-9.4467e-4*effective_zams_mass*effective_zams_mass;
            }
            else if(effective_zams_mass < 66.32)
            {
                a=1.119e-2+1.133e-4*effective_zams_mass;
                b=2.3992e-1-7.6394e-3*effective_zams_mass+3.0795e-5*effective_zams_mass*effective_zams_mass;
            }
            else
            {
                a=-2.325e-2+1.0086e-3*effective_zams_mass-5.467e-6*effective_zams_mass*effective_zams_mass;
                b=2.3992e-1-7.6394e-3*effective_zams_mass+3.0795e-5*effective_zams_mass*effective_zams_mass;
            }
            if(b<-0.423)
            {
                c=4.3012e-1-8.3078e-1*b;
            }
            else
            {
                c=4.6923e-1-1.3706*b;
            }

            /* calculate surface carbon, noting it must be > 0 */
            zc=a+b*zo+c*zo*zo;
            zc=Max(0,zc);

            /* make into "real" surface abundances */
            zo*=zcno;
            zc*=zcno;

            /*
             * nitrogen : conserve CNO : calculate change in C12 and O16, assume this
             * all goes into N14
             */
            dzc=zc-star->Xenv[XC12];
            dzo=zo-star->Xenv[XO16];
            dzn=14.0*(dzc/12.0+dzo/16.0);
            zn-=dzn;

            /* Carbon 13 - estimated from the models of Meynet and Maeder 1994 */
            if(x>0.3)
            {
                // gamma13 ~ 0.1 at low mass, <=0.001 at high mass
                gamma13=Max(0.0001,1.12540e-01*(1.0-1.00900/
                                                (1.0+pow(0.8,star->effective_zams_mass -3.20510e+01))));
                gamma13=Min(0.1,gamma13);
                if(zn/zcno > 0.5)
                {
                    // reduce C13 when N14 is the major CNO burning product
                    gamma13*=-2.0;
                }
#ifdef METALLICITY_CORRECTIONS
                // approximate metallicity correction
                gamma13 *= (0.02/metallicity);
#endif
                zc13 +=(-dzc) * gamma13;
                zc13=Max(0.0,zc13);
            }
        }
    } // non-helium star check

    Dprint("Post-hydrogen star : x=%g y=%g zc=%g\n",x,y,zc);


    // O18,O17 ~ O16  if X>0 (again from MM94)
    if(x>0.01)
    {
        zo18=2e-3*zo;
        zo17=6e-4*zo;
    }
    else
    {
        zo18=0.0;
        zo17=0.0;
    }


    // fluorine? tricky. see Meynet and Arnould Astron. Astrophys. 355, 176-180 (2000)
    // suggestion is overproduction in CHeB by factor ~ 50
    if(star->stellar_type==CHeB)
    {
        /*
         * if surface F19 is the same as ZAMS then increase it - note there
         * must be a better way to do this!
         */
        if(Fequal(star->Xenv[XF19],star->Xinit[XF19]))
        {
            star->Xenv[XF19]=star->Xinit[XF19]*50;
        }
    }
    // Al26 is destroyed during and after CHeB, assuming it exists at all!
    if(star->stellar_type>=4)
    {
        star->Xenv[XAl26]=0.0;
    }

    /*
     * Other NeNa? So damned complicated for a simple model like this! Currently
     * we just do not know without detailed models to predict...
     */

    // Now take into account the growth of the core as we approach the helium star phase
    if((star->stellar_type<7)&&((DELTA_CORE < 0.5)&&(DELTA_CORE>0)))
    {
        // linear drop to a helium star
        //zn = (DELTA_CORE) * (zn/2.5);
        /* same for C13 */
        zc13 = (DELTA_CORE) * (zc13/2.5);
        /* create Ne22 from N14, C13 and O18 */
        ne22 += 22.0*( (star->Xenv[XN14]-zn)/14.0 +
                       (star->Xenv[XC13]-zc13)/13.0+
                       (star->Xenv[XO18]-zo18)/18.0);
        zo18=0.0;
        zo17=0.0;
    }


    /*
     * check if the star has eaten past its hydrogen-burning shell
     * and yet is still an EAGB star... this should really (and usually is)
     * a helium star, but a check can't hurt
     */
    if((star->stellar_type==EAGB)&&(star->mass<star->max_EAGB_He_core_mass))
    {
        // h-burnt: all CNO->N, all H->He
        zn+=zc13+zc+zo18+zo+zo17;
        zc13=0;
        zc=0;
        zo18=0;
        zo17=0;
        zo=0;
        y+=x;
        x=0;
    }


    /* Helium stars! */
    if(star->stellar_type>=7)
    {
        Dprint("Helium star (x=%g y=%g zc=%g) t=%g tm=%g tn=%g (vs global tm=%g tn=%g)\n",x,y,zc,
               stardata->model.time,star->tm,star->tn,
               stardata->common.tm,
               stardata->common.tn

            );

        if(star->newhestar==TRUE)
        {
            /* star is a newly born helium star */
            star->newhestar=FALSE;
            // save the metallicity in the abscence of helium burning
            star->hezamsmetallicity=1.0-star->Xenv[XH1]-star->Xenv[XHe4];
            // reset He star age
            star->prevHeaj=0.0;
        }

        /*
         * Sometimes we're called with an age which is not realistic
         * so determine what the age should be
         */
        Heaj=Max(star->age,star->prevHeaj);
        tHeMS=Min(Heaj,star->tm);

        // not really Z, but used as Z
        if(star->hezamsmetallicity > -TINY)
            metallicity=star->hezamsmetallicity;

        x=0; y=0; zc=0; zo=0; zn=0; ne22=0;

        Dprint("HESTARs %d tms=%g age=%g (prevHeaj=%g, using Heaj=%g) tHeMS=%g\n",
               star->stellar_type,
               star->he_t,
               star->age,star->prevHeaj,Heaj,
               tHeMS
            );
        // Guess core mass for terminal abunds
        if(star->core_mass[CORE_CO]>0.0)
        {
            mcoterm=star->core_mass[CORE_CO];
        }
        else
        {
            // we don't have a core mass! so guess it
            // from the MS timescale and assume it grows
            // by a fixed amount during shell burning (10%)
            mcoterm=star->he_f*0.9*star->he_mcmax;
        }

        /*
         * The fits are over the range 1.7 to ~25 solar mass
         * (fitted to the core mass) so limit mcoterm to this range
         */
        mcoterm=Max(1.7,Min(25.0,mcoterm));

        /*
         * New fits to Lynnette's pre-SN models for Z=0.02.
         * These are roughly valid down to Z=0.001 but diverge
         * wildly (depending on the mass-loss) after that.
         * This is fitted to the terminal abundances for Z>=0.004
         * (The metallicity dependence is not too strong)
         */
        yterm=2.78710e+00*exp(-mcoterm*
                              (1+-7.97500e+00*Max(0.004,stardata->common.metallicity)))+1.04130e-01*
            pow(mcoterm,(3.28860e-01+5.28350e+00*Max(0.004,stardata->common.metallicity)));
        yterm=Max(0.1,Min(0.9,yterm)); // just in case something weird happens in a binary

        zcterm=(2.48660e-02)+(3.27300e+00)*yterm+
            (-8.75280e+00)*yterm*yterm+
            (5.80320e+00)*yterm*yterm*yterm+(3.02240e+00)*Max(0.004,stardata->common.metallicity);
        zcterm=Max(0.05,Min(0.475,zcterm)); // limit to range seen in the models
        zoterm=1-metallicity-yterm-zcterm;  // oxygen is everything else (roughly)

        /*
         * So now we have the terminal abundances, we assume they vary linearly
         * with time to the end of the HeMS.
         */
        dtfrac=Max(TINY,Min((Heaj-star->he_t)/(tHeMS-star->he_t),1.0));

        x=0; // no hydrogen
        y=(1.0-stardata->common.metallicity)*(1.0-dtfrac)+dtfrac*yterm;

        dtfrac=Pow2(dtfrac);
        zc=Max(star->Xenv[XC12],0*(1.0-dtfrac)+dtfrac*zcterm);

        dtfrac=Pow3(dtfrac);
        zo=Max(star->Xenv[XO16],0*(1.0-dtfrac)+dtfrac*zoterm);

        // convert CNO other than N14 to ne22
        ne22=ne22+zn+zo18+zo17+zc13;
        zn=0.0;
        zc13=0;
        zo18=0;
        zo17=0;

        Dprint("HESTAR frac=%g x=%g y=%g c=%g o=%g ne22=%g %g\n",dt,x,y,zc,zo,ne22,
               zc13+zc+zo+zn+ne22+x+y+zo18+zo17+mg+ne20);

        // c burning products
        // ne20 doesn't change (in first approximation!)
        // magnesium doesn't change at Z=0.02 - other metallicities to follow (perhaps)
        star->prevHeaj=star->age;
    }

    CLEANRANGE(x);
    CLEANRANGE(y);
    CLEANRANGE(zc);
    CLEANRANGE(zc13);
    CLEANRANGE(zn);
    CLEANRANGE(zo);
    CLEANRANGE(zo17);
    CLEANRANGE(zo18);
    CLEANRANGE(ne20);
    CLEANRANGE(ne22);
    CLEANRANGE(mg);

    star->Xenv[XH1]=x;
    star->Xenv[XHe4]=y;
    star->Xenv[XC12]=zc;
    star->Xenv[XC13]=zc13;
    star->Xenv[XN14]=zn;
    star->Xenv[XO16]=zo;
    star->Xenv[XO17]=zo17;
    star->Xenv[XO18]=zo18;
    star->Xenv[XNe20]=ne20;
    star->Xenv[XNe22]=ne22;
    star->Xenv[XMg24]=mg;



    /*
     * Force abundances to 1.0: we use either H, He or C depending on which
     * is most abundant and hence generate the least "error"
     */
    Dprint("preNORM st=%d Xsum=%g : H1=%g He4=%g C12=%g O16=%g Ne20=%g Ne22=%g \n",star->stellar_type,nucsyn_totalX(stardata,star->Xenv),
           star->Xenv[XH1],
           star->Xenv[XHe4],
           star->Xenv[XC12],
           star->Xenv[XO16],
           star->Xenv[XNe20],
           star->Xenv[XNe22]
        );

    if(star->stellar_type>=7)
    {
        /* helium stars */
        if(star->Xenv[XHe4]<0.4)
        {
            /* extreme helium stars: force C12 */
            star->Xenv[XC12]=0.0;star->Xenv[XC12]=1.0-nucsyn_totalX(stardata,star->Xenv);
        }
        else
        {
            /* normal helium stars: force He4 */
            star->Xenv[XHe4]=0.0;star->Xenv[XHe4]=1.0-nucsyn_totalX(stardata,star->Xenv);
        }
    }
    else
    {
        star->Xenv[XH1]=0.0;star->Xenv[XH1]=1.0-nucsyn_totalX(stardata,star->Xenv);
    }
    // allow Max 1% error (should be zero!)
    if(nucsyn_totalX(stardata,star->Xenv)>1.1)
    {
        Exit_binary_c(BINARY_C_OUT_OF_RANGE,"nucsyn_WR Xsum > 1.1\n");
    }
    Dprint("postNORM H1=%g He4=%g C12=%g Xtot=%g\n",
           star->Xenv[XH1],
           star->Xenv[XHe4],
           star->Xenv[XC12],
           nucsyn_totalX(stardata,star->Xenv)
        );

    /*
     * Sometimes the surface abundance is just below zero, we don't want this to
     * happen. It's not serious... but annoying.
     */
    CLEANRANGE(star->Xenv[XC12]);
    CLEANRANGE(star->Xenv[XHe4]);
    CLEANRANGE(star->Xenv[XH1]);
#endif // NUCSYN_WR_TABLES
#endif //NUCSYN_WR

    return;

    /**************************************************/


}

#endif // NUCSYN
