#ifndef NUCSYN_STRIP_AND_MIX_H
#define NUCSYN_STRIP_AND_MIX_H

/*
 * Header file for nucsyn's strip and mix
 */

/* data columns */
#define H1 0
#define He4 1
#define C12 2
#define N14 3
#define O16 4
// #define Ne 5
// #define Mg 6

struct layerinfo_t {
    double mu;
    Shell_index orig_sh;
    Abundance X[ISOTOPE_ARRAY_SIZE];
};

static void first_timestep_setup(struct star_t * Restrict star,
                                 struct stardata_t * Restrict const stardata,
                                 const double phase_start_mass
    );
static void burn_lithium(struct star_t * Restrict star);

static void TAMS_mass_accretion(struct star_t * Restrict star,
                                struct stardata_t * Restrict const stardata,
                                const double phase_start_mass,
                                const double pms_mass);
static void mass_changes(struct star_t * Restrict star,
                         struct stardata_t * Restrict const stardata,
                         const double phase_start_mass,
                         const double mshellmax
    );

static void mixing(struct star_t * Restrict star,
                   struct stardata_t * Restrict const stardata,
                   const double phase_start_mass,
                   const double mshellmax);

#ifdef __HAVE_GNU_QSORT_R
static int compare_layerinfo_by_mu(const void * Restrict a,
                                   const void * Restrict b,
                                   void * arg);
#endif
#ifdef __HAVE_BSD_QSORT_R
static int compare_layerinfo_by_mu(void * arg,
                                   const void * Restrict a,
                                   const void * Restrict b);
#endif

#endif // NUCSYN_STRIP_AND_MIX_H
