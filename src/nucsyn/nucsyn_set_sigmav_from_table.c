#include "../binary_c.h"
No_empty_translation_unit_warning;


#if defined NUCSYN && defined NUCSYN_SIGMAV_PRE_INTERPOLATE

void nucsyn_set_sigmav_from_table(struct stardata_t * const stardata,
                                  struct store_t * const store,
                                  const double t9, // T(K)/1e9
                                  double * Restrict const sigmav // target for the interpolation result
    )
{
    /*
     * Use the interpolation routine to calculate the reaction rates
     */
    double x[] = {
#ifdef NUCSYN_SIGMAV_INTERPOLATE_LOGT9
        log10(t9)
#else
        t9
#endif
    };

    Interpolate(
        store->sigmav,
        x,
        sigmav,
        FALSE
        );

#ifdef NUCSYN_SIGMAV_INTERPOLATE_LOGSIGMAV
    size_t i;
    for(i=0;i<SIGMAV_SIZE;i++)
    {
        *(sigmav+i)=exp10(*(sigmav+i));
    }
#endif
}
#endif
