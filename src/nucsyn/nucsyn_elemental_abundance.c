#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN

Abundance nucsyn_elemental_abundance(const char * Restrict const element,
                                     const Abundance * Restrict const X,
                                     struct stardata_t * const stardata Maybe_unused,
                                     struct store_t * const store)
{
    /*
     * Given an element string, calculate the elemental abundance
     * by mass fraction, given an array of mass fractions.
     *
     * Return -1.0 (unphysical) on a set up call.
     *
     * The isotopes corresponding to an element are stored
     * in the store->icache
     */

    Abundance Xelem; // calculated mass fraction (returned)

    if(unlikely(element==NULL && X==NULL))
    {
        /*
         * Set up call
         */
        if(store->icache==NULL)
        {
            nucsyn_make_icache(store);
        }
        else
        {
            nucsyn_free_icache(store->icache);
        }
        Xelem = -1.0; /* unphysical */
    }
    else
    {
        /*
         * ID isotopes of this element
         */

        /* calculate the atomic number of the element */
        const Atomic_number Z = nucsyn_element_to_atomic_number(store,
                                                                element,
                                                                1);
        const unsigned int nisotopes = store->nisotopes[Z];
        const Isotope ** const icache = (const Isotope ** const)store->icache;
        Xelem = 0.0;
        Dprint("Get elemental abundance of %s atomic number %d with %u isotopes\n",element,Z,nisotopes);

        /* add abundances from isotope numbers stored in icache */
        unsigned int c;
        for(c=0; c<nisotopes; c++)
        {
            Xelem += X[icache[Z][c]];
        }
    }

    return Xelem;
}




#endif // NUCSYN
