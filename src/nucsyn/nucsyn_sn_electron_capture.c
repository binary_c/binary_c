#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Electron capture SN yields
 */
#ifdef NUCSYN
#include "nucsyn_isotopes.h"

void nucsyn_sn_electron_capture(struct stardata_t * const stardata,
                                Abundance * Restrict const X)
{
    Clear_isotope_array(X);
    /*
     * Use yields of Wanajo et al 2008: either the ST model
     * or the FP3 model. ST should be the default.
     */

    if(stardata->preferences->electron_capture_supernova_algorithm == NUCSYN_ECSN_WANAJO_2008_ST)
    {
#include "nucsyn_sn_electron_capture_wanajo_ST.h"
    }
    else if(stardata->preferences->electron_capture_supernova_algorithm == NUCSYN_ECSN_WANAJO_2008_FP3)
    {
#include "nucsyn_sn_electron_capture_wanajo_FP3.h"
    }
    else
    {
        Exit_binary_c(BINARY_C_ALGORITHM_OUT_OF_RANGE,
                      "Unknown electron capture algorithm %d\n",
                      stardata->preferences->electron_capture_supernova_algorithm);
    }
    nucsyn_renormalize_abundance(X);
}

#endif // NUCSYN
