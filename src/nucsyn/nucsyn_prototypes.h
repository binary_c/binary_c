#pragma once
/*
 * The binary_c stellar population nucleosynthesis framework.
 *
 * Contact:  rob.izzard@gmail.com
 *
 * https://binary_c.gitlab.io/
 * https://gitlab.com/binary_c/binary_c
 * https://groups.google.com/forum/#!forum/binary_c-nucsyn-announce
 * https://groups.google.com/forum/#!forum/binary_c-nucsyn-devel
 * https://twitter.com/binary_c_code
 * https://www.facebook.com/groups/149489915089142/
 *
 * Please see the files README, LICENCE and CHANGES
 *
 * This file contains prototypes for the nucsyn library
 */

#ifndef NUCSYN_PROTOTYPES_H
#define NUCSYN_PROTOTYPES_H
#include "nucsyn.h"
#include "../binary_c_structures.h"

/*
 * supernova_yield is called even if NUCSYN is not defined, it is
 * an exceptional function :)
 */
void nucsyn_sn_yield(struct stardata_t * Restrict const stardata,
                     struct star_t * exploder,
                     struct star_t * pre_explosion_star,
                     double dm,
                     Stellar_type pre_explosion_stellar_type
    );

/* all other functions require NUCSYN to be defined */
#ifdef NUCSYN
void nucsyn_log(const double t,
                const double m,
                const double mc,
                double r,
                const double rc,
                const double lum,
                struct star_t *star,
                struct stardata_t *stardata,
                double lambda,
                Abundance * Restrict Xabund,
                const double tpagb_runtime);
void nucsyn_short_log(struct stardata_t * Restrict const stardata);
void nucsyn_long_log(struct stardata_t * Restrict const stardata);

void nucsyn_hbb(const double temp,
                const double rho,
                const double thisdt,
                Abundance * Restrict const Xhbb,
                struct star_t *const star,
                struct stardata_t *  const stardata);


double nucsyn_molecular_weight(const Abundance * Restrict const X,
                               const struct stardata_t * Restrict const stardata,
                               const double ionisation_fraction);
double nucsyn_effective_molecular_weight(const Abundance * const Restrict X,
                                         const Stellar_type stellar_type,
                                         const double * Restrict const molweights);
void nucsyn_set_nuc_masses(Nuclear_mass * Restrict const mnuc,
                           Nuclear_mass * Restrict const imnuc,
                           Nuclear_mass * Restrict const mnuc_amu, // (as mnuc /= AMU_GRAMS)
                           Nuclear_mass * Restrict const imnuc_amu,
                           Molecular_weight * Restrict  const molweight,
                           double * Restrict const ZonA,
                           Atomic_number * Restrict const atomic_number,
                           Nucleon_number * Restrict const nucleon_number,
                           double * Restrict const ionised_molecular_weight_multiplier);

void Xmult(Abundance * Restrict const X,
           const double f);
void X_to_N(const Nuclear_mass * Restrict const imnuc,
            const double dens,
            Number_density * Restrict const N,
            const Abundance * Restrict const X,
            const Isotope num_species);
void N_to_X(const Nuclear_mass * Restrict const mnuc,
            const double dens,
            const Number_density * Restrict const N,
            Abundance * Restrict const X,
            const Isotope num_species);

double nucsyn_third_dredge_up(struct stardata_t * const Restrict stardata,
                              struct star_t * const newstar);

#ifndef NUCSYN_CALIBRATION_LOG
Constant_function
#endif
double nucsyn_hbbtmax(double menv_1tp,
                      double mc_1tp,
                      const Abundance Z);

void nucsyn_set_sigmav(struct stardata_t * const stardata,
                       struct store_t * const store,
                       const double temp, /* temperature */
                       Reaction_rate * const sigmav, /* reaction rates */
                       const double * Restrict const multipliers /* reaction rate multipliers */
#ifdef RATES_OF_AMANDA_NE22PG_FIX
                       , const Boolean ne22pg_cf88
#endif
    );

void nucsyn_mix_shells(const double m1,
                       Abundance * const Restrict X1,
                       const double m2,
                       Abundance * const Restrict X2);

Constant_function double nucsyn_lamaxf(const double m,
                                       const Abundance z,
                                       const struct star_t *star,
                                       const struct stardata_t *stardata
    );



void nucsyn_tpagb(double * Restrict const  age,
                  double * Restrict const  mc,
                  double * Restrict const  mt,
                  double * Restrict const  lum,
                  double * Restrict const  r,
                  struct star_t * const newstar,
                  struct stardata_t * Restrict const  stardata);

void nucsyn_set_1st_dup_abunds(Abundance * Restrict const X,
                               double m,
                               const Stellar_type stellar_type,
                               struct star_t *const star,
                               struct stardata_t *const stardata,
                               Boolean asevent);

void nucsyn_set_post_2nd_dup_abunds(Abundance * Restrict const X,
                                    const double m,
                                    const Abundance z,
                                    const double mcbagb,
                                    struct star_t *const star,
                                    struct stardata_t *Restrict const stardata);




Constant_function double nucsyn_mhbbmin(const Abundance  z);
void nucsyn_dilute_shell(const double m1,
                         Abundance * Restrict const X1,
                         const double m2,
                         const Abundance * const Restrict X2);

void nucsyn_dilute_shell_to(const double m1,
                            const Abundance * const Restrict X1,
                            const double m2,
                            const Abundance * const Restrict X2,
                            Abundance * Restrict X3_to_be);

Constant_function double nucsyn_wgcoremasslum(const double mc,
                                              const double mass,
                                              const double mc_1tp,
                                              const Abundance z,
                                              const double alpha,
                                              Boolean hbbB);

#ifdef NUCSYN_ANAL_BURN
void nucsyn_anal_CNO_burn(double * Restrict t,
                          Number_density * Restrict Nin,
                          double dtseconds,
                          Boolean eqonly);
int nucsyn_anal_NeNa_burn(double * Restrict t,
                          Number_density * Restrict Nin,
                          const double dtseconds,
                          const Boolean eqonly);
int nucsyn_anal_NeNa_burn1(struct stardata_t * stardata,
                           double * Restrict t,
                           Number_density * Restrict Nin,
                           const double dtseconds);

void nucsyn_anal_MgAl_burn(struct stardata_t * stardata,
                           double * Restrict t,
                           Number_density * Restrict Nin,
                           const double dtseconds);
#endif // NUCSYN_ANAL_BURN


void nucsyn_set_third_dredgeup_abunds(Abundance * Restrict const dup_material,
                                      struct star_t * const newstar,
                                      const Abundance z,
                                      struct stardata_t * const stardata,
                                      const double mc);



double nucsyn_set_hbb_conditions(struct star_t * const newstar,
                                 struct stardata_t * Restrict const stardata,
                                 const double m0,
                                 const double menv);
Constant_function double nucsyn_tpagb_rhomax(const double m0,
                                             const Abundance z);

void nucsyn_init_first_pulse(struct star_t * const star,
                             struct stardata_t * const stardata);

void Hot_function nucsyn_calc_yields(struct stardata_t * Restrict const stardata,
                                     struct star_t * const star,
                                     const double dmlose, /* mass lost as wind/otherwise */
                                     Abundance * const Xlose, /* abundance of this lost material */
                                     const double dmacc, /* mass gained by accretion */
                                     Abundance * const Xacc, /* abundance of this accretion */
                                     const Star_number starnum,
                                     const int final,
                                     const Yield_source source
    ) Nonnull_some_arguments(1,2);




void nucsyn_set_tpagb_free_parameters(double m,
                                      Abundance z,
                                      struct star_t * const star,
                                      struct stardata_t * const stardata);
double Pure_function nucsyn_totalX_range(struct stardata_t * const stardata,
                                         const Abundance * Restrict X,
                                         const Isotope Xmin,
                                         const Isotope Xmax) Nonnull_some_arguments(1,2);

void nucsyn_binary_yield(struct stardata_t * Restrict const stardata,
                         const Boolean final)
    Nonnull_some_arguments(1);

void nucsyn_zero_yields(struct star_t * const Restrict star);

void nucsyn_set_WD_abunds(struct stardata_t * const stardata,
                          Abundance * Restrict const Xenv,
                          const Stellar_type stellar_type);

void Nonnull_some_arguments(1) nucsyn_set_remnant_abunds(Abundance * Restrict const Xenv);

double Pure_function Nonnull_some_arguments(1) nucsyn_totalX(const Abundance * Restrict const X);

#ifdef NUCSYN_THERMOHALINE_MIXING_TIME
Constant_function double nucsyn_thermohaline_mixing_time(const double mu0,
                                                         const double mu1,
                                                         const double dmacc,
                                                         const double m,
                                                         const double l,
                                                         const double r);
#endif

void nucsyn_set_nova_abunds(struct stardata_t * const stardata,
                            const struct star_t * const star,
                            Abundance * Restrict const Xdonor,
                            Abundance * Restrict const Xnova);

void nucsyn_remove_dm_from_surface(
    struct stardata_t * Restrict const stardata Maybe_unused,
    struct star_t * Restrict const star,
    const double dm,
    Abundance ** Restrict const X,
    Boolean * Restrict const allocated);



double Pure_function nucsyn_choose_wind_mixing_factor(const struct stardata_t * Restrict const stardata,
                                                      const Star_number k /* Number of ACCRETOR star */);


void nucsyn_s_process(Abundance * Restrict const dup_material,
                      struct star_t * const newstar,
                      Abundance z,
                      struct stardata_t * const stardata);


void nucsyn_WR(struct star_t * const star,
               struct stardata_t * Restrict const stardata,
               const double stellar_mass Maybe_unused);

void nucsyn_WR_table(struct stardata_t * Restrict const stardata,
                     struct star_t * const star);
void nucsyn_WR_table_lookup(struct stardata_t * Restrict const stardata,
                            const double * Restrict const x,
                            Abundance * Restrict const r,
                            const int wr_wind);
void nucsyn_WR_RS_table_lookup(struct stardata_t * Restrict const stardata,
                               double * Restrict const x,
                               Abundance * Restrict const r);

WR_type Pure_function nucsyn_WR_type(const struct star_t * Restrict const star);

#ifdef NUCSYN_J_LOG
void nucsyn_j_log(struct stardata_t * Restrict const stardata);
#endif

void nucsyn_radioactive_decay(struct stardata_t * Restrict const stardata);
void nucsyn_do_radioactive_decay(const struct stardata_t * Restrict const stardata,
                                 Abundance * const X);

Abundance Pure_function * nucsyn_observed_surface_abundances(struct star_t * Restrict star);

void nucsyn_mix_accretion_layer_and_envelope(struct stardata_t * const stardata,
                                             struct star_t * Restrict const star,
                                             double menv);

void nucsyn_merge_NSs(const struct stardata_t * Restrict const stardata Maybe_unused);

Moles Pure_function nucsyn_mole_fraction(const Isotope i, /* the species */
                                         const Abundance * Restrict const X, /* mass fractions */
                                         const Nuclear_mass * Restrict const mnuc /* nuclear masses */);
Abundance_ratio Pure_function
nucsyn_square_bracket(const Abundance * Restrict const X,
                      const Abundance * Restrict const Xsolar,
                      const Isotope top_isotope,
                      const Isotope bot_isotope);

Abundance_ratio Pure_function
nucsyn_square_multibracket(const Abundance * Restrict const X,
                           const Abundance * Restrict const Xsolar,
                           const Isotope top_isotopes[],
                           const int n_top,
                           const Isotope bottom_isotope[],
                           const int n_bottom);

void nucsyn_initial_abundances(
    struct stardata_t * Restrict const stardata Maybe_unused,
    Abundance * X,
    const Abundance z,
    const Abundance_mix mix,
    const Boolean modulate);

void nucsyn_planetary_nebulae(const struct stardata_t * Restrict const stardata,
                              const Boolean PNetype,
                              const Star_number starnum,
                              const double dmass,
                              const Abundance *  X,
                              const double vexp);

void nucsyn_set_abunds_array(Abundance * Restrict const array,
                             const Abundance c,
                             const Boolean electrons);

void nucsyn_lowz_yields(Abundance * Restrict const X,
                        double mass);

/* Functions used to add or calculate supernova yields */
void nucsyn_sn_iwamoto_1999_DD2(Abundance * Restrict const X);
void nucsyn_sn_livne_arnett_1995(Abundance * Restrict const X,
                                 const double mcore,
                                 const double menv);
void nucsyn_sn_woosley_taam_weaver_1986(Abundance * Restrict const X );

double nucsyn_sn_core_collapse(Abundance * const X,
                               Abundance * const Xenv,
                               const double mco,
                               const double menv,
                               double dm,
                               Abundance z,
                               const Yield_source src_id,
                               struct stardata_t * const stardata,
                               const struct star_t * const pre_exploder_star,
                               struct star_t * const exploder);

void nucsyn_sn_woosley_weaver_1995(struct stardata_t * const  stardata,
                                   Abundance * const X,
                                   double mco,
                                   Abundance z);

void nucsyn_sn_chieffi_limongi_2004(struct stardata_t * const stardata,
                                    Abundance * const X,
                                    const double mco,
                                    const Abundance z,
                                    const double mcut,
                                    struct star_t * const star);

void nucsyn_sn_limongi_chieffi_2018(struct stardata_t * const stardata,
                                    Abundance * const X,
                                    const double mco,
                                    const Abundance z,
                                    const double mcut,
                                    struct star_t * const star);

void nucsyn_core_collapse_r_process(Abundance * Restrict const Xr,
                                    struct stardata_t * Restrict const stardata);
void nucsyn_r_process_Simmerer2004(Abundance * Restrict const Xr,
                                   struct stardata_t * Restrict const stardata);
void nucsyn_r_process_Arlandini1999(Abundance * Restrict const Xr,
                                    struct stardata_t * Restrict const stardata);


/*
 * Nuclear network solvers
 */
void nucsyn_burn(const double logT,
                 const double density,
                 const double dt,
                 Number_density * Restrict const N,
                 struct star_t * const star Maybe_unused,
                 struct stardata_t * const stardata);

void ppderivatives(const double * const Restrict y,
                   double * const Restrict dydt,
                   const double * const Restrict sigmav,
                   const double * const Restrict N Maybe_unused);
void coldCNOderivatives(const double * Restrict const y,
                        double * Restrict const dydt,
                        const double * Restrict const sigmav,
                        const double * Restrict const N Maybe_unused);
void NeNaMgAlderivatives(const double * Restrict const y,
                         double * Restrict const dydt,
                         const double * Restrict const sigmav,
                         const double * Restrict const N Maybe_unused);

void ppjacobian(double * const * const Restrict J,
                const double * const Restrict N,
                const double * const Restrict sigmav);


#if defined NUCSYN_NETOWRK_HOTCNO &&            \
    defined NUCSYN_NETWORK_COLDCNO
void nucsyn_network_test(struct stardata_t * Restrict const stardata);
#endif

double nucsyn_network_burn(double * Restrict const Nin,
                           const double * Restrict const sigmav,
                           const double maxt,
                           nucsyn_burnfunc burnfunc,
                           nucsyn_logfunc logfunc,
                           const double dtguess,
                           struct stardata_t * const Restrict stardata,
                           const Reaction_network network_id,
                           const Boolean vb);


double nucsyn_network_burn_lsoda(double * Restrict const Nin,
                                 const double * Restrict const sigmav,
                                 const double maxt,
                                 nucsyn_burnfunc burnfunc,
                                 nucsyn_logfunc logfunc,
                                 const double dtguess,
                                 struct stardata_t * const Restrict stardata,
                                 const Reaction_network network_id,
                                 const Boolean vb);

#ifdef __HAVE_LIBSUNDIALS_CVODE__
double nucsyn_network_burn_cvode(double * Restrict const Nin,
                                 const double * Restrict const sigmav,
                                 const double maxt,
                                 nucsyn_burnfunc burnfunc,
                                 nucsyn_logfunc logfunc Maybe_unused,
                                 const double dtguess Maybe_unused,
                                 struct stardata_t * const Restrict stardata,
                                 const Reaction_network network_id Maybe_unused,
                                 const Boolean vb);
#endif // __HAVE_LIBSUNDIALS_CVODE__

/* nuclear burning functions */
double nucsyn_burn_kaps_rentrop_pp (struct stardata_t * const Restrict stardata,
                                    double * const Nin,
                                    const double * const sigmav,
                                    const double h);
double nucsyn_burn_ppfast (struct stardata_t * Restrict const stardata,
                           double * Restrict const Nin,
                           const double * Restrict const sigmav,
                           const double h);
double nucsyn_burn_kaps_rentrop_coldCNO (struct stardata_t * Restrict const stardata,
                                         double * Restrict const Nin,
                                         const double * Restrict const sigmav,
                                         const double h);
double nucsyn_burn_hotCNO (struct stardata_t * Restrict const stardata,
                           double * Restrict const Nin,
                           const double * Restrict const sigmav,
                           const double h);
double nucsyn_burn_NeNa (struct stardata_t * Restrict const stardata,
                         double * Restrict Nin,
                         const double * Restrict sigmav,
                         const double h);
double nucsyn_burn_NeNaMgAlnoleak (struct stardata_t * Restrict const stardata,
                                   double * Restrict Nin,
                                   const double * Restrict sigmav,
                                   const double h);
double nucsyn_burn_kaps_rentrop_NeNaMgAl (struct stardata_t * Restrict const stardata,
                                          double * Restrict const Nin,
                                          const double * Restrict const sigmav,
                                          const double h);
double nucsyn_burn_lsoda_pp(struct stardata_t * const Restrict stardata,
                            double * const Nin,
                            const double * const sigmav,
                            const double h);

#ifdef __HAVE_LIBSUNDIALS_CVODE__
double nucsyn_burn_cvode_pp(struct stardata_t * const Restrict stardata,
                            double * const Nin,
                            const double * const sigmav,
                            const double h);
double nucsyn_burn_cvode_coldCNO(struct stardata_t * const Restrict stardata,
                                 double * const Nin,
                                 const double * const sigmav,
                                 const double h);
double nucsyn_burn_cvode_NeNaMgAl(struct stardata_t * const Restrict stardata,
                                  double * const Nin,
                                  const double * const sigmav,
                                  const double h);
#endif // __HAVE_LIBSUNDIALS_CVODE__

double nucsyn_burn_lsoda_coldCNO(struct stardata_t * Restrict const stardata,
                                 double * Restrict const Nin,
                                 const double * Restrict const sigmav,
                                 const double h);
double nucsyn_burn_lsoda_NeNaMgAl(struct stardata_t * Restrict const stardata,
                                  double * Restrict const Nin,
                                  const double * Restrict const sigmav,
                                  const double h);

double nucsyn_burn_helium(struct stardata_t * Restrict const stardata,
                          double * Restrict const Nin,
                          const double * Restrict const sigmav,
                          const double h);

Boolean nucsyn_helium_checkfunction(struct stardata_t * const stardata,
                                    double * const N,
                                    const double temperature,
                                    const double density);
Boolean nucsyn_hydrogen_pp_checkfunction(struct stardata_t * const stardata,
                                         double * const N,
                                         const double temperature,
                                         const double density);
Boolean nucsyn_hydrogen_CNO_checkfunction(struct stardata_t * const stardata Maybe_unused,
                                          double * const N,
                                          const double temperature,
                                          const double density Maybe_unused);
Boolean nucsyn_hydrogen_NeNaMgAl_checkfunction(struct stardata_t * const stardata Maybe_unused,
                                               double * const N,
                                               const double temperature,
                                               const double density Maybe_unused);

double Constant_function nucsyn_CN_timescale(const double t9,
                                             const double NH1,
                                             const double f);

void nucsyn_thermohaline_mix(struct stardata_t * const stardata,
                             const Star_number k, // accreting star number
                             Abundance * const Xacc, // accretion abundances
                             const double dmacc, // mass accreted
                             const Abundance * const Xenv, // envelope abundances: pre mix
                             Abundance * const nXenv // envelope abundances: post mix (written)
    );

void nucsyn_set_sigmav_from_table(struct stardata_t * const stardata,
                                  struct store_t * const store,
                                  const double t9, // T(K)/1e9
                                  double * Restrict const sigmav // target for the interpolation result
    );

void print_sigmav_table(struct store_t * Restrict store);

double Pure_function nucsyn_free_electron_density(const double * Restrict const N,// number density
                                                  const int * Restrict const Z); // atomic numbers

void nucsyn_burning_cycles(Abundance * Restrict const N,
                           Reaction_rate * Restrict const sigmav,
                           const double dt,
                           const double temperature,
                           const double density,
                           struct stardata_t * Restrict const stardata);

void nucsyn_reset_reaction_rates(struct preferences_t * Restrict const preferences);

void nucsyn_setup_default_mass_shells(struct stardata_t * Restrict const stardata);

void nucsyn_set_nuclear_burning_timestep(struct stardata_t * Restrict const stardata,
                                         struct star_t * Restrict star);

void nucsyn_main_sequence_convective_envelope_size_table(struct stardata_t * stardata);


double Constant_function nucsyn_main_sequence_convective_envelope_size(double x,
                                                                       double y,
                                                                       double z);



double Pure_function nucsyn_envelope_mass(const struct star_t * Restrict const star);


void nucsyn_first_dup_interpolation(struct stardata_t * const stardata,
                                    double m,
                                    const double z,
                                    double * Restrict r);

void nucsyn_first_dup_apply(const double m,
                            struct stardata_t * const stardata,
                            Abundance * Restrict const X,
                            double * Restrict const r);
double nucsyn_match_chi_squared(const struct stardata_t * Restrict const stardata,
                                const struct star_t * Restrict const star);
void nucsyn_match_set_abundance(const Isotope i,
                                const Abundance abundance,
                                const Abundance error,
                                struct stardata_t * Restrict const stardata
    );

void nucsyn_sn_electron_capture(struct stardata_t * Restrict const stardata,
                                Abundance * Restrict const X);
void nucsyn_huge_pulse(struct star_t * Restrict const star,
                       struct stardata_t * Restrict const stardata);

void nucsyn_check_abundance_array(struct stardata_t * Restrict const stardata,
                                  const Abundance * Restrict const X,
                                  const char * Restrict const s);
#ifdef LITHIUM_TABLES
void nucsyn_lithium(struct star_t * Restrict const star,
                    struct stardata_t * Restrict const stardata);

#endif


void nucsyn_make_isotope_names(struct store_t * Restrict const store);

Abundance nucsyn_elemental_abundance(const char * Restrict const element,
                                     const Abundance * Restrict const X,
                                     struct stardata_t * const stardata Maybe_unused,
                                     struct store_t * const store);

Abundance_ratio nucsyn_elemental_square_bracket(const char * Restrict const top,
                                                const char * Restrict const bottom,
                                                const Abundance * Restrict const X,
                                                const Abundance * Restrict const Xsolar,
                                                struct stardata_t * Restrict const stardata);

Atomic_number nucsyn_element_to_atomic_number(struct store_t * Restrict const store,
                                              const char * Restrict const element,
                                              const unsigned int error_strategy);

void nucsyn_TZ_surface_abundances(struct star_t * Restrict const star);


void nucsyn_strip_and_mix(struct stardata_t * const stardata,
                          struct star_t * const star);
void mix_down_to_core(struct star_t * Restrict const star);
double mshells(struct star_t * Restrict const star);
void sort_by_molecular_weight(struct star_t * Restrict const star,
                              struct stardata_t * Restrict const stardata);
void nucsyn_mix_stars(struct stardata_t * Restrict const stardata,
                      const double mlost,
                      const double m3);
Boolean can_use_strip_and_mix(struct stardata_t * const stardata,
                              struct star_t * Restrict const star);
void strip_and_mix_remove_mass(struct star_t * Restrict const star,
                               double dmlose);
void halve_shells(struct star_t * Restrict const star);

void nucsyn_build_store_contents(struct store_t * Restrict const store);
void nucsyn_free_store_contents(struct store_t * Restrict const store);

double Pure_function nucsyn_recombination_energy_per_gram(struct stardata_t * Restrict const stardata,
                                                          Abundance * Restrict const X);

void nucsyn_check_for_second_dredge_up(struct stardata_t * const stardata,
                                       struct star_t * const newstar,
                                       const double mcbagb);
void nucsyn_renormalize_abundance(Abundance * Restrict const X);

void nucsyn_sn_Seitenzahl2013(struct stardata_t * Restrict const stardata,
                              Abundance * Restrict const X);
void nucsyn_sn_Seitenzahl2013_automatic(struct stardata_t * Restrict const stardata,
                                        Abundance * Restrict const X);

void nucsyn_angelou_lithium(struct stardata_t * const stardata,
                            struct star_t * const star);

Yield_source Pure_function nucsyn_sn_source(struct star_t * Restrict const exploder);

Abundance nucsyn_elemental_abundance_by_number(
    const char * Restrict const element,
    const Abundance * Restrict const X,
    struct stardata_t * const stardata,
    struct store_t * const store,
    const double density);

Abundance_ratio nucsyn_elemental_abundance_ratio_by_number(
    const char * Restrict const numerator,
    const char * Restrict const denominator,
    const Abundance * Restrict const X,
    struct stardata_t * const stardata,
    struct store_t * const store);

void nucsyn_make_icache(struct store_t * const store);
void nucsyn_free_icache(Isotope ** icache);

void nucsyn_add_mass_to_surface(const Star_number k,
                                const double dm,
                                const Abundance * Restrict const X,
                                double * Restrict const ndmacc,
                                Abundance ** Restrict newXacc);

void nucsyn_remove_mass_from_surface(
    struct stardata_t * stardata,
    struct star_t * Restrict star,
    const double dm,
    Abundance ** Restrict X,
    double * Restrict ndmacc,
    Boolean *Restrict allocated);



Boolean nucsyn_thermohaline_unstable(struct stardata_t * const stardata,
                                     struct star_t * const star);

void nucsyn_thermohaline_mix_star(struct stardata_t * const stardata,
                                  struct star_t * const star);

#ifdef STELLAR_POPULATIONS_ENSEMBLE
void ensemble_hash_error_handler(void * s,
                                 const int error_number,
                                 const char * const format,
                                 va_list args);
#endif // STELLAR_POPULATIONS_ENSEMBLE


void nucsyn_dust_from_common_envelope(struct stardata_t * stardata,
                                      const double common_envelope_mass,
                                      double * mass_carbon_grains,
                                      double * mass_MgSiO3_grains);


double Hot_function Pure_function nucsyn_nucleon_density(
#ifdef NANCHECKS
    struct stardata_t * Restrict const stardata,
#endif
    const Abundance * Restrict const N,
    const int * Restrict const A
#ifdef NANCHECKS
    ,const Boolean inout
#endif
    );

void nucsyn_burn_CNO_completely(Abundance * const X);


Constant_function double nucsyn_reaclib(
    const double t9, // temperature (/1e9)
    const double i9, // 1.0/t9
    const double tm13, // pow(t9,-0.3333333333)
    const double t913, // pow(t9,0.333333333333333333333333)
    const double t953, // pow(t9,1.66666666666666666666)
    const double lt9, // (natural) log(t9)
    const double ra, // reaclib parameters
    const double rb,
    const double rc,
    const double rd,
    const double re,
    const double rf,
    const double rg);


void nucsyn_load_condensation_data(struct stardata_t * const stardata);


double nucsyn_elemental_condensation_temperature(struct stardata_t * const stardata,
                                                 const char * const element);


Abundance * nucsyn_core_abundances(struct stardata_t * const stardata,
                                   struct star_t * const star);



double nucsyn_sn_Gronow2021a(struct stardata_t * const stardata,
                             Abundance * Restrict const X,
                             const double mcore,
                             const double menv);
double nucsyn_sn_Gronow2021b(struct stardata_t * const stardata,
                             Abundance * Restrict const X,
                             const double metallicity,
                             const double mcore,
                             const double menv);

void nucsyn_NSNS(struct stardata_t * const stardata,
                 Abundance * const Xej);

void nucsyn_Radice2018(struct stardata_t * const stardata,
                       Abundance * const Xej);

void nucsyn_sn_he_degenerate_core(Abundance * const X, //space calloc'd previously
                                  struct stardata_t * const stardata,
                                  const struct star_t * const pre_explosion_star);

double nucsyn_total_yield(struct stardata_t * Restrict const stardata);

void nucsyn_renormalize_to_max(Abundance * Restrict const X);
char * nucsyn_yield_string(struct star_t * const star);

#endif /* NUCSYN */
#endif /* NUCSYN_PROTOYPES */
