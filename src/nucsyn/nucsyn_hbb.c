#include "../binary_c.h"
No_empty_translation_unit_warning;


#if defined NUCSYN && \
    defined NUCSYN_TPAGB_HBB

#define TIMESTEP (1e6*stardata->model.dtm)
//#define NUCSYN_TPAGB_HBB_CLAYTON_TABLE
#ifdef NUCSYN_TPAGB_HBB_CLAYTON_TABLE
void nucsyn_clayton_table(double *mnuc);
#endif

#if (DEBUG==1)
#define HBB_DEBUG
#endif

/***************************************************************************
 * Function to do the hot bottom burning for Asymptotic Giant Branch stars *
 ***************************************************************************/
void nucsyn_hbb(const double T, /* (log10) temperature */
                const double rho, /* density */
                const double dt, /* time we burn for (years) */
                Abundance * Restrict const Xhbb, /* incoming mass fractions */
                struct star_t * const star,
                struct stardata_t * const stardata
    )
{
    const Nuclear_mass * const mnuc = stardata->store->mnuc;
    const Nuclear_mass * const imnuc = stardata->store->imnuc;

//#define __TEST_HE_BURN
#ifdef __TEST_HE_BURN
    Abundance * X = New_clear_isotope_array;
    Number_density * N = New_isotope_array;

    const double timemax = 100;
    const double dtime = timemax/1000.0;
    const double logtemp = log10(3e8);
    const double testrho = 1e2;

    /* start with a lot of helium */
    Clear_isotope_array(X);
    X[XHe4] = 1.0;
    X_to_N(imnuc,testrho,N,X,ISOTOPE_ARRAY_SIZE);

    double time = 0.0;
    while(time < timemax)
    {
        nucsyn_burn(logtemp,testrho,dtime,N,star,stardata);
        N_to_X(mnuc,testrho,N,X,ISOTOPE_ARRAY_SIZE);
        time += dtime;
        printf("HEBURN %g %g %g %g %g %g %g\n",
               time,
               X[XHe4],
               X[XC12],
               X[XO16],
               X[XNe20],
               X[XMg24],
               nucsyn_totalX(stardata,X)
            );
    }
    Safe_free(X);
    Safe_free(N);

    Flexit;
#endif//__TEST_HE_BURN

#ifdef NUCSYN_HBB_RENORMALIZE_MASS_FRACTIONS
    Abundance Xtot;
#endif

#ifdef NUCSYN_TPAGB_HBB_CLAYTON_TABLE
    nucsyn_clayton_table(mnuc);
#endif

#ifndef NUCSYN_ALLOW_COOL_CNO_BURNING
    if(T<6) return; /* no point burning! */
#endif
    if(dt<0.0) return; // will fail

    /* Convert to number densities */
#ifdef HBB_DEBUG
    Dprint("nucsyn_hbb.c start\nConvert X to N : X[H1,C12,C13,N14]=%g,%g,%g,%g -> ",
           Xhbb[XH1],Xhbb[XC12],Xhbb[XC13],Xhbb[XN14]);
#endif
#ifdef NANCHECKS
#include "nucsyn_strided_isotope_loops.h"
    Nucsyn_isotope_stride_loop(
        if(isnan(Xhbb[i])!=0)
        {
            Exit_binary_c(BINARY_C_EXIT_NAN,"nucsyn_hbb (preburn) isotope %u/%u () Xhbb is %g\n",
                          i,
                          ISOTOPE_ARRAY_SIZE,
                          Xhbb[i]);
        }
        );
#endif
    Number_density * Nhbb = New_isotope_array;
    X_to_N(imnuc,rho,Nhbb,Xhbb,ISOTOPE_ARRAY_SIZE);

#ifdef HBB_DEBUG
    Dprint("N=%g,%g,%g\n",Nhbb[XC12],Nhbb[XC13],Nhbb[XN14]);
    Dprint("Xsum is %g Nsum in %g ",nucsyn_totalX(Xhbb),nucsyn_totalX(Nhbb));
#endif

#ifdef NANCHECKS
    Nucsyn_isotope_stride_loop(
        if(isnan(Nhbb[i])!=0)
        {
            Exit_binary_c(BINARY_C_EXIT_NAN,"nucsyn_hbb (preburn) isotope %u/%u () is %g\n",
                          i,
                          ISOTOPE_ARRAY_SIZE,
                          Nhbb[i]);
        }
    );
#endif

     /* do the burn ( N(t) -> N(t+dt) ) */
    nucsyn_burn(T,rho,dt,Nhbb,star,stardata);

#ifdef NANCHECKS
    Nucsyn_isotope_stride_loop(
        if(isnan(Nhbb[i])!=0)
        {
            Exit_binary_c(BINARY_C_EXIT_NAN,
                          "nucsyn_hbb (postburn) isotope %u/%u () is %g\n",
                          i,
                          ISOTOPE_ARRAY_SIZE,
                          Nhbb[i]);
        }
        );
#endif

    /* covert number density to mass fractions */
    N_to_X(mnuc,rho,Nhbb,Xhbb,ISOTOPE_ARRAY_SIZE);

#ifdef NUCSYN_HBB_RENORMALIZE_MASS_FRACTIONS
    /*
     * renormalize mass fractions : NB this conceals errors which
     * you might not want to conceal! If the errors are > 1% then
     * an error is generated and the code stops.
     */
    Xtot = nucsyn_totalX(stardata,Xhbb);
    if(fabs(Xtot-1.0) > 0.1)
    {
        Forward_isotope_loop(i)
        {
            printf("X%d=%g ",i,Xhbb[i]);
        }
        printf("\n");
        Exit_binary_c(BINARY_C_HBB_MASS_FRACTION_ERROR,
                      "Xtot (%g) differs from 1.0 by > 1%% in nucsyn_hbb\n",Xtot);
    }
    Xmult(Xhbb,1.0/Xtot);
#endif

    Safe_free(Nhbb);

    return;
}

#endif // NUCSYN_TPAGB_HBB && NUCSYN
