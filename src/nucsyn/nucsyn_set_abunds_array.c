#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN

void nucsyn_set_abunds_array(Abundance * Restrict const array,
                             const Abundance c,
                             const Boolean electrons)
{
    /*
     * Set the abundance c into all the array elements
     */
    Nucsyn_isotope_stride_loop(
        array[i] = c;
        );
    if(electrons) array[Xe] = c;
}

#endif // NUCSYN
