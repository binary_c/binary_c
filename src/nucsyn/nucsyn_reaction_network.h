#ifdef NUCSYN
//Reaction C12pgC13  : C12 + p -> C13 + g
// This is reaction SIGMAV_T12
species_matrix[XC13][0]=XC12;
reaction_matrix[XC13][0]=(SIGMAV_T12);
species_matrix[XC12][0]=-XC12;
reaction_matrix[XC12][0]=(SIGMAV_T12);
species_matrix[XH1][0]=-XC12;
reaction_matrix[XH1][0]=(SIGMAV_T12);
//Reaction C13pgN14  : C13 + p -> N14 + g
// This is reaction SIGMAV_T13
species_matrix[XN14][0]=XC13;
reaction_matrix[XN14][0]=(SIGMAV_T13);
species_matrix[XC13][1]=-XC13;
reaction_matrix[XC13][1]=(SIGMAV_T13);
species_matrix[XH1][1]=-XC13;
reaction_matrix[XH1][1]=(SIGMAV_T13);
//Reaction N14pgN15  : N14 + p -> N15 + g
// This is reaction SIGMAV_T14
species_matrix[XN15][0]=XN14;
reaction_matrix[XN15][0]=(SIGMAV_T14);
species_matrix[XN14][1]=-XN14;
reaction_matrix[XN14][1]=(SIGMAV_T14);
species_matrix[XH1][2]=-XN14;
reaction_matrix[XH1][2]=(SIGMAV_T14);
//Reaction N15pgO16  : N15 + p -> O16 + g
// This is reaction SIGMAV_T15_BRANCH
species_matrix[XO16][0]=XN15;
reaction_matrix[XO16][0]=(SIGMAV_T15_BRANCH);
species_matrix[XN15][1]=-XN15;
reaction_matrix[XN15][1]=(SIGMAV_T15_BRANCH);
species_matrix[XH1][3]=-XN15;
reaction_matrix[XH1][3]=(SIGMAV_T15_BRANCH);
//Reaction N15paC12  : N15 + p -> C12 + a
// This is reaction SIGMAV_T15
species_matrix[XC12][1]=XN15;
reaction_matrix[XC12][1]=(SIGMAV_T15);
species_matrix[XN15][2]=-XN15;
reaction_matrix[XN15][2]=(SIGMAV_T15);
species_matrix[XH1][4]=-XN15;
reaction_matrix[XH1][4]=(SIGMAV_T15);
species_matrix[XHe4][0]=XN15;
reaction_matrix[XHe4][0]=(SIGMAV_T15);
//Reaction O16pgO17  : O16 + p -> O17 + g
// This is reaction SIGMAV_T16
species_matrix[XO17][0]=XO16;
reaction_matrix[XO17][0]=(SIGMAV_T16);
species_matrix[XO16][1]=-XO16;
reaction_matrix[XO16][1]=(SIGMAV_T16);
species_matrix[XH1][5]=-XO16;
reaction_matrix[XH1][5]=(SIGMAV_T16);
//Reaction O17paN14  : O17 + p -> N14 + a
// This is reaction SIGMAV_T17
species_matrix[XN14][2]=XO17;
reaction_matrix[XN14][2]=(SIGMAV_T17);
species_matrix[XO17][1]=-XO17;
reaction_matrix[XO17][1]=(SIGMAV_T17);
species_matrix[XH1][6]=-XO17;
reaction_matrix[XH1][6]=(SIGMAV_T17);
species_matrix[XHe4][1]=XO17;
reaction_matrix[XHe4][1]=(SIGMAV_T17);
//Reaction Ne20pgNe21  : Ne20 + p -> Ne21 + g
// This is reaction SIGMAV_T20
species_matrix[XNe21][0]=XNe20;
reaction_matrix[XNe21][0]=(SIGMAV_T20);
species_matrix[XNe20][0]=-XNe20;
reaction_matrix[XNe20][0]=(SIGMAV_T20);
species_matrix[XH1][7]=-XNe20;
reaction_matrix[XH1][7]=(SIGMAV_T20);
//Reaction Ne21pgNe22  : Ne21 + p -> Ne22 + g
// This is reaction SIGMAV_T21
species_matrix[XNe22][0]=XNe21;
reaction_matrix[XNe22][0]=(SIGMAV_T21);
species_matrix[XNe21][1]=-XNe21;
reaction_matrix[XNe21][1]=(SIGMAV_T21);
species_matrix[XH1][8]=-XNe21;
reaction_matrix[XH1][8]=(SIGMAV_T21);
//Reaction Ne22pgNa23  : Ne22 + p -> Na23 + g
// This is reaction SIGMAV_T22
species_matrix[XNa23][0]=XNe22;
reaction_matrix[XNa23][0]=(SIGMAV_T22);
species_matrix[XNe22][1]=-XNe22;
reaction_matrix[XNe22][1]=(SIGMAV_T22);
species_matrix[XH1][9]=-XNe22;
reaction_matrix[XH1][9]=(SIGMAV_T22);
//Reaction Na23pgMg24  : Na23 + p -> Mg24 + g
// This is reaction SIGMAV_T23P
species_matrix[XMg24][0]=XNa23;
reaction_matrix[XMg24][0]=(SIGMAV_T23P);
species_matrix[XNa23][1]=-XNa23;
reaction_matrix[XNa23][1]=(SIGMAV_T23P);
species_matrix[XH1][10]=-XNa23;
reaction_matrix[XH1][10]=(SIGMAV_T23P);
//Reaction Na23paNe20  : Na23 + p -> Ne20 + a
// This is reaction SIGMAV_T23
species_matrix[XNe20][1]=XNa23;
reaction_matrix[XNe20][1]=(SIGMAV_T23);
species_matrix[XNa23][2]=-XNa23;
reaction_matrix[XNa23][2]=(SIGMAV_T23);
species_matrix[XH1][11]=-XNa23;
reaction_matrix[XH1][11]=(SIGMAV_T23);
species_matrix[XHe4][2]=XNa23;
reaction_matrix[XHe4][2]=(SIGMAV_T23);
//Reaction Mg24pgMg25  : Mg24 + p -> Mg25 + g
// This is reaction SIGMAV_T24
species_matrix[XMg25][0]=XMg24;
reaction_matrix[XMg25][0]=(SIGMAV_T24);
species_matrix[XMg24][1]=-XMg24;
reaction_matrix[XMg24][1]=(SIGMAV_T24);
species_matrix[XH1][12]=-XMg24;
reaction_matrix[XH1][12]=(SIGMAV_T24);
//Reaction Mg25pgAl26  : Mg25 + p -> Al26 + g
// This is reaction SIGMAV_T25
species_matrix[XAl26][0]=XMg25;
reaction_matrix[XAl26][0]=(SIGMAV_T25);
species_matrix[XMg25][1]=-XMg25;
reaction_matrix[XMg25][1]=(SIGMAV_T25);
species_matrix[XH1][13]=-XMg25;
reaction_matrix[XH1][13]=(SIGMAV_T25);
//Reaction Mg26pgAl27  : Mg26 + p -> Al27 + g
// This is reaction SIGMAV_T26
species_matrix[XAl27][0]=XMg26;
reaction_matrix[XAl27][0]=(SIGMAV_T26);
species_matrix[XMg26][0]=-XMg26;
reaction_matrix[XMg26][0]=(SIGMAV_T26);
species_matrix[XH1][14]=-XMg26;
reaction_matrix[XH1][14]=(SIGMAV_T26);
//Reaction Al26betaMg26  : // Beta decay Al26 -> Mg26
species_matrix[XMg26][1]=XAl26;
reaction_matrix[XMg26][1]=-(SIGMAV_TBETA26);
species_matrix[XAl26][1]=-XAl26;
reaction_matrix[XAl26][1]=-(SIGMAV_TBETA26);

*/
#endif
