#pragma once
#ifndef NUCSYN_WR_H
#define NUCSYN_WR_H

/*
 * Macros for the nucsyn_WR* functions
 */

#define CLEANRANGE(A) (A)=Max(0.0,Min(1.0,(A)))

/*
 * Useful macros: DEPTH is the ZAMS mass minus the current mass i.e. the depth
 * we have reached into the stellar envelope. DELTA_CORE is the current
 * envelope mass. WCO_TEST is TRUE if C+O > He. 
 */
#define DEPTH (Max(star->effective_zams_mass-star->mass,TINY))
#define DELTA_CORE (Max(star->mass-star->core_mass[CORE_He],TINY))

// NB this has been fixed: WCO_TEST in my thesis was by MASS and it should
// have been by NUMBER. Lynnette points out that this makes little
// difference to the evolution (and yield) but DOES alter the WC/WO ratio.
// In fact I cannot make any WO stars with it correctly set!
#define WCO_TEST ((star->Xenv[XC12]/12.0+star->Xenv[XC13]/13.0+star->Xenv[XO16]/16.0)/(1e-14+star->Xenv[XHe4]/4.0))

//#define WCO_TEST ((star->Xenv[XC12]+star->Xenv[XC13]+star->Xenv[XO16])/(star->Xenv[XHe4]+1e-14))

#endif // NUCSYN_WR_H
