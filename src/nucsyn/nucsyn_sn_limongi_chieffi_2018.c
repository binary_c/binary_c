#include "../binary_c.h"
No_empty_translation_unit_warning;

#if defined NUCSYN



void nucsyn_sn_limongi_chieffi_2018(struct stardata_t * const stardata,
                                    Abundance * const X,
                                    const double mco,
                                    const Abundance z,
                                    const double mcut,
                                    struct star_t * const star Maybe_unused)
{
   /*
     * Non-orthogonal table has _NLINES lines, _NZ metallicities,
     * _NSTARS stars (each with different MCO), fm = mcut/mco values
     * 338 isotopes.
     */

    if(stardata->persistent_data->Limongi_Chieffi_2018_mapped == NULL)
    {
        Load_and_remap_table(
            stardata->persistent_data->Limongi_Chieffi_2018_mapped,
            "nucsyn/nucsyn_sn_limongi_chieffi_2018.dat",
            All_cols,
            New_res(0,10,0),
            0,
            TRUE
            );
    }

    if(X != NULL)
    {
        /*
         * Interpolate, returns isotopes in Limongi/Chieffi 2018 order
         */
        const double fm = Limit_range( mcut / mco, 0.0, 1.0 );
        Dprint("interpolate { %g %g %g } \n",z,mco,fm);
        double * unsorted_result = Malloc(
            sizeof(double)*(
                (stardata->persistent_data->Limongi_Chieffi_2018_mapped->ndata +
                 stardata->persistent_data->Limongi_Chieffi_2018_mapped->nparam) *
                stardata->persistent_data->Limongi_Chieffi_2018_mapped->nlines)
            );
        const double params[] = {z,mco,fm};
        Interpolate(stardata->persistent_data->Limongi_Chieffi_2018_mapped,
                    params,
                    unsorted_result,
                    FALSE);


        /*
         * Set isotopes for binary_c
         */
#undef Y
#define Y(ISOTOPE, N) X[X##ISOTOPE] = unsorted_result[N];
#include "nucsyn_sn_limongi_chieffi_2018_isotopes.def"
#undef Y
        X[XAl26] += unsorted_result[43]; // Al26g
        X[XAl26] += unsorted_result[44]; // Al26m

        /*
         * Normalize mass fractions
         */
        nucsyn_renormalize_abundance(X);

        Dprint("CCSN Z = %g M_CO = %g mcut = %g, mcut/mco = %g, H=%g C=%g O=%g Fe=%g\n",
               z,
               mco,
               mcut,
               mcut/mco,
               X[XHe4],
               X[XC12],
               X[XO16],
               X[XFe56]);
        Safe_free(unsorted_result);

    }
}

#endif // NUCSYN
