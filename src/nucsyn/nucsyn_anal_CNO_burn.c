#include "../binary_c.h"
No_empty_translation_unit_warning;

#if (defined NUCSYN && defined NUCSYN_ANAL_BURN)
/*
 * CNO burn a la Clayton Nin with timescales t, optimised for speed, not
 * clarity (hey, it works for me ;) - designed to work with the TPAGB code
 * (seems to be ok...)
 */

/*     Nin 1,2,3 are C12,C13,N14 */
//#define CNO_DEBUG

void nucsyn_anal_CNO_burn(double * Restrict t,
                          Number_density * Restrict Nin,
                          const double dtseconds,
                          const Boolean eqonly)
{

    double t12,t13,t14;
    double i12,i13,i14;
    double sigma,delta,lambda2,lambda3;
    Number_density eqabunds[4];
    double smd,spd;
    Number_density dum[5];
    double tsum,csum,B,C;

    /* set timescales */
    t12=t[1];
    t13=t[2];
    t14=t[3];

#ifdef CNO_DEBUG
    printf("\n\nTimescales 1=%g 2=%g 3=%g vs dt=%g\n",
           t[1],t[2],t[3],dtseconds);
    printf("Abunds in %g %g %g\n",Nin[1],Nin[2],Nin[3]);
#endif

/* set inverse timescales */
    i12=1.00/t12;
    i13=1.00/t13;
    i14=1.00/t14;


/*     calculate sigma */
    sigma=i12+i13+i14;

    /*
     * for delta we have to be careful! the argument to sqrt must be > 0
     * otherwise there will be no solutions! In that case, just return, we
     * don't really know what to do...
     */
    B=sigma*sigma-4.0*(i12*(i13+i14)+i13*i14);
    if(B>0)
    {
        delta=sqrt(B);
    }
    else
    {
#ifdef CNO_DEBUG
        printf("Argument to sqrt in delta calculation < 0 - oops!\n");
#endif
        //fprintf(stderr,"skip burn\n");
#ifdef NUCSYN_CNO_ASSUME_STABLE
        /* Assume delta is just small, since B is small */
        delta=TINY;
#else
        fprintf(stderr,"PANTS\n");
        /* Assume we have no idea what to do! */
        return;
#endif
    }

#ifdef CNO_DEBUG
    printf("delta=%g from sigma*sigma=%g 4(...)=%g\n",
           delta,
           sigma*sigma,
           4.0*(i12*i13+i12*i14+i13*i14));
#endif

/*     set eigenvalues */
    lambda2=dtseconds*(delta-sigma)/2.0;
    lambda3=-dtseconds*(delta+sigma)/2.0;
#ifdef CNO_DEBUG
    printf("eigenvalues lambda2=%g lambda3=%g\n",
           lambda2,lambda3);
#endif

/*     calculate equilibrium abundances */
    tsum=t12+t13+t14;
    csum=Nin[1]+Nin[2]+Nin[3];
    eqabunds[1]=csum*t12/tsum; //C12
    eqabunds[2]=csum*t13/tsum; //C13
    eqabunds[3]=csum*t14/tsum; //N14
#ifdef CNO_DEBUG
    printf("eqabunds 1,2,3=%g,%g,%g\n",eqabunds[1],eqabunds[2],eqabunds[3]);
#endif

    if(eqonly==TRUE)
    {
        /* dump equilibrium abundances */
        Nin[1]=eqabunds[1];
        Nin[2]=eqabunds[2];
        Nin[3]=eqabunds[3];
        return;
    }
#ifdef CNO_DEBUG
    printf("sigma=%g delta=%g\n",sigma,delta);
#endif

    smd=0.5*(sigma-delta);
    spd=0.5*(sigma+delta);

/*     calculate dummy varibles (in silly order, I admit) */
    dum[1]=i12/(i13-smd);
#ifdef CNO_DEBUG
    printf("dum[1]=%g from i12=%g i13=%g smd=%g\n",
           dum[1],i12,i13,smd);
#endif
    dum[3]=Nin[1]-eqabunds[1];
    dum[4]=(i12-spd)*t14;
    dum[2]=-(1.00+dum[4]);
#ifdef CNO_DEBUG
    printf("dummy 2,3,4=%g,%g,%g\n",
           dum[2],dum[3],dum[4]);
#endif

/*     calculate C and B
 *     C12 -> Nin(1), C13 -> Nin(2), N14 -> Nin(3) */
    C=((Nin[2]-eqabunds[2])-dum[1]*dum[3])/(dum[2]-dum[1]);
    B=dum[3]-C;
#ifdef CNO_DEBUG
    printf("Calc C from (Nin[2]+%g - eqabunds[2]=%g -dum[1]=%g*dum[3]=%g )/ (dum[2]=%g - dum[1]=%g)\n",
           Nin[2],eqabunds[2],dum[1],dum[3],dum[2],dum[1]);
    printf("Calc B from dum[3]=%g - C=%g\n",
           dum[3],C);
#endif

/* exponential terms */
    C *= exp(lambda3);
    B *= exp(lambda2);

#ifdef CNO_DEBUG
    printf("C=%g B=%g\n",C,B);
#endif

/*     calculate the abundance at t+dtseconds
 *     C12 */
    Nin[1]=Max(0.0,eqabunds[1]+B+C);
/*     C13 */
    Nin[2]=Max(0.0,eqabunds[2]+B*dum[1]+C*dum[2]);
/*     N14 */
    Nin[3]=Max(0.0,eqabunds[3]+B*(-1.00-dum[1])+C*dum[4]);


#ifdef CNO_DEBUG
    printf("Calc Nin[1] from eqabunds[1]=%g + B=%g + C=%g\n",
           eqabunds[1],B,C);
    printf("Calc Nin[2] from eqabunds[2]=%g + B=%g * dum[1]=%g + C=%g * dum[2]=%g\n",
           eqabunds[2],B,dum[1],C,dum[2]);
    printf("Calc Nin[3] from equabunds[3]=%g B=%g dum[1]=%g C=%g dum[4]=%g\n",
           eqabunds[3],B,dum[1],C,dum[4]);

    printf("now Nin (passed out!)=%g,%g,%g\n",
           Nin[1],Nin[2],Nin[3]);
#endif
}
#endif/* NUCSYN */
