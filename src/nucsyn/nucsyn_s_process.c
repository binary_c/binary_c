#include "../binary_c.h"
No_empty_translation_unit_warning;


#if defined(NUCSYN) && defined(NUCSYN_S_PROCESS)

#include "nucsyn_s_process.h"

#define DECAY(A,B,C)                                                    \
    Dprint("DECAY iso %d was %g (decay to %i=%g)...",A,sint[A],C,sint[C]); \
    {                                                                   \
        double y;                                                       \
        y=sint[A];                                                      \
        sint[A]*=exp(-tip/B);                                           \
        sint[C]+=y-sint[A];                                             \
        sint[C]=Max(0.0,sint[C]);                                       \
        Dprint("is now %g (%g)\n",sint[A],sint[C]);                     \
    }

void nucsyn_s_process(Abundance * Restrict const dup_material,
                      struct star_t * const newstar,
                      Abundance z,
                      struct stardata_t * const stardata)
{
    /*
     * Function to implement the s-process elements in the TPAGB intershell
     * region.
     *
     * 22/05/2004 : Updated to include As,Se,Br,Nb,Rh,I,Cs,Ho,Au,Pt,Zn,Ga,Ge
     *
     * 03/11/2005 : Updated to use interpolations to Maria's data rather than
     *              fits for Ba, Kr, Y and Pb.
     *
     * 10/02/2013 : re-incorporated into binary_c/nucsyn, but for all isotopes,
     *              not just elements (same as the old 'extended' s-process)
     *
     * 28/05/2015 : fix for intel's compiler... sigh.
     *
     * 01/09/2016 : Move data to the store
     *
     * 05/04/2017 : Default to putting the data in with objcopy and use
     *              make_data_objects.sh  (or meson/make_dataobjects.sh)
     *
     *  The data table has the form:
     *
     * Z M c13eff NTP Ag107 Ag109 Ag111 As75 As76 As77 Au197 Au198 Au199 Ba134 Ba135 Ba136 Ba137 Ba138 Ba139 Ba140 Bi209 Bi210 Bi999 Br79 Br80 Br81 Br82 Br83 Cd108 Cd109 Cd110 Cd111 Cd112 Cd113 Cd114 Cd115 Cd116 Cd117 Ce140 Ce141 Ce142 Ce143 Co59 Co60 Cs133 Cs134 Cs135 Cs136 Cs137 Cu63 Cu64 Cu65 Cu67 Dy160 Dy161 Dy162 Dy163 Dy164 Dy165 Dy166 Er164 Er165 Er166 Er167 Er168 Er169 Er170 Er171 Er172 Eu151 Eu152 Eu153 Eu154 Eu155 Eu156 Eu157 Fe56 Fe57 Fe58 Fe59 Fe60 Ga69 Ga71 Ga72 Ga73 Gd152 Gd153 Gd154 Gd155 Gd156 Gd157 Gd158 Gd159 Gd160 Ge70 Ge71 Ge72 Ge73 Ge74 Ge76 Hf176 Hf177 Hf178 Hf179 Hf180 Hf181 Hf182 Hg198 Hg199 Hg200 Hg201 Hg202 Hg203 Hg204 Ho163 Ho164 Ho165 Ho166 Ho167 I127 I128 I129 I130 I131 In113 In115 In117 Ir191 Ir192 Ir193 Ir194 Ir195 Kr80 Kr81 Kr82 Kr83 Kr84 Kr85 Kr86 La139 La140 La141 Lu175 Lu176 Lu177 Lu178 Mo100 Mo92 Mo93 Mo94 Mo95 Mo96 Mo97 Mo98 Mo99 Nb93 Nb94 Nb95 Nb96 Nd142 Nd143 Nd144 Nd145 Nd146 Nd147 Nd148 Nd149 Nd150 Ni58 Ni59 Ni60 Ni61 Ni62 Ni63 Ni64 Os186 Os187 Os188 Os189 Os190 Os191 Os192 Os193 Os194 Pb204 Pb205 Pb206 Pb207 Pb208 Pd104 Pd105 Pd106 Pd107 Pd108 Pd109 Pd110 Pm145 Pm146 Pm147 Pm148 Pm149 Po210 Pr141 Pr142 Pr143 Pt192 Pt193 Pt194 Pt195 Pt196 Pt197 Pt198 Rb85 Rb86 Rb87 Re185 Re186 Re187 Re188 Rh103 Rh105 Ru100 Ru101 Ru102 Ru103 Ru104 Ru96 Ru97 Ru98 Ru99 Sb121 Sb122 Sb123 Sb124 Sb125 Se76 Se77 Se78 Se79 Se80 Se82 Sm144 Sm145 Sm146 Sm147 Sm148 Sm149 Sm150 Sm151 Sm152 Sm153 Sm154 Sn114 Sn115 Sn116 Sn117 Sn118 Sn119 Sn120 Sn121 Sn122 Sn123 Sn124 Sn888 Sr86 Sr87 Sr88 Sr89 Sr90 Ta179 Ta180 Ta181 Ta182 Ta183 Ta184 Ta888 Tb159 Tb160 Tb161 Tc97 Tc98 Tc99 Te122 Te123 Te124 Te125 Te126 Te127 Te128 Te130 Tl203 Tl204 Tl205 Tm169 Tm170 Tm171 Tm172 W180 W181 W182 W183 W184 W185 W186 W187 W188 Xe128 Xe129 Xe130 Xe131 Xe132 Xe133 Xe134 Xe135 Xe136 Y89 Y90 Y91 Yb170 Yb171 Yb172 Yb173 Yb174 Yb175 Yb176 Zn64 Zn65 Zn66 Zn67 Zn68 Zn70 Zr90 Zr91 Zr92 Zr93 Zr94 Zr95 Zr96
     *
     */

    // result of the interpolation
    double sint[NUCSYN_EXTENDED_S_PROCESS_NUM_FITTED_ISOTOPES+2];

    // parameters
    double x[]={
        Max(1e-4,Min(0.02,z)),
        Max(1.5,Min(5.0,newstar->mc_1tp + newstar->menv_1tp)),
        Max(1.0/16,Min(2.9,stardata->preferences->c13_eff)),
        Max(2.0,Min(28,newstar->num_thermal_pulses_since_mcmin))
    };

    /*
     * Use linear interpolation over Maria's data to calculate the
     * s-process abundances (3rd arg is 4 if you're not interpolating Zr)
     */

    Interpolate(stardata->store->s_process_Gallino,
                x,
                sint,
                INTERPOLATE_USE_CACHE);


    /* Radioactive decay of elements in s */
    {
        double tip=newstar->interpulse_period*1e6; // interpulse period in years
        DECAY(165,1.41e5,38);
        //DECAY(234,4.59e3,19); // Gallino says eliminato Sep, 25, 1998
        DECAY(129,2.94e5,21);
        DECAY(330,2.35e5,151);
        DECAY(227,1.8e5,225);
        DECAY(42,4.06e6,10);
        DECAY(172,3.81e3,213);
    }
    /*
     * Here's a correction for dealing with the size of the C13 pocket
     * and of the intershell in maria's tables (b-hhh...! )
     *
     * Update: put the ratio in the preferences array.
     * It is DEFAULT_MC13_FRACTION by default.
     */
    double mc13_pocket_ratio=
        stardata->preferences->mc13_pocket_multiplier*
        DEFAULT_MC13_FRACTION*
        dm_intershell(newstar->core_mass[CORE_He])/STD_M_C13_POCKET;

    Isotope i;
    for(i=0;i<NUCSYN_EXTENDED_S_PROCESS_NUM_FITTED_ISOTOPES;i++)
    {
        sint[i]=Max(1e-30,sint[i]*mc13_pocket_ratio);
    }

    Dprint("Decays done\n");

    /*
     * Set material in the dredge up region
     */
    dup_material[XFe56]=sint[72];
#ifdef NUCSYN_ALL_ISOTOPES
    dup_material[XAg107]=sint[0];
    dup_material[XAg109]=sint[1];
    dup_material[XAg111]=sint[2];
    dup_material[XAs75]=sint[3];
    dup_material[XAs76]=sint[4];
    dup_material[XAs77]=sint[5];
    dup_material[XAu197]=sint[6];
    dup_material[XAu198]=sint[7];
    dup_material[XAu199]=sint[8];
    dup_material[XBa134]=sint[9];
    dup_material[XBa135]=sint[10];
    dup_material[XBa136]=sint[11];
    dup_material[XBa137]=sint[12];
    dup_material[XBa138]=sint[13];
    dup_material[XBa139]=sint[14];
    dup_material[XBa140]=sint[15];
    dup_material[XBi209]=sint[16];
    dup_material[XBi210]=sint[17];
    dup_material[XBi210]+=sint[18]; // WTF is this? it was Bi999! end of the network?
    dup_material[XBr79]=sint[19];
    dup_material[XBr80]=sint[20];
    dup_material[XBr81]=sint[21];
    dup_material[XBr82]=sint[22];
    dup_material[XBr83]=sint[23];
    dup_material[XCd108]=sint[24];
    dup_material[XCd109]=sint[25];
    dup_material[XCd110]=sint[26];
    dup_material[XCd111]=sint[27];
    dup_material[XCd112]=sint[28];
    dup_material[XCd113]=sint[29];
    dup_material[XCd114]=sint[30];
    dup_material[XCd115]=sint[31];
    dup_material[XCd116]=sint[32];
    dup_material[XCd117]=sint[33];
    dup_material[XCe140]=sint[34];
    dup_material[XCe141]=sint[35];
    dup_material[XCe142]=sint[36];
    dup_material[XCe143]=sint[37];
    dup_material[XCo59]=sint[38];
    dup_material[XCo60]=sint[39];
    dup_material[XCs133]=sint[40];
    dup_material[XCs134]=sint[41];
    dup_material[XCs135]=sint[42];
    dup_material[XCs136]=sint[43];
    dup_material[XCs137]=sint[44];
    dup_material[XCu63]=sint[45];
    dup_material[XCu64]=sint[46];
    dup_material[XCu65]=sint[47];
    dup_material[XCu67]=sint[48];
    dup_material[XDy160]=sint[49];
    dup_material[XDy161]=sint[50];
    dup_material[XDy162]=sint[51];
    dup_material[XDy163]=sint[52];
    dup_material[XDy164]=sint[53];
    dup_material[XDy165]=sint[54];
    dup_material[XDy166]=sint[55];
    dup_material[XEr164]=sint[56];
    dup_material[XEr165]=sint[57];
    dup_material[XEr166]=sint[58];
    dup_material[XEr167]=sint[59];
    dup_material[XEr168]=sint[60];
    dup_material[XEr169]=sint[61];
    dup_material[XEr170]=sint[62];
    dup_material[XEr171]=sint[63];
    dup_material[XEr172]=sint[64];
    dup_material[XEu151]=sint[65];
    dup_material[XEu152]=sint[66];
    dup_material[XEu153]=sint[67];
    dup_material[XEu154]=sint[68];
    dup_material[XEu155]=sint[69];
    dup_material[XEu156]=sint[70];
    dup_material[XEu157]=sint[71];

    dup_material[XFe57]=sint[73];
    dup_material[XFe58]=sint[74];
    dup_material[XFe59]=sint[75];
    dup_material[XFe60]=sint[76];
    dup_material[XGa69]=sint[77];
    dup_material[XGa71]=sint[78];
    dup_material[XGa72]=sint[79];
    dup_material[XGa73]=sint[80];
    dup_material[XGd152]=sint[81];
    dup_material[XGd153]=sint[82];
    dup_material[XGd154]=sint[83];
    dup_material[XGd155]=sint[84];
    dup_material[XGd156]=sint[85];
    dup_material[XGd157]=sint[86];
    dup_material[XGd158]=sint[87];
    dup_material[XGd159]=sint[88];
    dup_material[XGd160]=sint[89];
    dup_material[XGe70]=sint[90];
    dup_material[XGe71]=sint[91];
    dup_material[XGe72]=sint[92];
    dup_material[XGe73]=sint[93];
    dup_material[XGe74]=sint[94];
    dup_material[XGe76]=sint[95];
    dup_material[XHf176]=sint[96];
    dup_material[XHf177]=sint[97];
    dup_material[XHf178]=sint[98];
    dup_material[XHf179]=sint[99];
    dup_material[XHf180]=sint[100];
    dup_material[XHf181]=sint[101];
    dup_material[XHf182]=sint[102];
    dup_material[XHg198]=sint[103];
    dup_material[XHg199]=sint[104];
    dup_material[XHg200]=sint[105];
    dup_material[XHg201]=sint[106];
    dup_material[XHg202]=sint[107];
    dup_material[XHg203]=sint[108];
    dup_material[XHg204]=sint[109];
    dup_material[XHo163]=sint[110];
    dup_material[XHo164]=sint[111];
    dup_material[XHo165]=sint[112];
    dup_material[XHo166]=sint[113];
    dup_material[XHo167]=sint[114];
    dup_material[XI127]=sint[115];
    dup_material[XI128]=sint[116];
    dup_material[XI129]=sint[117];
    dup_material[XI130]=sint[118];
    dup_material[XI131]=sint[119];
    dup_material[XIn113]=sint[120];
    dup_material[XIn115]=sint[121];
    dup_material[XIn117]=sint[122];
    dup_material[XIr191]=sint[123];
    dup_material[XIr192]=sint[124];
    dup_material[XIr193]=sint[125];
    dup_material[XIr194]=sint[126];
    dup_material[XIr195]=sint[127];
    dup_material[XKr80]=sint[128];
    dup_material[XKr81]=sint[129];
    dup_material[XKr82]=sint[130];
    dup_material[XKr83]=sint[131];
    dup_material[XKr84]=sint[132];
    dup_material[XKr85]=sint[133];
    dup_material[XKr86]=sint[134];
    dup_material[XLa139]=sint[135];
    dup_material[XLa140]=sint[136];
    dup_material[XLa141]=sint[137];
    dup_material[XLu175]=sint[138];
    dup_material[XLu176]=sint[139];
    dup_material[XLu177]=sint[140];
    dup_material[XLu178]=sint[141];
    dup_material[XMo100]=sint[142];
    dup_material[XMo92]=sint[143];
    dup_material[XMo93]=sint[144];
    dup_material[XMo94]=sint[145];
    dup_material[XMo95]=sint[146];
    dup_material[XMo96]=sint[147];
    dup_material[XMo97]=sint[148];
    dup_material[XMo98]=sint[149];
    dup_material[XMo99]=sint[150];
    dup_material[XNb93]=sint[151];
    dup_material[XNb94]=sint[152];
    dup_material[XNb95]=sint[153];
    dup_material[XNb96]=sint[154];
    dup_material[XNd142]=sint[155];
    dup_material[XNd143]=sint[156];
    dup_material[XNd144]=sint[157];
    dup_material[XNd145]=sint[158];
    dup_material[XNd146]=sint[159];
    dup_material[XNd147]=sint[160];
    dup_material[XNd148]=sint[161];
    dup_material[XNd149]=sint[162];
    dup_material[XNd150]=sint[163];
    dup_material[XNi58]=sint[164];
    dup_material[XNi59]=sint[165];
    dup_material[XNi60]=sint[166];
    dup_material[XNi61]=sint[167];
    dup_material[XNi62]=sint[168];
    dup_material[XNi63]=sint[169];
    dup_material[XNi64]=sint[170];
    dup_material[XOs186]=sint[171];
    dup_material[XOs187]=sint[172];
    dup_material[XOs188]=sint[173];
    dup_material[XOs189]=sint[174];
    dup_material[XOs190]=sint[175];
    dup_material[XOs191]=sint[176];
    dup_material[XOs192]=sint[177];
    dup_material[XOs193]=sint[178];
    dup_material[XOs194]=sint[179];
    dup_material[XPb204]=sint[180];
    dup_material[XPb205]=sint[181];
    dup_material[XPb206]=sint[182];
    dup_material[XPb207]=sint[183];
    dup_material[XPb208]=sint[184];
    dup_material[XPd104]=sint[185];
    dup_material[XPd105]=sint[186];
    dup_material[XPd106]=sint[187];
    dup_material[XPd107]=sint[188];
    dup_material[XPd108]=sint[189];
    dup_material[XPd109]=sint[190];
    dup_material[XPd110]=sint[191];
    dup_material[XPm145]=sint[192];
    dup_material[XPm146]=sint[193];
    dup_material[XPm147]=sint[194];
    dup_material[XPm148]=sint[195];
    dup_material[XPm149]=sint[196];
    dup_material[XPo210]=sint[197];
    dup_material[XPr141]=sint[198];
    dup_material[XPr142]=sint[199];
    dup_material[XPr143]=sint[200];
    dup_material[XPt192]=sint[201];
    dup_material[XPt193]=sint[202];
    dup_material[XPt194]=sint[203];
    dup_material[XPt195]=sint[204];
    dup_material[XPt196]=sint[205];
    dup_material[XPt197]=sint[206];
    dup_material[XPt198]=sint[207];
    dup_material[XRb85]=sint[208];
    dup_material[XRb86]=sint[209];
    dup_material[XRb87]=sint[210];
    dup_material[XRe185]=sint[211];
    dup_material[XRe186]=sint[212];
    dup_material[XRe187]=sint[213];
    dup_material[XRe188]=sint[214];
    dup_material[XRh103]=sint[215];
    dup_material[XRh105]=sint[216];
    dup_material[XRu100]=sint[217];
    dup_material[XRu101]=sint[218];
    dup_material[XRu102]=sint[219];
    dup_material[XRu103]=sint[220];
    dup_material[XRu104]=sint[221];
    dup_material[XRu96]=sint[222];
    dup_material[XRu97]=sint[223];
    dup_material[XRu98]=sint[224];
    dup_material[XRu99]=sint[225];
    dup_material[XSb121]=sint[226];
    dup_material[XSb122]=sint[227];
    dup_material[XSb123]=sint[228];
    dup_material[XSb124]=sint[229];
    dup_material[XSb125]=sint[230];
    dup_material[XSe76]=sint[231];
    dup_material[XSe77]=sint[232];
    dup_material[XSe78]=sint[233];
    dup_material[XSe79]=sint[234];
    dup_material[XSe80]=sint[235];
    dup_material[XSe82]=sint[236];
    dup_material[XSm144]=sint[237];
    dup_material[XSm145]=sint[238];
    dup_material[XSm146]=sint[239];
    dup_material[XSm147]=sint[240];
    dup_material[XSm148]=sint[241];
    dup_material[XSm149]=sint[242];
    dup_material[XSm150]=sint[243];
    dup_material[XSm151]=sint[244];
    dup_material[XSm152]=sint[245];
    dup_material[XSm153]=sint[246];
    dup_material[XSm154]=sint[247];
    dup_material[XSn114]=sint[248];
    dup_material[XSn115]=sint[249];
    dup_material[XSn116]=sint[250];
    dup_material[XSn117]=sint[251];
    dup_material[XSn118]=sint[252];
    dup_material[XSn119]=sint[253];
    dup_material[XSn120]=sint[254];
    dup_material[XSn121]=sint[255];
    dup_material[XSn122]=sint[256];
    dup_material[XSn123]=sint[257];
    dup_material[XSn124]=sint[258];
    dup_material[XSn124]+=sint[259]; // er... WTF is this?
    dup_material[XSr86]=sint[260];
    dup_material[XSr87]=sint[261];
    dup_material[XSr88]=sint[262];
    dup_material[XSr89]=sint[263];
    dup_material[XSr90]=sint[264];
    dup_material[XTa179]=sint[265];
    dup_material[XTa180]=sint[266];
    dup_material[XTa181]=sint[267];
    dup_material[XTa182]=sint[268];
    dup_material[XTa183]=sint[269];
    dup_material[XTa184]=sint[270];
    dup_material[XTa184]+=sint[271]; // er... WTF is this?
    dup_material[XTb159]=sint[272];
    dup_material[XTb160]=sint[273];
    dup_material[XTb161]=sint[274];
    dup_material[XTc97]=sint[275];
    dup_material[XTc98]=sint[276];
    dup_material[XTc99]=sint[277];
    dup_material[XTe122]=sint[278];
    dup_material[XTe123]=sint[279];
    dup_material[XTe124]=sint[280];
    dup_material[XTe125]=sint[281];
    dup_material[XTe126]=sint[282];
    dup_material[XTe127]=sint[283];
    dup_material[XTe128]=sint[284];
    dup_material[XTe130]=sint[285];
    dup_material[XTl203]=sint[286];
    dup_material[XTl204]=sint[287];
    dup_material[XTl205]=sint[288];
    dup_material[XTm169]=sint[289];
    dup_material[XTm170]=sint[290];
    dup_material[XTm171]=sint[291];
    dup_material[XTm172]=sint[292];
    dup_material[XW180]=sint[293];
    dup_material[XW181]=sint[294];
    dup_material[XW182]=sint[295];
    dup_material[XW183]=sint[296];
    dup_material[XW184]=sint[297];
    dup_material[XW185]=sint[298];
    dup_material[XW186]=sint[299];
    dup_material[XW187]=sint[300];
    dup_material[XW188]=sint[301];
    dup_material[XXe128]=sint[302];
    dup_material[XXe129]=sint[303];
    dup_material[XXe130]=sint[304];
    dup_material[XXe131]=sint[305];
    dup_material[XXe132]=sint[306];
    dup_material[XXe133]=sint[307];
    dup_material[XXe134]=sint[308];
    dup_material[XXe135]=sint[309];
    dup_material[XXe136]=sint[310];
    dup_material[XY89]=sint[311];
    dup_material[XY90]=sint[312];
    dup_material[XY91]=sint[313];
    dup_material[XYb170]=sint[314];
    dup_material[XYb171]=sint[315];
    dup_material[XYb172]=sint[316];
    dup_material[XYb173]=sint[317];
    dup_material[XYb174]=sint[318];
    dup_material[XYb175]=sint[319];
    dup_material[XYb176]=sint[320];
    dup_material[XZn64]=sint[321];
    dup_material[XZn65]=sint[322];
    dup_material[XZn66]=sint[323];
    dup_material[XZn67]=sint[324];
    dup_material[XZn68]=sint[325];
    dup_material[XZn70]=sint[326];
    dup_material[XZr90]=sint[327];
    dup_material[XZr91]=sint[328];
    dup_material[XZr92]=sint[329];
    dup_material[XZr93]=sint[330];
    dup_material[XZr94]=sint[331];
    dup_material[XZr95]=sint[332];
    dup_material[XZr96]=sint[333];
#endif//NUCSYN_ALL_ISOTOPES
    Dprint("Dup material all set: return\n");
}


#endif   //NUCSYN && NUCSYN_S_PROCESS
