#include "../binary_c.h"
No_empty_translation_unit_warning;


#if (defined NUCSYN && defined NUCSYN_WR_TABLES)
// data tables

#ifdef NUCSYN_WR_RS_TABLE_NeNa_ONLY
#include "rs_NeNa.h"
#else
#include "rs.h"
#endif //NUCSYN_WR_RS_TABLE_NeNa_ONLY

/*
 * The part of the WR code which does the lookups : separately
 * to speed up compilation
 *
 * This version is for Richard Stancliffe's table
 */

void nucsyn_WR_RS_table_lookup(struct stardata_t * Restrict const stardata,
                               double * Restrict const x,
                               Abundance * Restrict const r)
{
    /*
     * This array never changes so can be static
     */
    Const_data_table rs_data[]={NUCSYN_RS_DATA};

    rinterpolate(rs_data,
                 stardata->persistent_data->rinterpolate_data,
                 3,
#ifdef NUCSYN_WR_RS_TABLE_NeNa_ONLY
                 2,
#else
                 44,
#endif //NUCSYN_WR_RS_TABLE_NeNa_ONLY
                 NUCSYN_RS_DATA_LINES,
                 x,
                 r,
                 1);

}

#endif //NUCSYN_WR_TABLES && NUCSYN
