#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN
static void _process_data_table(struct data_table_t * const table);
static struct data_table_t * _load_table(struct stardata_t * const stardata,
                                         const size_t n);

double nucsyn_sn_Gronow2021b(struct stardata_t * const stardata,
                             Abundance * Restrict const X,
                             const double metallicity,
                             const double mcore,
                             const double mshell)
{
    /*
     * Yields of Gronow et al. (2021)
     * b) A&A 656, A94 (varying metallicity)
     */
    if(stardata->persistent_data->Gronow2021b_core_ejecta == NULL)
    {
        /*
         * First, load the Gromow2021a data for solar metallicity
         * and set its metallicity in the leading column to Solar
         * (Asplund et al. 2009, i.e. Zsolar = 0.0142)
         */
        nucsyn_sn_Gronow2021a(stardata,NULL,0.0,0.0);

        /*
         * Copy the 2021a solar-metallicity data into the 2021b tables.
         * This also makes the shell_ejecta and core_ejecta tables
         * that we'll fill below.
         */
        struct data_table_t * shell_ejecta = copy_data_table(stardata->persistent_data->Gronow2021a_core_ejecta);
        struct data_table_t * core_ejecta = copy_data_table(stardata->persistent_data->Gronow2021a_shell_ejecta);
        Delete_data_table(stardata->persistent_data->Gronow2021a_core_ejecta);
        Delete_data_table(stardata->persistent_data->Gronow2021a_shell_ejecta);

        /* first column should be metallicity */
        add_leading_columns_to_data_table(core_ejecta,1);
        set_data_table_column(core_ejecta,0,0.0142);
        add_leading_columns_to_data_table(shell_ejecta,1);
        set_data_table_column(shell_ejecta,0,0.0142);

        /*
         * Secondly, load the Gromow2021b data at other metallicities
         */

        /*
         * Get the isotope list
         */
        Isotope * isotope_list = NULL;
        size_t nlines,length,maxwidth;

        char * files[] = {
            "src/supernovae/Gronow2021b/table1.dat",
        };
        size_t isotope_offset = 0;
        for(size_t nfile=0; nfile<Array_size(files); nfile++)
        {
            /*
             * Get list of isotopes
             */
            char * file = files[nfile];
            struct string_array_t ** strings =
                load_data_as_strings(stardata,
                                     file,
                                     &nlines,
                                     &length,
                                     &maxwidth,
                                     ' ',
                                     0,
                                     0);

            isotope_list = Realloc(isotope_list,
                                   sizeof(Isotope)*(isotope_offset+nlines));
            for(size_t i=0; i<nlines; i++)
            {
                char * s = strings[i]->strings[0];
                size_t ndigits = 0;
                size_t nalpha = 0;

                while(isdigit(s[ndigits]))
                {
                    ndigits++;
                }
                while(isalpha(s[nalpha + ndigits]))
                {
                    nalpha++;
                }
                char isotope[6] = {'\0'};
                memcpy(isotope,
                       s + ndigits,
                       nalpha*sizeof(char));
                memcpy(isotope + nalpha,
                       s,
                       ndigits*sizeof(char));
                Isotope n_isotope = Xn;

                /*
                 * linear search : slow but we
                 * do it only once per isotope
                 */
                for(size_t j=0; j<ISOTOPE_ARRAY_SIZE; j++)
                {
                    if(Strings_equal(nucsyn_isotope_strings[j],
                                     isotope))
                    {
                        n_isotope = j;
                        break;
                    }
                }

                /*
                 * Now, if the isotope is Xn, we know it has no
                 * match and should be ignored
                 */
                Dprint("%zu %s : ndigits %zu, nalpha %zu : isotope \"%s\" %u\n",
                       i,
                       s,
                       ndigits,
                       nalpha,
                       isotope,
                       n_isotope);

                isotope_list[i+isotope_offset] = n_isotope;
            }
            isotope_offset += nlines;
            free_array_of_string_arrays(&strings,
                                        nlines);
        }

        /*
         * Metallicities relative to solar
         */
        const double metallicities[] = {0.001,0.01,3};
        const double metadata[][3] =
            {
                /* MCO, Mshell, Mshell (at detonation) */
                {0.803, 0.028, 0.040}, // table 1 M08_03*
                {0.803, 0.053, 0.075}, // table 2 M08_05*
                {0.795, 0.109, 0.109}, // table 3 M08_10*
                {0.905, 0.026, 0.043}, // table 4 M09_03*
                {0.899, 0.053, 0.074}, // table 5 M09_05*
                {0.888, 0.108, 0.108}, // table 6 M09_10*
                {1.005, 0.020, 0.028}, // table 7 M10_02*
                {1.028, 0.027, 0.047}, // table 8 M10_03*
                {1.002, 0.052, 0.074}, // table 9 M10_05*
                {1.105, 0.090, 0.133}, // table 10 M10_10*
                {1.100, 0.054, 0.123}  // table 11 M11_05*
            };

        /* loop over tables */
        for(size_t itable=0; itable<Array_size(metadata); itable++)
        {
            /* load in data */
            const size_t file_number = itable + 1;
            struct data_table_t * table = _load_table(stardata,
                                                      file_number);
            const size_t width = table->ndata + table->nparam;

            /* prepend metadata */
            const double MCO = ((int)(10.0*(0.05+metadata[itable][0])))/10.0;
            const double Mshell = metadata[itable][1];
            set_data_table_column(table,1,MCO);
            set_data_table_column(table,2,Mshell);

            /* prepend metallicities */
            for(size_t iZ=0; iZ<Array_size(metallicities); iZ++)
            {
                const double Z = 0.0142*metallicities[iZ];
                *(table->data +    2*iZ *width + 0) = Z;
                *(table->data + (1+2*iZ)*width + 0) = Z;
            }

            /*
             * join to the core and shell tables, selecting every
             * other row of the new data to choose core or shell
             * respectively
             */
            join_data_tables_in_place(stardata,
                                      shell_ejecta,
                                      table,
                                      0,1,
                                      0,2);

            join_data_tables_in_place(stardata,
                                      core_ejecta,
                                      table,
                                      0,1,
                                      1,2);

            /* free temporary table */
            Delete_data_table(table);
        }

        /*
         * Sort tables
         */
#define _sort(TABLE)                                \
        {                                           \
            struct data_table_t * sorted_table;     \
            sorted_table = sort_data_table(TABLE);  \
            memcpy(TABLE->data,                     \
                   sorted_table->data,              \
                   Data_table_data_size(TABLE));    \
            Delete_data_table(sorted_table);        \
        }

        _sort(shell_ejecta);
        _sort(core_ejecta);

        /*
         * Dummy interpolation to acquire rinterpolate tables
         */
        Interpolate(core_ejecta,NULL,NULL,FALSE);
        Interpolate(shell_ejecta,NULL,NULL,FALSE);

        /*
         * Acquire the tables
         */
        struct rinterpolate_table_t * unmapped_core_ejecta =
            rinterpolate_table_struct(stardata->persistent_data->rinterpolate_data,
                                      core_ejecta->data,
                                      NULL);

        struct rinterpolate_table_t * unmapped_shell_ejecta =
            rinterpolate_table_struct(stardata->persistent_data->rinterpolate_data,
                                      shell_ejecta->data,
                                      NULL);


        /*
         * Make mapped data tables
         *
         * We only need to map the shell masses in
         * the third column (number 2)
         */
        Boolean change_resolution[] = { FALSE, FALSE, TRUE };
        double new_resolutions_f[] = { 0, 0, 10 };


        struct rinterpolate_table_t * const mapped_core_ejecta =
            rinterpolate_map_right_columns(stardata->persistent_data->rinterpolate_data,
                                           unmapped_core_ejecta,
                                           change_resolution,
                                           new_resolutions_f,
                                           NULL);

        if(mapped_core_ejecta == NULL)
        {
            Exit_binary_c(2,"map of core ejecta failed\n");
        }


        struct rinterpolate_table_t * const mapped_shell_ejecta =
            rinterpolate_map_right_columns(stardata->persistent_data->rinterpolate_data,
                                           unmapped_shell_ejecta,
                                           change_resolution,
                                           new_resolutions_f,
                                           NULL);

        if(mapped_shell_ejecta == NULL || mapped_shell_ejecta->n == 0)
        {
            Exit_binary_c(2,"map of shell ejecta failed\n");
        }

        /*
         * Free temporary tables
         */
        Delete_data_table(shell_ejecta);
        Delete_data_table(core_ejecta);

        /*
         * Put tables and metadata in the store
         */
        NewDataTable_from_Pointer(mapped_core_ejecta->data,
                                  stardata->persistent_data->Gronow2021b_core_ejecta,
                                  mapped_core_ejecta->n,
                                  mapped_core_ejecta->d,
                                  mapped_core_ejecta->l);
        NewDataTable_from_Pointer(mapped_shell_ejecta->data,
                                  stardata->persistent_data->Gronow2021b_shell_ejecta,
                                  mapped_shell_ejecta->n,
                                  mapped_shell_ejecta->d,
                                  mapped_shell_ejecta->l);
        stardata->persistent_data->Gronow2021b_isotope_list = isotope_list;
        stardata->persistent_data->Gronow2021b_nisotopes = isotope_offset;
        Dprint("We have an isotope list of %zu isotopes\n",isotope_offset);
    }

    /* clear X mass fractions array */
    Clear_isotope_array(X);

    if(mcore+mshell > 0.0)
    {
        /*
         * Interpolate for SNIa yields
         */
        const double x[] = {
            metallicity,
            mcore,
            mshell
        };

        /* space for result of interpolation */
        double * r = Malloc(stardata->persistent_data->Gronow2021b_core_ejecta->ndata*sizeof(double));

        /* get core ejecta */
        Interpolate(stardata->persistent_data->Gronow2021b_core_ejecta,
                    x,
                    r,
                    FALSE);

        /* set in isotopes */
        Isotope * const list = stardata->persistent_data->Gronow2021b_isotope_list;
        const double frac = mcore / (mcore + mshell);
        for(size_t i=0; i<stardata->persistent_data->Gronow2021b_nisotopes; i++)
        {
            if(list[i] != Xn)
            {
                X[list[i]] += r[i] * frac;
            }
        }

        /* helium shell */
        Interpolate(stardata->persistent_data->Gronow2021b_shell_ejecta,
                    x,
                    r,
                    FALSE);

        /* set in isotopes */
        const double frac1 = 1.0 - frac;
        for(size_t i=0; i<stardata->persistent_data->Gronow2021b_nisotopes; i++)
        {
            if(list[i] != Xn)
            {
                X[list[i]] += r[i] * frac1;
            }
        }
        Safe_free(r);

        /*
         * Enforce sum of isotopes = 1
         */
        nucsyn_renormalize_abundance(X);

        /*
        printf("SN Ia DD yields : { %g, %g, %g }\n",x[0],x[1],x[2]);
        for(Isotope i=0; i<ISOTOPE_ARRAY_SIZE; i++)
        {
            if(Is_not_zero(X[i]))
            {
                printf("%s = %g\n",
                       nucsyn_isotope_strings[i],
                       X[i]);
            }
        }
        printf("total %g\n",nucsyn_totalX(stardata,X));
        */
    }

    /* return mass ejected */
    return mcore + mshell;
}

static void _process_data_table(struct data_table_t * const table)
{
    /*
     * Process a data table:
     * 1) normalize columns to be mass fractions
     * 2) transpose
     */

    /* normalize yields to mass fractions */
    const size_t width = table->nparam + table->ndata;
    for(size_t col=table->nparam; col<width; col++)
    {
        double * const p0 = table->data + col;
        double * p = p0;
        double sum = 0.0;
        for(size_t row=0; row<table->nlines; row++)
        {
            sum += *p;
            p += width;
        }
        p = p0;
        for(size_t row=0; row<table->nlines; row++)
        {
            *p /= sum;
            p += width;
        }
    }

    transpose_data_table(table,table->nparam);
    remove_data_table_leading_rows(table,1);
    add_leading_columns_to_data_table(table,3);
}


static struct data_table_t * _load_table(struct stardata_t * const stardata,
                                         const size_t n)
{
    struct data_table_t * t;
    New_data_table(t);
    char * filename = NULL;
    if(asprintf(&filename,"src/supernovae/Gronow2021b/table%zu.dat",n))
    {
        Load_table(t,
                   filename,
                   All_cols,
                   0,
                   0,
                   FALSE);
        _process_data_table(t);
    }
    Safe_free(filename);
    return t;
}
#endif // NUCSYN
