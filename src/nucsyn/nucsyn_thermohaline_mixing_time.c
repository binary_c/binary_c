#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef NUCSYN

#undef NUCSYN_THERMOHALINE_MIXING_TIME
#ifdef NUCSYN_THERMOHALINE_MIXING_TIME
Constant_function double nucsyn_thermohaline_mixing_time(const double mu0,
                                                         const double mu1,
                                                         const double dmacc,
                                                         const double m,
                                                         const double l,
                                                         const double r)
{
    return(0.0);


    /* Function to estimate the thermohaline mixing time */

    /*
     * mu0 is the mean molecular weight of the region below that corresponding
     * to mu1, and mu1>mu0 is assumed (e.g. He on H). dmacc is the mass of the
     * accretion layer, m,l and r are the total mass, luminosity and radius of
     * the star.
     */

    double w; /* Radius ("width") of thermohaline layer */
    double mufac; /* A function of mu that is about 1 (usually!) */
    double tau; /* the mixing timescale */
    double k;

    /* convert from solar units */
    r*=R_SUN;
    l*=L_SUN;
    m*=M_SUN;
    dmacc*=M_SUN;

    /*
     * Assume the accreted material has density k*density of the star
     * (dubious!)
     */
    k=1.0;
    w=dmacc*r/(3.0*m*k);

    /*
     * calculate the function of mu, this is always about 1 unless mu1 is very
     * similar to mu0
     */
    mufac=0.5*(mu0+mu1)*mu0/(mu1-mu0);

    /* calculate tau */
    tau=(w/r)*cbrt(mufac*(GRAVITATIONAL_CONSTANT*m*m/(2.0*l*r)));
    //fprintf(stderr,"TDIFF w=%g mufac=%g\n",w,mufac);
    return(tau);


}

#endif
#endif
