#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef NUCSYN

#undef MIXDEBUG

#ifdef MIXDEBUG
#define Mprint(...) Dprint(__VA_ARGS__)
#else
#define Mprint(...)
#endif

void nucsyn_remove_dm_from_surface(
    struct stardata_t * Restrict const stardata Maybe_unused,
    struct star_t * Restrict const star,
    const double dm,
    Abundance ** Restrict const X,
    Boolean * Restrict const allocated)
{
    /*
     * Function to remove an amount dm (>=0) from the surface of star DONOR,
     * intelligently decide whether it is supposed to come from the accretion
     * layer or from the envelope, and return the abundance of the amount lost
     * in *X.
     *
     * We do NOT actually remove the mass, just set the abundances in X,
     * which are a copy of the appropriate abundances.
     *
     * Note that X must NOT be NULL, it must be allocated memory.
     */
    if(dm < DM_TINY)
    {
        /* need to set an amount in X, to stop valgrind failures */
        Mprint("valgrind proof line");
        *X = Is_not_zero(star->dmacc) ? star->Xacc : star->Xenv;
        *allocated = FALSE;
    }
    else
    {
        Mprint("In nucsyn_remove_dm_from_surface dm=%g\n");

        if(dm < TINY)
        {
            Mprint("dm<TINY so remove directly from the surface\n");
            *X = star->Xenv;
            *allocated = FALSE;
        }
        else if(star->dmacc > TINY)
        {
            /* Accretion layer is present */
            if(dm > star->dmacc)
            {
                Mprint("Removing all of the accretion layer (%g) and some from the surface (%g) \n",star->dmacc,dm-star->dmacc);
                /*
                 * dm is larger than the accretion layer, so take some from the
                 * envelope
                 */
                *X = New_isotope_array;
                nucsyn_dilute_shell_to(
                    /* All of the accretion layer */
                    star->dmacc,
                    star->Xacc,
                    /* Excess from the envelope */
                    dm - star->dmacc,
                    star->Xenv,
                    *X);
                *allocated = TRUE;
            }
            else
            {
                Mprint("Removing all of dm from accretion layer\n");
                /*
                 * We can take all the mass straight from the accretion layer of
                 * the DONOR
                 */
                *X = star->Xacc;
                *allocated = FALSE;
             }
        }
        else
        {
            Mprint("Removing all the mass from the envelope\n");

            /* No accretion layer : take all the mass from the envelope */
            *X = star->Xenv;
            *allocated = FALSE;
        }
    }
}


#endif /* NUCSYN */
