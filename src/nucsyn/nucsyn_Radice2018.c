#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN

/*
 * Load in the nucleosynthetic yield data from processed
 * Radice et al. (2018) NSNS merger models.
 */

void nucsyn_Radice2018(struct stardata_t * const stardata,
                       Abundance * const Xej)
{
    if(stardata->persistent_data->Radice2018_ejecta == NULL)
    {
        init_Radice2018(stardata);
    }

    const Star_number i = stardata->star[0].mass > stardata->star[1].mass ? 0 : 1;
    const double x[] = {
        stardata->star[i].mass, /* most massive mass */
        stardata->star[Other_star(i)].mass /* least massive mass */
    };
    double * XNSNS = Malloc(sizeof(double) * stardata->persistent_data->Radice2018_ejecta->ndata);
    Clear_isotope_array(Xej);
    Interpolate(stardata->persistent_data->Radice2018_ejecta,
                x,
                XNSNS,
                FALSE);
    memcpy(Xej,
           XNSNS+1, /* skip first number which is the mass ejected */
           ISOTOPE_MEMSIZE);
    nucsyn_renormalize_abundance(Xej);
    Safe_free(XNSNS);
}
#endif//NUCSYN
