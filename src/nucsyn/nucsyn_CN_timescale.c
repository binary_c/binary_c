#include "../binary_c.h"
No_empty_translation_unit_warning;


double Constant_function nucsyn_CN_timescale(const double t,
                                             const double NH1,
                                             const double f)
{
    /*
     * Approximate the time it takes to change the C12 abundance
     * by a significant (but <<1) fraction f
     */

    /* for low temperature, assume a long time */
    if(t<1e4)
    {
        return 1e100;
    }
    else
        /* for high temperature, assume a short time */
        if(t>1e9)
        {
            return TINY;
        }
        else
        {
            double sigmav;
            /*
             * fit for temperature range 1e5 to 1e9K, good to within 4.39% (mean error
             * 0.76%, RMS error 0.4027), quicker to calculate than the NACRE
             * formula and just as accurate!
             */
            double lt=log10(t);
            sigmav=exp10(-1.76240e+03+6.74470e+02*lt+-9.61580e+01*Pow2(lt)+5.82540e+00*Pow3(lt)+-1.19750e-01*Pow4(lt));

/* perhaps use full NACRE reaction rate? */
#ifdef USE_NACRE
            const double t9=t/1e9;
            const double  tm13=pow(t9,(-0.333333333333333));
            const double  t23=Pow2(tm13);
            const double  t93=Pow3(t9);
            const double  t92=Pow2(t9);
            const double  t15=pow(t9,-1.50);
            const double  i9=1.00/t9; // on the assumption that *i9 is quicker than /t9
            sigmav  =2.0e7*t23*
                exp(-13.6920*tm13 - t92*4.72589790) *
                (1.00+9.890*t9-59.80*t92+266.00*t93) +
                t15*(1.0e5*exp(-4.9130*i9) + 4.24e5*exp(-21.620*i9));
            sigmav/=AVOGADRO_CONSTANT;
#endif

            /*
             * Calculate timescale and return
             */
            return(f/(NH1*sigmav));
        }
}
