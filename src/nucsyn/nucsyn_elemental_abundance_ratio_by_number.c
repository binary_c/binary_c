#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN

/*
 * Ratio of two elemental abundances by number
 */
Abundance_ratio nucsyn_elemental_abundance_ratio_by_number(
    const char * Restrict const numerator,
    const char * Restrict const denominator,
    const Abundance * Restrict const X,
    struct stardata_t * const stardata,
    struct store_t * const store)
{
    const Number_density x =
        nucsyn_elemental_abundance_by_number(numerator,
                                             X,
                                             stardata,
                                             store,
                                             1.0);
    const Number_density y =
        nucsyn_elemental_abundance_by_number(denominator,
                                             X,
                                             stardata,
                                             store,
                                             1.0);

    return (Abundance_ratio) (x/y);
}
#endif // NUCSYN
