#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN

#include "nucsyn_WR.h"

WR_type Pure_function nucsyn_WR_type(const struct star_t * Restrict const star)
{
    WR_type wr_type;
    double teff;
#if (DEBUG==1)
    //char *wr_type_strings[]={WR_TYPE_STRINGS};
#endif
    teff = 1000.0*pow((1130.0*star->luminosity/Pow2(Max(star->radius,TINY))),
                      0.25);

    /* Classify the WR stellar type (WN/WC/WO L etc) */

    teff = log10(teff);
    teff = Max(teff,TINY);
#define XNMIN 0.01


    // Ignore the effective temperature clause?
    // because it's dodgy (according to Lynnette)
    if(teff>4.0) // should be  >4.0
    {
        if((star->Xenv[XH1]<0.4)&&(star->Xenv[XH1]>XNMIN))
        {
            wr_type=WR_WNL;
        }
        else if(star->Xenv[XH1]<XNMIN)
        {
            if(WCO_TEST > 1.0)
            {
                wr_type=WR_WO;
            }
            else if(WCO_TEST > 3e-2)
            {
                wr_type=WR_WC;
            }
            else
            {
                wr_type=WR_WNE;
            }
        }
        else
        {
            wr_type=WR_OB;
        }
    }
    else
    {
        wr_type=WR_PRE_OB;
    }

    return wr_type;
}

#endif
