#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN

void nucsyn_free_store_contents(struct store_t * Restrict const store)
{
    /*
     * Free the content in the store.
     * See also build_store_contents.c
     *
     * Note:
     * NOTHING in here can depend on the current stardata.
     * You will NOT have access to stardata here.
     */
    if(store != NULL)
    {
        if(store->icache != NULL)
        {
            nucsyn_elemental_abundance(NULL,NULL,NULL,store);
        }
        nucsyn_element_to_atomic_number(store,NULL,0);
        Safe_free(store->nisotopes);
        Safe_free(store->ionised_molecular_weight_multiplier);
        Safe_free(store->atomic_number);
        Safe_free(store->nucleon_number);
        Safe_free(store->molweight);
        Safe_free(store->mnuc);
        Safe_free(store->mnuc_amu);
        Safe_free(store->imnuc);
        Safe_free(store->imnuc_amu);
        Safe_free(store->ZonA);
        Safe_free(store->condensation_temperatures);

#ifdef FIRST_DREDGE_UP_HOLLY
        Delete_data_table(store->Holly_1DUP_table);
#endif
#ifdef FIRST_DREDGE_UP_EVERT
        Delete_data_table(store->1DUP_table);
#endif
#ifdef NUCSYN_STRIP_AND_MIX
        Delete_data_table(store->TAMS);
#endif
#ifdef NUCSYN_S_PROCESS
        Delete_data_table(store->s_process_Gallino);
#endif
#ifdef NUCSYN_NOVAE
        Delete_data_table_not_contents(store->novae_JH98_CO);
        Delete_data_table_not_contents(store->novae_JH98_ONe);
        Delete_data_table_not_contents(store->novae_Jose2022_ONe);
#endif
#ifdef NUCSYN_SIGMAV_PRE_INTERPOLATE
        Delete_data_table(store->sigmav);
#endif
    }
}

#endif //NUCSYN
