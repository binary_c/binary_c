#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef NUCSYN
/*
 * Mimic a complete CNO burn in abundance array X
 */
void nucsyn_burn_CNO_completely(Abundance * const X)
{
    X[XHe4] += X[XH1];
    X[XH1] = 0.0;
    X[XN14] = 14.0 * (X[XC12]/12.0+X[XC13]/13.0+X[XN14]/14.0+X[XN15]/15.0+X[XO16]/16.0+X[XO17]/17.0+X[XO18]/18.0);
    X[XC12] = X[XC13] = X[XN15] = X[XO16] = X[XO17] = X[XO18] = 0.0;
}
#endif//NUCSYN
