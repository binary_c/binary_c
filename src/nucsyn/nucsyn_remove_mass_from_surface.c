#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN

void nucsyn_remove_mass_from_surface(
    struct stardata_t * stardata,
    struct star_t * Restrict star,
    const double dm,
    Abundance ** Restrict Xp,
    double * Restrict ndmacc,
    Boolean * Restrict allocated)
{
    /*
     * Remove material dm (>0.0) from the star, and
     * set X, the abundance of the material removed,
     * if X is non-NULL.
     *
     * Allocated can be NULL but only if X is NULL.
     */

    ndmacc[star->starnum] = Max(0.0, ndmacc[star->starnum] - dm);
    if(likely(Xp!=NULL))
    {
        nucsyn_remove_dm_from_surface(
            stardata,
            star,
            dm,
            Xp,
            allocated);
    }
    else if(allocated != NULL)
    {
        *allocated = FALSE;
    }
}
#endif //NUCSYN
