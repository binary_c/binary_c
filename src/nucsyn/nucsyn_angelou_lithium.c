#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef NUCSYN_ANGELOU_LITHIUM

/*
 * Functionality to simulate lithium enhancements for
 * George Angelou.
 */

static void nucsyn_angelou_decay_lithium(
    struct stardata_t * const stardata,
    struct star_t * const star
    );

static void react_lithium(struct stardata_t * stardata,
                          const double fac,
                          double * X);

#include "nucsyn_angelou.h"

/*
 * Set debug to 1 for output
 */
#define ANGELOU_DEBUG 0

/* debug statement */
#define Aprint(...)                             \
    if(ANGELOU_DEBUG)                           \
    {                                           \
        fprintf(stdout,                         \
                "%s %d t=%g  ",                 \
                __FILE__,                       \
                __LINE__,                       \
                stardata->model.time);          \
        fprintf(stdout,                         \
                __VA_ARGS__);                   \
    }


void nucsyn_angelou_lithium(struct stardata_t * stardata,
                            struct star_t * star)
{
    /*
     * Lithium is spiked when boost is TRUE.
     */
    int boost = NUCSYN_ANGELOU_LITHIUM_BOOST_NONE;

    /*
     * Boost lithium at a lithium_time relative to the
     * phase start time (star->stellar_type_tstart).
     */
    double lithium_time = angelou_factor(time,
                                         star->stellar_type);
    double phase_time = stardata->model.time - star->stellar_type_tstart;

    if(Is_not_zero(lithium_time) &&
       In_range(lithium_time - phase_time,
                0.0,
                stardata->model.dtm))
    {
        /*
         * We want lithium to spike in this timestep
         */
        boost = NUCSYN_ANGELOU_LITHIUM_BOOST_IN_TIME_RANGE;
        Aprint("Boost lithium by time algorithm : t = %g\n",
               stardata->model.time);
    }

    /*
     * Boost lithium when the star's rotation rate exceeds some limit
     */

    if(Is_not_zero(stardata->preferences->angelou_lithium_vrot_trigger) &&
       star->v_eq > stardata->preferences->angelou_lithium_vrot_trigger)
    {
        boost = NUCSYN_ANGELOU_LITHIUM_BOOST_VROT;
        Aprint("Boost lithium because v_eq (=%g) > %g\n",
               star->v_eq,
               stardata->preferences->angelou_lithium_vrot_trigger);
    }

    /*
     * Boost lithium when the star's rotation rate relative to critical
     * exceeds some limit
     */
    if(Is_not_zero(stardata->preferences->angelou_lithium_vrotfrac_trigger) &&
       star->v_eq_ratio > stardata->preferences->angelou_lithium_vrotfrac_trigger)
    {
        boost = NUCSYN_ANGELOU_LITHIUM_BOOST_VROTFRAC;
        Aprint("Boost lithium because v_eq/v_kep (=%g) > %g\n",
               star->v_eq_ratio,
               stardata->preferences->angelou_lithium_vrotfrac_trigger);
    }

    /*
     * Apply lithium boost
     */
    if(boost != NUCSYN_ANGELOU_LITHIUM_BOOST_NONE)
    {
        star->Xenv[XLi7] = star->Xacc[XLi7] =
            angelou_factor(massfrac,star->stellar_type);

        Aprint("Boost lithium to env=%g acc=%g (stellar type %d)\n",
               star->Xenv[XLi7],
               star->Xacc[XLi7],
               star->stellar_type);
    }
    else if(star->Xenv[XLi7] > NUCSYN_ANGELOU_LITHIUM_MINIMUM_Li7)
    {
        /*
         * Otherwise decay...
         */
        nucsyn_angelou_decay_lithium(stardata,star);
    }

    /*
     * Set the lithium boost flag which is used to calculate
     * the timestep. We don't want a short timestep when the boost
     * happens, only after it stops (or perhaps just before it).
     */
    if(boost != NUCSYN_ANGELOU_LITHIUM_BOOST_NONE)
    {
        star->angelou_lithium_boost_this_timestep = TRUE;
        if(0)fprintf(stderr,"BOOST star %d at %g reason %s at %g Li7 now %g\n",
                star->starnum,
                stardata->model.time,
                NUCSYN_ANGELOU_LITHIUM_BOOST_String(boost),
                stardata->model.time,
                star->Xenv[XLi7]
            );
    }
    else
    {
        if(0)fprintf(stderr,"NOBOOST star %d at %g Li7 now %g\n",
                star->starnum,
                stardata->model.time,
                star->Xenv[XLi7]
            );
    }

    if(0&&star->starnum==0)
        printf("TIME %g nucsyn done boost? %d prev %d\n",
               stardata->model.time,
               star->angelou_lithium_boost_this_timestep,
               stardata->previous_stardata->star[star->starnum].angelou_lithium_boost_this_timestep);
}


static void nucsyn_angelou_decay_lithium(
    struct stardata_t * stardata,
    struct star_t * star
    )
{

    /*
     * Reject long timesteps during the decay phase:
     *
     */
    if(
        star->Xenv[XLi7] > (1.0-TINY) * NUCSYN_ANGELOU_LITHIUM_MINIMUM_Li7 &&
        stardata->previous_stardata->star[star->starnum].angelou_lithium_boost_this_timestep == TRUE &&
        stardata->model.dtm > (1.0+TINY) * NUCSYN_ANGELOU_LITHIUM_MAX_TIMESTEP_FACTOR * angelou_factor(decay_time,star->stellar_type)
        )
    {
        if(0)fprintf(stderr,"RJCT %g %g\n",
                stardata->model.time,
                star->Xenv[XLi7]);
        star->reject = TRUE;
    }
    else
    {
        if(stardata->preferences->angelou_lithium_decay_function ==
           ANGELOU_LITHIUM_DECAY_FUNCTION_EXPONENTIAL)
        {
            double decay_time = angelou_factor(decay_time,
                                               star->stellar_type);
            if(Is_not_zero(decay_time))
            {
                /*
                 * fac is the "decay" factor: this is integrated
                 * over the timestep (and, for short timesteps, is
                 * approximately exp(-timestep/decay_time) )
                 */
                double fac = pow(exp(-stardata->model.dtm),1.0/decay_time);
                if(!Fequal(fac,1.0))
                {
                    react_lithium(stardata,fac,star->Xenv);

                    if(star->dmacc > 0.0)
                        react_lithium(stardata,fac,star->Xacc);

                    Aprint("Decay lithium by %g : is now Xenv=%g Xacc=%g\n",
                           fac,
                           star->Xenv[XLi7],
                           star->Xacc[XLi7]);
                }
            }
        }
        else
        {
            Exit_binary_c(
                BINARY_C_ALGORITHM_OUT_OF_RANGE,
                "Unknown decay function %d for George Angelou's lithium",
                stardata->preferences->angelou_lithium_decay_function
                );
        }
    }
}


static void react_lithium(struct stardata_t * stardata,
                          const double fac,
                          double * X)
{
    /*
     * Li7 + p -> He4 + He4
     *
     * Convert mass fractions passed in (X)
     * to number densities (N) at an arbitrary
     * density (1.0). Then "decay" by factor (fac),
     * and convert back to mass fractions.
     */
    Number_density N[ISOTOPE_ARRAY_SIZE];
    X_to_N(stardata->store->imnuc,
           1.0,
           N,
           X,
           ISOTOPE_ARRAY_SIZE);
    Number_density was = N[XLi7];
    N[XLi7] *= fac;
    Number_density dN = N[XLi7] - was;
    N[XH1] -= dN;
    N[XHe4] += 2.0 * dN;
    N_to_X(stardata->store->mnuc,
           1.0,
           N,
           X,
           ISOTOPE_ARRAY_SIZE);
}

#endif // NUCSYN_ANGELOU_LITHIUM
