#include "../binary_c.h"
No_empty_translation_unit_warning;


#if (defined NUCSYN && defined NUCSYN_WR_TABLES)
// data tables
#include "nucsyn_WR_mm_table.method0.h"
#include "nucsyn_WR_nl_table.method0.h"

/*
 * The part of the WR code which does the lookups : separately
 * to speed up compilation.
 */

void nucsyn_WR_table_lookup(struct stardata_t * Restrict const stardata,
                            const double * Restrict const x,
                            Abundance * Restrict const r,
                            const int wr_wind)
{

    /*
     * We have two data tables: one for MM and one for NL mass loss, so choose
     * which to use and set it in the *data pointer
     *
     * For other mass-loss rates, default to NL (this is closest to the
     * Hurley et al. 2002 prescription).
     */
    Const_data_table data_mm[]={NUCSYN_WR_MM_TABLE};
    Const_data_table data_nl[]={NUCSYN_WR_NL_TABLE};
    double * data;
    long int table_lines;

    /* Choose which data table to use */
    if(wr_wind==WR_WIND_MAEDER_MEYNET)
    {
        data=(double *)data_mm;
        table_lines=NUCSYN_WR_MM_TABLE_LINES;
    }
    else
    {
        /* default to NL mass loss */
        data=(double *)data_nl;
        table_lines=NUCSYN_WR_NL_TABLE_LINES;
    }

    /* Interpolate to determine the abundance from Lynnette's models */
    rinterpolate(data,
                 stardata->persistent_data->rinterpolate_data,
                 3,
                 6,
                 table_lines,
                 x,
                 r,
                 0);

}
#endif//NUCSYN_WR_TABLES
