#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef NUCSYN
void nucsyn_dilute_shell(const double m1,
                         Abundance * Restrict const X1,
                         const double m2,
                         const Abundance * const Restrict X2)
{

    /*
     * Mix shell 2 into shell 1 without changing shell 2
     * (shell 1 is changed).
     */
    const double f1 = m1 / (m1 + m2);
    const double f2 = 1.0 - f1;

    Nucsyn_isotope_stride_loop( X1[i] = f1 * X1[i] + f2 * X2[i] );
}
#endif // NUCSYN
