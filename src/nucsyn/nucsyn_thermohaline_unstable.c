#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN

/*
 * Function to return TRUE if the star's current accretion layer
 * has a higher molecular weight than the material below it,
 * and hence is thermohaline unstable.
 *
 * Note: we assume an ionisation fraction of 100%.
 */

Boolean nucsyn_thermohaline_unstable(struct stardata_t * const stardata,
                                     struct star_t * const star)
{
    
    const double mu_acc = nucsyn_molecular_weight(star->Xacc,
                                                  stardata,
                                                  1.0);
    const double mu_env = nucsyn_molecular_weight(star->Xenv,
                                                  stardata,
                                                  1.0);

    return mu_acc > mu_env ? TRUE : FALSE;
}


#endif



