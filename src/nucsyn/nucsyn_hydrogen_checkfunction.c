#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef NUCSYN

Boolean nucsyn_hydrogen_pp_checkfunction(struct stardata_t * const stardata Maybe_unused,
                                         double * const N,
                                         const double temperature,
                                         const double density Maybe_unused)
{
    /*
     * Check temperature, density and input abundance
     * array (number densities): return TRUE if we should
     * burn hydrogen (through the pp chain), FALSE otherwise
     */
    return (N[XH1]>TINY && temperature >= 1e5) ? TRUE : FALSE;
}

Boolean nucsyn_hydrogen_CNO_checkfunction(struct stardata_t * const stardata Maybe_unused,
                                          double * const N,
                                          const double temperature,
                                          const double density Maybe_unused)
{
    /*
     * Check temperature, density and input abundance
     * array (number densities): return TRUE if we should
     * burn hydrogen through the CNO cycle, FALSE otherwise.
     */
    return ((N[XN14]>TINY ||
             N[XC12]>TINY ||
             N[XO16]>TINY ||
             N[XC13]>TINY ||
             N[XN15]>TINY ||
             N[XO17]>TINY ||
             N[XO18]>TINY)
             && temperature >= 1e6) ? TRUE : FALSE;
}

Boolean nucsyn_hydrogen_NeNaMgAl_checkfunction(struct stardata_t * const stardata Maybe_unused,
                                               double * const N,
                                               const double temperature,
                                               const double density Maybe_unused)
{
    /*
     * Check temperature, density and input abundance
     * array (number densities): return TRUE if we should
     * burn hydrogen through the NeNaMgAl chain, FALSE otherwise.
     */
    return ((N[XNe20]>TINY ||
             N[XNe21]>TINY ||
             N[XNe22]>TINY ||
             N[XNa23]>TINY ||
             N[XMg24]>TINY ||
             N[XMg25]>TINY ||
             N[XMg26]>TINY ||
             N[XAl27]>TINY ||
             N[XAl26]>TINY)
            && temperature >= 1e6) ? TRUE : FALSE;
}

#endif//NUCSYN
