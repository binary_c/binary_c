#pragma once
#ifndef NUCSYN_SOLVERS_H
#define NUCSYN_SOLVERS_H
#include "nucsyn_solvers.def"

#undef X
#define X(CODE,STRING,FUNC) NUCSYN_SOLVER_##CODE,
enum{
    NUCSYN_SOLVERS_LIST
};
#undef X

#define X(CODE,STRING,FUNC) STRING,
static char * nucsyn_solver_strings[] Maybe_unused = { NUCSYN_SOLVERS_LIST };
#undef X

#undef X

#endif // NUCSYN_SOLVERS_H
