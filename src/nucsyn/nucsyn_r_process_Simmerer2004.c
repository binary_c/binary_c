#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef NUCSYN

void nucsyn_r_process_Simmerer2004(Abundance * const Restrict Xr Maybe_unused,
                                   struct stardata_t * const Restrict stardata Maybe_unused)
{
    /*
     * Function to set the r-process abundances into Xr, which should
     * be memory space which have been set to zero (i.e. allocated
     * with calloc)
     *
     * Data from Simmerer 2004
     *
     */
#ifdef NUCSYN_ALL_ISOTOPES
    Xr[XGa69]+=0.0775678542516041;
    Xr[XGe70]+=0.271172710395757;
    Xr[XRu99]+=0.00642577825855473;
    Xr[XI127]+=0.00745624579143637;
#ifdef __DEPRECATED
    /*
     * TODO : this needs updating: elemental abundances are
     * no longer accepted. You could use Arlandini 1999 instead.
     */

    Xr[XSm]+=0.0018010039309481;
    Xr[XNd]+=0.00349741871585397;
    Xr[XPb]+=0.00888840571260937;
    Xr[XTc]+=0.00122207061518154;
    Xr[XY]+=0.00804109814424159;
    Xr[XBa]+=0.00761852150980095;
    Xr[XCe]+=0.0019705093100246;
    Xr[XLa]+=0.00105494184982992;
    Xr[XZr]+=0.012803678461279;
    Xr[XYb]+=0.00194622165267805;
    Xr[XSr]+=0.015476420904763;

    Xr[XEr]+=0.00240879447017699;
    Xr[XEu]+=0.000944009099263257;
    Xr[XGd]+=0.00299028150589135;
    Xr[XHf]+=0.000988383024011691;
    Xr[XHg]+=0.00202580878754506;
    Xr[XLu]+=0.000374426404523603;
    Xr[XOs]+=0.00853794474997532;
    Xr[XPr]+=0.00079774144942364;
    Xr[XRe]+=0.000603419625785667;
    Xr[XSb]+=0.00206206318776232;
    Xr[XSn]+=0.0062472996716529;
    Xr[XTa]+=0.000162409292966118;
    Xr[XTb]+=0.000658351990103993;
    Xr[XTe]+=0.0348992722301329;
    Xr[XTl]+=0.000746387066772652;
    Xr[XTm]+=0.000361570546529658;
    Xr[XW]+=0.000800122140691803;
    Xr[XXe]+=0.034488842127528;
    Xr[XBi]+=0.00134184457811946;
    Xr[XAg]+=0.00324077143998944;
    Xr[XCd]+=0.00593211098636763;
    Xr[XMo]+=0.00420462492477428;
    Xr[XPd]+=0.00568333173213979;
    Xr[XIn]+=0.000959917253005494;
    Xr[XKr]+=0.13139483936872;
    Xr[XRb]+=0.0171419885577115;
    Xr[XDy]+=0.00404961661493478;
    Xr[XAs]+=0.0275707112896714;
    Xr[XSe]+=0.219364492076826;
    Xr[XBr]+=0.0256023132087668;
    Xr[XNb]+=0.000705589244057662;
    Xr[XRh]+=0.00205328951201788;

    Xr[XCs]+=0.0028904607351068;
    Xr[XHo]+=0.000945131714036511;
    Xr[XAu]+=0.00239341988474928;
    Xr[XPt]+=0.0174855400037058;
#endif // __DEPRECATED
#endif // NUCSYN_ALL_ISOTOPES
}

#endif // NUCSYN
