#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef NUCSYN

Abundance_ratio nucsyn_elemental_square_bracket(const char * const top,
                                                const char * const bottom,
                                                const Abundance * Restrict const X,
                                                const Abundance * Restrict const Xsolar,
                                                struct stardata_t * Restrict const stardata)
{
    Dprint("Square bracket [%s/%s]\n",top,bottom);
    /*
     *
     * Square bracket notation:
     * [A/B] = log(N_A/N_B) - log(N_A_solar/N_B_solar)
     *       ~ log(X_A/X_B) - log(X_A_solar/X_B_solar)
     * where log=log10


log(NA/NB) - log(NAsolar/NBsolar) = log(NA/NB / NAsolar/NBsolar)
     *
     * A=top, B=bottom.
     *
     * This function is for *elements* : see nucsyn_square_bracket
     * and nucsyn_square_multibracket for isotopic alternatives.
     *
     * The ~ is approximately true assuming all the isotopes of an
     * element have the same mass. They don't, so we calculate the
     * ratios by number.
     */

    const Abundance numerator =
        REALLY_TINY +
        nucsyn_elemental_abundance_ratio_by_number(top,
                                                   bottom,
                                                   X,
                                                   stardata,
                                                   stardata->store);

    const Abundance denominator =
        REALLY_TINY +
        nucsyn_elemental_abundance_ratio_by_number(top,
                                                   bottom,
                                                   Xsolar,
                                                   stardata,
                                                   stardata->store);

    double sqb = log10( numerator / denominator );

    /*
     * Limit sqb: in the case where abundances are really close to solar,
     * the REALLY_TINY terms will give you a result around 10^-6, so just truncate
     * at 1e-5, which is 4 orders of magnitude better than stellar observations
     * (except with grains... perhaps...:)
     */

    if(Abs_less_than(sqb,NUCSYN_SQUARE_BRACKET_FLOOR)) sqb=0.0;

    Dprint("sqb[%s/%s] = log10(%g) - log10(%g) = %g - %g = %g\n",
           top,
           bottom,
           numerator,
           denominator,
           log10(numerator),
           log10(denominator),
           sqb);

    return sqb;
}


#endif//NUCSYN
