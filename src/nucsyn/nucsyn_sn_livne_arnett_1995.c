#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN


void nucsyn_sn_livne_arnett_1995(Abundance * Restrict const X,
                                 const double mcore,
                                 const double menv)
{
    // A fit to the ELD models of Livne and Arnett, 1995
    Abundance tmpX[13],Xtot=0.0;
    Isotope i;
    Clear_isotope_array(X);

#ifdef SN_DEBUG
    printf("mcore %g  ; menv %g\n",mcore,menv);
#endif

    tmpX[0] = Max(0.0,(1.88500e-02+1.91900e-01*mcore-1.90040e-01*mcore*mcore)*(1+7.39800e-01*menv)); // He4
    tmpX[1] = Max(0.0,(3.24480e-01-7.56680e-01*mcore+4.46930e-01*mcore*mcore)*(1-2.56920e+00*menv)); // C12
    tmpX[2] = Max(0.0,(7.93240e-01-1.41840e+00*mcore+6.71590e-01*mcore*mcore)*(1-2.23940e+00*menv)); // O16
    tmpX[3] = Max(0.0,(5.68650e-02-1.15740e-01*mcore+6.05770e-02*mcore*mcore)*(1-2.47720e+00*menv)); // Ne20
    tmpX[4] = Max(0.0,(6.82680e-02-1.82690e-02*mcore-5.33900e-02*mcore*mcore)*(1-1.95270e+00*menv)); // Mg24
    tmpX[5] = Max(0.0,(1.95370e-02+4.92250e-01*mcore-4.22380e-01*mcore*mcore)*(1-1.22900e+00*menv)); // Si28
    tmpX[6] = Max(0.0,(3.34830e-01-7.46490e-01*mcore+5.24310e-01*mcore*mcore)*(1-1.02880e+00*menv)); // S32
    tmpX[7] = Max(0.0,(1.55300e-01-4.25910e-01*mcore+3.07710e-01*mcore*mcore)*(1+5.01040e-01*menv)); // Ar36
    tmpX[8] = Max(0.0,(4.78990e-02-1.39970e-01*mcore+1.02860e-01*mcore*mcore)*(1+3.83960e+01*menv)); // Ca40
    tmpX[9] = Max(0.0,(1.59640e-01-3.98550e-01*mcore+2.57570e-01*mcore*mcore)*(1-2.40520e+00*menv)); // Ti44
    tmpX[10] = Max(0.0,(1.24830e-01-3.17780e-01*mcore+2.09460e-01*mcore*mcore)*(1-2.23520e+00*menv)); // Cr48
    tmpX[11] = Max(0.0,(3.56740e-01-9.95960e-01*mcore+7.02430e-01*mcore*mcore)*(1-2.24070e-01*menv)); // Fe52
    tmpX[12] = Max(0.0,(-7.99240e-01+2.22830e+00*mcore-1.25290e+00*mcore*mcore)*(1+1.39220e+01*menv)); // Ni56

    // Normalize to 1 and multiply by (mcore+menv)
    for(i=0;i<13;i++)
    {
#ifdef SN_DEBUG
        if(tmpX[i]<0){Exit_binary_c(BINARY_C_OUT_OF_RANGE,"oops isotope %d is < 0 !\n",i);}
#endif
        Xtot += tmpX[i];
    }
    Xtot = 1.0 / Xtot; // calculate this ratio only once!
    X[XHe4] += tmpX[0]*Xtot;
    X[XC12] += tmpX[1]*Xtot;
    X[XO16] += tmpX[2]*Xtot;
    X[XNe20] += tmpX[3]*Xtot;
    X[XMg24] += tmpX[4]*Xtot;
    X[XSi28] += tmpX[5]*Xtot;
    X[XFe56] += tmpX[12]*Xtot;//Ni56
#ifdef NUCSYN_ALL_ISOTOPES
    X[XS32] += tmpX[6]*Xtot;
    X[XAr36] += tmpX[7]*Xtot;
    X[XCa40] += tmpX[8]*Xtot;
    X[XTi44] += tmpX[9]*Xtot;
    X[XCr48] += tmpX[10]*Xtot;
    X[XFe52] += tmpX[11]*Xtot;
#endif//NUCSYN_ALL_ISOTOPES

}

#endif // NUCSYN
