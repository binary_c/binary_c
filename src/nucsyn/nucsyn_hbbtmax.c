#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN

Constant_function double nucsyn_hbbtmax(double menv_1tp,
                                        double mc_1tp,
                                        const Abundance Z)
{
    /*
     * Maximum temperature the HBB layer will reach as a function of mass and
     * metallicity
     */
    double y;
    const Abundance lz = log10(Z/0.02);
    
    /*
     * Limit menv_1tp and mc_1tp to prevent high mass tpagb stars
     * from crashing the code: fits are invalid above these 
     * (approximate) ranges anyway
     */ 
    menv_1tp = Min(5.5,menv_1tp);
    mc_1tp = Min(1.38,mc_1tp);

    y=
        0.9982 * // Fix of Izzard et al 2006 to make NeNa/MgAl agree better
        (4.44290e-02*menv_1tp+(-2.27390e-02*lz*lz-8.28510e-02*lz+1.67930)*
         (1-1.61930e-01*mc_1tp+1.63740e-01*mc_1tp*mc_1tp)+6.03790);


    return(y);
}
#endif //NUCSYN
