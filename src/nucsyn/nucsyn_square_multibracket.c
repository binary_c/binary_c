#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN

Abundance_ratio Pure_function nucsyn_square_multibracket(const Abundance * Restrict const X,
                                                         const Abundance * Restrict const Xsolar,
                                                         const Isotope top_isotopes[],
                                                         const int n_top,
                                                         const Isotope bottom_isotopes[],
                                                         const int n_bottom)
{

    /*
     *
     * Square bracket notation:
     * [A/B] = log(N_A/N_B) - log(N_A_solar/N_B_solar)
     *       = log(X_A/X_B) - log(X_A_solar/X_B_solar)
     * where log=log10
     *
     * This function is identical to nucsyn_square_bracket but for 
     * a selection of isotopes, given by their isotope numbers
     *
     * Note that if you want *elemental* square brackets, consider
     * using the (much simpler!) nucsyn_elemental_square_bracket
     * function.
     */
    int i;
    Isotope j;
    Abundance Xtop=TINY,Xbottom=TINY,Xsolar_top=TINY,Xsolar_bottom=TINY;
    for(i=0;i<n_top;i++)
    {
        j=top_isotopes[i];
        Xtop+=X[j];
        Xsolar_top+=Xsolar[j];
    }
    for(i=0;i<n_bottom;i++)
    {
        j=bottom_isotopes[i];
        Xbottom+=X[j];
        Xsolar_bottom+=Xsolar[j];
    }

    double sqb=log10(Xtop/Xsolar_top)-log10(Xbottom/Xsolar_bottom);
    /*
     * Limit sqb: in the case where abundances are really close to solar,
     * the TINY terms will give you a result around 10^-6, so just truncate
     * at 1e-5, which is 4 orders of magnitude better than stellar observations
     * (except with grains... perhaps...:)
     */
    if(Abs_less_than(sqb,1e-5)) sqb=0.0;
    
    return(sqb);
}
#endif
