#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN
/*
 * Make an array of isotope name strings
 */
void nucsyn_make_isotope_names(struct store_t * Restrict const store)
{
    if(store->isotope_strings == NULL)
    {
        store->isotope_strings = (char**)nucsyn_isotope_strings;
    }
}
#endif // NUCSYN
