#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef NUCSYN
#include "nucsyn_sn_yield.h"

/*
 * Function to calculate and add the yield due to a supernova
 *
 * Input parameters:
 *
 * exploder contains the stellar structure *after* the explosion
 *
 * pre_explosion_star contains the stellar structure *before* the explosion
 *
 * dm is the mass ejected
 */

void nucsyn_sn_yield(struct stardata_t * const stardata,
                     struct star_t * const exploder,
                     struct star_t * const pre_explosion_star,
                     double dm, /* mass ejected > 0 */
                     const Stellar_type pre_explosion_companion_stellar_type
    )
{
    struct star_t * const companion = Other_star_struct(exploder);

    /*
     *  dm < 0 is a formal error
     */
    if(dm < -TINY)
    {
        Exit_binary_c(BINARY_C_MASS_OUT_OF_RANGE,
                      "SN yield function called with dm = %g <= 0 : this cannot be correct\n",
                      dm);
    }

    /*
     * array to store the yields before calling calc_yields
     */
    Abundance * X = New_clear_isotope_array;

    /*
     * the metallicity for the explosion (might be <1e-4)
     */
    const Abundance metallicity = stardata->common.nucsyn_metallicity>-TINY ?
        stardata->common.nucsyn_metallicity : stardata->common.metallicity;

    Dprint("SN yield star %d, exploder->SN_type = %d = %s, mass ejected dm = %g, pre_explosion stellar type %d, nucsyn metallicity %g\n",
           exploder->starnum,
           exploder->SN_type,
           supernova_type_strings[exploder->SN_type],
           dm,
           pre_explosion_star->stellar_type,
           metallicity);

#ifdef NUCSYN_GCE_OUTFLOW_CHECKS
    // set velocity for the SN wind
    exploder->vwind = NUCSYN_GCE_SUPERNOVA_OUTFLOW_VELOCITY;
#endif // NUCSYN_GCE_OUTFLOW_CHECKS
    exploder->sn_last_time = exploder->SN_type;

    /*
     * Save the stellar remnant's abundances
     */
    Abundance * Xremnant = New_isotope_array_from(exploder->Xenv);

    /*
     * Use saved surface abundance prior to remnant formation
     */
    Copy_abundances(pre_explosion_star->Xenv,
                    exploder->Xenv);

#ifdef NUCSYN_STRIP_AND_MIX
    /* mix down to the core */
    mix_down_to_core(exploder);
#endif // NUCSYN_STRIP_AND_MIX
#ifdef SN_TEST
    // call the test function
    nucsyn_sn_test(stardata);
#endif // SN_TEST

    /*
     * Select yields according to explosion type
     */
    const Yield_source src_id = supernova_source_id(exploder,
                                                    pre_explosion_star,
                                                    companion);
    Dprint("Yield src id %d\n",src_id);

    switch (exploder->SN_type)
    {
    case SN_IBC:
    case SN_II:
    case SN_GRB_COLLAPSAR:
    case SN_PI:
    case SN_PPI:
    case SN_PHDIS:
    {
        Dprint("CORE_COLLAPSE_SNIBC, II, GRB_COLLAPSAR, PI, PPI or PHDIS : McHe=%g McCO=%g dm=%g\n",
               pre_explosion_star->core_mass[CORE_He],
               pre_explosion_star->core_mass[CORE_CO],
               dm);

        /*
         * menv is the hydrogen or helium envelope mass:
         * we have to restrict this to be positive because
         * winds may erode the star into the CO core
         */
        const double menv = Max(0.0,
                                pre_explosion_star->mass - pre_explosion_star->core_mass[CORE_CO]);
        dm = nucsyn_sn_core_collapse(X,
                                     pre_explosion_star->Xenv,
                                     pre_explosion_star->core_mass[CORE_CO],
                                     menv,
                                     dm,
                                     metallicity,
                                     src_id,
                                     stardata,
                                     pre_explosion_star,
                                     exploder
            );
    }
    break;
    case SN_IA_He:

        Dprint("SNIa of HeWD. Companion is stellar type %d\n",
               companion->stellar_type);

        if(pre_explosion_star->stellar_type == HeWD)
        {
            nucsyn_sn_woosley_taam_weaver_1986(X);
        }
        else
        {
            Dprint("The other star is exploding?");
            Other_exploder(1);
        }
        break;

    case SN_IA_He_Coal:
        /*
         * HeWD + HeWD
         */
        Dprint("HeWD explosion by merger, companion's stellar type is %d",
               companion->stellar_type);

        if(pre_explosion_star->stellar_type == HeWD)
        {
            if(companion->stellar_type == HeWD)
            {
                nucsyn_sn_woosley_taam_weaver_1986(X);
            }
            else
            {
                Exit_binary_c(
                    BINARY_C_ALGORITHM_BRANCH_FAILURE,
                    "We have a HeWD-HeWD merger (SNIa?) but the other star is not a HeWD?!\n"
                    );
            }
        }
        else
        {
            Other_exploder(2);
        }
        break;

    case SN_IA_SUBCHAND_He_Coal:
        /*
         * Sub-MCh HeWD + COWD
         */
        Dprint("yields of HeWD + COWD\n");
        if(stardata->preferences->triggered_SNIa_algorithm == TYPE_IA_SUPERNOVA_ALGORITHM_DD2)
        {
            nucsyn_sn_iwamoto_1999_DD2(X);
        }
        else if(stardata->preferences->triggered_SNIa_algorithm == TYPE_IA_SUPERNOVA_ALGORITHM_SEITENZAHL2013)
        {
            nucsyn_sn_Seitenzahl2013(stardata,X);
        }
        else if(stardata->preferences->triggered_SNIa_algorithm == TYPE_IA_SUPERNOVA_ALGORITHM_SEITENZAHL2013_AUTOMATIC)
        {
            nucsyn_sn_Seitenzahl2013_automatic(stardata,X);
        }
        else if(stardata->preferences->triggered_SNIa_algorithm == TYPE_IA_SUPERNOVA_ALGORITHM_LIVNE_ARNETT_1995)
        {
            nucsyn_sn_livne_arnett_1995(X,
                                        pre_explosion_star->core_mass[CORE_CO],
                                        pre_explosion_star->mass - pre_explosion_star->core_mass[CORE_CO]);
        }
        break;

    case SN_IA_SUBCHAND_CO_Coal:
        /*
         * Sub-MCh COWD + COWD
         */
        Dprint("yields of COWD + COWD\n");
        if(stardata->preferences->triggered_SNIa_algorithm == TYPE_IA_SUPERNOVA_ALGORITHM_DD2)
        {
            nucsyn_sn_iwamoto_1999_DD2(X);
        }
        else if(stardata->preferences->triggered_SNIa_algorithm == TYPE_IA_SUPERNOVA_ALGORITHM_SEITENZAHL2013)
        {
            nucsyn_sn_Seitenzahl2013(stardata,X);
        }
        else if(stardata->preferences->triggered_SNIa_algorithm == TYPE_IA_SUPERNOVA_ALGORITHM_SEITENZAHL2013_AUTOMATIC)
        {
            nucsyn_sn_Seitenzahl2013_automatic(stardata,X);
        }
        else if(stardata->preferences->triggered_SNIa_algorithm == TYPE_IA_SUPERNOVA_ALGORITHM_LIVNE_ARNETT_1995)
        {
            nucsyn_sn_livne_arnett_1995(X,
                                        pre_explosion_star->core_mass[CORE_CO],
                                        pre_explosion_star->mass - pre_explosion_star->core_mass[CORE_CO]);
        }
        break;

    case SN_IA_COWD_DDet:
    case SN_IA_ONeWD_DDet:
    case SN_IA_Hybrid_HeCOWD:
        /*
         * Double detonation
         *
         * CO and ONeWDs accrete helium-rich material until the accumulated
         * material exceeds a mass of 0.15 when it ignites. For a COWD with
         * mass less than 0.95 the system will be destroyed in a possible
         * Type 1a SN. COWDs with mass greater than 0.95 and ONeWDs will
         * survive with all the material converted to ONe.
         *
         * V1.10 of BSE
         * Now changed to an ELD for all COWDs when 0.15
         * accreted (JH 11/01/00).
         *
         * 13/09/2023 RGI : changed ELD to DD
         */
    {
        const double mcore = pre_explosion_star->core_mass[pre_explosion_star->stellar_type == COWD ? CORE_CO : CORE_ONe];
        const double menv = pre_explosion_star->mass - mcore;

        if(stardata->preferences->type_Ia_DDet_yield_algorithm == TYPE_IA_SUPERNOVA_ALGORITHM_DD2)
        {
            dm = mcore + menv;
            nucsyn_sn_iwamoto_1999_DD2(X);
        }
        else if(stardata->preferences->triggered_SNIa_algorithm == TYPE_IA_SUPERNOVA_ALGORITHM_SEITENZAHL2013)
        {
            dm = mcore + menv;
            nucsyn_sn_Seitenzahl2013(stardata,X);
        }
        else if(stardata->preferences->triggered_SNIa_algorithm == TYPE_IA_SUPERNOVA_ALGORITHM_SEITENZAHL2013_AUTOMATIC)
        {
            dm = mcore + menv;
            nucsyn_sn_Seitenzahl2013_automatic(stardata,X);
        }
        else if(stardata->preferences->type_Ia_DDet_yield_algorithm == TYPE_IA_SUPERNOVA_ALGORITHM_LIVNE_ARNETT_1995)
        {
            Dprint("DD : CO core %g Msun : envelope %g Msun, pre-explosion stellar type %d\n",
                   mcore,
                   menv,
                   pre_explosion_star->stellar_type);

            if(pre_explosion_star->stellar_type == COWD)
            {
                if(companion->stellar_type == HeWD ||
                   NAKED_HELIUM_STAR(companion->stellar_type))
                {
                    /*
                     * Fits to Livne and Arnett 1995
                     * (as function of core and env mass)
                     */
                    nucsyn_sn_livne_arnett_1995(X,
                                                mcore,
                                                menv);
                }
                else
                {
                    /*
                     * H-rich : currently use Livne
                     */
                    nucsyn_sn_livne_arnett_1995(X,
                                                mcore,
                                                menv);
                }
                dm = mcore + menv;
            }
            else
            {
                Other_exploder(3);
            }
        }
        else if(stardata->preferences->type_Ia_DDet_yield_algorithm == TYPE_IA_SUPERNOVA_ALGORITHM_GRONOW_2021)
        {
            Dprint("DD : CO core mass %g : envelope size %g, pre-explosion stellar type %d\n",
                   mcore,
                   menv,
                   pre_explosion_star->stellar_type);

            /*
             * We use paper b combined with paper a,
             * so we can vary the metallicity.
             */
            dm = nucsyn_sn_Gronow2021b(stardata,
                                       X,
                                       stardata->common.nucsyn_metallicity,
                                       mcore,
                                       menv);

        }
        else
        {
            Exit_binary_c(BINARY_C_OUT_OF_RANGE,
                          "Unknown type Ia DD yield algorithm %d\n",
                          stardata->preferences->type_Ia_DDet_yield_algorithm);
        }
    }
    break;

    case SN_IA_HeStar:
        Dprint("He star Ia, pre-explosion mass %g has degenerate MCh core : SNIa explosion with helium. Eject dm = %g\n",
               pre_explosion_star->mass,
               dm);
        nucsyn_sn_he_degenerate_core(X,
                                     stardata,
                                     pre_explosion_star);

        break;

    case SN_IA_CHAND:

        Dprint("Chandrasekhar mass SN Ia by accretion: accretor type %d mass %g, donor type %d mass %g\n",
               pre_explosion_star->stellar_type,
               pre_explosion_star->mass,
               companion->stellar_type,
               companion->mass
            );
        /*
         * NB allow HeWDs to explode this way also.
         * Presumably they would convert to COWDs
         * the process.
         */

        if(pre_explosion_star->stellar_type == COWD ||
           pre_explosion_star->stellar_type == HeWD)
        {
            if(stardata->preferences->type_Ia_MCh_supernova_algorithm ==
               TYPE_IA_SUPERNOVA_ALGORITHM_DD2)
            {
                nucsyn_sn_iwamoto_1999_DD2(X);
            }
            else if(stardata->preferences->type_Ia_MCh_supernova_algorithm ==
                    TYPE_IA_SUPERNOVA_ALGORITHM_SEITENZAHL2013)
            {
                nucsyn_sn_Seitenzahl2013(stardata,X);
            }
            else if(stardata->preferences->type_Ia_MCh_supernova_algorithm ==
                    TYPE_IA_SUPERNOVA_ALGORITHM_SEITENZAHL2013_AUTOMATIC)
            {
                nucsyn_sn_Seitenzahl2013_automatic(stardata,X);
            }
            else if(stardata->preferences->triggered_SNIa_algorithm == TYPE_IA_SUPERNOVA_ALGORITHM_LIVNE_ARNETT_1995)
            {
                nucsyn_sn_livne_arnett_1995(X,
                                            pre_explosion_star->core_mass[CORE_CO],
                                            pre_explosion_star->mass - pre_explosion_star->core_mass[CORE_CO]);
            }
        }
        else
        {
            show_cmd_line_args(stardata,stderr);
            Other_exploder(4);
        }
        break;

    case SN_IA_CHAND_Coal:

        // NB this function cannot be called by ONeWD+ONeWD merger case
        Dprint("M > Chandrasekhar mass by merger, stellar types are pre-explosion accretor %d, donor %d",
               pre_explosion_star->stellar_type,
               pre_explosion_companion_stellar_type);

        if(pre_explosion_star->stellar_type == HeWD &&
           pre_explosion_companion_stellar_type == HeWD)
        {
            /* HeWD + HeWD merger */
            Dprint("HeWD HeWD merger -> Woosley, Taam, Weaver 1986 yields");
            nucsyn_sn_woosley_taam_weaver_1986(X);
        }
        else if(pre_explosion_star->stellar_type == COWD &&
                (pre_explosion_companion_stellar_type == COWD ||
                 pre_explosion_companion_stellar_type == MASSLESS_REMNANT))
        {
            if(stardata->preferences->type_Ia_MCh_supernova_algorithm ==
               TYPE_IA_SUPERNOVA_ALGORITHM_DD2)
            {
                /*
                 * Use Iwamoto results for single COWD (M=MCh)
                 * for dm ~= MCh of the material
                 */
                Dprint("Iwamoto DD2 MCh");
                nucsyn_sn_iwamoto_1999_DD2(X);
            }
            else if(stardata->preferences->type_Ia_MCh_supernova_algorithm ==
                    TYPE_IA_SUPERNOVA_ALGORITHM_SEITENZAHL2013)
            {
                Dprint("Seitenzahl 2013");
                nucsyn_sn_Seitenzahl2013(stardata,X);
            }
            else if(stardata->preferences->type_Ia_MCh_supernova_algorithm ==
                    TYPE_IA_SUPERNOVA_ALGORITHM_SEITENZAHL2013_AUTOMATIC)
            {
                Dprint("Seitenzahl 2013 automatic");
                nucsyn_sn_Seitenzahl2013_automatic(stardata,X);
            }
            else if(stardata->preferences->triggered_SNIa_algorithm == TYPE_IA_SUPERNOVA_ALGORITHM_LIVNE_ARNETT_1995)
            {
                nucsyn_sn_livne_arnett_1995(X,
                                            pre_explosion_star->core_mass[CORE_CO],
                                            pre_explosion_star->mass - pre_explosion_star->core_mass[CORE_CO]);
            }
            else
            {
                Exit_binary_c(BINARY_C_OUT_OF_RANGE,
                              "Unknown type Ia MCh coalesce yield algorithm %d\n",
                              stardata->preferences->type_Ia_DDet_yield_algorithm);
            }
        }
        else
        {
            printf("pre explosion type %d, companion %d\n",
                   pre_explosion_star->stellar_type,
                   pre_explosion_companion_stellar_type);
            Other_exploder(5);
        }
        break;

    case SN_IIa:
        /*
         * SNIIa : the core explodes, no remnant is left.
         */
    {
        const double menv = pre_explosion_star->mass - pre_explosion_star->core_mass[CORE_CO];
        const double dmCO = Max(0.0,dm - menv);

        Dprint("SNIIa menv=%g mco=%g core_mass=%g -> dmCO = %g\n",
               menv,
               dmCO,
               Outermost_core_mass(&stardata->star[1]),
               dmCO);

        Abundance * Restrict XSN = New_clear_isotope_array;
        if(dmCO > TINY)
        {
            if(stardata->preferences->type_Ia_MCh_supernova_algorithm ==
               TYPE_IA_SUPERNOVA_ALGORITHM_DD2)
            {
                nucsyn_sn_iwamoto_1999_DD2(XSN);
            }
            else if(stardata->preferences->type_Ia_MCh_supernova_algorithm ==
                    TYPE_IA_SUPERNOVA_ALGORITHM_SEITENZAHL2013)
            {
                nucsyn_sn_Seitenzahl2013(stardata,XSN);
            }
            else if(stardata->preferences->type_Ia_MCh_supernova_algorithm ==
                    TYPE_IA_SUPERNOVA_ALGORITHM_SEITENZAHL2013_AUTOMATIC)
            {
                nucsyn_sn_Seitenzahl2013_automatic(stardata,XSN);
            }
        }
        Dprint("Mix dmCO %g (1-Xtot %g) with menv %g (1-Xtot %g)\n",
               dmCO,
               1.0 - nucsyn_totalX(XSN),
               menv,
               1.0 - nucsyn_totalX(exploder->Xenv));
        nucsyn_mix_shells(dmCO,XSN,
                          menv,exploder->Xenv);
        Copy_abundances(XSN,X);
        Safe_free(XSN);
    }
    break;
    case SN_ECAP:
        /*
         * Electron Capture supernova of an ONeMg core,
         * probably in a TPAGB star, maybe a CO core too (although
         * these are probably thermonuclear)
         */
    {
        const double mcbagb = mcagbf(exploder->phase_start_mass,
                                     stardata->common.giant_branch_parameters);
        double menv = pre_explosion_star->mass - Outermost_core_mass(pre_explosion_star); //note because this is a TPAGB star, this core mass is McHE~McCO

        const Boolean oxygen_neon_core = ONe_core(stardata,pre_explosion_star);

#ifdef NUCSYN_SN_ELECTRON_CAPTURE_WANAJO_2008
        if(oxygen_neon_core == TRUE)
        {
            /*
             * Assume burnt material comes from the envelope:
             * required to conserve mass!
             */
            menv -= ELECTRON_CAPTURE_EJECTED_MASS;
            menv = Max(0.0,menv);
        }
#endif// NUCSYN_SN_ELECTRON_CAPTURE_WANAJO_2008

        Dprint("Electron capture supernova with core mass %g made of %s, eject menv = %g\n",
               mcbagb,
               (oxygen_neon_core ? "ONe" : "CO"),
               menv
            );

        /* eject the envelope */
        Abundance * Xenv = New_isotope_array_from(exploder->Xenv);
        Abundance * Xcore = New_isotope_array;

        const double dmCO = Max(0.0, dm - menv);
        double dmcore = 0.0;

        Dprint("Electron capture SN: dmCO=%g mcbagb=%g\n",
               dmCO,
               mcbagb);


        /*
         * In the case of ONe cores, eject a burnt layer, leave a NS.
         *
         * In the case of CO cores, eject the entire core.
         */
        if(oxygen_neon_core == TRUE)
        {
            Dprint("Electron capture SN : ONe burnt layer ejected, NS remains\n");
            nucsyn_sn_electron_capture(stardata,
                                       Xcore);
            dmcore = ELECTRON_CAPTURE_EJECTED_MASS;
        }
        else
        {
            /* should be like a Ia? */
            Clear_isotope_array(Xcore);
            Xcore[XC12]=0.2;
            Xcore[XO16]=0.8;
            Dprint("Electron capture SN : CO core ejected\n");
            dmcore = dmCO;
        }
        nucsyn_mix_shells(dmcore,Xcore,
                          menv,Xenv);
        Copy_abundances(Xcore,X);
        Safe_free(Xcore);
        Safe_free(Xenv);
    }
    break;
    case SN_AIC:

        /*
         * Accretion Induced Collapse - assume direct
         * change from WD to NS i.e. zero yield although there are suspicions
         * that there might be some r-process yield... ????
         *
         * what should we eject? try the surface abundance
         * for lack of anything else...
         */
        Dprint("AIC supernova eject dm = %g \n",dm);
        Copy_abundances(exploder->Xenv,X);
        break;
    default:
        Backtrace;
        Exit_binary_c(BINARY_C_ALGORITHM_BRANCH_FAILURE,
                      "SN of type %d, %s, has no yield function: please fix nucsyn_sn_yield to handle this supernova type.",
                      exploder->SN_type,
                      SN_String(exploder->SN_type));
    }

    nancheck("SN X",X,ISOTOPE_ARRAY_SIZE);
    nancheck("companion Xenv",companion->Xenv,ISOTOPE_ARRAY_SIZE);

    Dprint("SN pre-yield, src_id = %d, unknown? %s\n",
           src_id,
           Yesno(SOURCE_UNKNOWN == src_id));

    if(src_id == SOURCE_UNKNOWN)
    {
        Exit_binary_c(BINARY_C_ALGORITHM_BRANCH_FAILURE,
                      "SN of type %d %s failed to map to a source. This is a fatal error, please fix nucsyn_sn_yield.c\n",
                      exploder->SN_type,
                      SN_String(exploder->SN_type));
    }
    else
    {
        Dprint("Call calc yields for SN explosion. dm = %g, X H1 %g He4 %g C12 %g O16 %g Fe56 %g 1-Xtot %g\n",
               dm,
               X[XH1],
               X[XHe4],
               X[XC12],
               X[XO16],
               X[XFe56],
               1.0-nucsyn_totalX(X));
        calc_yields(stardata,
                    exploder,
                    dm,
                    X,
                    0.0,
                    NULL,
                    pre_explosion_star->starnum,
                    YIELD_NOT_FINAL,
                    src_id);

        if(Is_not_zero(companion->dm_companion_SN))
        {
            if(companion->dm_companion_SN < TINY)
            {
                /*
                 * mass ejected from companion
                 */
                Abundance * Xcompanion;
                Boolean allocated;
                nucsyn_remove_dm_from_surface(
                    stardata,
                    companion,
                    companion->dm_companion_SN,
                    &Xcompanion,
                    &allocated);
                Dprint("Call calc yields to eject mass from companion. dm = %g, X H1 %g He4 %g C12 %g O16 %g Fe56 %g Xtot %g\n",
                       companion->dm_companion_SN,
                       Xcompanion[XH1],
                       Xcompanion[XHe4],
                       Xcompanion[XC12],
                       Xcompanion[XO16],
                       Xcompanion[XFe56],
                       nucsyn_totalX(Xcompanion));
                calc_yields(stardata,
                            exploder,
                            companion->dm_companion_SN,
                            Xcompanion,
                            0.0,
                            NULL,
                            pre_explosion_star->starnum,
                            YIELD_NOT_FINAL,
                            SOURCE_SNSTRIP);
                if(allocated == TRUE)
                {
                    Safe_free(Xcompanion);
                }
            }
            else
            {
                /*
                 * Mass accreted onto companion.
                 *
                 * Assume
                 *
                 * 1) mixing of any pre-existing accretion layer
                 *
                 * 2) that the impacting ejecta are a mix of the
                 *    ejecta from the exploding star. Probably the
                 *    slowest ejecta are likely to accrete most, but
                 *    then again the first to impact will be the
                 *    outer envelope. What to do...???
                 */
                nucsyn_mix_accretion_layer_and_envelope(stardata,
                                                        companion,
                                                        -1.0);
                Dprint("Call calc yields to accrete mass on the companion with dm = %g, X H1 %g He4 %g C12 %g O16 %g Fe56 %g Xtot %g\n",
                       companion->dm_companion_SN,
                       X[XH1],
                       X[XHe4],
                       X[XC12],
                       X[XO16],
                       X[XFe56],
                       nucsyn_totalX(X));
                calc_yields(stardata,
                            companion,
                            0.0,
                            NULL,
                            companion->dm_companion_SN,
                            X,
                            companion->starnum,
                            YIELD_NOT_FINAL,
                            SOURCE_OTHER);
                nucsyn_dilute_shell(companion->dm_companion_SN,X,
                                    companion->mass,companion->Xenv);
            }
        }
        companion->dm_companion_SN = 0.0;
    }

    /* restore remnant's surface abundances */
    Copy_abundances(Xremnant,exploder->Xenv);

    Safe_free(Xremnant);
    Safe_free(X);
}

#endif // NUCSYN
