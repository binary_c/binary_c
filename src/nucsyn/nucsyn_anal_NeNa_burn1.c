#include "../binary_c.h"
#include <gsl/gsl_linalg.h>
No_empty_translation_unit_warning;

#if (defined NUCSYN && defined NUCSYN_ANAL_BURN)

/* NeNa cycle burning: version 2 */

/* This new version does the maths properly using a matrix inversion
 * rather than the dubious method used in the previous version of this
 * function. NB the previous version has been left in the code for a while
 * just in case I have to compare.
 *
 * There are two debugging levels: DEBUG is quite basic so you can check
 * what goes in and out. MOREDEBUG is very comprehensive, and gives you
 * information about every step of the calculation. This proved invaluable
 * when trying to fix the code! So please leave it there.
 */



int nucsyn_anal_NeNa_burn1(struct stardata_t * stardata,
                           double * Restrict t,
                           Abundance * Restrict Nin,
                           const double dtseconds)

{
    /* Useful function */


    /* Nin is Ne20, Ne21, Ne22 and Na23 and is a 1-based array */
    /* t is an array of timescales corresponding to tau20,21,22,23 */
    /* dtseconds is the burn time in seconds */
    double alpha,beta,gamma,delta; /* 1.0/timescales */
    Number_density nenasum; /* sum of all abundances */
    /* equilibrium abundances */
    Number_density eqabunds[5];
    /* set the timescales */
    double t20=t[1],t21=t[2],t22=t[3],t23=t[4];
    double tsum; /* sum of timescales */
    double b,c,d; /* cubic equation coeffs */
    double U[5][5]; /* eigenvectors */
    double real_solutions[4]; /* real part of the cubic solutions */
    double imaginary_solutions[4]; /* imaginary part */
    Number_density Nnew[5]; /* The new abundances */
    // NB Nnew must be set to zero here
    double dF; /* change in Fluorine */
    double **matrixN=NULL;
#ifdef MOREDEBUG
    double **matrixM=NULL;
    double **orig=NULL;
#endif
    double **Avector=NULL;
    double lambda[5];
    double s;//,maxtscale,mintscale;
    unsigned int solutions; /* number of solutions of the cubic */
    int i,j=0,k; /* counters */
    /********************************************************/
    Dprint("\n");

    /* Burn fluorine source */
    dF=Nin[0]*(1.0-exp(-dtseconds/t[0]));
    dF=0.0;
    Nin[0]=Nin[0]-dF;
    Nin[1]=Nin[1]+dF;

    Dprint("dF19=%g F19 now %g\n",dF,Nin[0]);

    /* NeNa cycle */
    /* Define sum of number density */
    nenasum=Nin[1]+Nin[2]+Nin[3]+Nin[4];

    /* Define sum of timescales */
    tsum=t20+t21+t22+t23;

    /* Calculate lambda=0 solution : equilibrium values */
    eqabunds[1]=nenasum*t20/tsum; /* Ne20 */
    eqabunds[2]=nenasum*t21/tsum; /* Ne21 */
    eqabunds[3]=nenasum*t22/tsum; /* Ne22 */
    eqabunds[4]=nenasum*t23/tsum; /* Na23 */

    /*
      maxtscale=Max(t20,Max(t21,Max(t22,t23)));
      mintscale=Min(t20,Min(t21,Min(t22,t23)));
      printf("Burn time %g vs max timescale %g\n",dtseconds,maxtscale);
      printf("Burn time %g vs min timescale %g\n",dtseconds,mintscale);

      // burn time long c.f. timescale
      if(dtseconds>10.0*maxtscale)
      {
      Nnew[1]=eqabunds[1];
      Nnew[2]=eqabunds[2];
      Nnew[3]=eqabunds[3];
      Nnew[4]=eqabunds[4];
      return(0);
      }

      // burn time short c.f. timescales : do nothing
      if(dtseconds<0.01*mintscale)
      {
      Nnew[1]=Nin[1];
      Nnew[2]=Nin[2];
      Nnew[3]=Nin[3];
      Nnew[4]=Nin[4];
      return(0);
      }
    */


    Dprint("\nNeNa burn\n");
    Dprint("NeNa tsum=%g nenasum=%g\n",tsum,nenasum);
    Dprint("            %10s %10s %10s %10s\n","Ne20","Ne21","Ne22","Na23");
    Dprint("Abunds in : %10.5g %10.5g %10.5g %10.5g\n",Nin[1],Nin[2],Nin[3],Nin[4]);
    Dprint("Numb fracs: %10.5g %10.5g %10.5g %10.5g\n",Nin[1]/nenasum,Nin[2]/nenasum,
           Nin[3]/nenasum,Nin[4]/nenasum);
    Dprint("Eq abunds : %10.5g %10.5g %10.5g %10.5g\n",eqabunds[1],eqabunds[2],eqabunds[3],eqabunds[4]);
    Dprint("Eq abunds : %10.5g %10.5g %10.5g %10.5g\n",eqabunds[1]/nenasum,eqabunds[2]/nenasum,
           eqabunds[3]/nenasum,eqabunds[4]/nenasum);

    Dprint("Timescales: %10.5g %10.5g %10.5g %10.5g\n",t20,t21,t22,t23);
    Dprint("   /years : %10.5g %10.5g %10.5g %10.5g\n",
           t20/YEAR_LENGTH_IN_SECONDS,t21/YEAR_LENGTH_IN_SECONDS,
           t22/YEAR_LENGTH_IN_SECONDS,t23/YEAR_LENGTH_IN_SECONDS);

    Dprint("Burn time : %10.5g s = % 10.5g\n",dtseconds,dtseconds/YEAR_LENGTH_IN_SECONDS);


    /* there are always problems when one timescale is << the others
       which are due to propagating numerical errors. I have not yet
       managed to cure these! Update: with the new solution method (below)
       these problems are not so bad.
    */

    /* define alpha to delta */
    alpha=1.0/t20;
    beta=1.0/t21;
    gamma=1.0/t22;
    delta=1.0/t23;

    /* calculate b,c,d for the cubic eigenvalue equation */
    b=alpha+beta;
    c=(gamma+alpha)*delta+ b*gamma +(alpha+delta)*beta;
    d=b*gamma*delta + alpha*beta*(delta +gamma) ;
    b+=gamma+delta;
#ifdef MOREDEBUG
    printf("Cubic b=%g c=%g d=%g\n",b,c,d);
    printf("Gnuplot\nx*x*x+%g*x*x+%g*x+%g\n",b,c,d);
#endif
    /* obtain eigenvalues (or at least the real parts) */
    SolveCubic2(b,c,d,
                &solutions,
                real_solutions,
                imaginary_solutions);

#ifdef MOREDEBUG
    printf("Nsolutions : %d\n",solutions);
    printf("Cubic roots are : %g %g+%g*i %g+%g*i  (Re/Im : %g %g)\n",
           real_solutions[0],
           real_solutions[1],
           imaginary_solutions[1],
           real_solutions[2],
           imaginary_solutions[2],
           real_solutions[1]/imaginary_solutions[1],
           real_solutions[2]/imaginary_solutions[2]
        );


    // dump if accuracy is bad
    printf("Check cubic accuracy\n");
    for(i=0;i<3;i++)
    {
        printf("At root %d terms are:\nxxx=%g bxx=%g cx=%g d=%g : Total %g\n",
               i,
               Pow3(real_solutions[i]),
               b*Pow2(real_solutions[i]),
               c*real_solutions[i],
               d,
               Pow3(real_solutions[i])+
               b*Pow2(real_solutions[i])+
               c*real_solutions[i]+
               d
            );
    }
#endif



    if(solutions<3)
    {
        /*
         * There are not enough real solutions for this to work, so skip for a
         * timestep
         */
        Dprint("Skipping NeNa on this timestep because there is only one real solution to the cubic - and this leads to problems.\n");


        /*
         * Usually this means one of the burn timescales is very short
         * and the others are long, so set the short one to eq. and add the
         * difference to the product. Also check if all timescales are short,
         * in which case jump to the eq. solution for them all
         */

        if(Max(Max(t22,t21),Max(t20,t23))<dtseconds)
        {
            Dprint("All timescales are short, jump to eq. solution\n");

            Nin[1]=eqabunds[1];
            Nin[2]=eqabunds[2];
            Nin[3]=eqabunds[3];
            Nin[4]=eqabunds[4];
        }
        else
        {
            Dprint("Approximate solutions to avoid oscillation\n");

            /*
             * assuming the Ne21(pg)Ne22 reaction is always the quickest, we can
             * ignore the other reactions and just decay the Ne21 exponentially
             */
            dF=Nin[2]*(1.0-exp(-dtseconds/t21));
            Nin[2]-=dF;
            Nin[3]+=dF;
            dF=Nin[3]*(1.0-exp(-dtseconds/t22));
            Nin[3]-=dF;
            Nin[4]+=dF;
            dF=Nin[4]*(1.0-exp(-dtseconds/t23));
            Nin[4]-=dF;
            Nin[1]+=dF;
            dF=Nin[1]*(1.0-exp(-dtseconds/t20));
            Nin[1]-=dF;
            Nin[2]+=dF;
        }


        Dprint("Abunds out: %10.5g %10.5g %10.5g %10.5g\n",Nin[1],Nin[2],Nin[3],Nin[4]);
        Dprint("Numb fracs: %10.5g %10.5g %10.5g %10.5g\n",Nin[1]/nenasum,Nin[2]/nenasum,
               Nin[3]/nenasum,Nin[4]/nenasum);
        Dprint("Sum in/out = %g\n",
               (Nin[1]+Nin[2]+Nin[3]+Nin[4])/nenasum);

        // Check NeNa total is conserved to within 10%
        if(Abs_more_than((Nin[1]+Nin[2]+Nin[3]+Nin[4]-nenasum) ,0.1*nenasum))
        {
            Exit_binary_c(BINARY_C_OUT_OF_RANGE,"Nena not conserved error!\n");
        }

        return(-1);
    }
    else
    {
        /* we have 3 real solutions */

        // set array of eigenvalues lambda
        lambda[1]=0;
        for(i=2;i<5;i++)
        {
            lambda[i]=real_solutions[i-2];
        }
#ifdef MOREDEBUG
        printf("set memory\n");fflush(stdout);
#endif

        // set memory space for the matrix
#ifdef MOREDEBUG
        matrixM=(double **)Calloc(6,sizeof(double*));
        printf("Sizes pointer %d double %d\n",sizeof(double*),sizeof(double));

        orig=(double **)Calloc(6,sizeof(double*));
#endif
        matrixN=(double **)Calloc(6,sizeof(double*));
        Avector=(double **)Calloc(6,sizeof(double*));

        for(i=1;i<5;i++)
        {
            matrixN[i]=(double *)Calloc(6,sizeof(double));
#ifdef MOREDEBUG
            matrixM[i]=(double *)Calloc(6,sizeof(double));
            orig[i]=(double *)Calloc(6,sizeof(double));
#endif
            Avector[i]=(double *)Calloc(6,sizeof(double));
        }

#ifdef MOREDEBUG
        printf("set eigenvectors\n");fflush(stdout);
#endif
        // for each eigenvalue set the eigenvectors
        // then set the matrices M and N
        // and the initial vector
        for(i=1;i<5;i++) // loop over eigenvalue i
        {
#ifdef MOREDEBUG
            printf("i=%d lambda=%g\n",i,lambda[i]);fflush(stdout);
#endif
            // this is the new solution (with w=1)
            // U[x][y] is for eigenvalue x, isotope y
#ifdef MOREDEBUG
            printf("Set eigenvector i=%d : ",i);
#endif
            U[i][1]=1.0;
            U[i][2]=(lambda[i]/gamma+1)*(lambda[i]+alpha)*(lambda[i]/delta+1)/
                beta;
            U[i][3]=(lambda[i]+alpha)*(lambda[i]/delta+1)/gamma;
            U[i][4]=(lambda[i]+alpha)/delta;
#ifdef MOREDEBUG
            printf("%g %g %g %g\n",U[i][1],U[i][2],U[i][3],U[i][4]);
#endif
            // Normalize eigenvector
            /*
              s=0.0;
              for(j=1;j<5;j++)
              {
              s+=U[i][j]*U[i][j];
              }
              s=sqrt(s);
              for(j=1;j<5;j++)
              {
              U[i][j]/=s;
              }
            */
            // and set up the matrix:
            // column i=eigenvalue
            // row j=isotope
            for(j=1;j<5;j++)
            {
                matrixN[j][i]=U[i][j];

            }

            // and initial values
            Avector[1][i]=Nin[i];
        }
        /*
        // Invert A
        s=Avector[1][1];
        Avector[1][1]=Avector[1][4];
        Avector[1][4]=s;
        s=Avector[1][2];
        Avector[1][2]=Avector[1][3];
        Avector[1][3]=Avector[1][2];
        */

#ifdef MOREDEBUG
        // set the matrix M
        matrixM[1][1]=-alpha;
        matrixM[1][4]=delta;
        matrixM[2][1]=alpha;
        matrixM[2][2]=-beta;
        matrixM[3][2]=beta;
        matrixM[3][3]=-gamma;
        matrixM[4][3]=gamma;
        matrixM[4][4]=-delta;

        // copy matrix N to orig
        for(i=1;i<5;i++) // row
        {
            for(j=1;j<5;j++) // column
            {
                orig[i][j]=matrixN[i][j];
            }
        }

        printf("matrixM in eigenproblem is\n");fflush(stdout);

        for(i=1;i<5;i++) // row
        {
            for(j=1;j<5;j++) // column
            {
                printf("% 6.3e ",matrixM[i][j]);
            }
            printf("\n");
        }


        // test eigenvectors
        for(i=1;i<5;i++) // loop over eigenvalue/eigenvector i
        {
            printf("Test eigenvalue %d = %g \n",i,lambda[i]);
            // loop over isotope k
            for(k=1;k<5;k++)
            {
                printf("Isotope k=%d : ",k);
                s=0.0;
                for(j=1;j<5;j++) // j is the dummy variable
                {
                    s += matrixM[k][j]*U[i][j];
                }
                printf("% 6.3e vs lambda * U%d = %g error %2.2f %%\n",
                       s,i,U[i][k]*lambda[i],
                       100.0*(1.0-s/(U[i][k]*lambda[i])));
            }
        }

        printf("matrixN for inversion is\n");fflush(stdout);
        for(i=1;i<5;i++) // row
        {
            for(j=1;j<5;j++) // column
            {
                printf("% 6.3e ",matrixN[i][j]);
            }
            printf("\n");
        }

        /* maxima output
           printf("N: MATRIX[");fflush(stdout);
           for(i=1;i<5;i++) // row
           {
           printf("[");
           for(j=1;j<5;j++) // column
           {
           printf("% 6.3e ",matrixN[i][j]);
           if(j!=4) printf(",");
           }

           printf("]");
           if(i!=4) printf(",");
           }
           printf("]\n");
        */
        printf("Initial values\n");
        for(i=1;i<5;i++)
        {
            printf("% 6.3e\n",Avector[1][i]);
        }



#endif

        /*
         * Use GSL's LU decomposition to invert
         * the matrix.
         */
        double * Avector_copy = Calloc(4,sizeof(double));
        double * matrixN_copy = Calloc(4*4,sizeof(double));
        double * invdata = Calloc(4*4,sizeof(double));
        for(i=1;i<5;i++)
        {
            Avector_copy[i-1] = Avector[1][i];
            for(j=1;j<5;j++)
            {
                int index = (i-1)*4 + (j-1);
                matrixN_copy[index] = matrixN[i][j];
            }
        }

        gsl_matrix_view m = gsl_matrix_view_array(matrixN_copy,4,4);
        gsl_matrix_view inv = gsl_matrix_view_array(invdata,4,4);
        gsl_permutation * p = gsl_permutation_alloc (4);

#ifdef ALLOC_CHECKS
    if(p==NULL)
    {
        Exit_binary_c_no_stardata(
            BINARY_C_ALLOC_FAILED,
            "Failed to allocate p prior to LU decomp\n");
    }
#endif


        int ss;

        gsl_linalg_LU_decomp(&m.matrix, p, &ss);
        gsl_linalg_LU_invert(&m.matrix, p, &inv.matrix);

        //printf("GSL gave matrix N : \n");
        for(i=1;i<5;i++)
        {
            for(j=1;j<5;j++)
            {
                matrixN[i][j] = gsl_matrix_get(&inv.matrix,i-1,j-1);
                //printf("% 6.3e ",matrixN[i][j]);
            }
            //printf("\n");
        }
        gsl_permutation_free(p);
        Safe_free(Avector_copy);
        Safe_free(matrixN_copy);
        Safe_free(invdata);

#ifdef MOREDEBUG
        printf("Inverted matrixN\n");
        for(i=1;i<5;i++)
        {
            for(j=1;j<5;j++)
            {
                printf("% 6.3e ",matrixN[i][j]);
            }
            printf("\n");
        }
        /* maxima output
           printf("orig: MATRIX[");fflush(stdout);
           for(i=1;i<5;i++) // row
           {
           printf("[");
           for(j=1;j<5;j++) // column
           {
           printf("% 6.3e ",orig[i][j]);
           if(j!=4) printf(",");
           }

           printf("]");
           if(i!=4) printf(",");
           }
           printf("];\n");

           printf("Ninv: MATRIX[");fflush(stdout);
           for(i=1;i<5;i++) // row
           {
           printf("[");
           for(j=1;j<5;j++) // column
           {
           printf("% 6.3e ",matrixN[i][j]);
           if(j!=4) printf(",");
           }

           printf("]");
           if(i!=4) printf(",");
           }
           printf("];\n");
        */

        printf("Test inverse (this should be the identity)\n");
        for(i=1;i<5;i++) // row of the result matrix
        {
            for(j=1;j<5;j++) // column of the result matrix
            {
                // Multiply M and N matrices
                s=0.0;
                for(k=1;k<5;k++)
                {
                    s+=matrixN[i][k]*orig[k][j];
                }
                printf("% 6.3e ",s);
            }
            printf("\n");

        }

#endif
        // calculate normalizing coefficients by multiplying
        // inverse of N times initial abundances
        for(i=1;i<5;i++) // i = eigenvalue number
        {
            s=0;
            for(k=1;k<5;k++) // k = dummy variable
            {
#ifdef MOREDEBUG
                printf("Coeff %d add %g * %g now ",
                       i,matrixN[i][k],Nin[k]);
#endif
                s+=matrixN[i][k]*Nin[k];
#ifdef MOREDEBUG
                printf("%g\n",s);
#endif
            }
            Avector[1][i]=s;
#ifdef MOREDEBUG
            printf("A%d = % 6.3e\n",i,s);
#endif
        }

        // Avector now contains the normalizing coefficients

#ifdef MOREDEBUG
        // check normalizing constants

        // NOTE THE ORDERING HERE IS INCORRECT! PLEASE FIX IT!

        for(j=1;j<5;j++) // loop j over isotope
        {
            printf("isotope %d init %g vs init by calculation ",j,Nin[j]);
            // loop j over isotope, i over eigenvalue/vector
            s=0.0;
            for(i=1;i<5;i++) // loop i over eigenvector
            {
                s+=Avector[1][i]*U[j][i];
            }
            printf("%g\n",s);
        }
#endif
        // set solution vector

        // loop j over isotope number
        for(j=1;j<5;j++)
        {
            // loop i over the eigenvalues/vectors/normalizing constants
            Nnew[j]=0;
            for(i=1;i<5;i++)
            {
#ifdef MOREDEBUG
                printf("Add lambda[i]=%g U[i=%d][j=%d]=%g * A%d=%g ",
                       lambda[i],i,j,
                       U[j][i],i,Avector[1][j]);
#endif
                Nnew[j]+=U[i][j]*
                    Avector[1][i]*
                    exp(lambda[i]*dtseconds);

#ifdef MOREDEBUG
                printf("-> %g\n",Nnew[j]);
#endif
            }
#ifdef MOREDEBUG
            printf("Set isotope %d to %g (frac %3.3f %%)\n",
                   j,Nnew[j],100.0*Nnew[j]/Nin[j]);
#endif
        }
#ifdef MOREDEBUG
        printf("Check Nenasum %2.2f %% of original\n",
               100.0*(Nnew[1]+Nnew[2]+Nnew[3]+Nnew[4])/nenasum);
#endif

        // check that the abundances are all ok
        j=0;
        for(i=1;i<5;i++)
        {
            if(Nnew[i]<0)
            {
#ifdef MOREDEBUG
                printf("WARNING: isotope %d has abundance %g < 0\n",i,Nnew[i]);
#endif
                j=-1;
                i=5;
            }
        }

        if(j==0)
        {
            for(i=1;i<5;i++)
            {
                // abundance > 0 - this is probably an ok solution
                Nin[i]=Nnew[i];
            }
#ifdef MOREDEBUG
            printf("NENA PASS %g %g %g %g\n",t20,t21,t22,t23);
#endif
        }
#if (DEBUG==1)
        else
        {
            Exit_binary_c(BINARY_C_OUT_OF_RANGE,"Solution has a negative abundance! is %g %g %g %g total %g NENA FAIL %g %g %g %g\n",
                          Nnew[1],Nnew[2],Nnew[3],Nnew[4],
                          Nin[1]+Nin[2]+Nin[3]+Nin[4],
                          t20,t21,t22,t23);
        }
#endif


        Dprint("Abunds out: %10.5g %10.5g %10.5g %10.5g\n",Nin[1],Nin[2],Nin[3],Nin[4]);
        Dprint("Numb fracs: %10.5g %10.5g %10.5g %10.5g\n",Nin[1]/nenasum,Nin[2]/nenasum,
               Nin[3]/nenasum,Nin[4]/nenasum);
        Dprint("Sum in/out = %g\n",
               (Nin[1]+Nin[2]+Nin[3]+Nin[4])/nenasum);

        // Check NeNa total is conserved to within 10%
        if(fabs(Nin[1]+Nin[2]+Nin[3]+Nin[4]-nenasum)/nenasum > 0.1)
        {
            Exit_binary_c(BINARY_C_OUT_OF_RANGE,"Nena not conserved error!\n");
        }
        for(i=1;i<5;i++)
        {
            Safe_free(matrixN[i]);
#ifdef MOREDEBUG
            Safe_free(matrixM[i]);
            Safe_free(orig[i]);
#endif
            Safe_free(Avector[i]);
        }
#ifdef MOREDEBUG
        Safe_free(orig);
        Safe_free(matrixM);
#endif
        Safe_free(matrixN);
        Safe_free(Avector);

        return(j);
    }
}




#endif
