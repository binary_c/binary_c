#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef NUCSYN
#include "nucsyn/nucsyn_strided_isotope_loops.h"

/*
 * Short functions to convert and multiply mass fraction arrays
 */
void Xmult(Abundance * Restrict const X,
           const double f)
{
    /* multiply an abundance array X by f */
    Nucsyn_isotope_stride_loop(X[i]*=f);
}


/*
 * Functions to convect from mass fraction (X) to number density (N) and vice
 * versa : requires the nuclear masses (mnuc, must be set!) and the density.
 * The number of species in the various arrays is passed in, though is usually
 * ISOTOPE_ARRAY_SIZE
 */

void X_to_N(const Nuclear_mass * Restrict const imnuc, /* NB inuc=1.0/mnuc */
            const double dens,
            Number_density * Restrict const N,
            const Abundance * Restrict const X,
            const Isotope num_species)
{
    if(num_species == ISOTOPE_ARRAY_SIZE)
    {
        Nucsyn_isotope_stride_loop(N[i] = imnuc[i] * X[i] * dens);
    }
    else
    {
        PRAGMA_GCC_IVDEP
            for(Isotope i=0;i<num_species;i++)
            {
                N[i] = imnuc[i] * X[i] * dens;
            }
    }
}

void N_to_X(const Nuclear_mass * Restrict const mnuc,
            const double dens,
            const Number_density * Restrict const N,
            Abundance * Restrict const X,
            const Isotope num_species)
{
    const double idens = 1.0/dens;
    if(num_species == ISOTOPE_ARRAY_SIZE)
    {
        Nucsyn_isotope_stride_loop(X[i] = mnuc[i] * N[i] * idens);
    }
    else
    {
        PRAGMA_GCC_IVDEP
            for(Isotope i=0;i<num_species;i++)
            {
                X[i] = mnuc[i] * N[i] * idens;
            }
    }
}
#endif//NUCSYN
