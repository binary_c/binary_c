#include "../binary_c.h"
No_empty_translation_unit_warning;


#if defined NUCSYN && defined NUCSYN_RADIOACTIVE_DECAY


/*
 * Radioactive decay of both stars
 */
void nucsyn_radioactive_decay(struct stardata_t * Restrict const stardata)
{
    Foreach_star(star)
    {
        nucsyn_do_radioactive_decay(stardata,
                                    star->Xenv);
        nucsyn_do_radioactive_decay(stardata,
                                    star->Xacc);
    }
    Dprint("Done radioactive decay\n");
}

#endif // NUCSYN_RADIOACTIVE_DECAY && NUCSYN
