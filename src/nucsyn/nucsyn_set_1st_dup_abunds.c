#include "../binary_c.h"
No_empty_translation_unit_warning;

#if defined NUCSYN &&                           \
    defined NUCSYN_FIRST_DREDGE_UP

#ifdef NUCSYN_FIRST_DREDGE_UP_AMANDAS_TABLE
#include "nucsyn_amanda_1dup_v2.h"
#endif//NUCSYN_FIRST_DREDGE_UP_AMANDAS_TABLE

#ifdef NUCSYN_FIRST_DREDGE_UP_RICHARDS_TABLE
#include "nucsyn_richard_1dup.h"
#endif // NUCSYN_FIRST_DREDGE_UP_RICHARDS_TABLE

/*********************************************************/
/* Function to set up the post 1st dregdge up abundances */
/*********************************************************/
#define FDUP_CNOTOT(A) (A[XC12]+A[XC13]+A[XN14]+A[XN15]+A[XO16]+A[XO17])

/* in this mass/metallicity range we can use richard's abundance changes */
#define USE_RICHARD ((m>=0.0)&&(m<=12.0)&&(Less_or_equal(stardata->common.nucsyn_metallicity,1e-4)))

/*
 * X= abundances, changed by this function
 * m,z = mass and metallicity of star
 * star,stardata the appropriate structure for the star
 * asevent = if TRUE then 1st DUP is done instantaneously
 *           if FALSE *and* if NUCSYN_FIRST_DREDGE_UP_PHASE_IN is defined
 *           then 1st DUP is phased in as f(Mc) or f(logL)
 */

void nucsyn_set_1st_dup_abunds(Abundance * Restrict const X,
                               double m,
                               const Stellar_type stellar_type,
                               struct star_t *const star,
                               struct stardata_t *const stardata,
                               Boolean asevent)
{
    /*
     * Don't repeat first dredge up
     */
    if(star->first_dredge_up == TRUE) return;

    double r[72]; // results of interpolations

    const Abundance z = stardata->common.nucsyn_metallicity;

    Dprint("Set 1st DUP abunds in star %d (type %d): m=%g z=%g [asevent=%d]\n",
           star->starnum,stellar_type,m,z,asevent);

#ifdef NUCSYN_STRIP_AND_MIX
    /* do not let these elements change */
    double was[5] = {star->Xenv[XH1],
                     star->Xenv[XHe4],
                     star->Xenv[XC12],
                     star->Xenv[XN14],
                     star->Xenv[XO16]};
#endif // NUCSYN_STRIP_AND_MIX



#ifdef NUCSYN_FIRST_DREDGE_UP_ACCRETION_CORRECTION
    /*
     * the mass used in the tabular interpolation should be the
     * terminal MS mass, not the current mass
     */
    m = star->MS_mass;
#endif//NUCSYN_FIRST_DREDGE_UP_ACCRETION_CORRECTION

#ifdef NUCSYN_FIRST_DREDGE_UP_PHASE_IN
    /*
     * phase in changes to the surface abundance as
     * a function of core mass
     */

    /* save the core mass */
    double mc = star->core_mass[CORE_He];

    /* calculate core mass at which 1st DUP will reach its deepest */
    const double mc1dup = Min(star->mass,
                              mc_1DUP(stardata,m,z));

    if(Is_zero(star->mc_gb_was))
    {
        star->first_dup_phase_in_firsttime = TRUE;
    }
    else if(star->core_mass[CORE_He] < star->mc_gb_was)
    {
        /* core has shrunk: reset star->mc_gb_was : should not happen..
           but...*/
        star->mc_gb_was=0.0;
        star->first_dup_phase_in_firsttime = TRUE;

        Dprint("# First call to DUP phase in\n");
    }

    Dprint("# First DUP phase in defined\n");
#endif // NUCSYN_FIRST_DREDGE_UP_PHASE_IN

    Dprint("# Star %d: m=%g, z=%g, nucsyn_z=%g; C12=%g C13=%g N14=%g ",star->starnum,m,z,stardata->common.nucsyn_metallicity,star->Xenv[XC12],star->Xenv[XC13],star->Xenv[XN14]);

    /* do the interpolation */
    nucsyn_first_dup_interpolation(stardata,
                                   m,
                                   z,
                                   r);

    Dprint("#\n# 1dup: ZAMS m=%g vs current %g, as event? %d (stellar_type %d)\n",m,star->mass,asevent,stellar_type);

    /* range of the interpolation */
    const double zmax = 0.02;
#ifdef NUCSYN_FIRST_DREDGE_UP_RICHARDS_TABLE
    const double zmin = 1e-5;
#else
    const double zmin = 1e-4;
#endif

    Dprint("Interpolation range: zmin=%g zmax=%g\n",zmin,zmax);

    if((stardata->common.nucsyn_metallicity < zmin-TINY)||
       (stardata->common.nucsyn_metallicity > zmax+TINY))
    {
        /*
         * The metallicity in the nucsyn routines is
         * outside the fitted range, so adjust the results
         * by scaling all the deltas
         */
        const double fz = stardata->common.nucsyn_metallicity /
            Max(zmin,Min(zmax,stardata->common.nucsyn_metallicity));

        Dprint("# nucsyn_Z=%g (cf %g) is outside table range: Scale deltas by fz factor %g (zmin=%g zmax=%g)\n",stardata->common.nucsyn_metallicity,Max(zmin,Min(zmax,z)),fz,zmin,zmax);

#ifdef NUCSYN_FIRST_DREDGE_UP_RICHARDS_TABLE
        if(USE_RICHARD)
        {
            /*  */
            for(unsigned int i=5;i<44;i++)
            {
                r[i]*=fz;
            }
            r[38]/=fz; r[39]/=fz; /* H and He are 38 and 39 */
        }
        else
#endif // NUCSYN_FIRST_DREDGE_UP_RICHARDS_TABLE
        {
            /* scale all changes except H, He, Li and Be */
            for(unsigned int i=7;i<72;i++)
            {
                r[i]*=fz;
            }
        }
    }

#ifdef NUCSYN_FIRST_DREDGE_UP_ACCRETION_CORRECTION
    /*
     * Correct CNO changes due to accretion by scaling everything
     * to current total CNO (relative to initial CNO)
     */
    Abundance cno_zams=FDUP_CNOTOT(stardata->preferences->zero_age.XZAMS);

#ifdef NUCSYN_FIRST_DREDGE_UP_ACCRETION_CORRECTION_FROM_TAMS
    /*
     * Use the TAMS abundances as the correction: this is more
     * correct, and works well if the accretion layer is small.
     */
    Abundance cno_now = FDUP_CNOTOT(star->XTAMS);
    double fc = cno_now/cno_zams;
    Dprint("# First DUP CNO enhancement factors fc=%g (CNO ZAMS=%g envelope=%g TAMS=%g: we are using %g)\n",fc,cno_zams,FDUP_CNOTOT(star->Xenv),FDUP_CNOTOT(star->XTAMS),cno_now);
#else
    /*
     * Otherwise, assume the abundances now are ~ those which were at the
     * TAMS, which is true if thermohaline mixing is efficient.
     */
    Abundance cno_now = FDUP_CNOTOT(star->Xenv);
    double fc = cno_now/cno_zams;
    Dprint("# First DUP CNO enhancement factors fc=%g (CNO ZAMS=%g envelope=%g : we are using %g)\n",fc,cno_zams,FDUP_CNOTOT(star->Xenv),cno_now);
#endif // NUCSYN_FIRST_DREDGE_UP_ACCRETION_CORRECTION_FROM_TAMS

    Dprint("# First DUP CNO was [C12/Fe56]=%g [C13/Fe56]=%g [N14/Fe56]=%g [O16/Fe56]=%g (C/O=%g C12/C13=%g C13=%g)\n",
           nucsyn_square_bracket(X,stardata->preferences->zero_age.Xsolar,XC12,XFe56),
           nucsyn_square_bracket(X,stardata->preferences->zero_age.Xsolar,XC13,XFe56),
           nucsyn_square_bracket(X,stardata->preferences->zero_age.Xsolar,XN14,XFe56),
           nucsyn_square_bracket(X,stardata->preferences->zero_age.Xsolar,XO16,XFe56),
           X[XC12]/X[XO16]*(16.0/12.0),
           X[XC12]/X[XC13]*(13.0/12.0),
           X[XC13]
        );

#ifdef NUCSYN_FIRST_DREDGE_UP_RICHARDS_TABLE
    if(USE_RICHARD)
    {
        /* Scale CNO (Richard) */
        r[5] *= fc;
        r[6] *= fc;
        r[7] *= fc;
        r[8] *= fc;
        r[9] *= fc;
        r[40] *= fc;
        r[41] *= fc;
        r[42] *= fc;
    }
    else
#endif // NUCSYN_FIRST_DREDGE_UP_RICHARDS_TABLE
    {
        /* Scale CNO (Amanda) */
        r[7] *= fc;
        r[8] *= fc;
        r[10] *= fc;
        r[11] *= fc;
        r[12] *= fc;
        r[14] *= fc;
        r[15] *= fc;
        r[16] *= fc;
        r[17] *= fc;
    }
#endif //NUCSYN_FIRST_DREDGE_UP_ACCRETION_CORRECTION


#ifdef NUCSYN_FIRST_DREDGE_UP_PHASE_IN
    /*
     * phase in changes to the surface abundance as
     * a function of core mass
     */
    if(asevent == TRUE &&
       star->first_dup_phase_in_firsttime == FALSE)
    {
        /* called as a event but we have been in here before! pretend... and finish 1st DUP */
        asevent = FALSE;
        star->first_dredge_up = TRUE;
        mc = star->core_mass[CORE_He]; /* enforce 100% of the DUP */
        Dprint("# Warning: Called as event, but not phase in has happened before: truncated RGB? forcing asevent to be FALSE\n");

    }

    if(asevent==FALSE)
    {
        /*
         * dX/dMc  ~ delta X / (mc1dup - mc_bagb)
         *
         * Or, perhaps,
         *
         * dX/dMc ~ delta X / ( mc * ln(McDUP/McBGB))
         */

        if(star->mc_gb_was > TINY)
        {
            /* calculate change in core mass */
            const double dmc = mc - star->mc_gb_was;

            /* hence the phase in factor, which should sum to 1! */
            double dfphasein = dmc / (mc1dup - star->mc_bgb);
            Dprint("dfphasein = %g / (%g - %g) = %g\n",dmc,mc1dup,star->mc_bgb,dfphasein);
            dfphasein = Max(0.0,Min(1.0,dfphasein));

            /* if we've reached the deepest point, don't come back into this function! */
            if(More_or_equal(mc, mc1dup)) star->first_dredge_up=TRUE;

            /* now alter the abundances according to the phase-in factor */
            for(unsigned int i=0;i<72;i++) r[i] *= dfphasein;

#if (DEBUG==1)
#ifdef NUCSYN_FIRST_DREDGE_UP_RICHARDS_TABLE
            Dprint("#   Phase in 1st dup mc=%g (mc1dup=%g) dfphasein=%g H1=%g (delta=%g) He4=%g (delta=%g) C12=%g (delta=%g) N14=%g (delta=%g)\n",
                   mc,mc1dup,dfphasein,
                   X[XH1],r[38],X[XHe4],r[39],X[XC12],r[40],X[XN14],r[41]
                );
#else
            Dprint("#   Phase in 1st dup mc=%g (mc1dup=%g) dfphasein=%g H1=%g (delta=%g) He4=%g (delta=%g) C12=%g (delta=%g) N14=%g (delta=%g)\n",
                   mc,mc1dup,dfphasein,
                   X[XH1],r[0],X[XHe4],r[3],X[XC12],r[7],X[XN14],r[11]
                );
#endif // NUCSYN_FIRST_DREDGE_UP_RICHARDS_TABLE
#endif // NUCSYN_FIRST_DREDGE_UP_PHASE_IN
        }
        else
        {
            /* first time: no change */
            memset(r,0,sizeof(double)*72);
        }
        star->mc_gb_was = star->core_mass[CORE_He];
    }
#endif //NUCSYN_FIRST_DREDGE_UP_PHASE_IN


    /* apply the changes */
    nucsyn_first_dup_apply(m,
                           stardata,
                           X,
                           r);

    /*
     * dodgy fudges to make X=1.0: required
     * because we are interpolating and there
     * are thus "errors"
     */
    Nucsyn_isotope_stride_loop( X[i] = Max(0.0,Min(1.0,X[i])) );
    nucsyn_renormalize_to_max(X);

#if DEBUG==1 && \
    defined NUCSYN_FIRST_DREDGE_UP_ACCRETION_CORRECTION
    Dprint("# First DUP CNO becomes [C12/Fe56]=%g [C13/Fe56]=%g [N14/Fe56]=%g [O16/Fe56]=%g (C/O=%g C12/C13=%g C13=%g N14=%g)\n",
           nucsyn_square_bracket(X,stardata->preferences->zero_age.Xsolar,XC12,XFe56),
           nucsyn_square_bracket(X,stardata->preferences->zero_age.Xsolar,XC13,XFe56),
           nucsyn_square_bracket(X,stardata->preferences->zero_age.Xsolar,XN14,XFe56),
           nucsyn_square_bracket(X,stardata->preferences->zero_age.Xsolar,XO16,XFe56),
           X[XC12]/X[XO16]*(16.0/12.0),
           X[XC12]/X[XC13]*(13.0/12.0),
           X[XC13],X[XN14]
        );

#ifdef NUCSYN_ALL_ISOTOPES
#undef _denom
#define _denom XFe52,XFe54,XFe56,XFe57,XFe58
#define _denomn 5
#else
#define _denom XFe56
#define _denomn 1
#endif // NUCSYN_ALL_ISOTOPES
    Dprint("# Multibracket [C/Fe]=%g [N/Fe]=%g [O/Fe]=%g\n",
           nucsyn_square_multibracket(X,stardata->preferences->zero_age.Xsolar,
                                      (Isotope[]){XC12,XC13},2,
                                      (Isotope[]){_denom},_denomn),
           nucsyn_square_multibracket(X,stardata->preferences->zero_age.Xsolar,
                                      (Isotope[]){XN14,XN15},2,
                                      (Isotope[]){_denom},_denomn),
           nucsyn_square_multibracket(X,stardata->preferences->zero_age.Xsolar,
                                      (Isotope[]){XO16,XO17,XO18},3,
                                      (Isotope[]){_denom},_denomn)
        );

#endif


#ifdef NUCSYN_STRIP_AND_MIX
    /*
     * When using the strip-table,
     */
    star->Xenv[XH1] = was[0];
    star->Xenv[XHe4] = was[1];
    star->Xenv[XC12] = was[2];
    star->Xenv[XN14] = was[3];
    star->Xenv[XO16] = was[4];
#endif

    return;
}



void nucsyn_first_dup_interpolation(struct stardata_t * const stardata,
                                    double m,
                                    const double z,
                                    double * Restrict r)
{


#ifdef NUCSYN_FIRST_DREDGE_UP_AMANDAS_TABLE
    m=Max(0.5,m);
    m=Min(15.0,m);

#ifdef NUCSYN_FIRST_DREDGE_UP_RICHARDS_TABLE
    if(USE_RICHARD)
    {
        Dprint("Do interpolation for first DUP using Richard's tables\n");
        Const_data_table richard_table[]={RICHARD_1DUP_DATA_TABLE};
        // reuse r[72], as richard's data only uses 44 (<72) isotopes
        rinterpolate(richard_table,
                     stardata->persistent_data->rinterpolate_data,
                     2,
                     44,
                     NUCSYN_RICHARD_TABLE_LINES,
                     (double[]){log10(stardata->common.nucsyn_metallicity/0.02),m},
                     r,
                     1);
        Dprint("# Richard's table (raw) m=%g Z=%g: DH1=%g DHe4=%g DC12=%g DC13=%g DN14=%g DO16=%g\n",
               m,stardata->common.nucsyn_metallicity,r[38],r[39],r[40],r[5],r[41],r[42]);
    }
    else
#endif// NUCSYN_FIRST_DREDGE_UP_RICHARDS_TABLE
    {
        Const_data_table amanda_table[]={AMANDA_1DUP_DATA_TABLE};
        Dprint("Do interpolation for first DUP using Amanda's tables\n");
        rinterpolate(amanda_table,
                     stardata->persistent_data->rinterpolate_data,
                     2,
                     72,
                     80,
                     (double[]){Min(6.0,m),z},
                     r,
                     1);
        Dprint("# Amanda's table (raw) m=%g nucsynZ=%g: DH1=%g DHe4=%g DC12=%g DC13=%g DN14=%g DO16=%g\n",
               m,z,r[0],r[3],r[7],r[8],r[11],r[15]);
    }
#endif // NUCSYN_FIRST_DREDGE_UP_AMANDAS_TABLE


}

void nucsyn_first_dup_apply(const double m,
                            struct stardata_t * const stardata,
                            Abundance * Restrict const X,
                            double * Restrict const r)
{

    /* apply the first DUP changes r to abundance array X */

#ifdef NUCSYN_FIRST_DREDGE_UP_RICHARDS_TABLE
    if(USE_RICHARD)
    {
        Dprint("# Richard's table (modified) r array : DH1=%g DHe4=%g DC12=%g DC13=%g DN14=%g DO16=%g\n",
               r[38],r[39],r[40],r[5],r[41],r[42]);

        X[XH2]+=r[0];X[XHe3]+=r[1];

#if !defined LITHIUM_TABLES && !defined NUCSYN_ANGELOU_LITHIUM
        X[XLi7]+=r[2];
#endif

        X[XBe7]+=r[3];X[XB11]+=r[4];X[XC13]+=r[5];
//X[XC14]+=r[6];
        X[XN15]+=r[7];X[XO17]+=r[8];X[XO18]+=r[9];X[XF19]+=r[10];X[XNe21]+=r[11];X[XNe22]+=r[12];X[XNa22]+=r[13];X[XNa23]+=r[14];X[XMg24]+=r[15];X[XMg25]+=r[16];X[XMg26]+=r[17];X[XAl26]+=r[18]+r[19]; X[XAl27]+=r[20];X[XSi28]+=r[21];X[XSi29]+=r[22];X[XSi30]+=r[23];
        X[XFe56]+=r[28];
#ifdef NUCSYN_ALL_ISOTOPES
        X[XP31]+=r[24];X[XS32]+=r[25];X[XS33]+=r[26];X[XS34]+=r[27];X[XFe57]+=r[29];X[XFe58]+=r[30];

//X[XFe59]+=r[31];X[XFe60]+=r[32];

        X[XCo59]+=r[33];X[XNi58]+=r[34];
//X[XNi59]+=r[35];
        X[XNi60]+=r[36];X[XNi61]+=r[37];

#endif//NUCSYN_ALL_ISOTOPES
        X[XH1]+=r[38];X[XHe4]+=r[39];X[XC12]+=r[40];X[XN14]+=r[41];X[XO16]+=r[42];X[XNe20]+=r[43];

        Dprint("# Richard's table (modified) final X : H1=%g He4=%g C12=%g C13=%g N14=%g O16=%g\n",
               X[XH1],X[XHe4],X[XC12],X[XC13],X[XN14],X[XO16]);

    }
    else
#endif // NUCSYN_FIRST_DREDGE_UP_RICHARDS_TABLE

#ifdef NUCSYN_FIRST_DREDGE_UP_AMANDAS_TABLE
    {
        Dprint("# Amanda : DH1=%g DHe4=%g DC12=%g DC13=%g DN14=%g DO16=%g\n",
               r[0],r[3],r[7],r[8],r[11],r[15]);

        X[XH1]+=r[0];  X[XH2]+=r[1];  X[XHe3]+=r[2];  X[XHe4]+=r[3];
#if !defined LITHIUM_TABLES && !defined NUCSYN_ANGELOU_LITHIUM
        X[XLi7]+=r[4]; X[XLi7]=Max(0.0,X[XLi7]);
#endif
        X[XBe7]+=r[5]; X[XBe7]=Max(0.0,X[XBe7]);  X[XC12]+=r[7];  X[XC13]+=r[8];  X[XN13]+=r[10];  X[XN14]+=r[11];  X[XN15]+=r[12];  X[XO15]+=r[14];  X[XO16]+=r[15];  X[XO17]+=r[16];  X[XO18]+=r[17];  X[XF17]+=r[19];  X[XF18]+=r[20];  X[XF19]+=r[21];  X[XNe20]+=r[24];  X[XNe21]+=r[25];  X[XNe22]+=r[26];  X[XNa22]+=r[28];  X[XNa23]+=r[29];
// X[XNa24]+=r[30];

        X[XMg24]+=r[32];  X[XMg25]+=r[33];  X[XMg26]+=r[34];  X[XAl26]+=r[37];  X[XAl27]+=r[39];
//X[XAl28]+=r[40];
        X[XFe56]+=r[58];
        X[XSi28]+=r[42];  X[XSi29]+=r[43];  X[XSi30]+=r[44];

#ifdef NUCSYN_ALL_ISOTOPES
        X[XP30]+=r[49];  X[XP31]+=r[50];  X[XS32]+=r[54];  X[XS33]+=r[55];  X[XS34]+=r[56];
//X[XS35]+=r[57];
          X[XFe57]+=r[59];  X[XFe58]+=r[60];
//X[XFe59]+=r[61];
//  X[XFe60]+=r[62];
        X[XCo59]+=r[64];
// X[XCo60]+=r[65];  X[XCo61]+=r[66];
        X[XNi58]+=r[67];
//X[XNi59]+=r[68];
        X[XNi60]+=r[69];  X[XNi61]+=r[70];
#endif//NUCSYN_ALL_ISOTOPES
    }
#endif // NUCSYN_FIRST_DREDGE_UP_AMANDAS_TABLE

}

#endif // NUCSYN && NUCSYN_FIRST_DREDGE_UP
