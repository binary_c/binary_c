#include "../binary_c.h"
No_empty_translation_unit_warning;


#if defined NUCSYN && defined NUCSYN_SIGMAV_PRE_INTERPOLATE

void print_sigmav_table(struct store_t * Restrict store)
{
    size_t sigmav_size=(size_t)(SIGMAV_SIZE+1);
    int i,j;
    _printf("Sigmav table\n");
    for(i=0;i<NUCSYN_SIGMAV_INTERPOLATION_RESOLUTION;i++)
    {
        _printf("Sigmav table line %d at %p: ",
                i,
                (void*)((char*)store->sigmav->data+i*sigmav_size));
        for(j=0;j<6;j++)
        {
            _printf("%d=%g ",j,*(store->sigmav->data+i*sigmav_size+j));
            fflush(stdout);
        }
        _printf("\n");
    }
}

#endif
