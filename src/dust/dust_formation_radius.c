#include "../binary_c.h"
No_empty_translation_unit_warning;


double Pure_function dust_formation_radius(const struct star_t * Restrict const star)
{
    /*
     * Dust formation radius as in
     * Abate et al. 2014,2015
     */
    return
        dust_formation_radius_given_condensation_temperature(star,
                                                             dust_condensation_temperature(star));
}
