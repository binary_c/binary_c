############################################################
#
# Population ensemble inlist for binary_c's ensemble_manager
#
############################################################

############################################################
#
# This is the generic "GCE" (Galactic Chemical Evolution)
# /cosmology yieldsets project
#
project = "GCE"

############################################################
#
# binary_c arguments : these control the physics and ensemble
#
#
# binary_c_args are passed into binary_c as arguments,
#
# e.g.
#    binary_c ... alpha_ce 1.0 lambda_ce 0.5 ...
#
# you should give each a list of possible values.
#
# Note that 'defaults' should take an empty list, because this
# just uses all the binary_c defaults.
#
# You can specify the default value of each variable your
# lists, but this will not be set hence we run as few
# populations as possible.
#
# The entries in binary_c_args are:
# 1) A key,value pair where the key is the arg name, and the
#    value is a list of values to which this arg is set.
#
# 2) A key,value pair where the key is the arg name, and the
#    value is a tuple which in turn contains as elements:
#    [0] a list of values to which this arg is set
#    [1] a dictionary which is expanded for each of these values
#
# This way you can easily nest your variables to any extent.
#
# e.g.
#
# This changes the metallicity of each run
#
# { 'metallicity' : [1e-4, 1e-3, 1e-2] }
#
# i.e. 3 combinations.
#
# This changes the alpha_ce for each of those metallicities,
# i.e. 9 combinations.
#
# { 'metallicity' :
#    (
#       [ 1e-4, 1e-3, 1e-2 ],
#       {
#           'alpha_ce' : [0.1,0.5,1.0]
#       }
#    )
# }
#
############################################################

# binaries is a local variable
#
# if True we include parameters that change binary systems
#    and set up the Moe distributions for binaries.
#
# if False we include only standard single-(and binary-) star
#    physics, and run only single stars.
#
binaries = True

variable_binary_c_args = {
    'metallicity' :
    [
        0.02 #1e-4,1e-3,0.004,0.008,0.01,0.02,0.03
    ],
}

fixed_binary_c_args = {

            # winds and mass loss
            'wind_mass_loss' : [
                #'WIND_ALGORITHM_HURLEY2002',
                #'WIND_ALGORITHM_SCHNEIDER2018',
                'WIND_ALGORITHM_BINARY_C_2022'
            ],
    'overspin_algorithm' : [
        'OVERSPIN_MASSLOSS',
    ],
    'rotationally_enhanced_mass_loss' : [
        'ROTATIONALLY_ENHANCED_MASSLESS_ANGMOM'
    ],
    'gbwind' : [
        'GB_WIND_REIMERS',
        #   'GB_WIND_SCHROEDER_CUNTZ_2005',
        #   'GB_WIND_GOLDMAN_ETAL_2017'
    ],

    'gb_reimers_eta' : [
        0.5
    ],
    'tpagbwind' : [
        'TPAGB_WIND_VW93_KARAKAS',
        #    'TPAGB_WIND_VW93_ORIG',
        #   'TPAGB_WIND_REIMERS',
        #  'TPAGB_WIND_BLOECKER'
    ],
    'rotationally_enhanced_mass_loss' : [
        'ROTATIONALLY_ENHANCED_MASSLOSS_NONE',
        # 'ROTATIONALLY_ENHANCED_MASSLOSS_LANGER_FORMULA',
        #'ROTATIONALLY_ENHANCED_MASSLOSS_ANGMOM',
        #'ROTATIONALLY_ENHANCED_MASSLOSS_LANGER_FORMULA_AND_ANGMOM'
    ],
    'max_HeWD_mass' : [
        0.7
    ],
    # massive-star supernovae
    'BH_prescription' : [
        #'BH_HURLEY2002',
        #'BH_BELCZYNSKI',
        #'BH_SPERA2015',
        'BH_FRYER12_DELAYED'
        #'BH_FRYER12_RAPID',
        #'BH_FRYER12_STARTRACK'
    ],

    # core-collapse supernova kicks
    'sn_kick_distribution_IBC' : [
        'KICK_VELOCITY_GIACOBBO_MAPELLI_2020'
    ],
    'sn_kick_distribution_II' : [
        'KICK_VELOCITY_GIACOBBO_MAPELLI_2020'
    ],
    'sn_kick_dispersion_IBC' : [
        265.0
    ],
    'sn_kick_dispersion_II' : [
        265.0
    ],
    # core-collapse supernova yields
    'core_collapse_supernova_algorithm' : [
        'NUCSYN_CCSN_LIMONGI_CHIEFFI_2018'
    ],
    'core_collapse_rprocess_algorithm' : [
        'NUCSYN_CCSN_RPROCESS_SIMMERER2004'
    ],
    'core_collapse_rprocess_mass' : [
        1e-6
    ],

    # nucleosynthesis
    'initial_abundance_mix' : [
        'NUCSYN_INIT_ABUND_MIX_ASPLUND2009'
    ],
    'delta_mcmin' : [
        -0.1
    ],
    'lambda_min' : [
        'THIRD_DREDGE_UP_LAMBDA_MIN_AUTO'
    ],
    'mc13_pocket_multiplier' : [
        1.0
    ],

    # binary physics:
    # this is only included if binaries==True
    **({
        # qcrit for common envelope
        'qcrit_nuclear_burning' : 'QCRIT_TEMMINK2022',

        # Companion-reinforced attrition
        'CRAP_parameter' : [
            0.0
        ],

        # WD accretion rate limits
        'WD_accretion_rate_new_giant_envelope_lower_limit_hydrogen_donor' : [
            'DONOR_RATE_ALGORITHM_K2014_P2014'
        ],
        'WD_accretion_rate_new_giant_envelope_lower_limit_helium_donor' : [
            'DONOR_RATE_ALGORITHM_K2014_P2014'
        ],
        'WD_accretion_rate_new_giant_envelope_lower_limit_other_donor' : [
            'DONOR_RATE_ALGORITHM_K2014_P2014'
        ],
        'WD_accretion_rate_novae_upper_limit_hydrogen_donor' : [
            'DONOR_RATE_ALGORITHM_K2014_P2014'
        ],
        'WD_accretion_rate_novae_upper_limit_helium_donor' : [
            'DONOR_RATE_ALGORITHM_K2014_P2014'
        ],
        'WD_accretion_rate_novae_upper_limit_other_donor' : [
            'DONOR_RATE_ALGORITHM_K2014_P2014'
        ],
        'COWD_to_ONeWD_accretion_rate' : [
            2.05e-6
        ],

                # hence novae
                'nova_retention_method' : [
                    'NOVA_RETENTION_ALGORITHM_CONSTANT',
                ],
        'nova_retention_fraction_H' : [
            1e-3
        ],
        'nova_retention_fraction_He' : [
            1e-3
        ],
        'nova_yield_CO_algorithm' : [
            'NOVA_YIELD_CO_ALGORITHM_JOSE_HERNANZ_1998',
        ],
        'nova_yield_ONe_algorithm' : [
            'NOVA_YIELD_ONe_ALGORITHM_JOSE_2022',
        ],

        # low/intermediate-mass-star Ia supernovae

        # mass to be accreted for COWD to explode in
        # double detotation
        'mass_accretion_for_COWD_DDet' : [
            0.05
        ],
        # disable ONeWD double detonations
        'mass_accretion_for_ONeWD_DDet' : [
            0.0
        ],
        # SNIa yield choice
        'type_Ia_MCh_supernova_algorithm' : [
            'TYPE_IA_SUPERNOVA_ALGORITHM_SEITENZAHL2013_AUTOMATIC'
        ],
        'Seitenzahl2013_model' : [
            'N100'
        ],
        # turn thermohaline mixing on/off when accreting
        'no_thermohaline_mixing' : [
            0
        ],

        # common envelopes
        # combinations of different alpha,lambda for common envelope
        'comenv_prescription' : [
            'COMENV_BSE',
        ],
        'minimum_donor_menv_for_comenv' : [
            0.1
        ],
        'alpha_ce' : [
            0.25
        ],
        'lambda_ce' : [
            'LAMBDA_CE_KLENCKI_2020'
        ],
        'lambda_ionisation' : [
            0.1
        ],
        'post_ce_adaptive_menv' : [
            'False'
        ],

        # tides
        'tidal_strength_factor' : [
            1.0
        ],
        'E2_prescription' : [
            'E2_IZZARD'
        ],

        # RLOF
        'RLOF_method' : [
            'RLOF_METHOD_CLAEYS'
        ],
        'use_periastron_Roche_radius' : [
            'False'
        ],

        'WRLOF_method' : [
            #'WRLOF_NONE',
            'WRLOF_Q_DEPENDENT'
            #'WRLOF_QUADRATIC'
        ],

        # generic stellar mergers
        'merger_mass_loss_fraction' : [
            'MERGER_MASS_LOSS_FRACTION_GLEBBEEK2013'
        ],

                # NSNS mergers : fraction and yields
                'merger_mass_loss_fraction_by_stellar_type_NS' : [
                    'MERGER_MASS_LOSS_FRACTION_RADICE2018'
                ],
        'NS_merger_yield_algorithm' : [
            'NS_MERGER_ALGORITHM_RADICE2018'
        ],
        'Radice2018_EOS' : [
            'RADICE2018_EOS_BHBlp'
        ],

    } if binaries else {})
}

##################################################
#
# ensemble arguments, these control the ensemble's grid and
# other miscellaneous things
#
############################################################
# YOU NEED TO check these parameters make sense for you
#
ensemble_args = {
    'dists' : 'Moe', # Moe, MoePDF, 2008(+), 2018(+)
    'binaries' : binaries,
    'r' : 40, # resolution of M1 (if not const_dt), M2 and period, e.g. 100
    'M1spacing' : 'const_dt', # const_dt, logM1
    'fsample' : 0.25,
    'normalize' : True,
    'mmax' : 100.0,
    'log_runtime_systems' : 0,
    'run_zero_probability_system' : 0,
    'combine_ensemble_with_thread_joining' : True,

    # if using Moe's distribution, we want a
    # list of mass spacings that's a bit different to normal
    'const_dt_mass_spacings' : '((0.07,1,-0.1),(0.8,1.5,0.05),(1,30,-0.5),(30,80,1))',

    ##################################################
    'verbosity' : 1,
    'log_dt' : 300, # 300, # log to stdout every log_dt seconds
    'log_args' : 1, # do not save args by default
    'log_args_dir' : '/tmp/binc',
    'log_failed_systems' : True,
    'log_failed_systems_dir': "/tmp/binerror",

    ############################################################
    # ensemble options
    'ensemble' : 1,
    'ensemble_defer' : 1,
    'ensemble_logtimes' : 1,
    'ensemble_startlogtime' : 0.1, # only want old stars
    #'ensemble_dt' : 1e3, # not required when ensemble_logtimes==1
    'ensemble_logdt' : 0.1, # not required when ensemble_logtimes==0
    'ensemble_filters_off' : 1,
    'save_ensemble_chunks' : False,

    # filters required for GCE
    'ensemble_filter_CHEMICAL_YIELDS' : 1, # require chemical yields
    'ensemble_filter_SCALARS' : 1, # required scalar number counts
    'ensemble_filter_TEST' : 1, # include test data
    'ensemble_filter_INITIAL_DISTRIBUTIONS' : 1, # always save initial distributions

    'dry_run_num_cores' : 18,
    'num_cores' : 18,
    'do_dry_run' : True,
    'comenv_algorithm' : 'COMENV_ALGORITHM_BINARY_C',

}


############################################################
#
# HPC arguments
#
############################################################
HPC_options = {
    'njobs' : 16,
    'memory' : '2000MB',
    'warn_max_memory' : '4000MB',
    'max_time' : 10000,
    'directory' : None,
    'condor_requirements' : '',
    'slurm_partition' : 'shared',
    'cluster_type' : None,
}
