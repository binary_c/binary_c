#!/usr/bin/env perl
use rob_misc;

# script to sync the latest librinterpolate to binary_c

my $src = "$ENV{HOME}/git/libmemoize/";
my $dest = '.';

`rsync -avP $src/src/*.c ./`;
`rsync -avP $src/src/*.h ./`;
`rsync -avP $src/CHANGELOG $src/LICENCE ./`;
`rsync -avP $src/README ./`;
unlink 'test_memoize.c';
unlink 'memoize-config.c';

foreach my $cfile (`ls *.c`)
{
    chomp $cfile;
    my $code = slurp($cfile);

    # headers
    $code = '#define __BINARY_C_LINT_SKIP
#include "../binary_c_code_options.h"
#include "../binary_c_error_codes.h"
#include "../binary_c_exit_macros.h"
#include "../breakpoints/breakpoints_prototypes.h"
#include "../binary_c_exit_prototypes.h"

#ifndef __HAVE_LIBMEMOIZE__

' . $code . '
#endif // __HAVE_LIBMEMOIZE__
typedef int prevent_ISO_compile_warning;
        ';

    # use native realloc
    $code =~ s/Realloc\(/realloc\(/g;

    dumpfile($cfile,$code."\n\n");

}

foreach my $hfile ('memoize_internal.h')
{
    my $code = slurp($hfile);
    $code =~s/fprintf.*\s+exit\(1\)/Exit_binary_c_no_stardata(BINARY_C_ALLOC_FAILED,\"libmemoize memory allocation failed\")/;
    dumpfile($hfile,$code."\ntypedef int prevent_ISO_compile_warning;\n\n");
}
