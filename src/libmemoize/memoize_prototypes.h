#pragma once
#ifndef MEMOIZE_PROTOTYPES_H
#define MEMOIZE_PROTOTYPES_H

#ifdef MEMOIZE
#include "memoize_compiler.h"

/*
 * Prototypes for the memoize library.
 *
 * Please see the LICENCE file for licensing conditions. 
 */

void * memoize_store_result(struct memoize_hash_t * RESTRICT const memo,
                            const char * RESTRICT const funcname,
                            const size_t nmax,
                            const size_t parameter_memsize,
                            const void * const parameter,
                            const size_t result_memsize,
                            const void * const result
#ifdef MEMOIZE_STATS
                            ,ticks * const timer
#endif // MEMOIZE_STATS
);
struct memoize_hash_item_t * Pure_function
memoize_search_hash(const struct memoize_hash_t * RESTRICT const memo,
                    const char * RESTRICT const funcname);

struct memoize_hash_t * memoize_initialize(struct memoize_hash_t * RESTRICT * RESTRICT const m);
void memoize_clear_hash_contents(struct memoize_hash_t * RESTRICT const memo);

void memoize_free(struct memoize_hash_t ** RESTRICT memo);

void * Pure_function memoize_search_result(
#ifndef MEMOIZE_STATS
    const
#endif
    struct memoize_hash_t * RESTRICT const memo,
    const char * RESTRICT const funcname,
    const void * const parameter
#ifdef MEMOIZE_STATS
    ,ticks * const timer
#endif // MEMOIZE_STATS
    );
void memoize_status(const struct memoize_hash_t * const memo);

#ifdef MEMOIZE_GETTICKS
ticks getticks(void);
#endif
size_t Pure_function memoize_sizeof(struct memoize_hash_t * const memo);

#endif // MEMOIZE

#endif // MEMOIZE_PROTOTYPES_H
