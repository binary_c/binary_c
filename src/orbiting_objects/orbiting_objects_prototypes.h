#ifndef ORBITING_OBJECTS_PROTOTYPES_H
#define ORBITING_OBJECTS_PROTOTYPES_H

#ifdef ORBITING_OBJECTS

struct orbiting_object_t * new_orbiting_object(struct stardata_t * const stardata);
void evaporate_orbiting_object(struct stardata_t * const stardata,
                               size_t i);

void add_zero_age_orbiting_object_from_arg(
    struct stardata_t * Restrict const stardata,
    char * const Restrict argstring);

struct orbiting_object_t * add_orbiting_object_from_arg(
    struct stardata_t * Restrict const stardata,
    char * Restrict argstring);

void orbiting_objects_derivatives(
    struct stardata_t * Restrict const stardata
    );
void apply_orbiting_object_derivatives(
    struct stardata_t * Restrict const stardata,
    const double dt
    );
void initialize_orbiting_objects(struct stardata_t * Restrict const stardata);
double minimum_stable_orbit_separation(struct stardata_t * const Maybe_unused Restrict stardata,
                                       struct orbiting_object_t * const o,
                                       const double central_object_mass,
                                       const double separation);

double orbiting_object_temperature(struct stardata_t * const stardata,
                                   struct orbiting_object_t * const o);

void update_orbiting_objects(struct stardata_t * Restrict const stardata);

void unbind_orbiting_object(struct orbiting_object_t * const Restrict o);

const char * orbiting_object_central_object_string(
    struct stardata_t * Restrict const stardata,
    struct orbiting_object_t * Restrict const o
    );

const char * orbiting_object_central_object_short_string(
    struct stardata_t * Restrict const stardata,
    struct orbiting_object_t * Restrict const o
    );

double orbiting_object_distance(struct orbiting_object_t * const o,
                                const double theta);
double orbiting_object_distance_cgs(struct orbiting_object_t * const o,
                                    const double theta);
double orbiting_system_velocity(struct stardata_t * const stardata,
                                struct orbiting_object_t * const o,
                                const double theta);
double orbiting_system_velocity_cgs(struct stardata_t * const stardata,
                                    struct orbiting_object_t * const o,
                                    const double theta);
double orbiting_system_energy(struct stardata_t * const stardata,
                              struct orbiting_object_t * const o,
                              const double theta);
double orbiting_system_energy_cgs(struct stardata_t * const stardata,
                                  struct orbiting_object_t * const o,
                                  const double theta);
double orbiting_system_kinetic_energy(struct stardata_t * const stardata,
                                      struct orbiting_object_t * const o,
                                      const double theta);
double orbiting_system_kinetic_energy_cgs(struct stardata_t * const stardata,
                                          struct orbiting_object_t * const o,
                                          const double theta);
double orbiting_system_potential_energy(struct stardata_t * const stardata,
                                        struct orbiting_object_t * const o,
                                        const double theta);
double orbiting_system_potential_energy_cgs(struct stardata_t * const stardata,
                                            struct orbiting_object_t * const o,
                                            const double theta);

double orbiting_system_angular_momentum_cgs(struct stardata_t * const stardata,
                                            struct orbiting_object_t * const o);
double orbiting_system_angular_momentum(struct stardata_t * const stardata,
                                        struct orbiting_object_t * const o);

struct orbiting_object_t * locate_orbiting_object(
    struct stardata_t * Restrict const stardata,
    const char * const name,
    void * central_object,
    struct orbiting_object_t * const object_array
    );


double orbiting_object_tidal_torque(struct stardata_t * const stardata,
                                    struct orbiting_object_t * const o);

struct orbiting_object_t * copy_orbiting_objects(struct orbiting_object_t * const objects,
                                                 const size_t n);

void free_orbiting_objects(struct orbiting_object_t ** objects,
                           const size_t n);

#endif // ORBITING_OBJECTS

#endif // ORBITING_OBJECTS_PROTOTYPES_H
