#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef ORBITING_OBJECTS

double orbiting_system_velocity(struct stardata_t * const stardata,
                                struct orbiting_object_t * const o,
                                const double theta)
{
    /*
     * Calculate the orbital velocity (really its speed)
     *  as a function of angle theta, in code units.
     *
     * To convert to cgs, multiply by
     * sqrt(GRAVITATIONAL_CONSTANT*M_SUN/R_SUN)
     */
    const double M1 = o->mass;
    const double M2 = Central_object_property(o,mass);
    const double r = orbiting_object_distance(o,theta);
    const double a = o->orbit.separation;
    return
        sqrt((M1 + M2) * (2.0/r - 1.0/a));
}
#endif // ORBITING_OBJECTS
