#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef ORBITING_OBJECTS

/*
 * Initialize orbiting objects
 *
 * We should have one of the period, separation or angular momentum
 * and from those we set the other two.
 */

void initialize_orbiting_objects(struct stardata_t * Restrict const stardata)
{
    /*
     * Convert zero_age objects to actual objects
     */
    for(size_t i=0; i<stardata->preferences->zero_age.n_orbiting_objects; i++)
    {
        add_orbiting_object_from_arg(stardata,
                                     stardata->preferences->zero_age.orbiting_object[i]);
    }

    /*
     * Now initialize the objects
     */
    Foreach_orbiting_object(o)
    {
        /*
         * Check values
         */
        if(o->central_object == NULL)
        {
            Exit_binary_c(BINARY_C_SETUP_UNKNOWN_ARGUMENT,
                          "Orbiting object has no central object, please set one with orbits=<star[1,2,...],binary>.");
        }
        if(Is_zero(o->mass))
        {
            Exit_binary_c(BINARY_C_SETUP_UNKNOWN_ARGUMENT,
                          "Orbiting object has zero mass: you must give it a mass with M=<whatever>.");
        }

        /*
         * Calculate central mass
         */
        const double M_c_cgs =
            M_SUN * Central_object_property(o,mass);

        /*
         * First, convert the orbit to cgs
         */
        o->mass *= M_SUN;
        o->orbit.separation *= R_SUN;
        o->orbit.period *= YEAR_LENGTH_IN_SECONDS;
        o->orbit.angular_momentum = Angular_momentum_code_to_cgs(o->orbit.angular_momentum);

        if(!Is_zero(o->orbit.period) && Is_zero(o->orbit.angular_frequency))
        {
            /*
             * Have angular frequency : set period
             */
            o->orbit.angular_frequency = 2.0 * PI / o->orbit.period;
        }


        if(!Is_zero(o->orbit.angular_momentum))
        {
            /*
             * we have J, want sep and period
             *
             * J^2 = G * M^2 * M_c * sep * (1 - e^2)
             *
             * sep = J^2 / (G * M^2 * M_c * (1 - e^2))
             *
             * per = 2pi (sep)**1.5 / (G * M_c)**2
             */
            o->orbit.separation =
                Pow2(o->orbit.angular_momentum / o->mass) /
                (GRAVITATIONAL_CONSTANT * M_c_cgs * (1.0 - Pow2(o->orbit.eccentricity)));

            o->orbit.period =
                2.0 * PI * Pow1p5(o->orbit.separation) /
                sqrt(GRAVITATIONAL_CONSTANT * M_c_cgs);
        }
        else if(!Is_zero(o->orbit.period))
        {
            /*
             * we have the period, want J and sep
             *
             * J^2 = G * M^2 * M_c * sep * (1 - e^2)
             *
             * sep = J^2 / (G * M^2 * M_c * (1 - e^2))
             *
             * per = 2pi (sep)**1.5 / (G * M_c)**2
             */
            o->orbit.separation =
                pow(sqrt(GRAVITATIONAL_CONSTANT * M_c_cgs) *
                    o->orbit.period / (2.0 * PI),2.0/3.0);

            o->orbit.angular_momentum =
                o->mass *
                sqrt(o->orbit.separation * (GRAVITATIONAL_CONSTANT * M_c_cgs * (1.0 - Pow2(o->orbit.eccentricity))));
        }
        else if(!Is_zero(o->orbit.separation))
        {
            /*
             * we have the separation, want J and period
             *
             * J^2 = G * M_c * M^2 * sep * (1 - e^2)
             *
             * sep = J^2 / (G * M^2 * M_c * (1 - e^2))
             *
             * per = 2pi (sep)**1.5 / (G * M_c)**2
             */

            /* calculate period and angular momentum */
            o->orbit.period =
                2.0 * PI * Pow1p5(o->orbit.separation) /
                sqrt(GRAVITATIONAL_CONSTANT * M_c_cgs);

            o->orbit.angular_momentum =
                o->mass *
                sqrt(o->orbit.separation * (GRAVITATIONAL_CONSTANT * M_c_cgs * (1.0 - Pow2(o->orbit.eccentricity))));
        }
        else
        {
            Exit_binary_c(BINARY_C_SETUP_FAILED_TO_LOAD_PARAMETERS,
                          "You must set orbiting object index=%zu's orbital angular momentum (Jorb=...), orbital period (per=...) or separation (sep=....).",o->index);
        }

        /*
         * convert to code units
         */
        o->mass /= M_SUN;
        o->orbit.separation /= R_SUN;
        o->orbit.period /= YEAR_LENGTH_IN_SECONDS;
        o->orbit.angular_momentum = Angular_momentum_cgs_to_code(o->orbit.angular_momentum);

        /*
         * Hence angular frequency
         */
        o->orbit.angular_frequency = 2.0 * PI / o->orbit.period;

        /*
         * Recalculate angular momentum to include
         * central object: this is required if we're to later calculate
         * whether the object is bound. In the case that
         * o->mass << central object mass, this doesn't matter.
         */
        o->orbit.angular_momentum = orbiting_system_angular_momentum(stardata,o);
    }
}

#endif // ORBITING_OBJECTS
