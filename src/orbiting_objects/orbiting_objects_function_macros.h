#pragma once
#ifndef ORBITING_OBJECTS_FUNCTION_MACROS_H
#define ORBITING_OBJECTS_FUNCTION_MACROS_H

/*
 * Function macros for orbiting objects
 */

#define Orbiting_object_is_bound(O)             \
    ((O)->type != ORBITING_OBJECT_TYPE_UNBOUND)


/*
 * Macro to acquire a property that can be
 * just added, like mass or luminosity
 */
#define Central_object_property(O,X)                        \
    (                                                       \
        (O)->type == ORBITING_OBJECT_TYPE_CIRCUMSTELLAR ?   \
        (((struct star_t const *)(O)->central_object)->X) : \
        (O)->type == ORBITING_OBJECT_TYPE_CIRCUMBINARY ?    \
        (Sum_of_stars(X)) :                                 \
        -1.0 /* error! */                                    \
        )

/*
 * Central object size
 */
#define Central_object_size(O)                              \
    (                                                       \
        o->type == ORBITING_OBJECT_TYPE_CIRCUMSTELLAR ?     \
        ((struct star_t const*)o->central_object)->radius : \
        o->type == ORBITING_OBJECT_TYPE_CIRCUMBINARY ?      \
        stardata->common.orbit.separation :                 \
        -1.0 /* error */                                    \
        )


/* wrapper for Concat : expands A,B */
#ifndef ConcatC
#define ConcatC(A,B) Concat(A,B)
#endif

#define Foreach_orbiting_object(...)            \
    ConcatC(                                    \
        Foreach_orbiting_object_implementation, \
        NARGS(__VA_ARGS__)                      \
        )(                                      \
            Foreachorbitingobject,              \
            __COUNTER__,                        \
            __VA_ARGS__                         \
            )

#define Foreach_orbiting_object_implementation1(LABEL,                  \
                                                LINE,                   \
                                                OBJECT)                 \
    size_t Concat3(i,LABEL,LINE) = 0;                                   \
    for(struct orbiting_object_t * OBJECT;                              \
        ((Concat3(i,LABEL,LINE) < stardata->common.n_orbiting_objects)  \
         && ((OBJECT) = stardata->common.orbiting_object +              \
             Concat3(i,LABEL,LINE)));                                   \
        Concat3(i,LABEL,LINE)++)

#define Foreach_orbiting_object_implementation2(LABEL,                  \
                                                LINE,                   \
                                                OBJECT,                 \
                                                N)                      \
    for(size_t N = 0,                                                   \
            Concat3(i,LABEL,LINE) = 1;                                  \
        Concat3(i,LABEL,LINE);                                          \
        Concat3(i,LABEL,LINE) = 0)                                      \
        for(struct orbiting_object_t * OBJECT;                          \
            (((N) < stardata->common.n_orbiting_objects)                \
             && ((OBJECT) = stardata->common.orbiting_object + (N)));   \
            (N)++)

#define Foreach_bound_orbiting_object(...)              \
    ConcatC(                                            \
        Foreach_bound_orbiting_object_implementation,   \
        NARGS(__VA_ARGS__)                              \
        )(                                              \
            Foreachboundorbitingobject,                 \
            __COUNTER__,                                \
            __VA_ARGS__                                 \
            )

#define Foreach_bound_orbiting_object_implementation1(LABEL,            \
                                                      LINE,             \
                                                      OBJECT)           \
    Foreach_orbiting_object_implementation1(LABEL,LINE,OBJECT)          \
        if(Orbiting_object_is_bound(OBJECT))

#define Foreach_bound_orbiting_object_implementation2(LABEL,            \
                                                      LINE,             \
                                                      OBJECT,           \
                                                      N)                \
    Foreach_orbiting_object_implementation2(LABEL,LINE,OBJECT,N)        \
        if(Orbiting_object_is_bound(OBJECT))



#define Foreach_unbound_orbiting_object(...)            \
    ConcatC(                                            \
        Foreach_unbound_orbiting_object_implementation, \
        NARGS(__VA_ARGS__)                              \
        )(                                              \
            Foreachunboundorbitingobject,               \
            __COUNTER__,                                \
            __VA_ARGS__                                 \
            )

#define Foreach_unbound_orbiting_object_implementation1(LABEL,  \
                                                        LINE,   \
                                                        OBJECT) \
    Foreach_orbiting_object_implementation1(LABEL,LINE,OBJECT)  \
        if(!Orbiting_object_is_bound(OBJECT))

#define Foreach_unbound_orbiting_object_implementation2(LABEL,      \
                                                        LINE,       \
                                                        OBJECT,     \
                                                        N)          \
    Foreach_orbiting_object_implementation2(LABEL,LINE,OBJECT,N)    \
        if(!Orbiting_object_is_bound(OBJECT))

#endif // ORBITING_OBJECTS_FUNCTION_MACROS_H
