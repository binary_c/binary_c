#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef ORBITING_OBJECTS

double orbiting_system_velocity_cgs(struct stardata_t * const stardata,
                                    struct orbiting_object_t * const o,
                                    const double theta)
{
    /*
     * Wrapper for orbiting_system_velocity to return
     * the result in cgs
     */
    return
        sqrt(GRAVITATIONAL_CONSTANT*M_SUN/R_SUN) *
        orbiting_system_velocity(stardata,o,theta);
}
#endif // ORBITING_OBJECTS
