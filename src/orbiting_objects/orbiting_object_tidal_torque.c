#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef ORBITING_OBJECTS

double orbiting_object_tidal_torque(struct stardata_t * const stardata,
                                    struct orbiting_object_t * const o)
{
    /*
     * Zahn-like torque on an orbiting object
     */
    double torque_cgs = 0.0;
    Foreach_star(star)
    {
        if(o->central_object != NULL &&
           (o->type == ORBITING_OBJECT_TYPE_CIRCUMBINARY ||
            (o->type == ORBITING_OBJECT_TYPE_CIRCUMSTELLAR && star == o->central_object)))
        {
            double
                domega_dt = 0.0,
                dJ_dt_object = 0.0,
                dJ_dt_orbit = 0.0,
                de_dt_star = 0.0,
                de_dt_orbit = 0.0;
            tides_generic(stardata,
                          star,
                          &o->orbit,
                          o->mass,
                          star->effective_radius,
                          stardata->preferences->orbiting_objects_tides_multiplier,
                          FALSE,
                          &domega_dt,
                          &dJ_dt_object,
                          &dJ_dt_orbit,
                          &de_dt_star,
                          &de_dt_orbit,
                          FALSE);

            /*
             * The angular momentum is converted from stellar spin
             * to the object's orbital angular momentum
             */
            o->derivative[DERIVATIVE_ORBITING_OBJECT_ORBITAL_ANGULAR_MOMENTUM] +=
                dJ_dt_orbit;
            star->derivative[DERIVATIVE_STELLAR_ANGMOM_TIDES] -= dJ_dt_orbit;
        }
    }

    return torque_cgs;
}

#endif//ORBITING_OBJECTS
