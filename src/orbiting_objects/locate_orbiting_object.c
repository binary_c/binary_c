#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef ORBITING_OBJECTS

struct orbiting_object_t * locate_orbiting_object(
    struct stardata_t * Restrict const stardata,
    const char * const name,
    void * central_object,
    struct orbiting_object_t * const object_array
    )
{
    /*
     * Locate an orbiting object by name and perhaps also
     * central_object.
     *
     * name is required (char * string).
     *
     * central_object (void * pointer) is checked to be matching
     * if it is non-NULL.
     *
     * object_array is an array of objects that is
     * used instead of that in stardata->common. If NULL
     * the stardata->common.orbiting_object array is used.
     *
     * Returns a pointer to the matched object, or NULL
     * if none match.
     */
    struct orbiting_object_t * const stardata_array_was =
        stardata->common.orbiting_object;

    if(object_array != NULL)
    {
        /*
         * Temporarily override stardata->common.orbiting_object
         * It's set back to stardata_array_was on return.
         */
        stardata->common.orbiting_object = object_array;
    }

    /*
     * Loop to find a match
     */
    Foreach_orbiting_object(o,i)
    {
        if(Strings_equal(o->name,name) &&
           (central_object == NULL ||
            o->central_object == central_object))
        {
            stardata->common.orbiting_object = stardata_array_was;
            return o;
        }
    }

    stardata->common.orbiting_object = stardata_array_was;
    return NULL;
}
#endif // ORBITING_OBJECTS
