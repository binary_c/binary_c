#ifndef MAIN_PROTOTYPES_H
#define MAIN_PROTOTYPES_H
#include "./misc/misc.h"
/*
 * The binary_c stellar population nucleosynthesis framework.
 *
 * Copyright Robert Izzard 2000-2025
 *
 * Contact rob.izzard@gmail.com
 *
 * Code repository:
 * https://gitlab.com/binary_c
 *
 * Documentation is in doc/
 * Code is in src/
 *
 * To build, run
 *
 * ---
 *
 * meson setup builddir
 * cd builddir
 * ninja binary_c_install
 *
 * ---
 *
 * Please see the files README, LICENCE and CHANGES,
 * and the doc/ directory for documentation which is mirrored
 * at https://binary_c.gitlab.io
 *
 **********************
 *
 * Purpose:
 *
 *  prototypes used by binary_c's main function only.
 * Note: this is likely to be deprecated at some point.
 *
 **********************
 */
void fool_cpu(void);


#endif // MAIN_PROTOTYPES_H
