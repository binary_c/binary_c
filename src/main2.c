#include "binary_c.h"
No_empty_translation_unit_warning;

/*
 * The binary_c stellar population nucleosynthesis framework.
 *
 * Copyright Robert Izzard 2000-2025
 *
 * Contact rob.izzard@gmail.com
 *
 * Code repository:
 * https://gitlab.com/binary_c
 *
 * Documentation is in doc/
 * Code is in src/
 *
 * To build, run
 *
 * ---
 *
 * meson setup builddir
 * cd builddir
 * ninja binary_c_install
 *
 * ---
 *
 * Please see the files README, LICENCE and CHANGES,
 * and the doc/ directory for documentation which is mirrored
 * at https://binary_c.gitlab.io
 *
 **********************
 *
 * Subroutine: fool_cpu
 *
 * Purpose: To trick the Intel C compiler into thinking an
 *          AMD system is actually Intel, and hence use the compiler's
 *          many speedups.
 *
 * Arguments: none
 *
 * Returns: nothing
 *
 **********************
 */

void fool_cpu(void)
{
#if (defined __INTEL_COMPILER && FOOL_INTEL_COMPILER)
    /* Intel's compiler is crippled on AMD64, so un-cripple it */
    extern  int __intel_cpu_indicator;
    extern  void __intel_cpu_indicator_init(void)  ;

    //__intel_cpu_indicator_init(); // not required
    Dprint("INTEL says cpu = %d\n",__intel_cpu_indicator);

    /* fool intel's compiler into thinking we have sse(2) even
       thought we really do have sse(2)! */

    /*
     * 0x0001=1 MMX and CMOV not supported
     * 0x0010=16 MMX and CMOV supported
     * 0x0080=128 SSE supported by CPU and operating
     * 0x0200=512 SSE2 (family = * 0x0F)
     * 0x0400=1024 SSE2 (family != * 0x0F or not Intel)
     * 0x0800=2048 SSE3
     * 0x1000=4096 SSE3B (supplementary SSE3)
     */
    //__intel_cpu_indicator = 1024;
    __intel_cpu_indicator = FAKE_INTEL_CPU_INDICATOR;

#endif // FOOL_INTEL_COMPILER && __INTEL_COMPILER
}
