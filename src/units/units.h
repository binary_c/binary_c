#pragma once
#ifndef UNITS_H
#define UNITS_H

/*
 * Binary_c units macros
 *
 * Note: physical constants are defined in binary_c_physical_constants.h
 */
#include "units_SI.h"
#include "units.def"

/*
 * Factors for converting from cgs to code units:
 * divide by these to convert
 */
#define MASS_CODE_UNIT (M_SUN)
#define LENGTH_CODE_UNIT (R_SUN)
#define LUMINOSITY_CODE_UNIT (L_SUN)
#define ANGULAR_MOMENTUM_CODE_UNIT (M_SUN*R_SUN*R_SUN/YEAR_LENGTH_IN_SECONDS)
#define TIME_CODE_UNIT (YEAR_LENGTH_IN_SECONDS * 1e6)
#define VELOCITY_CODE_UNIT (1e5)
#define FREQUENCY_CODE_UNIT (1.0)
#define ACCELERATION_CODE_UNIT (1.0)
#define ENERGY_CODE_UNIT (1.0)
#define NONE_CODE_UNIT (1.0)
#define G_CODE_UNIT (GRAVITATIONAL_CONSTANT * M_SUN / Pow3(R_SUN) * Pow2(YEAR_LENGTH_IN_SECONDS))

#undef X
#define X(TYPE) CODE_UNIT_TYPE_##TYPE,
enum { CODE_UNIT_TYPE_LIST };
#undef X


#endif // UNITS_H
