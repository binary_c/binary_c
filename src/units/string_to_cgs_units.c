#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "units.def"

#ifdef __DEPRECATED
static double get_cgs_unit1(struct stardata_t * stardata,
                            char * const Restrict s,
                            struct unit_t * const u);
#endif // __DEPRECATED
static double get_cgs_unit2(struct stardata_t * stardata,
                            char * const Restrict s,
                            struct unit_t * const u);
static void init_unit_alphas(struct stardata_t * const stardata);

void string_to_cgs_units(struct stardata_t * stardata Maybe_unused,
                         char * const Restrict s,
                         double * const value,
                         struct unit_t * const Restrict u,
                         int * const Restrict errnum)
{
    /*
     * Given a string, convert to a value in cgs units
     */
    char * endptr = NULL;

    /*
     * First convert to a double-precision number
     */
    *value = fast_strtod(s,&endptr);
    *errnum = errno;

    /*
     * If endptr == NULL, we have no numerical match,
     * so default the value to 1.0 and start scanning
     * for a unit anyway.
     */
    if(endptr == NULL)
    {
        endptr = s;
        *value = 1.0;
    }

    /*
     * If no error, convert to cgs
     */
    if(endptr != NULL && *errnum == 0)
    {
        *value *= get_cgs_unit2(stardata,
                                endptr,
                                u);
    }
    else
    {
        Exit_binary_c(BINARY_C_SETUP_UNKNOWN_ARGUMENT,
                      "Expected string \"%s\" to contain a floating point number, but there was an error on conversion. Please check it.",
                      s);
    }
}

static double get_cgs_unit2(struct stardata_t * const Restrict stardata Maybe_unused,
                            char * const Restrict s,
                            struct unit_t * const u)
{
    /*
     * Given string s, return the unit in cgs
     * and set the unit string (e.g. "Msun")
     * in unitstring
     *
     * This version of the function sets up a
     * cache array in tmpstore which is labelled by the
     * initial character in the string. This tree
     * structure speeds up parsing if the number of
     * units is large (>~30).
     */

    /*
     * Initialize units data
     */
    if(stardata->tmpstore->unit_alphas == NULL)
    {
        init_unit_alphas(stardata);
    }

    /*
     * Search for a matching unit
     */
    const char sstart = toupper(s[0]);
    if(sstart != '\0')
    {
        for(size_t i=0; i<stardata->tmpstore->n_unit_alphas;i++)
        {
            if(sstart == stardata->tmpstore->unit_alphas[i].alpha)
            {
                const size_t n = stardata->tmpstore->unit_alphas[i].nunits;
                if(n == 1)
                {
                    /* no loop required */
                    const struct unit_t * const _u =
                        &stardata->tmpstore->unit_alphas[i].unit[0];
                    if(Strings_equal_case_insensitive(s,_u->string) ||
                       Strings_equal_case_insensitive(s,_u->longstring))
                    {
                        memcpy(u,_u,sizeof(struct unit_t));
                        return u->unit_value;
                    }
                }
                else
                {
                    /* loop over (hopefully few) options */
                    for(size_t j=0; j<n; j++)
                    {
                        const struct unit_t * const _u =
                            &stardata->tmpstore->unit_alphas[i].unit[j];
                        if(Strings_equal_case_insensitive(s,_u->string) ||
                           Strings_equal_case_insensitive(s,_u->longstring))
                        {
                            memcpy(u,_u,sizeof(struct unit_t));
                            return u->unit_value;
                        }
                    }
                }

                /* no match */
                Exit_binary_c(BINARY_C_SETUP_UNKNOWN_ARGUMENT,
                              "\"%s\" failed to match any known unit, please check it.\n",
                              s);
            }
        }
    }

    /*
     * No unit matched, just a number.
     */
    u->unit_value = 1.0;
    u->string = NULL;
    u->longstring = NULL;
    u->code_unit_value = 1.0;
    u->code_unit_type = CODE_UNIT_TYPE_NONE;
    return 1.0;
}

static void init_unit_alphas(struct stardata_t * const stardata)
{
    size_t n_units Maybe_unused = 0;
    size_t n_startchars = 0;
    size_t n_alloc_startchars = 16;
    char * startchars = Malloc(sizeof(char)*n_alloc_startchars);
#undef X
#define X(LONGSTRING,STRING,CGS_UNIT,CODE_UNIT,CODE_UNIT_TYPE)  \
    n_units++;                                                  \
    n_startchars++;                                             \
    if(n_startchars >= n_alloc_startchars)                      \
    {                                                           \
        n_alloc_startchars *= 2;                                \
        startchars = Realloc(startchars,                        \
                             sizeof(char)*n_alloc_startchars);  \
    }                                                           \
    startchars[n_startchars] = (STRING)[0];

    do
    {
        ALL_UNITS_LIST;
    }while(0);

    /*
     * Make unit_alpha structures in tmpstore
     */
    stardata->tmpstore->n_unit_alphas = 0;
#undef X
#define X(LONGSTRING,STRING,CGS_UNIT,CODE_UNIT,CODE_UNIT_TYPE)          \
    {                                                                   \
        const char c1 = toupper((STRING)[0]);                           \
        for(size_t i=0; i<n_startchars;i++)                             \
        {                                                               \
            Boolean match = FALSE;                                      \
            for(size_t j=0; j<stardata->tmpstore->n_unit_alphas;j++)    \
            {                                                           \
                if(c1 == stardata->tmpstore->unit_alphas[j].alpha)      \
                { /* already matched */                                 \
                    match = TRUE;                                       \
                    j = stardata->tmpstore->n_unit_alphas+1;            \
                }                                                       \
            }                                                           \
            if(match == FALSE)                                          \
            {                                                           \
                /* make new unit_alpha */                               \
                const size_t _n = stardata->tmpstore->n_unit_alphas;    \
                stardata->tmpstore->n_unit_alphas++;                    \
                stardata->tmpstore->unit_alphas =                       \
                    Realloc(stardata->tmpstore->unit_alphas,            \
                            sizeof(struct unit_alpha_t) *               \
                            stardata->tmpstore->n_unit_alphas);         \
                stardata->tmpstore->unit_alphas[_n].alpha = c1;         \
                stardata->tmpstore->unit_alphas[_n].nunits = 0;         \
                stardata->tmpstore->unit_alphas[_n].unit = NULL;        \
            }                                                           \
        }                                                               \
    }

    do
    {
        ALL_UNITS_LIST;
    }while(0);

    /*
     * Populate units data in tmpstore
     */
#undef X
#define X(LONGSTRING,STRING,CGS_UNIT,CODE_UNIT,CODE_UNIT_TYPE)          \
    {                                                                   \
        const char c1 = toupper((STRING)[0]);                           \
        for(size_t j=0; j<stardata->tmpstore->n_unit_alphas;j++)        \
        {                                                               \
            if(c1 == stardata->tmpstore->unit_alphas[j].alpha)          \
            {                                                           \
                const size_t _n = stardata->tmpstore->unit_alphas[j].nunits; \
                stardata->tmpstore->unit_alphas[j].nunits++;            \
                stardata->tmpstore->unit_alphas[j].unit =               \
                    Realloc(stardata->tmpstore->unit_alphas[j].unit,    \
                            sizeof(struct unit_t) *                     \
                            stardata->tmpstore->unit_alphas[j].nunits); \
                struct unit_t * const _u =                              \
                    &stardata->tmpstore->unit_alphas[j].unit[_n];       \
                _u->unit_value = CGS_UNIT;                              \
                _u->string = STRING;                                    \
                _u->longstring = LONGSTRING;                            \
                _u->code_unit_value = CODE_UNIT;                        \
                _u->code_unit_type = CODE_UNIT_TYPE;                    \
            }                                                           \
        }                                                               \
    }

    do
    {
    ALL_UNITS_LIST;
    }while(0);

    Safe_free(startchars);
}




#ifdef __DEPRECATED
static double get_cgs_unit1(struct stardata_t * const Restrict stardata Maybe_unused,
                            char * const Restrict s,
                            struct unit_t * const u)
{
    /*
     * Given string s, fill the unit struct u
     * (allocated elsewhere) appropriately
     *
     * This version of the function is a simple
     * linear search, which is fast if the number
     * of units is small.
     */
    const size_t n = strlen(s);

    u->unit_value = 1.0;
    u->string = NULL;
    u->longstring = NULL;
    u->code_unit_value = 1.0;
    u->code_unit_type = CODE_UNIT_TYPE_NONE;

    if(n > 0)
    {
        /*
         * Convert to upper case
         */
        if(s != NULL)
        {
#undef X
#define X(LONGSTRING,STRING,CGS_UNIT,CODE_UNIT,CODE_UNIT_TYPE)  \
            if(Strings_equal_case_insensitive(s,STRING)||       \
               Strings_equal_case_insensitive(s,LONGSTRING))    \
            {                                                   \
                u->unit_value = CGS_UNIT;                       \
                u->longstring = LONGSTRING;                     \
                u->string = STRING;                             \
                u->code_unit_value = CODE_UNIT;                 \
                u->code_unit_type = CODE_UNIT_TYPE;             \
                break;                                          \
            }                                                   \

            do
            {
                ALL_UNITS_LIST;
            }while(0);
#undef X
        }
    }

    return u->unit_value;
}
#endif // DEPRECATED
