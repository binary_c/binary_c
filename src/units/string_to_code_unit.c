#include "../binary_c.h"
No_empty_translation_unit_warning;

void string_to_code_units(struct stardata_t * const Restrict stardata,
                          char * const Restrict s,
                          double * const Restrict value,
                          struct unit_t * u,
                          int * const Restrict errnum)
{
    /*
     * Given a string, convert to a value in code units
     *
     * First, conver to cgs.
     */
    string_to_cgs_units(stardata,
                        s,
                        value,
                        u,
                        errnum);

    if(*errnum == 0)
    {
        /*
         * Then, if no error, convert to code units.
         */
        *value /= u->code_unit_value;
    }

    return;
}
