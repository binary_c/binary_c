#pragma once
#ifndef INTERFACE_PROTOTYPES_H
#define INTERFACE_PROTOTYPES_H
int interface_stellar_structure(struct stardata_t * const stardata,
                                const Caller_id caller_id,
                                struct star_t * const oldstar,
                                struct star_t * newstar,
                                ...);
int vinterface_stellar_structure(struct stardata_t * const stardata,
                                 const Caller_id caller_id,
                                 struct star_t * const oldstar,
                                 struct star_t * newstar,
                                 va_list v);

int interface_free_memory(struct stardata_t * const stardata Maybe_unused);

int interface_shutdown(struct stardata_t * const stardata Maybe_unused);

int interface_initialize(struct stardata_t * const stardata Maybe_unused);



#endif // INTERFACE_PROTOTYPES_H
