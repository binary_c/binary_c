#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "../stellar_structure/stellar_structure_debug.h"

/*
 * Interface function to connect binary_c to the stellar-evolution
 * library of choice.
 *
 * interface_stellar_structure wraps vinterface_stellar_structure
 * and passes any va_args to it.
 *
 * vinterface_stellar_structure calls the appropriate algorithm.
 */
static int _perform_structure_calculation(struct stardata_t * const stardata,
                                          const Caller_id caller_id,
                                          struct star_t * oldstar,
                                          struct star_t * newstar,
                                          va_list v,
                                          const Boolean newstar_null);

static int _wrapper(struct stardata_t * const stardata,
                    const Caller_id caller_id,
                    struct star_t * oldstar,
                    struct star_t * newstar,
                    va_list v,
                    const Boolean newstar_null);

int interface_stellar_structure(struct stardata_t * const stardata,
                                const Caller_id caller_id,
                                struct star_t * oldstar,
                                struct star_t * newstar,
                                ...)
{
    va_list v1,v2;
    va_start(v1,newstar);
    va_copy(v2,v1);
    const int ret = vinterface_stellar_structure(stardata,
                                           caller_id,
                                           oldstar,
                                           newstar,
                                           v2);
    va_end(v1);
    va_end(v2);
    return ret;
}

int vinterface_stellar_structure(struct stardata_t * const stardata,
                                 const Caller_id caller_id,
                                 struct star_t * oldstar,
                                 struct star_t * newstar,
                                 va_list v)
{
    int ret = -1; // return value

    /*
     * If newstar is NULL, copy reslts directly into oldstar
     * and free newstar on exit.
     */
    Boolean newstar_null;
    Dprint("oldstar bse = %p %p, parent=%p ancestor=%p\n",
           (void*)oldstar->bse,
           (void*)(oldstar->bse ? oldstar->bse->timescales : NULL),
           (void*)oldstar->parent,
           (void*)oldstar->ancestor);
    if(newstar == NULL)
    {
        newstar = New_star;
        newstar_null = TRUE;
        Dprint("using provided oldstar %p (bse %p %p), allocated new star %p (bse %p %p) for stellar structure calculation\n",
               (void*)oldstar,
               (void*)oldstar->bse,
               (void*)(oldstar->bse?oldstar->bse->timescales:NULL),
               (void*)newstar,
               (void*)newstar->bse,
               (void*)(newstar->bse?newstar->bse->timescales:NULL));
    }
    else
    {
        newstar_null = FALSE;
        Dprint("using provided stars old=%p new=%p for stellar structure calculation\n",
               (void*)oldstar,
               (void*)newstar);
    }
    Dprint("newstar bse = %p %p \n",
           (void*)newstar->bse,
           (void*)(newstar->bse ? newstar->bse->timescales : NULL));

    /*
     * Call the appropriate stellar structure algorithm
     */
    copy_star(stardata,
              oldstar,
              newstar);
    Dprint("copied star from oldstar (Mgrav %g Mbary %g) to newstar (Mgrav %g Mbary %g)\n",
           oldstar->mass,
           oldstar->baryonic_mass,
           newstar->mass,
           newstar->baryonic_mass);

    /*
     * Do structure calculation
     */
    _wrapper(stardata,
             caller_id,
             oldstar,
             newstar,
             v,
             newstar_null);

    /*
     * Copy newstar to oldstar if we weren't given a separate
     * newstar structure
     */
    if(newstar_null == TRUE)
    {
        copy_star(stardata,
                  newstar,
                  oldstar);
        free_star(&newstar);
    }

    return ret;
}

static int _wrapper(struct stardata_t * const stardata,
                    const Caller_id caller_id,
                    struct star_t * oldstar,
                    struct star_t * newstar,
                    va_list v,
                    const Boolean newstar_null)
{
    int ret = -1;
    if(oldstar->type == STAR_NORMAL)
    {
        /*
         * Normal star. Just evolve it.
         */
        ret = _perform_structure_calculation(stardata,
                                             caller_id,
                                             oldstar,
                                             newstar,
                                             v,
                                             newstar_null);
    }
    else
    {
        /*
         * Proxy star: evolve the inner binary
         */
        Foreach_star_in(newstar->stardata,
                        embedded_star)
        {
            const int r = vinterface_stellar_structure(newstar->stardata,
                                                       caller_id,
                                                       embedded_star,
                                                       NULL,
                                                       v);
            if(ret==-1 || r!=0)
            {
                ret = r;
            }
        }

        /*
         * And convert the embedded stardata to oldstar
         */
        stardata_to_star(newstar);
    }
    return ret;
}

static int _perform_structure_calculation(struct stardata_t * const stardata,
                                          const Caller_id caller_id,
                                          struct star_t * oldstar,
                                          struct star_t * newstar,
                                          va_list v,
                                          const Boolean newstar_null)
{
    /*
     * Perform stellar structure calculation for one (real) star
     */
    int ret = -1;
    switch(stardata->preferences->stellar_structure_algorithm)
    {

    case STELLAR_STRUCTURE_ALGORITHM_NONE:
        /* do nothing */
        break;

    case STELLAR_STRUCTURE_ALGORITHM_EXTERNAL_FUNCTION:
        /*
         * Use an externally defined function
         */
        if(stardata->preferences->stellar_structure_hook == NULL)
        {
            Exit_binary_c(BINARY_C_POINTER_FAILURE,
                          "Attempt to call an external stellar structure function which has not been defined. Oops!\n");
        }
        ret = stardata->preferences->stellar_structure_hook(caller_id,
                                                            stardata,
                                                            oldstar,
                                                            newstar,
                                                            v);
        break;

    case STELLAR_STRUCTURE_ALGORITHM_MODIFIED_BSE:
        /*
         * The BSE algorithm
         */
#ifdef BSE
    {
        Dprint("CALLED by %s oldstar->bse %p, newstar->bse = %p\n",
               Stellar_structure_caller_string(caller_id),
               (void*)oldstar->bse,
               (void*)newstar->bse);

/*
 * You could call this
 ret = stellar_structure_BSE(stardata,
 caller_id,
 newstar,
 newstar->bse);
 * but the following is identical and faster (saves some memcpys).
 */
        ret = stellar_structure_BSE_with_newstar(stardata,
                                                 caller_id,
                                                 oldstar,
                                                 newstar,
                                                 newstar->bse);


        Dprint("newstar type %d, M %g McHe %g McCO %g McONe %g Mcn %g McBH %g, R %g, deny_SN %u\n",
               newstar->stellar_type,
               newstar->mass,
               newstar->core_mass[CORE_He],
               newstar->core_mass[CORE_CO],
               newstar->core_mass[CORE_ONe],
               newstar->core_mass[CORE_NEUTRON],
               newstar->core_mass[CORE_BLACK_HOLE],
               newstar->radius,
               newstar->deny_SN
            );
    }
#else
    Exit_binary_c(BINARY_C_ALGORITHM_OUT_OF_RANGE,
                  "Algorithm BSE is not built or linked in");
#endif
    break;
    case STELLAR_STRUCTURE_ALGORITHM_MINT:
#ifdef MINT
        /*
         * New MINT algorithm
         */
        ret = MINT_stellar_structure(stardata,
                                     oldstar,
                                     newstar,
                                     caller_id);
        break;
#else
        if(newstar_null == TRUE)
        {
            free_star(&newstar);
        }
        Exit_binary_c(BINARY_C_ALGORITHM_OUT_OF_RANGE,
                      "Algorithm MINT is not built or linked in");
#endif//MINT
    default:
        if(newstar_null == TRUE)
        {
            free_star(&newstar);
        }
        Exit_binary_c(BINARY_C_ALGORITHM_OUT_OF_RANGE,
                      "Unknown stellar structure algorithm %d\n",
                      stardata->preferences->stellar_structure_algorithm);
    }
    return ret;
}
