#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Do supernova kick(s)
 *
 * At present, this calls monte_carlo_kick, but in
 * future there are likely to be other methods.
 */

void supernova_kick(struct stardata_t * const stardata,
                    struct stardata_t * const pre_explosion_stardata,
                    struct star_t * const star,
                    struct star_t * const pre_explosion_star)
{
    Dprint("Supernova kick wrapper");
    if(stardata->preferences->monte_carlo_sn_kicks==TRUE)
    {
        if(stardata->preferences->custom_supernova_kick_hook != NULL)
        {
            stardata->preferences->custom_supernova_kick_hook(
                stardata,
                pre_explosion_stardata,
                star,
                pre_explosion_star);
        }
        else
        {
            monte_carlo_kick(star,
                             pre_explosion_star,
                             star->mass, // remnant mass
                             pre_explosion_star->mass,// mass before explosion
                             (Other_star_struct(star))->mass,
                             &stardata->common.orbit.eccentricity,
                             &stardata->common.orbit.separation,
                             &stardata->common.orbit.angular_momentum,
                             stardata,
                             pre_explosion_stardata);
        }
    }

}
