#include "../binary_c.h"
No_empty_translation_unit_warning;


double Pure_function supernova_luminous_energy(struct stardata_t * Restrict const stardata,
                                               const struct star_t * Restrict const star)
{
    /*
     * Estimate the luminous energy in a supernova
     * in erg
     */
    return 1e-2 * supernova_kinetic_energy(stardata,
                                           star);
}
