#include "../binary_c.h"
No_empty_translation_unit_warning;


double Pure_function supernova_ejecta_velocity(struct stardata_t * Restrict const stardata Maybe_unused,
                                               const struct star_t * Restrict const star Maybe_unused)
{
    /*
     * SN ejection velocity, km/s
     */
    double v;
    switch(star->SN_type)
    {
    case SN_IA_He:
    case SN_IA_ONeWD_DDet:
    case SN_IA_COWD_DDet:
    case SN_IA_CHAND:
    case SN_IA_He_Coal:
    case SN_IA_CHAND_Coal:
        v = 1e4;
        break;
    case SN_IBC:
    case SN_II:
    case SN_GRB_COLLAPSAR:;
        /*
         * assume 1/2 m v^2 = energy
         */
        const double M = star->SN_mass_ejected * M_SUN; /* g */
        const double Ekin = supernova_kinetic_energy(stardata,star); /* erg */
        if(M > 0.0)
        {
            v = sqrt(2.0 * Ekin / M) * 1e-5;
        }
        else
        {
            v = 0.0;
        }

        Dprint("v SN Ekin = %g, M = %g foxe -> v = %g km/s\n",Ekin,M,v);
        break;
    default:
        v = 0.0;
        break;
    };

    return v;
}
