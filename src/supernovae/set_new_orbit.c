#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "supernovae.h"

void set_new_orbit(struct stardata_t * const stardata,
                   struct stardata_t * const pre_explosion_stardata,
                   struct star_t * const pre_explosion_star,
                   struct star_t * const exploder,
                   struct star_t * const companion,
                   struct kick_system_t * const kick_system,
                   const int kick_distribution,
                   const double kick_dispersion,
                   const double kick_companion,
                   const double m1 Maybe_unused,
                   const double m1n,
                   double m2 Maybe_unused,
                   double * const jorb,
                   double * const separation,
                   double * const eccentricity
    )
{

    /*
     * Determine the magnitude of the new relative velocity.
     * Eq. A9 (note: if the kick speed is zero, this is just
     * the orbital speed squared without any change)
     */
    kick_system->new_orbital_speed_squared =
        + kick_system->kick_speed_squared
        + kick_system->orbital_speed_squared
        - 2.0 * kick_system->orbital_speed * kick_system->kick_speed *
        kick_system->cosphi *(kick_system->cosomega*kick_system->sinbeta +
                              kick_system->sinomega*kick_system->cosbeta);

    kick_system->new_orbital_speed = sqrt(kick_system->new_orbital_speed_squared);

    Dprint("New orbital speed %g cf. old %g (kick_speed = %g, sq = %g, beta = %g, sinbeta = %g, cosbeta = %g)\n",
           kick_system->new_orbital_speed,
           kick_system->orbital_speed,
           kick_system->kick_speed,
           kick_system->kick_speed_squared,
           asin(kick_system->sinbeta),
           kick_system->sinbeta,
           kick_system->cosbeta
        );
    SNprint("SN vn2=%g from vk2=%g vorb2=%g cos phi=%g omega=%g beta=%g\n",
           kick_system->new_orbital_speed_squared,
           kick_system->kick_speed_squared,
           kick_system->orbital_speed_squared,
           kick_system->cosphi,
           kick_system->cosomega,
           kick_system->cosbeta

        );

    /*
     * Determine the magnitude of the cross product of the
     * separation vector and the new relative velocity.
     * Eq. A12
     */
    const double v1sq = Pow2(kick_system->kick_speed * kick_system->sinphi);
    const double v2sq = Pow2(kick_system->kick_speed * kick_system->cosomega * kick_system->cosphi -
                             kick_system->orbital_speed * kick_system->sinbeta);
    kick_system->hn_squared = Pow2(kick_system->separation) * (v1sq + v2sq);
    kick_system->hn = sqrt(kick_system->hn_squared);

    Dprint("hn^2 = (%g)^2 * (v1sq = %g + v2sq = %g) = %g\n",
           Pow2(kick_system->separation),
           v1sq,
           v2sq,
           kick_system->hn_squared);

    SNprint("SN v1sq=%g v2sq=%g hn2=%g\n",
            v1sq,
            v2sq,
            kick_system->hn_squared);

    /*
     * Perhaps use the Tauris and Takens 1998 method for
     * calculating the post-SN orbit.
     */
    Dprint("CALL TT pre_explosion_star = %p , companion = %p\n",
           (void*)pre_explosion_star,
           (void*)companion);
    Dprint("CALL TT masses expoder = %g , companion = %g\n",
           pre_explosion_star->mass,
           companion->mass);
    Dprint("CALL TT kick speed %g\n",kick_system->kick_speed);

    if(stardata->preferences->post_SN_orbit_method == POST_SN_ORBIT_TT98)
    {
        tauris_takens_orbit(stardata,
                            pre_explosion_stardata,
                            pre_explosion_star,
                            exploder,
                            companion,
                            kick_system,
                            kick_distribution,
                            kick_dispersion,
                            kick_companion);
    }

    Dprint("kick_speed = %g, orbital_speed = %g, new_orbital_speed_squared = %g\n",
           kick_system->kick_speed,
           kick_system->orbital_speed,
           kick_system->new_orbital_speed_squared
        );

    /*
     * Calculate the new semi-major axis if the binary
     * was already bound.
     */
    const double m2n = companion->mass;
    const double new_system_mass = m1n + m2n;

    if(kick_system->bound_before_explosion == TRUE)
    {
        *separation = Min(1e100,
                          1.0/(2.0/kick_system->separation -
                               kick_system->new_orbital_speed_squared/
                               (GMRKM*new_system_mass)));

        Dprint("new separation = 1/(2/%g - %g/%g) = %12.12g\n",
               kick_system->separation,
               kick_system->new_orbital_speed_squared,
               new_system_mass,
               *separation);
    }

    SNprint("SN new separation %g from r=%g vn2=%g new_system_mass=%g : old omega_orb=%g %g\n",
            *separation,
            kick_system->separation,
            kick_system->new_orbital_speed_squared,
            new_system_mass,
            pre_explosion_stardata->common.orbit.angular_frequency,
            stardata->common.orbit.angular_frequency
        );


    Dprint("Pre-sep calc : sgl = %d\n",stardata->model.sgl);

    if(*separation < MINIMUM_SEPARATION_TO_BE_CALLED_BINARY ||
       *separation > MAXIMUM_SEPARATION_TO_BE_CALLED_BINARY ||
       stardata->model.sgl)
    {
        /* disrupted binary */
        *eccentricity = -1.0;
        stardata->model.sgl = TRUE;
    }
    else
    {
        /*
         * Calculate the new eccentricity.
         */
        *eccentricity = sqrt(
            Max(0.0,
                1.0 - kick_system->hn_squared/
                (GMRKM*(*separation)*new_system_mass)));
        Dprint("New eccentricity %g : hn^2=%g sep=%g new_system_mass=%g\n",
               *eccentricity,
               kick_system->hn_squared,
               *separation,
               new_system_mass);

        if(Fequal(*eccentricity,1.0))
        {
            /*
             * This shouldn't happen (often, at least)
             */
            Exit_binary_c(BINARY_C_OUT_OF_RANGE,
                          "Post-SN eccentricity is 1.0 : this is wrong");
            *eccentricity = -1.0;
            stardata->model.sgl = TRUE;
        }
        else
        {
            /*
             * Calculate the new orbital angular momentum taking care to convert
             * hn to units of Rsun^2/yr.
             *
             * hn = | r x v | = GMa(1-e^2)
             *
             * J = reduced_mass * r x v
             *
             * |J| = hn * reduced_mass
             *     = sqrt( m1*m2*2pi * (1-e^2) * sep / (m1+m2) )
             */
            const double reduced_mass = m1n*m2n/new_system_mass;
            *jorb = reduced_mass * kick_system->hn * (YEAR_LENGTH_IN_SECONDS/R_SUN_KM);

            /*
             * Determine the angle between the new and old orbital angular
             * momentum vectors. (Eq. A13, used for debugging)
             */

            /*
              const double cmu = (kick_system->orbital_speed*kick_system->sinbeta-
              kick_system->kick_speed*kick_system->cosomega*kick_system->cosphi)/(Max(1e-50,v1sq + v2sq));
              Dprint("%g %g %g %g %g %g %g\n",
              kick_system->orbital_speed,
              kick_system->sinbeta,
              kick_system->kick_speed,
              kick_system->cosomega,
              kick_system->cosphi,
              v1sq,
              v2sq);

              Dprint("Angle between old and new ang mom vectors, mu = %g (cmu = %g)\n",
              acos(cmu),cmu);
            */

            /*
             * Calculate the components of the velocity of the new centre-of-mass.
             * Eq. A14, assumes m2=m2n, and superceded by Tauris and Takens calculations)
             */
            /*
              const double mx1 = kick_system->kick_speed*m1n/new_system_mass;
              const double mx2 = kick_system->orbital_speed*(m1-m1n)*m2/
              Max(1e-50,new_system_mass*(m1+m2));
              const double vsx = mx1*kick_system->cosomega*kick_system->cosphi + mx2*kick_system->sinbeta;
              const double vsy = mx1*kick_system->sinomega*kick_system->cosphi + mx2*kick_system->cosbeta;
              const double vsz = mx1*kick_system->sinphi;
              Dprint("New COM velocity (%g, %g, %g)\n",vsx,vsy,vsz);
            */
        }
    }
}
