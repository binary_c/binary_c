#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Event handling function for supernovae: this deals with
 * an erase action
 */

Event_handler_function supernova_erase_event_handler(void * eventp Maybe_unused,
                                                     struct stardata_t * stardata,
                                                     void * data Maybe_unused)
{
    struct binary_c_event_t * const event = (struct binary_c_event_t *) eventp;
    struct binary_c_new_supernova_event_t * new_sn = (struct binary_c_new_supernova_event_t *) event->data;

    Dprint("SN event eraser star %p %d\n",
           (void*)new_sn->pre_explosion_star,
           new_sn->pre_explosion_star->starnum);

    free_stardata(&new_sn->pre_explosion_stardata);
    free_star(&new_sn->post_explosion_star);
    Safe_free(event->data); // == new_sn
    return NULL;
}
