#include "../binary_c.h"
No_empty_translation_unit_warning;



double Pure_function supernova_kinetic_energy_Marassi_2019(struct stardata_t * Restrict const stardata,
                                                           const struct star_t * Restrict const star)
{
    supernova_load_Marassi_2019(stardata);

    /*
     * Use the Marassi et al. (2019) data to estimate the
     * kinetic energy of the SN.
     *
     * We use log10(Z/0.0142) as a proxy for [Fe/H].
     */
    Dprint("Interpolate Marassi data\n");
    const double params[] = {
        log10(stardata->common.metallicity/0.0142),
        Hydrogen_burnt_core_mass(star)
    };
    double result[5];
    Interpolate(stardata->persistent_data->Marassi_2019_mapped,
                params,
                result,
                FALSE);

    /*
     * From Marassi et al. (2019):
     * result[0] is the kinetic energy in a mass
     * result[2] is the mass ejected
     *
     * We eject a different mass, so scale the energy
     * with mass_ejected/result[2].
     */

    return result[0] * FOE * star->SN_mass_ejected / result[2];
}
