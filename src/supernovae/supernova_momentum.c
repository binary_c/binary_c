#include "../binary_c.h"
No_empty_translation_unit_warning;


double Pure_function supernova_momentum(struct stardata_t * Restrict const stardata,
                                        const struct star_t * Restrict const pre_explosion_star)
{
    /*
     * Estimate the momentum in supernova ejecta (cgs)
     */
    const double v = 1e5 * supernova_ejecta_velocity(stardata,pre_explosion_star); /* velocity in cm/s */
    const double m = pre_explosion_star->SN_mass_ejected;
    return m * v;
}
