#include "../binary_c.h"
No_empty_translation_unit_warning;


void supernova_yield(struct stardata_t *stardata,
                     struct star_t * exploder,
                     struct star_t * pre_explosion_star,
                     const double dm, /* mass ejected */
                     const Stellar_type pre_explosion_companion_stellar_type Maybe_unused
    )
{
    /*
     * Eject mass dm from a supernova
     */
#ifdef NUCSYN
    Dprint("yield mass %g from supernova in star %d\n",
           dm,pre_explosion_star->starnum);
    nucsyn_sn_yield(stardata,
                    exploder,
                    pre_explosion_star,
                    dm,
                    pre_explosion_companion_stellar_type);
#endif // NUCSYN

#ifdef STELLAR_POPULATIONS_ENSEMBLE
    const Yield_source source = supernova_source_id(exploder,
                                                    pre_explosion_star,
                                                    Other_star_struct(exploder));
    if(source != SOURCE_UNKNOWN)
    {
        stardata->model.ensemble_yield[source] += dm;
    }
#endif // STELLAR_POPULATIONS_ENSEMBLE
}
