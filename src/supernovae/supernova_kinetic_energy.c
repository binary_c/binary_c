#include "../binary_c.h"
No_empty_translation_unit_warning;


double Pure_function supernova_kinetic_energy(struct stardata_t * Restrict const stardata,
                                              const struct star_t * Restrict const star)
{
    /*
     * Estimate the kinetic energy of a supernova
     * in erg
     */
    double energy;
    switch(star->SN_type)
    {
    case SN_IA_He:
    case SN_IA_ONeWD_DDet:
    case SN_IA_COWD_DDet:
    case SN_IA_CHAND:
    case SN_IA_He_Coal:
    case SN_IA_CHAND_Coal:
    case SN_IA_Hybrid_HeCOWD:
    case SN_IA_Hybrid_HeCOWD_subluminous:
    case SN_IA_HeStar:
    case SN_IA_SUBCHAND_He_Coal:
    case SN_IA_SUBCHAND_CO_Coal:
        energy = 1.0 * FOE;
        break;
    case SN_IBC:
    case SN_II:
    case SN_GRB_COLLAPSAR:
        if(star->SN_mass_ejected > 0.0)
        {
            if(stardata->preferences->core_collapse_energy_prescription == CORE_COLLAPSE_ENERGY_ONE_FOE)
            {
                energy = 1.0 * FOE;
            }
            else if(stardata->preferences->core_collapse_energy_prescription == CORE_COLLAPSE_ENERGY_MARASSI2019)
            {
                energy = supernova_kinetic_energy_Marassi_2019(stardata,
                                                               star);
            }
            else
            {
                energy = 0.0; /* prevent compiler warning */
                Exit_binary_c(BINARY_C_ALGORITHM_OUT_OF_RANGE,
                              "Supernova core-collapse energy algorithm %d is unknown.",
                              stardata->preferences->core_collapse_energy_prescription);
            }
        }
        else
        {
            /* no material ejected : dark SN */
            energy = 0.0;
        }
        break;
    default:
        energy = 0.0;
        break;
    };

    return energy;
}
