#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "supernovae.h"

void setup_binary_at_kick(struct stardata_t * stardata,
                          const double semi_major_axis,
                          const double eccentricity,
                          const double system_gravitational_mass,
                          struct kick_system_t * kick_system)
{
    Dprint("sgl = %d\n",stardata->model.sgl);
    if(semi_major_axis < TINY ||
       eccentricity < -TINY ||
       eccentricity > 1.0 ||
       stardata->model.sgl == TRUE
        )
    {
        /*
         * System is already disrupted or merged :
         * pretend it's just at very large semi_major_axis.
         */
        kick_system->bound_before_explosion = FALSE;
        kick_system->mean_anomaly =
            kick_system->eccentric_anomaly =
            kick_system->sinbeta =
            kick_system->orbital_speed =
            kick_system->orbital_speed_squared =
            0.0;
        kick_system->cosbeta = 1.0;
        kick_system->separation = 1e50;
    }
    else
    {
        kick_system->bound_before_explosion = TRUE;
        /*
         * Find the separation at the moment of explosion
         */
        Random_seed * Restrict random_seed = & stardata->common.random_seed;

        /* choose mean anomaly, NB cannot be zero */
        const double mean_anomaly =
            More_or_equal(stardata->preferences->SN_mean_anomaly,0.0) ?
            stardata->preferences->SN_mean_anomaly :
            Max(TINY,(double) random_number(stardata,random_seed)*TWOPI);
        const double eccentric_anomaly = eccentric_anomaly_from_mean_anomaly(
            stardata,
            eccentricity,
            mean_anomaly,
            0.1,
            MEAN_ANOMALY_TOLERANCE,
            MEAN_ANOMALY_MAX_ITERATIONS);

        kick_system->mean_anomaly = mean_anomaly;
        kick_system->eccentric_anomaly = eccentric_anomaly;

        /*
         * Separation at the moment of the kick
         *
         * this is the calculation seen most often as
         *
         * r = a (1 - e cos(theta))
         */
        kick_system->separation = semi_major_axis * (1.0 - eccentricity * cos(eccentric_anomaly));
        Dprint("separation at kick %g (a = %g)\n",kick_system->separation,semi_major_axis);


        /*
         * Find the orbital speed and relative
         * velocity angle, beta (as cos and sin beta)
         */

        // Hurley+ 2002 Eq. A2
        double ecc2 = Pow2(eccentricity);
        kick_system->sinbeta = sqrt((Pow2(semi_major_axis)*(1.0-ecc2))/
                                    (kick_system->separation*(2.0*semi_major_axis-kick_system->separation)));
        kick_system->cosbeta = -eccentricity*sin(eccentric_anomaly)/
            sqrt(1.0-ecc2*Pow2(cos(eccentric_anomaly)));

        // Eq. A5 in km^2 s^-2
        kick_system->orbital_speed_squared = GMRKM * system_gravitational_mass *
            (2.0/kick_system->separation - 1.0/semi_major_axis);

        // km s^-1
        kick_system->orbital_speed = sqrt(kick_system->orbital_speed_squared);
    }
    Dprint("Orbital speed at kick %20.12g (m1+m2=%g r=%g sep=%g bound? %s), Initial e=%g\n",
           kick_system->orbital_speed,
           system_gravitational_mass,
           kick_system->separation,
           semi_major_axis,
           (kick_system->bound_before_explosion == TRUE ? "Y" : "N"),
           eccentricity);

}
