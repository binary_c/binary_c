#pragma once
#ifndef DEBUG_PROTOTYPES_H
#define DEBUG_PROTOTYPES_H

void Gnu_format_args(6,7) debug_fprintf(struct stardata_t * const stardata,
                                        const char * Restrict const filename,
                                        const char * Restrict const function,
                                        const int fileline,
                                        const Boolean newline Maybe_unused,
                                        const char * Restrict const format,
                                        ...);

binary_c_API_function
void Print_trace(FILE * stream);
void print_trace(FILE * stream);
#if defined BACKTRACE && defined BACKTRACE_SYMBOLS_LOCAL
#ifdef __HAVE_LIBBFD__
void backtrace_symbols_fd_local(void *const *buffer, int size, int fd);
char **backtrace_symbols_local(void *const *buffer, int size);
#endif //__HAVE_LIBBFD__
#ifdef USE_BACKTRACE_CACHE
void backtrace_free_cache_memory(void);
#endif
#endif // BACKTRACE && BACKTRACE_SYMBOLS_LOCAL

void show_stardata(struct stardata_t * const stardata Maybe_unused);
void diff_stardata(struct stardata_t * const stardata Maybe_unused);

#ifdef __HAVE_LIBBACKTRACE__
#include "libbacktrace_prototypes.h"
#endif

#ifndef VALGRIND
void test_for_valgrind(void);
#endif

#endif // DEBUG_PROTOTYPES_H
