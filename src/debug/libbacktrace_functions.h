#pragma once
#ifndef LIBBACKTRACE_FUNCTIONS_H
#define LIBBACKTRACE_FUNCTIONS_H

struct _callback_data
{
    struct bt_ctx * ctx;
    FILE * stream;
};

/*
 * Stuff for using libbacktrace, taken from their
 * example code at
 * https://github.com/ErwanLegrand/libbacktrace/blob/master/examples/bt.c
 * although the updated version is at
 * https://github.com/ianlancetaylor/libbacktrace
 */
static void libbacktrace_error_callback(void *data, const char *msg, int errnum)
{
    //printf("binary_c: libbacktrace error callback\n");fflush(NULL);
    fprintf(stderr, "ERROR: %s (%d)", msg, errnum);fflush(NULL);
    struct bt_ctx *ctx = data;
    ctx->error = 1;
}

static void libbacktrace_syminfo_callback(void *data Maybe_unused,
                                          uintptr_t pc,
                                          const char *symname,
                                          uintptr_t symval Maybe_unused,
                                          uintptr_t symsize Maybe_unused)
{
    //printf("binary_c: libbacktrace syminfo callback\n");fflush(NULL);
    if(symname)
    {
        printf("%lx %s ??:0\n",
               (unsigned long)pc,
               symname);
    }
    else
    {
        printf("%lx ?? ??:0\n",
               (unsigned long)pc);
    }
}

static int libbacktrace_full_callback(void *data,
                                      uintptr_t pc,
                                      const char *filename,
                                      int lineno,
                                      const char *function)
{
    //printf("binary_c: libbacktrace full callback\n");fflush(NULL);
    struct _callback_data * cdata = (struct _callback_data*) data;
    if(function)
    {
        fprintf(cdata->stream,
                "%lx %s %s:%d\n",
                (unsigned long)pc,
                function,
                filename?filename:"??",
                lineno);
    }
    else
    {
        backtrace_syminfo(cdata->ctx->state,
                          pc,
                          (backtrace_syminfo_callback)libbacktrace_syminfo_callback,
                          libbacktrace_error_callback,
                          data);
    }
    return 0;
}



static inline int libbacktrace_simple_callback(void *data,
                                               uintptr_t pc)
{
    struct _callback_data * cdata = (struct _callback_data *)data;
    struct bt_ctx *ctx = (struct bt_ctx*) cdata->ctx;
    backtrace_pcinfo(ctx->state,
                     pc,
                     libbacktrace_full_callback,
                     libbacktrace_error_callback,
                     data);
    return 0;
}

static inline int libbacktrace_callback(void *data,
                                        uintptr_t pc)
{
    //printf("binary_c: libbacktrace simple callback\n");fflush(NULL);
    struct _callback_data * cdata = (struct _callback_data *)data;
    struct bt_ctx *ctx = (struct bt_ctx*) cdata->ctx;
    backtrace_pcinfo(ctx->state,
                     pc,
                     libbacktrace_full_callback,
                     libbacktrace_error_callback,
                     data);
    return 0;
}

static inline void libbacktrace_bt(struct backtrace_state *state,
                                   FILE * stream)
{
    struct bt_ctx ctx = {
        state,
        0
    };
    struct _callback_data cdata = {
        &ctx,
        stream
    };
    backtrace_simple(state,
                     LIBBACKTRACE_SKIP,
                     libbacktrace_simple_callback,
                     libbacktrace_error_callback,
                     &cdata);

}
#endif // LIBBACKTRACE_FUNCTIONS_H
