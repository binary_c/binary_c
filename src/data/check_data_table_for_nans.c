#include "../binary_c.h"
No_empty_translation_unit_warning;

Boolean check_data_table_for_nans(const struct data_table_t * const table)
{
    /*
     * Check a data table for nans: return TRUE
     * if a NaN is found, FALSE otherwise.
     */

#undef item
#define item(I,J)                               \
    *(table->data +                             \
      (I)*(table->nparam + table->ndata) +      \
      (J))

    for(size_t i=0; i<table->nlines; i++)
    {
        for(size_t j=0; j<table->nparam; j++)
        {
            if(isnan(item(i,j)))
            {
                return TRUE;
            }
        }
        for(size_t j=0; j<table->ndata; j++)
        {
            if(isnan(item(i,j+table->nparam)))
            {
                return TRUE;
            }
        }
    }
    return FALSE;
}
