#include "../binary_c.h"
No_empty_translation_unit_warning;

void remove_data_table_leading_rows(struct data_table_t * const table,
                                 const size_t n)
{
    /*
     * Remove n leading rows
     */
    const size_t width = table->nparam + table->ndata;
    double * const newdata = Malloc(sizeof(double) * width * (table->nlines - n));
    memcpy(newdata,
           table->data + width * n,
           sizeof(double) * width * (table->nlines - n));
    Safe_free(table->data);
    table->data = newdata;
    table->nlines -= n;
}
