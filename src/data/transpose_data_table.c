#include "../binary_c.h"
No_empty_translation_unit_warning;

void transpose_data_table(struct data_table_t * const table,
                                 const size_t new_nparam)
{
    /*
     * Transpose the data in table in place.
     */
    const size_t oldwidth = table->nparam + table->ndata;
    const size_t newwidth = table->nlines;
    double * const olddata = table->data;
    double * const newdata = Malloc(sizeof(double) * oldwidth * table->nlines);
    for(size_t row=0; row<table->nlines; row++)
    {
        for(size_t col=0; col<oldwidth; col++)
        {
            *(newdata + col * newwidth + row) = *(olddata + row * oldwidth + col);
        }
    }
    Safe_free(table->data);
    table->data = newdata;
    table->nlines = oldwidth;
    table->nparam = new_nparam;
    table->ndata = newwidth - new_nparam;
}
