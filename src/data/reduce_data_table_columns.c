#include "../binary_c.h"
No_empty_translation_unit_warning;

struct data_table_t * reduce_data_table_columns(struct stardata_t * const stardata,
                                                struct data_table_t * const table,
                                                int * wanted_columns,
                                                int n_wanted_columns)
{
    /*
     * Given a data table, select certain columns and
     * return a new table with only those selected.
     *
     * wanted_columns is a pointer to an array of the
     * desired columns, of which there are n_wanted_columns.
     *
     * If wanted_columns is NULL, we choose them all.
     */

    const size_t orig_width = table->ndata + table->nparam;

    Boolean alloc_wanted_columns;
    if(wanted_columns == NULL ||
       wanted_columns[0] == -1)
    {
        /*
         * We want all columns
         */
        n_wanted_columns = orig_width;
        wanted_columns = Malloc(sizeof(int) * orig_width);
        for(int j=0; j<n_wanted_columns; j++)
        {
            wanted_columns[j] = j;
        }
        alloc_wanted_columns = TRUE;
        printf("reduce : all columns : n_wanted_columns %d\n",
               n_wanted_columns);
    }
    else
    {
        alloc_wanted_columns = FALSE;
        printf("reduce : orig_width %zu, n_wanted_columns %d\n",
               orig_width,
               n_wanted_columns);
    }

    /*
     * And count the new number of parameters and data
     */
    int  ndata = 0;
    int nparam = 0;
    for(int j=0; j<n_wanted_columns; j++)
    {
        if(wanted_columns[j] < (int)table->nparam)
        {
            nparam++;
        }
        else
        {
            ndata++;
        }
    }

    /*
     * Malloc space for reduced data
     */
    double * reduced_data = Malloc(n_wanted_columns * table->nlines * sizeof(double));
    double * reduced_p = reduced_data;
    Dprint("reduce\n");
    for(size_t i=0; i<table->nlines; i++)
    {
        double * const all_line = table->data + i * orig_width;
        for(int j=0; j<n_wanted_columns; j++)
        {
            *(reduced_p++) = *(all_line + wanted_columns[j]);
        }
    }

    /*
     * Make reduced table
     */
    struct data_table_t * reduced_table = copy_data_table(table);
    Safe_free(reduced_table->data);
    reduced_table->data = reduced_data;
    reduced_table->nparam = nparam;
    reduced_table->ndata = ndata;
    reduced_table->label = table->label == NULL ? NULL : strdup(table->label);

    /*
     * Free allocated memory
     */
    if(alloc_wanted_columns == TRUE)
    {
        Safe_free(wanted_columns);
    }

    /*
     * Return the reduced table
     */
    return reduced_table;
}
