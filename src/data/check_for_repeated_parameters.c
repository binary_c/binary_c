#include "../binary_c.h"
No_empty_translation_unit_warning;

Boolean check_for_repeated_parameters(struct stardata_t * const stardata MAYBE_UNUSED,
                                      struct data_table_t * const table,
                                      size_t * const firstmatch)
{
    /*
     * Check table for repeated sets of parameters.
     *
     * Return TRUE if repeats found, FALSE otherwise.
     *
     * firstmatch, if not NULL, is set to the first
     * matching line number
     */
    const size_t ll = table->ndata + table->nparam;
    const size_t cmpsize = sizeof(double) * table->nparam;
    double * const data = table->data;
    for(size_t i=0; i<table->nlines; i++)
    {
        /*
         * compare previous line : this is the most likely
         * scenario
         */
        if(i>0 &&
           memcmp(data + (i-1) * ll,
                  data + i * ll,
                  cmpsize)==0)
        {
            if(firstmatch != NULL)
            {
                *firstmatch = i;
            }
            return TRUE;
        }
    }

    for(size_t i=0; i<table->nlines; i++)
    {
        /*
         * Compare all other lines
         */
        for(size_t j=0; j<table->nlines; j++)
        {
            if(j != i && // cannot compare same lines
               j != i-1 && // already done this
               i>0 &&
               memcmp(data + j * ll,
                      data + i * ll,
                      cmpsize)==0)
            {
                if(firstmatch != NULL)
                {
                    *firstmatch = i;
                }
                return TRUE;
            }
        }
    }
    return FALSE;
}
