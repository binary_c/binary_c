#include "../binary_c.h"
No_empty_translation_unit_warning;

void add_leading_columns_to_data_table(struct data_table_t * const table,
                                 const size_t n_new_columns)
{
    /*
     * Add n_new_columns leading columns as parameters
     */
    const size_t oldwidth = table->nparam + table->ndata;
    const size_t newwidth = n_new_columns + oldwidth;
    double * const newdata = Malloc(sizeof(double) * newwidth * table->nlines);
    double * const olddata = table->data;
    for(size_t row=0; row<table->nlines; row++)
    {
        memset(newdata + row * newwidth,
               0,
               sizeof(double) * n_new_columns);
        memcpy(newdata + row * newwidth + n_new_columns,
               olddata + row * oldwidth,
               sizeof(double) * oldwidth);
    }
    Safe_free(table->data);
    table->data = newdata;
    table->nparam += n_new_columns;
}
