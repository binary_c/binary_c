#include "../binary_c.h"
No_empty_translation_unit_warning;

void join_data_tables_in_place(struct stardata_t * const stardata,
                               struct data_table_t * const table1,
                               struct data_table_t * const table2,
                               const size_t offset1,
                               const size_t step1,
                               const size_t offset2,
                               const size_t step2)
{
    /*
     * wrapper for join_data_tables to leave the result in table1
     */
    struct data_table_t * joined = join_data_tables(stardata,
                                                    table1,
                                                    table2,
                                                    offset1,
                                                    step1,
                                                    offset2,
                                                    step2);
    if(joined != NULL)
    {
        /*
         * Copy pointer to joined data and update
         * number of lines
         */
        Safe_free(table1->data);
        memcpy(table1,
               joined,
               sizeof(struct data_table_t));
        Safe_free_nocheck(joined);
    }
}
