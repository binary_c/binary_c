

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit


#include <stdio.h>
#include <ctype.h>
//#define JSMN_HEADER
#include "jsmn/jsmn.h"

/*
 * Verbose logging?
 */
#undef jprint
#define jprint(...)                             \
    if(cdict->vb)                               \
    {                                           \
        fprintf(stdout,"JSON_TO_C:");           \
        fprintf(stdout,__VA_ARGS__);            \
        fflush(stdout);                         \
    }
//#undef jprint
//#define jprint(...) /* do nothing */

struct nest_stack_t {
    union cdict_value_union value;
    int cdict_type;
    int start;
    int end;
};
static CDict_key_type float_or_int(const char * const s);
static void show_stack(struct nest_stack_t ** stack,
                       const int stacksize) CDict_maybe_unused;

#define _JSMN_type_string(TYPE)                                         \
    (t->type == JSMN_UNDEFINED ? "JSMN_UNDEFINED" :                     \
     t->type == JSMN_OBJECT ? "JSMN_OBJECT" :                           \
     t->type == JSMN_ARRAY ? "JSMN_ARRAY" :                             \
     t->type == JSMN_STRING ? "JSMN_STRING" :                           \
     t->type == JSMN_PRIMITIVE ? "JSMN_PRIMITIVE (float, boolean or int)" : \
     "unknown JSMN type")


__CDict_Nonnull_some_arguments(1,3)
CDict_API_function
void cdict_json_to(const char * const json_buffer,
                   const size_t buflen,
                   struct cdict_t * const cdict)
{
    /*
     * Given JSON in a buffer string, of length buflen,
     * use JSMN to split it and then load it into
     * a cdict.
     */
    cdict->vb=0;

    /*
     * Use JSMN to convert the JSON buffer
     */
    jsmn_parser parser;

    /*
     * Determine number of required tokens
     */
    jsmn_init(&parser);
    const int ntokens = jsmn_parse(&parser,
                                   json_buffer,
                                   buflen,
                                   NULL,
                                   0);

    /*
     * Allocate memory for tokens
     */
    jsmntok_t * tokens = __CDict_calloc(cdict,
                                        ntokens,
                                        sizeof(jsmntok_t));
    jsmn_init(&parser);
    const int err = jsmn_parse(&parser,
                               json_buffer,
                               buflen,
                               tokens,
                               ntokens);
    if(err < 0)
    {
        /*
         * Some kind of jsmn error
         */
        cdict_error(cdict,
                    CDICT_ERROR_JSMN_ERROR,
                    "failed to parse JSON buffer, error is %d \"%s\"\n",
                    err,
                    err == JSMN_ERROR_INVAL ? "JSMN_ERROR_INVAL" :
                    err == JSMN_ERROR_NOMEM ? "JSMN_ERROR_NOMEM" :
                    err == JSMN_ERROR_PART ? "JSMN_ERROR_PART" :
                    "Unknown JSMN error");
    }
    else
    {
        /*
         * All seems well, load the tokens into the cdict.
         *
         * Note that we avoid token 0 because this is the outermost
         * { ... } which is already the cdict.
         */
        enum {
            JSON_UNDEF = 0,
            JSON_KEY = 1,
            JSON_VALUE = 2
        };

        /*
         * max nest depth: this is not particularly
         * reliable but gives us some idea of the size
         * of the nest stack
         */
        int maxnest = 0;
        int i = 0;
        while(i<ntokens)
        {
            maxnest = __CDict_Max(tokens[i++].size,
                                  maxnest);
        }

        /* build nest-string stack */
        int stackn = 0; // current stack top
        int stacksize = maxnest + 2; // max stack size
        struct nest_stack_t ** nest_stack =
            __CDict_malloc(cdict,
                           sizeof(struct nest_stack_t*) * stacksize);
        for(i=0;i<stacksize;i++)
        {
            nest_stack[i] =
                __CDict_malloc(cdict,
                               sizeof(struct nest_stack_t));
            memset(&(nest_stack[i]->value),0,sizeof(union cdict_value_union));
            nest_stack[i]->cdict_type = CDICT_DATA_TYPE_UNKNOWN;
        }
        union cdict_key_union * key_stack =
            __CDict_calloc(cdict,
                           stacksize,
                           sizeof(union cdict_key_union));
        CDict_key_type * key_type_stack =
            __CDict_calloc(cdict,
                           stacksize,
                           sizeof(CDict_key_type));

        /* we start with a key */
        unsigned int json_type = JSON_UNDEF;

        /* loop over tokens */
        i=1;
        while(i<ntokens)
        {
            const jsmntok_t * const t = &tokens[i];

            jprint("\n\ntoken %d at %p type %d == %s from %d to %d, contents %.*s, nested %d\n",
                   i,
                   (void*)t,
                   (int)t->type,
                   _JSMN_type_string(t->type),
                   t->start,
                   t->end,
                   t->end - t->start,
                   json_buffer + t->start,
                   t->size
                );

            unsigned int prev_json_type = json_type;
            if(prev_json_type == JSON_KEY)
            {
                /* we were a key type, can now only be a value */
                json_type = JSON_VALUE;
            }
            else if(prev_json_type == JSON_UNDEF ||
                    prev_json_type == JSON_VALUE)
            {
                /* we were a value, must now be a key */
                json_type = JSON_KEY;
            }
            else if(t->size == 0)
            {
                /* final : must be value */
                json_type = JSON_VALUE;
            }

            /*
             * Clean objects off the stack
             */
            if(json_type == JSON_KEY &&
               stackn > 0 &&
               t->type != JSMN_OBJECT &&
               t->start > nest_stack[stackn-1]->end)
            {
                /* remove top stack item (is freed later) */
                while(stackn > 0 &&
                      t->start > nest_stack[stackn-1]->end)
                {
                    stackn--;
                }
            }

            if(t->type == JSMN_STRING)
            {
                /*
                 * string
                 */
                char * s = CDict_stringn(cdict,
                                         json_buffer + t->start,
                                         t->end - t->start);

                if(json_type == JSON_KEY)
                {
                    /* add key to the stack */
                    stackn++;
                    if(stackn > stacksize)
                    {
                        stacksize++;
                        nest_stack = __CDict_realloc(cdict,
                                                     nest_stack,
                                                     sizeof(struct nest_stack_t*) * stacksize);
                        memset(&(nest_stack[stackn-1]->value),0,sizeof(union cdict_value_union));
                        nest_stack[stackn-1]->cdict_type = CDICT_DATA_TYPE_UNKNOWN;
                        key_stack = __CDict_realloc(cdict,
                                                    key_stack,
                                                    sizeof(union cdict_key_union) * stacksize);
                        memset(key_stack[stackn-1].void_pointer_data,0,sizeof(union cdict_key_union));

                    }
                    nest_stack[stackn-1]->end = -1;
                    nest_stack[stackn-1]->value.string_data = s;
                    nest_stack[stackn-1]->cdict_type = CDICT_DATA_TYPE_STRING;
                    key_stack[stackn-1].string_data = s;
                    key_type_stack[stackn-1] = CDICT_DATA_TYPE_STRING;
                }
                else
                {
                    /* nest value */
                    struct nest_stack_t * last = nest_stack[stackn-1];

                    last->end = t->end;

                    // make final key
                    CDict_key_type keytype = last->cdict_type;
                    __CDict_new_key_union(key);

                    if(keytype == CDICT_DATA_TYPE_STRING)
                    {
                        key.string_data = last->value.string_data;
                    }
                    else if(keytype == CDICT_DATA_TYPE_DOUBLE)
                    {
                        key.double_data = last->value.double_data;
                    }
                    else if(keytype == CDICT_DATA_TYPE_BOOLEAN)
                    {
                        key.Boolean_data = last->value.Boolean_data;
                    }
                    else if(keytype == CDICT_DATA_TYPE_INT)
                    {
                        key.int_data = last->value.int_data;
                    }
                    else
                    {
                        key.void_pointer_data = last->value.void_pointer_data;
                    }

                    // make final value
                    __CDict_new_value_union(value);
                    value.string_data = s;

                    // fill paths and pathtypenums
                    cdict_nest(cdict,
                               key_stack, // paths
                               (const size_t)stackn-1, // npath
                               key_type_stack, // pathtypenums
                               key, // key
                               keytype, // keytype
                               value, // value
                               -1,
                               CDICT_DATA_TYPE_STRING, // valuetype
                               NULL,
                               NULL,
                               CDICT_NEST_ACTION_APPEND);
                }
            }
            else if(t->type == JSMN_OBJECT)
            {
                /*
                 * Object type { ... } : requires a Nested cdict,
                 * should only be a value
                 */
                json_type = JSON_UNDEF;
                nest_stack[stackn-1]->end = t->end;
            }
            else if(t->type == JSMN_PRIMITIVE)
            {
                /*
                 * float, int or boolean type
                 */
                jprint("Float int or boolean value type, json_type %s\n",
                       _JSMN_type_string(json_type));
                char * s = strndup(json_buffer + t->start,
                                   t->end - t->start);
                const size_t l = strlen(s);
                CDict_key_type value_type;
                __CDict_new_value_union(value);

                /*
                 * Explicitly check for booleans
                 */
                if(l == 4 && strncmp(s,"true",4)==0)
                {
                    value_type =  CDICT_DATA_TYPE_BOOLEAN;
                    value.Boolean_data = TRUE;
                }
                else if(l == 5 && strncmp(s,"false",5)==0)
                {
                    value_type =  CDICT_DATA_TYPE_BOOLEAN;
                    value.Boolean_data = FALSE;
                }
                else
                {
                    /*
                     * Is a number.
                     *
                     * Try to distinguish integers from doubles
                     */
                    value_type = float_or_int(s);
                    if(value_type == CDICT_DATA_TYPE_INT)
                    {
                        value.int_data = (int)strtol(s,NULL,10);
                    }
                    else
                    {
                        value.double_data = CDict_strtod(s,NULL);
                    }
                }
                free(s);

                if(json_type == JSON_KEY)
                {
                    /* add key to the stack */
                    stackn++;
                    if(stackn > stacksize)
                    {
                        stacksize++;
                        nest_stack = __CDict_realloc(cdict,
                                                     nest_stack,
                                                     sizeof(struct nest_stack_t*) * stacksize);
                        key_stack = __CDict_realloc(cdict,
                                                    key_stack,
                                                    sizeof(union cdict_key_union) * stacksize);
                    }
                    nest_stack[stackn-1]->value = value;
                    if(value_type == CDICT_DATA_TYPE_DOUBLE)
                    {
                        key_stack[stackn-1].double_data = value.double_data;
                    }
                    else if(value_type == CDICT_DATA_TYPE_BOOLEAN)
                    {
                        key_stack[stackn-1].Boolean_data = value.Boolean_data;
                    }
                    else if(value_type == CDICT_DATA_TYPE_INT)
                    {
                        key_stack[stackn-1].int_data = value.int_data;
                    }
                    nest_stack[stackn-1]->cdict_type = value_type;
                    nest_stack[stackn-1]->end = -1;
                    key_type_stack[stackn-1] = value_type;
                }
                else
                {
                    /* nest value */
                    struct nest_stack_t * last = nest_stack[stackn-1];

                    /* update stack final position */
                    last->end = t->end;

                    // make final key
                    CDict_key_type keytype = last->cdict_type;
                    jprint("Make keytype %d == %s\n",
                           keytype,
                           __CDict_typename(keytype));
                    __CDict_new_key_union(key);
                    if(keytype == CDICT_DATA_TYPE_STRING)
                    {
                        char * const _s = CDict_string(cdict,
                                                       last->value.string_data);
                        key.string_data = _s;
                    }
                    else if(keytype == CDICT_DATA_TYPE_DOUBLE)
                    {
                        key.double_data = last->value.double_data;
                    }
                    else if(keytype == CDICT_DATA_TYPE_BOOLEAN)
                    {
                        key.Boolean_data = last->value.Boolean_data;
                    }
                    else if(keytype == CDICT_DATA_TYPE_INT)
                    {
                        key.int_data = last->value.int_data;
                    }
                    else
                    {
                        key.void_pointer_data = last->value.void_pointer_data;
                    }

                    // fill paths and pathtypenums
                    cdict_nest(cdict,
                               key_stack, // paths
                               (const size_t)stackn-1, // npath
                               key_type_stack, // pathtypenums
                               key,
                               keytype,
                               value,
                               -1,
                               value_type, // valuetype
                               NULL,
                               NULL,
                               CDICT_NEST_ACTION_APPEND);
                }
            }
            else if(t->type == JSMN_ARRAY)
            {
                /*
                 * double, int, boolean or string array
                 */
                jprint("ARRAY\n");
                const size_t array_size = t->size;
                const jsmntok_t * child = t+1;
                CDict_key_type child_type = CDICT_DATA_TYPE_UNKNOWN;
                CDict_key_type array_type = CDICT_DATA_TYPE_UNKNOWN;
                union cdict_value_union array_data = {0};
                memset(&array_data,0,sizeof(union cdict_value_union));

                {
                    char * s = strndup(json_buffer + child->start,
                                       child->end - child->start);

                    /* choose type */
                    if(child->type == JSMN_STRING)
                    {
                        child_type = CDICT_DATA_TYPE_STRING;
                        __CDict_malloc(cdict,
                                       sizeof(char*)*t->size);
                    }
                    else if(child->type == JSMN_OBJECT)
                    {
                        child_type = CDICT_DATA_TYPE_CDICT_ARRAY;
                        array_data.cdict_array_data =
                            __CDict_malloc(cdict,
                                           sizeof(struct cdict_t)*t->size);
                        cdict_push_tofree(cdict,
                                          array_data.cdict_array_data);
                    }
                    else if(child->type == JSMN_PRIMITIVE)
                    {
                        if(s[0] == 't' ||
                           s[0] == 'f' ||
                           s[0] == 'T' ||
                           s[0] == 'F')
                        {
                            child_type = CDICT_DATA_TYPE_BOOLEAN;
                            array_data.Boolean_array_data =
                                __CDict_malloc(cdict,
                                               sizeof(Boolean)*t->size);
                            cdict_push_tofree(cdict,
                                              array_data.Boolean_array_data);
                        }
                        else if(s[0] == '-' ||
                                s[0] == '+' ||
                                (s[0] >= '0' && s[0] <= '9'))
                        {
                            child_type = float_or_int(s);
                            if(child_type == CDICT_DATA_TYPE_INT)
                            {
                                array_data.int_array_data =
                                    __CDict_malloc(cdict,
                                                   sizeof(int)*t->size);
                                cdict_push_tofree(cdict,
                                                  array_data.int_array_data);
                            }
                            else
                            {
                                array_data.double_array_data =
                                    __CDict_malloc(cdict,
                                                   sizeof(double)*t->size);
                                cdict_push_tofree(cdict,
                                                  array_data.double_array_data);
                            }
                        }
                        else
                        {
                            cdict_error(cdict,
                                        CDICT_ERROR_JSMN_CHILD_TYPE_DETECTION_FAILED,
                                        "failed to determine JSON array child element type using jsmn\n");
                        }
                    }
                    else
                    {
                        cdict_error(cdict,
                                    CDICT_ERROR_JSMN_CHILD_TYPE_UNSUPPORTED,
                                    "failed to determine JSON array child type %d is not JSMN_STRING or JSMN_PRIMITIVE, so is not supported.\n");
                    }

                    CDict_Safe_free(cdict,s);
                    array_type =
                        __CDict_map_scalar_type_to_array_type(child_type);
                    jprint("Scalar type %d mapped to array type %d\n",
                           child_type,
                           array_type);
                }

                /*
                 * Set the data from converted strings into the
                 * correct member of the array_data union
                 */
                for(size_t j=0; j<array_size; j++)
                {
                    const size_t slen = child->end - child->start;
                    char * s = strndup(json_buffer + child->start,
                                       slen);
                    if(s == NULL)
                    {
                        cdict_error(cdict,
                                    CDICT_ERROR_OUT_OF_MEMORY,
                                    "strndup failed in cdict_json_to_c : out of memory?");
                    }
                    else
                    {
                        if(array_type == CDICT_DATA_TYPE_INT_ARRAY)
                        {
                            array_data.int_array_data[j] = (int)strtol(s,NULL,10);
                        }
                        else if(array_type == CDICT_DATA_TYPE_DOUBLE_ARRAY)
                        {
                            array_data.double_array_data[j] = CDict_strtod(s,NULL);
                        }
                        else if(array_type == CDICT_DATA_TYPE_BOOLEAN_ARRAY)
                        {
                            array_data.Boolean_array_data[j] =
                                (s[0] == 't' || s[0] == 'T') ? TRUE : FALSE;
                        }
                        else if(array_type == CDICT_DATA_TYPE_CDICT_ARRAY)
                        {
                            CDict_new(c);
                            cdict_json_to(s,slen,c);
                            array_data.cdict_array_data[j] = c;
                        }
                        CDict_Safe_free(cdict,s);
                        child++;
                    }
                }

                // make final key
                struct nest_stack_t * last = nest_stack[stackn-1];
                last->end = t->end;
                CDict_key_type keytype = last->cdict_type;
                __CDict_new_key_union(key);
                if(keytype == CDICT_DATA_TYPE_STRING)
                {
                    key.string_data = strdup(last->value.string_data);
                }
                else if(keytype == CDICT_DATA_TYPE_DOUBLE)
                {
                    key.double_data = last->value.double_data;
                }
                else if(keytype == CDICT_DATA_TYPE_BOOLEAN)
                {
                    key.Boolean_data = last->value.Boolean_data;
                }
                else if(keytype == CDICT_DATA_TYPE_INT)
                {
                    key.int_data = last->value.int_data;
                }
                else
                {
                    key.void_pointer_data = last->value.void_pointer_data;
                }

                // fill paths and pathtypenums
                cdict_nest(cdict,
                           key_stack, // paths
                           (const size_t)stackn-1, // npath
                           key_type_stack, // pathtypenums
                           key, // key
                           keytype, // keytype
                           array_data, // value = array data
                           t->size, // array size
                           array_type, // array valuetype
                           NULL,
                           NULL,
                           CDICT_NEST_ACTION_APPEND);

                /*
                 * Skip the processed tokens
                 */
                i += t->size;
            }
            else
            {
                /*
                 * oops
                 */
                printf("ERROR in cdict_to_json() : type %u did not match any JSMN type.\n",
                       t->type);
            }


            /*
             * If we're a value, pop off the location stack
             * unless we're a nested cdict
             */
            if(
                json_type == JSON_VALUE &&
                t->type != JSMN_OBJECT &&
                t->start > nest_stack[stackn-1]->end
                )
            {
                /* remove top stack item */
                while(t->start > nest_stack[stackn-1]->end)
                {
                    if(nest_stack[stackn-1]->cdict_type == CDICT_DATA_TYPE_STRING)
                    {
                        CDict_Safe_free(cdict,nest_stack[stackn-1]->value.string_data);
                        CDict_Safe_free(cdict,key_stack[stackn-1].string_data);
                    }
                    stackn--;
                }
            }

            //show_stack(nest_stack,stackn);
            i++;
        }

        /*
         * Free memory
         */
        for(i=0;i<stacksize;i++)
        {
            if(nest_stack[i]->cdict_type == CDICT_DATA_TYPE_STRING)
            {
                //CDict_Safe_free(cdict,nest_stack[i]->value.string_data);
                //CDict_Safe_free(cdict,key_stack[i].string_data);
            }
            CDict_Safe_free(cdict,nest_stack[i]);
        }
        CDict_Safe_free(cdict,key_stack);
        CDict_Safe_free(cdict,key_type_stack);
        CDict_Safe_free(cdict,nest_stack);
        CDict_Safe_free(cdict,tokens);
    }



    char * buffer = NULL;
    size_t buffer_size = 0;
    CDict_to_JSON(cdict,
                  buffer,
                  buffer_size,
                  CDICT_JSON_WHITESPACE);
    jprint("Final JSON from cdict = %p (buffer_size %zu) :\n%s\n",
           (void*)cdict,
           buffer_size,
           buffer);
    CDict_Safe_free(cdict,buffer);
}


static void show_stack(struct nest_stack_t ** stack,
                       const int stacksize)
{
    int i;
    for(i=0;i<stacksize;i++)
    {
        const struct nest_stack_t * const s = stack[i];
        printf("%3d: %10s to %10d : ",
               i,
               __CDict_value_descriptor(s->cdict_type),
               s->end);
        if(s->cdict_type == CDICT_DATA_TYPE_STRING)
        {
            printf("%s ",s->value.string_data);
        }
        else if(s->cdict_type == CDICT_DATA_TYPE_BOOLEAN)
        {
            printf("%s ",__CDict_truefalse(s->value.Boolean_data));
        }
        else if(s->cdict_type == CDICT_DATA_TYPE_INT)
        {
            printf("%d ",s->value.int_data);
        }
        else if(s->cdict_type == CDICT_DATA_TYPE_DOUBLE)
        {
            printf("%g ",s->value.double_data);
        }
        else
        {
            printf("%p ",(void*)s->value.void_pointer_data);
        }
        printf("\n");
    }
    printf("\n");
}

static CDict_key_type float_or_int(const char * const s)
{
    int n,int_value;
    const int r = sscanf(s,"%d%n",&int_value,&n);
    if(r==1 && n>=0 && (size_t)n==strlen(s))
    {
        /* int */
        return CDICT_DATA_TYPE_INT;
    }
    else
    {
        /* assume otherwise double */
        return CDICT_DATA_TYPE_DOUBLE;
    }
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        