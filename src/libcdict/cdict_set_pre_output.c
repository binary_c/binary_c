

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit

 
/*
 * Set metadata in an entry: to do this we
 * only need to copy the pointer.
 *
 * Note: we only set the label if it is non-NULL.
 */

__CDict_Nonnull_some_arguments(1)
CDict_API_function
void cdict_set_pre_output(struct cdict_t * const cdict,
                          struct cdict_entry_t * const entry,
                          const void * const pre_output_data,
                          cdict_pre_output_f pre_output_function)
{
    if(entry->pre_output == NULL)
    {
        entry->pre_output = __CDict_malloc(cdict,sizeof(struct cdict_pre_output_t));
    }
    entry->pre_output->data = (void *) pre_output_data;
    entry->pre_output->function = (cdict_pre_output_f) pre_output_function;
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        