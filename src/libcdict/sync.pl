#!/usr/bin/env perl
use rob_misc;

# script to sync the latest librinterpolate to binary_c

my $src = "$ENV{HOME}/git/libcdict/";
my $dest = '.';

mkdir 'ryu';
mkdir 'jsmn';
`cp $src/src/*.c ./`;
`cp $src/src/ryu/*.c ./ryu/`;
`cp $src/src/jsmn/*.c ./jsmn/`;
`cp $src/src/*.h ./`;
`cp $src/src/ryu/*.h ./ryu/`;
`cp $src/src/jsmn/*.h ./jsmn/`;
`rsync -avP $src/CHANGELOG $src/LICENCE $src/README $src/README.md ./`;
unlink 'cdict';
unlink 'cdict-config.c';
unlink 'cdict_tests.c';

foreach my $cfile (`ls *.c ryu/*.c`)
{
    chomp $cfile;
    my $relative = ($cfile=~/ryu\//) ? '../' : '';
    print "Process $cfile (relative '$relative')\n";
    my $code = slurp($cfile);
    my $gsource;
    if($code=~s/(\#define _GNU_SOURCE)//g)
    {
        $gsource = '
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
';
    }
    else
    {
        $gsource = '';
    }
    my $binch = '#include "../'.$relative.'binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "'.$relative.'cdict.h"
#undef exit
';

    $code =~ s/(\#include \"cdict\.h\"\n)/$binc/;

    # disable fortran API
    if($cfile=~/fortran/)
    {
        $code = "#undef __DO_NOT_INCLUDE_FORTRAN_CODE\n#ifdef __DO_NOT_INCLUDE_FORTRAN_CODE\n" . $code . "\n#endif // __DO_NOT_INCLUDE_FORTRAN_CODE\n";
    }

    $code =
"

$binch
".
        $code . '
#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        ';



    dumpfile($cfile,$code);
}

foreach my $hfile (`ls *.h ryu/*.h`)
{
    chomp $hfile;
    my $relative = ($hfile=~/ryu\//) ? '../' : '';
    print "Process $hfile (relative '$relative')\n";
    my $code = slurp($hfile);

    my $binch = '#include "../'.$relative.'binary_c.h"
        #ifndef __HAVE_LIBCDICT__
        ';

    # disable fortran API
    if($hfile=~/fortran/)
    {
        $code = "#undef __DO_NOT_INCLUDE_FORTRAN_CODE\n#ifdef __DO_NOT_INCLUDE_FORTRAN_CODE\n" . $code . "\n#endif // __DO_NOT_INCLUDE_FORTRAN_CODE\n";
    }

    $code = $binch . "\n" . $code .
'
#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
       ';

    # make cdict API functions hidden
    $code=~s/\#define CDict_API_function __attribute__ \(\(visibility\(\"default\"\)\)\)/\#define CDict_API_function __attribute__ \(\(visibility\(\"hidden\"\)\)\)/g;

    dumpfile($hfile,$code);
}
