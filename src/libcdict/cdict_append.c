

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit


#include <stdio.h>

/*
 * Set entry into the cdict, or append if it already exists.
 *
 * Returns a pointer to the entry that is appended or created,
 * or NULL on error (e.g. if the data type is a pointer which
 * cannot be added).
 */

__CDict_Nonnull_some_arguments(1)
CDict_API_function
struct cdict_entry_t * cdict_append(struct cdict_t * const cdict,
                                    union cdict_key_union key,
                                    const CDict_key_type keytype,
                                    const union cdict_value_union value,
                                    const cdict_size_t nvalue,
                                    const CDict_value_type valuetype,
                                    const void * const metadata,
                                    cdict_metadata_free_f metadata_free_function)
{
    /*
     * Add entry into cdict, appending if required.
     */
    if(cdict->vb)
    {
        printf("cdict_append : call cdict_contains keytype=%d\n",keytype);
    }
    struct cdict_entry_t * entry = cdict_contains(cdict,
                                                  key,
                                                  keytype);

    if(entry != NULL)
    {
        /*
         * Entry exists, append it
         */
        entry = cdict_append_given_entry(cdict,
                                         entry,
                                         keytype,
                                         value,
                                         nvalue,
                                         valuetype,
                                         metadata_free_function);
    }
    else
    {
        /*
         * Set new entry
         */
        entry = cdict_set(cdict,
                          key,
                          keytype,
                          value,
                          nvalue,
                          valuetype,
                          metadata,
                          metadata_free_function);
    }

    return entry;
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        