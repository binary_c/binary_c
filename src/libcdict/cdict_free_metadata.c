

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit



/*
 * Convenience function to free the metadata
 */
CDict_API_function
void cdict_free_metadata(struct cdict_t * const cdict,
                         struct cdict_entry_t * entry)
{
    if(entry && entry->metadata)
    {
        CDict_Safe_free(cdict,
                        entry->metadata);
    }
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        