

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit


#include <stdio.h>

/*
 * Set entry into the cdict, or append if it already exists.
 *
 * Returns a pointer to the entry that is appended or created,
 * or NULL on error (e.g. if the data type is a pointer which
 * cannot be added).
 *
 * This function requires the pre-existing entry be passed,
 * so entry must be non-NULL.
 *
 * If you want to set an entry that may not exist, call
 * cdict_append() instead.
 */

__CDict_Nonnull_some_arguments(1)
CDict_API_function
struct cdict_entry_t * cdict_append_given_entry(
    struct cdict_t * const cdict,
    struct cdict_entry_t * const entry,
    const CDict_key_type keytype,
    const union cdict_value_union value,
    const cdict_size_t nvalue,
    const CDict_value_type valuetype,
    cdict_metadata_free_f metadata_free_function
    )
{
    /*
     * Add entry into cdict, appending if required.
     *
     * TODO : array types
     */
    if(cdict->vb)
    {
        printf("cdict_append_given_entry : call cdict_contains keytype=%d\n",keytype);
    }
    /*
     * Append existing entry
     *
     * This is trivial for numerical types,
     * but we treat chars similarly.
     *
     * Strings are also easy to concatenate.
     *
     * We cannot append cdict pointers: yet.
     */
    if(valuetype == CDICT_DATA_TYPE_DOUBLE)
    {
        entry->value.value.double_data += value.double_data;
    }
    else if(valuetype == CDICT_DATA_TYPE_INT)
    {
        entry->value.value.int_data += value.int_data;
    }
    else if(valuetype == CDICT_DATA_TYPE_STRING)
    {
        /*
         * String type : remember to free the old string
         * if it's in the tofree list.
         */
        char * was = entry->value.value.string_data;
        const int ret = asprintf(&was,
                                 "%s%s",
                                 was,
                                 value.string_data);
        if(ret >= 0)
        {
            cdict_free_from_tofree(cdict,
                                   was);
        }
        else
        {
            cdict_error(cdict,
                        CDICT_ERROR_OUT_OF_MEMORY,
                        "asprintf failed in cdict_append_given_entry().\n");
        }
        CDict_Safe_free(cdict,was);
    }
    /*
     * Scalar types
     */
    else if(valuetype == CDICT_DATA_TYPE_UNSIGNED_INT)
    {
        entry->value.value.unsigned_int_data += value.unsigned_int_data;
    }
    else if(valuetype == CDICT_DATA_TYPE_SHORT_INT)
    {
        entry->value.value.short_int_data += value.short_int_data;
    }
    else if(valuetype == CDICT_DATA_TYPE_UNSIGNED_SHORT_INT)
    {
        entry->value.value.unsigned_short_int_data += value.unsigned_short_int_data;
    }
    else if(valuetype == CDICT_DATA_TYPE_LONG_INT)
    {
        entry->value.value.long_int_data += value.long_int_data;
    }
    else if(valuetype == CDICT_DATA_TYPE_UNSIGNED_LONG_INT)
    {
        entry->value.value.unsigned_long_int_data += value.unsigned_long_int_data;
    }
    else if(valuetype == CDICT_DATA_TYPE_LONG_LONG_INT)
    {
        entry->value.value.long_long_int_data += value.long_long_int_data;
    }
    else if(valuetype == CDICT_DATA_TYPE_UNSIGNED_LONG_LONG_INT)
    {
        entry->value.value.unsigned_long_long_int_data += value.unsigned_long_long_int_data;
    }
    else if(valuetype == CDICT_DATA_TYPE_CHAR)
    {
        entry->value.value.char_data += value.char_data;
    }
    else if(valuetype == CDICT_DATA_TYPE_FLOAT)
    {
        entry->value.value.float_data += value.float_data;
    }
    /*
     * Booleans are ANDed
     */
    else if(valuetype == CDICT_DATA_TYPE_BOOLEAN)
    {
        entry->value.value.Boolean_data &= value.Boolean_data;
    }
    /*
     * Pointer types: map to ptrdiff_t then append, i.e.
     * treat the value as an offset. It's not clear if this
     * really works: it's up to the user to get it right.
     */
    else if(valuetype == CDICT_DATA_TYPE_UNSIGNED_INT_POINTER)
    {
        entry->value.value.unsigned_int_pointer_data += (ptrdiff_t)value.unsigned_int_pointer_data;
    }
    else if(valuetype == CDICT_DATA_TYPE_SHORT_INT_POINTER)
    {
        entry->value.value.short_int_pointer_data += (ptrdiff_t)value.short_int_pointer_data;
    }
    else if(valuetype == CDICT_DATA_TYPE_UNSIGNED_SHORT_INT_POINTER)
    {
        entry->value.value.unsigned_short_int_pointer_data += (ptrdiff_t)value.unsigned_short_int_pointer_data;
    }
    else if(valuetype == CDICT_DATA_TYPE_LONG_INT_POINTER)
    {
        entry->value.value.long_int_pointer_data += (ptrdiff_t)value.long_int_pointer_data;
    }
    else if(valuetype == CDICT_DATA_TYPE_UNSIGNED_LONG_INT_POINTER)
    {
        entry->value.value.unsigned_long_int_pointer_data += (ptrdiff_t)value.unsigned_long_int_pointer_data;
    }
    else if(valuetype == CDICT_DATA_TYPE_LONG_LONG_INT_POINTER)
    {
        entry->value.value.long_long_int_pointer_data += (ptrdiff_t)value.long_long_int_pointer_data;
    }
    else if(valuetype == CDICT_DATA_TYPE_UNSIGNED_LONG_LONG_INT_POINTER)
    {
        entry->value.value.unsigned_long_long_int_pointer_data += (ptrdiff_t)value.unsigned_long_long_int_pointer_data;
    }
    else if(valuetype == CDICT_DATA_TYPE_FLOAT_POINTER)
    {
        entry->value.value.float_pointer_data += (ptrdiff_t)value.float_pointer_data;
    }

    /*
     * Array types: reallocate space for the new data
     */
    else if(__CDict_datatype_is_array(valuetype))
    {
        if(nvalue > 0)
        {
            /*
             * Macro to extend arrays
             */
#undef __extend_array
#define __extend_array(VAR)                                     \
            void * const was = entry->value.value.VAR;          \
            entry->value.value.VAR =                            \
                __CDict_realloc(cdict,                          \
                                entry->value.value.VAR,         \
                                __CDict_sizeof(valuetype) *     \
                                (entry->value.count + nvalue)); \
            if(entry->value.value.VAR != was)                   \
            {                                                   \
                cdict_replace_tofree(cdict,                     \
                                     was,                       \
                                     entry->value.value.VAR);   \
            }                                                   \
            memcpy(entry->value.value.VAR + entry->value.count, \
                   value.VAR,                                   \
                   nvalue * __CDict_sizeof(valuetype));         \
            entry->value.count += nvalue;

            if(valuetype == CDICT_DATA_TYPE_INT_ARRAY)
            {
                __extend_array(int_array_data);
            }
            else if(valuetype == CDICT_DATA_TYPE_UNSIGNED_INT_ARRAY)
            {
                __extend_array(unsigned_int_array_data);
            }
            else if(valuetype == CDICT_DATA_TYPE_SHORT_INT_ARRAY)
            {
                __extend_array(short_int_array_data);
            }
            else if(valuetype == CDICT_DATA_TYPE_UNSIGNED_SHORT_INT_ARRAY)
            {
                __extend_array(unsigned_short_int_array_data);
            }
            else if(valuetype == CDICT_DATA_TYPE_LONG_INT_ARRAY)
            {
                __extend_array(long_int_array_data);
            }
            else if(valuetype == CDICT_DATA_TYPE_UNSIGNED_LONG_INT_ARRAY)
            {
                __extend_array(unsigned_long_int_array_data);
            }
            else if(valuetype == CDICT_DATA_TYPE_LONG_LONG_INT_ARRAY)
            {
                __extend_array(long_long_int_array_data);
            }
            else if(valuetype == CDICT_DATA_TYPE_UNSIGNED_LONG_LONG_INT_ARRAY)
            {
                __extend_array(unsigned_long_long_int_array_data);
            }
            else if(valuetype == CDICT_DATA_TYPE_FLOAT_ARRAY)
            {
                __extend_array(float_array_data);
            }
            else if(valuetype == CDICT_DATA_TYPE_DOUBLE_ARRAY)
            {
                __extend_array(double_array_data);
            }
            else if(valuetype == CDICT_DATA_TYPE_STRING_ARRAY)
            {
                __extend_array(string_array_data);
            }
            else if(valuetype == CDICT_DATA_TYPE_BOOLEAN_ARRAY)
            {
                __extend_array(Boolean_array_data);
            }
            else
            {
                /*
                 * Formally an error
                 */
                return NULL;
            }
        }
    }
    else
    {
        /*
         * Formally an error
         */
        return NULL;
    }

    /*
     * Overwrite the free function
     */
    if(metadata_free_function != NULL)
    {
        if(entry->metadata == NULL)
        {
            entry->metadata = __CDict_malloc(cdict,sizeof(struct cdict_metadata_t));
        }
        entry->metadata->free_function = metadata_free_function;
    }

    return entry;
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        