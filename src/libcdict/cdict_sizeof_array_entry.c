

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit


/*
 * Return the size of dereferenced pointer type,
 * or 0 on error.
 */

#undef X
#define X(                                              \
    TYPE,                                               \
    MEMBER,						\
    CTYPE,						\
    DEFCTYPE,                                           \
    CARRAY,                                             \
    DESCRIPTOR,                                         \
    FORMAT,                                             \
    DEREF_FORMAT,                                       \
    DEREF_CTYPE,                                        \
    GROUP,                                              \
    ARRAY_TO_SCALAR_MAPPER,                             \
    SCALAR_TO_ARRAY_MAPPER,                             \
    NUMERIC,                                            \
    DEMO                                                \
    )                                                   \
    sizeof(DEREF_CTYPE),

size_t __CDict_sizeof_array_entry(const CDict_data_type n)
{
    static const size_t __sizes[] = {
        __CDICT_DATA_TYPES__
    };
    return n < CDICT_DATA_NUMBER_OF_TYPES ? (size_t) __sizes[n] : (size_t)0;
}
#undef X

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        