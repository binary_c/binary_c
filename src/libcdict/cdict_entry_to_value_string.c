

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit


#include <stdio.h>

__CDict_Nonnull_some_arguments(1,2)
CDict_API_function
int cdict_entry_to_value_string(struct cdict_t * const cdict,
                                const struct cdict_entry_t * const entry,
                                char ** const value_string,
                                const char * const format)
{
    /*
     * Set the value_string based on the data in entry.
     *
     * For details, see cdict_value_to_value_string().
     */
    return cdict_value_to_value_string(cdict,
                                       &entry->value,
                                       value_string,
                                       format,
                                       -1);
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        