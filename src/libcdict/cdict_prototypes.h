#include "../binary_c.h"
        #ifndef __HAVE_LIBCDICT__
        
#pragma once
#ifndef CDICT_PROTOTYPES_H
#define CDICT_PROTOTYPES_H


/*
 * prototypes of functions used in libcdict.
 */


/*
 * Allocation and destruction functions
 */
__CDict_malloc_like
CDict_API_function
struct cdict_t * cdict_new(void);

CDict_API_function
void cdict_free(struct cdict_t ** const cdict,
                const Boolean free_value_contents,
                cdict_metadata_free_f metadata_free_function);

/*
 * Cdict setting functions
 */
__CDict_Nonnull_some_arguments(1)
CDict_API_function
struct cdict_entry_t * cdict_set(struct cdict_t * const cdict,
                                 const union cdict_key_union key,
                                 const CDict_key_type keytype,
                                 const union cdict_value_union value,
                                 const cdict_size_t nvalue,
                                 const CDict_value_type valuetype,
                                 const void * const metadata,
                                 cdict_metadata_free_f metadata_free_function);

__CDict_Nonnull_some_arguments(1)
void cdict_set_map(struct cdict_t * const cdict,
                   struct cdict_key_t * const key,
                   const CDict_key_type keytype,
                   const char * const keystring);

__CDict_Nonnull_some_arguments(1)
CDict_API_function
struct cdict_entry_t * cdict_set_with_formats(struct cdict_t * const cdict,
                                              const union cdict_key_union key,
                                              const CDict_key_type keytype,
                                              const char * const keyformat,
                                              const union cdict_value_union value,
                                              const cdict_size_t nvalue,
                                              const CDict_value_type valuetype,
                                              const char * const valueformat,
                                              const void * const metadata,
                                              cdict_metadata_free_f metadata_free_function);



__CDict_Nonnull_some_arguments(1)
CDict_API_function
struct cdict_entry_t * cdict_append(struct cdict_t * const cdict,
                                    const union cdict_key_union key,
                                    const CDict_key_type keytype,
                                    const union cdict_value_union value,
                                    const cdict_size_t nvalue,
                                    const CDict_value_type valuetype,
                                    const void * const metadata,
                                    cdict_metadata_free_f metadata_free_function);


__CDict_Nonnull_some_arguments(1)
CDict_API_function
struct cdict_entry_t * cdict_append_given_entry(
    struct cdict_t * const cdict,
    struct cdict_entry_t * const entry,
    const CDict_key_type keytype,
    const union cdict_value_union value,
    const cdict_size_t nvalue,
    const CDict_value_type valuetype,
    cdict_metadata_free_f metadata_free_function
    );


__CDict_Nonnull_some_arguments(1)
CDict_API_function
void cdict_set_metadata(struct cdict_t * const cdict,
                        struct cdict_entry_t * const entry,
                        const void * const metadata,
                        cdict_metadata_free_f metadata_free_function);


__CDict_Nonnull_some_arguments(1)
CDict_API_function
void * cdict_get_metadata(const struct cdict_entry_t * const entry);
__CDict_Nonnull_some_arguments(1)
cdict_metadata_free_f cdict_get_metadata_function(const struct cdict_entry_t * const entry);


/*
 * Check for entry functions
 */
CDict_API_function
struct cdict_entry_t * cdict_contains(struct cdict_t * const cdict,
                                      const union cdict_key_union key,
                                      const CDict_key_type keytype);

CDict_API_function
struct cdict_entry_t * cdict_contains_any_type(struct cdict_t * const cdict,
                                               struct cdict_key_t * const key);

__CDict_Pure_function
__CDict_Nonnull_all_arguments
CDict_API_function
Boolean cdict_contains_cdicts(const struct cdict_t * const cdict);

/*
 * entry (key or value or both) -> string functions
 */
__CDict_Nonnull_some_arguments(1,2)
CDict_API_function
int cdict_entry_to_key_string(struct cdict_t * const cdict,
                              struct cdict_entry_t * const entry,
                              const char * const format);

__CDict_Nonnull_some_arguments(1,2)
CDict_API_function
int cdict_entry_to_value_string(struct cdict_t * const cdict,
                                const struct cdict_entry_t * const entry,
                                char ** const value_string,
                                const char * const format);

__CDict_Nonnull_some_arguments(1,2)
CDict_API_function
int cdict_entry_to_key_and_value_strings(struct cdict_t * const cdict,
                                         struct cdict_entry_t * const entry,
                                         char ** const value_string);

__CDict_Nonnull_some_arguments(1,2)
CDict_API_function
int cdict_entry_index_to_value_string(struct cdict_t * const cdict,
                                      const struct cdict_entry_t * const entry,
                                      char ** value_string,
                                      const char * const format,
                                      const cdict_size_t index);

CDict_API_function
int cdict_key_and_value_strings(struct cdict_t * const cdict,
                                struct cdict_key_t * const key,
                                struct cdict_value_t * const value,
                                char ** const value_string);

__CDict_Nonnull_some_arguments(1,2)
CDict_API_function
int cdict_set_key_string(struct cdict_t * const cdict,
                         struct cdict_key_t * const key,
                         const char * const format);

__CDict_Nonnull_some_arguments(1,2)
CDict_API_function
int cdict_value_to_value_string(struct cdict_t * const cdict,
                                const struct cdict_value_t * const value,
                                char ** const value_string,
                                const char * const format,
                                const cdict_size_t index);


CDict_API_function
void * cdict_get_union_data_pointer(struct cdict_entry_t * const entry);

CDict_API_function
int cdict_error(const struct cdict_t * const cdict,
                const int error_number,
                const char * const format,
                ...);

#ifdef CDICT_USE_RYU
/*
 * RGI added wrapper functions to behave as asprintf
 * for doubles and floats.
 * Note: these are cdict API functions.
 */
CDict_API_function
int cdict_ryu_d2s_asprintf(struct cdict_t * const cdict,
                           char ** s,
                           const double f);

CDict_API_function
int cdict_ryu_f2s_asprintf(struct cdict_t * const cdict,
                           char ** const s,
                           const float f);
#endif // CDICT_USE_RYU


/*
 * Set key/hashv calculation parameters
 */
CDict_API_function
void cdict_set_numerics(const double abs,
                        const double rel,
                        const size_t nbytes_double,
                        const size_t nbytes_float);

/*
 * JSON outpout
 */
__CDict_Nonnull_some_arguments(1,2,3)
CDict_API_function
int cdict_to_json(struct cdict_t * const cdict,
                  char ** const bufferp,
                  size_t * const buffer_sizep,
                  const unsigned int nesting_level,
                  const char * const item_separator,
                  const char * const key_separator,
                  const Boolean indent,
                  const Boolean newlines,
                  const Boolean sort);

CDict_API_function
void cdict_tests(void);

/*
 * Cdict nesting function.
 */
CDict_API_function
struct cdict_entry_t *
cdict_nest(struct cdict_t * const cdict __CDict_maybe_unused,
           union cdict_key_union * paths __CDict_maybe_unused,
           const size_t npath __CDict_maybe_unused,
           CDict_key_type * pathtypenums,
           union cdict_key_union key __CDict_maybe_unused,
           CDict_key_type keytype __CDict_maybe_unused,
           union cdict_value_union value __CDict_maybe_unused,
           const cdict_size_t nvalue,
           CDict_value_type valuetype __CDict_maybe_unused,
           void * metadata __CDict_maybe_unused,
           cdict_metadata_free_f metadata_free_function __CDict_maybe_unused,
           const CDict_action_type action
    );

/*
 * Get nested cdict data
 */
__CDict_Nonnull_some_arguments(1,2)
CDict_API_function
struct cdict_entry_t *
cdict_nest_get(struct cdict_t * const cdict __CDict_maybe_unused,
               union cdict_key_union * paths __CDict_maybe_unused,
               const size_t npath __CDict_maybe_unused,
               CDict_key_type * pathtypenums __CDict_maybe_unused);

/* union setting functions */
CDict_API_function
union cdict_key_union cdict_union_from_double(double d);
CDict_API_function
union cdict_key_union cdict_union_from_long_double(long double d);
CDict_API_function
union cdict_key_union cdict_union_from_int(int i);
CDict_API_function
union cdict_key_union cdict_union_from_unsigned_int(unsigned int i);
CDict_API_function
union cdict_key_union cdict_union_from_unsigned_short_int(unsigned short int i);
CDict_API_function
union cdict_key_union cdict_union_from_short_int(short int i);
CDict_API_function
union cdict_key_union cdict_union_from_long_int(long int i);
CDict_API_function
union cdict_key_union cdict_union_from_long_long_int(long long int i);
CDict_API_function
union cdict_key_union cdict_union_from_char(char s);
CDict_API_function
union cdict_key_union cdict_union_from_float(float f);
CDict_API_function
union cdict_key_union cdict_union_from_double_pointer(double * d);
CDict_API_function
union cdict_key_union cdict_union_from_long_double_pointer(long double * d);
CDict_API_function
union cdict_key_union cdict_union_from_int_pointer(int * i);
CDict_API_function
union cdict_key_union cdict_union_from_unsigned_int_pointer(unsigned int * i);
CDict_API_function
union cdict_key_union cdict_union_from_short_int_pointer(short int * i);
CDict_API_function
union cdict_key_union cdict_union_from_unsigned_short_int_pointer(unsigned short int * i);
CDict_API_function
union cdict_key_union cdict_union_from_void_pointer(void * p);
CDict_API_function
union cdict_key_union cdict_union_from_string(char * s);
CDict_API_function
union cdict_key_union cdict_union_from_float_pointer(float * f);
CDict_API_function
union cdict_key_union cdict_union_from_Boolean(Boolean b);
CDict_API_function
union cdict_key_union cdict_union_from_Boolean_pointer(Boolean * b);
CDict_API_function
union cdict_key_union cdict_union_from_double_array(double * d);
CDict_API_function
union cdict_key_union cdict_union_from_long_double_array(long double * d);
CDict_API_function
union cdict_key_union cdict_union_from_int_array(int * i);
CDict_API_function
union cdict_key_union cdict_union_from_unsigned_int_array(unsigned int * i);
CDict_API_function
union cdict_key_union cdict_union_from_char_array(char * s);
CDict_API_function
union cdict_key_union cdict_union_from_float_array(float * f);
CDict_API_function
union cdict_key_union cdict_union_from_Boolean_array(Boolean * b);
CDict_API_function
union cdict_key_union cdict_union_from_string_array(char ** c_array);
CDict_API_function
union cdict_key_union cdict_union_from_void_pointer_array(void * v);
CDict_API_function
union cdict_key_union cdict_union_from_cdict_pointer(struct cdict_t * cdict);
CDict_API_function
union cdict_key_union cdict_union_from_unknown(float f);
CDict_API_function
union cdict_key_union cdict_union_from_unsigned_long_int(unsigned long int i);
CDict_API_function
union cdict_key_union cdict_union_from_unsigned_long_long_int(unsigned long long int i);
CDict_API_function
union cdict_key_union cdict_union_from_long_int(long int i);
CDict_API_function
union cdict_key_union cdict_union_from_long_int_pointer(long int * i);
CDict_API_function
union cdict_key_union cdict_union_from_long_long_int_pointer(long long int * i);
CDict_API_function
union cdict_key_union cdict_union_from_unsigned_long_int_pointer(unsigned long int * i);
union cdict_key_union cdict_union_from_unsigned_long_long_int_pointer(unsigned long long int * i);

CDict_API_function
int cdict_stats(struct cdict_t * const cdict,
                const char * const prepend_string,
                char ** outstring);

CDict_API_function
void cdict_free_metadata(struct cdict_t * const cdict,
                         struct cdict_entry_t * entry);


CDict_API_function
void cdict_free_tofree(struct cdict_t * const cdict);

CDict_API_function
void cdict_push_tofree(struct cdict_t * const cdict,
                       void * p);

CDict_API_function
void cdict_replace_tofree(struct cdict_t * const cdict,
                          void * was,
                          void * is);

CDict_API_function
void cdict_free_from_tofree(struct cdict_t * const cdict,
                            void * p);

__CDict_Nonnull_some_arguments(1,3)
CDict_API_function
void cdict_json_to(const char * const json_buffer,
                   const size_t buflen,
                   struct cdict_t * const cdict);


__CDict_Nonnull_some_arguments(1,2)
CDict_API_function
void cdict_load_json(struct cdict_t * const cdict,
                     const char * const filename);

__CDict_Nonnull_some_arguments(1,2,3,4)
CDict_API_function
void cdict_to_json_file(struct cdict_t * cdict,
                        const char * const filename,
                        const char * const item_separator,
                        const char * const key_separator,
                        const Boolean indent,
                        const Boolean newlines,
                        const Boolean sort);

CDict_API_function
void cdict_clean_cdict(struct cdict_t * h);

CDict_API_function
cdict_size_t cdict_copy(struct cdict_t * const src,
                        struct cdict_t * const dest);

__CDict_Nonnull_some_arguments(1,2)
CDict_API_function
struct cdict_entry_t ** cdict_grep(struct cdict_t * const cdict,
                                   Boolean(*grep_function)(struct cdict_entry_t * entry),
                                   cdict_size_t * nmatches);

CDict_API_function
void cdict_delete_entry(struct cdict_t * const cdict,
                        struct cdict_entry_t ** entryp,
                        const Boolean free_value_contents);

/************************************************************
 * The following functions are NOT part of the API, so are
 * NOT publicly available in the libcdict shared library.
 ************************************************************/


/*
 * Floating point truncation functions
 */
__CDict_Pure_function
float cdict_truncate_float(const float d,
                           const size_t nbytes);
__CDict_Pure_function
double cdict_truncate_double(const double d,
                             const size_t nbytes);
__CDict_Pure_function
long double cdict_truncate_long_double(const long double d,
                                       const size_t nbytes);

/*
 * Sorting functions
 */
__CDict_Pure_function
__CDict_Nonnull_all_arguments
int cdict_sort_auto(struct cdict_t * const cdict,
                    struct cdict_key_t * const a,
                    struct cdict_key_t * const b);

__CDict_Pure_function
__CDict_Nonnull_all_arguments
int cdict_sort_auto_reverse(struct cdict_t * const cdict,
                            struct cdict_key_t * const a,
                            struct cdict_key_t * const b);

__CDict_Pure_function
__CDict_Nonnull_all_arguments
int cdict_keycmp_auto(struct cdict_t * const cdict,
                      struct cdict_key_t * const a,
                      struct cdict_key_t * const b,
                      const size_t len);

__CDict_Pure_function
int cdict_cf_doubles(const double x,
                     const double y);

__CDict_Pure_function
int cdict_cf_long_doubles(const long double x,
                          const long double y);

__CDict_Pure_function
__CDict_Nonnull_all_arguments
int cdict_sort_by_entry_auto(struct cdict_t * const cdict,
                             struct cdict_entry_t * const a,
                             struct cdict_entry_t * const b);

__CDict_Pure_function
__CDict_Nonnull_all_arguments
int cdict_sort_by_entry_auto_reverse(struct cdict_t * const cdict,
                                     struct cdict_entry_t * const a,
                                     struct cdict_entry_t * const b);



struct cdict_key_t * cdict_new_key(struct cdict_t * const cdict,
                                   const union cdict_key_union keydata,
                                   const CDict_key_type keytype);

int cdict_union_to_string(struct cdict_t * const cdict,
                          const union cdict_key_union * const key_union,
                          const CDict_key_type key_type,
                          const char * const format,
                          char ** target);


void cdict_fill_key(struct cdict_key_t * const key,
                    struct cdict_t * const cdict,
                    const union cdict_key_union keydata,
                    const CDict_key_type keytype,
                    Boolean do_map);
#ifdef CDICT_USE_RYU
char * cdict_ryu_d2s(struct cdict_t * const cdict,
                     double f);
char * cdict_ryu_f2s(struct cdict_t * const cdict,
                     float f);
char * cdict_ryu_d2fixed(struct cdict_t * const cdict,
                         double d,
                         uint32_t precision);
char * cdict_ryu_d2exp(struct cdict_t * const cdict,
                       double d,
                       uint32_t precision);

#endif // CDICT_USE_RYU


void * cdict_malloc_function(struct cdict_t * const cdict,
                             size_t size);
void *cdict_realloc_function(struct cdict_t * const cdict,
                             void * ptr,
                             size_t size);
void * cdict_calloc_function(struct cdict_t * const cdict,
                             size_t nmemb,
                             size_t size);

CDict_API_function
void cdict_free_alloc_function(struct cdict_t * const cdict,
                               void * const ptr);
CDict_API_function
int cdict_asprintf(struct cdict_t * const cdict,
                   char ** const strp,
                   const char * fmt,
                   ...);
void cdict_free_key(struct cdict_t * const cdict,
                    struct cdict_key_t ** const keyp,
                    const Boolean free_key);

size_t cdict_sizeof_pointer(struct cdict_t * const cdict,
                            const void * const ptr);



void cdict_alloc_stats(struct cdict_t * const cdict);

size_t cdict_total_alloced(struct cdict_t * const cdict,
                           unsigned int mode);

void cdict_free_stats(struct cdict_t * const cdict);

char * cdict_fast_double_parser(const char * p,
                                double * const outDouble);
double cdict_fast_strtod(const char * nptr,
                         char ** const endptr);

char * __CDict_descriptor(const CDict_data_type n);
size_t __CDict_sizeof(const CDict_data_type n);
char * __CDict_generic_format_string(const CDict_data_type n);
char * __CDict_generic_deref_format_string(const CDict_data_type n);
size_t __CDict_group(const CDict_data_type n);
size_t __CDict_sizeof_deref(const CDict_data_type n);
size_t __CDict_sizeof_array_entry(const CDict_data_type n);
CDict_data_type __CDict_map_array_type_to_scalar_type(const CDict_data_type n);
CDict_data_type __CDict_map_scalar_type_to_array_type(const CDict_data_type n);
CDict_numeric_type __CDict_numeric(const CDict_data_type n);

void cdict_free_entry_contents(struct cdict_t * const cdict,
                               struct cdict_t * const tofree_cdict,
                               struct cdict_entry_t ** const entryp,
                               cdict_metadata_free_f free_function,
                               const Boolean free_value_contents);

__CDict_Nonnull_some_arguments(1,2,4)
int cdict_make_string_from_var(struct cdict_t * const cdict,
                               char ** const string,
                               const char * const format,
                               const void * const data,
                               const CDict_data_type type,
                               const cdict_size_t index
    );
void * cdict_union_to_pointer(const union cdict_value_union * const value_union,
                              const CDict_value_type value_type);

struct cdict_entry_stack_t * cdict_new_entry_stack(const size_t nstack);
void cdict_push_entry_stack(struct cdict_entry_stack_t * const stack,
                            struct cdict_entry_t * const entry,
                            const Boolean entered);
struct cdict_entry_stack_item_t * cdict_replace_top_of_entry_stack(struct cdict_entry_stack_t * const stack,
                                                                   struct cdict_entry_t * const entry,
                                                                   const Boolean entered);
struct cdict_entry_stack_item_t * cdict_top_of_entry_stack(struct cdict_entry_stack_t * const stack);
void cdict_pop_entry_stack(struct cdict_entry_stack_t * const stack);
void cdict_print_entry_stack(struct cdict_entry_stack_t * const stack);

__CDict_Nonnull_some_arguments(2)
CDict_API_function
struct cdict_entry_t * cdict_iter(struct cdict_t * const cdict,
                                  struct cdict_iterator_t ** const iteratorp,
                                  const Boolean sort);

__CDict_Nonnull_some_arguments(1)
CDict_API_function
void cdict_set_pre_output(struct cdict_t * const cdict,
                          struct cdict_entry_t * const entry,
                          const void * const pre_output_data,
                          cdict_pre_output_f pre_output_function);

__CDict_Nonnull_some_arguments(1)
CDict_API_function
void cdict_alloc_stats_struct(struct cdict_t * const cdict);


void cdict_push_pointer(struct cdict_t * const cdict,
                        void * const ptr,
                        const size_t size,
                        unsigned int mode);
void cdict_del_pointer(struct cdict_t * const cdict,
                       void * const ptr);

size_t cdict_bsd_strlcat(char *dst,
                         const char *src,
                         size_t dsize);
#endif // CDICT_PROTOTYPES_H

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
       