

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit

void * cdict_union_to_pointer(const union cdict_value_union * const value_union,
                              const CDict_value_type value_type)
{
    /*
     * Convert value_union to a void pointer, and
     * return the void pointer. If value_type is not a pointer
     * returns NULL.
     *
     * Note: we could *probably* do this by assuming all
     * pointers are like void* pointers, and just return that.
     *
     * However, that might well break C's union rules. So instead
     * match the type and return that.
     *
     * Sorry about the void** hack to get the data: we have to
     * cast to the pointer of the data available, then dereference
     * this, otherwise the compiler complains.
     */
#undef X
#define X(TYPE,                                                         \
          MEMBER,                                                       \
          CTYPE,                                                        \
          DEFCTYPE,                                                     \
          CARRAY,                                                       \
          DESCRIPTOR,                                                   \
          FORMAT,                                                       \
          DEREF_FORMAT,                                                 \
          DEREF_CTYPE,                                                  \
          GROUP,                                                        \
          ARRAY_TO_SCALAR_MAPPER,                                       \
          SCALAR_TO_ARRAY_MAPPER,                                       \
          NUMERIC,                                                      \
          DEMO)                                                         \
    {                                                                   \
        void **v = (void **) & value_union->MEMBER##_data;              \
        if(CDICT_GROUP_##GROUP == CDICT_GROUP_POINTER &&                \
           CDICT_DATA_TYPE_##TYPE == value_type)                        \
        {                                                               \
            return *v;                                                  \
        }                                                               \
    }
    __CDICT_DATA_TYPES__;
#undef X
    return NULL;
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        