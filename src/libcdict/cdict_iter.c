

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit


#include <stdio.h>

//#define cprint(...) printf(__VA_ARGS__);
#define cprint(...) /* do nothing */


/*
 * Cdict iterator. Loops over each (nested) entry
 * in a cdict in turn.
 */

__CDict_Nonnull_some_arguments(2)
CDict_API_function
struct cdict_entry_t * cdict_iter(struct cdict_t * const cdict,
                                  struct cdict_iterator_t ** const iteratorp,
                                  const Boolean sort)
{
    struct cdict_iterator_t * cdict_iterator;

    if(cdict == NULL)
    {
        /* clean up iterator */
        cdict_iterator = *iteratorp;
        for(size_t i=0; i<cdict_iterator->stack->nstack; i++)
        {
            free(cdict_iterator->stack->items[i]);
        }
        free(cdict_iterator->stack->items);
        free(cdict_iterator->stack);
        free(cdict_iterator);
        *iteratorp = NULL;
        return NULL;
    }

    if(sort)
    {
        CDict_sort(cdict);
    }

    if(*iteratorp == NULL)
    {
        /*
         * Set up iterator
         */
        cdict_iterator = calloc(1,sizeof(struct cdict_iterator_t));
        cdict_iterator->stack = cdict_new_entry_stack(0);
        *iteratorp = cdict_iterator;
        struct cdict_entry_t * entry = cdict->cdict_entry_list;
        const Boolean entered = FALSE;
        cdict_push_entry_stack(cdict_iterator->stack,
                               entry,
                               entered);
    }
    else
    {
        cdict_iterator = *iteratorp;
        struct cdict_entry_stack_item_t * const top = cdict_top_of_entry_stack(cdict_iterator->stack);
        cdict_replace_top_of_entry_stack(cdict_iterator->stack,
                                         top->entry->rc_hh.next,
                                         FALSE);
        cprint("new top = %p\n",
               (void*)cdict_top_of_entry_stack(cdict_iterator->stack));
    }

    /*
     * First, move as far in as we can
     */
    while(TRUE)
    {
        cdict_print_entry_stack(cdict_iterator->stack);
        struct cdict_entry_stack_item_t * const top = cdict_top_of_entry_stack(cdict_iterator->stack);
        if(top && top->entry)
        {
            if(CDict_entry_is_hash(top->entry))
            {
                if(top->entered == FALSE && sort)
                {
                    /* first time: perhaps sort? */
                    CDict_sort(top->entry->value.value.cdict_pointer_data);
                }

                struct cdict_entry_t * next = top->entry->value.value.cdict_pointer_data->cdict_entry_list;
                if(top->entered == FALSE)
                {
                    top->entered = TRUE;
                    cdict_push_entry_stack(cdict_iterator->stack,
                                           next,
                                           FALSE);
                }
                else
                {
                    next = top->entry->rc_hh.next;
                    cprint("Entered: choose %p -> next = %p\n",
                           (void*)next,
                           (void*)next);
                    cdict_replace_top_of_entry_stack(cdict_iterator->stack,
                                                     next,
                                                     FALSE);
                }
            }
            else
            {
                cprint("SCALAR\n");
                struct cdict_entry_t * entry = top->entry;
                cprint("return %p\n",(void*)entry);
                return entry;
            }
        }
        else
        {
            cprint("No top : pop and reverse!\n");
            cdict_pop_entry_stack(cdict_iterator->stack);

            if(cdict_iterator->stack->nstack == 0)
            {
                /*
                 * The end, return NULL
                 */
                return NULL;
            }
        }
    }

    return NULL;
}

struct cdict_entry_stack_t * cdict_new_entry_stack(const size_t nstack)
{
    /*
     * Make a new stack
     */
    struct cdict_entry_stack_t * const stack = calloc(1,sizeof(struct cdict_entry_stack_t));
    stack->items = calloc(nstack,sizeof(struct cdict_entry_stack_item_t*));
    stack->nstack = nstack;
    return stack;
}

void cdict_push_entry_stack(struct cdict_entry_stack_t * const stack,
                            struct cdict_entry_t * const entry,
                            const Boolean entered)
{
    /*
     * Push an entry onto the stack
     */
    struct cdict_entry_stack_item_t ** p =
        realloc(stack->items,
                sizeof(struct cdict_entry_stack_item_t*)*(stack->nstack+1));
    if(p)
    {
        stack->items = p;
        stack->items[stack->nstack] = malloc(sizeof(struct cdict_entry_t));
        stack->items[stack->nstack]->entry = entry;
        stack->items[stack->nstack]->entered = entered;
        stack->nstack++;
    }
}

struct cdict_entry_stack_item_t * cdict_replace_top_of_entry_stack(struct cdict_entry_stack_t * const stack,
                                                                   struct cdict_entry_t * const entry,
                                                                   const Boolean entered)
{
    /*
     * Replace the top entry in the stack with an alternative
     * entry.
     */
    if(stack->nstack == 0)
    {
        return NULL;
    }
    else
    {
        stack->items[stack->nstack-1]->entry = entry;
        stack->items[stack->nstack-1]->entered = entered;
        return stack->items[stack->nstack-1];
    }
}


struct cdict_entry_stack_item_t * cdict_top_of_entry_stack(struct cdict_entry_stack_t * const stack)
{
    /*
     * Return the top entry in the stack
     */
    if(stack->nstack == 0)
    {
        return NULL;
    }
    else
    {
        return stack->items[stack->nstack-1];
    }
}

void cdict_pop_entry_stack(struct cdict_entry_stack_t * const stack)
{
    /*
     * Remove the top entry in the stack
     */
    if(stack->nstack != 0)
    {
        struct cdict_entry_stack_item_t * const item = cdict_top_of_entry_stack(stack);
        free(item);
        stack->nstack--;
        struct cdict_entry_stack_item_t ** p =
            realloc(stack->items,
                    sizeof(struct cdict_entry_stack_item_t*)*(stack->nstack+1));
        if(p)
        {
            stack->items = p;
        }
    }
}


void cdict_print_entry_stack(struct cdict_entry_stack_t * const stack CDict_maybe_unused)
{
#if (cprint+0)
    cprint("STACK size %zu\n",
           stack->nstack);

    for(size_t i=0; i<stack->nstack; i++)
    {
        struct cdict_entry_t * entry = stack->items[i] ? stack->items[i]->entry : NULL;
        cprint(" ...  %zu : %p \"%s\" entered? %d\n",
               i,
               (void*)entry,
               entry ? entry->key.string : NULL,
               entry ? stack->items[i]->entered : -1);
    }
#endif
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        