

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit


CDict_API_function
void cdict_delete_entry(struct cdict_t * const cdict,
                        struct cdict_entry_t ** entryp,
                        const Boolean free_value_contents)
{
    /*
     * Delete an entry at *entryp in a cdict now,
     * optionally deleteing the contents of its value and key
     *
     * Do nothing if either of entryp or *entryp are NULL.
     */
    if(entryp==NULL || *entryp==NULL) return;
    cdict_free_entry_contents(cdict,
                              NULL,
                              entryp,
                              NULL,
                              free_value_contents);
    CDict_del(cdict,*entryp);
    CDict_Safe_free(cdict,*entryp);
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        