

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit


/*
 * Grep a cdict: we provide a cdict and a function,
 * and return a list of entries.
 *
 * We check each entry in cdict, and When the function
 * is TRUE for the entry, the entry pointer is added to
 * a list. This list is returned and must be freed manually.
 */

__CDict_Nonnull_some_arguments(1,2)
CDict_API_function
struct cdict_entry_t ** cdict_grep(struct cdict_t * const cdict,
                                   Boolean(*grep_function)(struct cdict_entry_t * entry),
                                   cdict_size_t * nmatches)
{
    struct cdict_entry_t ** return_list = NULL;
    *nmatches = 0;
    size_t alloced = 0;
    CDict_loop(cdict,entry)
    {
        if(grep_function(entry) == TRUE)
        {
            /*
             * Match: push to list
             */
            (*nmatches)++;
            if((size_t)*nmatches > alloced)
            {
                /*
                 * Need a longer list : double it to
                 * avoid many repeated realloc calls
                 */
                alloced = alloced == 0 ? 1 : (2 * alloced);
                return_list = cdict_realloc_function(cdict,
                                                     return_list,
                                                     alloced * sizeof(struct cdict_entry_t *));
            }
            return_list[*nmatches-1] = entry;
        }
    }
    if(*nmatches > 0)
    {
        /*
         * Shrink memory allocated to what's required
         */
        return_list = cdict_realloc_function(cdict,
                                             return_list,
                                             (*nmatches) * sizeof(struct cdict_entry_t *));
    }
    return return_list;
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        