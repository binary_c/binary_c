

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit


/*
 * Free up the memory associated with a cdict and,
 * optionally, free its contents as well (if they
 * are pointers).
 */

CDict_API_function
void cdict_free(struct cdict_t ** const cdictp,
                const Boolean free_value_contents,
                cdict_metadata_free_f free_function_for_metadata)
{
    if(cdictp == NULL || *cdictp == NULL) return;

    struct cdict_t * cdict = * cdictp;
    const Boolean vb = cdict->vb;
    if(vb)printf("cdict_free (entrance) : %p -> %p : free-Function_override %p, free_value_contents? %d\n",
                 (void*)cdictp,
                 (void*)cdict,
                 __CDict_cast_function_pointer(free_function_for_metadata),
                 free_value_contents);
    CDict_new(tofree_cdict);
    tofree_cdict->vb = (*cdictp)->vb;
    tofree_cdict->use_cache = FALSE;
    CDict_loop(cdict,entry)
    {
        if(entry)
        {
            /* free the contents of the entry */
            cdict_free_entry_contents(cdict,
                                      tofree_cdict,
                                      &entry,
                                      /* use the free function passed in or the entry's free function */
                                      free_function_for_metadata != NULL ? free_function_for_metadata :
                                      entry->metadata != NULL ? entry->metadata->free_function :
                                      NULL,
                                      free_value_contents);

            /* free the cdict entry from the cdict */
            CDict_del(cdict,entry);

            /* and free its memory */
            CDict_Safe_free(cdict,entry);
        }
    }


    /*
     * Free metadata: we stored this in a cdict so it's
     * all freed at the end and there are no double frees.
     */
    CDict_loop(tofree_cdict,entry)
    {
        /*
         * The data location is the entry's key:
         * this is just a label.
         *
         * The tofree struct is the entry's value
         */
        struct cdict_tofree_t * tofree = (struct cdict_tofree_t *)entry->value.value.void_pointer_data;
        if(tofree->metadata != NULL)
        {
            entry->metadata = malloc(sizeof(struct cdict_metadata_t));
            entry->metadata->data = tofree->metadata;
            entry->metadata->free_function = tofree->metadata_free_function;
        }
        if(tofree->metadata_free_function)
        {
            tofree->metadata_free_function(cdict,entry);
        }
        CDict_Safe_free(tofree_cdict,entry->metadata);
        CDict_del(tofree_cdict,entry);
        if(entry->metadata != NULL)
        {
            CDict_Safe_free(cdict,entry->metadata->data);
            CDict_Safe_free(cdict,entry->metadata);
        }
        CDict_Safe_free(cdict,entry);
        CDict_Safe_free(cdict,tofree);
    }


    /* final free of the cdict(es) : everything should be NULL now */
    if(vb)printf("Safe_free cdict at %p\n",
                 (void*)cdict);
    if(cdict->stats != NULL)
    {
        cdict_free_stats(cdict);
    }

    CDict_free_to_free_list(cdict);
    CDict_Safe_free(cdict,cdict);
    CDict_Safe_free(tofree_cdict,tofree_cdict);

    if(vb)
        printf("cdict_free : (exit) %p\n",
               (void*)cdict);
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        