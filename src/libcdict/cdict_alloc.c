

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit


#include <stdio.h>

#include <stdarg.h>


/*
 * Wrappers for malloc, calloc, realloc, free used in libcdict
 */
static struct cdict_pointer_list_item_t * cdict_lookup_pointer(
    struct cdict_t * const cdict,
    const void * const ptr,
    size_t * index);

void * cdict_malloc_function(struct cdict_t * const cdict,
                             size_t size)
{
    void * const p = malloc(size);
    if(p &&
       cdict != NULL &&
       cdict->ancestor != NULL &&
       cdict->ancestor->stats != NULL)
    {
        cdict->ancestor->stats->malloc_count++;
        cdict->ancestor->stats->malloc_size += size;
        cdict_push_pointer(cdict,p,size,CDICT_MODE_MALLOC);
    }
    return p;
}

void * cdict_realloc_function(struct cdict_t * const cdict,
                              void * ptr,
                              size_t size)
{
    void * const p = realloc(ptr,size);

    if(cdict != NULL &&
       cdict->ancestor != NULL &&
       cdict->ancestor->stats != NULL)
    {
        cdict->ancestor->stats->realloc_count++;
        cdict->ancestor->stats->realloc_size += size;
        cdict_push_pointer(cdict,p,size,CDICT_MODE_REALLOC);
    }

    return p;
}

void * cdict_calloc_function(struct cdict_t * const cdict,
                             size_t nmemb,
                             size_t size)
{
    void * p = calloc(nmemb,size);
    if(p &&
       cdict != NULL &&
       cdict->ancestor != NULL &&
       cdict->ancestor->stats != NULL)
    {
        cdict->ancestor->stats->calloc_count++;
        cdict->ancestor->stats->calloc_size += size*nmemb;
        cdict_push_pointer(cdict,
                           p,
                           size,
                           CDICT_MODE_CALLOC);
    }
    return p;
}

CDict_API_function
void cdict_free_alloc_function(struct cdict_t * const cdict,
                               void * const ptr)
{
    if(ptr != NULL)
    {
        if(cdict != NULL &&
           cdict->ancestor != NULL &&
           cdict->ancestor->stats != NULL &&
           cdict != ptr)
        {
            cdict->ancestor->stats->free_count++;
            cdict_del_pointer(cdict,ptr);
        }
        if(ptr != NULL)
        {
            free(ptr);
        }
    }
    else if(cdict != NULL &&
            cdict->ancestor != NULL &&
            cdict->ancestor->stats != NULL)
    {
        cdict->ancestor->stats->free_null_count++;
    }
}


static struct cdict_pointer_list_item_t *
cdict_lookup_pointer(struct cdict_t * const cdict,
                     const void * const ptr,
                     size_t * index)
{
    if(cdict != NULL &&
       cdict->ancestor != NULL &&
       cdict->ancestor->stats != NULL)
    {
        for(size_t i=0; i<cdict->ancestor->stats->pointer_list_n; i++)
        {
            if(cdict->ancestor->stats->pointer_list[i] != NULL &&
               cdict->ancestor->stats->pointer_list[i]->pointer == ptr)
            {
                /* return a pointer to the list item */
                *index = i;
                return *(cdict->ancestor->stats->pointer_list + i);
            }
        }
    }
    *index = 0;
    return NULL;
}

void cdict_del_pointer(struct cdict_t * const cdict,
                       void * const ptr)
{
    struct cdict_pointer_list_item_t * l = NULL;
    if(cdict != NULL &&
       cdict->ancestor != NULL &&
       ptr != NULL)
    {
        size_t i;
        l = cdict_lookup_pointer(cdict,ptr,&i);
        if(l != NULL)
        {
            free(l);
            l = NULL;
            cdict->ancestor->stats->pointer_list[i] = NULL;
            cdict->ancestor->stats->pointer_list_full = FALSE;
        }
    }
}

void cdict_push_pointer(struct cdict_t * const cdict,
                        void * const ptr,
                        const size_t size,
                        unsigned int mode)
{
    /*
     * Push a pointer onto the list or, in the case of realloc
     * of an existing pointer, change its size
     */
    struct cdict_pointer_list_item_t * l = NULL;

    if(cdict != NULL &&
       cdict->ancestor != NULL)
    {
        if(mode == CDICT_MODE_REALLOC &&
           ptr != NULL)
        {
            size_t i = 0;
            l = cdict_lookup_pointer(cdict,ptr,&i);
        }

        if(l == NULL)
        {
            /* nothing found that matches */

            /* look for a gap */
            if(cdict->ancestor->stats->pointer_list_full == FALSE &&
               cdict->ancestor->stats->pointer_list_n > 0)
            {
                for(size_t i=0; i<cdict->ancestor->stats->pointer_list_n; i++)
                {
                    if(*(cdict->ancestor->stats->pointer_list + i) == NULL)
                    {
                        /* found a gap in the list, set l to point to it */
                        l = *(cdict->ancestor->stats->pointer_list + i) =
                            malloc(
                                sizeof(struct cdict_pointer_list_item_t
                                    ));
                        break;
                    }
                }
            }

            if(l == NULL)
            {
                /* nothing found : extend the list */
                cdict->ancestor->stats->pointer_list_full = TRUE;

                /* add a new pointer in the list */
                cdict->ancestor->stats->pointer_list =
                    realloc(cdict->ancestor->stats->pointer_list,
                            sizeof(struct cdict_pointer_list_item_t *) *
                            (1 + cdict->ancestor->stats->pointer_list_n));

                /* set the pointer to a new item */
                *(cdict->ancestor->stats->pointer_list +
                  cdict->ancestor->stats->pointer_list_n) = malloc(
                      sizeof(struct cdict_pointer_list_item_t
                          ));

                /* hence set l to point to this */
                l = *(cdict->ancestor->stats->pointer_list +
                      cdict->ancestor->stats->pointer_list_n);
                cdict->ancestor->stats->pointer_list_n++;
            }
        }

        if(l != NULL)
        {
            /* set data in the list item */
            l->pointer = ptr;
            l->size = size;
            l->mode = mode;
        }
    }
}

size_t cdict_sizeof_pointer(struct cdict_t * const cdict,
                            const void * const ptr)
{
    size_t index;
    struct cdict_pointer_list_item_t * const item =
        cdict_lookup_pointer(cdict,ptr,&index);
    return item != NULL ? item->size : 0;
}

void cdict_alloc_stats(struct cdict_t * const cdict)
{
    if(cdict != NULL &&
       cdict->ancestor != NULL &&
       cdict->ancestor->stats != NULL)
    {
        struct cdict_t * const a = cdict->ancestor;
        size_t total_size = cdict_total_alloced(cdict,
                                                CDICT_MODE_ALL);
        printf("Cdict has %zu allocated pointers pointing to %zu bytes\n",
               a->stats->pointer_list_n,
               total_size);
    }
}

size_t cdict_total_alloced(struct cdict_t * const cdict,
                           unsigned int mode)
{
    size_t total = 0;
    /*
    printf("total alloced : cdict %p ancestor %p pointer_line %p list_n %zu\n",
                (void*)cdict,
                (void*)(cdict ? cdict->ancestor : NULL),
                (void*)((cdict && cdict->ancestor) ? cdict->ancestor->stats->pointer_list : NULL),
                (cdict && cdict->ancestor) ? cdict->ancestor->stats->pointer_list_n : 0
        );
    */

    if(cdict != NULL &&
       cdict->ancestor != NULL &&
       cdict->ancestor->stats->pointer_list != NULL &&
       cdict->ancestor->stats->pointer_list_n > 0 )
    {
        for(size_t i=0; i<cdict->ancestor->stats->pointer_list_n; i++)
        {
            if(cdict->ancestor->stats->pointer_list[i] != NULL &&
               cdict->ancestor->stats->pointer_list[i]->pointer != NULL &&
               (mode == CDICT_MODE_ALL ||
                cdict->ancestor->stats->pointer_list[i]->mode == mode))
            {
                total += cdict->ancestor->stats->pointer_list[i]->size;
            }
        }
    }
    return total;
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        