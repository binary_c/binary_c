#include "../binary_c.h"
        #ifndef __HAVE_LIBCDICT__
        
#pragma once
#ifndef CDICT_API_H
#define CDICT_API_H
#include "cdict_array_setting.h"
/*
 * libcdict macro API
 *
 * Note: we use function macros to wrap access to libcdict's
 * functions. This allows faster, easier and more flexible
 * use of libcdict.
 *
 * Function macros which are to be accessible to
 * the outside world are of the form:
 *     CDict_...(args)
 *
 *
 * Function macros which are internal are of the form
 *     __CDict_...(args)
 * and may be here or cdict_macros.h (whichever is
 * most appropriate).
 *
 * Constants are upper case macros of the form
 *     CDICT_...
 * and should be in cdict_macros.h, not here.
 */


/*
 * Macro to concatenate three arguments
 */
#undef Concat3
#define Concat3(A,B,C) A##B##C


/*
 * CDict_new(CDICT [,PARENT] ) : make a new cdict called
 * CDICT, declaring it as a variable.
 *
 * If your variable is already declared as CDICT use
 * CDict_new_from(CDICT [,PARENT]) instead.
 *
 * Use CDict_renew instead of CDict_new if your
 * cdict variable already exists but remember to CDict_free
 * it first.
 */


/*
 * 1 argument :
 * Make a new cdict, CDICT.
 */
#define __CDict_new_1(CDICT)                    \
    struct cdict_t * __CDict_renew_1(CDICT);

#define __CDict_renew_1(CDICT)                  \
    (CDICT) = cdict_new();                      \
    (CDICT)->ancestor = (CDICT);

/*
 * 2 arguments :
 * Make a new cdict, CDICT, with parent, PARENT,
 * and set the ancestor to be the PARENT's ancestor
 * so we can trace right up the tree.
 */
#define __CDict_new_2(CDICT,PARENT)                     \
    struct cdict_t * __CDict_renew_2((CDICT),(PARENT))

#define __CDict_renew_2(CDICT,PARENT)                       \
    (CDICT) = cdict_new();                                  \
    if((PARENT)!=NULL)                                      \
    {                                                       \
        (CDICT)->parent = (PARENT);                         \
        (CDICT)->ancestor = (PARENT)->ancestor;             \
        (CDICT)->error_handler = (PARENT)->error_handler;   \
        (CDICT)->error_data = (PARENT)->error_data;         \
        (CDICT)->use_cache = (PARENT)->use_cache;           \
        (CDICT)->force_maps = (PARENT)->force_maps;         \
    }

#define __CDict_new_paste(a,b) __CDict_paste(a,b)
#define CDict_new(...)                          \
    __CDict_new_paste(                          \
        __CDict_new_,                           \
        CDICT_NARGS(__VA_ARGS__))(__VA_ARGS__)

#define __CDict_renew_paste(a,b) __CDict_paste(a,b)
#define CDict_renew(...)                        \
    __CDict_renew_paste(                        \
        __CDict_renew_,                         \
        CDICT_NARGS(__VA_ARGS__))(__VA_ARGS__)

/*
 * CDict_new_from is like CDict_new but relies on a
 * pointer passed in
 */
#define __CDict_new_from_1(CDICT)               \
    (CDICT) = cdict_new();                      \
    (CDICT)->ancestor = (CDICT);
#define __CDict_new_from_2(CDICT,PARENT)                    \
    (CDICT) = cdict_new();                                  \
    if((PARENT)!=NULL)                                      \
    {                                                       \
        (CDICT)->parent = (PARENT);                         \
        (CDICT)->ancestor = (PARENT)->ancestor;             \
        (CDICT)->error_handler = (PARENT)->error_handler;   \
        (CDICT)->error_data = (PARENT)->error_data;         \
        (CDICT)->use_cache = (PARENT)->use_cache;           \
        (CDICT)->force_maps = (PARENT)->force_maps;         \
    }

#define __CDict_new_from_paste(a,b) __CDict_paste(a,b)
#define CDict_new_from(...)                     \
    __CDict_new_paste(                          \
        __CDict_new_from_,                      \
        CDICT_NARGS(__VA_ARGS__))(__VA_ARGS__)

/*
 * Call free on a pointer
 */
#define CDict_Safe_free(...) (__CDict_Safe_free(__VA_ARGS__))

/*
 * Turn on memory monitoring
 */
#define CDict_activate_memory_monitoring(CDICT)                     \
    cdict_alloc_stats_struct(CDICT);                                \
    (CDICT)->stats->malloc_count = 0;                               \
    (CDICT)->stats->calloc_count = 0;                               \
    (CDICT)->stats->realloc_count = 0;                              \
    (CDICT)->stats->asprintf_count = 0;                             \
    (CDICT)->stats->free_count = 0;                                 \
    (CDICT)->stats->free_null_count = 0;                            \
    (CDICT)->stats->malloc_size = 0;                                \
    (CDICT)->stats->calloc_size = 0;                                \
    (CDICT)->stats->realloc_size = 0;                               \
    (CDICT)->stats->asprintf_size = 0;                              \
    (CDICT)->stats->pointer_list_full = TRUE;                       \
    (CDICT)->stats->pointer_list = NULL;                            \
    (CDICT)->stats->pointer_list_n = 0;

/*
 * CDict_set functions : set an element in the cdict
 */
/* 3 arguments : just set the KEY,VALUE into CDICT */
#define __CDict_set_3(CDICT,                                    \
                      KEY,                                      \
                      VALUE)                                    \
    __extension__({                                             \
            printf("set3 %d\n",                                 \
                   __CDict_valuetype_num(VALUE)                 \
                );                                              \
            CDict_set_with_types((CDICT),                       \
                                 (KEY),                         \
                                 __CDict_keytype_num(KEY),      \
                                 (VALUE),                       \
                                 __CDict_valuetype_num(VALUE)); \
        })

/* 4 arguments : set the metadata, assume it doesn't need freeing */
#define __CDict_set_4(CDICT,                                        \
                      KEY,                                          \
                      VALUE,                                        \
                      METADATA)                                     \
    CDict_set_with_types_and_metadata((CDICT),                      \
                                      (KEY),                        \
                                      __CDict_keytype_num(KEY),     \
                                      (VALUE),                      \
                                      __CDict_valuetype_num(VALUE), \
                                      METADATA,                     \
                                      NULL)

/* 5 arguments : metadata and function to free the metadata */
#define __CDict_set_5(CDICT,                                        \
                      KEY,                                          \
                      VALUE,                                        \
                      METADATA,                                     \
                      METADATA_FREE_FUNCTION)                       \
    CDict_set_with_types_and_metadata((CDICT),                      \
                                      (KEY),                        \
                                      __CDict_keytype_num(KEY),     \
                                      (VALUE),                      \
                                      __CDict_valuetype_num(VALUE), \
                                      METADATA,                     \
                                      METADATA_FREE_FUNCTION)

#define __CDict_set_paste(a,b) __CDict_paste(a,b)
#define CDict_set(...)                                          \
    __CDict_set_paste(__CDict_set_,                             \
                      CDICT_NARGS(__VA_ARGS__))(__VA_ARGS__)



/*
 * CDict_set_with_types : set KEY, type KEYTYPE, with VALUE, type VALUETYPE,
 * and metadata METADATA (which can be NULL if you want to ignore it)
 * into the cdict at CDICT. Returns the (possibly new) entry in the CDICT.
 */
#define CDict_set_with_types(CDICT,                             \
                             KEY,                               \
                             KEYTYPE,                           \
                             VALUE,                             \
                             VALUETYPE)                         \
    cdict_set((CDICT),                                          \
              __extension__(union cdict_key_union)(KEY),        \
              (KEYTYPE),                                        \
              __extension__(union cdict_value_union)(VALUE),    \
              CDict_array_size(VALUE),                          \
              (VALUETYPE),                                      \
              NULL, /* metadata */                              \
              NULL /* metadata free function */                 \
        )

#define CDict_set_with_types_and_metadata(CDICT,                    \
                                          KEY,                      \
                                          KEYTYPE,                  \
                                          VALUE,                    \
                                          VALUETYPE,                \
                                          METADATA,                 \
                                          METADATA_FREE_FUNCTION    \
    )                                                               \
    cdict_set((CDICT),                                              \
              __extension__(union cdict_key_union)(KEY),            \
              (KEYTYPE),                                            \
              __extension__(union cdict_value_union)(VALUE),        \
              CDict_array_size(VALUE),                              \
              (VALUETYPE),                                          \
              (METADATA),                                           \
              (METADATA_FREE_FUNCTION)                              \
        )

/*
 * CDict_set_metadata : set an entry's metadata directly.
 */
#define CDict_set_metadata(CDICT,                   \
                           ENTRY,                   \
                           METADATA,                \
                           METADATA_FREE_FUNCTION)  \
    cdict_set_metadata((CDICT),                     \
                       (ENTRY),                     \
                       (METADATA),                  \
                       (METADATA_FREE_FUNCTION))
/*
 * CDict_get_metadata : get an entry's metadata directly.
 */
#define CDict_get_metadata(ENTRY)               \
    cdict_get_metadata((ENTRY))



/*
 * As CDict_set_with_types with specified format string
 */
#define CDict_set_with_types_and_formats(CDICT,         \
                                         KEY,           \
                                         KEYTYPE,       \
                                         KEYFORMAT,     \
                                         VALUE,         \
                                         VALUETYPE,     \
                                         VALUEFORMAT)   \
    cdict_set_with_formats(                             \
        CDICT,                                          \
        __extension__(union cdict_key_union)(KEY),      \
        (KEYTYPE),                                      \
        (KEYFORMAT),                                    \
        __extension__(union cdict_value_union)(VALUE),  \
        CDict_array_size(VALUE),                        \
        (VALUETYPE),                                    \
        (VALUEFORMAT),                                  \
        NULL,  /* metadata */                           \
        NULL /* metadata free function */               \
        );

#define CDict_set_with_types_formats_and_metadata(CDICT,                \
                                                  KEY,                  \
                                                  KEYTYPE,              \
                                                  KEYFORMAT,            \
                                                  VALUE,                \
                                                  VALUETYPE,            \
                                                  VALUEFORMAT,          \
                                                  METADATA,             \
                                                  METADATA_FREE_FUNCTION) \
    cdict_set_with_formats(                                             \
        CDICT,                                                          \
        __extension__(union cdict_key_union)(KEY),                      \
        (KEYTYPE),                                                      \
        (KEYFORMAT),                                                    \
        __extension__(union cdict_value_union)(VALUE),                  \
        CDict_array_size(VALUE),                                        \
        (VALUETYPE),                                                    \
        (VALUEFORMAT),                                                  \
        (METADATA),                                                     \
        (METADATA_FREE_FUNCTION)                                        \
        );



/*
 * CDict delete entry from cdict. Note, this only
 * deletes the reference the cdict: it does NOT delete
 * the entry or its contents.
 */
#define CDict_del(CDICT,ENTRY)                          \
    if((ENTRY)!=NULL)                                   \
    {                                                   \
        CDICT_UTHASH_DELETE((CDICT),                    \
                            CDICT_HANDLE,               \
                            (CDICT)->cdict_entry_list,  \
                            (ENTRY));                   \
    }

/*
 * CDict delete entry and its contents from cdict.
 */
#define CDict_del_and_contents(CDICT,ENTRY,FREE_CONTENTS)       \
    __extension__                                               \
    ({                                                          \
        struct cdict_entry_t *                                  \
            Concat3(__entry__,LABEL,LINE) = (ENTRY);            \
        if((ENTRY)!=NULL)                                       \
        {                                                       \
            cdict_delete_entry((CDICT),                         \
                               &Concat3(__entry__,LABEL,LINE),  \
                               (FREE_CONTENTS));                \
        }                                                       \
    })

/*
 * Delete a whole cdict.
 *
 * Call CDict_free to do a simple free of the cdict entry.
 *
 * Call CDict_free_with_function to pass entry to the given
 * function which does the freeing of the entry contents.
 *
 * All set CDICT to be NULL after calling cdict_free().
 */
#define CDict_free(CDICT)                       \
    cdict_free(&(CDICT),                        \
               FALSE,                           \
               NULL);                           \
    (CDICT) = NULL;

/*
 * Call CDict_free_and_free_contents to also free pointer data.
 */
#define CDict_free_and_free_contents(CDICT)     \
    cdict_free(&(CDICT),                        \
               TRUE,                            \
               NULL);                           \
    (CDICT) = NULL;

/*
 *
 */
#define CDict_free_with_metadata_function(CDICT,FUNCTION)   \
    cdict_free(&(CDICT),                                    \
               FALSE,                                       \
               (FUNCTION));                                 \
    (CDICT) = NULL;

/*
 * CDict_append : as cdict_set but tries to add two numbers
 * or concatenate strings, if possible.
 */
#define CDict_append(...)                                       \
    __CDict_set_paste(__CDict_append_,                          \
                      CDICT_NARGS(__VA_ARGS__))(__VA_ARGS__)

#define __CDict_append_3(CDICT,                                 \
                         KEY,                                   \
                         VALUE)                                 \
    cdict_append((CDICT),                                       \
                 __extension__(union cdict_key_union)(KEY),     \
                 __CDict_keytype_num(KEY),                      \
                 __extension__(union cdict_value_union)(VALUE), \
                 CDict_array_size(VALUE),                       \
                 __CDict_valuetype_num(VALUE),                  \
                 NULL, /* metadata */                           \
                 NULL /* metadata free function */              \
        )

#define CDict_append_with_types(CDICT,                          \
                                KEY,                            \
                                KEYTYPE,                        \
                                VALUE,                          \
                                VALUETYPE)                      \
    cdict_append((CDICT),                                       \
                 __extension__(union cdict_key_union)(KEY),     \
                 (KEYTYPE),                                     \
                 __extension__(union cdict_value_union)(VALUE), \
                 CDict_array_size(VALUE),                       \
                 (VALUETYPE),                                   \
                 NULL, /* metadata */                           \
                 NULL /* metadata free function */              \
        )

#define CDict_append_with_types_and_metadata(CDICT,                     \
                                             KEY,                       \
                                             KEYTYPE,                   \
                                             VALUE,                     \
                                             VALUETYPE,                 \
                                             METADATA,                  \
                                             METADATA_FREE_FUNCTION)    \
    cdict_append((CDICT),                                               \
                 __extension__(union cdict_key_union)(KEY),             \
                 (KEYTYPE),                                             \
                 __extension__(union cdict_value_union)(VALUE),         \
                 CDict_array_size(VALUE),                               \
                 (VALUETYPE),                                           \
                 (METADATA),                                            \
                 (METADATA_FREE_FUNCTION))

/*
 * Macro to determine whether a cdict contains an entry with
 * given key data and type and, if it does, returns it (otherwise NULL)
 */
#define CDict_contains_with_type(CDICT,KEYDATA,KEYTYPE)             \
    cdict_contains((CDICT),                                         \
                   __extension__(union cdict_key_union)(KEYDATA),   \
                   (KEYTYPE))

/*
 * As CDict_contains_with_type but automatically
 * determine the key type
 */
#define CDict_contains(CDICT,KEYDATA)                               \
    cdict_contains((CDICT),                                         \
                   __extension__(union cdict_key_union)(KEYDATA),   \
                   __CDict_keytype_num(KEYDATA))


/*
 * Convenience macro to access the key,value pair of a cdict
 * entry as strings. Note you must declare the KEY (and fill it)
 * and VALUESTRING as char* types yourself, and you must free the
 * memory that is allocated into them.
 */
#define CDict_key_and_value_strings(CDICT,          \
                                    KEY,            \
                                    VALUE,          \
                                    VALUESTRING)    \
    cdict_key_and_value_strings(                    \
        (CDICT),                                    \
        (KEY),                                      \
        (VALUE),                                    \
        &(VALUESTRING)                              \
        )

/*
 * Macros to access the value and key as strings with a format
 * specified by the user, if it is non-NULL.
 *
 * There are two sets: those that take ENTRY and those that
 * take either KEY or VALUE.
 */


/* entry -> string */
#define CDict_entry_to_value_string(CDICT,          \
                                    ENTRY,          \
                                    VALUESTRING,    \
                                    FORMAT)         \
    cdict_entry_to_value_string(                    \
        (CDICT),                                    \
        (ENTRY),                                    \
        &(VALUESTRING),                             \
        (FORMAT))

#define CDict_entry_index_to_value_string(CDICT,        \
                                          ENTRY,        \
                                          VALUESTRING,  \
                                          FORMAT,       \
                                          INDEX)        \
    cdict_entry_index_to_value_string(                  \
        (CDICT),                                        \
        (ENTRY),                                        \
        &(VALUESTRING),                                 \
        (FORMAT),                                       \
        (INDEX))

#define CDict_entry_to_key_string(CDICT,        \
                                  ENTRY,        \
                                  FORMAT)       \
    cdict_entry_to_key_string((CDICT),          \
                              (ENTRY),          \
                              (FORMAT))

#define CDict_entry_to_key_and_value_strings(CDICT,         \
                                             ENTRY,         \
                                             VALUESTRING)   \
    cdict_entry_to_key_and_value_strings((CDICT),           \
                                         (ENTRY),           \
                                         &(VALUESTRING))


/* key/value -> string */
#define CDict_value_to_value_string(CDICT,          \
                                    VALUE,          \
                                    VALUESTRING,    \
                                    FORMAT)         \
    cdict_value_to_value_string((CDICT),            \
                                (VALUE),            \
                                &(VALUESTRING),     \
                                (FORMAT),           \
                                0)

#define CDict_value_index_to_value_string(CDICT,        \
                                          VALUE,        \
                                          VALUESTRING,  \
                                          FORMAT,       \
                                          INDEX)        \
    cdict_value_to_value_string((CDICT),                \
                                (VALUE),                \
                                &(VALUESTRING),         \
                                (FORMAT),               \
                                (INDEX))

/*
 * Loop over CDICT calling each key,value pair ENTRY.
 * NB in NO PARTICULAR ORDER unless pre-sorted.
 *
 * Note: we use CDICT_ITER_DECLARE_ENTRY, a modified
 * version of uthash's HASH_ITER which uses C99 constructs
 * to build the loop with automatic (unique) temporary
 * variables and sends in the name of the ENTRY so this
 * can be probed in the loop, and does not require it
 * to be declared in advance.
 */
#define CDict_loop(CDICT,ENTRY)                         \
    CDICT_UTHASH_ITER_C99((CDICT),                      \
                          CDICT_HANDLE,                 \
                          (CDICT)->cdict_entry_list,    \
                          (ENTRY))


/*
 * Sorting.
 *
 * We provide CDict_sort to do this automatically,
 * CDict_reverse_sort to do it automatically in reverse
 * and CDict_sort_by_func to do it with your own sorting
 * function.
 */
#define CDict_sort(CDICT)                       \
    CDICT_UTHASH_SRT((CDICT),                   \
                     CDICT_HANDLE,              \
                     (CDICT)->cdict_entry_list, \
                     cdict_sort_by_entry_auto)

#define CDict_reverse_sort(CDICT)                       \
    CDICT_UTHASH_SRT((CDICT),                           \
                     CDICT_HANDLE,                      \
                     (CDICT)->cdict_entry_list,         \
                     cdict_sort_by_entry_auto_reverse)

#define CDict_sort_by_func(CDICT,SORTFUNC)      \
    CDICT_UTHASH_SRT((CDICT),                   \
                     CDICT_HANDLE,              \
                     (CDICT)->cdict_entry_list, \
                     (SORTFUNC))

/*
 * As CDict_loop but default sorts first. Cannot be
 * nested.
 */
#define CDict_sorted_loop(CDICT,ENTRY)          \
    CDict_sort(CDICT);                          \
    CDict_loop((CDICT),                         \
               (ENTRY))

#define CDict_reverse_sorted_loop(CDICT,ENTRY)  \
    CDict_reverse_sort(CDICT);                  \
    CDict_loop((CDICT),                         \
               (ENTRY))

#define CDict_JSON_buffer_to_CDict(BUFFER,CDICT)    \
    cdict_json_to((BUFFER),                         \
                  strlen(BUFFER),                   \
                  (CDICT))

#define CDict_JSON_file_to_CDict(CDICT,FILE)    \
    cdict_load_json((CDICT),(FILE))

#define CDict_to_JSON_file(...)                 \
    __CDict_to_JSON_file(__VA_ARGS__)

#define __CDict_to_JSON_file(CDICT,             \
                             FILE,              \
                             ITEM_SEPARATOR,    \
                             KEY_SEPARATOR,     \
                             INDENT,            \
                             NEWLINES,          \
                             SORT)              \
    cdict_to_json_file((CDICT),                 \
                       (FILE),                  \
                       (ITEM_SEPARATOR),        \
                       (KEY_SEPARATOR),         \
                       (INDENT),                \
                       (NEWLINES),              \
                       (SORT))


/*
 * Wrapper for the JSON function
 */
#define CDict_to_JSON(...)                      \
    __CDict_to_JSON(__VA_ARGS__)

#define __CDict_to_JSON(CDICT,                  \
                        BUFFER,                 \
                        BUFFER_SIZE,            \
                        ITEM_SEPARATOR,         \
                        KEY_SEPARATOR,          \
                        INDENT,                 \
                        NEWLINES,               \
                        SORT)                   \
    cdict_to_json(                              \
        (CDICT),                                \
        &(BUFFER),                              \
        &(BUFFER_SIZE),                         \
        0,                                      \
        (ITEM_SEPARATOR),                       \
        (KEY_SEPARATOR),                        \
        (INDENT),                               \
        (NEWLINES),                             \
        (SORT)                                  \
        )

/* convenience wrappers */

#define CDict_print_JSON(...)                   \
    __CDict_set_paste(                          \
        __CDict_print_JSON_,                    \
        CDICT_NARGS(__VA_ARGS__))               \
        (__VA_ARGS__)

#define __CDict_print_JSON_1(CDICT)             \
    __CDict_print_JSON_2((CDICT),TRUE)

#define __CDict_print_JSON_2(CDICT,SORT)        \
    do{                                         \
        char * __JSON_buffer = NULL;            \
        size_t __JSON_buffer_size = 0;          \
        CDict_to_JSON(                          \
            (CDICT),                            \
            __JSON_buffer,                      \
            __JSON_buffer_size,                 \
            ",",                                \
            " : ",                              \
            CDICT_JSON_INDENT,                  \
            CDICT_JSON_NEWLINES,                \
            (SORT)                              \
            );                                  \
        fputs(__JSON_buffer,stdout);            \
        CDict_Safe_free((CDICT),                \
                        __JSON_buffer);         \
    }while(0)

#define __CDict_print_JSON_3(CDICT,SORT,STREAM) \
    do{                                         \
        char * __JSON_buffer = NULL;            \
        size_t __JSON_buffer_size = 0;          \
        CDict_to_JSON(                          \
            (CDICT),                            \
            __JSON_buffer,                      \
            __JSON_buffer_size,                 \
            ",",                                \
            " : ",                              \
            CDICT_JSON_INDENT,                  \
            CDICT_JSON_NEWLINES,                \
            (SORT)                              \
            );                                  \
        fputs(__JSON_buffer,                    \
              (STREAM)                          \
            );                                  \
        CDict_Safe_free((CDICT),                \
                        __JSON_buffer);         \
    }while(0)

#define CDict_print_compact_JSON(CDICT,         \
                                 SORT)          \
    do{                                         \
        char * __JSON_buffer = NULL;            \
        size_t __JSON_buffer_size = 0;          \
        CDict_to_JSON(                          \
            (CDICT),                            \
            __JSON_buffer,                      \
            __JSON_buffer_size,                 \
            ",",                                \
            ":",                                \
            CDICT_JSON_NO_INDENT,               \
            CDICT_JSON_NO_NEWLINES,             \
            (SORT)                              \
            );                                  \
        fputs(__JSON_buffer,                    \
              stdout);                          \
        CDict_Safe_free((CDICT),                \
                        __JSON_buffer);         \
    }while(0)

/*
 * Set floating-point comparison tolerances and
 * truncation byte counts
 */
#define CDict_set_numerics(ABS,                 \
                           REL,                 \
                           NBYTES_DOUBLE,       \
                           NBYTES_FLOAT)        \
    cdict_set_numerics((ABS),                   \
                       (REL),                   \
                       (NBYTES_DOUBLE),         \
                       (NBYTES_FLOAT))




/*
 * Convenience function:
 * given a list of strings (in ...) we construct
 * a nested cdict in a cdict in a cdict (...) of these
 * names, and put the value (of given type)
 *
 * Note that we append by default
 */
#define CDict_nest(CDICT,KEY,VALUE,...)         \
    __CDict_nest_with_metadata(                 \
        (CDICT),                                \
        (KEY),                                  \
        (VALUE),                                \
        NULL,                                   \
        NULL,                                   \
        CDICT_NEST_ACTION_APPEND,               \
        __VA_ARGS__)

/* as CDict_nest but set rather than append */
#define CDict_nest_set(CDICT,KEY,VALUE,...)     \
    __CDict_nest_with_metadata(                 \
        (CDICT),                                \
        (KEY),                                  \
        (VALUE),                                \
        NULL,                                   \
        NULL,                                   \
        CDICT_NEST_ACTION_SET,                  \
        __VA_ARGS__)

/*
 * Two ways to access CDict_nest_with_metadata :
 * default to APPEND, but provide API access to
 * SET also.
 */
#define CDict_nest_with_metadata(               \
    CDICT,                                      \
    KEY,                                        \
    VALUE,                                      \
    METADATA,                                   \
    METADATA_FREE_FUNCTION,                     \
    ...)                                        \
    __CDict_nest_with_metadata(                 \
        (CDICT),                                \
        (KEY),                                  \
        (VALUE),                                \
        (METADATA),                             \
        (METADATA_FREE_FUNCTION),               \
        CDICT_NEST_ACTION_APPEND,               \
        __VA_ARGS__)

#define CDict_nest_with_metadata_set(           \
    CDICT,                                      \
    KEY,                                        \
    VALUE,                                      \
    METADATA,                                   \
    METADATA_FREE_FUNCTION,                     \
    ...)                                        \
    __CDict_nest_with_metadata(                 \
        (CDICT),                                \
        (KEY),                                  \
        (VALUE),                                \
        (METADATA),                             \
        (METADATA_FREE_FUNCTION),               \
        CDICT_NEST_ACTION_SET,                  \
        __VA_ARGS__)

#define __CDict_nest_with_metadata(...)         \
    __CDict_nest_with_metadata_implementation(  \
        CDict_nest_with_metadata,               \
        __COUNTER__,                            \
        __VA_ARGS__                             \
        )

#define __CDict_nest_with_metadata_implementation(                      \
    LABEL,                                                              \
    LINE,                                                               \
    CDICT,                                                              \
    KEY,                                                                \
    VALUE,                                                              \
    METADATA,                                                           \
    METADATA_FREE_FUNCTION,                                             \
    ACTION,                                                             \
    ...)                                                                \
                                                                        \
    __extension__                                                       \
    ({                                                                  \
        struct cdict_entry_t * Concat3(__entry,LABEL,LINE) = NULL;      \
        do                                                              \
        {                                                               \
            /* make the path to keys */                                 \
            union cdict_key_union Concat3(__CDICT_nest_path,LABEL,LINE)[] = \
                {                                                       \
                    __CDict_Data_array_to_key_union_array(              \
                        __VA_ARGS__                                     \
                        )                                               \
                };                                                      \
                                                                        \
            /* how many keys in the path? */                            \
            const size_t Concat3(__CDICT_nest_npath,LABEL,LINE) =       \
                sizeof(Concat3(__CDICT_nest_path,LABEL,LINE))/          \
                sizeof((Concat3(__CDICT_nest_path,LABEL,LINE))[0]);     \
                                                                        \
            /* what type are they? */                                   \
            CDict_key_type Concat3(pathtypenums,LABEL,LINE)[] = {       \
                CDict_Data_array_to_typenum_array(__VA_ARGS__)          \
            };                                                          \
                                                                        \
            /* set the data */                                          \
            Concat3(__entry,LABEL,LINE) =                               \
                cdict_nest(                                             \
                    (CDICT),                                            \
                    (union cdict_key_union*)Concat3(__CDICT_nest_path,LABEL,LINE), \
                    Concat3(__CDICT_nest_npath,LABEL,LINE),             \
                    Concat3(pathtypenums,LABEL,LINE),                   \
                    (union cdict_key_union)(KEY),                       \
                    __CDict_keytype_num(KEY),                           \
                    (union cdict_value_union)(VALUE),                   \
                    CDict_array_size(VALUE),                            \
                    __CDict_valuetype_num(VALUE),                       \
                    (METADATA),                                         \
                    (METADATA_FREE_FUNCTION),                           \
                    (ACTION)                                            \
                    );                                                  \
        }while(0);                                                      \
        Concat3(__entry,LABEL,LINE);                                    \
    })

/*
 * Return a nested cdict entry matching the string
 * passed in after the CDICT
 */
#define CDict_nest_get_entry(...)               \
    CDict_nest_get_entry_implementation(        \
        CDict_nest_get_entry,                   \
        __COUNTER__,                            \
        __VA_ARGS__)


#define CDict_nest_get_entry_implementation(                            \
    LABEL,                                                              \
    LINE,                                                               \
    CDICT,                                                              \
    ...)                                                                \
    __extension__                                                       \
    ({                                                                  \
        struct cdict_entry_t * Concat3(__entry,LABEL,LINE) = NULL;      \
        do                                                              \
        {                                                               \
            /* make the path to keys */                                 \
            union cdict_key_union Concat3(__CDICT_nest_path,LABEL,LINE)[] = \
                {                                                       \
                    __CDict_Data_array_to_key_union_array(              \
                        __VA_ARGS__                                     \
                        )                                               \
                };                                                      \
                                                                        \
            /* how many keys in the path? */                            \
            const size_t Concat3(__CDICT_nest_npath,LABEL,LINE) =       \
                sizeof(Concat3(__CDICT_nest_path,LABEL,LINE))/          \
                sizeof((Concat3(__CDICT_nest_path,LABEL,LINE))[0]);     \
                                                                        \
            /* what type are they? */                                   \
            CDict_key_type Concat3(pathtypenums,LABEL,LINE)[] = {       \
                CDict_Data_array_to_typenum_array(__VA_ARGS__)          \
            };                                                          \
                                                                        \
            /* set the data */                                          \
            Concat3(__entry,LABEL,LINE) =                               \
                cdict_nest_get(                                         \
                    (CDICT),                                            \
                    (union cdict_key_union*)Concat3(__CDICT_nest_path,LABEL,LINE), \
                    Concat3(__CDICT_nest_npath,LABEL,LINE),             \
                    Concat3(pathtypenums,LABEL,LINE)                    \
                    );                                                  \
                                                                        \
        }while(0);                                                      \
        Concat3(__entry,LABEL,LINE);                                    \
    })


/*
 * From a nested cdict, get the data pointer at the
 * passed in location
 */
#define CDict_nest_get_data_pointer(...)        \
    CDict_nest_get_data_pointer_implementation( \
        CDict_nest_get_data_pointer,            \
        __COUNTER__,                            \
        __VA_ARGS__)

#define CDict_nest_get_data_pointer_implementation( \
    LABEL,                                          \
    LINE,                                           \
    CDICT,                                          \
    ...)                                            \
    __extension__                                   \
    ({                                              \
        struct cdict_entry_t * const                \
            Concat3(__get_entry,LABEL,LINE) =       \
            CDict_nest_get_entry((CDICT),           \
                                 __VA_ARGS__);      \
                                                    \
        Concat3(__get_entry,LABEL,LINE)             \
            ?                                       \
            cdict_get_union_data_pointer(           \
                Concat3(__get_entry,LABEL,LINE)     \
                )                                   \
            :                                       \
            NULL;                                   \
    })


/*
 * From a nested cdict, get the data value at the passed in
 * location.
 *
 * Note that the type of VAR must match the type of the
 * data in the value, so if you're looking for a double
 * value, VAR must be a double. If you don't know the
 * type, all bets are off!
 */
#define CDict_nest_get_data_value(...)          \
    CDict_nest_get_data_value_implementation(   \
        CDict_nest_get_data_value,              \
        __COUNTER__,                            \
        __VA_ARGS__)

#define CDict_nest_get_data_value_implementation(                       \
    LABEL,                                                              \
    LINE,                                                               \
    CDICT,                                                              \
    VAR,                                                                \
    ...)                                                                \
    __extension__                                                       \
    ({                                                                  \
        __typeof__(VAR) * Concat3(x,LABEL,LINE) =                       \
            CDict_nest_get_data_pointer((CDICT),                        \
                                        __VA_ARGS__);                   \
        if(Concat3(x,LABEL,LINE) == NULL)                               \
        {                                                               \
            cdict_error((CDICT),                                        \
                        CDICT_ERROR_NESTED_PATH_NOT_FOUND,              \
                        "entry at nested path not found in cdict %p in CDict_nest_get_data_value\n", \
                        (void*)(CDICT));                                \
        }                                                               \
        (__typeof__(VAR))(* Concat3(x,LABEL,LINE));                     \
    })

/*
 * Anonymously make a string and return it as
 * a char* pointer. Return NULL on failure or
 * empty string (considered a failure).
 *
 * The string is added to the cdict's tofree list, so is
 * freed with the cdict
 */
#define CDict_string(...)                       \
    CDict_string_implementation(                \
        CDict_string_implementation,            \
        __COUNTER__,                            \
        __VA_ARGS__)

#define CDict_string_implementation(LABEL,                              \
                                    LINE,                               \
                                    CDICT,                              \
                                    STRING)                             \
    __extension__                                                       \
    ({                                                                  \
        char * Concat3(string,LABEL,LINE) = strdup(STRING);             \
        cdict_push_tofree((CDICT),Concat3(string,LABEL,LINE));          \
        strlen(Concat3(string,LABEL,LINE)) <= 0 ? NULL : (char*)Concat3(string,LABEL,LINE); \
    })

/*
 * As above for n characters.
 */
#define CDict_stringn(...)                        \
    CDict_stringn_implementation(                 \
        CDict_stringn_implementation,             \
        __COUNTER__,                              \
        __VA_ARGS__)

#define CDict_stringn_implementation(LABEL,                             \
                                    LINE,                               \
                                    CDICT,                              \
                                    STRING,                             \
                                    N)                                  \
    __extension__                                                       \
    ({                                                                  \
        char * Concat3(string,LABEL,LINE) = strndup((STRING),(N));      \
        cdict_push_tofree((CDICT),Concat3(string,LABEL,LINE));          \
        strlen(Concat3(string,LABEL,LINE)) <= 0 ? NULL : (char*)Concat3(string,LABEL,LINE); \
    })

#define CDict_asprintf(...)                     \
    CDict_asprintf_implementation(              \
        CDict_asprintf,                         \
        __COUNTER__,                            \
        __VA_ARGS__)

#ifdef __CDICT_HAVE_VA_OPT__
/*
 * Newer compilers have __VA_OPT__
 */
#define CDict_asprintf_implementation(LABEL,                            \
                                      LINE,                             \
                                      CDICT,                            \
                                      STRING,                           \
                                      FORMAT,                           \
                                      ...)                              \
    __extension__                                                       \
    ({                                                                  \
        const int Concat3(n,LABEL,LINE) =                               \
            cdict_asprintf((CDICT),                                     \
                           &(STRING),                                   \
                           (FORMAT)                                     \
                           __VA_OPT__(,) __VA_ARGS__);                  \
        if(Concat3(n,LABEL,LINE) < 0 || (STRING) == NULL)               \
        {                                                               \
            cdict_error((CDICT),                                        \
                        CDICT_ERROR_ASPRINTF_FAILED,                    \
                        "asprintf failed (returned %d) in CDict_asprintf()\n", \
                        Concat3(n,LABEL,LINE));                         \
        }                                                               \
        cdict_push_tofree((CDICT),(STRING));                            \
        Concat3(n,LABEL,LINE);                                          \
    })
#else
/*
 * Older gcc-compatible compilers use ##__VA_ARGS__ instead
 * of VA_OPT
 */
#define CDict_asprintf_implementation(LABEL,                            \
                                      LINE,                             \
                                      CDICT,                            \
                                      STRING,                           \
                                      FORMAT,                           \
                                      ...)                              \
    __extension__                                                       \
    ({                                                                  \
        const int Concat3(n,LABEL,LINE) =                               \
            cdict_asprintf((CDICT),                                     \
                           &(STRING),                                   \
                           (FORMAT),                                    \
                           ##__VA_ARGS__);                              \
        if(Concat3(n,LABEL,LINE) < 0 || (STRING) == NULL)               \
        {                                                               \
            cdict_error((CDICT),                                        \
                        CDICT_ERROR_ASPRINTF_FAILED,                    \
                        "asprintf failed (returned %d) in CDict_asprintf()\n", \
                        Concat3(n,LABEL,LINE));                         \
        }                                                               \
        cdict_push_tofree((CDICT),(STRING));                            \
        Concat3(n,LABEL,LINE);                                          \
    })
#endif // __CDICT_HAVE_VA_OPT__

/*
 * Convenience function to free the metadata
 * when it's just a single pointer
 */
#define CDict_free_metadata cdict_free_metadata

/*
 * CDict stats calls
 */
#define __CDict_stats_1(CDICT)                  \
    cdict_stats((CDICT),NULL,NULL)

#define __CDict_stats_2(CDICT,PREPEND_STRING)   \
    cdict_stats((CDICT),(PREPEND_STRING),NULL)

#define __CDict_stats_3(CDICT,PREPEND_STRING,OUTSTRING) \
    cdict_stats((CDICT),(PREPEND_STRING),(OUTSTRING))

#define __CDict_stats_paste(a,b) __CDict_paste(a,b)
#define CDict_stats(...)                                        \
    __CDict_stats_paste(__CDict_stats_,                         \
                        CDICT_NARGS(__VA_ARGS__))(__VA_ARGS__)


/*
 * Add and remove pointers to the "tofree" list
 */
#define CDict_add_to_free_list(CDICT,PTR)       \
    cdict_push_tofree((CDICT),(PTR))

#define CDict_free_to_free_list(CDICT)          \
    cdict_free_tofree(CDICT)


/*
 * Wrapper for the CDict_assert_(1,2) macros
 */
#define CDict_assert(...)                       \
    __CDict_set_paste(                          \
        __CDict_assert_,                        \
        CDICT_NARGS(__VA_ARGS__))               \
        (__VA_ARGS__)

/*
 * Assert that X is TRUE, otherwise call cdict_error.
 * This single-argument form does not take a cdict, so there
 * is no control over the error handler.
 */
#define __CDict_assert_1(X)                                     \
    if((X) == FALSE)                                            \
    {                                                           \
        cdict_error(NULL,                                       \
                    CDICT_ERROR_ASSERT_FAILED,                  \
                    "Assert failed at line %ld of \"%s\"\n",    \
                    __LINE__,                                   \
                    __FILE__);                                  \
    }
/*
 * This two argument form takes a cdict, so you can call its
 * error function.
 */
#define __CDict_assert_2(CDICT,X)                               \
    if((X) == FALSE)                                            \
    {                                                           \
        cdict_error((CDICT),                                    \
                    CDICT_ERROR_ASSERT_FAILED,                  \
                    "Assert failed at line %ld of \"%s\"\n",    \
                    __LINE__,                                   \
                    __FILE__);                                  \
    }

/*
 * Copy cdict from SRC to DEST
 */
#define CDict_copy(SRC,DEST)                    \
    cdict_copy((SRC),(DEST))

/*
 * Grep on a cdict
 */
#define CDict_grep(CDICT,FUNC,NMATCHES_POINTER)     \
    cdict_grep((CDICT),(FUNC),(NMATCHES_POINTER))


/*
 * Wrapper for cdict_iter
 */
#define CDict_iter(...)                         \
    cdict_iter(__VA_ARGS__)

#endif // CDICT_API_H

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
       