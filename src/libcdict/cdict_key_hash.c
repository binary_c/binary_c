

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit

#include <math.h>
size_t cdict_hashv_float_nbytes = CDICT_HASHV_FLOAT_NBYTES_DEFAULT;
size_t cdict_hashv_double_nbytes = CDICT_HASHV_DOUBLE_NBYTES_DEFAULT;
size_t cdict_hashv_long_double_nbytes = CDICT_HASHV_LONG_DOUBLE_NBYTES_DEFAULT;

#ifdef __DEPRECATED


/*
 * Return hashv to identify a cdict bucket for the given key
 */

#define hashv_printf(...) /* do nothing */
//#define hashv_printf(...) if(cdict->vb){printf(__VA_ARGS__);}

__CDict_Nonnull_all_arguments
CDict_API_function
unsigned cdict_key_hash(const struct cdict_t * const cdict __CDict_maybe_unused,
                        struct cdict_key_t * key,
                        size_t len __CDict_maybe_unused)
{
    hashv_printf("make hashv from key %p type = %d (%s), format %s\n",
                 key,
                 key->type,
                 __CDict_key_descriptor(key->type),
                 key->format);

    unsigned hashv; /* hash value to be set */

    /*
     * Copy the key vaue to
     */
    if(key->type == CDICT_DATA_TYPE_DOUBLE ||
       key->type == CDICT_DATA_TYPE_LONG_DOUBLE ||
       key->type == CDICT_DATA_TYPE_FLOAT)
    {
        if(key->type == CDICT_DATA_TYPE_DOUBLE)
        {
            double d;
            double f;
            f = (double)key->key.double_data;
            hashv_printf("double key\n");
            if(cdict_hashv_double_nbytes > 0)
            {
                /*
                 * Doubles and floats have limited precision:
                 * truncate the number before setting the hashv
                 *
                 * cf.
                 * https://stackoverflow.com/questions/4238122/hash-function-for-floats
                 *
                 * see also
                 * https://troydhanson.github.io/uthash/userguide.html
                 * (Specifying an alternate key comparison function)
                 *
                 * https://diego.assencio.com/?index=67e5393c40a627818513f9bcacd6a70d
                 */

                /*
                 * double-precision truncation
                 */
                hashv_printf("Double was %50.40e\n",f);
                d = cdict_truncate_double(f,
                                          cdict_hashv_double_nbytes);
            }
            else
            {
                /*
                 * Alternative (slower) algorithm:
                 * use the exponent as the hash key
                 */
                int exp;
                const double fraction = frexp(f, &exp);
                d = (double) exp + fraction;
            }
            hashv_printf("Double  is %50.40e\n",d);

            /*
             * Use uthash's HASH_JEN to convert to hashv
             */
            hashv_printf("Use HASH_JEN on %zu\n",sizeof(double));
            __CDict_hash_function(cdict,
                                  &d,
                                  sizeof(double),
                                  hashv);
        }
        else if(key->type == CDICT_DATA_TYPE_LONG_DOUBLE)
        {
            long double d;
            long double f;
            f = (long double)key->key.long_double_data;
            hashv_printf("long double key\n");

            if(cdict_hashv_long_double_nbytes > 0)
            {
                /*
                 * Doubles and floats have limited precision:
                 * truncate the number before setting the hashv
                 *
                 * cf.
                 * https://stackoverflow.com/questions/4238122/hash-function-for-floats
                 *
                 * see also
                 * https://troydhanson.github.io/uthash/userguide.html
                 * (Specifying an alternate key comparison function)
                 *
                 * https://diego.assencio.com/?index=67e5393c40a627818513f9bcacd6a70d
                 */

                /*
                 * double-precision truncation
                 */
                hashv_printf("Double was %50.40Le\n",f);
                d = cdict_truncate_long_double(f,
                                               cdict_hashv_long_double_nbytes);
            }
            else
            {
                /*
                 * Alternative (slower) algorithm:
                 * use the exponent as the hash key
                 */
                int exp;
                const long double fraction = frexpl(f, &exp);
                d = (long double) exp + fraction;
            }
            hashv_printf("Double  is %50.40Le\n",d);

            /*
             * Use uthash's HASH_JEN to convert to hashv
             */
            hashv_printf("Use HASH_JEN on %zu\n",sizeof(long double));
            __CDict_hash_function(cdict,
                                  &d,
                                  sizeof(long double),
                                  hashv);
        }
        else
        {
            /* as above for a float */
            float f = cdict_truncate_float(key->key.float_data,
                                           cdict_hashv_float_nbytes);
            hashv_printf("float key\n");
            __CDict_hash_function(cdict,
                                  &f,
                                  sizeof(float),
                                  hashv);
        }
    }
    else
    {
        /*
         * We can use the key data directly, note that
         * this relies on the union having been zeroed
         * correctly (which the set functions do).
         */
        hashv_printf("use key directly\n");
        __CDict_hash_function(cdict,
                              &key->key,
                              sizeof(union cdict_key_union),
                              hashv);
    }

    hashv_printf("HASH_JEN gave hashv = %u\n",hashv);

    return (unsigned)hashv;
}
*/
#endif //__DEPRECATED

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        