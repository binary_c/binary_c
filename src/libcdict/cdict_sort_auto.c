

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit

#include <math.h>
#include <stdio.h>
#include "cdict_sort_auto.h"

/*
 * Auto-sort cdict keys a and b
 */

/*
 * Global variables:
 * default tolerances for floating-point comparisons
 */
long double cdict_absolute_tolerance = CDICT_ABSOLUTE_TOLERANCE_DEFAULT;
long double cdict_relative_tolerance = CDICT_RELATIVE_TOLERANCE_DEFAULT;

//#define hprint(...) printf(__VA_ARGS__);fflush(stdout);

#ifndef hprint
/* we can only be a pure function if not debugging */
#define hprint(...) /* do nothing */
__CDict_Pure_function
#endif // hprint check
__CDict_Nonnull_all_arguments
int cdict_sort_auto(struct cdict_t * const cdict,
                     struct cdict_key_t * const a,
                     struct cdict_key_t * const b)
{
    int ret;
    hprint("cdict_sort_auto: %p %p : map_types %d %d : mapped? %d %d : \"%s\" \"%s\"\n",
           (void*)a,
           (void*)b,
           a->map_type,
           b->map_type,
           (int)a->mapped,
           (int)b->mapped,
           a->string,
           b->string);

    if(a->mapped == TRUE &&
       b->mapped == TRUE)
    {
        hprint("Both are mapped");
        if(a->map_type == b->map_type)
        {
            hprint("Types are equal");
            /*
             * Keys are of the same type, yay!
             */
            if(a->map_type == CDICT_DATA_TYPE_LONG_DOUBLE)
            {
                /*
                 * Double precision
                 */
                hprint("cdict_sort_auto: same type, mapped as long doubles\n");
                ret = cdict_cf_long_doubles(a->map_data.long_double_data,
                                            b->map_data.long_double_data);
            }
            else if(a->map_type == CDICT_DATA_TYPE_LONG_INT)
            {
                /* cmp for long ints */
                hprint("cdict_sort_auto: same type, mapped as long ints\n");
                ret = __cdict_cmp(a->map_data.long_long_int_data,
                                  b->map_data.long_long_int_data);
            }
            else if(a->map_type == CDICT_DATA_TYPE_STRING)
            {
                /* strcmp for strings */
                hprint("cdict_sort_auto: same type, mapped as strings, use strcmp\n");
                hprint("String1: %p \"%s\"\n",(void*)a->map_data.string_data,a->map_data.string_data);
                hprint("String2: %p \"%s\"\n",(void*)b->map_data.string_data,b->map_data.string_data);
                ret = strcmp(a->map_data.string_data,
                             b->map_data.string_data);
            }
            else if(a->map_type == CDICT_DATA_TYPE_BOOLEAN)
            {
                /* strcmp for strings */
                hprint("cdict_sort_auto: same type, mapped as booleans\n");
                ret = __cdict_cmp(a->map_data.string_data,
                                  b->map_data.string_data);
            }
            else
            {
                /* must be pointers, just numerically compare */
                hprint("cdict_sort_auto: same type, mapped as pointers\n");
                ret = __cdict_cmp(a->map_data.pointer_data,
                                  b->map_data.pointer_data);
            }
        }
        else
        {
            /*
             * Keys differ in type:
             * If both are numeric, use their equivalent long doubles.
             * Otherwise, use the strings.
             */
            if(__CDict_datatype_is_numeric_or_char_or_boolean(a->map_type) &&
               __CDict_datatype_is_numeric_or_char_or_boolean(b->map_type))
            {
                ret = __cdict_cmp(a->map_as_long_double,
                                  b->map_as_long_double);
            }
            else
            {
                ret = strcmp(a->string,
                             b->string);
            }
        }
        hprint("cdict_sort_auto ret: %d\n",ret);
    }
    else
    {
        hprint("UNMAPPED\n");

#ifdef hprint
        Boolean head __CDict_maybe_unused = FALSE;
        {
            hprint("CF KEYS at %p, %p :",(void*)a,(void*)b);
            hprint("datatypes %s, %s : strings are %s, %s\n",
                   __CDict_key_descriptor(a->type),
                   __CDict_key_descriptor(b->type),
                   a->string,
                   b->string);
        }
#endif //hprint

        /*
         * Numbers and chars can be sorted directly
         */

        /* TODO should include booleans here */
        if(__CDict_datatype_is_numeric_or_char_or_boolean(a->type) &&
           __CDict_datatype_is_numeric_or_char_or_boolean(b->type))
        {

            if(a->type == CDICT_DATA_TYPE_BOOLEAN &&
               b->type == CDICT_DATA_TYPE_BOOLEAN)
            {
                return __cdict_cmp(a->key.Boolean_data,
                                   b->key.Boolean_data);
            }
            else if(__CDict_datatype_is_floating_point(a->type) &&
                    __CDict_datatype_is_floating_point(b->type))
            {
                /*
                 * Both are some kind of floating point types,
                 * compare directly as long doubles
                 */
                const long double _a = (long double)CDict_key_scalar(a);
                const long double _b = (long double)CDict_key_scalar(b);

                /*
                 * Compare within eps.
                 * cf. https://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/
                 */
                const long double diff = fabsl(_a - _b);
                hprint("diff = %Lg cf tol %Lg\n",diff,cdict_absolute_tolerance);
                if (diff <= cdict_absolute_tolerance)
                {
                    ret = 0;
                }
                else
                {
                    const long double f_a = fabsl(_a);
                    const long double f_b = fabsl(_b);
                    const long double largest = (f_b > f_a) ? f_b : f_a;
                    if(diff <= largest * cdict_relative_tolerance)
                    {
                        ret = 0;
                    }
                    else
                    {
                        /*
                         * Numbers are not equal, __cdict_cmp will do
                         */
                        ret = __cdict_cmp(_a,_b);
                    }
                }
                hprint("both floats %30.20Le vs %30.20Le cf. doubles : a<b ? %d a>b ? %d : cmp %d : ret = %d",
                       _a,
                       _b,
                       _a<_b,
                       _a>_b,
                       __cdict_cmp(_a,_b),
                       ret
                    );
            }
            else if(__CDict_datatype_is_int(a->type) &&
                    __CDict_datatype_is_int(b->type))
            {
                /*
                 * Both are integer types, compare directly
                 * as long ints
                 */
                const long int _a = CDict_key_scalar(a);
                const long int _b = CDict_key_scalar(b);
                ret = __cdict_cmp(_a, _b);
                hprint("both ints, cf. long ints");
            }
            else
            {
                /*
                 * Both are numeric, but differ in type: convert to
                 * doubles and hope for the best
                 */
                const double _a = CDict_key_scalar(a);
                const double _b = CDict_key_scalar(b);
                ret = __cdict_cmp(_a, _b);
                hprint("both numeric, cf. doubles");
            }
        }
        /*
         * Pointers can be compared, not sure what it means
         */
        else if((__CDict_datatype_is_pointer(a->type) &&
                 a->type != CDICT_DATA_TYPE_STRING)
                &&
                (__CDict_datatype_is_pointer(b->type) &&
                 b->type != CDICT_DATA_TYPE_STRING))
        {
            const void * _a = (const void*)CDict_union_pointer(a);
            const void * _b = (const void*)CDict_union_pointer(b);
            ret = __cdict_cmp(_a,_b);
            hprint("both pointers, __cdict_cmp directly");
        }
        /*
         * No idea what to do with other data types:
         * convert to strings, see if we can use
         * integer or double comparison, then revert
         * to strcmp
         */
        else
        {
            /*
             * Convert to strings _a and _b
             */
            char *_a = NULL, *_b = NULL;
            char * endptra, * endptrb;
            hprint("types %d %d\n",
                   a->type,
                   b->type
                );

            hprint("a : is pointer? %d is string? %d is type %d\n",
                   __CDict_datatype_is_pointer(a->type),
                   a->type == CDICT_DATA_TYPE_STRING,
                   a->type);
            hprint("b : is pointer? %d is string? %d is type %d\n",
                   __CDict_datatype_is_pointer(b->type),
                   b->type == CDICT_DATA_TYPE_STRING,
                   b->type);

            /*
             * Because everything else failed, we
             * require strings to be set (sorry, this
             * might be slow)
             */
            if(a->string == NULL)
            {
                cdict_set_key_string(cdict,a,NULL);
            }
            if(b->string == NULL)
            {
                cdict_set_key_string(cdict,b,NULL);
            }

            if(a->type == CDICT_DATA_TYPE_STRING &&
               b->type == CDICT_DATA_TYPE_STRING)
            {
                /*
                 * Both are strings
                 */
                hprint("CF strings %s %s\n",a->string,b->string);
                ret = strcmp(a->string,
                             b->string);
            }
            else
            {
                /*
                 * Try converting both to long ints
                 */
                _a = a->string;
                _b = b->string;
                const long int al = strtol(a->string,&endptra,10);
                const long int bl = strtol(b->string,&endptrb,10);
                if(a->string != endptra &&
                   b->string != endptrb)
                {
                    /*
                     * Both converted to long ints successfully
                     */
                    hprint("strtol for both, cf. as long ints");
                    ret = __cdict_cmp(al,bl);
                }
                else
                {
                    /*
                     * Fall back to doubles
                     */
                    hprint("fall back to doubles");
                    const double ad = CDict_strtod(_a,&endptra);
                    const double bd = CDict_strtod(_b,&endptrb);
                    hprint("got ad=%g bd=%g\n",ad,bd);
                    if(_a!=endptra && _b!=endptrb)
                    {
                        /*
                         * Conversions to double precision succeeded
                         */
                        ret = __cdict_cmp(ad,bd);
                        hprint("CDict_strtod for both, cf. as doubles");
                    }
                    else
                    {
                        /*
                         * Finally try strcmp
                         */
                        ret = strcmp(_a,_b);
                        hprint("??? fallback on strcmp");
                    }
                }
            }
        }
    }
    hprint(" -> return %d\n",ret);
    return ret;
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        