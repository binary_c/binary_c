

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit


#include <stdio.h>
#ifdef CDICT_USE_RYU
#include "ryu/ryu.h"
#endif// CDICT_USE_RYU


__CDict_Nonnull_some_arguments(1,2)
CDict_API_function
int cdict_value_to_value_string(struct cdict_t * const cdict,
                                const struct cdict_value_t * const value,
                                char ** const value_string,
                                const char * const format,
                                const cdict_size_t index)
{
    /*
     * Set the value_string based on the data in value.
     *
     * If format (passed in) is set, use it.
     * If value->format is non-NULL, use it.
     * Otherwise use the default format string.
     *
     * We use asprintf to make the string, and return what
     * it returns, or HASH_ERROR_TO_STRING_TYPE_FAILED on
     * error (e.g. unknown data type).
     *
     * When there is an error, *value_string is set to NULL.
     *
     * Array-type data uses the index as an offset, or if
     * index is negative shows the whole array.
     *
     * Return value is the number of characters (>=0) in the string,
     * as returned by sprintf or asprintf, excluding the trailing
     * null byte. On error, the negative of the error code is
     * returned.
     */
    const Boolean vb = FALSE || cdict->vb;
    if(vb == TRUE)
    {
        printf("cdict_value_to_value_string cdict = %p, value = %p, value_string = %p *value_string = %p, format = \"%s\" (value->type %d, value->format \"%s\", value is array? %s), index %lld\n",
               (void*)cdict,
               (void*)value,
               (void*)value_string,
               (void*)(value_string ? *value_string : NULL),
               format,
               value->type,
               value->format,
               __CDict_Yesno(__CDict_datatype_is_array(value->type)),
               index);
        if(value->type == 0)
        {
            printf("is a string\n");
        }
        fflush(stdout);
    }

    int ret = -CDICT_ERROR_TO_STRING_TYPE_FAILED; /* return value */

    if(__CDict_datatype_is_array(value->type) &&
       index < 0)
    {
        /*
         * Construct whole array string  [ ... ]
         */
        size_t size = cdict_asprintf(cdict,value_string,"[");
        if(value->count > 0)
        {
            const cdict_size_t last = value->count-1;
            for(cdict_size_t i=0; i<value->count; i++)
            {
                char * v;

                /*
                 * Call this function with index i but
                 * the equivalent scalar scalar type
                 */
                {
                    struct cdict_value_t tmpvalue;
                    memcpy(&tmpvalue,
                           value,
                           sizeof(struct cdict_value_t));
                    cdict_value_to_value_string(cdict,
                                                &tmpvalue,
                                                &v,
                                                format,
                                                i);
                }

                char * chunk;
                size_t chunk_size = 0;
                chunk_size = (size_t) cdict_asprintf(cdict,
                                                     &chunk,
                                                     "\"%s\"%s",
                                                     v,
                                                     i != last ? "," : "");
                CDict_Safe_free(cdict,v);
                if(chunk_size > 0)
                {
                    /*
                     * Append chunk onto the value_string
                     */
                    *value_string = __CDict_realloc(cdict,
                                                    *value_string,
                                                    size + chunk_size);
                    if(chunk != NULL)
                    {
                        memcpy(*value_string + size,
                               chunk,
                               chunk_size*sizeof(char));
                        size += chunk_size;
                    }
                    else
                    {
                        CDict_Safe_free(cdict,
                                        chunk);
                        return CDICT_ERROR_TO_STRING_TYPE_FAILED;
                    }
                }
                CDict_Safe_free(cdict,
                                chunk);
            }
        }
        *value_string = __CDict_realloc(cdict,
                                        *value_string,
                                        size + 2);
        *(*value_string + size++) = ']';
        *(*value_string + size++) = '\0';
        ret = size + 1;
    }
    else
    {
        if(value->type == CDICT_DATA_TYPE_CDICT_ARRAY)
        {
            /*
             * cdict from an array of cdicts
             */
        }
        else
        {
            /*
             * Scalar type or scalar from an array
             *
             * Note that the value->type can be either scalar or array
             * type: in the case of array types, we know that index
             * >=0 so we want to access only one element of the array.
             */

            /*
             * Choose the format we'd like to use
             */
            const char * const use_format =
                format != NULL ? format :
                value->format != NULL ? value->format :
                __CDict_generic_format_string(value->type);

            if(use_format != NULL)
            {
                ret = cdict_make_string_from_var(cdict,
                                                 value_string,
                                                 use_format,
                                                 (void*)&value->value,
                                                 value->type,
                                                 index);
            }
            else
            {
                ret = -CDICT_ERROR_TO_STRING_TYPE_FAILED;
            }
        }
    }
    return ret;
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        