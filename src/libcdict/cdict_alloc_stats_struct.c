

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit


#include <stdio.h>

__CDict_Nonnull_some_arguments(1)
CDict_API_function
void cdict_alloc_stats_struct(struct cdict_t * const cdict)
{
    /*
     * Allocate memory for the cdict->stats structure
     */
    cdict->stats = __CDict_malloc(cdict,
                                  sizeof(struct cdict_stats_t));
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        