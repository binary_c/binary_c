

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit



/*
 * Make a new key of the given type, and return
 * a pointer to it
 */
struct cdict_key_t * cdict_new_key(struct cdict_t * const cdict,
                                   const union cdict_key_union keydata,
                                   const CDict_key_type keytype)
{
    struct cdict_key_t * const key =
        __CDict_calloc(cdict,
                       1,
                       sizeof(struct cdict_entry_t));
    cdict_fill_key(key,
                   cdict,
                   keydata,
                   keytype,
                   TRUE);
    return key;
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        