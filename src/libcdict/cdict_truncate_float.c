

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit


__CDict_Pure_function
float cdict_truncate_float(const float d,
                            const size_t nbytes)
{
    /*
     * Truncate float d to nbytes
     *
     * The union shares the float data, d, with
     * an 8-bit (one byte) integer, i. We assume
     * that sizeof(float) > sizeof(unit8_t)
     * which is true on any platform I know of.
     */
    union {
        float d; // full float number
        uint8_t i; // byte representation
    } u;

    /* set the float in the union */
    memcpy(&u.d, &d, sizeof(float));
    /* and clear the rest of the union, just in case */
    memset(&u.d+sizeof(u),0,sizeof(u)-sizeof(float));

    /*
     * This works on intel, probably depends on big-vs-little endian:
     * you have been warned!
     */
    for(size_t i=0; i<sizeof(u); i++)
    {
        const size_t offset = sizeof(u) - i - 1;
        *((uint8_t*)(&u.i + offset)) &= i<nbytes ? 0xff : 0x00;
    }
    return u.d;
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        