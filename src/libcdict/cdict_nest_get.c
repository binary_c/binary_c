

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit

#undef exit
#undef nestprint

/*
 * Get data from a nested cdict.
 *
 * Returns the entry that is set, or NULL on error.
 */

//#define nestprint(...) if(cdict->ancestor->vb==TRUE){printf(__VA_ARGS__);fflush(stdout);}
#define nestprint(...) /* do nothing */
//#define nestprint(...) printf(__VA_ARGS__);fflush(stdout);

__CDict_Nonnull_some_arguments(1,2)
CDict_API_function
struct cdict_entry_t *
cdict_nest_get(struct cdict_t * const cdict __CDict_maybe_unused,
                union cdict_key_union * paths __CDict_maybe_unused,
                const size_t npath __CDict_maybe_unused,
                CDict_key_type * pathtypenums __CDict_maybe_unused
    )
{    /*
      * Given a path, get the entry in a cdict.
      *
      * The path is a list of strings, e.g.
      *
      * "x", "y", "z"
     */
    nestprint("cdict_nest_get in %p, paths %p, npath %zu\n",
              (void*)cdict,
              (void*)paths,
              npath);

    struct cdict_t * h = NULL;
    const Boolean vb = cdict->vb;

    if(vb)
    {
        for(size_t i=0; i<npath; i++)
        {
            char * s;
            cdict_union_to_string(cdict,
                                   &paths[i],
                                   pathtypenums[i],
                                   NULL,
                                   &s);
            nestprint("%zu = %s \"%s\" -> ",
                      i,
                      __CDict_key_descriptor(pathtypenums[i]),
                      s);
            CDict_Safe_free(cdict,s);
        }
        nestprint("<data>\n");
    }

    /*
     * If not found in the cache, enter nested location
     * into the cdict
     */
    struct cdict_entry_t * entry = NULL;
    if(h == NULL)
    {
        /*
         * Check that the nest path exists
         */
        h = cdict;
        for(size_t i=0; i<npath; i++)
        {
            entry = cdict_contains(h,
                                   paths[i],
                                   pathtypenums[i]);

            if(entry != NULL)
            {
                /*
                 * we found a cdict
                 */
                h = entry->value.value.cdict_pointer_data;
                nestprint("Nest_get: At %zu : found cdict %p in entry %p\n",
                          i,
                          (void*)h,
                          (void*)entry);
            }
            else
            {
                /*
                 * No cdict found: return NULL
                 */
                h = NULL;
                i = npath + 1; /* break */
                nestprint("Nest_get: At %zu : no cdict found, return h = %p\n",
                          i,
                          (void*)h);
            }
        }
    }

    nestprint("return entry %p\n",(void*)entry);
    return entry;
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        