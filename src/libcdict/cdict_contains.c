

#include "../binary_c.h"
No_empty_translation_unit_warning;
#undef is_integer
#ifndef __HAVE_LIBCDICT__
#include "cdict.h"
#undef exit


__CDict_Pure_function
CDict_API_function
struct cdict_entry_t * cdict_contains(struct cdict_t * const cdict,
                                      const union cdict_key_union keydata,
                                      const CDict_key_type keytype)
{
    /*
     * If cdict contains an entry with the given key,
     * of the given keytype, return the entry.
     *
     * Return NULL if not found.
     *
     * If cdict is NULL, also return NULL.
     */
    if(cdict == NULL) return NULL;

    if(cdict->vb)
    {
        printf("cdict_contains : cdict %p %p : keytype %d\n",
               (void*)cdict,
               (void*)cdict->cdict_entry_list,
               keytype);
    }

    /*
     * Make a temporary key struct of the type "keytype"
     * using the keydata
     */
    struct cdict_key_t * key = __CDict_malloc(cdict,sizeof(struct cdict_key_t));
    memset(key,0,sizeof(struct cdict_key_t));
    cdict_fill_key(key,
                   cdict,
                   keydata,
                   keytype,
                   FALSE);

    /*
     * Do the find
     */
    if(cdict->vb)
    {
        printf("cdict_contains (pre): map has set string to \"%s\" : cdict %p contains? entry_list = %p, key = %p , keytype = %d; ",
               key->string,
               (void*)cdict,
               (void*)cdict->cdict_entry_list,
               (void*)key,
               keytype
            );
    }

    struct cdict_entry_t * target = NULL;
    CDICT_UTHASH_FIND(cdict,
                      CDICT_HANDLE,
                      cdict->cdict_entry_list,
                      key,
                      __CDict_sizeof(keytype),
                      target);
    if(cdict->vb)
    {
        printf("cdict_contains : (post) found %p\n",
               (void*)target);
        printf("cdict_contains : free tmp key %p string %p = \"%s\"\n",
               (void*)key,
               (void*)key->string,
               key->string ? key->string : "");
    }

    /*
     * free memory of temp key struct and return what was found
     */
    cdict_free_key(cdict,&key,TRUE);

    return target;
}

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
        