#include "../binary_c.h"
        #ifndef __HAVE_LIBCDICT__
        
#pragma once
#ifndef CDICT_SORT_AUTO_T
#define CDICT_SORT_AUTO_T

/*
 * CDict union scalar access
 */
#define CDict_key_scalar(KEY)                                           \
    (                                                                   \
        KEY->type == CDICT_DATA_TYPE_DOUBLE ? (double)KEY->key.double_data : \
        KEY->type == CDICT_DATA_TYPE_LONG_DOUBLE ? (long double)KEY->key.long_double_data : \
        KEY->type == CDICT_DATA_TYPE_INT ? (int)KEY->key.int_data  :    \
        KEY->type == CDICT_DATA_TYPE_BOOLEAN ? (Boolean)KEY->key.Boolean_data  : \
        KEY->type == CDICT_DATA_TYPE_LONG_INT ? (long int)KEY->key.long_int_data  : \
        KEY->type == CDICT_DATA_TYPE_LONG_LONG_INT ? (long long int)KEY->key.long_long_int_data  : \
        KEY->type == CDICT_DATA_TYPE_UNSIGNED_INT ? (unsigned int)KEY->key.unsigned_int_data : \
        KEY->type == CDICT_DATA_TYPE_UNSIGNED_LONG_INT ? (unsigned long int)KEY->key.unsigned_long_int_data  : \
        KEY->type == CDICT_DATA_TYPE_UNSIGNED_LONG_LONG_INT ? (unsigned long long int)KEY->key.unsigned_long_long_int_data  : \
        KEY->type == CDICT_DATA_TYPE_SHORT_INT ? (short int)KEY->key.short_int_data : \
        KEY->type == CDICT_DATA_TYPE_UNSIGNED_SHORT_INT ? (unsigned short int)KEY->key.char_data : \
        KEY->type == CDICT_DATA_TYPE_FLOAT ? (float)KEY->key.float_data : \
        KEY->type == CDICT_DATA_TYPE_CHAR ? (char)KEY->key.char_data :  \
        0                                                               \
        )

/*
 * CDict union pointer access: these are all cast to a void*\
 */
#define CDict_union_pointer(KEY)                                        \
    ((void*)(                                                           \
        KEY->type == CDICT_DATA_TYPE_STRING ? (void*)KEY->key.string_data : \
        KEY->type == CDICT_DATA_TYPE_INT_POINTER ? (void*)KEY->key.int_pointer_data : \
        KEY->type == CDICT_DATA_TYPE_LONG_INT_POINTER ? (void*)KEY->key.long_int_pointer_data : \
        KEY->type == CDICT_DATA_TYPE_LONG_LONG_INT_POINTER ? (void*)KEY->key.long_long_int_pointer_data : \
        KEY->type == CDICT_DATA_TYPE_DOUBLE_POINTER ? (void*)KEY->key.double_pointer_data : \
        KEY->type == CDICT_DATA_TYPE_LONG_DOUBLE_POINTER ? (void*)KEY->key.double_pointer_data : \
        KEY->type == CDICT_DATA_TYPE_VOID_POINTER ? (void*)KEY->key.void_pointer_data : \
        KEY->type == CDICT_DATA_TYPE_UNSIGNED_INT_POINTER ? (void*)KEY->key.unsigned_int_pointer_data : \
        KEY->type == CDICT_DATA_TYPE_SHORT_INT_POINTER ? (void*)KEY->key.short_int_pointer_data : \
        KEY->type == CDICT_DATA_TYPE_UNSIGNED_SHORT_INT_POINTER ? (void*)KEY->key.unsigned_short_int_pointer_data : \
        KEY->type == CDICT_DATA_TYPE_FLOAT_POINTER ? (void*)KEY->key.float_pointer_data : \
        NULL                                                            \
        ))

/*
 * Comparison macro for sort function
 */
#define __cdict_cmp(A,B) ((A) < (B) ? -1 : (A) > (B) ? +1 : 0)

/*
 * Debugging
 */
#define hprint(...) if(head==FALSE)printf("CDICT_SORT ");head=TRUE;printf(__VA_ARGS__);
#undef hprint

#endif // CDICT_SORT_AUTO_T

#endif // __HAVE_LIBCDICT__

typedef int prevent_ISO_C_warning;
       