#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "RLOF.h"

Boolean RLOF_mass_transferred_in_one_orbit(struct stardata_t * Restrict const stardata,
                                           const double mass_transfer_rate,
                                           struct RLOF_orbit_t * Restrict const RLOF_orbit)
{
    /*
     * Calculates the mass transferred in one orbit
     *
     * returns: SYSTEM_ACTION_CONTINUE_RLOF
     */
    RLOF_stars;

    /*
     * Mass lost by the donor in an orbit
     * is just the orbital period * mass loss rate.
     * NB this is negative (because it is mass loss)
     */
    RLOF_orbit->dM_RLOF_lose = -stardata->common.orbit.period *
        mass_transfer_rate;
    Dprint("dM_RLOF_lose = (-orbital_period=%g * mdot=%g) -> %g\n",
           -stardata->common.orbit.period,
           mass_transfer_rate,
           RLOF_orbit->dM_RLOF_lose);

    /*
     * For various reasons, you might want to limit the mass transfer
     * rate (which is dealt with as the mass transferred in a timestep)
     *
     * At this point in the RLOF algorithm, dM_RLOF_lose is the mass
     * lost in a single orbit (negative).
     */

    /*
     * We can never strip more than the stellar envelope from a
     * giant-like star in an orbital period.
     */
    if(GIANT_LIKE_STAR(donor->stellar_type))
    {
        Dprint("donor menv=%g mc=%g\n",Envelope_mass(ndonor),donor->core_mass[CORE_He]);
        RLOF_orbit->dM_RLOF_lose = - Min(-RLOF_orbit->dM_RLOF_lose,
                                         Envelope_mass(ndonor));

        if(donor->stellar_type==HERTZSPRUNG_GAP)
        {
            /*
             * limit to 1% of the fractional envelope
             * mass transferred in one go
             */
            //RLOF_orbit->dM_RLOF_lose *= Max(0.010,Envelope_mass(ndonor)/donor->mass);
        }
    }
    else if(0 && donor->stellar_type>=HeWD)
    {
        Dprint("Pre-modulate : %g\n",RLOF_orbit->dM_RLOF_lose);

        /*
         * This was in BSE, but I do not know why.
         */
        //RLOF_orbit->dM_RLOF_lose *= 1.0e+03/Max(donor->radius,1.0e-04);
        Dprint("Post-modulate : %g\n",RLOF_orbit->dM_RLOF_lose);

    }
    return SYSTEM_ACTION_CONTINUE_RLOF;
}
