#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Donor star's maximum rate limit : thermal
 */

double Pure_function RLOF_donor_thermal_rate_limit(const struct stardata_t * const stardata,
                                                   const struct star_t * const donor)
{
    const double thermal_rate = donor->mass / donor->tkh;
    return
        stardata->preferences->donor_limit_thermal_multiplier < -TINY ?
        VERY_LARGE_MASS_TRANSFER_RATE :
        (thermal_rate * stardata->preferences->donor_limit_thermal_multiplier);
}
