#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "RLOF.h"
#include "../binary_star_functions/donor_types.h"
double Pure_function Hachisu_max_rate(struct stardata_t * const stardata Maybe_unused,
                                      const struct star_t * const donor,
                                      const struct star_t * const accretor)
{
    /*
     * Calculate the maximum Hachisu accretion rate
     * in Msun/year.
     *
     * See Claeys et al. (C14) Eq. B.4
     */
    double rate;
    if(HYDROGEN_DONOR)
    {
        double X;
#ifdef NUCSYN
        X = donor->Xenv[XH1];
#else
        X = 0.7;
#endif
        rate =
            5.3e-7 *
            (1.7-X)/Max(1e-10,X) *
            Max(0.0, accretor->mass - 0.4);
    }
    else if(HELIUM_DONOR)
    {
        rate = 7.2e-6 * Max(0.0,accretor->mass - 0.6);
        /*
         * 1.585893192e-6, compiler will pre-compute this
         */
        rate = Max(rate,exp10(-5.8));
    }
    else
    {
        /* what to do for other donors? */
        rate = VERY_LARGE_MASS_TRANSFER_RATE;
    }


    rate = Max(1e-50,rate);

    return rate;
}
