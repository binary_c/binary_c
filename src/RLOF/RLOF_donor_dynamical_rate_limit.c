#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Donor star's maximum rate limit : dynamical
 */

double Pure_function RLOF_donor_dynamical_rate_limit(const struct stardata_t * const stardata,
                                                     const struct star_t * const donor)
{
    return
        stardata->preferences->donor_limit_dynamical_multiplier < -TINY ?
        VERY_LARGE_MASS_TRANSFER_RATE :
        (donor->mass / dynamical_timescale(donor) * stardata->preferences->donor_limit_dynamical_multiplier);

}
