#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "RLOF.h"
void RLOF_init_dM_orbit(struct stardata_t * Restrict const stardata,
                        struct RLOF_orbit_t * Restrict const RLOF_orbit)
{
    /*
     * Initialize the dM_orbit struct
     */
    if(RLOF_orbit==NULL)
    {
        Exit_binary_c(BINARY_C_ALLOC_FAILED,"Failed to allocate memory for RLOF_orbit struct");
    }
    Dprint("Zero dM_RLOF_ derivatives\n");
    RLOF_orbit->dM_RLOF_lose = 0.0;
    RLOF_orbit->dM_RLOF_transfer = 0.0;
    RLOF_orbit->dM_RLOF_accrete = 0.0;
    Star_number k;
    Starloop(k)
    {
        RLOF_orbit->dM_other[k] = 0.0;
    }
}
