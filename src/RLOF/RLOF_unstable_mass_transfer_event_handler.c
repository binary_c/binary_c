#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "RLOF.h"

Event_handler_function RLOF_unstable_mass_transfer_event_handler(void * eventp,
                                                                 struct stardata_t * const stardata,
                                                                 void * data)

{
    /*
     * Event handler to catch dynamical mass transfer from a low-mass
     * main sequence star, and act accordingly.
     */
    Dprint("event RLOF dyn MS %p %p %p\n",(void*)eventp,(void*)stardata,(void*)data);

    struct binary_c_event_t * event Maybe_unused = eventp;
    struct binary_c_unstable_RLOF_event_t * event_data =
        (struct binary_c_unstable_RLOF_event_t *) data;

    /*
     * Other events should be stopped, we should just let
     * unstable RLOF happen.
     */
    erase_events_of_type(
        stardata,
        BINARY_C_EVENT_ERASE_EVENTS,
        stardata->common.current_event
        );

    RLOF_unstable_mass_transfer(stardata,
                                event_data->instability,
                                TRUE);

    return NULL;
}
