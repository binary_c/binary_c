#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "RLOF.h"

/*
 * Determine whether the transferred material forms an accretion
 * disk around the secondary or hits the secondary in a direct
 * stream, by using eq.(1) of Ulrich & Burger (1976, ApJ, 206, 509)
 * fitted to the calculations of Lubow & Shu (1975, ApJ, 198, 383).
 *
 * In this function we detect whether there is a disk (returns TRUE)
 * or not (returns FALSE).
 */
Boolean Pure_function RLOF_test_for_accretion_disc(struct stardata_t * Restrict const stardata)
{
    const struct star_t * const accretor = & stardata->star[stardata->model.naccretor];

    const Boolean x = accretor->rmin > accretor->radius ?
        /*
         * Accretion stream misses the star, forms
         * a disk at ~1.7 rmin
         */
        TRUE :
        /*
         * Direct impact accretion
         */
        FALSE;

    //printf("Disc accretion? %s\n",Yesno(x));

    return x;
}
