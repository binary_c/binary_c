#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "RLOF.h"

Boolean Pure_function RLOF_overflowing(struct stardata_t * Restrict const stardata,
                                       const double fac)
{
    /*
     * Return TRUE if Roche-lobe overflow will happen (R>RL)
     * in either star.
     */
    Boolean overflowing = FALSE;
    Foreach_star(star)
    {
        const double RL = effective_Roche_radius(stardata,star);

        Dprint("t=%g STAR %d type %d : R=%g (effective) (RL = %g * fac = %g)=%g  : overflowing? %s\n",
               stardata->model.time,
               star->starnum,
               star->stellar_type,
               star->radius,
               RL,
               fac,
               RL * fac,
               Yesno(star->radius > RL * fac)
               );

        if(star->radius > RL * fac)
        {
            overflowing = TRUE;
            star->RLOFing = TRUE;
        }
        else
        {
            star->RLOFing = FALSE;
        }
    }
    return overflowing;

}
