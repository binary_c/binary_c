#pragma once

/*
 * The binary_c stellar population nucleosynthesis framework.
 *
 * Copyright Robert Izzard 2000-2025
 *
 * Contact rob.izzard@gmail.com
 *
 * Code repository:
 * https://gitlab.com/binary_c
 *
 * Documentation is in doc/
 * Code is in src/
 *
 * To build, run
 *
 * ---
 *
 * meson setup builddir
 * cd builddir
 * ninja binary_c_install
 *
 * ---
 *
 * Please see the files README, LICENCE and CHANGES,
 * and the doc/ directory for documentation which is mirrored
 * at https://binary_c.gitlab.io
 *
 **********************
 * Purpose:
 *
 * Collision matrix.
 *
 * This matrix determines the stellar type when two stars merge.
 * It is equivalent to the "ktypes" array in the original BSE.
 *
 * See Hurley et al 2002, MNRAS 329, 897, Table 2 for details.
 *
 **********************
 */
#define COLLISION_MATRIX {                                              \
        { 1, 1, 102, 103, 104, 105, 106, 4, 106, 106, 3, 6, 6, 13, 14 }, \
        { 1, 1, 102, 103, 104, 105, 106, 4, 106, 106, 3, 6, 6, 13, 14 }, \
        { 102, 102, 103, 103, 104, 104, 105, 104, 104, 104, 103, 105, 105, 113, 114 }, \
        { 103, 103, 103, 103, 104, 104, 105, 104, 104, 104, 103, 105, 105, 113, 114 }, \
        { 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 113, 114 }, \
        { 105, 105, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 104, 113, 114 }, \
        { 106, 106, 105, 105, 104, 104, 106, 104, 106, 106, 105, 106, 106, 113, 114 }, \
        { 4, 4, 104, 104, 104, 104, 104, 7, 108, 109, 7, 9, 9, 13, 14 }, \
        { 106, 106, 104, 104, 104, 104, 106, 108, 108, 109, 107, 109, 109, 113, 114 }, \
        { 106, 106, 104, 104, 104, 104, 106, 109, 109, 109, 107, 109, 109, 113, 114 }, \
        { 3, 3, 103, 103, 104, 104, 105, 7, 107, 107, 15, 9, 9, 13, 14 }, \
        { 6, 6, 105, 105, 104, 104, 106, 9, 109, 109, 9, 11, 12, 13, 14 }, \
        { 6, 6, 105, 105, 104, 104, 106, 9, 109, 109, 9, 12, 12, 13, 14 }, \
        { 13, 13, 113, 113, 113, 113, 113, 13, 113, 113, 13, 13, 13, 13, 14 }, \
        { 14, 14, 114, 114, 114, 114, 114, 14, 114, 114, 14, 14, 14, 14, 14 } \
    }

#define COLLISION_MATRIX_SIZE 15
