/*
 * Add a new event to the event stack.
 *
 * Note: This function should not be called directly.
 *       lease use the Add_new_event macro instead.
 *
 * Returns the number, on the stack, of the newly assigned
 * event (which is also the new stack size, hence is >= 0).
 *
 * Allocates and assigns memory for a binary_c_event_t struct
 * which it populations with a calling function and data.
 * The function cannot be NULL because it is to be called,
 * but the data can be.
 *
 * An event handler function should take stardata and
 * the data as arguments.
 *
 * The additional function call, erase_func, is used
 * to define a function that erases the event data properly.
 * This can be NULL in which case it is ignored.
 *
 * Note: if stardata->model.deny_new_events is TRUE
 *       then do nothing and return BINARY_C_EVENT_DENIED
 *       (which is -1 and hence can never be a new event number).
 *
 * Note: this function does NOT allocate the memory associated
 * with the event, it is up to the event setter to do this.
 */
#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "binary_c_events.h"

int new_event(const char * Restrict const caller,
              struct stardata_t * const stardata,
              const Event_type type,
              Event_handler_function func(EVENT_HANDLER_ARGS),
              Event_handler_function erase_func(EVENT_HANDLER_ARGS),
              void * data,
              const Boolean unique
    )
{
    Eprint("New event type %d (%s) from caller %s (data = %p, unique = %d, handler = %p, deny = %d)\n",
           type,
           Binary_c_event_string(type),
           caller,
           (void*)data,
           unique,
           Cast_function_pointer(func),
           stardata->model.deny_new_events);
    //if(stardata->model.model_number >= 7790 && stardata->model.model_number <= 7792) Backtrace;

    if(stardata->model.deny_new_events == TRUE)
    {
        /*
         * We're not allowed to add a new event
         */
        Eprint("Event has been denied : deny_new_events==TRUE (intermediate? %d, reject? %u) \n",
               stardata->model.intermediate_step,
               stardata->model.reject_shorten_timestep
            );
        return BINARY_C_EVENT_DENIED;
    }
    else if(unique == TRUE &&
            events_pending_of_type(stardata,type) > 0)
    {
        /*
         * If this event is supposed to be unique, and there
         * are events of this type already on the queue,
         * don't allow overpopulation
         */
        Eprint("Event has been denied : events of type %d should be unique and this is not\n",type);
        return BINARY_C_EVENT_DENIED;
    }
    else
    {

        /*
         * n is the new event number
         */
        const int n = stardata->common.n_events;

        /*
         * Resize the stack to include an extra event.
         *
         * A Realloc here is slow, we could introduce a
         * stack that doubles in size each time, but remember
         * an event happens rarely, and even more rarely is
         * there more than one event, so this is not going to be
         * a bottleneck.
         */
        Eprint("realloc size %zu onto %p\n",
               (size_t)((n+1)*sizeof(struct binary_c_event_t *)),
               (void*)stardata->common.events);
        stardata->common.events = Realloc(
            stardata->common.events,
            (stardata->common.n_events+1)*
            sizeof(struct binary_c_event_t *)
            );

        /*
         * Allocate memory for a new event on the stack
         */
        struct binary_c_event_t * const event =
            stardata->common.events[stardata->common.n_events] =
            Malloc(sizeof(struct binary_c_event_t));

        /*
         * Increase the stack counter
         */
        stardata->common.n_events++;

        /*
         * Set the function and data associated with this event
         */
        Eprint("New event : from %p caller %s\n",
               Cast_function_pointer(func),caller);
        event->func = func;
        event->erase_func = erase_func;
        event->data = (void*)data;
        event->caller = (char*)caller;
        event->type = type;

        /*
         * Return the new event's counter
         */
        Eprint("return event's counter: %d\n",n);
        return n;
    }
}
