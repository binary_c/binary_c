#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Generic event handler : calls the appropriate function
 * given the type of the passed in event
 */
Event_handler_function generic_event_handler(
    void * const eventp Maybe_unused,
    struct stardata_t * const stardata,
    void * data Maybe_unused)
{
    struct binary_c_event_t * event = eventp;
    switch(event->type)
    {
    case BINARY_C_EVENT_SUPERNOVA0:
    case BINARY_C_EVENT_SUPERNOVA1:
        supernova_event_handler(eventp,stardata,data);
        break;
    case BINARY_C_EVENT_NOVA:
        nova_event_handler(eventp,stardata,data);
        break;
    case BINARY_C_EVENT_WIND:
        break;
    case BINARY_C_EVENT_COMMON_ENVELOPE:
        common_envelope_event_handler(eventp,stardata,data);
        break;
    case BINARY_C_EVENT_RLOF_START:
        break;
    case BINARY_C_EVENT_CONTACT_SYSTEM:
        contact_system_event_handler(eventp,stardata,data);
        break;
    case BINARY_C_EVENT_UNSTABLE_RLOF:
        RLOF_unstable_mass_transfer_event_handler(eventp,stardata,data);
        break;
    default:
        break;
    }
    return NULL;
}
