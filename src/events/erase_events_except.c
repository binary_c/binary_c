#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "binary_c_events.h"

void erase_events_except(struct stardata_t * Restrict const stardata,
                         const Event_counter not_this_one)
{
    /*
     * Erase all pending events except
     */
    Eprint("Erase events except %u (starting stack length %u)\n",
           not_this_one,
           stardata->common.n_events);

    /*
     * Erase all events except not_this_one, but do
     * not change the stack order while doing this
     */
    for(Event_counter i=0;i<stardata->common.n_events;i++)
    {
        if(i != not_this_one)
        {
            Eprint("Erase event %u\n",i);
            erase_event(stardata,
                        &stardata->common.events[i]);
        }
    }

    /*
     * Move not_this_one to position 0, and shrink the stack
     * to size 1.
     */
    Eprint("Move event %u to 0\n",
           not_this_one);
    stardata->common.events[0] = stardata->common.events[not_this_one];
    stardata->common.n_events = 1;
    stardata->common.events = Realloc(
        stardata->common.events,
        stardata->common.n_events*
        sizeof(struct binary_c_event_t*)
        );
}
