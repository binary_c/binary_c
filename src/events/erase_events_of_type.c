#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "binary_c_events.h"

void erase_events_of_type(struct stardata_t * Restrict const stardata,
                          const Event_type type,
                          const struct binary_c_event_t * Restrict const except)
{
    /*
     * Erase any pending events of a given type without
     * triggering them, except even "except" which is
     * a pointer to an event that must not be deleted.
     *
     * If type is BINARY_C_EVENT_ERASE_EVENTS then any type is
     * removed (that is not excepted, see below).
     *
     * If except==NULL, it is ignored.
     *
     * Normally, except is set to stardata->common.current_event
     * so the current event cannot be erased while it's being
     * processed.
     */
    Eprint("Erase events of type %d %s (except %p) n = %u\n",
           type,
           Binary_c_event_string(type),
           (void*)except,
           stardata->common.n_events);

    Event_counter i;
    for(i=0;i<stardata->common.n_events;i++)
    {
        struct binary_c_event_t * event = stardata->common.events[i];
        const Boolean do_erase = Boolean_((event != NULL) &&
                                          (except == NULL || event != except) &&
                                          (type==BINARY_C_EVENT_ERASE_EVENTS || event->type == type));

        Eprint("event %u type %d want to match %d : non-NULL? %s : is exception? %s : of given type? %s -> do_erase? %s\n",
               i,
               event ? event->type : -1,
               type,
               Yesno(event != NULL),
               Yesno(event == except),
               Yesno(event ? (event->type == type) : 0),
               Yesno(do_erase));

        if(do_erase == TRUE)
        {
            /*
             * Do the erasing
             */
            Eprint("call erase event\n");
            erase_event(stardata,&event);

            stardata->common.events[i] = NULL;

            /*
             * Shift the stack at the current location
             */
            shift_event_stack(stardata,i);
        }
    }
}
