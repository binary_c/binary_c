#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "binary_c_events.h"

void trigger_event(struct stardata_t * const stardata,
                   const Event_counter i)
{
    /*
     * Trigger the event at position i
     */
    struct binary_c_event_t * const event = stardata->common.events[i];
    stardata->common.had_event = TRUE;
    stardata->common.current_event = event;

    Eprint("Event 0/%u = %p (func=%p data=%p)\n",
           stardata->common.n_events,
           (void*)event,
           Cast_function_pointer(event->func),
           (void*)event->data
        );

    /*
     * If it is non-NULL, call the events
     *  handler function with stardata
     * and the event data.
     */
    if(*event->func!=NULL)
    {
        Eprint("call func with (stardata = %p, data = %p)\n",
               (void*)stardata,
               (void*)event->data);
        (*event->func)(event,
                       stardata,
                       event->data);
        Eprint("post func call : event = %p\n",(void*)event);
    }
    else
    {
        Eprint("event handler function is NULL : do not call\n");
    }

    Eprint("free the data at event = %p\n",(void*)event);
    Eprint("with data %p\n",(void*)(event!=NULL ? event->data : NULL));

    /*
     * Free the data (which may be NULL)
     */
    if(event != NULL)
    {
        Safe_free(event->data);
    }

    Eprint("free the event\n");

    /*
     * Free the event that has been caught, although don't NULL it
     */
    Unsafe_free(event);

    /*
     * Shift down the stack at i
     */
    shift_event_stack(stardata,i);

    stardata->common.current_event = NULL;
}
