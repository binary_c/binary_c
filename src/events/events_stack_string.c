/*
 * Return a string which describes the event stack.
 *
 * The string needs to be freed when you are finished
 * with it.
 */

#include "../binary_c.h"
No_empty_translation_unit_warning;


char * events_stack_string(struct stardata_t * Restrict const stardata)
{

#define __format "%u=%p=[type=%d=%s func=%p data=%p caller=%s] "
#define __args                                  \
    k,                                          \
        (void*)ev,                              \
        ev->type,                               \
        Binary_c_event_string(ev->type),        \
        __extension__(void*)ev->func,           \
        ev->data,                               \
        ev->caller
    struct binary_c_event_t * ev;
    Event_counter k;
    size_t n = 0;
    for(k=0; k<stardata->common.n_events; k++)
    {
#undef sprintf
        ev = stardata->common.events[k];
        n += snprintf(NULL,
                      0,
                      __format,
                      __args);
    }

    char * c = n >0 ?  Malloc(sizeof(char)*n) : NULL;
    char * c0 = c;
    if(n>0)
    {
        for(k=0; k<stardata->common.n_events; k++)
        {
            ev = stardata->common.events[k];
            c += snprintf(c,
                          c0 + n - c,
                          __format,
                          __args);
        }
    }
    return c0;
}
