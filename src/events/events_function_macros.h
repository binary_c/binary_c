#pragma once
#ifndef EVENTS_FUNCTION_MACROS_H
#define EVENTS_FUNCTION_MACROS_H

/*
 * Macro to add an event to the stack
 */
#define Add_new_event(STARDATA,                 \
                      TYPE,                     \
                      HANDLER,                  \
                      ERASE_HANDLER,            \
                      DATA,                     \
                      UNIQUE)                   \
                                                \
    new_event(                                  \
        (char *)__func__,                       \
        (STARDATA),                             \
        (TYPE),                                 \
        (HANDLER),                              \
        (ERASE_HANDLER),                        \
        (void*)(DATA),                          \
        (UNIQUE)                                \
        )


/*
 * Macro to deny/stop denial
 */
#define Set_event_denial(B)                     \
    {                                           \
        if(0)printf("set event denial %d in %s\n",      \
               B,                               \
               (char*)__func__);                \
        stardata->model.deny_new_events = (B);  \
    }

#endif // EVENTS_FUNCTION_MACROS_H
