#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "binary_c_events.h"

void events_clean_log_stack(struct stardata_t * const stardata)
{
    /*
     * Clean the events log stack
     */
    Dprint("clean log stack of %u events\n",
           stardata->common.event_log_stack_n);

    for(Event_counter n = 0; n < stardata->common.event_log_stack_n; n++)
    {
        struct binary_c_event_log_t * elog = (struct binary_c_event_log_t * )
            stardata->common.event_log_stack[n];
        if(elog != NULL)
        {
            /*
             * Free the event log data:
             * if we have a custom function, freefunc,
             * use it,
             */
            if(elog->freefunc != NULL)
            {
                elog->freefunc((void**)&elog->data);
            }
            else
            {
                Safe_free(elog->data);
            }
            Safe_free(elog);
        }
    }
    Safe_free(stardata->common.event_log_stack);
    stardata->common.event_log_stack_n = 0;
}
