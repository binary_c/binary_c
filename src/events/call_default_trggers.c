#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "binary_c_events.h"

void call_default_triggers(struct stardata_t * Restrict const stardata)
{
    /*
     * binary_c's default events handlers
     */
    const int vb = 0;
    Event_counter k;

    Eprint("check event stack\n");
    if(vb)
    {
        for(k=0; k<stardata->common.n_events; k++)
        {
            Eprint("EVENT STACK IN t=%g %d : %u/%u %p : event[%u] = %p\n",
                   stardata->model.time,
                   stardata->model.model_number,
                   k,
                   stardata->common.n_events,
                   (void*)stardata->common.events,
                   k,
                   (void*)stardata->common.events[k]);
        }
    }

    stardata->common.had_event =
        Boolean_(stardata->common.n_events > 0);

    Eprint("Call %u event triggers\n",
           stardata->common.n_events);
    for(Event_counter i=0;i<stardata->common.n_events;i++)
    {
        Eprint("%u : %p type %d %s\n",
               i,
               (void*)stardata->common.events[i],
               stardata->common.events[i]->type,
               Binary_c_event_string(stardata->common.events[i]->type));
    }

    /*
     * Trigger events in order
     */
    Boolean erase = FALSE;
    for(int j=BINARY_C_EVENT_NONE+1; j<BINARY_C_EVENT_NUMBER; j++)
    {
        Eprint("Have %u pending events of type %d %s\n",
               events_pending_of_type(stardata,j),
               j,
               Binary_c_event_string(j));
    }

    for(int j=BINARY_C_EVENT_NONE+1; j<BINARY_C_EVENT_NUMBER; j++)
    {
        while(erase == FALSE &&
              events_pending_of_type(stardata,j) > 0)
        {
            for(Event_counter i=0;i<stardata->common.n_events && erase==FALSE;i++)
            {
                if(stardata->common.events[i]->type == j)
                {
                    Eprint("Trigger event %u of type %d %s\n",
                           i,
                           j,
                           Binary_c_event_string(j));
                    /*
                     * If only this event should happen,
                     * delete all others and reset stack position to 0
                     */
                    if(unique_event[j])
                    {
                        Eprint("event is unique so setting erase = TRUE which prevents all other triggers\n");
                        erase = TRUE;

                        Eprint("erasing all subsequent events except this one\n");
                        erase_events_except(stardata,i);

                        /*
                         * restart the loop
                         */
                        i = 0;
                        j = 0;
                    }

                    /*
                     * Call the trigger
                     */
                    trigger_event(stardata,
                                  i);

                    /*
                     * Break out of both the for and while loops
                     */
                    if(erase == TRUE)
                    {
                        break;
                    }
                }
            }
        }
    }

    if(vb)
    {
        if(stardata->common.n_events)
        {
            for(k=0; k<stardata->common.n_events; k++)
            {
                Eprint("EVENT STACK OUT t=%g %d : %u/%u %p : event[%u] = %p\n",
                       stardata->model.time,
                       stardata->model.model_number,
                       k,
                       stardata->common.n_events,
                       (void*)stardata->common.events,
                       k,
                       (void*)stardata->common.events[k]);
            }
        }
        else
        {
            Eprint("EVENT STACK OUT empty\n");
        }
    }
}
