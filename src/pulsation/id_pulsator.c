#include "../binary_c.h"
No_empty_translation_unit_warning;
#include "pulsation_macros.h"

void id_pulsator(struct stardata_t * const stardata,
                 double logTeff,
                 double logL,
                 Pulsator_ID * const n,
                 Pulsator_ID ** ids)
{
    /*
     * Make a list of pulsation modes applicable to this star
     *
     * Data is from Simon Jeffery's HR diagram
     *
     * Returns n : the number of pulsator_ellipses that match
     *         ids : the IDs of the pulsator_ellipses that match
     */

    /*
     * First, Assume we have no pulsator_ellipses
     */
    *n = 0;
    *ids = NULL;

    /*
     * Next, loop to detect pulsator_ellipses
     */
    Pulsator_ID i;
    for(i=0; i<stardata->store->num_pulsator_ellipses; i++)
    {
        if(in_pulsator_ellipse(logTeff,
                               logL,
                               &stardata->store->pulsator_ellipses[i]) == TRUE)
        {
            /*
             * Pulsator!
             */
            *ids = Realloc(*ids,
                           sizeof(Pulsator_ID) * (*n+1));
            if(*ids)
            {
                (*ids)[*n] = i;
                (*n)++;
            }
            else
            {
                Exit_binary_c(BINARY_C_MALLOC_FAILED,
                              "Failed to Realloc ids array in id_pulsator().");
            }
        }
    }
}
