#pragma once
#ifndef PULSATION_MACROS_H
#define PULSATION_MACROS_H


/*
 * Data defines ellispses:
 * logTeff, logL, a, b, angle (radians)
 *
 * a is the horizontal (logTeff) dimension
 * b is the vertical (logL) dimension
 * the angle is *clockwise* rotation in radians, but this is
 * converted automatically
 */

#define CSJ_PULSATOR_LIST                                               \
    X(                   DELTA_CEPHI,      "δ Cep",    3.76,      3.5,       0.04,         1, 0.0523599) \
    X(                    W_VIRGINIS,      "W Vir",    3.79,      2.8,       0.04,         1, 0.0523599) \
    X(                     BL_BOOTIS,     "BL Boo",    3.86,      2.5,       0.03,       0.3, 0.0698132) \
    X(                      RR_LYRAE,     "RR Lyr",    3.86,      1.6,      0.045,       0.2, 0.0523599) \
    X(                   DELTA_SCUTI,      "δ Sct",    3.89,      1.2,      0.035,      0.25,         0) \
    X(                 GAMMA_DORADUS,      "γ Dor",    3.86,     0.95,       0.02,      0.25,         0) \
    X(        RAPIDLY_OSCILLATING_AP,       "roAp",    3.93,     1.25,   0.356931, 0.0447211,    1.3734) \
    X(                    SOLAR_LIKE, "solar-like",    3.81,      0.4,   0.500899,      0.04,   1.51087) \
    X(                     XI_HYDRAE,      "ξ Hyd",    3.71,     1.45,   0.450444, 0.0799999,   1.61521) \
    X(                            SR,         "SR",    3.65,     2.15,   0.450444, 0.0800002,   1.61521) \
    X(                          MIRA,       "Mira",   3.585,     3.25,   0.750816, 0.0986154,   1.61743) \
    X(                   BETA_CEPHEI,      "β Cep",    4.36,      3.9,       0.06,       0.7,  -0.15708) \
    X(                     PERSEI_53,     "53 Per",     4.2,      2.8,      0.035,      0.55,  -0.15708) \
    X(                    V361_HYDRA,   "V361 Hya",    4.52,      1.4,       0.05,      0.22,         0) \
    X(                V1093_HERCULES,  "V1093 Her",    4.43,      1.4,       0.05,       0.2,         0) \
    X( BLUE_LARGE_AMPLITUDE_PULSATOR,       "BLAP",    4.53,      1.3,       0.04,       1.3,      0.02) \
    X(                      V652_HER,   "V652 Her",    4.34,      3.2, 0.00999975,       0.1,   3.14159) \
    X(                 PV_TELESCOPII,     "PV Tel",    4.15,        4,       0.35,       0.2,   3.14159) \
    X(            R_CORONAE_BOREALIS,      "R CrB",     3.8,        4,  0.0999999,       0.2,   3.14159) \
    X(                   ALPHA_CYGNI,      "α Cyg",    4.25,     4.55,        0.3,      0.25,   3.14159) \
    X(       LUBMINOUS_BLUE_VARIABLE,        "LBV",     4.3,      5.2,        0.4,       0.3,   3.14159) \
    X(                        RV_TAU,     "RV Tau",     3.7,     4.55,        0.2,  0.206155,         0) \
    X(                      V366_AQR,   "V366 Aqr",    4.59,      1.7,       0.04,      0.14,     -0.15) \
    X(                   SUBDWARF_OV,       "sdOV",     4.7,      2.1,       0.06,      0.28,     -0.15) \
    X(                       GW_VIR1,    "GW Vir1",    4.95,      1.1,       0.06,       0.7,     -0.08) \
    X(                       GW_VIR2,    "GW Vir2",    5.05,      3.6,       0.11,       0.3,       0.1) \
    X(                       HOT_DAV,    "hot-DAV",   4.495,    -0.85,    0.25045,      0.05,   1.51087) \
    X(                           DBV,        "DBV",     4.4,   -1.275,   0.378319, 0.0657647,   1.43825) \
    X(                           DQV,        "DQV",     4.3,    -1.65,   0.250799,      0.04,   1.49097) \
    X(                           DAV,        "DAV",   4.065,    -2.55,   0.254018, 0.0502493,    1.3927) \
    X(                       EHM_DAV,    "EHM-DAV", 4.08279, -3.22723,  0.0300002, 0.0999999,         0) \
    X(                       ELM_HeV,    "ELM-HeV",    4.06,      0.3,  0.0300002,       0.1,         0) \
    X(                       ELM_DAV,    "ELM-DAV",    3.98,     -1.8,       0.02,       0.2,         0)

#undef X
#define X(MACRO,STRING,LOGTEFF,LOGL,A,B,P)      \
    PULSATOR_##MACRO ,
enum{ CSJ_PULSATOR_LIST };

#undef X


#endif // PULSATION_MACROS_H
