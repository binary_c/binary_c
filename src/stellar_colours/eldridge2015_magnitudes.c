/*
 * Calculate stellar colours according to Eldridge's 2015 table
 * for the Basel library and for O stars.
 */
#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef STELLAR_COLOURS

#include "stellar_colour_macros.h"
//#include "eldridge2015-ostar.h"
//#include "eldridge2015-basel.h"
#define ELDRIDGE2015_LENGTH_BASEL 29900
#define ELDRIDGE2015_METADATA_BASEL 3,20,1300
#define ELDRIDGE2015_LENGTH_OSTAR 25300
#define ELDRIDGE2015_METADATA_OSTAR 3,20,1100

/*
 * magnitudes should be an array :
 * double magnitudes[STELLAR_MAGNITUDE_NUMBER]
 */
void Nonnull_all_arguments eldridge2015_magnitudes(struct stardata_t * const stardata,
                                                   struct star_t * const star,
                                                   double * const magnitudes)
{

    //Const_data_table basel[ELDRIDGE2015_LENGTH_BASEL] = {ELDRIDGE2015_DATA_ARRAY_BASEL};
    extern double _binary_eldridge2015_basel_dat_start[];
    //extern double _binary_eldridge2015_ostar_dat_start[];

    Const_data_table * basel = _binary_eldridge2015_basel_dat_start;
    //Const_data_table * ostars = _binary_eldridge2015_basel_dat_start;

    //Const_data_table ostars[ELDRIDGE2015_LENGTH_OSTAR] = {ELDRIDGE2015_DATA_ARRAY_OSTAR};
    const double param[3] = {
        log10(stardata->common.metallicity/0.02),
        log10(Teff_from_star_struct(star)),
        logg(star),
    };
    double mags[20];

#ifdef CDEBUG
    const char **c = stellar_magnitude_strings;
#endif

    rinterpolate(basel,
                 stardata->persistent_data->rinterpolate_data,
                 ELDRIDGE2015_METADATA_BASEL,
                 param,
                 mags,
                 0);

    /*
     * Adjust for luminosity: the colours assume L = 1 Lsun.
     */
    const double ll =
        magnitude_from_luminosity(star->luminosity) +
        - magnitude_from_luminosity(1.0);

#define _Set_mag(FROM,TO)                       \
    magnitudes[TO] = mags[FROM] + ll;

    /* Johnson colours (Vega) */
    _Set_mag(ELDRIDGE_U,STELLAR_MAGNITUDE_U);
    _Set_mag(ELDRIDGE_B,STELLAR_MAGNITUDE_B);
    _Set_mag(ELDRIDGE_V,STELLAR_MAGNITUDE_V);
    _Set_mag(ELDRIDGE_R,STELLAR_MAGNITUDE_R);
    _Set_mag(ELDRIDGE_I,STELLAR_MAGNITUDE_I);


    /* 2MASS */
    _Set_mag(ELDRIDGE_J,STELLAR_MAGNITUDE_J);
    _Set_mag(ELDRIDGE_H,STELLAR_MAGNITUDE_H);
    _Set_mag(ELDRIDGE_K,STELLAR_MAGNITUDE_K);

    /*
     * SDSS (AB)
     */
    _Set_mag(ELDRIDGE_u,STELLAR_MAGNITUDE_u);
    _Set_mag(ELDRIDGE_g,STELLAR_MAGNITUDE_g);
    _Set_mag(ELDRIDGE_r,STELLAR_MAGNITUDE_r);
    _Set_mag(ELDRIDGE_i,STELLAR_MAGNITUDE_i);
    _Set_mag(ELDRIDGE_z,STELLAR_MAGNITUDE_z);

    /*
     * Convert to Vega according to
     * http://www.astronomy.ohio-state.edu/~martini/usefuldata.html
     * (taken from Blanton et al., 2007)
     */
    magnitudes[STELLAR_MAGNITUDE_u] += + 0.91;
    magnitudes[STELLAR_MAGNITUDE_g] += - 0.08;
    magnitudes[STELLAR_MAGNITUDE_r] += + 0.16;
    magnitudes[STELLAR_MAGNITUDE_i] += + 0.37;
    magnitudes[STELLAR_MAGNITUDE_z] += + 0.54;

    /*
     * Hubble filters
     */
    _Set_mag(ELDRIDGE_f300w,STELLAR_MAGNITUDE_f300w);
    _Set_mag(ELDRIDGE_f336w,STELLAR_MAGNITUDE_f336w);
    _Set_mag(ELDRIDGE_f435w,STELLAR_MAGNITUDE_f435w);
    _Set_mag(ELDRIDGE_f450w,STELLAR_MAGNITUDE_f450w);
    _Set_mag(ELDRIDGE_f555w,STELLAR_MAGNITUDE_f555w);
    _Set_mag(ELDRIDGE_f606w,STELLAR_MAGNITUDE_f606w);
    _Set_mag(ELDRIDGE_f814w,STELLAR_MAGNITUDE_f814w);

#ifdef CDEBUG
    printf("\}\n");
#endif
}


#endif // STELLAR_COLOURS
