#pragma once

#ifndef YBC_ARRAYS_H
#define YBC_ARRAYS_H

/*
 * Expand a list of VA_ARGS and process
 * each to set the info-> variables from
 * a list of metadata.
 */

#define YBC_Process_Metadata__c(A,B) YBC_Process_Metadata__c1(A,B)
#define YBC_Process_Metadata__c1(A,B) YBC_Process_Metadata__c2(A,B)
#define YBC_Process_Metadata__c2(A,B) A##B

#define YBC_Process_Metadata__do0(...)

#define YBC_Process_Metadata_debug FALSE

#define YBC_Process_Metadata__do1(VAR,...)                  \
    {                                                       \
        const ssize_t nstrings = double_array->n;           \
        if(YBC_Process_Metadata_debug)                      \
        {                                                   \
            printf("Set info->%s = metadata[%zd] = %g\n",   \
                   #VAR,                                    \
                   nstrings-1-0,                            \
                   metadata[nstrings-1-0]);                 \
        }                                                   \
        info->VAR = metadata[nstrings-1-0];                 \
    }
#define YBC_Process_Metadata__do2(VAR,...)                  \
    {                                                       \
        const ssize_t nstrings = double_array->n;           \
        if(YBC_Process_Metadata_debug)                      \
        {                                                   \
            printf("Set info->%s = metadata[%zd] = %g\n",   \
                   #VAR,                                    \
                   nstrings-2,                              \
                   metadata[nstrings-2]);                   \
        }                                                   \
        info->VAR = metadata[nstrings-2];                   \
    }                                                       \
    YBC_Process_Metadata__do1(__VA_ARGS__)
#define YBC_Process_Metadata__do3(VAR,...)                  \
    {                                                       \
        const ssize_t nstrings = double_array->n;           \
        if(YBC_Process_Metadata_debug)                      \
        {                                                   \
            printf("Set info->%s = metadata[%zd] = %g\n",   \
                   #VAR,                                    \
                   nstrings-3,                              \
                   metadata[nstrings-3]);                   \
        }                                                   \
        info->VAR = metadata[nstrings-3];                   \
    }                                                       \
    YBC_Process_Metadata__do2(__VA_ARGS__)
#define YBC_Process_Metadata__do4(VAR,...)                  \
    {                                                       \
        const ssize_t nstrings = double_array->n;           \
        if(YBC_Process_Metadata_debug)                      \
        {                                                   \
            printf("Set info->%s = metadata[%zd] = %g\n",   \
                   #VAR,                                    \
                   nstrings-1-3,                            \
                   metadata[nstrings-4]);                   \
        }                                                   \
        info->VAR = metadata[nstrings-4];                   \
    }                                                       \
    YBC_Process_Metadata__do3(__VA_ARGS__)

#define YBC_Process_Metadata__do5(VAR,...)                  \
    {                                                       \
        const ssize_t nstrings = double_array->n;           \
        if(YBC_Process_Metadata_debug)                      \
        {                                                   \
            printf("Set info->%s = metadata[%zd] = %g\n",   \
                   #VAR,                                    \
                   nstrings-5,                              \
                   metadata[nstrings-5]);                   \
        }                                                   \
        info->VAR = metadata[nstrings-5];                   \
    }                                                       \
    YBC_Process_Metadata__do4(__VA_ARGS__)

#define YBC_Process_Metadata__do6(VAR,...)                  \
    {                                                       \
        const ssize_t nstrings = double_array->n;           \
        if(YBC_Process_Metadata_debug)                      \
        {                                                   \
            printf("Set info->%s = metadata[%zd] = %g\n",   \
                   #VAR,                                    \
                   nstrings-6,                              \
                   metadata[nstrings-6]);                   \
        }                                                   \
        info->VAR = metadata[nstrings-6];                   \
    }                                                       \
    YBC_Process_Metadata__do5(__VA_ARGS__)

#define YBC_Process_Metadata__do7(VAR,...)                  \
    {                                                       \
        const ssize_t nstrings = double_array->n;           \
        if(YBC_Process_Metadata_debug)                      \
        {                                                   \
            printf("Set info->%s = metadata[%zd] = %g\n",   \
                   #VAR,                                    \
                   nstrings-7,                              \
                   metadata[nstrings-7]);                   \
        }                                                   \
        info->VAR = metadata[nstrings-7];                   \
    }                                                       \
    YBC_Process_Metadata__do6(__VA_ARGS__)

#define YBC_Process_Metadata__do8(VAR,...)                  \
    {                                                       \
        const ssize_t nstrings = double_array->n;           \
        if(YBC_Process_Metadata_debug)                      \
        {                                                   \
            printf("Set info->%s = metadata[%zd] = %g\n",   \
                   #VAR,                                    \
                   nstrings-8,                              \
                   metadata[nstrings-8]);                   \
        }                                                   \
        info->VAR = metadata[nstrings-8];                   \
    }                                                       \
    YBC_Process_Metadata__do7(__VA_ARGS__)

#define YBC_Process_Metadata__do9(VAR,...)                  \
    {                                                       \
        const ssize_t nstrings = double_array->n;           \
        if(YBC_Process_Metadata_debug)                      \
        {                                                   \
            printf("Set info->%s = metadata[%zd] = %g\n",   \
                   #VAR,                                    \
                   nstrings-9,                              \
                   metadata[nstrings-9]);                   \
        }                                                   \
        info->VAR = metadata[nstrings-9];                   \
    }                                                       \
    YBC_Process_Metadata__do8(__VA_ARGS__)

#define YBC_Process_Metadata__proc(N,...)                               \
    YBC_Process_Metadata__c(YBC_Process_Metadata__do,N)(__VA_ARGS__)

#define YBC_Process_Metadata(...)                   \
    YBC_Process_Metadata__proc(NARGS(__VA_ARGS__),  \
                               __VA_ARGS__)


#endif// YBC_ARRAYS_H
