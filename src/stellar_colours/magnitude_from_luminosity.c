#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef STELLAR_COLOURS

/*
 * convert luminosity (in L_SUN) to absolute magnitude
 *
 * Note: this is the definition of resolution B2 of the
 * International Astronomical Union.
 */

Constant_function double magnitude_from_luminosity(const double luminosity)
{
    return -2.5*log10(luminosity*L_SUN/BOLOMETRIC_L0);
}

#endif // STELLAR_COLOURS
