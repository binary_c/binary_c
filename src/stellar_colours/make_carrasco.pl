#!/usr/bin/env perl
use strict;
use rob_misc;
use rinterpolate;

foreach my $file ('table3.dat',
                  'table4.dat',
                  'table5.dat')
{
    my $h = $file;
    $h=~s/\.dat$/.h/;
    $h='carrasco2014_cooling_'.$h;
    print $h;

    my $data = slurp($file);
    $data=~s/^\s+//gm;
    $data=~s/ +/,/g;
    $data=~s/$/,\\/gm;
    $data=~s/,\\\s*,\\$//;
    $data.="\n";

    my $H = $h;
    $H=~s/.h$//;
    $H=uc($H);
    $data = "#define $H ".$data."\n";

    dumpfile($h,$data);
}
