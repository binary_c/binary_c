#include "../binary_c.h"
No_empty_translation_unit_warning;


void Nonnull_all_arguments unresolved_stellar_magnitudes(struct stardata_t * const stardata,
                                                         double ** magnitudes_p)
{
    /*
     * Calculate unresolved binary magnitudes
     */
    if(*magnitudes_p == NULL)
    {
        *magnitudes_p = Calloc(STELLAR_MAGNITUDE_NUMBER,
                               sizeof(double));
    }
    double * const magnitudes = *magnitudes_p;

    /*
     * Get each star's magnitudes
     */
    double ** stellar_mags =
        stardata->tmpstore->stellar_magnitudes;

    Boolean luminous_star[NUMBER_OF_STARS] = {FALSE, FALSE};
    Star_number k;
    Starloop(k)
    {
        if(Is_luminous(k))
        {
            luminous_star[k] = TRUE;
            stellar_magnitudes(stardata,
                               &stardata->star[k],
                               &stellar_mags[k]);
        }
    }

    /*
     * Combine the magnitudes. In band x, we have for flux in the band x,
     *
     * m_x = -2.5 log10(flux / reference flux)
     *
     *     = -2.5 log10(flux) + 2.5 log10(reference flux)
     *
     *     = -2.5 log10(flux/Lsun) + 2.5 log10(Lsun) + 2.5 log10(reference flux)
     *
     *
     * So
     *
     *  log10(flux) = - m_x / 2.5 + log10(reference flux)
     *
     *  flux = 10 ^ (- m_x / 2.5 + log10(reference flux))
     *
     *       = 10 ^ (-m_x/2.5) * (reference flux)
     *
     *
     * In a binary we have
     *
     * flux = (flux1 + flux2)
     *
     *      = (10^(-m_x1/2.5) + 10^(-m_x2/2.5)) * reference_flux
     *
     *      = 10 ^ (-m_X/2.5) * reference_flux
     *
     *
     * Hence
     *
     * 10^(-m_x1/2.5) + 10^(-m_x2/2.5) = 10^(-m_X/2.5)
     *
     * m_X = -2.5 * log10(10^(-m_x1/2.5) + 10^(-m_x2/2.5))
     *
     *
     * We can also use the add_magnitudes functions to do this.
     */

    /*
      magnitudes[i] = - 2.5 * log10(
      exp10( - stellar_magnitudes[0][i]/2.5) +
      exp10( - stellar_magnitudes[1][i]/2.5)
      );
    */
    Stellar_magnitude_index i;
    if(stardata->preferences->zero_age.multiplicity == 1 &&
       luminous_star[0])
    {
        /*
         * Can only be a single star : do not include
         * the secondary (which is pretending to be a
         * low-mass main sequence star).
         */
        Magnitude_loop(i)
        {
            magnitudes[i] = stellar_mags[0][i];
        }
    }
    else
    {
        if(luminous_star[0])
        {
            if(luminous_star[1])
            {
                /*
                 * Both stars are luminous
                 */
                Magnitude_loop(i)
                {
                    magnitudes[i] =
                        add_magnitudes(stellar_mags[0][i],
                                       stellar_mags[1][i]);
                }
            }
            else
            {
                /*
                 * Only star 0 is luminous
                 */
                Magnitude_loop(i)
                {
                    magnitudes[i] = stellar_mags[0][i];
                }
            }
        }
        else if(luminous_star[1])
        {
            /*
             * Only star 1 is luminous
             */
            Magnitude_loop(i)
            {
                magnitudes[i] = stellar_mags[1][i];
            }
        }
        else
        {
            /*
             * Neither star is luminous : set to
             * STELLAR_MAGNITUDE_UNPHYSICAL
             */
            Magnitude_loop(i)
            {
                magnitudes[i] = STELLAR_MAGNITUDE_UNPHYSICAL;
            }
        }
    }
}
