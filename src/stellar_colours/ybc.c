#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef YBC
#include <fitsio.h>
#include <dirent.h>
#include "ybc_arrays.h"

/*
 * Functions to access the YBC bolometric corrections
 * database.
 *
 * Astronomy & Astrophysics, Volume 632, id.A105;
 * doi:10.1051/0004-6361/201936612
 *
 * Many thanks to Yang Chen for demo code, what's below
 * is a rewritten-from-scratch, stripped-down version
 * that follows binary_c code standards.
 *
 * You can find the YBC data files at
 * https://gitlab.com/cycyustc/ybc_tables
 *
 * You need to have git-lfs installed to get them.
 * On Ubuntu run:
 *
 ***
 * sudo apt-get install git-lfs
 * git lfs install
 * git clone https://gitlab.com/cycyustc/ybc_tables
 * cd ybc_tables
 * git lfs pull
 ***
 *
 * The original YBC code is at
 * git@gitlab.com:cycyustc/ybccodepub.git
 *
 * and the following code borrows ideas heavily
 * from this, although all the binary_c YBC loader
 * is new and the interpolation is done using
 * librinterpolate rather than internally to the
 * YBC routines.
 *
 * Note: YBC_ROTATING and YBC_LIMB_DARKENING are not yet
 *       officially supported (but may work).
 */

/*
 * Set YBC_debug to TRUE if you want to
 * show debug output
 */
static const Boolean YBC_debug = FALSE;

struct YBC_FITSfile_info_t ** YBC_load(struct stardata_t * const stardata Maybe_unused,
                                       const int instrument,
                                       char * const prefix,
                                       char * const dir Maybe_unused,
                                       char ** wanted_columns,
                                       const int wanted_ncolumns)
{
    /*
     * Load YBC data for a particular instrument (e.g. GAIA)
     * as defined in YBC_INSTRUMENTS_LIST.
     *
     * Function input:
     *
     * stardata : binary_c's main structure
     * instrument : binary_c's index for this instrument
     * prefix : a directory prefix string
     * dir : the directory containing the FITS files
     * (set in YBC_INSTRUMENTS_LIST)
     * wanted_columns : an array of strings which are the
     *                  columns to be extracted from YBC.
     * (set in YBC_INSTRUMENTS_LIST)
     * wanted_ncolumns: the number of strings in wanted_columns.
     * (set in YBC_INSTRUMENTS_LIST)
     *
     * If wanted_columns is NULL, or wanted_ncolumns is zero,
     * we extract all the columns from the file.
     *
     * Returns a list of YBC_FITSfile_info_t structs, one
     * for each data file.
     */

    /*
     * Construct YBC directory and get the information
     * about each of the files.
     */
    char * dirname;
    size_t nfiles;
    struct YBC_FITSfile_info_t ** infos;
    if(asprintf(&dirname,
                "%s/%s",
                stardata->preferences->YBC_path,
                dir) > 0)
    {
        YBCdebug("call YBC_analyse_files in dir %s, prefix %s\n",
                 dirname,
                 prefix);
        infos = YBC_analyse_files(stardata,
                                  dirname,
                                  prefix,
                                  &nfiles);
        YBCdebug("infos %p has %zu files\n",
                 (void*)infos,
                 nfiles);
    }
    else
    {
        Exit_binary_c(BINARY_C_ALLOC_FAILED,
                      "Could not make string with asprintf.");
    }

    if(infos)
    {
        YBCdebug("Found %zu files\n",nfiles);

        /*
         * Loop over the files and combine their
         * data appropriate for interpolation.
         */
        YBC_combine_files(stardata,
                          instrument,
                          infos,
                          nfiles,
                          wanted_columns,
                          wanted_ncolumns);

        /*
         * Free the info structs
         */
        YBC_free_info_structs(&infos,nfiles);
    }
    Safe_free(dirname);

    return NULL;
}

void YBC_free_info_structs(struct YBC_FITSfile_info_t *** infos_p,
                           const size_t nfiles)
{
    /*
     * Free a list of info structs
     */
    struct YBC_FITSfile_info_t ** const infos = *infos_p;
    for(size_t i=0;i<nfiles;i++)
    {
        YBC_free_info_struct(&infos[i]);
    }
    Safe_free(*infos_p);
}

void YBC_free_info_struct(struct YBC_FITSfile_info_t ** info_p)
{
    /*
     * Free the info struct and its contents
     */
    if(info_p != NULL)
    {
        struct YBC_FITSfile_info_t * const info = *info_p;
        Safe_free(info->filepath);
        Safe_free(info->filename);
        Safe_free(info->dirpath);
        Safe_free(info->dirpath_with_prefix);
        Safe_free(info->prefix);
        Safe_free(info->dataset);
        Safe_free(info->table->data);
        Safe_free_nocheck(info->table);
        Safe_free(*info_p);
    }
}


void YBC_load_fitsfile(struct stardata_t * const stardata,
                       struct YBC_FITSfile_info_t * const info,
                       char ** wanted_columns,
                       int wanted_ncolumns,
                       const Boolean filter_null_data)
{
    /*
     * Load YBC FITS file data.
     *
     * Return the associated YBC file info struct, or NULL on error.
     *
     * If we have already loaded this file, just return.
     */
    if(info->table->data != NULL)
    {
        return;
    }

    /*
     * We haven't already loaded it, so now we must
     */
    int status = 0;
    fitsfile * fp_fits;
    {
        if(fits_open_file(&fp_fits,
                          info->filepath,
                          READONLY,
                          &status) != 0
           || status != 0)
        {
            /*
             * Error : file open failed
             */
            Exit_binary_c(BINARY_C_FILE_OPEN_ERROR,
                          "Could not open FITS file \"%s\" status=%d\n",
                          info->filepath,
                          status);
        }
    }
    YBCdebug("opened FITS file %s\n",
             info->filepath);

    /*
     * Move to second header data unit
     */
    {
        int hdutype;
        status = 0;
        if(fits_movabs_hdu(fp_fits,
                           2,
                           &hdutype,
                           &status) != 0
           || status != 0)
        {
            Exit_binary_c(BINARY_C_FILE_READ_ERROR,
                          "Error reading FITS file \"%s\" (NAXIS) status=%d : failed to move to second header data unit.\n",
                          info->filepath,
                          status);
        }
    }

    /*
     * Read NAXIS1 and NAXIS2 keywords
     */
    int nfound;
    status = 0;
    if(fits_read_keys_lng(fp_fits,
                          "NAXIS",
                          1,
                          2,
                          info->naxes,
                          &nfound,
                          &status) != 0
       || status != 0)
    {
        Exit_binary_c(BINARY_C_FILE_READ_ERROR,
                      "Error reading FITS file \"%s\" (NAXIS) status=%d : failed to read NAXIS1 and NAXIS2.\n",
                      info->filepath,
                      status);
    }

    YBCdebug("status %d, nfound %d : Fits file \"%s\" has axes %ld and %ld\n",
             status,
             nfound,
             info->filepath,
             info->naxes[0],
             info->naxes[1]);

    /*
     * Read in key information
     */

#define X(VAR, CTYPE, FITSTYPE, NAME)                                   \
    status = 0;                                                         \
    if(fits_read_key(fp_fits,                                           \
                     FITSTYPE,                                          \
                     NAME,                                              \
                     &info->VAR,                                        \
                     NULL,                                              \
                     &status) || status != 0)                           \
    {                                                                   \
        Exit_binary_c(BINARY_C_FILE_READ_ERROR,                         \
                      "Error reading FITS file \"%s\" status=%d : failed to read %s (C variable info->%s, C type %s, FITS type %s.\n", \
                      info->filepath,                                   \
                      status,                                           \
                      #NAME,                                            \
                      #VAR,                                             \
                      #CTYPE,                                           \
                      #FITSTYPE);                                       \
    }
    FITS_KEY_LIST;
#undef X

    /*
     * info->naxes[1] is the number of lines of data in the table
     */
    YBCdebug("Ntot %d, naxes %ld %ld\n",info->Ntot,info->naxes[0],info->naxes[1]);

    /*
     * Read in column name strings and match to wanted columns
     */
    char ** column_names = Malloc(sizeof(char*)*info->Ntot);
    Boolean * want_column = Malloc(sizeof(Boolean)*info->Ntot);
    const Boolean want_all_columns = !(wanted_ncolumns > 0 && wanted_columns != NULL);
    int new_wanted_ncolumns;
    if(want_all_columns)
    {
        /*
         * We want an interpolation table of all data columns
         */
        for(int i=0; i<info->Ntot; i++)
        {
            want_column[i] = TRUE;
        }
        wanted_ncolumns = info->Ntot;
        new_wanted_ncolumns = info->Ntot;
    }
    else
    {
        new_wanted_ncolumns = 0;
    }

    /*
     * Get column names, match to wanted_columns
     */
    for(int i=0; i<info->Ntot; i++)
    {
        /*
         * Note that columns are named TTYPE%d where
         * the integer starts counting from 1, hence i+1
         * below.
         */
        char * name;
        if(asprintf(&name,
                    "TTYPE%d",
                    i+1) == 0)
        {
            Exit_binary_c(BINARY_C_ALLOC_FAILED,
                          "Could not make string with asprintf.");
        }

        column_names[i] = Malloc(sizeof(char)*COLUMN_NAME_STRING_LENGTH);
        status = 0;
        if(fits_read_key(fp_fits,
                         TSTRING,
                         name,
                         column_names[i],
                         NULL,
                         &status) != 0
           || status != 0)
        {
            Exit_binary_c(BINARY_C_FILE_READ_ERROR,
                          "Error reading FITS file \"%s\" : %s not found (status %d)",
                          info->filepath,
                          name,
                          status);
        }
        YBCdebug("col %d \"%s\" -> \"%s\" (len %zu)\n",i,name,column_names[i],strlen(column_names[i]));
        Safe_free(name);

        if(want_all_columns == FALSE)
        {
            /*
             * We always want the interpolation parameters
             * in columns 0 .. info->naxis - 1, usually these
             * are columns:
             * 0 : logTeff
             * 1 : logg (or logRt for the PoWR sets)
             */
            want_column[i] = i < info->naxis ? TRUE : FALSE;

            /*
             * Try to match strings in wanted_columns[]
             * to this column
             */
            for(int j=0; j<wanted_ncolumns; j++)
            {
                if(Strings_equal(wanted_columns[j],
                                 column_names[i]))
                {
                    want_column[i] = TRUE;
                }
            }

            /*
             * Count for later mallocs
             */
            if(want_column[i] == TRUE)
            {
                YBCdebug("WANT COLUMN %d\n",i);
                new_wanted_ncolumns++;
            }
        }
    }

    /*
     * Allocate memory so we can copy the FITS file
     * content into an array of doubles ("data") which
     * will go into a data_table_t struct.
     */
    wanted_ncolumns = new_wanted_ncolumns;
    const size_t s_float = info->naxes[1] * sizeof(double);
    double * data = Malloc(wanted_ncolumns * info->naxes[1] * sizeof(float *));
    float * tmp = Malloc(s_float);

    /*
     * Read in arrays of floats and convert to doubles
     */

    /*
     * i is the column in the data table
     */
    for(int i=0, di=0; i<info->Ntot; i++)
    {
        if(want_column[i] == TRUE)
        {
            status = 0;
            if(fits_read_col(fp_fits,
                             TFLOAT,
                             i+1,
                             1,
                             1,
                             info->naxes[1],
                             NULL, // nullval : ignore
                             tmp,
                             NULL, // anynul : ignore
                             &status) != 0
               || status != 0)
            {
                Exit_binary_c(BINARY_C_FILE_READ_ERROR,
                              "Error reading FITS file \"%s\" : fits_read_col returned status %d",
                              info->filepath,
                              status);
            }

            /*
             * Convert float to double precision in data[]
             */
            for(int j=0; j<info->naxes[1]; j++)
            {
                /* put tmp[j] in data[di][j] */
                *(data + wanted_ncolumns * j + di) = (double) tmp[j];
            }
            di++;
        }
    }


    /*
     * Free memory
     */
    Safe_free(tmp);
    for(int i=0; i<info->Ntot; i++)
    {
        Safe_free(column_names[i]);
    }
    Safe_free(column_names);
    Safe_free(want_column);

    /*
     * Close FITS file
     */
    fits_close_file(fp_fits,
                    &status);

    /*
     * Make binary_c data_table_t
     */
    NewDataTable_from_Pointer(data,
                              info->table,
                              info->naxis,
                              wanted_ncolumns - info->naxis,
                              info->naxes[1]);


    /*
     * Check for NaNs, if there are any we  either filter them
     * or exit.
     */
    if(check_data_table_for_nans(info->table))
    {
        if(filter_null_data == YBC_FILTER_NULL_DATA)
        {
            /*
             * Try to filter out NULL data
             */
            struct data_table_t * const t = info->table;
            const size_t width = t->ndata + t->nparam;
            Boolean * badline = Malloc(t->nlines * sizeof(Boolean));
            Boolean found_nan;

            for(size_t i=0; i<t->nlines; i++)
            {
                badline[i] = FALSE;
                for(size_t j=0; j<width; j++)
                {
                    if(isnan(*(t->data + width * i + j)))
                    {
                        badline[i] = TRUE;
                        found_nan = TRUE;
                        j = width;
                    }
                }
            }

            if(found_nan == TRUE)
            {
                /*
                 * Count the good lines
                 */
                size_t n_good_lines = 0;
                for(size_t i=0; i<t->nlines; i++)
                {
                    if(badline[i] == FALSE)
                    {
                        n_good_lines++;
                    }
                }

                /*
                 * Make new data with only the good lines
                 */
                double * const newdata = Malloc(sizeof(double)*n_good_lines*width);
                const size_t linesize = sizeof(double) * width;
                for(size_t i=0, newi=0; i<t->nlines; i++)
                {
                    if(badline[i] == FALSE)
                    {
                        memcpy(newdata + newi * width,
                               data + i * width,
                               linesize);
                        newi++;
                    }
                }

                /*
                 * Replace the previous data pointer with the
                 * new (filtered) data, and update the number of lines
                 * of filtered data.
                 */
                Safe_free(t->data);
                t->data = newdata;
                t->nlines = n_good_lines;
            }

            Safe_free(badline);
        }
        else
        {
            /*
             * Exit on error
             */
            show_data_table(info->table);
            Exit_binary_c(BINARY_C_ALGORITHM_BRANCH_FAILURE,
                          "YBC just-loaded table from \"%s\" has nans\n",
                          info->filepath);
        }
    }

    //show_data_table(info->table);

    YBCdebug("table from \"%s\" : nparam %zu, ndata %zu, nlines %zu\n",
             info->filepath,
             info->table->nparam,
             info->table->ndata,
             info->table->nlines);

    return;
}

void load_YBC_tables_into_persistent_data(struct stardata_t * const stardata,
                                          char * const prefix)
{
    /*
     * Load YBC tables into the store
     */

    YBCdebug("load YBC prefs path %s persistent_data %p persistent tables %p path %s\n",
             stardata->preferences->YBC_path,
             (void*)stardata->persistent_data,
             (void*)stardata->persistent_data->YBC_tables,
             stardata->persistent_data->YBC_path);

    if(
        /*
         * ... if preferences path is non-NULL
         */
        //stardata->preferences->YBC_path != NULL &&

        /*
         * And persistent_data->YBC_path is NULL
         * or has changed
         */
        (stardata->persistent_data->YBC_tables == NULL ||
         stardata->persistent_data->YBC_path == NULL ||
         !Strings_equal(stardata->persistent_data->YBC_path,
                        stardata->preferences->YBC_path)))
    {
        if(stardata->persistent_data->YBC_tables != NULL)
        {
            /*
             * YBC path has changed: free everything
             * previously loaded before reload.
             */
            YBCdebug("Path changed : free old YBC tables");
            YBC_free_persistent_data_memory(stardata->persistent_data);
        }

        /*
         * Load required tables
         */
        YBCdebug("Load required tables\n");

        if(stardata->persistent_data->YBC_path != NULL)
        {
            Safe_free(stardata->persistent_data->YBC_path);
        }
        stardata->persistent_data->YBC_path = strdup(stardata->preferences->YBC_path);

#undef X
#define _mag_macros(...) /* do nothing */
#define X(ID,DIR,FITS_COLUMNS_ARRAY,MAGNITUDE_ARRAY)                    \
        YBCdebug("Check YBC_use_instrument[%d] ID=%s (stardata %p tmpstore %p)\n", \
                 YBC_INSTRUMENT_##ID,                                   \
                 #ID,                                                   \
                 (void*)stardata,                                       \
                 (void*)stardata ? (void*)stardata->tmpstore : NULL     \
            );                                                          \
        YBCdebug("is %d\n",                                             \
                 stardata->tmpstore->YBC_use_instrument[YBC_INSTRUMENT_##ID]); \
                                                                        \
        if(stardata->tmpstore->YBC_use_instrument[YBC_INSTRUMENT_##ID]) \
        {                                                               \
            char * cols[] = {FITS_COLUMNS_ARRAY};                       \
            const int instrument = YBC_INSTRUMENT_##ID;                 \
            YBCdebug("Load table \"%s\" at position %d\n",              \
                     #ID,                                               \
                     instrument);                                       \
            YBC_load(stardata,                                          \
                     instrument,                                        \
                     prefix,                                            \
                     DIR,                                               \
                     cols,                                              \
                     Array_size(cols));                                 \
            YBCdebug("loaded table \"%s\" from dir \"%s\" in position %d at %p\n", \
                     #ID,                                               \
                     DIR,                                               \
                     YBC_INSTRUMENT_##ID,\
                     (void*)stardata->persistent_data->YBC_tables[YBC_INSTRUMENT_##ID]); \
        }

        if(stardata->tmpstore->YBC_use_instrument != NULL)
        {
            YBC_INSTRUMENTS_LIST;
        }
#undef X
    }
    else
    {
        YBCdebug("Use existing tables\n");
    }
}

double YBC_magnitudes(struct stardata_t * const stardata,
                      struct star_t * const star,
                      double * const magnitudes)
{
    /*
     * Compute YBC magnitudes for this star
     *
     * The magnitudes are set in the *magnitudes array
     * with the appropriate binary_c indices.
     *
     * Returns: the table index to match Chen et al. (2019)
     * Fig. 1.
     */

    /*
     * First, make sure the tables are loaded
     */
    load_YBC_tables_into_persistent_data(stardata,
                                         YBC_PREFIX);

    const double mbol = magnitude_from_luminosity(star->luminosity);
    const double teff = Teff_from_star_struct(star);
    const double log_teff = log10(teff);
    const double log_g = logg(star);
    const double mdot_wind = Max(0.0,-star->derivative[DERIVATIVE_STELLAR_MASS_WIND_LOSS]);
    const double log10mdot = log10(mdot_wind);
    const double M_H = log10(stardata->common.metallicity / 0.02);
#ifdef NUCSYN
    double *Xobs = nucsyn_observed_surface_abundances(star);
    const double C_O = Xobs[XC12]/12.0 / Xobs[XO16]/16.0;
#endif//NUCSYN

#undef X
#define X(TYPE,...) #TYPE,
    static char * YBC_combined_library_strings[] Maybe_unused = { YBC_COMBINED_LIBRARIES };
#undef X

    /*
     * Transformed radius (Eq.11 of Chen+2019,
     * used for PoWR models) : this is taken from
     * Chen's code, what are the units?
     */
    const double vinf = 74.0 + 2.145 * sqrt(star->mass/star->radius)*617.0; // get from binary_c?
    const double log_Rt = Is_zero(mdot_wind) ? -100.0 :
        (log10(star->radius * pow(vinf * 4e-8/mdot_wind,2.0/3.0)));

    YBCdebug("YBC mags logteff %g logg %g logRt %g mbol %g\n",
             log_teff,
             log_g,
             log_Rt,
             mbol);

    /*
     * Choose tables to use for interpolation
     * (section 3.10 of Chen+ 2019 A&A https://arxiv.org/pdf/1910.09037.pdf
     *
     * f is the interpolation parameter. f=0 means use table 0, f=1 means use table 1.
     */
    double params[2][YBC_COMBINED_MAXNPARAM];
    int table[2];
    double f;
    if(teff > 6500.0)
    {
        table[0] = YBC_COMBINED_LIBRARY_ATLAS9;
        params[0][0] = M_H;
        params[0][1] = log_teff;
        params[0][2] = log_g;
        table[1] = YBC_COMBINED_LIBRARY_ATLAS9;
        params[1][0] = M_H;
        params[1][1] = log_teff;
        params[1][2] = log_g;
        f = 0.0;
    }
    else if(teff < 5500.0)
    {
        table[0] = YBC_COMBINED_LIBRARY_PHOENIX;
        params[0][0] = M_H;
        params[0][1] = log_teff;
        params[0][2] = log_g;
        table[1] = YBC_COMBINED_LIBRARY_PHOENIX;
        params[1][0] = M_H;
        params[1][1] = log_teff;
        params[1][2] = log_g;
        f = 0.0;
    }
    else
    {
        table[0] = YBC_COMBINED_LIBRARY_ATLAS9;
        params[0][0] = M_H;
        params[0][1] = log_teff;
        params[0][2] = log_g;
        table[1] = YBC_COMBINED_LIBRARY_PHOENIX;
        params[1][0] = M_H;
        params[1][1] = log_teff;
        params[1][2] = log_g;
        f = (teff - 5500.0) / (6500.0 - 5500.0);
    }

#ifdef NUCSYN
    if(C_O <= 1.0 &&
       teff < 4850.0 &&
       log_g < 1.5) // in the paper this was > 1.5 : wrong sign?
    {
        table[0] = YBC_COMBINED_LIBRARY_COMARCS_MS;
        params[0][0] = M_H;
        params[0][1] = log_teff;
        params[0][2] = log_g;
        table[1] = YBC_COMBINED_LIBRARY_COMARCS_MS;
        params[1][0] = M_H;
        params[1][1] = log_teff;
        params[1][2] = log_g;
        f = 0.0;
    }
    /*
     * TODO :
     * interpolate PHOENIX and COMARCS_MS
     * "where they overlap" -> how do we do this?!
     */
    /*
      if(C_O <= 1.0 && teff < 4850.0 && log_g > 1.5 &&
      teff < 5500.0)
      {
      table[0] = YBC_COMBINED_LIBRARY_PHOENIX;
      params[0][0] = M_H;
      params[0][1] = log_teff;
      params[0][2] = log_g;
      table[1] = YBC_COMBINED_LIBRARY_COMARCS_MS;
      params[1][0] = M_H;
      params[1][1] = log_teff;
      params[1][2] = log_g;
      stardata->persistent_data->YBC_tables[instrument][table[i]]->
      const double fT = ;
      const double fg = ;
      f = Min(fT,fg);
      }
    */
    if(C_O > 1.0 && teff < 4500.0 && log_g < 2.5)
    {
        table[0] = YBC_COMBINED_LIBRARY_COMARCS_C;
        params[0][0] = M_H;
        params[0][1] = C_O;
        params[0][2] = log_teff;
        params[0][3] = log_g;
        table[1] = YBC_COMBINED_LIBRARY_COMARCS_C;
        params[1][0] = M_H;
        params[1][1] = C_O;
        params[1][2] = log_teff;
        params[1][3] = log_g;
        f = 1.0;
    }
#endif//NUCSYN
    if(teff > 2e4 && mdot_wind > 1e-8)
    {
        table[0] = YBC_COMBINED_LIBRARY_WM_BASIC;
        params[0][0] = stardata->common.metallicity;
        params[0][1] = log10mdot;
        params[0][2] = log_teff;
        params[0][3] = log_g;
        table[1] = YBC_COMBINED_LIBRARY_WM_BASIC;
        params[1][0] = stardata->common.metallicity;
        params[1][1] = log10mdot;
        params[1][2] = log_teff;
        params[1][3] = log_g;
        f = 0.0;
    }
    if(! WHITE_DWARF(star->stellar_type) &&
       (teff > 1e5
#ifdef NUCSYN
        || Xobs[XH1] < 0.8 * stardata->preferences->zero_age.XZAMS[XH1]
        || Xobs[XH1] < 0.65
#endif
           ))
    {
        table[0] = YBC_COMBINED_LIBRARY_PoWR_WNL_WNE;
        params[0][0] = stardata->common.metallicity;
#ifdef NUCSYN
        params[0][1] = Xobs[XH1];
#else
        params[0][1] = 0.7;
#endif
        params[0][2] = log_teff;
        params[0][3] = log_Rt;
        table[1] = YBC_COMBINED_LIBRARY_PoWR_WNL_WNE;
        params[1][0] = stardata->common.metallicity;
#ifdef NUCSYN
        params[1][1] = Xobs[XH1];
#else
        params[1][1] = 0.7;
#endif
        params[1][2] = log_teff;
        params[1][3] = log_Rt;
        f = 0.0;
    }
#ifdef NUCSYN
    /*
     * Note: we check that the stellar type
     * is not white dwarf, which is not done
     * in Chen et al. (2019).
     */
    if(! WHITE_DWARF(star->stellar_type) &&
       Xobs[XC12]/12.0 > Xobs[XN14]/14.0 &&
       Xobs[XC12] > 0.05)
    {
        table[0] = YBC_COMBINED_LIBRARY_PoWR_WNE_WC;
        params[0][0] = stardata->common.metallicity;
        params[0][1] = Xobs[XHe4];
        params[0][2] = log_teff;
        params[0][3] = log_Rt;
        table[1] = YBC_COMBINED_LIBRARY_PoWR_WNE_WC;
        params[1][0] = stardata->common.metallicity;
        params[1][1] = Xobs[XHe4];
        params[1][2] = log_teff;
        params[1][3] = log_Rt;
        f = 0.0;
    }
#endif//NUCSYN
    if(log_g > 6.0 &&
       teff > 6300.00)
    {
        table[0] = YBC_COMBINED_LIBRARY_KOESTER;
        params[0][0] = log_teff;
        params[0][1] = log_g;
        table[1] = YBC_COMBINED_LIBRARY_KOESTER;
        params[1][0] = log_teff;
        params[1][1] = log_g;
        f = 0.0;
    }

    const double ff[2] = { f, 1.0 - f };

    YBCdebug("GAIAX Interpolate tables %d and %d, f = %g and %g\n",
             table[0],
             table[1],
             ff[0],
             ff[1]);

    /*
     * Loop over instruments to obtain each requested
     * set of filters.
     */

#undef _mag_macros
#define _mag_macros(...) Prefix_macros(STELLAR_MAGNITUDE_YBC_, __VA_ARGS__)

#undef X
#define X(ID,DIR,STRING_ARRAY,MAGNITUDE_IDS)                            \
    {                                                                   \
        const int instrument = YBC_INSTRUMENT_##ID;                     \
        YBCdebug("instrument %d use? %s\n",                             \
                 instrument,                                            \
                 Truefalse(stardata->tmpstore->YBC_use_instrument[instrument])); \
        if(stardata->tmpstore->YBC_use_instrument[instrument])          \
        {                                                               \
            const Stellar_magnitude_index mags[] = {MAGNITUDE_IDS};     \
            const size_t nmags = Array_size(mags);                      \
            const size_t result_size = sizeof(double)*nmags;            \
            double preresult[2][Array_size(mags)];                      \
            double result[Array_size(mags)];                            \
                                                                        \
            YBCdebug("i loop\n");                                       \
            for(int i=0;i<2;i++)                                        \
            {                                                           \
                YBCdebug("i=%d ff[%d] = %g\n",i,i,ff[i]);               \
                if(Is_not_zero(ff[i]))                                  \
                {                                                       \
                    YBCdebug("%p %p %p %p",                             \
                             (void*)stardata,                           \
                             stardata ? (void*)stardata->persistent_data : NULL, \
                             stardata->persistent_data ? (void*)stardata->persistent_data->YBC_tables : NULL, \
                             stardata->persistent_data->YBC_tables ? (void*)stardata->persistent_data->YBC_tables[instrument] : NULL); \
                    struct rinterpolate_table_t * const r =             \
                        stardata->persistent_data->YBC_tables[instrument][table[i]]; \
                    YBCdebug("r = %p\n",(void*)r);                      \
                    YBCdebug("Rinterpolate YBC_tables[%d = %s][%d = %s] \n",\
                             instrument,                                \
                             #ID,                                       \
                             table[i],                                  \
                             YBC_combined_library_strings[table[i]]);   \
                    YBCdebug("%s ",(stardata && stardata->persistent_data && stardata->persistent_data->YBC_tables[instrument][table[i]]) ? stardata->persistent_data->YBC_tables[instrument][table[i]]->label : NULL); \
                    YBCdebug("= %p (data at %p)\n",                     \
                             (void*)r,                                  \
                             r?(void*)r->data:NULL);                    \
                                                                        \
                    Rinterpolate(                                       \
                        r,                                              \
                        params[i],                                      \
                        preresult[i],                                   \
                        0                                               \
                        );                                              \
                    YBCdebug("Done Rinterpolate\n");                    \
                }                                                       \
                else                                                    \
                {                                                       \
                    /* set result to zero to avoid valgrind errors */   \
                    memset(preresult[i],                                \
                           0,                                           \
                           result_size);                                \
                }                                                       \
            }                                                           \
                                                                        \
            /* interpolate */                                           \
            memset(result,                                              \
                   0,                                                   \
                   result_size);                                        \
            for(int i=0;i<2;i++)                                        \
            {                                                           \
                for(size_t j=0;j<nmags;j++)                             \
                {                                                       \
                    result[j] += ff[i] * preresult[i][j];               \
                }                                                       \
            }                                                           \
                                                                        \
            /* set binary_c magnitudes array */                         \
            for(size_t j=0; j<nmags; j++)                               \
            {                                                           \
                magnitudes[mags[j]] = mbol - result[j];                 \
                YBCdebug("set mag %zu/%zu = [%u] = %g - %g = %g\n",     \
                         j,                                             \
                         nmags,                                         \
                         mags[j],                                       \
                         mbol,                                          \
                         result[j],                                     \
                         magnitudes[mags[j]]);                          \
            }                                                           \
        }                                                               \
    }

    if(stardata->tmpstore->YBC_use_instrument != NULL)
    {
        YBC_INSTRUMENTS_LIST;
    }
#undef X

    YBCdebug("returning from YBC_magnitudes\n");
    const double YBCf = YBC_library_type_float(table,
                                               f,
                                               log10mdot);

    return YBCf;
}


struct YBC_FITSfile_info_t ** YBC_analyse_files(struct stardata_t * const stardata Maybe_unused,
                                                char * const dirpath,
                                                char * const prefix,
                                                size_t * const size)
{
    struct YBC_FITSfile_info_t ** infolist = NULL;

    /*
     * Determine YBC data set
     */
    char * const c0 = strrchr(dirpath,'/');
    char * c1 = c0 - 1;
    while(c1 > dirpath && *c1 != '/')
    {
        c1--;
    }
    if(c0 && c1)
    {
        c1++;
        Safe_free(stardata->persistent_data->YBC_table_dataset);
        stardata->persistent_data->YBC_table_dataset = strndup(c1,c0-c1);
        YBCdebug("Set table dataset \"%s\"\n",
                 stardata->persistent_data->YBC_table_dataset);
    }
    else
    {
        Exit_binary_c(BINARY_C_FILE_OPEN_ERROR,
                      "Could not determine YBC dataset from directory \"%s\" because I could not find the final / ... this is an error.",
                      dirpath);
    }


    /*
     * Loop over files
     */
    char * dirpath_with_prefix;
    if(asprintf(&dirpath_with_prefix,
                "%s/%s",
                dirpath,
                prefix) > 0)
    {
        YBCdebug("DIRPATH %s\n ",dirpath_with_prefix);
        *size = 0;
        DIR * dfp = opendir(dirpath_with_prefix);
        if(dfp)
        {
            YBCdebug("opened dir %p\n",(void*)dfp);
            struct dirent *dp;
            while ((dp = readdir(dfp)) != NULL)
            {
                YBCdebug("File %s\n",dp->d_name);
                if(//dp->d_name &&
                   dp->d_name[0] != '.')
                {
                    infolist = Realloc(infolist,
                                       (*size+1) * sizeof(struct YBC_FITSfile_info_t *));
                    infolist[*size] =
                        YBC_analyse_file(stardata,
                                         prefix,
                                         dirpath,
                                         dp->d_name);
                    infolist[*size]->dataset =
                        strdup(stardata->persistent_data->YBC_table_dataset);
                    (*size)++;
                }
            }
        }
        closedir(dfp);
        Safe_free(dirpath_with_prefix);
    }
    else
    {
        Exit_binary_c(BINARY_C_ALLOC_FAILED,
                      "Could not make string with asprintf.");
    }
    return infolist;
}

struct YBC_FITSfile_info_t * YBC_analyse_file(
    struct stardata_t * const stardata Maybe_unused,
    char * const prefix,
    char * const dirpath,
    char * const filename
    )
{
    struct YBC_FITSfile_info_t * info = NULL;

    /*
     * Construct directory path with prefix
     */
    char * dirpath_with_prefix = NULL;

    if(asprintf(&dirpath_with_prefix,
                "%s/%s",
                dirpath,
                prefix) > 0)
    {
        /*
         * Construct full path
         */
        char * fullpath = NULL;
        if(asprintf(&fullpath,
                    "%s/%s",
                    dirpath_with_prefix,
                    filename) > 0)
        {
            info = Calloc(1,sizeof(struct YBC_FITSfile_info_t));
            info->table = NULL;
            NewDataTable_from_Pointer(NULL,
                                      info->table,
                                      0,
                                      0,
                                      0);
            info->filepath = fullpath;
            info->filename = strdup(filename);
            info->dirpath = strdup(dirpath);
            info->dirpath_with_prefix = dirpath_with_prefix;
            info->prefix = strdup(prefix);

            /*
             * Set variables to NaN : this allows
             * us to update these variables later.
             */
#undef X
#define X(VAR) info->VAR = NAN;
            YBC_LOOKUP_VARS;
#undef X

            /*
             * From the filename, determine which dataset we are using.
             */
            info->type = YBC_spectral_library_type(stardata,
                                                   fullpath);

            /*
             * Determine the spectral library type from the filename and,
             * if we found a type, get spectral library information.
             */
            if(info->type >= 0)
            {
                YBC_spectral_library_info(stardata,
                                          info);
            }
        }
        else
        {
            Exit_binary_c(BINARY_C_ALLOC_FAILED,
                          "Could not make string with asprintf.");
        }
    }
    else
    {
        Exit_binary_c(BINARY_C_ALLOC_FAILED,
                      "Could not make string with asprintf.");
    }
    return info;
}


int YBC_spectral_library_type(
    struct stardata_t * const stardata Maybe_unused,
    char * const filepath)
{
    /*
     * Given a filepath, determine the YBC type
     * as an integer binary_c can use.
     *
     * On failure, returns -1.
     */
    int type = -1;

#undef X
#define X(LIBRARY,STRINGS,...)                  \
    #LIBRARY,
    static char *library_strings[] Maybe_unused = { YBC_LIBRARIES };
#undef X
#define X(LIBRARY,STRINGS,...)                  \
    if(type == -1)                              \
    {                                           \
        char *strings[] = { STRINGS };          \
        const int n = Array_size(strings);      \
        Boolean found = TRUE;                   \
        for(int i=0; i<n; i++)                  \
        {                                       \
            if(strstr(filepath,                 \
                      strings[i]) == NULL)      \
            {                                   \
                found = FALSE;                  \
                break;                          \
            }                                   \
        }                                       \
        if(found == TRUE)                       \
        {                                       \
            type = YBC_LIBRARY_##LIBRARY;       \
        }                                       \
    }
    YBC_LIBRARIES;
#undef X

    if(type != -1)
    {
        YBCdebug("Matched \"%s\" to library %d \"%s\"\n",
                 filepath,
                 type,
                 library_strings[type]);
    }
    else
    {
        YBCdebug("Failed to match \"%s\" to a library.",
                 filepath);
    }
    return type;
}

void YBC_spectral_library_info(struct stardata_t * const stardata Maybe_unused,
                               struct YBC_FITSfile_info_t * const info)
{
    /*
     * Load data from the .list file
     *
     * Use custom functions to fill the information,
     * and then use the data in the list file to fill
     * out the rest.
     */
#define X(LIBRARY,STRINGS,...)                                  \
    if(info->type == YBC_LIBRARY_##LIBRARY)                     \
    {                                                           \
        size_t nmetadata;                                       \
        YBC_spectral_library_info_from_list_file(stardata,      \
                                                 info,          \
                                                 &nmetadata);   \
        YBC_spectral_library_info_##LIBRARY(stardata,           \
                                            info);              \
        YBC_clean_spectral_library_info(stardata,               \
                                        info);                  \
    }
    YBC_LIBRARIES;
#undef X
}


void YBC_clean_spectral_library_info(struct stardata_t * const stardata Maybe_unused,
                                     struct YBC_FITSfile_info_t * const info)
{
    /*
     * Given information about a spectral library,
     * clean it up if possible, e.g. if metallicity
     * is available, but not M_H, set M_H, and vice
     * versa.
     *
     * The values are set to NaN initially, so
     * we can work out which have not yet been
     * updated with the isnan() function. This is
     * an expensive function, so we create an array
     * of Booleans to limit the number of calls.
     */
#define ISNAN_LIST  \
    X( metallicity) \
    X(         M_H) \
    X(          XH) \
    X(         XHe)
#undef X
#define X(VAR) _V_##VAR,
    enum { ISNAN_LIST ISNAN_NUMBER };
#undef X

    const Boolean _isnan[ISNAN_NUMBER] = {
#define X(VAR) isnan(info->VAR),
        ISNAN_LIST
    };

    if(_isnan[_V_metallicity] && !_isnan[_V_XH] && !_isnan[_V_XHe])
    {
        info->metallicity = 1.0 - info->XH - info->XHe;
    }
    if(_isnan[_V_M_H] && !_isnan[_V_metallicity])
    {
        info->M_H = log10(info->metallicity);
    }
    if(_isnan[_V_metallicity] && !_isnan[_V_M_H])
    {
        info->metallicity = exp10(info->M_H);
    }
    if(_isnan[_V_XHe] && !_isnan[_V_XH] && !_isnan[_V_metallicity])
    {
        info->XHe = 1.0 - info->XH - info->metallicity;
    }
    if(_isnan[_V_XH] && !_isnan[_V_XHe] && !_isnan[_V_metallicity])
    {
        info->XH = 1.0 - info->XHe - info->metallicity;
    }
}

void YBC_spectral_library_info_from_list_file(struct stardata_t * const stardata Maybe_unused,
                                              struct YBC_FITSfile_info_t * const info,
                                              size_t * const n_metadata)
{
    /*
     * The .list file contains information
     * that is not in the filenames.
     *
     * Note that we can either construct the listfile
     * filename ourselves, or use one passed in to handle
     * the case(s) where there are multiple .list files.
     */
    FILE *  fp;
    char * listfile;
    *n_metadata = 0;

    int x;
    if(stardata->preferences->YBC_listfile[0] != '\0')
    {
        listfile = strdup(stardata->preferences->YBC_listfile);
        x = strlen(listfile);
    }
    else
    {
        x = asprintf(&listfile,
                     "%s/%s/Avodonnell94Rv3.1%s.list",
                     stardata->preferences->YBC_path,
                     info->dataset,
                     info->dataset
            );
        if(x == 0)
        {
            Exit_binary_c(BINARY_C_ALLOC_FAILED,
                          "Could not make string with asprintf.");
        }
    }


    {
        char * f = check_file_exists(listfile);
        const Boolean exists = f != NULL;
        Safe_free(f);
        if(exists == FALSE)
        {
            Exit_binary_c(BINARY_C_FILE_OPEN_ERROR,
                          "Could not open file %s, set in YBC_listfile, for reading. Please check that it exists.",
                          listfile);
        }
    }

    if(x > 0)
    {
        /*
         * Open the file
         */
        fp = fopen(listfile,
                   "r");

        if(fp)
        {
            char * line = NULL;
            size_t n Maybe_unused = 0;

            /*
             * Loop through the lines of the file
             */
            while(getline(&line,
                          &n,
                          fp)>0)
            {
                char * filename = Calloc(n,sizeof(char));

                /*
                 * Match line's first string to filename and,
                 * if this is the one we want, continue
                 */
                if(sscanf(line,"%s",filename) > 0 &&
                   Strings_equal(filename,info->filename))
                {
                    /*
                     * Convert subsequent list of strings to
                     * list of doubles
                     */
                    chomp(line);
                    struct string_array_t * string_array = new_string_array(0);
                    string_split(stardata,
                                 line + strlen(filename) + 1,
                                 string_array,
                                 ' ',
                                 0,
                                 0,
                                 FALSE);
                    if(string_array->n > 0)
                    {
                        struct double_array_t * double_array =
                            string_array_to_double_array(string_array,NAN);
                        double *metadata = double_array->doubles;
                        *n_metadata = (size_t)double_array->n;

                        /*
                         * Set the metadata into info
                         */
#undef X
#define X(TYPE,STRINGS,...)                                     \
                        if(info->type == YBC_LIBRARY_##TYPE)    \
                        {                                       \
                            YBC_Process_Metadata(__VA_ARGS__);  \
                        }
                        YBC_LIBRARIES;
#undef X
                        free_double_array(&double_array);
                        free_string_array(&string_array);
                    }
                }
                Safe_free(line);
                Safe_free(filename);
            }
            Safe_free(line);
            fclose(fp);
        }
        else
        {
            Exit_binary_c(BINARY_C_FILE_OPEN_ERROR,
                          "Tried to open list file \"%s\" but it does not exist. This file is required: try setting YBC_listfile to point to it.",
                          listfile);
        }
        Safe_free(listfile);
    }
    return;
}

void YBC_spectral_library_info_ATLAS9(struct stardata_t * const stardata Maybe_unused,
                                      struct YBC_FITSfile_info_t * const info)
{
    char * c = strstr(info->filename,"fp");
    if(c == NULL)
    {
        c = strstr(info->filename,"fm");
    }
    if(c)
    {
        const double sign = c[1] == 'p' ? +1.0 : -1.0;
        const char M_H[] = {c[2], '.', c[3], '\0'};
        info->M_H = sign * fast_strtod(M_H,NULL);
        info->metallicity = exp10(info->M_H);
    }
}

void YBC_spectral_library_info_PHOENIX(struct stardata_t * const stardata Maybe_unused,
                                       struct YBC_FITSfile_info_t * const info)
{
    char * c = strstr(info->filename,"M");
    if(c)
    {
        const double sign = c[1] == '-' ? -1.0 : +1.0;
        const char M_H[] = {c[2], c[3], c[4], '\0'};
        info->M_H = sign * fast_strtod(M_H,NULL);
        info->metallicity = exp10(info->M_H);
    }
    c = strstr(info->filename,"_a");
    if(c)
    {
        c += 2;
        const double sign = c[0] == '-' ? -1.0 : +1.0;
        const char alpha_Fe[] = {c[1], c[2], c[3], '\0'};
        info->alpha_Fe = sign * fast_strtod(alpha_Fe,NULL);
    }
}

void YBC_spectral_library_info_COMARCS_MS(struct stardata_t * const stardata Maybe_unused,
                                          struct YBC_FITSfile_info_t * const info)
{
    char * c = strstr(info->filename,"feh");
    if(c)
    {
        c += 3;
        const double sign = c[0] == '-' ? -1.0 : +1.0;
        c++;
        const char M_H[] = {c[0], c[1], c[2], c[3], '\0'};
        info->M_H = sign * fast_strtod(M_H,NULL);
        info->metallicity = exp10(info->M_H);
    }
}

void YBC_spectral_library_info_COMARCS_C(struct stardata_t * const stardata Maybe_unused,
                                         struct YBC_FITSfile_info_t * const info)
{
    /*
     * According to Aringer+ 2009 they use
     * the Anders and Grevesse solar mixture,
     * hence Zsol = 0.0189
     */
    const double Zsol = 0.0189;
    char * c = strstr(info->filename,"czuo");
    if(c)
    {
        c += 4;
        const char C_O[] = {c[0], c[1], '.', c[2], c[3], '\0'};
        info->C_O = fast_strtod(C_O,NULL);
    }
    c = strstr(info->filename,"Cstars_z");
    if(c)
    {
        c+=8;
        const char Z[] = {c[0], c[1], '.', c[2], c[3], '\0'};
        info->metallicity = fast_strtod(Z,NULL);
        info->M_H = log10(info->metallicity);
        info->metallicity *= Zsol;
    }
    c = strstr(info->filename,"xi");
    if(c)
    {
        c+=2;
        const char xi[] = {c[0], c[1], '\0'};
        info->microturbulent_velocity = fast_strtod(xi,NULL);
    }

    /*
     * filenames like
     * Avodonnell94Rv3.1COMARCS_Cstars_o88029_c91040_fe75630.BC.fits
     * are not much use: you must use the .list file
     * to obtain information about these (see function
     * YBC_spectral_library_info_from_list_file).
     */

}
void YBC_spectral_library_info_WM_BASIC(struct stardata_t * const stardata Maybe_unused,
                                        struct YBC_FITSfile_info_t * const info)
{
    char * c = strstr(info->filename,
                      "WM_Z");
    if(c)
    {
        c += 4;
        char Z[] = {c[0], c[1], c[2], c[3], c[4], c[5], '\0'};
        for(int i=0;i<6;i++)
        {
            if(Z[i] == 'M')
            {
                Z[i] = '\0';
            }
        }
        info->metallicity = fast_strtod(Z,NULL);
    }
    c = strstr(info->filename,
               "Mdot");
    if(c)
    {
        c += 4;
        char Mdot[] = {c[0],'\0'};
        info->log10mdot = -fast_strtod(Mdot,NULL);
        info->mdot = exp10(info->log10mdot); // Msun/year
    }
}

void YBC_spectral_library_info_PoWR(struct stardata_t * const stardata Maybe_unused,
                                    struct YBC_FITSfile_info_t * const info)

{
    /*
     * General PoWR table info
     *
     * Each macro in PoWR_TABLES_LIST is
     *
     * First filename match (string)
     * Second filename match (string)
     * metallicity / Zsolar,
     * XH,
     * XHe,
     * XC,
     * XN,
     * XO,
     * XNe
     *
     * Neon is not set if negative.
     *
     * Note that longer match strings should be first in the list.
     */
#define PoWR_TABLES_LIST                                                             \
    X( "subsmc-",   "-wne", 9.2e-5/1.4e-3, 0.0, 0.999,   1e-5, 6.1e-4, 1e-5,   -1.0) \
    X( "subsmc-", "-wnl20", 9.2e-5/1.4e-3, 0.2, 0.799,   1e-5, 6.1e-4, 1e-5,   -1.0) \
    X( "subsmc-", "-wnl40", 9.2e-5/1.4e-3, 0.4, 0.599,   1e-5, 6.1e-4, 1e-5,   -1.0) \
    X( "subsmc-", "-wnl60", 9.2e-5/1.4e-3, 0.6, 0.399,   1e-5, 6.1e-4, 1e-5,   -1.0) \
    X( "subsmc-",    "-wc", 9.2e-5/1.4e-3, 0.0, 0.549,    0.4,    0.0, 0.05, 8.3e-4) \
                                                                                     \
    X(    "lmc-",   "-wne",   7e-4/1.4e-3, 0.0, 0.995,   7e-4,   4e-3,  0.0,   -1.0) \
    X(    "lmc-", "-wnl20",   7e-4/1.4e-3, 0.2, 0.795,   7e-5,   4e-3,  0.0,   -1.0) \
    X(    "lmc-", "-wnl40",   7e-4/1.4e-3, 0.4, 0.595,   7e-5,   4e-3,  0.0,   -1.0) \
    X(    "lmc-",    "-wc",   7e-4/1.4e-3, 0.0,  0.55,    0.4,   4e-3, 0.05,   1e-3) \
                                                                                     \
    X(    "smc-",   "-wne",   3e-4/1.4e-3, 0.0, 0.998, 2.5e-5, 1.5e-3,  0.0,   -1.0) \
    X(    "smc-", "-wnl20",   3e-4/1.4e-3, 0.2, 0.798, 2.5e-5, 1.5e-3,  0.0,   -1.0) \
    X(    "smc-", "-wnl40",   3e-4/1.4e-3, 0.4, 0.598, 2.5e-5, 1.5e-3,  0.0,   -1.0) \
    X(    "smc-", "-wnl60",   3e-4/1.4e-3, 0.6, 0.398, 2.5e-5, 1.5e-3,  0.0,   -1.0) \
    X(    "smc-",    "-wc",   3e-4/1.4e-3, 0.0, 0.547,    0.4,    0.0, 0.05, 2.4e-3) \
                                                                                     \
    X(    "sol-",   "-wne",           1.0, 0.0,  0.98,   1e-4,  0.015, 1e-4,   -1.0) \
    X(    "sol-", "-wnl20",           1.0, 0.2,  0.78,   1e-4,  0.015,  0.0,   -1.0) \
    X(    "sol-", "-wnl50",           1.0, 0.5,  0.48,   1e-4,  0.015,  0.0,   -1.0) \
    X(    "sol-",    "-wc",           1.0, 0.0,  0.55,    0.4,    0.0, 0.05,   -1.0)

    const double Zsolar = 0.014;
#undef X
#define X(FIRST,SECOND,ZFAC,H,He,C,N,O,Ne)              \
    if(strstr(info->filename,(FIRST)) != NULL &&        \
       strstr(info->filename,(SECOND)) != NULL)         \
    {                                                   \
        info->metallicity = Zsolar * (ZFAC);            \
        info->XH = (H);                                 \
        info->XHe = (He);                               \
        info->XC = (C);                                 \
        info->XO = (O);                                 \
        if(More_or_equal((Ne),0.0))                     \
        {                                               \
            info->XNe = (Ne);                           \
        }                                               \
        return;                                         \
    }

    PoWR_TABLES_LIST;
#undef X

    Exit_binary_c(BINARY_C_ALGORITHM_BRANCH_FAILURE,
                  "YBC PoWR filename \"%s\" failed to match any known data set\n",
                  info->filepath);

}

void YBC_spectral_library_info_PoWR_WNL(struct stardata_t * const stardata,
                                        struct YBC_FITSfile_info_t * const info)
{
    YBC_spectral_library_info_PoWR(stardata,info);
}

void YBC_spectral_library_info_PoWR_WNE(struct stardata_t * const stardata,
                                        struct YBC_FITSfile_info_t * const info)
{
    YBC_spectral_library_info_PoWR(stardata,info);
}

void YBC_spectral_library_info_PoWR_WC(struct stardata_t * const stardata,
                                       struct YBC_FITSfile_info_t * const info)
{
    YBC_spectral_library_info_PoWR(stardata,info);
}

void YBC_spectral_library_info_KOESTER(struct stardata_t * const stardata Maybe_unused,
                                       struct YBC_FITSfile_info_t * const info Maybe_unused)
{
}

void YBC_spectral_library_info_TUC47(struct stardata_t * const stardata Maybe_unused,
                                     struct YBC_FITSfile_info_t * const info Maybe_unused)
{
}

void YBC_spectral_library_info_TLUSTY(struct stardata_t * const stardata Maybe_unused,
                                      struct YBC_FITSfile_info_t * const info Maybe_unused)
{
    char * c = strstr(info->filename,"H");
    if(c)
    {
        c++;
        const char XH[] = {c[0], c[1], c[2], '\0'};
        info->XH = fast_strtod(XH,NULL);
        info->XHe = 1.0 - info->XH;
    }
}


void YBC_combine_files(struct stardata_t * const stardata,
                       const int instrument,
                       struct YBC_FITSfile_info_t ** const infolist,
                       const size_t nfiles,
                       char ** wanted_columns,
                       const int wanted_ncolumns)
{
    /*
     * Combine YBC FITS data files into bigger interpolation tables
     */
    struct data_table_t ** YBC_combined_data_tables =
        Calloc(1,sizeof(struct data_table_t*) * YBC_COMBINED_LIBRARY_NUMBER);

#undef X
#define X(TYPE,...) #TYPE,
    static char * YBC_combined_library_strings[] = { YBC_COMBINED_LIBRARIES };
#undef X


    /*
     * ATLAS9
     *
     * We have many files at different metallicities.
     *
     * Combine into one interpolation table which has metallicity
     * as the leftmost column.
     */
    {
        struct data_table_t ** data_tables = NULL;
        double ** left_params = NULL;
        size_t n_ATLAS9 = 0;

        for(size_t i=0; i<nfiles; i++)
        {
            struct YBC_FITSfile_info_t * const info = infolist[i];
            if(info->type == YBC_LIBRARY_ATLAS9)
            {
                YBCdebug("Found ATLAS9 library at %s  with [M/H]=%g\n",
                         info->filename,
                         info->M_H);

                YBC_load_fitsfile(stardata,
                                  info,
                                  wanted_columns,
                                  wanted_ncolumns,
                                  YBC_FILTER_NULL_DATA);

                YBCdebug("Loaded into table with nparams = %zu, ndata = %zu, nlines = %zu\n",
                         info->table->nparam,
                         info->table->ndata,
                         info->table->nlines);

                data_tables = Realloc(data_tables,
                                      sizeof(struct data_table_t *) * (n_ATLAS9+1));
                left_params = Realloc(left_params,
                                      sizeof(double) * (n_ATLAS9+1));

                data_tables[n_ATLAS9] = info->table;
                left_params[n_ATLAS9] = Malloc(sizeof(double)*1);
                left_params[n_ATLAS9][0] = info->M_H;

                n_ATLAS9++;
            }
        }

        YBCdebug("Combine %zu ATLAS9 files\n",
                 n_ATLAS9);

        YBC_combined_data_tables[YBC_COMBINED_LIBRARY_ATLAS9] =
            combine_data_tables(stardata,
                                data_tables,
                                left_params,
                                n_ATLAS9,
                                1);

        for(size_t i=0;i<n_ATLAS9;i++)
        {
            Safe_free(left_params[i]);
        }
        Safe_free(left_params);
        Safe_free(data_tables);
    }

    /*
     * PHOENIX
     *
     * We have many files at different metallicities
     * and [alpha/Fe], but each metallicity has a unique
     * [alpha/Fe] so we don't need to use this as a lookup
     * parameter.
     *
     * Combine into one interpolation table which has metallicity
     * as the leftmost column.
     */
    {
        struct data_table_t ** data_tables = NULL;
        double ** left_params = NULL;
        size_t n_PHOENIX = 0;

        for(size_t i=0; i<nfiles; i++)
        {
            struct YBC_FITSfile_info_t * const info = infolist[i];
            if(info->type == YBC_LIBRARY_PHOENIX)
            {
                YBCdebug("Found PHOENIX library at %s with [M/H]=%g [a/Fe]=%g\n",
                         info->filename,
                         info->M_H,
                         info->alpha_Fe);

                YBC_load_fitsfile(stardata,
                                  info,
                                  wanted_columns,
                                  wanted_ncolumns,
                                  YBC_FILTER_NULL_DATA);

                YBCdebug("Loaded into table with nparams = %zu, ndata = %zu, nlines = %zu\n",
                         info->table->nparam,
                         info->table->ndata,
                         info->table->nlines);

                data_tables = Realloc(data_tables,
                                      sizeof(struct data_table_t *) * (n_PHOENIX+1));
                left_params = Realloc(left_params,
                                      sizeof(double) * (n_PHOENIX+2));

                data_tables[n_PHOENIX] = info->table;
                left_params[n_PHOENIX] = Malloc(sizeof(double)*1);
                left_params[n_PHOENIX][0] = info->M_H;
                n_PHOENIX++;
            }
        }

        YBCdebug("Combine %zu PHOENIX files\n",
                 n_PHOENIX);

        YBC_combined_data_tables[YBC_COMBINED_LIBRARY_PHOENIX] =
            combine_data_tables(stardata,
                                data_tables,
                                left_params,
                                n_PHOENIX,
                                1);

        for(size_t i=0;i<n_PHOENIX;i++)
        {
            Safe_free(left_params[i]);
        }
        Safe_free(left_params);
        Safe_free(data_tables);
    }

    /*
     * 47 TUC ATLAS 12 models
     *
     * There is no documentation for these, so they are not yet included.
     */
    {
    }

    /*
     * COMARCS M (and S?) stars
     *
     * These have metallicity as a parameter.
     */
    {
        struct data_table_t ** data_tables = NULL;
        double ** left_params = NULL;
        size_t n_COMARCS_MS = 0;

        for(size_t i=0; i<nfiles; i++)
        {
            struct YBC_FITSfile_info_t * const info = infolist[i];
            if(info->type == YBC_LIBRARY_COMARCS_MS)
            {
                YBCdebug("Found COMARCS_MS library at %s with [M/H]=%g\n",
                         info->filename,
                         info->M_H);

                YBC_load_fitsfile(stardata,
                                  info,
                                  wanted_columns,
                                  wanted_ncolumns,
                                  YBC_FILTER_NULL_DATA);

                YBC_filter_zero_column(info->table,1);

                YBCdebug("Loaded into table with nparams = %zu, ndata = %zu, nlines = %zu\n",
                         info->table->nparam,
                         info->table->ndata,
                         info->table->nlines);

                data_tables = Realloc(data_tables,
                                      sizeof(struct data_table_t *) * (n_COMARCS_MS+1));
                left_params = Realloc(left_params,
                                      sizeof(double) * (n_COMARCS_MS+1));

                data_tables[n_COMARCS_MS] = info->table;
                left_params[n_COMARCS_MS] = Malloc(sizeof(double)*1);
                left_params[n_COMARCS_MS][0] = info->M_H;

                n_COMARCS_MS++;
            }
        }

        YBCdebug("Combine %zu COMARCS_MS files\n",
                 n_COMARCS_MS);

        YBC_combined_data_tables[YBC_COMBINED_LIBRARY_COMARCS_MS] =
            combine_data_tables(stardata,
                                data_tables,
                                left_params,
                                n_COMARCS_MS,
                                1);

        for(size_t i=0;i<n_COMARCS_MS;i++)
        {
            Safe_free(left_params[i]);
        }
        Safe_free(left_params);
        Safe_free(data_tables);
    }


    /*
     * COMARCS C stars
     *
     * These have metallicity and C/O as two parameters.
     *
     * The parameter Cex in the YBC grid is just
     * log10(NC/12 - NO/16)
     * so we don't really need it.
     *
     * xi (microturbulent velocity) is also a parameter (2.5km/s),
     * but never changes so we ignore it.
     */
    {
        struct data_table_t ** data_tables = NULL;
        double ** left_params = NULL;
        size_t n_COMARCS_C = 0;
        for(size_t i=0; i<nfiles; i++)
        {
            struct YBC_FITSfile_info_t * const info = infolist[i];
            if(info->type == YBC_LIBRARY_COMARCS_C)
            {
                YBCdebug("Found COMARCS_C library at %s with [M/H]=%g and C/O=%g\n",
                         info->filename,
                         info->M_H,
                         info->C_O);

                YBC_load_fitsfile(stardata,
                                  info,
                                  wanted_columns,
                                  wanted_ncolumns,
                                  YBC_FILTER_NULL_DATA);
                YBC_filter_zero_column(info->table,1);

                data_tables = Realloc(data_tables,
                                      sizeof(struct data_table_t *) * (n_COMARCS_C+1));
                left_params = Realloc(left_params,
                                      sizeof(double) * (n_COMARCS_C+1));

                data_tables[n_COMARCS_C] = info->table;
                left_params[n_COMARCS_C] = Malloc(sizeof(double)*2);
                left_params[n_COMARCS_C][0] = info->M_H;
                left_params[n_COMARCS_C][1] = info->C_O;
                n_COMARCS_C++;
            }
        }

        YBCdebug("Combine %zu COMARCS_C files\n",
                 n_COMARCS_C);

        YBC_combined_data_tables[YBC_COMBINED_LIBRARY_COMARCS_C] =
            combine_data_tables(stardata,
                                data_tables,
                                left_params,
                                n_COMARCS_C,
                                2);
        for(size_t i=0;i<n_COMARCS_C;i++)
        {
            Safe_free(left_params[i]);
        }
        Safe_free(left_params);
        Safe_free(data_tables);
    }


    /*
     * WMbasic
     *
     * These have metallicity and mass-loss rate as parameters.
     */
    {
        struct data_table_t ** data_tables = NULL;
        double ** left_params = NULL;
        size_t n_WM_BASIC = 0;
        for(size_t i=0; i<nfiles; i++)
        {
            struct YBC_FITSfile_info_t * info = infolist[i];
            if(info->type == YBC_LIBRARY_WM_BASIC)
            {
                YBCdebug("Found WM_BASIC library at %s with [M/H]=%g and log10(Mdot)=%g\n",
                         info->filename,
                         info->M_H,
                         info->log10mdot);

                YBC_load_fitsfile(stardata,
                                  info,
                                  wanted_columns,
                                  wanted_ncolumns,
                                  YBC_FILTER_NULL_DATA);
                data_tables = Realloc(data_tables,
                                      sizeof(struct data_table_t *) * (n_WM_BASIC+1));
                left_params = Realloc(left_params,
                                      sizeof(double) * (n_WM_BASIC+1));

                data_tables[n_WM_BASIC] = info->table;
                left_params[n_WM_BASIC] = Malloc(sizeof(double)*2);
                left_params[n_WM_BASIC][0] = info->metallicity;
                left_params[n_WM_BASIC][1] = info->log10mdot;
                n_WM_BASIC++;
            }
        }

        YBCdebug("Combine %zu WM_BASIC files\n",
                 n_WM_BASIC);

        YBC_combined_data_tables[YBC_COMBINED_LIBRARY_WM_BASIC] =
            combine_data_tables(stardata,
                                data_tables,
                                left_params,
                                n_WM_BASIC,
                                2);
        for(size_t i=0;i<n_WM_BASIC;i++)
        {
            Safe_free(left_params[i]);
        }
        Safe_free(left_params);
        Safe_free(data_tables);
    }

    /*
     * PoWR
     *
     * We have three grids: WNE, WNL, WC, each a function of metallicity.
     */
    {
#undef X
#define X(LIBRARY,STRINGS,...)                  \
        #LIBRARY,
        static char *library_strings[] Maybe_unused = { YBC_LIBRARIES };
#undef X

        /*
         * We construct two tables:
         *
         * WN (WNL to WNE) with XH as the parameter
         * for use when the star has some hydrogen.
         *
         * and
         *
         * WC (WNE to WC) with XHe as the parameter
         * for hydrogen-exhausted stars.
         *
         * Then combine these to have metallicity as the
         * leftmost parameter.
         */
        const int types[2][2] = {
            {
                YBC_LIBRARY_PoWR_WNL, // XH = 0.2 - 0.6
                YBC_LIBRARY_PoWR_WNE // no hydrogen,  all helium (except Z)
            },
            {
                YBC_LIBRARY_PoWR_WNE, // no hydrogen,  all helium (except Z)
                YBC_LIBRARY_PoWR_WC   // no hydrogen, half helium, half carbon
            }
        };
        const int combined_types[2] = {
            YBC_COMBINED_LIBRARY_PoWR_WNL_WNE,
            YBC_COMBINED_LIBRARY_PoWR_WNE_WC
        };

        for(size_t j=0;j<Array_size(combined_types);j++)
        {
            int combined_type = combined_types[j];
            struct data_table_t ** data_tables = NULL;
            double ** left_params = NULL;

            size_t count = 0;
            Foreach_array(const int, type, types[j])
            {
                for(size_t i=0; i<nfiles; i++)
                {
                    struct YBC_FITSfile_info_t * const info = infolist[i];
                    if(info->type == type)
                    {
                        YBCdebug("Found %s library at %s with metallicity=%g, %s=%g\n",
                                 library_strings[type],
                                 info->filename,
                                 info->metallicity,
                                 combined_type==YBC_COMBINED_LIBRARY_PoWR_WNL_WNE ? "XH" : "XHe",
                                 combined_type==YBC_COMBINED_LIBRARY_PoWR_WNL_WNE ? info->XH : info->XHe);

                        YBC_load_fitsfile(stardata,
                                          info,
                                          wanted_columns,
                                          wanted_ncolumns,
                                          YBC_FILTER_NULL_DATA);

                        YBCdebug("Loaded into table with nparams = %zu, ndata = %zu, nlines = %zu\n",
                                 info->table->nparam,
                                 info->table->ndata,
                                 info->table->nlines);

                        data_tables = Realloc(data_tables,
                                              sizeof(struct data_table_t *) * (count+1));
                        left_params = Realloc(left_params,
                                              sizeof(double) * (count+1));

                        data_tables[count] = info->table;

                        left_params[count] = Malloc(sizeof(double)*2);
                        left_params[count][0] = info->metallicity;
                        left_params[count][1] = combined_type==YBC_COMBINED_LIBRARY_PoWR_WNL_WNE ? info->XH : info->XHe;

                        count++;
                    }
                }
            }

            YBC_combined_data_tables[combined_type] =
                combine_data_tables(stardata,
                                    data_tables,
                                    left_params,
                                    count, // ntables
                                    2 // nleftparams
                    );

            /*
             * Check here for doubled parameters: these
             * occurred in older versions of this code, but
             * seem not to be an issue now. That said, this check should
             * be relatively quick and only done once.
             */
            size_t firstmatch;
            if(check_for_repeated_parameters(stardata,
                                             YBC_combined_data_tables[combined_type],
                                             &firstmatch))
            {
                struct data_table_t * const t = YBC_combined_data_tables[combined_type];
                printf("Lines %zu and %zu of table %d/%zu have identical coords : ",
                       firstmatch-1,
                       firstmatch,
                       combined_type,
                       count);
                const size_t ll = t->nparam + t->ndata;
                for(size_t k=0; k<t->ndata; k++)
                {
                    printf("%g ",*(t->data + firstmatch*ll + k));
                }

                if(memcpy(t->data + (firstmatch-1) * t->nparam,
                          t->data + (firstmatch) * t->nparam,
                          sizeof(double) * t->ndata)==0)
                {
                    printf("but data identical");
                }
                else
                {
                    printf("with different data");
                }
                printf("\nlines identical : needs an algorithm!\n");

                Exit_binary_c(BINARY_C_INTERPOLATION_ERROR,
                              "Table has repeated paramaters so cannot be interpolated.\n");
            }

            for(size_t i=0;i<count;i++)
            {
                Safe_free(left_params[i]);
            }
            Safe_free(left_params);
            Safe_free(data_tables);
        }
    }

    /*
     * Koester WDs
     *
     * These have no extra parameters and are for Z=0.02 only.
     */
    for(size_t i=0; i<nfiles; i++)
    {
        struct YBC_FITSfile_info_t * const info = infolist[i];
        if(info->type == YBC_LIBRARY_KOESTER)
        {
            YBC_load_fitsfile(stardata,
                              info,
                              wanted_columns,
                              wanted_ncolumns,
                              YBC_FILTER_NULL_DATA);
            YBC_combined_data_tables[YBC_COMBINED_LIBRARY_KOESTER] =
                copy_data_table(info->table);
            break;
        }
    }

    /*
     * TLUSTY hot subdwarfs
     *
     * These are pure H and He, so we
     * use XH as the extra parameter.
     */
    YBC_combined_data_tables[YBC_COMBINED_LIBRARY_TLUSTY] = NULL;

    {
        struct data_table_t ** data_tables = NULL;
        double ** left_params = NULL;
        size_t n_TLUSTY = 0;

        for(size_t i=0; i<nfiles; i++)
        {
            struct YBC_FITSfile_info_t * const info = infolist[i];
            if(info->type == YBC_LIBRARY_TLUSTY)
            {
                YBCdebug("Found TLUSTY library at %s with XH = %g, XHe = %g\n",
                         info->filename,
                         info->XH,
                         info->XHe);

                YBC_load_fitsfile(stardata,
                                  info,
                                  wanted_columns,
                                  wanted_ncolumns,
                                  YBC_FILTER_NULL_DATA);

                YBCdebug("Loaded into table with nparams = %zu, ndata = %zu, nlines = %zu\n",
                         info->table->nparam,
                         info->table->ndata,
                         info->table->nlines);

                data_tables = Realloc(data_tables,
                                      sizeof(struct data_table_t *) * (n_TLUSTY+1));
                left_params = Realloc(left_params,
                                      sizeof(double) * (n_TLUSTY+1));

                data_tables[n_TLUSTY] = info->table;
                left_params[n_TLUSTY] = Malloc(sizeof(double)*1);
                left_params[n_TLUSTY][0] = info->XH;

                n_TLUSTY++;
            }
        }


        YBCdebug("Combine %zu TLUSTY files\n",
                 n_TLUSTY);

        YBC_combined_data_tables[YBC_COMBINED_LIBRARY_TLUSTY] =
            combine_data_tables(stardata,
                                data_tables,
                                left_params,
                                n_TLUSTY,
                                1);
        for(size_t i=0;i<n_TLUSTY;i++)
        {
            Safe_free(left_params[i]);
        }
        Safe_free(left_params);
        Safe_free(data_tables);
    }

    /*
     * Set up mapping arrays
     */
#undef X
#define X(TYPE,...)                             \
    {__VA_ARGS__},
    rinterpolate_float_t remap_resolutions[YBC_COMBINED_LIBRARY_NUMBER][YBC_COMBINED_MAXNPARAM] = {
        YBC_COMBINED_LIBRARIES_MAPS
#undef X
    };

    /*
     * We have now loaded the data_tables, and combined them,
     * now we need to convert them to rinterpolate tables.
     *
     * We first need to check they are orthogonal
     * and put them in a list of tables.
     */
    if(stardata->persistent_data->YBC_tables == NULL)
    {
        stardata->persistent_data->YBC_tables = Calloc(YBC_INSTRUMENT_NUMBER,
                                                       sizeof(struct rinterpolate_table_t**));
    }
    stardata->persistent_data->YBC_tables[instrument] = Calloc(YBC_COMBINED_LIBRARY_NUMBER,
                                                               sizeof(struct rinterpolate_table_t*));

#define _CHECK                                                          \
    {                                                                   \
        YBCdebug("combine line %d column_is_mapped : ",__LINE__);       \
        for(unsigned int _i=0; _i<stardata->persistent_data->rinterpolate_data->number_of_interpolation_tables; _i++) \
        {                                                               \
            YBCdebug("%u %p = %p, ",                                    \
                     _i,                                                \
                     (void*)stardata->persistent_data->rinterpolate_data->tables[_i], \
                     stardata->persistent_data->rinterpolate_data->tables[_i] ? (void*)stardata->persistent_data->rinterpolate_data->tables[_i]->column_is_mapped : NULL); \
        }                                                               \
        YBCdebug("\n");                                                 \
    }


    for(int i=0; i<YBC_COMBINED_LIBRARY_NUMBER; i++)
    {
        struct data_table_t * combined_data_table = YBC_combined_data_tables[i];

        YBCdebug("Combined table %d/%d \"%s\" [%s] is %p, data at %p, nparam %zu, ndata %zu, nlines %zu\n",
                 i,
                 YBC_COMBINED_LIBRARY_NUMBER,
                 YBC_combined_library_strings[i],
                 combined_data_table ? combined_data_table->label : "",
                 (void*)combined_data_table,
                 combined_data_table ? (void*)combined_data_table->data : NULL,
                 combined_data_table ? combined_data_table->nparam : 0,
                 combined_data_table ? combined_data_table->ndata : 0,
                 combined_data_table ? combined_data_table->nlines : 0);

        _CHECK;

        //show_data_table(combined_data_table);

        if(combined_data_table &&
           combined_data_table->data)
        {
            /*
             * Interpolate on NULL to set the table in
             * stardata->persistent_data->rinterpolate_data
             */
            YBCdebug("interpolate to set up table\n");
            Interpolate(combined_data_table,
                        NULL,
                        NULL,
                        0);
            YBCdebug("done interpolate to set up table\n");

            /*
             * Find the rinterpolate table struct
             * corresponding to combined_data_table
             * (they have the same data)
             */
            struct rinterpolate_table_t * maybe_disordered_table =
                rinterpolate_table_struct(stardata->persistent_data->rinterpolate_data,
                                          combined_data_table->data,
                                          NULL);

            YBCdebug("Made rinterpolate table (maybe_disordered_table) at %p (data %p), column_is_mapped %p\n",
                     (void*)maybe_disordered_table,
                     maybe_disordered_table ? (void*)maybe_disordered_table->data : NULL,
                     maybe_disordered_table ? (void*)maybe_disordered_table->column_is_mapped : NULL);

            /*
             * Check for nans, if there are nans we are stuffed, always.
             */
            if(rinterpolate_check_table_for_nans(maybe_disordered_table))
            {
                Exit_binary_c(BINARY_C_ALGORITHM_BRANCH_FAILURE,
                              "YBC combined table %d has nans\n",
                              i);
            }

            /*
             * Check if the table is orthogonal
             */
            const rinterpolate_Boolean_t orth = rinterpolate_is_table_orthogonal(
                stardata->persistent_data->rinterpolate_data,
                maybe_disordered_table,
                FALSE
                ) || TRUE;
            YBCdebug("Is orthogonal? %d\n",orth);

            /*
             * Assign columns for remapping.
             *
             * We assume here that there are a maximum of four
             * columns in the dataset.
             */
            rinterpolate_Boolean_t map[YBC_COMBINED_MAXNPARAM];
            rinterpolate_float_t * const res = remap_resolutions[i];

            /*
             * Based on the remap resolution list, determine whether
             * we need to remap this table (if all resolutions
             * are zero, do not remap).
             */
            Boolean do_remap = FALSE;
            for(int j=0; j<YBC_COMBINED_MAXNPARAM; j++)
            {
                if(res[j] > TINY)
                {
                    map[j] = TRUE;
                    do_remap = TRUE;
                }
                else
                {
                    map[j] = FALSE;
                }
            }

            YBCdebug("do remap? %d\n",do_remap);
            if(do_remap)
            {
                /*
                 * Remap columns
                 */
                YBCdebug("Remap columns of combined table [%d][%d]\n",
                         instrument,
                         i);
                for(int j=0; j<YBC_COMBINED_MAXNPARAM; j++)
                {
                    YBCdebug("col %d : map? %s, new resolution %g\n",
                             j,
                             Yesno(map[j]),
                             res[j]);
                }

                stardata->persistent_data->YBC_tables[instrument][i] =
                    rinterpolate_map_right_columns(stardata->persistent_data->rinterpolate_data,
                                                   maybe_disordered_table,
                                                   map,
                                                   res,
                                                   NULL);

                YBCdebug("post map table %p %p\n",
                         (void*)stardata->persistent_data->YBC_tables[instrument][i],
                         (void*)stardata->persistent_data->YBC_tables[instrument][i]->data);
                //rinterpolate_show_table(NULL, stardata->persistent_data->YBC_tables[instrument][i], stdout, 0);
            }
            else
            {
                /*
                 * Table is already orthogonal: just copy it
                 */
                YBCdebug("Copy table : original %p with column_is_mapped %p\n",
                         (void*)maybe_disordered_table,
                         (void*)maybe_disordered_table->column_is_mapped);

                YBCdebug("Copy onto %p\n",
                         (void*)stardata->persistent_data->YBC_tables[instrument][i]);
                stardata->persistent_data->YBC_tables[instrument][i] =
                    rinterpolate_copy_table(stardata->persistent_data->rinterpolate_data,
                                            maybe_disordered_table);
                YBCdebug("Copied table : new table %p with column_is_mapped %p\n",
                         (void*)stardata->persistent_data->YBC_tables[instrument][i],
                         (void*)stardata->persistent_data->YBC_tables[instrument][i]->column_is_mapped);
            }

            /*
             * Clean the old table from rinterpolate_data
             */
            YBCdebug("delete maybe_disordered_table\n");
            rinterpolate_delete_table(stardata->persistent_data->rinterpolate_data,
                                      maybe_disordered_table,
                                      NULL);
            Safe_free(maybe_disordered_table);
            YBCdebug("deleted maybe_disordered_table\n");

            if(orth==FALSE)
            {
                YBCdebug("orth is FALSE : checking new_orth\n");
                const rinterpolate_Boolean_t new_orth = rinterpolate_is_table_orthogonal(
                    stardata->persistent_data->rinterpolate_data,
                    stardata->persistent_data->YBC_tables[instrument][i],
                    FALSE
                    );
                YBCdebug("new_orth %s\n",Yesno(new_orth));
                if(new_orth == FALSE)
                {
                    Exit_binary_c(BINARY_C_ALGORITHM_BRANCH_FAILURE,
                                  "Combined YBC table %s should be orthogonal after a remap, but is not. This is an error that you should fix.",
                                  YBC_combined_library_strings[i]);
                }
            }
            YBCdebug("post combined check\n");
        }
        YBCdebug("table loop done i=%d/%d\n",
                 i,
                 YBC_COMBINED_LIBRARY_NUMBER);
    }

    YBCdebug("free combined tables\n");
    for(int i=0; i<YBC_COMBINED_LIBRARY_NUMBER; i++)
    {
        if(YBC_combined_data_tables[i])
        {
            Safe_free(YBC_combined_data_tables[i]->data);
            Safe_free_nocheck(YBC_combined_data_tables[i]);
        }
    }
    Safe_free(YBC_combined_data_tables);
    YBCdebug("return from YBC_combine\n");
}

void YBC_bin_column(struct data_table_t * const table,
                    const int ncol,
                    const double binwidth)
{
    /*
     * bin_column ncol to desired binwidth
     */
    double * data = table->data + ncol;
    const size_t nd = table->nparam + table->ndata;
    for(size_t l=0; l<table->nlines; l++)
    {
        *data = bin_data(*data, binwidth);
        data += nd;
    }
}

void YBC_filter_zero_column(struct data_table_t * const table,
                            const int ncol)
{
    /*
     * filter data in column ncol to make sure
     * very small numbers are actually zero
     */
    double * data = table->data + ncol;
    const size_t nd = table->nparam + table->ndata;
    for(size_t l=0; l<table->nlines; l++)
    {
        *data = fabs(*data) < TINY ? 0.0 : *data;
        data += nd;
    }
}


double YBC_library_type_float(const int table[2],
                              const double f,
                              const double log10mdot)
{
    /*
     * Return a floating point number to recrecate Fig. 1 of
     * Chen et al. (2019).
     *
     * If the table doesn't match one of the known
     * combined libraries, sets the value to 100.
     */
    double floats[2];
    for(int i=0; i<2; i++)
    {
        floats[i] =
#undef X
#define X(TYPE,FLOAT) (table[i] == YBC_COMBINED_LIBRARY_##TYPE) ? (FLOAT) :
            YBC_LIBRARY_TYPE_FLOATS  100;

        if(table[i] == YBC_COMBINED_LIBRARY_WM_BASIC)
        {
            /*
             * Special case
             */
            const double mdot = Limit_range(-log10mdot,5,7);
            const double g = (5.0 - mdot)/(7.0 - 5.0); /* in range -1 to 0 */
            floats[i] = 4.0 + 2.0 * g; /* in range 2 to 4 */
        }
    }

    return f*floats[0] + (1.0-f)*floats[1];
}

void YBC_free_persistent_data_memory(struct persistent_data_t * Restrict const persistent_data)
{
    /*
     * Free persistent_data memory used by YBC
     */
#undef X
#define X(ID,DIR,FITS_COLUMNS_ARRAY,MAGNITUDE_ARRAY)                    \
    if(persistent_data->YBC_tables &&                                   \
       persistent_data->YBC_tables[YBC_INSTRUMENT_##ID])                \
    {                                                                   \
        YBCdebug("have table %d\n",YBC_INSTRUMENT_##ID);                \
        for(int i=0;i<YBC_COMBINED_LIBRARY_NUMBER;i++)                  \
        {                                                               \
            YBCdebug("have table %d %d\n",YBC_INSTRUMENT_##ID,i);       \
            rinterpolate_delete_table(                                  \
                persistent_data->rinterpolate_data,                     \
                persistent_data->YBC_tables[YBC_INSTRUMENT_##ID][i],    \
                NULL                                                    \
                );                                                      \
            Safe_free_nocheck(persistent_data->YBC_tables[YBC_INSTRUMENT_##ID][i]); \
        }                                                               \
        Safe_free_nocheck(persistent_data->YBC_tables[YBC_INSTRUMENT_##ID]);    \
    }

    YBC_INSTRUMENTS_LIST;
#undef X

    Safe_free(persistent_data->YBC_tables);
    Safe_free(persistent_data->YBC_table_dataset);
    Safe_free(persistent_data->YBC_path);
}

void YBC_renew_instrument_list(struct stardata_t * const stardata)
{
    /*
     * Free and set the YBC instrument list
     */
    YBC_free_instrument_list(stardata->tmpstore);
    YBC_set_instrument_list(stardata);
}

void YBC_set_instrument_list(struct stardata_t * const stardata)
{
    /*
     * Given a comma-separated list of instruments in
     * stardata->preferences->YBC_instruments we split this
     * into an array stored in tmpstore.
     */
    if(stardata->preferences->YBC_instruments[0] != '\0' &&
       stardata->tmpstore->YBC_instruments_list == NULL)
    {
        stardata->tmpstore->YBC_instruments_list = new_string_array(0);
        string_split_preserve(stardata,
                              stardata->preferences->YBC_instruments,
                              stardata->tmpstore->YBC_instruments_list,
                              ',',
                              0,
                              0,
                              TRUE);

        /* count number of instruments we could have */
        if(stardata->tmpstore->YBC_use_instrument == NULL)
        {
            unsigned int n = 0;
#undef X
#define X(INSTRUMENT,B,C,D) n++;
            YBC_INSTRUMENTS_LIST;

            YBCdebug("cmd line gives %zd instruments, we have %u defined\n",
                     stardata->tmpstore->YBC_instruments_list->n,
                     n);

            stardata->tmpstore->YBC_use_instrument = Malloc(n * sizeof(Boolean));
            for(unsigned int i=0; i<n; i++)
            {
                stardata->tmpstore->YBC_use_instrument[i] = FALSE;
            }


#undef X
#define X(INSTRUMENT,B,C,D)                                             \
            for(ssize_t j=0; j<stardata->tmpstore->YBC_instruments_list->n; j++) \
            {                                                           \
                if(Strings_equal(#INSTRUMENT,                           \
                                 stardata->tmpstore->YBC_instruments_list->strings[j])) \
                {                                                       \
                    YBCdebug("match %s %s\n",                           \
                             #INSTRUMENT,                               \
                             stardata->tmpstore->YBC_instruments_list->strings[j]); \
                    stardata->tmpstore->YBC_use_instrument[YBC_INSTRUMENT_##INSTRUMENT] = TRUE; \
                }                                                       \
            }
            YBC_INSTRUMENTS_LIST;
        }

#undef X
#define X(INSTRUMENT,B,C,D)                                             \
        YBCdebug("Use instrument %s? %s\n",                             \
                 #INSTRUMENT,                                           \
                 Yesno(stardata->tmpstore->YBC_use_instrument[YBC_INSTRUMENT_##INSTRUMENT]));
        YBC_INSTRUMENTS_LIST;
#undef X
    }
}

void YBC_free_instrument_list(struct tmpstore_t * const tmpstore)
{
    /*
     * Free the existing instrument list from tmpstore
     */
    free_string_array(&tmpstore->YBC_instruments_list);
    Safe_free(tmpstore->YBC_use_instrument);
}

#endif // YBC
