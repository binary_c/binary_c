#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "../zfuncs/Karakas2002_lumfunc.h"

void table_Karakas2002_lumfunc(struct store_t * Restrict const store)
{
    NewDataTable_from_Array(
        KARAKAS2002_LUMFUNC_TABLE,
        store->Karakas2002_lumfunc,
        2,
        2,
        40
        );
}
