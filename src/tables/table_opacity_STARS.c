#include "../binary_c.h"
No_empty_translation_unit_warning;


#if defined OPACITY_ALGORITHMS &&               \
    defined OPACITY_ENABLE_ALGORITHM_STARS

#include "../opacity/opacity_STARS.h"

void table_opacity_STARS(struct store_t * Restrict const store)
{
    extern const unsigned char _binary_STARS_Z0_02_dat_start[];
    NewDataTable_from_Pointer((double*) &_binary_STARS_Z0_02_dat_start,
                              store->opacity_STARS,
                              TABLE_OPACITY_STARS_NPARAMS,
                              TABLE_OPACITY_STARS_NDATA,
                              TABLE_OPACITY_STARS_LINES);
}

#endif // OPACITY_ALGORITHMS
