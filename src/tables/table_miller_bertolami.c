#include "../binary_c.h"
No_empty_translation_unit_warning;


#include "../stellar_structure/miller_bertolami_postagb_n.h"
#include "../stellar_structure/miller_bertolami_postagb_coeffs_n.h"

void table_miller_bertolami(struct store_t * Restrict const store)
{
    extern const unsigned char _binary_miller_bertolami_postagb_dat_start[];
    NewDataTable_from_Pointer((double*)&_binary_miller_bertolami_postagb_dat_start,
                              store->miller_bertolami,
                              TABLE_MILLER_BERTOLAMI_N_PARAMS,
                              TABLE_MILLER_BERTOLAMI_N_DATA,
                              TABLE_MILLER_BERTOLAMI_LINES);
    extern const unsigned char _binary_miller_bertolami_postagb_coeffs_L_dat_start[];
    NewDataTable_from_Pointer((double*)&_binary_miller_bertolami_postagb_coeffs_L_dat_start,
                              store->miller_bertolami_coeffs_L,
                              TABLE_MILLER_BERTOLAMI_COEFFS_L_N_PARAMS,
                              TABLE_MILLER_BERTOLAMI_COEFFS_L_N_DATA,
                              TABLE_MILLER_BERTOLAMI_COEFFS_L_LINES);
    extern const unsigned char _binary_miller_bertolami_postagb_coeffs_R_dat_start[];
    NewDataTable_from_Pointer((double*)&_binary_miller_bertolami_postagb_coeffs_R_dat_start,
                              store->miller_bertolami_coeffs_R,
                              TABLE_MILLER_BERTOLAMI_COEFFS_R_N_PARAMS,
                              TABLE_MILLER_BERTOLAMI_COEFFS_R_N_DATA,
                              TABLE_MILLER_BERTOLAMI_COEFFS_R_LINES);
}
                  
