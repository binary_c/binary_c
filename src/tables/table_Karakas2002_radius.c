#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "../zfuncs/Karakas2002_radius.h"

void table_Karakas2002_radius(struct store_t * Restrict const store)
{
    NewDataTable_from_Array(
        KARAKAS2002_RADIUS_DATA,
        store->Karakas2002_radius,
        2,
        2,
        18*4);
}
