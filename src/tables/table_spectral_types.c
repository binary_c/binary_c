#include "../binary_c.h"
No_empty_translation_unit_warning;


static const double jaschek_jaschek_table[100] =

/* cols:
 * 0 : spectral type (O,B,A,F,G,K or M) as defined by the SPECTRAL_TYPE_
 *     macros (converted to a double by 1.0*)
 * 1 : subtype, in double format
 * 2 : temperature for a dwarf star
 * 3 : temperature for a giant star (logL < 4.9)
 * 4 : temperature for a supergiant star (logL > 4.9)
 */

{ 1.0*SPECTRAL_TYPE_O, 3.0, 52500.0, 50000.0, 47300.0,
  1.0*SPECTRAL_TYPE_O, 5.0, 44500.0, 42500.0, 40300.0,
  1.0*SPECTRAL_TYPE_O, 8.0, 35800.0, 34700.0, 34200.0,
  1.0*SPECTRAL_TYPE_B, 0.0, 30000.0, 29000.0, 26000.0,
  1.0*SPECTRAL_TYPE_B, 3.0, 18700.0, 17100.0, 16200.0,
  1.0*SPECTRAL_TYPE_B, 5.0, 15400.0, 15000.0, 13600.0,
  1.0*SPECTRAL_TYPE_B, 8.0, 11900.0, 12400.0, 11200.0,
  1.0*SPECTRAL_TYPE_A, 0.0,  9520.0, 10100.0,  9730.0,
  1.0*SPECTRAL_TYPE_A, 5.0,  8200.0,  8100.0,  8510.0,
  1.0*SPECTRAL_TYPE_F, 0.0,  7200.0,  7150.0,  7700.0,
  1.0*SPECTRAL_TYPE_F, 5.0,  6440.0,  6470.0,  6900.0,
  1.0*SPECTRAL_TYPE_G, 0.0,  6030.0,  5850.0,  6550.0,
  1.0*SPECTRAL_TYPE_G, 5.0,  5770.0,  5150.0,  4850.0,
  1.0*SPECTRAL_TYPE_K, 0.0,  5250.0,  4750.0,  4420.0,
  1.0*SPECTRAL_TYPE_K, 5.0,  4350.0,  3950.0,  3850.0,
  1.0*SPECTRAL_TYPE_M, 0.0,  3850.0,  3800.0,  3650.0,
  1.0*SPECTRAL_TYPE_M, 2.0,  3580.0,  3620.0,  3450.0,
  1.0*SPECTRAL_TYPE_M, 4.0,  3370.0,  3430.0,  2980.0,
  1.0*SPECTRAL_TYPE_M, 6.0,  3050.0,  3240.0,  2600.0,
  1.0*SPECTRAL_TYPE_M, 8.0,  2640.0,  3000.0,  2300};


void table_spectral_types(struct store_t * Restrict const store)
{
#define NLINES 20
    /*
     * Convert Jaschek/Jaschek table into three lookup tables
     */
    double * jaschek_jaschek_dwarf = Malloc(sizeof(double)*NLINES*2);
    double * jaschek_jaschek_giant = Malloc(sizeof(double)*NLINES*2);
    double * jaschek_jaschek_supergiant = Malloc(sizeof(double)*NLINES*2);

    int line;
    for(line=0;line<NLINES;line++)
    {
        int newline=19-line; // invert table

        /*
         * temperature depends on type (k) so a
         * different offset (2+k) in the jaschek table
         */
        *(jaschek_jaschek_dwarf + newline * 2) = *(jaschek_jaschek_table + line*5 + (2+0));
        *(jaschek_jaschek_giant + newline * 2) = *(jaschek_jaschek_table + line*5 + (2+1));
        *(jaschek_jaschek_supergiant + newline * 2) = *(jaschek_jaschek_table + line*5 + (2+2));

        /*
         * spectral type
         */
        double spectype = *(jaschek_jaschek_table + line*5) +
            0.1 * (*(jaschek_jaschek_table + line*5 + 1));
        *(jaschek_jaschek_dwarf + newline * 2 + 1) = spectype;
        *(jaschek_jaschek_giant + newline * 2 + 1) = spectype;
        *(jaschek_jaschek_supergiant + newline * 2 + 1) = spectype;
    }


    NewDataTable_from_Pointer(jaschek_jaschek_dwarf,
                              store->jaschek_jaschek_dwarf,
                              1,
                              1,
                              NLINES);
    NewDataTable_from_Pointer(jaschek_jaschek_giant,
                              store->jaschek_jaschek_giant,
                              1,
                              1,
                              NLINES);
    NewDataTable_from_Pointer(jaschek_jaschek_supergiant,
                              store->jaschek_jaschek_supergiant,
                              1,
                              1,
                              NLINES);
    //ShowDataTable(store->jaschek_jaschek_dwarf);
}
