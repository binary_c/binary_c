#include "../binary_c.h"
No_empty_translation_unit_warning;


#if defined NUCSYN && defined NUCSYN_NOVAE

#include "../nucsyn/nucsyn_novae.h"

void table_novae(struct store_t * Restrict const store)
{
    /*
     * Nova data from Jose and Hernanz (1998)
     */
    extern double _binary_nucsyn_novae_JH98_CO_dat_start[];
    extern double _binary_nucsyn_novae_JH98_ONe_dat_start[];
    extern double _binary_nucsyn_novae_Jose2022_ONe_dat_start[];
    NewDataTable_from_Pointer(_binary_nucsyn_novae_JH98_CO_dat_start,
                              store->novae_JH98_CO,
                              NUCSYN_NOVAE_JH98_CO_FREE_PARAMETERS,
                              NUCSYN_NOVAE_JH98_NUM_ISOTOPES,
                              NUCSYN_NOVAE_JH98_CO_LINES);
    NewDataTable_from_Pointer(_binary_nucsyn_novae_JH98_ONe_dat_start,
                              store->novae_JH98_ONe,
                              NUCSYN_NOVAE_JH98_ONe_FREE_PARAMETERS,
                              NUCSYN_NOVAE_JH98_NUM_ISOTOPES,
                              NUCSYN_NOVAE_JH98_ONe_LINES);
    NewDataTable_from_Pointer(_binary_nucsyn_novae_Jose2022_ONe_dat_start,
                              store->novae_Jose2022_ONe,
                              NUCSYN_NOVAE_JOSE2022_ONe_FREE_PARAMETERS,
                              NUCSYN_NOVAE_JOSE2022_ONe_NUM_ISOTOPES,
                              NUCSYN_NOVAE_JOSE2022_ONe_LINES);

}

#endif // NUCSYN && NUCSYN_NOVAE
