#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Include BSE tables
 */
#ifdef USE_BSE_TABLES
void table_BSE(struct store_t * Restrict const store)
{

#ifdef USE_BSE_TIMESCALES_H
#include  "BSE_TIMESCALES_H.h"

#define WIDTH (TSCLS_ARRAY_SIZE + LUMS_ARRAY_SIZE + GB_ARRAY_SIZE)
    NewDataTable_from_Array(
        TABLE_BSE_TIMESCALES_H_DATA,
        store->BSE_TIMESCALES_H,
        2,
        WIDTH,
        TABLE_BSE_TIMESCALES_H_LINES);

#endif //USE_BSE_TIMESCALES_H

}
#endif//USE_BSE_TABLES
