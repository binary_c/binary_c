#ifndef TABLES_PROTOTYPES_H
#define TABLES_PROTOTYPES_H

#include "../binary_c.h"
No_empty_translation_unit_warning;


void table_1DUP(struct store_t * Restrict const store);
void table_TAMS(struct store_t * Restrict const store);
void table_spectral_types(struct store_t * Restrict const store);
void table_radius_stripped(struct store_t * Restrict const store);

#ifdef USE_2012_MAIN_SEQUENCE_LIFETIMES_TABLE
void table_massive_MS_lifetimes(struct store_t * Restrict const store);
#endif

#ifdef NUCSYN
#ifdef NUCSYN_S_PROCESS
void table_s_process(struct store_t * Restrict const store);
#endif
#endif // NUCSYN
#ifdef STELLAR_COLOURS
void table_Eldridge2012_magnitudes(struct store_t * Restrict  store);
void table_Carrasco2014(struct store_t * Restrict const store);
#endif

#ifdef NUCSYN_SIGMAV_PRE_INTERPOLATE
void table_sigmav(struct store_t * Restrict const store);
#endif
void table_comenv(struct store_t * Restrict const store);


/*
 * Function to make the BSE tables
 */
#ifdef MAKE_BSE_TABLES
void make_BSE_tables(struct stardata_t * RESTICT const stardata);
#endif

/*
 * Function to include the BSE tables after making them
 */
void table_BSE(struct store_t * Restrict const store);

#ifdef COMENV_POLYTROPES
void table_comenv_polytropes(struct store_t * Restrict const store);
#endif

void table_Karakas_ncal(struct store_t * Restrict const store);

void table_novae(struct store_t * Restrict const store);

#ifdef OPACITY_ALGORITHMS
#ifdef OPACITY_ENABLE_ALGORITHM_PACZYNSKI
void table_opacity_paczynski(struct store_t * Restrict const store);
#endif
#ifdef OPACITY_ENABLE_ALGORITHM_FERGUSON_OPAL
void table_opacity_ferguson_opal(struct store_t * Restrict const store);
#endif
#ifdef OPACITY_ENABLE_ALGORITHM_STARS
void table_opacity_STARS(struct store_t * Restrict const store);
#endif
#endif // OPACITY_ALGORITHMS

void table_miller_bertolami(struct store_t * Restrict const store);

void No_return test_table(struct stardata_t * Restrict const stardata,
                          struct data_table_t * Restrict const table);

void table_Karakas2002_lumfunc(struct store_t * Restrict const store);
void table_Karakas2002_radius(struct store_t * Restrict const store);
void table_Hachisu_He(struct store_t * Restrict const store);
void table_Temmink2022(struct stardata_t * const stardata,
                       struct store_t * const store);

#endif // TABLES_PROTOTYPES_H
