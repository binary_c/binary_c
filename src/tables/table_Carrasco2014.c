#include "../binary_c.h"
No_empty_translation_unit_warning;


#include "../stellar_colours/carrasco2014_table3.h"
#include "../stellar_colours/carrasco2014_table4.h"
#include "../stellar_colours/carrasco2014_table5.h"
#include "../stellar_colours/carrasco2014_cooling_table3.h"
#include "../stellar_colours/carrasco2014_cooling_table4.h"
#include "../stellar_colours/carrasco2014_cooling_table5.h"

void table_Carrasco2014(struct store_t * Restrict const store)
{
    /*
     * White dwarf colours from Carrasco et al. (2014, A&A 565, A11)
     */
    NewDataTable_from_Array(CARRASCO2014_TABLE3,
                            store->carrasco2014_table3,
                            CARRASCO2014_NPARAM,
                            CARRASCO2014_NDATA,
                            360);
    NewDataTable_from_Array(CARRASCO2014_TABLE4,
                            store->carrasco2014_table4,
                            CARRASCO2014_NPARAM,
                            CARRASCO2014_NDATA,
                            240);
    NewDataTable_from_Array(CARRASCO2014_TABLE5,
                            store->carrasco2014_table5,
                            CARRASCO2014_NPARAM,
                            CARRASCO2014_NDATA,
                            85);

    /*
     * White dwarf cooling tracks from Carrasco et al. (2014)
     */
    NewDataTable_from_Array(CARRASCO2014_COOLING_TABLE3,
                            store->carrasco2014_cooling_table3,
                            2,
                            2,
                            2500);
    NewDataTable_from_Array(CARRASCO2014_COOLING_TABLE4,
                            store->carrasco2014_cooling_table4,
                            2,
                            2,
                            2500);
    NewDataTable_from_Array(CARRASCO2014_COOLING_TABLE5,
                            store->carrasco2014_cooling_table5,
                            2,
                            2,
                            2500);
}
