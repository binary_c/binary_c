#pragma once
#ifndef OPACITY_PRESCRIPTIONS_H
#define OPACITY_PRESCRIPTIONS_H

#include "opacity_prescriptions.def"

#undef X
#define X(CODE) OPACITY_ALGORITHM_##CODE,
enum { OPACITY_ALGORITHMS_LIST };
#undef X



#endif // OPACITY_PRESCRIPTIONS_H
