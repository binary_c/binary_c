#include "../binary_c.h"
No_empty_translation_unit_warning;

/*
 * Opacity wrapper function.
 *
 * We have multiple opacity routines, so choose these
 * as one of the OPACITY_ALGORITHM_* macros.
 */
#ifdef OPACITY_ALGORITHMS
#include "opacity.h"

double opacity(Opacity_function_args_in)
{
    double kap;

    /*
    printf("opacities %g %g -> %g and %g\n",
           op->temperature,
           op->density,
           opacity_paczynski(stardata,op,OPACITY_ALGORITHM_PACZYNSKI),
           opacity_ferguson_opal(stardata,op,OPACITY_ALGORITHM_FERGUSON_OPAL)
        );
    */

#ifdef OPACITY_ENABLE_ALGORITHM_PACZYNSKI
    if(algorithm == OPACITY_ALGORITHM_PACZYNSKI)
    {
        kap = opacity_paczynski(Opacity_function_args);
    }
    else
#endif

#ifdef OPACITY_ENABLE_ALGORITHM_STARS
    if(algorithm == OPACITY_ALGORITHM_STARS)
    {
        kap = opacity_STARS(Opacity_function_args);
    }
    else
#endif

#ifdef OPACITY_ENABLE_ALGORITHM_FERGUSON_OPAL

        if(algorithm == OPACITY_ALGORITHM_FERGUSON_OPAL)
    {
        kap = opacity_ferguson_opal(Opacity_function_args);
    }
        else
#endif

    {
        kap = 0.0;
        Exit_binary_c(BINARY_C_ALGORITHM_BRANCH_FAILURE,
                      "Opacity algorithm %d unknown\n",
                      algorithm);
    }

    return kap;
}

#endif // OPACITY_ALGORITHMS
