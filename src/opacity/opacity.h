#pragma once
#ifndef OPACITY_H
#define OPACITY_H

#define Opacity_function_args_in                \
    struct stardata_t * stardata,               \
        struct opacity_t * op,                  \
        int algorithm

        
#define Opacity_function_args                   \
    stardata,                                   \
        op,                                     \
        algorithm


#endif // OPACITY_H
