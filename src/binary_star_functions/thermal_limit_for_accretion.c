#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Thermal mass transfer rate accretion limit
 * 
 * Ignored (set to VERY_LARGE_MASS_TRANSFER_RATE)
 * if the accretion_limit_thermal_multiplier is negative. 
 */
double Pure_function thermal_limit_for_accretion(struct star_t * Restrict accretor,
                                                 struct stardata_t * Restrict const stardata)
{
  return
        stardata->preferences->accretion_limit_thermal_multiplier < -TINY ?
        VERY_LARGE_MASS_TRANSFER_RATE :
        (accretor->mass/accretor->tkh
         *
         stardata->preferences->accretion_limit_thermal_multiplier);
}
