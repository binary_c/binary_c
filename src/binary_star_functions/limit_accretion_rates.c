#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "donor_types.h"

/*
  static Pure_function double MM93_k(const double m1,
  const double m2,
  const double xL);
*/


#define Set_nova_state(STAR,STATE)              \
    (STAR)->nova = (STATE);                     \
    Dprint("Set nova state star %d to %s\n",    \
           (STAR)->starnum,                     \
           nova_state_strings[(STAR)->nova]);


void limit_accretion_rates(struct stardata_t * Restrict const stardata)
{

    /*
     * Limit accretion rates by e.g.
     * Super-Eddington mass ejection, novae,
     * this kind of thing, during STABLE mass transfer only.
     *
     * Unstable limits are dealt with elsewhere.
     */
    Star_number k;
    Starloop(k)
    {
        struct star_t * const accretor = &stardata->star[k];
        struct star_t * const donor = &stardata->star[Other_star(k)];

        Dprint("limit acc rate star %d : is currently %g\n",
               k,
               Mdot_net(accretor));
#ifdef KEMP_NOVAE
        accretor->dm_novaHcrit = Hnova_dMH(accretor);
        accretor->dm_novaHecrit = Henova_dMHe(accretor);
#endif//KEMP_NOVAE

        /*
         * Osaki 1985 based on Meyer & Meyer-Hofmeister 1983
         */

        /*
          double k = MM93_k(accretor->mass,
          donor->mass,
          donor->roche_radius/stardata->common.orbit.separation);
          double kappa0 = 0.1;
          double T = Teff_from_star_struct(donor);
          double rho0 = GRAVITATIONAL_CONSTANT*donor->mass*M_SUN/
          (kappa0 * Pow2(donor->radius*R_SUN) * GAS_CONSTANT * T);
          double Q = 2.0 * PI * GAS_CONSTANT * T *
          Pow3(stardata->common.orbit.separation*R_SUN)/
          (GRAVITATIONAL_CONSTANT * M_SUN * (accretor->mass + donor->mass) * k);
          double cs = sqrt(GAS_CONSTANT * T);
          double H = Pow2(donor->radius * R_SUN) * GAS_CONSTANT * T/
          (GRAVITATIONAL_CONSTANT * M_SUN * donor->mass);

          double dr = R_SUN*(donor->roche_radius - donor->radius);
          double mdot = 1.0/exp(1.0) *
          Q * cs * rho0 * exp(-dr/H);
          mdot *= YEAR_LENGTH_IN_SECONDS / M_SUN;
          accretor->derivative[DERIVATIVE_STELLAR_MASS_RLOF_GAIN] += mdot;
        */

        /*
         * Limit mass accretion rate.
         * Only applies if the accretion rate > 0
         */
        const double _accretion_rate = Mdot_net(accretor);
        if(Positive_nonzero(_accretion_rate))
        {
            Dprint("accretion rate is = %g > 0\n",
                   _accretion_rate);
            /* the actual limits */
            double accretion_limit[ACCRETION_LIMIT_N];

            /* booleans to determine whether to apply these limits */
            Boolean apply_accretion_limit[ACCRETION_LIMIT_N];

            /*
             * Accretor limits
             */


            /* Hachisu limit for accretion */
            const double hachisu_rate = stardata->preferences->hachisu_limiter_multiplier
                * Hachisu_max_rate(stardata,
                                   donor,
                                   accretor);

            if(
                stardata->preferences->hachisu_disk_wind == TRUE &&
                WHITE_DWARF(accretor->stellar_type) &&
                Mdot_net(accretor) > hachisu_rate
                )
            {
                apply_accretion_limit[ACCRETION_LIMIT_HACHISU] = TRUE;
                accretion_limit[ACCRETION_LIMIT_HACHISU] =
                    hachisu_rate;
            }
            else
            {
                accretion_limit[ACCRETION_LIMIT_HACHISU] = 0.0;
                apply_accretion_limit[ACCRETION_LIMIT_HACHISU] = FALSE;
            }

            /* Eddington limit for accretion */
            accretion_limit[ACCRETION_LIMIT_EDDINGTON] =
                eddington_limit_for_accretion(accretor->radius,
                                              stardata->preferences->accretion_limit_eddington_steady_multiplier,
                                              Hydrogen_mass_fraction(donor));
            apply_accretion_limit[ACCRETION_LIMIT_EDDINGTON] = TRUE;

            if(In_range(stardata->preferences->fixed_beta_mass_transfer_efficiency, 0, 1) == TRUE)
            {
                /*
                 * Fixed beta limit
                 */
                apply_accretion_limit[ACCRETION_LIMIT_FIXED_BETA] = TRUE;
                accretion_limit[ACCRETION_LIMIT_FIXED_BETA] =
                    fixed_limit_for_accretion(donor, stardata);

                /*
                 * Other limits should be zero
                 */
                accretion_limit[ACCRETION_LIMIT_THERMAL] = 0.0;
                apply_accretion_limit[ACCRETION_LIMIT_THERMAL] = FALSE;
                accretion_limit[ACCRETION_LIMIT_DYNAMICAL] = 0.0;
                apply_accretion_limit[ACCRETION_LIMIT_DYNAMICAL] = FALSE;
            }
            else
            {
                /* no fixed-beta limit */
                apply_accretion_limit[ACCRETION_LIMIT_FIXED_BETA] = FALSE;
                accretion_limit[ACCRETION_LIMIT_FIXED_BETA] = 0.0;

                /* Accretor thermal limit */
                accretion_limit[ACCRETION_LIMIT_THERMAL] =
                    thermal_limit_for_accretion(accretor, stardata);

                /* only for MS, HG or CHeB stars (as in BSE) */
                apply_accretion_limit[ACCRETION_LIMIT_THERMAL] =
                    Boolean_(ON_MAIN_SEQUENCE(accretor->stellar_type)  ||
                             accretor->stellar_type == HERTZSPRUNG_GAP ||
                             accretor->stellar_type == CHeB);

                /* Dynamical accretor limit */
                accretion_limit[ACCRETION_LIMIT_DYNAMICAL] =
                    dynamical_limit_for_accretion(accretor, stardata);
                apply_accretion_limit[ACCRETION_LIMIT_DYNAMICAL] = TRUE;
            }

            /* log limit hitting */
            Boolean hit_accretion_limit[ACCRETION_LIMIT_N];
            for(int i=0;i<ACCRETION_LIMIT_N;i++)
            {
                hit_accretion_limit[i] = Boolean_(Mdot_net(accretor) > accretion_limit[i]);
            }

            /* impose accretion limit */
#undef X
#define X(TYPE,STRING) STRING,
            static const char * strings[] = { ACCRETION_LIMITS_LIST };
#undef X
            for(int i=0;i<ACCRETION_LIMIT_N;i++)
            {
                const double excess_Mdot = Mdot_net(accretor) - accretion_limit[i];
                if(apply_accretion_limit[i] == TRUE)
                {
                    Dprint("impose limit %d %s: Mdot_net = %g, excess_Mdot = %g vs limit %g : apply? %s : over limit? %s\n",
                           i,
                           strings[i],
                           Mdot_net(accretor),
                           excess_Mdot,
                           accretion_limit[i],
                           Yesno(apply_accretion_limit[i]),
                           Yesno(excess_Mdot > 0.0)
                        );
                }

                /*
                 * Mass loss if a limit is hit
                 */
                if(apply_accretion_limit[i] == TRUE &&
                   excess_Mdot > 0.0)
                {
                    hit_accretion_limit[i] = TRUE;

                    Dprint("Star %d exceeded accretion limit %d (%s) : mdot acc %g, limit %g\n",
                           accretor->starnum,
                           i,
                           strings[i],
                           Mdot_net(accretor),
                           accretion_limit[i]
                        );

                    /*
                     * Convert excess_Mdot mass to non-conservative
                     * loss.
                     */
                    accretor->derivative[DERIVATIVE_STELLAR_MASS_NONCONSERVATIVE_LOSS] -=
                        excess_Mdot;
                }
            }

            /* legacy logging */
            if(hit_accretion_limit[ACCRETION_LIMIT_EDDINGTON] == TRUE)
            {
                stardata->model.supedd = TRUE;
            }

            /*
             * Accretion onto a compact object may lead to mass loss
             * (e.g. novae)
             */
            const double accretion_rate = Mdot_net(accretor);
            if(accretion_rate > 0.0 &&
               COMPACT_OBJECT(accretor->stellar_type) &&
               !COMPACT_OBJECT(donor->stellar_type))
            {
                if(WHITE_DWARF(accretor->stellar_type))
                {
                    /*
                     * Accretion onto a white dwarf
                     *
                     * Two rates are of interest:
                     *
                     * steady_burn_rate :
                     *    above this, but below
                     *    new_envelope_rate, burning is steady
                     *
                     * new_envelope_rate :
                     *    above this, a new stellar
                     *    envelope forms
                     *
                     * These rates depend on the composition of the
                     * donated material.
                     *
                     * At present we distinguish hydrogen-rich from helium-rich.                     *
                     * See Claeys et al. (2014) for details.
                     * He-rich: see Nomoto (1982).
                     *
                     * Note that, at present, we do not distinguish
                     * between different accretor types.
                     */

#ifdef KEMP_NOVAE
                    if(HYDROGEN_DONOR)
                    {
                        /*
                         * Burning regime according to Kato's 2014 accretion limits
                         */
                        accretor->WD_accretion_regime = H_accretion_regime_WD(stardata,
                                                                              accretor);
                    }
                    else if(HELIUM_DONOR)
                    {
                        /*
                         * Burning regime according to Piersanti's 2014 accretion limits
                         */
                        accretor->WD_accretion_regime = He_accretion_regime_WD(stardata,
                                                                               accretor);
                    }
#endif // KEMP_NOVAE

                    double steady_burn_rate;
                    double new_envelope_rate;
                    compact_object_accretion_limits(stardata,
                                                    accretor,
                                                    donor,
                                                    &steady_burn_rate,
                                                    &new_envelope_rate);

                    Nova_type nova_type Maybe_unused = NOVA_TYPE_HYDROGEN;
#ifdef KEMP_NOVAE
                    Boolean Hacc_but_below_steady_He_limit = FALSE;
                    Boolean Hacc_Hedet_regime Maybe_unused = FALSE;
#endif // KEMP_NOVAE
                    if(HYDROGEN_DONOR)
                    {
                        /*
                         * Check helium burning limits:
                         * it's possible to accrete above the steady_burn_limit
                         * for hydrogen, but below the steady_burn_limit for helium,
                         * in which case we should have helium novae.
                         *
                         * In this case we should enforce novae, just tag them as helium
                         * novae rather than hydrogen.
                         */
                        double steady_burn_rate_He;
                        double new_envelope_rate_He;

                        /*
                         * Find the accretion limits for helium
                         */
                        {
                            const Stellar_type st_was = donor->stellar_type;
                            donor->stellar_type = HeMS; /* force He limit calculation */
                            compact_object_accretion_limits(stardata,
                                                            accretor,
                                                            donor,
                                                            &steady_burn_rate_He,
                                                            &new_envelope_rate_He);
                            donor->stellar_type = st_was; /* restore stellar type */
                        }

                        /*
                         * Check that we're in the steady H-burning regime,
                         * but should have helium novae, this implies that
                         * steady_burn_rate_He > steady_burn_rate_H.
                         *
                         * Adjust the steady_burn_rate to force novae,
                         * but tag them as NOVA_TYPE_HELIUM.
                         */
                        if(accretion_rate > steady_burn_rate &&
                           accretion_rate < steady_burn_rate_He)
                        {
                            steady_burn_rate = steady_burn_rate_He;
                            nova_type = NOVA_TYPE_HELIUM;
#ifdef KEMP_NOVAE
                            Hacc_but_below_steady_He_limit = TRUE;
                            Hacc_Hedet_regime =
                                He_accretion_regime_WD(stardata,
                                                       accretor)==DETONATION_He;
#endif//KEMP_NOVAE
                        }
#ifdef KEMP_NOVAE
                        else if(accretion_rate < steady_burn_rate_He)
                        {
                            Hacc_but_below_steady_He_limit = TRUE;
                            Hacc_Hedet_regime =
                                He_accretion_regime_WD(stardata,
                                                       accretor)==DETONATION_He;
                        }
#endif // KEMP_NOVAE
                    }

#ifdef KEMP_NOVAE
#undef X
#define X(REGIME) #REGIME,
                    static const char * const accretion_regime_strings[] = { ACCRETION_REGIMES_LIST };
#undef X
                    Dprint("At t=%g : accretion rate = %g -> H novae? %s He novae? %s (acc %g, burn %g, newenv %g) acc regime %u %s\n",
                           stardata->model.time,
                           accretion_rate,
                           Yesno(accretion_rate < steady_burn_rate),
                           Yesno(nova_type == NOVA_TYPE_HELIUM),
                           accretion_rate,
                           steady_burn_rate,
                           new_envelope_rate,
                           accretor->WD_accretion_regime,
                           accretion_regime_strings[accretor->WD_accretion_regime]
                        );
#else
                    Dprint("At t=%g : accretion rate = %g -> H novae? %s He novae? %s (acc %g, burn %g, newenv %g) \n",
                           stardata->model.time,
                           accretion_rate,
                           Yesno(accretion_rate < steady_burn_rate),
                           Yesno(nova_type == NOVA_TYPE_HELIUM),
                           accretion_rate,
                           steady_burn_rate,
                           new_envelope_rate
                        );
#endif // KEMP_NOVAE

                    /*
                     * Check for novae
                     */
                    if(accretion_rate < steady_burn_rate)
                    {
                        /*
                         * Unsteady burning which may lead to novae, hence
                         * mass and angular momentum loss.
                         *
                         * When a nova occurs, the white dwarf retains a fraction
                         * f of the surface hydrogen layer.
                         *
                         * The rest, (1-f) * dM, is lost.
                         *
                         * f = 0 means the white dwarf does not change mass.
                         * f = 1 means the white dwarf increases mass at the maximum rate.
                         *
                         * Angular momentum is dealth with in
                         * angular_momentum_and_eccentricity_derivatives()
                         */

                        const double f = nova_retention_fraction(stardata,
                                                                 accretor,
                                                                 donor,
                                                                 accretion_rate,
                                                                 steady_burn_rate);

#ifdef KEMP_NOVAE
                        accretor->nova_eta = f;
#endif // KEMP_NOVAE

                        Dprint("nova_retention_factor f = %g\n",f);


                        /*
                         * Accretion-induced irradiation
                         * TODO
                         */
                        //double Lac = GRAVITATIONAL_CONSTANT * accretion_rate * accretor->mass * Pow2(M_SUN)/
                        //(2.0 * accretor->radius * R_SUN);
                        //double Lirr = Pow2(donor->radius/stardata->common.orbit.separation)*0.25 * Lac;
                        //double xi = Lirr / (L_SUN * donor->luminosity * 0.5);

                        /*
                         * Fraction of a spherical shell that is
                         * intercepted by the donor star
                         */
                        const double geometric_accretion_fraction =
                            Pow2(donor->radius)/
                            (4.0*Pow2(stardata->common.orbit.separation));

                        /*
                         * Reverse accretion
                         * M1 = WD, M2 = donor, q=M2/M1
                         * Shara+ 1986 Eq.11
                         * see also Martin, Livio and Schaefer (2011)
                         */
                        const double q = donor->mass/accretor->mass;

                        accretor->nova_beta =
                            Fequal(stardata->preferences->beta_reverse_nova,
                                   BETA_REVERSE_NOVAE_GEOMETRY) ?
                            // set from geometric argument
                            geometric_accretion_fraction :
                            // or manually
                            stardata->preferences->beta_reverse_nova;

                        /*
                         * Frictional angular momentum loss (FAML)
                         * Shara+ 1986 Eq.20 but with beta replaced by the true
                         * geometric factor. Should this be the same beta?
                         * This is not clear from the paper!
                         */
                        const double v_rd = orbital_velocity(stardata); // orbital velocity (km/s)
                        const double v_w = 1000.0; // nova velocity (km/s)
                        const double vrat = v_rd/v_w;
                        accretor->nova_faml = stardata->preferences->nova_faml_multiplier *
                            Pythag2(vrat,1.0) * geometric_accretion_fraction *
                            1.0 / (q*(1.0 + q)) / accretor->mass;


                        /*
                         * Calculate the recurrence time, i.e. the time
                         * between successive nova explosions
                         */
                        const double recurrence_time =
                            nova_recurrence_time(accretor,
                                                 accretion_rate,
                                                 accretor->mass);

                        /*
                         * Now we have calculated the properties of the
                         * novae, decide whether we should model them
                         * individually (with short timesteps), or in an
                         * average way (with long timesteps).
                         */
                        if(stardata->preferences->individual_novae == TRUE)
                        {
                            /*
                             * Novae are modelled individually
                             *
                             * First calculate the critical mass for ignition.
                             */
#ifdef KEMP_NOVAE
                            if(HYDROGEN_DONOR)
                            {
                                if(accretor->WD_accretion_regime == STRONGFLASH_H)
                                {
                                    accretor->derivative[DERIVATIVE_STELLAR_H_LAYER_MASS] =
                                        accretion_rate;
                                }
                            }
                            else if(HELIUM_DONOR)
                            {
                                if(accretor->WD_accretion_regime==STRONGFLASH_He ||
                                   accretor->WD_accretion_regime==DETONATION_He)
                                {
                                    accretor->derivative[DERIVATIVE_STELLAR_He_LAYER_MASS] =
                                        accretion_rate;
                                }
                            }
#else
                            /* Not KEMP_NOVAE code */
                            const Nova_type type =
                                HYDROGEN_DONOR ? NOVA_TYPE_HYDROGEN :
                                HELIUM_DONOR ? NOVA_TYPE_HELIUM :
                                NOVA_TYPE_UNKNOWN;
                            const double dMcrit = nova_explosion_layer_mass(stardata,accretor);
                            Dprint("DM %g : mdotcritdwarf %g\n",
                                   dMcrit,dwarf_nova_critical_mass_transfer_rate(stardata,donor,accretor));

                            Dprint("t=%g Nova layer dm_novaH was %g, now %g (+= (accretion_rate = %g * dt = %g) = %g) of M = %g : dMcrit = %g\n",
                                   stardata->model.time,
                                   accretor->dm_novaH,
                                   accretor->dm_novaH + accretion_rate * stardata->model.dt,

                                   accretion_rate,
                                   stardata->model.dt,
                                   accretion_rate*stardata->model.dt,

                                   accretor->mass,
                                   dMcrit
                                );

                            /*
                             * Increase the surface hydrogen layer mass.
                             */
                            accretor->derivative[DERIVATIVE_STELLAR_H_LAYER_MASS] = accretion_rate;

                            /*
                             * Check if we have sufficient mass for explosion
                             */
                            if(accretor->dm_novaH > dMcrit)
                            {
                                /*
                                 * Trigger a nova event
                                 */
                                struct binary_c_nova_event_t * new_nova =
                                    Malloc(sizeof(struct binary_c_nova_event_t));
                                new_nova->accretor = accretor;
                                new_nova->donor = donor;
                                new_nova->accretion_rate = accretion_rate;
                                new_nova->steady_burn_rate = steady_burn_rate;
                                new_nova->new_envelope_rate = new_envelope_rate;
                                new_nova->f = f;
                                new_nova->type = type;
                                new_nova->recurrence_time = recurrence_time;
                                Set_nova_state(accretor,
                                               NOVA_STATE_EXPLODING);
                                if(Add_new_event(stardata,
                                                 BINARY_C_EVENT_NOVA,
                                                 &nova_event_handler,
                                                 &nova_erase_event_handler,
                                                 new_nova,
                                                 NORMAL_EVENT)
                                   != BINARY_C_EVENT_DENIED
                                    )
                                {
                                    /*
                                     * Nova event added
                                     */
                                }
                                else
                                {
                                    /*
                                     * Failed to allocate new event data
                                     */
                                    Safe_free(new_nova);
                                }

                                /*
                                 * If we're exploding now, set the next timeout
                                 */
                                stardata->model.nova_timeout = stardata->model.time +
                                    NOVA_TIMEOUT_FACTOR * // usually 2
                                    1e-6 * // convert yr to Myr
                                    recurrence_time;
                            }
                            else
                            {
                                /*
                                 * Phase between novae.
                                 *
                                 * If there has been a previous nova explosion, i.e.
                                 * accretor->nova is not NOVA_STATE_NONE,
                                 * then we set accretor->nova to NOVA_STATE_QUIESCENT.
                                 *
                                 * If there has not yet been a nova, but we expect one
                                 * soon, set to NOVA_STATE_PRE_EXPLOSION.
                                 */
                                accretor->derivative[DERIVATIVE_STELLAR_MASS_NOVA] = 0.0;
                                Set_nova_state(accretor,
                                               (accretor->nova == NOVA_STATE_QUIESCENT ||
                                                accretor->nova == NOVA_STATE_EXPLODING) ?
                                               NOVA_STATE_QUIESCENT :
                                               NOVA_STATE_PRE_EXPLOSION);

                            }
#endif // KEMP_NOVAE (else)
                        }
                        else
                        {
                            /*
                             * Novae are modelled as average events, which are put in
                             * the normal mass loss.
                             *
                             * We set nova to be NOVA_STATE_EXPLODING on the assumption
                             * that the timestep is long so there are going to be some
                             * explosions, but it's not really relevant.
                             */
                            accretor->derivative[DERIVATIVE_STELLAR_MASS_NOVA] +=
                                - (1.0-f) * accretion_rate;
                            Set_nova_state(accretor,
                                           NOVA_STATE_EXPLODING);
                            Dprint("accretion rate %g -> dM/dt nova = %g\n",
                                   accretion_rate,
                                   accretor->derivative[DERIVATIVE_STELLAR_MASS_NOVA]);

                        }


                        /*
                         * Count novae
                         */
                        accretor->derivative[DERIVATIVE_STELLAR_NUM_NOVAE] =
                            1.0 / recurrence_time;

                        /*
                          donor->derivative[DERIVATIVE_STELLAR_MASS_IRRADIATIVE_LOSS] =
                          - 1.1e18 * (xi/4.0) * pow(1.0 + donor->mass,2.0/3.0) * pow(donor->mass,2.833)
                          * YEAR_LENGTH_IN_SECONDS / M_SUN *
                          stardata->preferences->nova_irradiation_multiplier;
                        */

                        /*
                          Dprint("IRRAD Lac=%g, Lirr=%g, Ldonor = %g, %g vs Mdot %g\n",
                          Lac/L_SUN,
                          Lirr/L_SUN,
                          donor->luminosity,
                          donor->derivative[DERIVATIVE_STELLAR_MASS_IRRADIATIVE_LOSS],
                          accretion_rate);
                        */
                        /*
                         * Nova-induced mass transfer back onto the companion
                         */
                        donor->derivative[DERIVATIVE_STELLAR_MASS_NOVA] +=
                            accretor->nova_beta * -accretor->derivative[DERIVATIVE_STELLAR_MASS_NOVA];
                    }
                    else
                    {
                        /*
                         * We have steady burning.
                         *
                         * Accretion rate exceeds the steady burning rate
                         * so either burning is steady, or the star forms
                         * a new envelope.
                         *
                         * Note: we do not treat the case where
                         * there is steady accretion of hydrogen, but then
                         * helium flashes underneath which would leave to
                         * helium novae. These should be caught here (todo!).
                         */
                        Set_nova_state(accretor,
                                       NOVA_STATE_NONE);

                        if(accretor->stellar_type == COWD &&
                           Is_not_zero(stardata->preferences->COWD_to_ONeWD_accretion_rate) &&
                           accretion_rate > stardata->preferences->COWD_to_ONeWD_accretion_rate)
                        {
                            /*
                             * Trigger conversion to ONe WD (non-violent edge lit
                             * detonation, essentially).
                             *
                             * In practice this only happens when accreting helium
                             * (hydrogen accretion will form a new
                             * giant envelope before this rate can be reached
                             * at all WD masses, see Wang et al. 2017).
                             */
                            accretor->stellar_type = ONeWD;
                        }

#ifdef KEMP_NOVAE
                        if(accretion_rate < new_envelope_rate)
                        {
                            /*
                             * Possibly a supersoft X-ray source : allow steady
                             * burning, do nothing here
                             */
                            accretor->nova_eta = 1.0;

                        }
                        else
                        {
                            /*
                             * Make a new giant envelope :
                             * do not alter the mass accretion rate.
                             * This is handled in apply_stellar_mass_and_angular_momentum.c
                             */
                            accretor->nova_eta = Min(1.0,
                                                     steady_burn_rate / accretion_rate);
                        }

                        if(HYDROGEN_DONOR)
                        {
                            /*
                             * Steady H-burning, but He may not be steady.
                             */
                            if(stardata->preferences->individual_novae == TRUE)
                            {
                                if(Hacc_but_below_steady_He_limit == TRUE)
                                {
                                    /*
                                     * Hacc_but_below_steady_He_limit == TRUE
                                     *
                                     * Steady H burning, but with He novae.
                                     *
                                     * Therefore, we want to transfer all the accreted
                                     * H directly to the He shell mass, and
                                     */
                                    accretor->derivative[DERIVATIVE_STELLAR_He_LAYER_MASS] =
                                        accretor->nova_eta * accretion_rate;
                                    accretor->derivative[DERIVATIVE_STELLAR_H_LAYER_MASS] =
                                        - accretor->dm_novaH / stardata->model.dt;
                                    Dprint("Steady H burn, with He novae _-> set dm_novaHe/dt = %g / %g = %g\n",
                                           accretor->dm_novaHe,
                                           stardata->model.dt,
                                           accretor->derivative[DERIVATIVE_STELLAR_He_LAYER_MASS]);
                                }
                                else
                                {
                                    /*
                                     * Here we are accreting above the steady
                                     * burning limit of both H and He.
                                     *
                                     * Therefore, we want to transfer all the accreted
                                     * material directly to the mass of the WD, and
                                     * the mass in the H and He layers is zero.
                                     */
                                    accretor->derivative[DERIVATIVE_STELLAR_H_LAYER_MASS] =
                                        - (accretor->dm_novaH) / stardata->model.dt;

                                    accretor->derivative[DERIVATIVE_STELLAR_He_LAYER_MASS] =
                                        - (accretor->dm_novaHe) / stardata->model.dt;
                                    Dprint("Above both burning limits -> set dm_novaHe/dt = %g / %g = %g\n",
                                           accretor->dm_novaHe,
                                           stardata->model.dt,
                                           accretor->derivative[DERIVATIVE_STELLAR_He_LAYER_MASS]);
                                }
                            }
                            else
                            {
                                /*
                                 * (this should be the individual_novae == FALSE case)
                                 * add mass at max_steady accretion rate directly to WD
                                 */
                            }
                        }
                        else if(HELIUM_DONOR)
                        {
                            /*
                             * add mass at max_steady accretion rate directly to WD.
                             */
                        }
#endif // KEMP_NOVAE
                    }

#ifdef KEMP_NOVAE
                    if(stardata->preferences->individual_novae == TRUE)
                    {
                        /*
                         * Check for individual H or He novae
                         */

                        /*
                         * Compute recurrence time (years)
                         */
                        const double recurrence_time = nova_recurrence_time(accretor,
                                                                            accretion_rate,
                                                                            accretor->mass);
#ifdef NOVA_DEBUG
                        printf("H-nova recurrence time %g y (limit_accretion_rates) accretion_rate %g\n",
                               recurrence_time,
                               accretion_rate);
#endif
                        if(
                            accretor->WD_accretion_regime == DETONATION_He
                            ||
                            Hacc_Hedet_regime == TRUE
                            )
                        {
                            accretor->dm_novaHecrit = Hedet_dMHe(accretor);
                        }

                        Dprint("limit nova star %d M=%g dm_novaH %g dm_novaHcrit %g, recurrence time %g\n",
                               accretor->starnum,
                               accretor->mass,
                               accretor->dm_novaH,
                               accretor->dm_novaHcrit,
                               recurrence_time);

                        if(
                            accretor->dm_novaH > accretor->dm_novaHcrit
                            &&
                            accretor->WD_accretion_regime == STRONGFLASH_H
                            )
                        {
                            /*
                             * Trigger a hydrogen nova event
                             */
                            struct binary_c_nova_event_t * new_nova =
                                Malloc(sizeof(struct binary_c_nova_event_t));
                            new_nova->accretor = accretor;
                            new_nova->donor = donor;
                            new_nova->accretion_rate = accretion_rate;
                            new_nova->steady_burn_rate = steady_burn_rate;
                            new_nova->new_envelope_rate = new_envelope_rate;
                            new_nova->f = accretor->nova_eta;
                            new_nova->type = NOVA_TYPE_HYDROGEN;
                            new_nova->dMH = accretor->dm_novaHcrit;
                            new_nova->dMHe = accretor->dm_novaHecrit;
                            new_nova->recurrence_time = recurrence_time;
                            Set_nova_state(accretor, NOVA_STATE_EXPLODING);
                            Dprint("H-nova add event on star %d dm %g vs crit %g\n",
                                   accretor->starnum,
                                   accretor->dm_novaH,
                                   accretor->dm_novaHcrit);
                            if(Add_new_event(stardata,
                                             BINARY_C_EVENT_NOVA,
                                             &nova_event_handler,
                                             &nova_erase_event_handler,
                                             new_nova,
                                             NORMAL_EVENT)
                               != BINARY_C_EVENT_DENIED)
                            {
                                /* nova event successfully added */
                            }
                            else
                            {
                                /*
                                 * Failed to allocate new event data
                                 */
                                Safe_free(new_nova);
                            }

                            /*
                             * If we're exploding now, set the next timeout
                             */
                            stardata->model.nova_timeout = stardata->model.time +
                                NOVA_TIMEOUT_FACTOR * // usually 2
                                1e-6 * // convert yr to Myr
                                recurrence_time;

                            accretor->derivative[DERIVATIVE_STELLAR_NUM_NOVAE] = 1.0 / recurrence_time;
                        }

                        else if(
                            accretor->dm_novaHe > accretor->dm_novaHecrit &&
                            (
                                accretor->WD_accretion_regime == STRONGFLASH_He
                                ||
                                Hacc_but_below_steady_He_limit == TRUE
                                )
                            )
                        {
                            /*
                             * Trigger a He nova event
                             */
                            struct binary_c_nova_event_t * new_nova =
                                Malloc(sizeof(struct binary_c_nova_event_t));
                            new_nova->accretor = accretor;
                            new_nova->donor = donor;
                            new_nova->accretion_rate = accretion_rate;
                            new_nova->steady_burn_rate = steady_burn_rate;
                            new_nova->f = accretor->nova_eta;
                            new_nova->type = NOVA_TYPE_HELIUM;
                            new_nova->dMH = accretor->dm_novaHcrit;
                            new_nova->dMHe = accretor->dm_novaHecrit;
                            new_nova->recurrence_time = recurrence_time;
                            Set_nova_state(accretor, NOVA_STATE_EXPLODING);
                            Dprint("He-nova add event dm %g vs crit %g\n",
                                   accretor->dm_novaH,
                                   accretor->dm_novaHcrit);
                            if(Add_new_event(stardata,
                                             BINARY_C_EVENT_NOVA,
                                             &nova_event_handler,
                                             &nova_erase_event_handler,
                                             new_nova,
                                             NORMAL_EVENT)
                               != BINARY_C_EVENT_DENIED)
                            {
                                /*
                                 * Nova event successfully added
                                 */
                            }
                            else
                            {
                                /*
                                 * Failed to allocate new event data
                                 */
                                Safe_free(new_nova);
                            }

                            /*
                             * If we're exploding now, set the next timeout
                             */
                            stardata->model.nova_timeout = stardata->model.time +
                                NOVA_TIMEOUT_FACTOR * // usually 2
                                1e-6 * // convert yr to Myr
                                nova_recurrence_time(accretor,
                                                     accretion_rate,
                                                     accretor->mass);
                            accretor->derivative[DERIVATIVE_STELLAR_NUM_NOVAE] = 1.0 /recurrence_time;

                        }
                        else
                        {
                            /*
                             * Phase between novae.
                             *
                             * If there has been a previous nova explosion, i.e.
                             * accretor->nova is not NOVA_STATE_NONE,
                             * then we set accretor->nova to NOVA_STATE_QUIESCENT.
                             *
                             * If there has not yet been a nova, but we expect one
                             * soon, set to NOVA_STATE_PRE_EXPLOSION.
                             */
                            accretor->derivative[DERIVATIVE_STELLAR_MASS_NOVA] = 0.0;
                            Set_nova_state(accretor,
                                           (accretor->nova == NOVA_STATE_QUIESCENT ||
                                            accretor->nova == NOVA_STATE_EXPLODING) ?
                                           NOVA_STATE_QUIESCENT :
                                           NOVA_STATE_PRE_EXPLOSION);
                        }
                    }
#endif // KEMP_NOVAE
                }
                else
                {
                    /*
                     * Accretion onto a neutron star/black hole, what to do?
                     */
                    Set_nova_state(accretor,
                                   NOVA_STATE_NONE);
                }
            }
            else
            {
                /*
                 * No longer a compact object : cannot have novae
                 */
                Set_nova_state(accretor, NOVA_STATE_NONE);
            }
        } // accretion rate > 0 check
        else
        {
            /*
             * Accretion rate is zero or negative, i.e. mass
             * is constant or being lost.
             */


            /*
             * If we were previously having novae, keep the state
             * at NOVA_STATE_QUIESCENT until nova_timeout has been reached
             */
            Set_nova_state(accretor,
                           (
                               (accretor->nova == NOVA_STATE_QUIESCENT ||
                                accretor->nova == NOVA_STATE_EXPLODING) &&
                               stardata->model.time < stardata->model.nova_timeout
                               )
                           ? NOVA_STATE_QUIESCENT : NOVA_STATE_NONE);
        }
    }
}
