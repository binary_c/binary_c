#include "../binary_c.h"
No_empty_translation_unit_warning;

double wind_orbital_angular_momentum_derivative(
    const struct stardata_t * Restrict const stardata,
    const struct orbit_t * const orbit,
    const double m[2],
    const double mdot_loss[2],
    const double mdot_gain[2],
    const double vwind[2])
{
    /*
     * Rate of change of orbital angular momentum because of
     * wind mass transfer.
     *
     * Returns the correct sign, i.e. negative for angular momentum loss.
     */
    double djorb_dt = 0.0;
    const double mtot = m[0] + m[1];
    const double mdot_system =
        /* mass loss : derivatives are negative, so this term is positive */
        -(mdot_loss[0]+mdot_loss[1])
        /* mass gain: derivatives are positive, so this term is negative */
        -(mdot_gain[0]+mdot_gain[1]);

    const double Jorb = orbit->angular_momentum;
    const double h = Jorb / mtot;
    const double q[2] = {
        m[0]/m[1],
        m[1]/m[0]
    };

    if(stardata->preferences->wind_angular_momentum_loss == WIND_ANGMOM_LOSS_BSE)
    {
        /* original Hurley/Tout (BSE) formalism */
        djorb_dt =  stardata->preferences->wind_djorb_fac *
            (
                (-mdot_loss[0] + q[0]*mdot_gain[0]) * Pow2(m[1])
                +
                (-mdot_loss[1] + q[1]*mdot_gain[1]) * Pow2(m[0])
                )
            * h/(m[0] * m[1]);
    }
    else if(stardata->preferences->wind_angular_momentum_loss == WIND_ANGMOM_LOSS_SPHERICALLY_SYMMETRIC)
    {
        /*
         * CAB + SdM 2010
         *   Assume that this takes away the specific AM of the mass losing
         *   star.
         *
         *   In case you want to improve this: This assumption may be alright
         *   for fast winds. For slow winds the AM loss is possibly
         *   considerably more if the wind can still interact with orbit. It
         *   may also feed some AM back into the orbit of the second
         *   star. see e.g Hurley et al thesis and Bonacic et al 2008 for an
         *   improved description.
         */
        const double dm_windloss_0 = -mdot_loss[0];
        const double dm_windloss_1 = -mdot_loss[1];
        const double dm_windaccr_0 = mdot_gain[0];
        const double dm_windaccr_1 = mdot_gain[1];

        const double dm_lost_from_star_0 =  dm_windloss_0 - dm_windaccr_1;
        const double dm_lost_from_star_1 =  dm_windloss_1 - dm_windaccr_0;

        djorb_dt = h /(m[0] * m[1])
            * ( dm_lost_from_star_0 * Pow2(m[1]) + dm_lost_from_star_1 * Pow2(m[0]));
    }
    else if(stardata->preferences->wind_angular_momentum_loss == WIND_ANGMOM_LOSS_LW ||
            stardata->preferences->wind_angular_momentum_loss == WIND_ANGMOM_LOSS_LW_HYBRID)
    {
        if(stardata->preferences->wind_angular_momentum_loss == WIND_ANGMOM_LOSS_LW)
        {
            /*
             * dJorb = lw * dM * Jorb/(M[0]+M[1])
             */
            djorb_dt = stardata->preferences->lw * mdot_system * Jorb / mtot;
        }
        else if(stardata->preferences->wind_angular_momentum_loss == WIND_ANGMOM_LOSS_LW_HYBRID)
        {
            // assume faster wind is donor (not always true!)
            const double vw1 = Max(vwind[0],vwind[1])*1e-5;

            // <vorb>
            const double vorb = 1e-5*sqrt ( GRAVITATIONAL_CONSTANT *
                                            mtot*M_SUN/
                                            (orbit->separation*R_SUN));

            if(vw1 > vorb)
            {
                /*
                 * fast wind : hence dJorb = lw * dM * h
                 * is slow, assume conservative mass transfer (djorb_dt=0)
                 */
                djorb_dt = stardata->preferences->lw * mdot_system * h;
            }
        }
    }

    return -djorb_dt;
}
