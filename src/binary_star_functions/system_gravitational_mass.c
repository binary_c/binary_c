#include "../binary_c.h"
No_empty_translation_unit_warning;


double Pure_function system_gravitational_mass(const struct stardata_t * const stardata)
{
    /*
     * Return the system gravitational mass
     */
    return Sum_of_stars(mass);
}
