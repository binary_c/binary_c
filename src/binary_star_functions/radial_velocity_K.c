#include "../binary_c.h"
No_empty_translation_unit_warning;


double Pure_function radial_velocity_K(struct stardata_t * Restrict const stardata,
                                       const double inclination,
                                       const Star_number n)
{
    /*
     * Radial velocity semi-amplitude K, in cgs (cm/s),
     * for a given inclination (in radians).
     *
     * If n=0, return it for the system.
     * If n=1,2 return it for star n-1.
     *
     * Returns zero (0.0) for non-binary systems.
     */
    double K;
    Star_number nstar = n-1;

    if(System_is_binary)
    {
        double aa = stardata->common.orbit.separation * R_SUN;

        if(nstar!=-1)
        {
            aa *= stardata->star[Other_star(nstar)].mass / 
                (stardata->star[0].mass + stardata->star[1].mass);
        }

        K = 2.0*PI * aa * sin(inclination)
            /
            (
                stardata->common.orbit.period*YEAR_LENGTH_IN_SECONDS*
                sqrt(1.0-Pow2(stardata->common.orbit.eccentricity))
                );
    }
    else
    {
        K = 0.0;
    }

    /*
      // alternative formula
    double Kalt = sqrt(GRAVITATIONAL_CONSTANT/(1.0-Pow2(stardata->common.orbit.eccentricity))) *
        stardata->star[Other_star(nstar)].mass * M_SUN * 
        sin(inclination) *
        1.0/sqrt(stardata->star[0].mass*M_SUN + stardata->star[1].mass*M_SUN) *
        1.0/sqrt(stardata->common.orbit.separation * R_SUN);

    printf("K (binary=%d this star %d, other star %d) = sqrt(%g * %g * (%g+%g)*(1+%g)/((1-%g) * %g*%g)) = sqrt(%g) = %g (Kalt = %g)\n",
           System_is_binary,
           nstar,
           Other_star(nstar),
           GRAVITATIONAL_CONSTANT,
           M_SUN,
           stardata->star[0].mass,
           stardata->star[1].mass,
           stardata->common.orbit.eccentricity,
           stardata->common.orbit.eccentricity,
           R_SUN,
           stardata->common.orbit.separation,
           GRAVITATIONAL_CONSTANT*
           M_SUN*(stardata->star[0].mass+stardata->star[1].mass)*
           (1.0+stardata->common.orbit.eccentricity)/
           ((1.0-stardata->common.orbit.eccentricity)*
            (R_SUN*stardata->common.orbit.separation)),
           K,Kalt);
    
    */
    return K;
}
