/*****************************************************************************
 * A routine to determine the outcome of a collision or coalescence
 * of two degenerate cores.
 * Entered with stellar_type1,2 = 2 or 3
 * with M <= Mflash, 6, 10, 11 or 12
 *****************************************************************************/

#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef BSE

#define THE 1.0e8
#define TC 1.0e9
#define GMR 1.906e15
#define CVHE 3.1e7
#define CVC 8.27e6
#define CVNE 7.44e6
#define EHE 5.812e17
#define EC 2.21e17
#define ENE 2.06e17

/** The stellar_typen are the stellar types, presumably for the stars 1 and 2 **/
/** with star 3 being the resultant **/
/** similarly for m **/

/** ebinde is the binding energy of the envelope  **/

int degenerate_core_merger(struct stardata_t * const stardata,
                           struct star_t * const star1,
                           struct star_t * const star2,
                           const Stellar_type stellar_type1,
                           const Stellar_type stellar_type2,
                           Stellar_type * const stellar_type3,
                           const double m1,
                           const double m2,
                           double * const m3,
                           double * const ebinde)
{
    /* a ton of dummy variables */
    double r1,r2,r3,mhe,mc,mne,ebindi,ebindf,deleb,de,enuc;
    double x,y,m0,mflash;

    if(stardata->preferences->degenerate_core_helium_merger_ignition)
    {
        /*
         * Special case for R stars:
         * HeWD + GB -> immediate core ignition to CHeB
         */
        if((stellar_type2==HeWD && stellar_type1==GIANT_BRANCH)||
           (stellar_type1==HeWD && stellar_type2==GIANT_BRANCH))
        {
            /*
             * Here you can either select a minimum mass for He ignition,
             * or perhaps just ignite all of them on the assumption that the
             * core would soon grow and ignite anyway. This is reasonable.
             */
            *stellar_type3 = GIANT_BRANCH;
            return 0;
        }

        if((stellar_type2==HeWD && stellar_type1==CHeB)||
           (stellar_type1==HeWD && stellar_type2==CHeB))
        {
            *stellar_type3 = CHeB;
            return 0;
        }
    }


    Dprint("In degenerate_core_merger... ");

    /*
     * Calculate the core radii setting m0 < mflash using dummy values as we
     * know it to be true if stellar_type = 2 or 3.
     */
    m0 = 1.0;
    mflash = 2.0;
    r1 = core_radius(stardata,
                     star1,
                     stellar_type1,
                     Core_stellar_type(stellar_type1),
                     m1,
                     m0,
                     0.0,
                     mflash);

    Dprint("Set r1=%g from stellar_type1=%d m1=%g m0=%g mflash=%g\n",r1,stellar_type1,m1,m0,mflash);
    r2 = core_radius(stardata,
                     star2,
                     stellar_type2,
                     Core_stellar_type(stellar_type2),
                     m2,
                     m0,
                     0.0,
                     mflash);

    Dprint("Set r2=%g from stellar_type2=%d m2=%g m0=%g mflash=%g\n",r2,stellar_type2,m2,m0,mflash);
    r3 = core_radius(stardata,
                     star1,
                     *stellar_type3,
                     Core_stellar_type(*stellar_type3),
                     *m3,
                     m0,
                     0.0,
                     mflash);
    Dprint("Set r3=%g from stellar_type3=%d m3=%g m0=%g mflash=%g\n",r3,*stellar_type3,*m3,m0,mflash);
    /*
     * Calculate the initial binding energy of the two seperate cores and the
     * difference between this and the final binding energy of the new core.
     */
    ebindi = m1*m1/r1 + m2*m2/r2;
    Dprint("Set ebindi =%g from m1=%g r1=%g m2=%g r2=%g\n",ebindi,m1,r1,m2,r2);

    ebindf = (*m3)*(*m3)/r3;
    Dprint("Set ebindf =%g from m3=%g r3=%g\n",ebindf,*m3,r3);

    deleb = fabs(ebindi - ebindf);
    Dprint("Deleb = %g from ebindi=%g ebindf=%g\n",deleb,ebindi,ebindf);


    /*
     * If an envelope is present reduce its binding energy by the amount
     * of energy liberated by the coalescence.
     */
    *ebinde = Max(0.0,(*ebinde - deleb));
    Dprint("degenerate_core_merger set ebinde %g from deleb=%g\n",*ebinde,deleb);


    if(stellar_type1<=FIRST_GIANT_BRANCH)
    {
        /*
         * Distribute the mass into core mass groups where mhe represents Helium
         * core mass, mc represents Carbon core mass and mne represents a Neon
         * core mass or any mass that is all converted Carbon.
         */
        mhe = 0.0;
        mc = 0.0;
        mne = 0.0;
        if(stellar_type1==HeWD)
        {
            mhe += m1;
        }
        else if(stellar_type1==ONeWD)
        {
            mne += m1;
        }
        else
        {
            mc += m1;
        }

        if((stellar_type2<=FIRST_GIANT_BRANCH)||
           (stellar_type2==HeWD))
        {
            mhe += m2;
        }
            else if(stellar_type2==ONeWD)
            {
                mne += m2;
            }
            else
            {
                mc += m2;
            }
            /*
             * Calculate the temperature generated by the merging of the cores.
             */
            //double temp = (deleb/(CVHE*mhe+CVC*mc+CVNE*mne))*GMR;

            /*
             * To decide if He is converted to C we use:
             *    3He4 -> C12 , T > 10^8 K , 7.274 Mev released,
             * to decide if C is converted further we use:
             *    2C12 -> Ne20 + alpha , T > 10^9 K , 4.616 Mev released.
             * and to decide if O is converted further we use:
             *    2O16 -> P31 + p , T > 10^9 K , 7.677 Mev released.
             * To obtain the heat capacity of an O/Ne WD and to gain an idea of the
             * energy released from the further processing of an O/Ne WD we use:
             *    2Ne20 + gamma -> O16 + Mg24 + alpha , T > 10^9 K , 4.583 Mev released.
             * For a CO core the composition is assumed to be 20% C, 80% O and for
             * an ONe core 80% O, 20% Ne.
             *
             * Decide if conversion of elements can take place.
             *     if(temp.gt.THE)then
             */

            if(stardata->preferences->degenerate_core_merger_nucsyn == FALSE ||
               Is_not_zero(stardata->preferences->degenerate_core_merger_dredgeup_fraction))
            {
                // Assume merger of the cores NEVER disrupts the
                // new core.
                enuc = x = y = 0.0;
            }
            else
            {
                x = 1.0;
                y = 0.0;
                enuc = (x*EHE*mhe + y*(EC*mc + ENE*mne))/GMR;
            }

            /*
             * Calculate the difference between the binding energy of the star
             * (core + envelope) and the nuclear energy. The new star will be
             * destroyed in a SN if dE < 0.
             */
            de = (ebindf + *ebinde) - enuc;

            /*
             * If the star survives and an envelope is present then reduce the
             * envelope binding energy by the amount of liberated nuclear energy.
             * The envelope will not survive if its binding energy is reduced to <= 0.
             */

            if(More_or_equal(de,0.0))
            {
                *ebinde = Max(0.0,((*ebinde) - enuc));
            }

            /*
             * Now determine the final evolution state of the merged core.
             */
            if(de<0.0)
            {
                *stellar_type3 = MASSLESS_REMNANT;
            }

            if(*stellar_type3==FIRST_GIANT_BRANCH)
            {
                if(x>0.5)
                {
                    *stellar_type3 = TPAGB;
                }
                else if(Less_or_equal(*ebinde,0.0))
                {
                    *stellar_type3 = HeWD;
                }
            }
            else if(*stellar_type3==CHeB)
            {
                if(x>0.5)
                {
                    *stellar_type3 = TPAGB;
                }
                else if(Less_or_equal(*ebinde,0.0))
                {
                    *stellar_type3 = HeMS;
                }
            }

            if((*stellar_type3==TPAGB)&&
               (y<0.5))
            {
                if(Less_or_equal((*ebinde),0.0))
                {
                    *stellar_type3 = COWD;
                }
            }
            else if((*stellar_type3==TPAGB)&&
                    (y>0.5)&&
                    Less_or_equal(*ebinde,0.0))
            {
                *stellar_type3 = ONeWD;
            }

            if((*stellar_type3==HeWD)&&
               (x>0.5))
            {
                *stellar_type3 = COWD;
            }

            if((*stellar_type3==COWD)&&
               (y>0.5))
            {
                *stellar_type3 = ONeWD;
            }


            /* V1.20pre50 allow HeWD above 0.5Msun to become HeMS */
            if((*stellar_type3==HeWD)&&
               (*m3 > HeWD_HeWD_IGNITION_MASS))
            {
                *stellar_type3=HeMS;
            }
            else if(WHITE_DWARF(*stellar_type3) &&
                    More_or_equal(*m3,
                                  chandrasekhar_mass_wrapper(stardata,
                                                             *stellar_type3,
                                                             *m3,
                                                             *m3)))
            {
                *stellar_type3 = MASSLESS_REMNANT;
            }

            if(*stellar_type3==MASSLESS_REMNANT)
            {
                *m3 = 0.0;
            }
        }

        return BINARY_C_NORMAL_EXIT;
    }
#endif
