#include "../binary_c.h"
No_empty_translation_unit_warning;


void determine_roche_lobe_radii(struct stardata_t * Restrict const stardata,
                                struct orbit_t * const orbit)
{
    /* Determine the Roche radii (and time derivative) for both stars */
    Star_number k;
    Starloop(k)
    {
        SETstar(k);
        determine_roche_lobe_radius(stardata,orbit,star);
    }
}
