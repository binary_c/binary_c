#include "../binary_c.h"
No_empty_translation_unit_warning;


#ifdef MAIN_SEQUENCE_STRIP

#include "radius_stripped.h"

double radius_stripped (const double phase_start_mass,
                        const double tauMS,
                        const double mass,
                        const double rwas,
                        struct star_t * const star,
                        struct stardata_t * const stardata)
{
    /*
     * Return the radius as a function of
     * 1) initial mass (before mass stripping, e.g. ZAMS mass)
     * 2) age relative to the main sequence lifetime of a single star with the
     * given initial mass (=tauMS)
     * 3) amount of mass stripped
     */

    // table lookup parameters
    double x[3] = { phase_start_mass,
                    tauMS,
                    mass / phase_start_mass };
    double r[1]; // results (i.e. radius multiplication factor)


    /* Interpolate to find the radius */
    Interpolate(store->MS_strip,
                x,r,
                TABLE_MASSLOSS_RADIUS,
                FALSE);

    /* multiplicative factor */
    double fr = r[0];

#ifdef MAIN_SEQUENCE_STRIP_USELOG
    fr = exp10(fr);
#endif // MAIN_SEQUENCE_STRIP_USELOG

    Dprint("MSSTRIP mt=%g m0=%g age/tms=%g m/m0=%g fm=%g result: log fr=%g : fr=%g : R=%g > %g\n",
           mass,
           x[0],x[1],x[2],
           mass/phase_start_mass,
           r[0],fr,
           rwas,
           rwas*fr
        );

    return fr;
}

#endif // MAIN_SEQUENCE_STRIP
