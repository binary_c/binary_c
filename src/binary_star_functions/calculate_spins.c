#include "../binary_c.h"
No_empty_translation_unit_warning;

void calculate_spins(struct stardata_t * Restrict const stardata)
{
    Foreach_evolving_star(star)
    {
        star->omega = omegaspin(stardata,star);
        Dprint("Spin star %d (type %d) from j=%30.12e r=%g menv = %g - %g = %g I = %30.12e omega = %30.12e (omega orb %30.12e)\n",
               star->starnum,
               star->stellar_type,
               star->angular_momentum,
               star->radius,
               star->mass,
               Outermost_core_mass(star),
               envelope_mass(star),
               moment_of_inertia(stardata,star,star->effective_radius),
               star->omega,
               System_is_binary ? stardata->common.orbit.angular_frequency : -1.0
            );
        Nancheck(star->omega);
    }
}
