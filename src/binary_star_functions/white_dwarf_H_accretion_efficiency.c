#include "../binary_c.h"
No_empty_translation_unit_warning;


double Pure_function white_dwarf_H_accretion_efficiency(
    struct stardata_t * const stardata,
    struct star_t * Restrict const donor,
    struct star_t * Restrict const accretor,
    const double Mdot_tr
    )
{
    /*
     * Determine the efficiency of hydrogen accretion onto
     * the WD. See Claeys et al 2014 (Appendix B).
     * which follows Hachisu et al. (1999) ApJ 522,487.
     */
    double efficiency;

    if(NAKED_HELIUM_STAR(donor->stellar_type))
    {
        efficiency = 1.0;
    }
    else
    {
        if(accretor->stellar_type == HeWD)
        {
            /* helium white dwarfs do not burn */
            efficiency = 0.0;
        }
        else
        {
            /*
             * Maximum transfer rate C14 Eq. B.4 (Msun/year)
             */
            const double Mdot_cr_H = Hachisu_max_rate(stardata,
                                                      donor,
                                                      accretor);

/*
 * RGI: Joke's code read like this, but
 * there's nothing in the paper (e.g. Eq. B.3)
 * to back it up, so I commented this out
 */
/*
  if(Mdot_tr > 1e-4)
  {
  efficiency = 1.0;
  }
*/

            /* Eq. B.3 */
            efficiency =
                (Mdot_tr > Mdot_cr_H) ? (Mdot_cr_H / Mdot_tr) :
                (Mdot_tr < Mdot_cr_H * 0.125) ? 0.0 :
                1.0;
        }

        /* must be in the range 0 .. 1 */
        Clamp(efficiency,0.0,1.0);
    }
    return efficiency;
}
