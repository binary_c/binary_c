#include "../binary_c.h"
No_empty_translation_unit_warning;


unsigned int test_if_primary_still_fills_roche_lobe(struct stardata_t * Restrict const stardata)
{
    /*
     * Test whether the primary still fills its Roche lobe (i.e.
     * R>RL where R is its radius, RL its Roche radius),
     * and whether the system is a contact binary.
     *
     * Returns:
     *
     * CONTACT if the binary is a contact binary
     *    (i.e. both stars have R>RL)
     *
     * NO_CONTACT if the binary is still undergoing RLOF
     *    (only the donor has R>RL)
     *
     * EVOLUTION_END_ROCHE if both stars have R<RL
     */
    RLOF_stars;

    Dprint("Test whether the primary still fills its Roche lobe\n");


#ifdef ADAPTIVE_RLOF
    double f = 1.0;
    if(stardata->model.in_RLOF==TRUE)
    {
        /*
         * Shrink the Roche lobe to keep the star in RLOF even if it's
         * not quite... this is a numerical stability trick, nothing more!
         */
        f = RLOF_STABILITY_FACTOR;
    }
#else
    const double f = 1.0;
#endif

    if(donor->radius > f*donor->roche_radius)
    {
        /*
         * Test for a contact system
         */
        Dprint("Primary overflows (st=%d M=%20.12g Mc=%g R=%g > (f=%g)*(RL=%g)=%g\n",
               donor->stellar_type,
               donor->mass,
               donor->core_mass[CORE_He],
               donor->radius,
               f,
               donor->roche_radius,
               f*donor->roche_radius);

        if(accretor->radius > f*accretor->roche_radius)
        {
            Dprint("Secondary also overflows (st=%d M=%20.12g Mc=%g R=%g > f*RL=%g) : return CONTACT\n",
                   accretor->stellar_type,
                   accretor->mass,
                   accretor->core_mass[CORE_He],
                   accretor->radius,
                   f*accretor->roche_radius);
            return EVOLUTION_CONTACT;
        }

        else
        {
            stardata->model.iter++;
            Dprint("Return NO_CONTACT (secondary st=%d M=%g Mc=%g R=%g < f*RL=%g)\n",
                   accretor->stellar_type,
                   accretor->mass,
                   accretor->core_mass[CORE_He],
                   accretor->radius,
                   f*accretor->roche_radius);
            return EVOLUTION_NO_CONTACT;
        }
    }
    else
    {
#ifdef ADAPTIVE_RLOF
        /* reset the RLOF clock */
        stardata->star[0].RLOF_starttime = 0.0;
#endif

        Dprint("END ROCHE at t=%g\n",stardata->model.time);

        return EVOLUTION_END_ROCHE;
    }
    Dprint("test_if_primary_fills_roche_lobe returns NO it doesn't\n");
}
