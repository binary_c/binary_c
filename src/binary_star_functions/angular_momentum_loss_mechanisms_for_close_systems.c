#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Angular momentum loss because of gravitational radiation
 */

void angular_momentum_loss_mechanisms_for_close_systems(struct stardata_t * Restrict const stardata)
{
    const double jdot = gravitational_radiation_jdot(stardata);
    const double edot = gravitational_radiation_edot(stardata);

    stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM_GRAVITATIONAL_RADIATION] = jdot;
    stardata->model.derivative[DERIVATIVE_ORBIT_ECCENTRICITY_GRAVITATIONAL_RADIATION] = edot;

    Dprint("Jdot = %30.12e (from close system losses)\n",jdot);
    Dprint("Edot = %30.12e (from close system losses)\n",edot);
}
