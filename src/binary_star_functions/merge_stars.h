#pragma once
#ifndef MERGE_STARS_H
#define MERGE_STARS_H

/*
 * Header file for merge_stars.c
 */

#ifdef NUCSYN
#define _Xej_dec , Abundance * const
#define _Xej _Xej_dec Xej Maybe_unused
#else
#define _Xej_dec
#define _Xej
#endif //NUCSYN

/* numbers of the more evolved, less evolved and merged stars
 * (the massless star is the less evolved) */
enum {
    MORE_EVOLVED = 0,
    LESS_EVOLVED = 1,
    MERGED = 2,
    MASSLESS = 1
};

#undef Mergeprint
#define Mergeprint(...) printf("CEE %40s:%5d (ycheck %g %g) ",          \
                               __func__,                                \
                               __LINE__,                                \
                               stardata->common.zero_age.mass[0] + stardata->common.zero_age.mass[1], \
                               system_gravitational_mass(stardata) + nucsyn_total_yield(stardata)); \
    Dprint(__VA_ARGS__)
#undef Mergeprint
#define Mergeprint(...) Dprint(__VA_ARGS__)
#define _Compute_timescales(STAR)                                       \
    stellar_timescales(stardata,                                        \
                       STAR,                                            \
                       STAR->bse,                                       \
                       STAR->phase_start_mass,                          \
                       (STAR->stellar_type == HeGB || STAR->stellar_type==HeHG) ? Outermost_core_mass(STAR) : STAR->mass, \
                       STAR->stellar_type);

#define _New_merged_supernova                   \
    new_supernova(stardata,                     \
                  stars[MERGED],                \
                  NULL,                         \
                  stars[MERGED]);

/*
 * Prototypes of functions used in merge_stars.c
 */
static void _to_MS_merger(struct stardata_t * const stardata,
                          struct star_t ** const stars
                          _Xej);
static void _to_HeMS_merger(struct stardata_t * const stardata,
                            struct star_t ** const stars
                            _Xej);
static void _to_hydrogen_giant_merger(struct stardata_t * const stardata,
                                      struct star_t ** const stars
                                      _Xej);
static void _to_helium_giant_merger(struct stardata_t * const stardata,
                                    struct star_t ** const stars
                                    _Xej);
static void _to_WD_merger(struct stardata_t * const stardata,
                          struct star_t ** const stars
                          _Xej);
static void _to_NS_BH_merger(struct stardata_t * const stardata,
                             struct star_t ** const stars
                             _Xej);
static void _to_massless_merger(struct stardata_t * const stardata,
                                struct star_t ** const stars
                                _Xej);
#ifdef NUCSYN
static void _initial_nucsyn(struct stardata_t * const stardata Maybe_unused,
                            struct star_t ** const stars Maybe_unused,
                            const double merged_mass,
                            Abundance * const Xej);
static void _merge_yields(struct star_t ** const stars);
#endif // NUCSYN
static void _merge_cores(struct stardata_t * const stardata,
                         struct star_t ** const stars);
static void _MS_merger_age_Selma(struct stardata_t * const stardata,
                                 struct star_t ** const stars);
static void _MS_merger_age_Tout(struct stardata_t * const stardata,
                                struct star_t ** const stars);



#endif // MERGE_STARS_H
