#include "../binary_c.h"
No_empty_translation_unit_warning;


#define TOTAL_RADIUS                                            \
    ( stardata->star[0].radius + stardata->star[1].radius )

#define PERIASTRON_DISTANCE                                             \
    (stardata->common.orbit.separation*(1.0 - stardata->common.orbit.eccentricity))

/*
 * This function checks that the stars do not collide at periastron 
 * (the point of closest approach) for the first time
 */
Boolean check_for_collision_at_periastron(struct stardata_t * Restrict const stardata)
{ 

    Boolean new_collision = 
        Boolean_(
            NEITHER_STAR_MASSLESS &&
            stardata->common.orbit.separation > -TINY &&
            Less_or_equal(PERIASTRON_DISTANCE,TOTAL_RADIUS) && 
            stardata->model.intpol==0
            );
    
    Dprint("Check for collision : peri distance = %g*(1-%g) = %g : radius = %g + %g = %g (RL0=%g, RL1=%g) -> %d\n",
           stardata->common.orbit.separation,
           stardata->common.orbit.eccentricity,
           PERIASTRON_DISTANCE,
           stardata->star[0].radius,
           stardata->star[1].radius,
           TOTAL_RADIUS,
           stardata->star[0].roche_radius,
           stardata->star[1].roche_radius,
           new_collision
        );

    Dprint("Stars : 1 st=%d m=%g : 2 st=%d m=%g : binary? %d\n",
           stardata->star[0].stellar_type,
           stardata->star[0].mass,
           stardata->star[1].stellar_type,
           stardata->star[1].mass,
           System_is_binary
        );

    
    Dprint("Check for collision at periastron t=%g : sep=%12.12g, ecc=%12.12g, periastron distance = %12.12g; total radius = %12.12g; intpol=%d; Collision ? %d\n",
           stardata->model.time,   
           stardata->common.orbit.separation,
           stardata->common.orbit.eccentricity,
           PERIASTRON_DISTANCE,
           TOTAL_RADIUS,stardata->model.intpol,
           new_collision);

#ifdef FILE_LOG
    if(new_collision == TRUE)
    {
        Append_logstring(LOG_PERIASTRON_COLLISION,
                         "R1+R2=%g+%g=%g > aperi=%g*(1-%g)=%g (binary? %d)",
                         stardata->star[0].radius,
                         stardata->star[1].radius,
                         TOTAL_RADIUS,
                         stardata->common.orbit.separation,
                         stardata->common.orbit.eccentricity,
                         PERIASTRON_DISTANCE,
                         System_is_binary
            );
    }
#endif //FILE_LOG

    return new_collision;
}
