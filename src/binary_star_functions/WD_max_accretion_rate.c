#include "../binary_c.h"
No_empty_translation_unit_warning;

#include "donor_types.h"


/*
 * Maximum stable accretion rate on a white dwarf
 *
 * Returns the rate in Msun/year
 *
 * Note: these are rates limited by nuclear burning only.
 * Other limits (e.g. thermal, Eddington, etc.) are imposed elsehwere.
 */
double WD_max_accretion_rate(struct stardata_t * Restrict const stardata,
                             struct star_t * const donor,
                             struct star_t * const accretor)
{
    /*
     * Choose rate based on donor stellar type
     */
    double rate =
        HYDROGEN_DONOR ? stardata->preferences->WD_accretion_rate_new_giant_envelope_lower_limit_hydrogen_donor :
        HELIUM_DONOR ? stardata->preferences->WD_accretion_rate_new_giant_envelope_lower_limit_helium_donor :
        stardata->preferences->WD_accretion_rate_new_giant_envelope_lower_limit_other_donor;

    if(rate < -TINY)
    {
        /*
         * Use rate algorithm
         */
        const int algorithm = Map_float_algorithm(rate);
#undef X
#define X(X,Y,STRING) STRING,
        static const char * strings[] = { RLOF_DONOR_RATE_ALGORITHMS_LIST };
#undef X
        Dprint("mapped rate %g to algorithm %d \"%s\"\n",
               rate,
               algorithm,
               strings[-algorithm]);

        switch(algorithm)
        {

        case DONOR_RATE_ALGORITHM_BSE:
            /*
             * BSE (Hurley et al. 2002) algorithm
             */
            if(HYDROGEN_DONOR)
            {
                rate = 2.71e-7;
            }
            else
            {
                /*
                 * Rate may be limited elsewhere
                 */
                rate = VERY_LARGE_MASS_TRANSFER_RATE;
            }
            break;

        case DONOR_RATE_ALGORITHM_CLAEYS2014:
            /*
             * Claeys et al. (2014) algorithm
             */
            rate = Hachisu_max_rate(stardata,
                                    donor,
                                    accretor);
            break;

#ifdef KEMP_NOVAE

        case DONOR_RATE_ALGORITHM_K2014_P2014:
            /*
             * Kato2014 H, Piersanti 2014 He.
             */
        {
            const double M_WD = accretor->mass;
            if(HYDROGEN_DONOR)
            {
                rate = pow(10,1.2909 * pow(M_WD,3) -4.8608 * pow(M_WD,2) + 6.5776 * M_WD - 9.4322);
            }
            else if(HELIUM_DONOR)
            {
                rate = pow(10,-6.840+1.349*M_WD);
            }
            else
            {
                /*
                 * burning other stuff is hard, probably not nuclear-limited.
                 */
                rate = VERY_LARGE_MASS_TRANSFER_RATE;
            }
        }
        break;

        case DONOR_RATE_ALGORITHM_WANGWU:
            /*
             * WANG2018H, WU2017He.
             */
        {
            const double M_WD = accretor->mass;
            if(HYDROGEN_DONOR && M_WD>=0.5)
            {
                /* interpolate */
                rate = 0.27*1e-7 * (Pow2(M_WD) + 25.52*M_WD - 9.02);
            }
            else if(HYDROGEN_DONOR && M_WD<0.5)
            {
                /* extrapolate */
                rate = exp10(2.712119930154697 * M_WD -8.323723305231614);
            }
            else if(HELIUM_DONOR && M_WD>=0.6)
            {
                /* interpolate */
                rate = 2.17*1e-6 * (Pow2(M_WD) + 0.82*M_WD - 0.38);
            }
            else if(HELIUM_DONOR && M_WD<0.6)
            {
                /* extrapolate */
                rate = exp10(1.8003008938336499 * M_WD - 7.069778803817572);
            }
            else
            {
                /*
                 * burning other stuff is hard, probably not nuclear-limited.
                 */
                rate = VERY_LARGE_MASS_TRANSFER_RATE;
            }
        }
        break;

#endif // KEMP_NOVAE

        default:
            rate = 0.0; // avoid compiler warnings
            Exit_binary_c(BINARY_C_SETUP_UNKNOWN_ARGUMENT,
                          "Unknown accretion rate new giant envelope limit method %g\n",
                          stardata->preferences->WD_accretion_rate_new_giant_envelope_lower_limit_hydrogen_donor);


        }
    }

    return Max(VERY_SMALL_MASS_TRANSFER_RATE,rate);
}
