#pragma once
#ifndef NS_MERGERS_H
#define NS_MERGERS_H

#ifdef NUCSYN
/*
 * NS yield merger algorithms
 */
#define NS_MERGER_YIELD_ALGORITHMS_LIST         \
    X( RADICE2018)

#undef X
#define X(TYPE) NS_MERGER_YIELD_ALGORITHM_##TYPE,

enum {
    NS_MERGER_YIELD_ALGORITHMS_LIST
};
#endif // NUCSYN

/*
 * Radice 2018 equations of state
 */
#define RADICE2018_EOS_LIST  \
    X( BHBlp)                \
    X(   DD2)                \
    X( LS220)                \
    X(  SHFo)
#undef X
#define X(TYPE) RADICE2018_EOS_##TYPE,
enum {
    RADICE2018_EOS_LIST
};


#endif // NS_MERGERS_H
