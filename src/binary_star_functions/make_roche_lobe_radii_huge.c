#include "../binary_c.h"
No_empty_translation_unit_warning;


void make_roche_lobe_radii_huge(struct stardata_t * Restrict const stardata)
{
    Foreach_evolving_star(star)
    {
        star->roche_radius = HUGE_ROCHE_MULTIPLIER*star->radius;
    }
}
