#include "../binary_c.h"
No_empty_translation_unit_warning;


/*
 * Zwick et al. 2020 (arXiv 1911.06024)
 * gravitational wave merger timescale,
 * based on Peters with quadrupole
 * post-Newtonian corrections.
 *
 * Returns the result in years or a very long
 * time if m1+m2 = 0 or a = 0
 *
 * Pass in m1,m2 in Msun (same as code units)
 */

double Zwick_grav_wave_merger_time(const struct stardata_t * Restrict const stardata,
                                   const double m1,
                                   const double m2,
                                   const double a,
                                   const double ecc)
{
    /*
     * Eq. 41
     */
    const double M = m1 + m2;
    const double rs = 2.0 * GRAVITATIONAL_CONSTANT * M * M_SUN / Pow2(SPEED_OF_LIGHT); // system Schwazschild radius in cm
    double timescale =
        Peters_grav_wave_merger_time(stardata,m1,m2,a,ecc)
        *
        pow(8.0, 1.0-sqrt(1.0-ecc))
        *
        exp(2.5 * rs / (a * (1.0 - ecc)));
    return timescale;
}
