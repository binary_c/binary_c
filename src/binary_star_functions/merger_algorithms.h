#pragma once
#ifndef MERGER_ALGORITHMS_H
#define MERGER_ALGORITHMS_H

#include "../binary_c_maths.h"
#include "merger_algorithms.def"

#undef X
#define X(CODE,NUM) MERGER_MASS_LOSS_FRACTION_##CODE = NUM,
enum { MERGER_MASS_LOSS_FRACTION_LIST };
#undef X


#define X(CODE) MSMS_MERGER_AGE_ALGORITHM_##CODE,
enum {  MSMS_MERGER_AGE_ALGORITHM_LIST };
#undef X

#define X(CODE) WDWD_MERGER_ALGORITHM_##CODE,
enum {  WDWD_MERGER_ALGORITHM_LIST };
#undef X

#define X(CODE) TZ_CHANNEL_##CODE,
enum {  TZ_CHANNEL_LIST };
#undef X

/* R star merger types */
#include "rstar_merger_types.def"
#undef X
#define X(CODE) RSTAR_##CODE,
enum { RSTAR_MERGER_TYPES_LIST };
#undef X

#include "NS_mergers.h"

#endif // MERGER_ALGORITHMS_H
