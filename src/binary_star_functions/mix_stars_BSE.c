#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BSE
//#define MERGED_STELLAR_TYPE_WARNING
#undef DEBUG
#define DEBUG 1

/*
 * the source of the envelope, and hence surface abundances, depends
 * on the stellar types (k1,k2) of the stars.
 */
enum {
    ENVELOPE_SOURCE_STAR1,
    ENVELOPE_SOURCE_STAR2,
    ENVELOPE_SOURCE_BOTH,
    ENVELOPE_SOURCE_TZ
};

void mix_stars_BSE(struct stardata_t * const Restrict stardata,
                   const Boolean eject)
{
    /**************************************
     *
     *     Author : J. R. Hurley
     *     Date :   7th July 1998
     *
     *     Evolution parameters for mixed star.
     *     ------------------------------------
     *
     * C version, 27/01/2001 (and onwards), Rob Izzard
     *
     * Nucleosynthesis added, 2002/3 Rob Izzard
     */
    double m01,m02,m03,m1,m2,m3,age1,age2,age3;
    double Maybe_unused mc1,mc2,mc3=0.0;
    double tms1,tms2,tms3,mcconv1,mcconv2,mcconv3=0.0;
    double mcCO1,mcCO2,mcCO3=0.0,mcHe3=0.0;
#ifdef NUCSYN
    Abundance *X1,*X2;
#endif //NUCSYN
    Boolean exploded = FALSE;
    Boolean remnant = FALSE;

    /*
     * New stars based on the old stars
     */
    struct star_t ** stars = Calloc(NUMBER_OF_STARS,
                                    sizeof(struct star_t *));
    stars[0] = new_star(stardata,&stardata->star[0]);
    stars[1] = new_star(stardata,&stardata->star[1]);

    /*
     * env_src tells us from where any ejected material comes
     *
     * 1 : star 1
     * 2 : star 2
     * 3 : both stars
     * 4 : Thorne-Zytkow object (treated differently)
     */
    int env_src;

    struct BSE_data_t * bse = new_BSE_data();

    Dprint("in mix k1=%d k2=%d\n",stars[0]->stellar_type,stars[1]->stellar_type);


    /*
     * Choose star numbers such that
     * k1 = stellar_type(i1) >= k2 = stellar_type(i2)
     */
    const Star_number i1 = stars[0]->stellar_type>=stars[1]->stellar_type ? 0 : 1;
    const Star_number i2 = Other_star(i1);

    /*
     * Specify case index for collision treatment, with k1>k2.
     */
    const Stellar_type k1 = stars[i1]->stellar_type;
    const Stellar_type k2 = stars[i2]->stellar_type;

    /*
     * Stellar type of the merged star
     */
    Stellar_type merged_stellar_type =
        merger_stellar_type(stardata,
                            stars[i1],
                            stars[i2]);
    Dprint("merged stellar type %d\n",merged_stellar_type);

    /*
     * Check for TZO
     */
    const Boolean TZO =
        POST_SN_OBJECT(merged_stellar_type) &&
        !POST_SN_OBJECT(k2);

    if(TZO)
    {
        merged_stellar_type = EAGB;
        stars[i1]->TZO = TZO_MERGER;
        stars[i2]->TZO = TZO_MERGER;
    }

#ifdef NS_BH_AIC_LOG
    /*
     * Do we count NSNS mergers in a BH as a NSNS event?
     * Say YES perhaps we should...
     */
    if((stars[i1]->stellar_type==NEUTRON_STAR)&&
       (stars[i2]->stellar_type==NEUTRON_STAR))
    {
        printf("NSNS p=%g t=%g in mix\n",stardata->model.probability,
               stardata->model.time);
        char c[STRING_LENGTH];
        sprintf(c,"NSNS at time t=%g\n",stardata->model.time);
        output_string_to_log(stardata,c);
    }
#endif //NS_BH_AIC_LOG

    // nucleosynthesis depends on k1 and k2
#ifdef NUCSYN
    Dprint("Star %d : m=%g, mc=%g, kw=%d\n",1,stars[0]->mass,stars[0]->core_mass[CORE_He],stars[0]->stellar_type);
    Dprint("Star %d : m=%g, mc=%g, kw=%d\n",2,stars[1]->mass,stars[1]->core_mass[CORE_He],stars[1]->stellar_type);

#ifdef NUCSYN_STRIP_AND_MIX
    mix_down_to_core(&(stardata->star[0]));
    mix_down_to_core(&(stardata->star[1]));
#endif // NUCSYN_STRIP_AND_MIX
#endif // NUCSYN

    /*
     * Decide where the new envelope comes from
     */
    if((k1<=TPAGB &&
        k2<=TPAGB)
       ||
       (NAKED_HELIUM_STAR(k1) &&
        NAKED_HELIUM_STAR(k2))
       ||
       (WHITE_DWARF(k1) &&
        WHITE_DWARF(k2))
        )
    {
        env_src = ENVELOPE_SOURCE_BOTH;
    }
    else if(k1>=NEUTRON_STAR && k2<NEUTRON_STAR)
    {
        /*
         * Note: this assumes the Eddington limit applies
         * ... if it doesn't, you may get some other kind of object
         */
        env_src = ENVELOPE_SOURCE_TZ;
    }
    else if(k1 > k2)
    {
        env_src = ENVELOPE_SOURCE_STAR2;
    }
    else
    {
        env_src = ENVELOPE_SOURCE_STAR1;
    }

    Dprint("env_src=%d : %s\n",
           env_src,
           (env_src == ENVELOPE_SOURCE_BOTH ? "both stars" :
            env_src == ENVELOPE_SOURCE_STAR1 ? "star 1" :
            env_src == ENVELOPE_SOURCE_STAR2 ? "star 2" :
            "Thorne-Zytkow object"));

#ifdef MERGED_STELLAR_TYPE_WARNING
    if(merged_stellar_type>100)
    {
        fprintf(stderr,
                "warning : mix merged_stellar_type>100 (merged_stellar_type=%d, k1=%d, k2=%d)\n",
                merged_stellar_type,
                k1,
                k2);
    }
#endif // MERGED_STELLAR_TYPE_WARNING

    /*
     * Determine evolution time scales for first star.
     */
    Dprint("Determine evolution time scales for first star.");
    m01 = stars[i1]->mass;
    m1 = stars[i1]->mass;
    mc1 = Outermost_core_mass(stars[i1]);
    mcconv1 = stars[i1]->max_MS_core_mass;
    mcCO1 = stars[i1]->core_mass[CORE_CO];
    age1 = stars[i1]->age;
    stellar_timescales(stardata,
                       stars[0],
                       bse,
                       m01,
                       m1,
                       k1);
    tms1 = Max(1.0,bse->tm);

    /*
     * Obtain time scales for second star.
     */
    Dprint("Determine evolution time scales for second star.");
    m02 = stars[i2]->mass;
    m2 = stars[i2]->mass;
    mc2 = Outermost_core_mass(stars[i2]);
    mcconv2 = stars[i2]->max_MS_core_mass;
    mcCO2 = stars[i2]->core_mass[CORE_CO];
    age2 = stars[i2]->age;
    stellar_timescales(stardata,
                       stars[1],
                       bse,
                       m02,
                       m2,
                       k2);
    tms2 = Max(1.0,bse->tm);
    Dprint("MIX IN\n");
    Dprint("m1=%g st1=%d mc1=%g McCO1=%g\n",m1,stardata->star[i1].stellar_type,mc1,mcCO1);
    Dprint("m2=%g st2=%d mc2=%g McCO2=%g\n",m2,stardata->star[i2].stellar_type,mc2,mcCO1);

#ifdef NUCSYN
    /*
     * Assume the merging event is enough to mix the envelope and
     * accretion layer of each star (if an accretion layer exists)
     */
    nucsyn_mix_accretion_layer_and_envelope(stardata,
                                            stars[i1],
                                            m1 - mc1);
    nucsyn_mix_accretion_layer_and_envelope(stardata,
                                            stars[i2],
                                            m2 - mc2);

    /*
     * new envelope abundances
     */
    X1 = stars[i1]->Xenv;
    X2 = stars[i2]->Xenv;
#endif // NUCSYN

    /*
     * Mix the two stars' envelopes according to env_src
     * Envelope masses are menv1 and menv2
     */
    if(env_src==ENVELOPE_SOURCE_STAR1)
    {
        /*
         * All the envelope comes from star i1, star i2 sinks
         * without mixing into the envelope
         * the envelope on i1 is already on i1 so we don't have to do
         * anything
         */
    }
    else if(env_src==ENVELOPE_SOURCE_STAR2)
    {
#ifdef NUCSYN
        /*
         * All the envelope comes from star i2
         */
        Copy_abundances(X2,stars[i1]->Xenv);
#endif // NUCSYN
    }
    else if(env_src==ENVELOPE_SOURCE_BOTH)
    {
        /*
         * All the envelope comes from either
         * 1) the completely mixed envelopes or
         * 2) in the case where a star is a WD, the mixture of
         * the WD and the envelope of the other star or
         * 3) complete mixing in the case where neither star
         * has a core (in this case the envelope is
         */
#ifdef NUCSYN
        double menv1,menv2;
        if(WHITE_DWARF(k2))
        {
            /*
             * for WD mixing into the envelope treat all WD as envelope
             */
            menv2 = m2;
        }
        else
        {
            /*
             * otherwise the envelope mass is the difference between
             * the total mass and the core mass. In the case of MS stars
             * this is the total mass because the core mass is zero.
             */
            menv2 = m2 - Outermost_core_mass(stars[i2]);
        }

        if(WHITE_DWARF(k1))
        {
            menv1 = m1;
        }
        else
        {
            menv1 = m1 - Outermost_core_mass(stars[i1]);
        }

        nucsyn_dilute_shell(menv1,
                            X1,
                            menv2,
                            X2);
#endif//NUCSYN
    }
    else if(env_src==ENVELOPE_SOURCE_TZ)
    {
        /* Thorne-Zytkow object */
        if(k2 < NEUTRON_STAR)
        {
            /*
             * Star i2 is totally lost to the ISM
             */
            Dprint("calc yields T-Z object mix i2=%d\n",i2);
            struct star_t * tzo = &stardata->star[i2];
#ifdef NUCSYN
            nucsyn_TZ_surface_abundances(tzo);
#endif // NUCSYN
            calc_yields(stardata,
                        tzo,
                        m2,
#ifdef NUCSYN
                        tzo->Xenv,
#endif // NUCSYN
                        0.0,
#ifdef NUCSYN
                        NULL,
#endif // NUCSYN
                        i2,
                        YIELD_NOT_FINAL,
                        SOURCE_THORNE_ZYTKOW);
        }
    }
    else
    {
        /*
         * all other cases, completely mix the stars
         */
#ifdef NUCSYN
        nucsyn_dilute_shell(m1,
                            X1,
                            m2,
                            X2);
#endif // NUCSYN
    }

    /*
     * Check for planetary systems - defined as HeWDs and low-mass WDs!
     */
    Dprint("Planet check: merged_stellar_type=%d\n",merged_stellar_type);
    if(k1==HeWD && m1<0.05)
    {
        merged_stellar_type = k2;
        if(k2<=MAIN_SEQUENCE)
        {
            merged_stellar_type = MAIN_SEQUENCE;
            age1 = 0.0;
        }
    }
    else if(k1>=COWD && merged_stellar_type==TPAGB && m1<0.5)
    {
        merged_stellar_type = HeGB;
    }
    if(k2==HeWD && m2<0.05)
    {
        merged_stellar_type = k1;
        if(k1 <= MAIN_SEQUENCE)
        {
            merged_stellar_type = MAIN_SEQUENCE;
            age2 = 0.0;
        }
    }

    if(merged_stellar_type >= 100)
    {
        Dprint("Forced merger instead of comenv (ok if envelope mass is small)\n");
        merged_stellar_type -= 100;
    }

    /*
     * Remember masses in the completely hydrogen and helium
     * burnt cores
     */
    mcHe3 = Hydrogen_burnt_core_mass(stars[i1]) +
        Hydrogen_burnt_core_mass(stars[i2]);

    Dprint("Masses %g %g\n",
           stars[i1]->mass,
           stars[i2]->mass);
    Dprint("ST %d %d\n",
           stars[i1]->stellar_type,
           stars[i2]->stellar_type);

    Dprint("Merged He core : %g + %g = %g\n",
           Hydrogen_burnt_core_mass(stars[i1]),
           Hydrogen_burnt_core_mass(stars[i2]),
           mcHe3);

    mcCO3 = Helium_burnt_core_mass(stars[i1]) +
        Helium_burnt_core_mass(stars[i2]);
    Dprint("Merged CO core : %g + %g = %g\n",
           Helium_burnt_core_mass(stars[i1]),
           Helium_burnt_core_mass(stars[i2]),
           mcCO3);

    /*
     * Specify total mass of the merged object.
     *
     * We lose a fraction floss.
     *
     * This can be set either:
     *
     * * For all stellar types by setting only merger_mass_loss_fraction
     *
     * * For each stellar type by setting merger_mass_loss_fraction_by_stellar_type[merged_stellar_type]
     *   with merger_mass_loss_fraction as a fallback.
     */

    /*
     * Assume the two stars just merge and lose
     * the appropriate merger_mass_loss_fraction.
     */
    const double floss = eject == TRUE ?
        merger_mass_loss_fraction(stardata,
                                  merged_stellar_type) :
        0.0;
    const double fkept = 1.0 - floss;
    m3 = (m1 + m2) * fkept;
    m03 = (m01 + m02) * fkept;


    m03 = Min(m3,m03);
    age3 = 0.0;

    if(Is_not_zero(stardata->preferences->HeWD_COWD_explode_above_mass) &&
       (m3 > stardata->preferences->HeWD_COWD_explode_above_mass) &&
       ((k1==HeWD && k2==COWD) ||
        (k1==COWD && k2==HeWD)))
    {
        /*
         * We want to explode this star: do not just
         * re-ignite helium
         */
        merged_stellar_type = COWD;
    }

    Dprint("merged_stellar_type = %d with total mass %g\n",
           merged_stellar_type,
           m3);

    /*
     * Evaluate apparent age and other parameters
     * depending on stellar type.
     * Also log and set up supernovae.
     */
    if(merged_stellar_type <= MAIN_SEQUENCE)
    {
        /*
         * Specify new age based on complete mixing.
         */
        if(k1==HeMS)
        {
            merged_stellar_type = HeMS;
        }
        mc3 = 0.0;
        mcHe3 = 0.0;
        mcCO3 = 0.0;
        mcconv3 = 0.0; //todo
        Dprint("Merged MS star timescales");
        stellar_timescales(stardata,
                           stars[0],
                           bse,
                           m03,
                           m3,
                           merged_stellar_type);
        tms3 = bse->tm;
        Dprint("tms3 %g\n",bse->tm);

        if(stardata->preferences->MSMS_merger_age_algorithm == MSMS_MERGER_AGE_ALGORITHM_SELMA)
        {
            //double age3_settling_of_cores, age3_completemixing, age3_oldimplementation;
            double fracMSage_moreEvolvedStar, fracMSage_lessEvolvedStar;
            double mass_moreEvolvedStar, mass_lessEvolvedStar;
            double mcore_moreEvolvedStar, mcore_lessEvolvedStar, mcore_merger;

            /*
             * Check which star is the most evolved star (can be the
             * secondary, for example systems that experienced slow case A
             * mass transfer )
             */
            if(age1/tms1 > age2/tms2)
            {
                fracMSage_moreEvolvedStar = age1/tms1;
                fracMSage_lessEvolvedStar = age2/tms2;
                mass_moreEvolvedStar = m1;
                mass_lessEvolvedStar = m2;
            }
            else
            {
                fracMSage_moreEvolvedStar = age2/tms2;
                fracMSage_lessEvolvedStar = age1/tms1;
                mass_moreEvolvedStar = m2;
                mass_lessEvolvedStar = m1;
            }

            /*
             * Get the size of the convective core at this moment.
             *
             * As a proxy I currently use the size that the core will have
             * at the end of the mains sequence / beginning of the
             * Hertzsprung gap.  This ignores the effect that the
             * convective core is typically larger at zero age and slowly
             * retreats.
             */
            mcore_moreEvolvedStar =
                effective_core_mass_fraction_of_MS_stars(mass_moreEvolvedStar,
                                                         stardata->common.metallicity);

            mcore_lessEvolvedStar =
                effective_core_mass_fraction_of_MS_stars(mass_lessEvolvedStar,
                                                         stardata->common.metallicity);

            mcore_merger = effective_core_mass_fraction_of_MS_stars(m3,
                                                                    stardata->common.metallicity);


            /*
             * Assume that the core of the most evolved star settles in
             * the center of the merger and that the other core will form
             * the layers on top of that.
             *
             * Then assume that the core of the merger produces will be
             * convective and mixes. Also assume that the relative age is
             * an indicator of the helium abundance in the core,
             * i.e. age/tms ~ Xhe_center and Xhe_center = helium present
             * in layer of the original star that will become part of the
             * new core / mcore_3
             *
             * Caveats: (I) in reality there may be more helium present in
             * the progenitor stars because we underestimate the current
             * core size, and their may be helium present in layers above
             * the core (II) This may in particular the case for the
             * primary star in slow case A mergers.
             *
             */

            /*
             * This parameter regulates an option for extra mixing of
             * the merger product. The new (convective) core is always
             * assumed to be mixed. If [mixing_in_MS_mergers > 0] an
             * extra fraction of the H-rich of the envelope of the
             * merger is mixed with the core. [mixing_in_MS_mergers = 1]
             * implies that the entire envelope (after mass loss during
             * the merger itself is mixed).
             */
            double mass_mixed_region = mcore_merger;
            if(stardata->preferences->mixingpar_MS_mergers > 0.0)
            {
                mass_mixed_region += (1.0-mcore_merger)*stardata->preferences->mixingpar_MS_mergers;
            }

            Dprint("mass mixed region %g\n",mass_mixed_region);
            if(mass_mixed_region > mcore_moreEvolvedStar + mcore_lessEvolvedStar)
            {
                /* new core contains both progenitor cores and some fresh material */
                age3 = tms3 * (  mcore_moreEvolvedStar * fracMSage_moreEvolvedStar +
                                 mcore_lessEvolvedStar * fracMSage_lessEvolvedStar ) / mass_mixed_region ;
                Dprint("age3 %g\n",age3);
            }
            /*
             * If a high fraction of mass is lost during the merger, the
             * new mixed region might be smaller than the original
             * cores. Assume that the core of the most evolved star sinks
             * to the center. (This might not be true for very extreme
             * mass ratios, where the entropy of the core of the companion
             * is lower than the evolved primary.) Evert Glebbeek find
             * that this may happen for mass ratioas < 0.1 (in models
             * without over shooting). I find that the mass ratios should
             * be more extreme, based on models with overshooting.  In
             * other words, these are binaries that I do not even consider
             * in the population synthesis.
             */
            else if(mass_mixed_region > mcore_moreEvolvedStar)
            {
                /*
                 * new core contains core of more evolved star
                 * plus part of the core of the less evolved star
                 */
                age3 = tms3 * ( mcore_moreEvolvedStar * fracMSage_moreEvolvedStar +
                                (mass_mixed_region-mcore_moreEvolvedStar)*fracMSage_lessEvolvedStar)
                    /mass_mixed_region;
                Dprint("age3 %g from tms3 = %g, mcore_moreEvolvedStar = %g, fracMSage_moreEvolvedStar = %g, mass_mixed_region %g, mcore_moreEvolvedStar = %g fracMSage_lessEvolvedStar = %g \n",age3,
                       tms3,
                       mcore_moreEvolvedStar,
                       fracMSage_moreEvolvedStar,
                       mass_mixed_region,
                       mcore_moreEvolvedStar,
                       fracMSage_lessEvolvedStar);
            }
            else
            {
                /*
                 * new core only contains material from the
                 * core of the more evolved star (i.e. have the same relative age)
                 */
                Dprint("age3 = %g from %g, %g\n",
                       age3,
                       tms3,
                       fracMSage_moreEvolvedStar);
                age3 = tms3 * fracMSage_moreEvolvedStar;
            }
        }
        else if(stardata->preferences->MSMS_merger_age_algorithm == MSMS_MERGER_AGE_ALGORITHM_TOUT)
        {
            /*
             * Old implementation (not recommended).  This assumes that
             * - core helium fraction changes linearly with time
             * - Both stars have core mass fractions of 0.1
             *   (severely underestimating it for high mass stars)
             * - The helium present in the cores of the original stars
             *   is mixed throughout the merger
             *
             * This assuption leads to highly  rejuvenated stars, in most cases setting the
             * relative age of the merger to just a few percent.
             * SdM
             */
            age3 = 0.10*tms3*(age1*m1/tms1 + age2*m2/tms2)/m3;
        }
        Dprint("age3 = %g\n",age3);
    }
    else if(GIANT_LIKE_STAR(merged_stellar_type))
    {
        Dprint("Giant like star\n");

        /*
         * If we're a star with a core that has some hydrogen
         * in it
         */

        if(merged_stellar_type <= CHeB)
        {
            mc3 = mcHe3;
            mcCO3 = 0.0;
        }
        /*
         * ... or some helium in it
         */
        else if(merged_stellar_type == EAGB ||
                merged_stellar_type == TPAGB)
        {
            mc3 = mcHe3;
        }
        else if(NAKED_HELIUM_STAR(merged_stellar_type))
        {
            if(merged_stellar_type == HeMS)
            {
                mc3 = 0.0;
            }
            else
            {
                mc3 = mcCO3;
            }
        }
        else
        {
            mc3 = mcHe3;
        }

        mc3 = Min(m3,mc3);
        mcHe3 = Min(m3,mcHe3);
        mcCO3 = Min(m3,mcCO3);
        mcconv3 = mcconv1 + mcconv2;

        if(merged_stellar_type == CHeB)
        {
            /* why set age3 only for CHeB stars? */
            age3 = age1;
        }
        giant_age(&mc3,
                  m3,
                  &merged_stellar_type,
                  &m03,
                  &age3,
                  stardata,
                  &(stardata->star[i1]));
        Dprint("post-gntage : stellar type %d, age3 %g\n",
               merged_stellar_type,
               age3);
    }
    else if(merged_stellar_type==HeMS)
    {
        mc3 = 0.0;
        mcCO3 = 0.0;
        mcconv3 = 0.0; // todo
        Dprint("Merged HeMS star timescales");
        stellar_timescales(stardata,
                           stars[0],
                           bse,
                           m03,
                           m3,
                           merged_stellar_type);
        tms3 = bse->tm;
        age3 = age2 * tms3/tms2 * m2/m3;
    }
    else if(WHITE_DWARF(merged_stellar_type))
    {
        /*
         * Ensure that a new WD has the initial mass set correctly.
         */
        if(stardata->preferences->WDWD_merger_algorithm == WDWD_MERGER_ALGORITHM_BSE
#ifdef KEMP_MERGERS
           || stardata->preferences->WDWD_merger_algorithm == WDWD_MERGER_ALGORITHM_PERETS2019
           || stardata->preferences->WDWD_merger_algorithm == WDWD_MERGER_ALGORITHM_HYBRID_PERETS2019_SATO2016
           || stardata->preferences->WDWD_merger_algorithm == WDWD_MERGER_ALGORITHM_SATO2016
           || stardata->preferences->WDWD_merger_algorithm == WDWD_MERGER_ALGORITHM_RUITER2013
           || stardata->preferences->WDWD_merger_algorithm == WDWD_MERGER_ALGORITHM_CHEN2016
#endif // KEMP_MERGERS
            )
        {
            /*
             * BSE algorithm of Hurley et al. (2002)
             */
            m03 = m3;
            mc3 = m3;
            mcCO3 = mcCO1 + mcCO2;
            mcconv3 = 0.0;

#ifdef COWD_QUIET_MERGER
            if(k1==COWD && k2==COWD)
            {
                /*
                 * Assume that the accreting COWD ignites carbon
                 * and is blown off in the 'explosion'
                 */
                const Boolean kmax = m1>m2 ? FALSE : TRUE;
                m3 = kmax==FALSE ? m1 : m2;
                m03 = kmax==FALSE ? m01 : m02;
            }
#endif // COWD_QUIET_MERGER

            if(Is_not_zero(stardata->preferences->COWD_COWD_explode_above_mass) &&
               (k1==COWD && k2==COWD))
            {
                /*
                 * COWD-COWD merged stars should
                 * explode as SNeIa if the merged mass exceeds
                 * the given limit
                 */
                if(m3 > stardata->preferences->COWD_COWD_explode_above_mass)
                {
                    if(m3 > chandrasekhar_mass_wrapper(stardata,
                                                       m3,
                                                       mc3,
                                                       merged_stellar_type))
                    {
                        stars[0]->SN_type = SN_IA_CHAND_Coal;
                    }
                    else
                    {
                        stars[0]->SN_type = SN_IA_SUBCHAND_CO_Coal;
                    }
                    exploded = TRUE;
                }
            }
            else if(Is_not_zero(stardata->preferences->HeWD_COWD_explode_above_mass) &&
                    ((k1==COWD && k2==HeWD) ||
                     (k1==HeWD && k2==COWD)))
            {
                /*
                 * HeWD-COWD merged stars should
                 * explode as SNeIa if the merged mass exceeds
                 * the given limit
                 */
                if(m3 > stardata->preferences->HeWD_COWD_explode_above_mass)
                {
                    if(m3 > chandrasekhar_mass_wrapper(stardata,
                                                       m3,
                                                       mc3,
                                                       merged_stellar_type))
                    {
                        stars[0]->SN_type = SN_IA_CHAND_Coal;
                    }
                    else
                    {
                        stars[0]->SN_type = SN_IA_SUBCHAND_He_Coal;
                    }
                    exploded = TRUE;
                }
            }
            else if(merged_stellar_type<ONeWD &&
                    More_or_equal(m3,
                                  chandrasekhar_mass_wrapper(stardata,
                                                             m3,
                                                             mc3,
                                                             merged_stellar_type)))
            {
                /*
                 * M > MCh SNIa
                 */
                stars[0]->SN_type = SN_IA_CHAND_Coal;
                exploded = TRUE;
            }

#ifdef KEMP_MERGERS
            else if(merged_stellar_type==ONeWD &&
                    More_or_equal(m3,stardata->preferences->chandrasekhar_mass))
            {
                /*
                 * Electron capture and form a neutron star
                 */
                if(stars[0]->stellar_type==COWD
                   &&
                   stars[1]->stellar_type==ONeWD)
                {
                    stars[0]->SN_type = SN_ECAP;
                    exploded = TRUE;
                    remnant = TRUE;
                }
                else
                {
                    stars[0]->SN_type = SN_ECAP;
                    exploded = TRUE;
                    remnant = TRUE;
                }
            }
#endif // KEMP_MERGERS
        }
        else if(stardata->preferences->WDWD_merger_algorithm == WDWD_MERGER_ALGORITHM_PERETS2019)
        {
            /*
             * Perets' algorithm (2019)
             */
            m03 = m3;
            mc3 = m3;
            mcCO3 = mcCO1 + mcCO2;
            mcconv3 = 0.0;

            if(
                /* both must be COWDs */
                stars[0]->stellar_type == COWD &&
                stars[1]->stellar_type == COWD &&
                /* one must be a hybrid */
                (stars[0]->hybrid_HeCOWD == TRUE ||
                 stars[1]->hybrid_HeCOWD == TRUE
#ifdef KEMP_MERGERS
                 ||
                 stars[0]->dm_novaHe>=MIN_MASS_He_FOR_HYBRID_COWD ||
                 stars[1]->dm_novaHe>=MIN_MASS_He_FOR_HYBRID_COWD
#endif // KEMP_MERGERS
                    ) &&
                /* and the merged mass must be 0.9<M<1.9 */
                m3 > 0.9 &&
                m3 < 1.9
                // total M > 1.2 -> normal Ia
                // < 1.2 subluminous Ia
                // need the other star to exceed the hybrid mass to make a disc
                )
            {
                Dprint("Yield SNIa m=%g\n",m1+m2);

                /*
                 * Hagai says:
                 * If M>1.2Msun we get a Ia.
                 * Around 1.1-1.2 it's in between.
                 * If M<1.1Msun we get a subluminous Ia.
                 *
                 * We may need the mass of the non-hybrid star to exceed that
                 * of the hybrid, but for now ignore this condition.
                 */
                if(m3 < 1.2)
                {
                    stars[0]->SN_type = SN_IA_Hybrid_HeCOWD_subluminous;
                }
                else
                {
                    stars[0]->SN_type = SN_IA_Hybrid_HeCOWD;
                }
                exploded = TRUE;
            }
        }
#ifdef KEMP_MERGERS
        else if(stardata->preferences->WDWD_merger_algorithm == WDWD_MERGER_ALGORITHM_SATO2016)
        {
            /*
             * Violent WD mergers
             */
            const double Mbig = Max(stars[0]->mass,stars[1]->mass);
            const double Msmall = Min(stars[0]->mass,stars[1]->mass);
            const double q_cr = 0.80*pow(Mbig,-0.84); /* Sato et al 2016, eq 4. */

            /*
             * This represents (roughly) the case where energy release from C fusion is taken into account.
             * The case where C fusion is NOT taken into account is 0.82 * Mbig**0.91 (eq 3).
             * both cases assume co-rotating WDs.
             */
            if(
                /* both must be COWDs */
                stars[0]->stellar_type == COWD &&
                stars[1]->stellar_type == COWD &&
                stars[0]->mass >= 0.75 &&
                stars[1]->mass >= 0.75 &&
                Msmall/Mbig >= q_cr
                )
            {
                stars[0]->SN_type = SN_IA_VIOLENT;
                exploded = TRUE;
            }
        }

        else if(stardata->preferences->WDWD_merger_algorithm == WDWD_MERGER_ALGORITHM_HYBRID_PERETS2019_SATO2016)
        {
            /*
             * SATO pre stuff:
             * These violent mergers have violent ends
             */
            const double Mbig = Max(stars[0]->mass,stars[1]->mass);
            const double Msmall = Min(stars[0]->mass,stars[1]->mass);
            const double q_cr = 0.80*pow(Mbig,-0.84); // sato et al 2016, eq 4.
            /* PERETS pre stuff */
            m03 = m3;
            mc3 = m3;
            mcCO3 = mcCO1 + mcCO2;
            mcconv3 = 0.0;

            /*
             * This represents (roughly) the case where energy release from C fusion is taken into account.
             * The case where C fusion is NOT taken into account is 0.82 * Mbig**0.91 (eq 3).
             *  both cases assume co-rotating WDs.
             */
            if(
                /* sato_not_perets */
                (
                    /* SATOcondition */
                    stars[0]->stellar_type == COWD &&
                    stars[1]->stellar_type == COWD &&
                    stars[0]->mass>=0.75 &&
                    stars[1]->mass>=0.75 &&
                    Msmall/Mbig >= q_cr)
                &&
                !(
                    //PERETScondition
                    /* both must be COWDs */
                    stars[0]->stellar_type == COWD &&
                    stars[1]->stellar_type == COWD &&
                    /* one must be a hybrid (or accreted a sufficiently large He shell)*/
                    (stars[0]->hybrid_HeCOWD == TRUE ||
                     stars[1]->hybrid_HeCOWD == TRUE ||
                     stars[0]->dm_novaHe>=MIN_MASS_He_FOR_HYBRID_COWD ||
                     stars[1]->dm_novaHe>=MIN_MASS_He_FOR_HYBRID_COWD) &&
                    /* and the merged mass must be 0.9<M<1.9 */
                    m3 > 0.9 &&
                    m3 < 1.9
                    // total M > 1.2 -> normal Ia
                    // < 1.2 subluminous Ia
                    // need the other star to exceed the hybrid mass to make a disc
                    //KEMP_NOVAE fixed apparent bug where the 0 in stars[0]->hybrid_HeCOWD==TRUE was a 1.
                    //KEMP_NOVAE included check to see if COWD had ACCRETED a He shell larger
                    // than MIN_MASS_He_FOR_HYBRID_COWD
                    // (set in binary_c parameters (master version set to 1e-3 as of 24 Apr))
                    )
                )
            {
                stars[0]->SN_type = SN_IA_VIOLENT;
                exploded = TRUE;
            }
            else if(
                //perets_not_sato
                !(
                    //SATOcondition
                    stars[0]->stellar_type == COWD &&
                    stars[1]->stellar_type == COWD &&
                    stars[0]->mass>=0.75 && stars[1]->mass>=0.75 &&
                    Msmall/Mbig >= q_cr)
                &&
                (
                    //PERETScondition
                    /* both must be COWDs */
                    stars[0]->stellar_type == COWD &&
                    stars[1]->stellar_type == COWD &&
                    /* one must be a hybrid (or accreted a sufficiently large He shell)*/
                    (stars[0]->hybrid_HeCOWD == TRUE ||
                     stars[1]->hybrid_HeCOWD == TRUE ||
                     stars[0]->dm_novaHe>=MIN_MASS_He_FOR_HYBRID_COWD ||
                     stars[1]->dm_novaHe>=MIN_MASS_He_FOR_HYBRID_COWD) &&
                    /* and the merged mass must be 0.9<M<1.9 */
                    m3 > 0.9 &&
                    m3 < 1.9
                    )
                )
            {
                /*
                 * Hagai says:
                 * If M>1.2Msun we get a Ia.
                 * Around 1.1-1.2 it's in between.
                 * If M<1.1Msun we get a subluminous Ia.
                 *
                 * We may need the mass of the non-hybrid star to exceed that
                 * of the hybrid, but for now ignore this condition.
                 */
                if(m3 < 1.2)
                {
                    stars[0]->SN_type = SN_IA_Hybrid_HeCOWD_subluminous;
                }
                else
                {
                    stars[0]->SN_type = SN_IA_Hybrid_HeCOWD;
                    //stardata->model.interesting_flag=11; // flag for HeCO hybrid merger 'normal' Type Ia
                }
                exploded = TRUE;
            }
            else if(
                //perets_and_sato
                (
                    //SATOcondition
                    stars[0]->stellar_type == COWD &&
                    stars[1]->stellar_type == COWD &&
                    stars[0]->mass>=0.75 && stars[1]->mass>=0.75 &&
                    Msmall/Mbig >= q_cr)
                &&
                (
                    //PERETScondition
                    /* both must be COWDs */
                    stars[0]->stellar_type == COWD &&
                    stars[1]->stellar_type == COWD &&
                    /* one must be a hybrid (or accreted a sufficiently large He shell)*/
                    (stars[0]->hybrid_HeCOWD == TRUE ||
                     stars[1]->hybrid_HeCOWD == TRUE ||
                     stars[0]->dm_novaHe>=MIN_MASS_He_FOR_HYBRID_COWD ||
                     stars[1]->dm_novaHe>=MIN_MASS_He_FOR_HYBRID_COWD) &&
                    /* and the merged mass must be 0.9<M<1.9 */
                    m3 > 0.9 &&
                    m3 < 1.9
                    )
                )
            {
                // if both conditions are met, treat as if perets
                // only had met but change the interesting flag to signal both were met
                /*
                 * Hagai says:
                 * If M>1.2Msun we get a Ia.
                 * Around 1.1-1.2 it's in between.
                 * If M<1.1Msun we get a subluminous Ia.
                 *
                 * We may need the mass of the non-hybrid star to exceed that
                 * of the hybrid, but for now ignore this condition.
                 */
                if(m3 < 1.2)
                {
                    /*
                     * HeCO hybrid merger subluminous Type Ia that also met
                     * conditions for Violent merger
                     */
                    stars[0]->SN_type = SN_IA_Hybrid_HeCOWD_subluminous;

                }
                else
                {
                    /*
                     * flag for HeCO hybrid merger 'normal' Type Ia
                     * that also met conditions for Violent merger
                     */
                    stars[0]->SN_type = SN_IA_Hybrid_HeCOWD;
                }
                exploded = TRUE;
            }
        }
        else if(stardata->preferences->WDWD_merger_algorithm == WDWD_MERGER_ALGORITHM_RUITER2013)
        {
            /*
             * These violent mergers have violent ends (RGI ???)
             */
            const double Mbig = Max(stars[0]->mass,
                                  stars[1]->mass);
            const double Msmall = Min(stars[0]->mass,
                                    stars[1]->mass);
            const double q_cr = Min(0.8*pow(Mbig/0.9,-stardata->preferences->eta_violent_WDWD_merger),1.0);

            /*
             * ETA_RUITER2013 default (as of 27 apr) == 0.75
             *
             * This represents (roughly) the case where energy release from C fusion is taken into account.
             * The case where C fusion is NOT taken into account is 0.82 * Mbig**0.91 (eq 3).
             * both cases assume co-rotating WDs.
             */
            if(
                /* both must be COWDs */
                stars[0]->stellar_type == COWD &&
                stars[1]->stellar_type == COWD &&
                Msmall/Mbig >= q_cr
                )
            {
                stars[0]->SN_type = SN_IA_VIOLENT;
                exploded = TRUE;
            }
        }
#endif // KEMP_MERGERS

        else if(stardata->preferences->WDWD_merger_algorithm == WDWD_MERGER_ALGORITHM_CHEN2016)
        {
            /*
             * Chen's algorithm (2016)
             */
            /* TODO */
            mcCO3 = mcCO1 + mcCO2; // prevent compiler warning
            mcconv3 = 0.0; // prevent compiler warning
            m03 = m3; // prevent compiler warning
            mc3 = m3; // prevent compiler warning

            Exit_binary_c(BINARY_C_ALGORITHM_OUT_OF_RANGE,
                          "WDWD merger algorithm %d of Chen 2016 not yet implemented\n",
                          stardata->preferences->WDWD_merger_algorithm);

        }
        else
        {
            mcCO3 = mcCO1 + mcCO2; // prevent compiler warning
            mcconv3 = 0.0; // prevent compiler warning
            m03 = m3; // prevent compiler warning
            mc3 = m3; // prevent compiler warning
            Exit_binary_c(BINARY_C_ALGORITHM_OUT_OF_RANGE,
                          "Unknown WDWD merger algorithm %d\n",
                          stardata->preferences->WDWD_merger_algorithm);
        }
    }
    else if(POST_SN_OBJECT(merged_stellar_type))
    {
        /*
         * Merger of two stars to give a NS or BH.
         *
         * We log NSNS/NSBH/BHBH as "supernovae" just in case
         * you ever want to kick them or model their "light" curves.
         *
         * We also trap Thorne-Zytkow object formation.
         */

        /*
         * First, if the new neutron star exceeds the
         * maximum mass limit, convert to a black hole.
         */
        if(merged_stellar_type==NEUTRON_STAR &&
           m3 > stardata->preferences->max_neutron_star_mass)
        {
            merged_stellar_type = BLACK_HOLE;
        }

        Dprint("Final NS/BH object of stellar type %d merged with k2=%d",
               merged_stellar_type,k2);

#ifdef NUCSYN
        nucsyn_set_remnant_abunds(&(stars[0]->Xenv[0]));
#endif // NUCSYN
        mc3 = m3;
        mcCO3 = m3;
        mcconv3 = 0.0;
    }
    else if(merged_stellar_type == MASSLESS_REMNANT)
    {
        mc3 = 0.0;
        mcCO3 = 0.0;
        mcconv3 = 0.0;

        Dprint("Merged and exploded to a massless remnant");

        stars[0]->SN_type = SN_IA_He;
        exploded = TRUE;
#ifdef NUCSYN
        /*
         * This can, according to p103 of Jarrod's thesis,
         * only be a HeWD-HeWD merger. Do we really believe
         * these explode?
         */
        nucsyn_set_remnant_abunds(&stars[0]->Xenv[0]);
#endif //NUCSYN
    }
    else if(merged_stellar_type>100)
    {
        /*
         * Common envelope case which should only be used after
         * common_envelope_evolution
         */
        merged_stellar_type = k1;
        age3 = age1;
        Dprint("age3 %g\n",age3);
        m3 = m1;
        mc3 = m1;
        m03 = m01;
        mcCO3 = mcCO1 + mcCO2;
        mcconv3 = mcconv1 + mcconv2;
    }
    else
    {
        /*
         * This should not be reached.
         */
        mc3 = 0.0;
        mcCO3 = 0.0;
        mcconv3 = 0.0;
        Exit_binary_c(BINARY_C_MIX_MERGED_STELLAR_TYPE_NOT_CAUGHT,
                      "Error in mix.c: \"merged_stellar_type not caught\"! k1=%d, k2=%d -> k3=%d\n",
                      k1,k2,merged_stellar_type);
    }

    /*
     * Put the result in star 1. Make star 2 a massless remnant.
     */
#if defined NUCSYN && defined NUCSYN_STRIP_AND_MIX
    double mass_lost = m1 + m2 - m3;
    nucsyn_mix_stars(stardata,mass_lost,m3);
#endif // NUCSYN && NUCSYN_STRIP_AND_MIX
    Dprint("age3 %g\n",age3);

    /*
     * If there has been a SN, let the event
     * handler take care of updating the star.
     * Its mass and core mass are set in news above.
     */
    stars[0]->mass = m3;
    set_no_core(stars[0]);
    const Core_type core_id = ID_core(merged_stellar_type);
    if(core_id != CORE_NONE)
    {
        stars[0]->core_mass[core_id] = mc3;
    }
    if(Is_zero(stars[0]->core_mass[CORE_He]))
    {
        stars[0]->core_mass[CORE_He] = Min(m3,mcHe3);
    }
    if(Is_zero(stars[0]->core_mass[CORE_CO]))
    {
        stars[0]->core_mass[CORE_CO] = Min(m3,mcCO3);
    }
    stars[0]->max_MS_core_mass = Min(m3,mcconv3);

    if(stars[0]->SN_type != SN_NONE &&
       eject == TRUE)
    {
        stars[0]->mass = m3;
        stars[0]->phase_start_mass = m3;
    }
    else
    {
        stars[0]->stellar_type = merged_stellar_type;
        stars[0]->phase_start_mass = m03;
        stars[0]->age = age3;
    }
    stellar_structure_make_massless_remnant(stardata,
                                            stars[1]);

    if(exploded == TRUE)
    {
        /*
         * merged star has exploded
         *
         * It may have a remnant, which will be
         * a neutron star or black hole.
         *
         * Otherwise, it's a massless remnant.
         */
        struct star_t * const news =
                        new_supernova(stardata,
                                      stars[0],
                                      stars[1],
                                      NULL);
        if(news)
        {
            if(remnant)
            {
                /*
                 * Merger that leaves behind a
                 * NS or BH
                 */
                news->mass = ns_bh_mass(stars[0]->mass,
                                        stars[0]->core_mass[CORE_CO],
                                        stardata,
                                        news,
                                        &news->baryonic_mass);
                news->core_mass[CORE_NEUTRON] = news->mass;
                news->stellar_type = NEUTRON_STAR;
            }
            else
            {
                /*
                 * Merger and explosion that destroys both stars
                 */
                news->mass = stars[0]->mass;
                stars[1]->mass = 0.0;
                stellar_structure_make_massless_remnant(stardata,
                                                        news);
            }
        }
    }

    /*
     * Companion becomes massless and hence the
     * system becomes single.
     */
    stardata->model.sgl = TRUE;

    /*
     * Preserve the compact core type
     */
    if(stars[0]->stellar_type < Max(stars[0]->compact_core_type,
                                    stars[1]->compact_core_type))
    {
        if(WHITE_DWARF(stars[0]->stellar_type))
        {
            stars[0]->stellar_type = Max(stars[0]->compact_core_type,
                                         stars[1]->compact_core_type);
        }
    }

    /*
     * Turn off mass transfer now the stars have merged
     */
    Zero_stellar_derivatives(&stardata->star[0]);
    Zero_stellar_derivatives(&stardata->star[1]);

#ifdef NUCSYN
    stardata->star[i1].dmacc = 0.0;
    stardata->star[i2].dmacc = 0.0;

    /*
     * if the star is a (hydrogen) MS star then reset its zams mass
     * and treat the star as an evolved ms star
     */
    if(merged_stellar_type <= MAIN_SEQUENCE)
    {
        stars[i1]->effective_zams_mass = m3;
        Copy_abundances(stars[i1]->Xenv,
                        stars[i1]->Xinit);
    }
    Dprint("MIXOUT abunds star 1 H1=%g He4=%g\n",
           stars[0]->Xenv[XH1],stars[1]->Xenv[XHe4]);
    Dprint("Star %d : m=%g, mc=%g, kw=%d\n",
           1,
           stars[0]->mass,
           stars[0]->core_mass[CORE_He],
           stars[0]->stellar_type);
    Dprint("Star %d : m=%g, mc=%g, kw=%d\n",
           2,
           stars[1]->mass,
           stars[1]->core_mass[CORE_He],
           stars[1]->stellar_type);

#endif // NUCSYN

    /*
     * Reset AGB parameters after a merger
     */
    if(k1!=TPAGB && stars[i1]->stellar_type==TPAGB)
    {
        stars[i1]->num_thermal_pulses = -1;
    }
    if(k2!=TPAGB && stars[i2]->stellar_type==TPAGB)
    {
        stars[i2]->num_thermal_pulses = -1;
    }

    /*
     * Cannot be hybrid cores
     */
    if(stars[i1]->stellar_type<COWD)
    {
        stars[i1]->hybrid_HeCOWD = FALSE;
    }
    if(stars[i2]->stellar_type<COWD)
    {
        stars[i2]->hybrid_HeCOWD = FALSE;
    }


    /*
     * Copy stars back
     */
    copy_star(stardata,stars[0],&stardata->star[0]);
    copy_star(stardata,stars[1],&stardata->star[1]);
    Dprint("merged: star0= %p M=%g\n",
           (void*)&stardata->star[0],
           stardata->star[0].mass);
    /*
     * Free memory
     */
    free_BSE_data(&bse);
    free_star(stars + 0);
    free_star(stars + 1);
    Safe_free(stars);
}
#endif // BSE
