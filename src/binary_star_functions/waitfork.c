#include "../binary_c.h"
No_empty_translation_unit_warning;


/* Wait for the process pid to finish */

void waitfork(pid_t p)
{
    waitpid(p,NULL,0);
}
