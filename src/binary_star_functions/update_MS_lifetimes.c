#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef BSE
void update_MS_lifetimes(struct stardata_t * Restrict const stardata)
{
    /*
     * Update the main sequence lifetime of the stars
     */
    Star_number k;
    Starloop(k)
    {
        SETstar(k);
        struct BSE_data_t * bse = new_BSE_data();
        stellar_timescales(stardata,
                           star,
                           bse,
                           star->phase_start_mass,
                           star->mass,
                           star->stellar_type);
        star->tms = bse->tm;
        free_BSE_data(&bse);
    }
}
#endif//BSE
