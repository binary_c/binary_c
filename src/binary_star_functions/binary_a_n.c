#include "../binary_c.h"
No_empty_translation_unit_warning;


double Pure_function binary_a_n(struct stardata_t * const stardata,
                                struct star_t * const star)
{
    /*
     * a_n factor for the star
     * 
     * see e.g. Hurley+2002 Eq.19
     */
    struct star_t * companion = Other_star_struct(star);
    return companion->mass/(star->mass + companion->mass) * 
        stardata->common.orbit.separation;

}
