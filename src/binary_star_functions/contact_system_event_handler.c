#include "../binary_c.h"
No_empty_translation_unit_warning;


Event_handler_function contact_system_event_handler(void * const eventp Maybe_unused,
                                                    struct stardata_t * const stardata,
                                                    void * const data Maybe_unused)
{
    //struct binary_c_event_t * const event = eventp;
    stardata->model.coalesce = TRUE;
    const int why = *(int*)data;
    stardata->star[0].deny_SN++;
    stardata->star[1].deny_SN++;
    contact_system(stardata,TRUE,why);
    stardata->star[0].deny_SN--;
    stardata->star[1].deny_SN--;
    return NULL;
}
