#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef KEMP_NOVAE
Accretion_regime He_accretion_regime_WD(struct stardata_t * const stardata,
                                        struct star_t * const accretor)
{
    /*
     * Boundary curves from Piersanti et al. (2014).
     *
     * Returns RegimeType denoting accretion regime.
     * 1. Red Giant (RG) Regime "RG_He"
     * 2. Steady Accretion (SS) Regime "STEADYBURN_HE"
     * 3. Mild Flash (MF) Regime "MILDFLASH_HE"
     * 4. Strong Flash (SF) Regime "STRONGFLASH_He"
     * 5. Detonation (Dt) Regime "DETONATION_He"
     *
     * The regime you're in depends on your WD mass
     * and the accretion rate onto the WD
     */
    const double Mdot = Mdot_net(accretor);
    Accretion_regime regime = NO_REGIME_DETERMINABLE;

    if(Mdot > VERY_TINY)
    {
        const double Mdot_log10 = log10(Mdot);
        const double M_WD = accretor->mass;

        /*
         * boundary curves
         */
        double RGSS_log10mdot = 0.0;
        if(stardata->preferences->WD_accretion_rate_new_giant_envelope_lower_limit_helium_donor==DONOR_RATE_ALGORITHM_K2014_P2014)
        {
            RGSS_log10mdot = -6.840+1.349*M_WD;
        }
        else if(
            stardata->preferences->WD_accretion_rate_new_giant_envelope_lower_limit_helium_donor==DONOR_RATE_ALGORITHM_WANGWU
            &&
            M_WD >= 0.6
            )
        {
            /* interpolate */
            RGSS_log10mdot = log10(2.17*1e-6 * (Pow2(M_WD) + 0.82*M_WD - 0.38));
        }
        else if(
            stardata->preferences->WD_accretion_rate_new_giant_envelope_lower_limit_helium_donor==DONOR_RATE_ALGORITHM_WANGWU
            &&
            M_WD >= 0.6)
        {
            /* extrapolate */
            RGSS_log10mdot = 1.8003008938336499 * M_WD -7.069778803817572;
        }


        double SSMF_log10mdot = 0.0;
        if(M_WD<1.1)
        {
            SSMF_log10mdot = -8.115 + 2.290*M_WD;
        }
        else
        {
            SSMF_log10mdot = -7.63 + 1.85*M_WD;
        }

        double MFSF_log10mdot = 0.0;
        if(stardata->preferences->WD_accretion_rate_novae_upper_limit_helium_donor==DONOR_RATE_ALGORITHM_K2014_P2014)
        {
            MFSF_log10mdot=-8.233+2.022*M_WD;
        }
        else if(
            stardata->preferences->WD_accretion_rate_novae_upper_limit_helium_donor==DONOR_RATE_ALGORITHM_WANGWU
            &&
            M_WD>=0.6
            )
        {
            /* interpolate */
            MFSF_log10mdot = log10(1.46*1e-6 * (Pow3(-M_WD) + 3.45*Pow2(M_WD)- 2.60*M_WD + 0.85));
        }
        else if(stardata->preferences->WD_accretion_rate_novae_upper_limit_helium_donor==DONOR_RATE_ALGORITHM_WANGWU
                && M_WD<0.6)
        {
            //extrap
            MFSF_log10mdot= 0.6666654157433166 * M_WD - 6.73595931104315;
        }


        const double SFDt_log10mdot = -8.313+1.018*M_WD;

        if(Mdot_log10>RGSS_log10mdot)
        {
            regime = RG_HE;
        }
        else if(
            Mdot_log10<RGSS_log10mdot
            &&
            Mdot_log10>SSMF_log10mdot
            )
        {
            regime = STEADYBURN_HE;
        }
        else if(
            stardata->preferences->WD_accretion_rate_novae_upper_limit_helium_donor==DONOR_RATE_ALGORITHM_K2014_P2014
            &&
            Mdot_log10<SSMF_log10mdot
            &&
            Mdot_log10>MFSF_log10mdot
            )
        {
            regime = MILDFLASH_HE;
        }
        else if(
            Mdot_log10 < MFSF_log10mdot
            &&
            Mdot_log10 > SFDt_log10mdot
            )
        {
            regime = STRONGFLASH_He;
        }
        else if(
            Mdot_log10 < SFDt_log10mdot
            )
        {
            regime = DETONATION_He;
        }
    }
    return regime;
}

#endif // KEMP_NOVAE
