#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef CIRCUMBINARY_DISK_DERMINE

/*
 * Circumbinary disc code based on Dermine+ 2013
 *
 * Should not be used any more: use the Izzard & Jermyn 2017 discs instead.
 */

void circumbinary_disk(struct stardata_t * Restrict const stardata)
{
    Star_number k;
    double Rd=215.5; // in Ro units (215.5Ro = 1AU)
    double Hd=21.5; // in Rd units
    double alphad=0.1; // disk viscosity parametre
    const double m=2.0;
    const double l=1.0;

    /*
     * Add/subtract mass to the disk from this timestep
     */
    double mdot_system=0.0,mdotCB,tauCB;
    double M_system=0.0;
    double dJdisk=0.0;
    double dJwind=0.0;
    double eold=0.0;
    const double alphaCB=stardata->preferences->alphaCB;/* wind->CB efficiency */
    //  double alphaCB2=alphaCB+stardata->common.orbit.separation/1000;

    /* calculate total system mass and mass-loss rate */
    Starloop(k)
    {
        M_system += stardata->star[k].mass;
        mdot_system += alphaCB*(stardata->star[k].dmr-stardata->star[k].dmt);
    }

    /* calculate cirucmbinary disk mass loss timescale */
    tauCB = circumbinary_disk_massloss_timescale(stardata);

    /* hence the rate of mass change of the disk */
    mdotCB=alphaCB * mdot_system -   stardata->common.m_circumbinary_disk/tauCB;

    double edot=0.0,Jorbdot=0.0,rresovera=0.0,adotovera=0.0;

    /* if we have a disk ... change the orbit */
    if(stardata->common.m_circumbinary_disk > 0.0)
    {
        eold=stardata->common.orbit.eccentricity;
        if(stardata->common.orbit.eccentricity<1e-1)
        {
            eold=stardata->common.orbit.eccentricity;
            stardata->common.orbit.eccentricity=1e-1;
        }
        /*
         * edot, Jorbdot : NB units per year
         */

        /*
         * double aAU=stardata->common.orbit.separation/AU_IN_SOLAR_RADII;// separation in AU
         */

        Jorbdot = 0.0;

        double mu = (stardata->star[0].mass*stardata->star[1].mass)/M_system; //reduced mass

        rresovera = pow((m+1.0)/l,2.0/3.0);
        adotovera = -(2.0*l/m*stardata->common.m_circumbinary_disk/
                      mu*alphad*Hd*Hd/Rd/Rd/rresovera*
                      stardata->common.orbit.angular_frequency);
        edot = ((1.0-stardata->common.orbit.eccentricity*stardata->common.orbit.eccentricity)
                /(2.0*stardata->common.orbit.eccentricity)*(l-m)/l*adotovera);

        // Calculate dJorb/dt due to binary-disk resonant interaction

        Jorbdot = stardata->common.orbit.angular_momentum*
            (0.5*adotovera-stardata->common.orbit.eccentricity*edot/
             (1.0-stardata->common.orbit.eccentricity*stardata->common.orbit.eccentricity));

        /*
         * Add the contribution of dJorb needed to form the disc itself.
         * i.e. dJdisk = dM_CB*r_res_2:1*v_CB
         * = alphaCB*Mdot*r_res*SEP*sqrt(G*M/(r_res*SEP))
         *
         * !!! NOTE that the AM of the fraction of wind which feeds the disk
         * (dJwind) is supposed to contribute to dJ_disk and
         * HAS THEN TO BE REMOVED from it to NOT BE COUNTED TWICE !!!
         * Its the last term in Jorbdot:
         *
         * In Mo, Ro, yr, G = 3.94 10^8
         */

        dJdisk = alphaCB*mdot_system*rresovera*stardata->common.orbit.separation
            *sqrt(3.94e8*M_system/(rresovera*stardata->common.orbit.separation));
        dJwind = alphaCB*set_djorb(stardata,
                                   stardata->common.orbit.angular_frequency);
        /*printf("AM lost Jorb=%g dJdisk=%g dJwind=%g dJres=%g M_CB=%g e=%g\n",
          stardata->common.orbit.angular_momentum,
          -dJdisk,
          dJwind,
          Jorbdot,
          stardata->common.m_circumbinary_disk,
          stardata->common.orbit.eccentricity
          );*/
        Jorbdot += -dJdisk + dJwind;
        // apply eccentricity change due to circumbinary disk

        // DEBUG: Problem when P drops fast
        if(stardata->common.orbit.period > 1.0)
        {
            stardata->model.derivative[DERIVATIVE_ORBIT_ECCENTRICITY_CBDISC] = edot;
        }

        /*
         * apply angular momentum change due to circumbinary disk
         */
        stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM_CBDISC] = Jorbdot;
        stardata->common.orbit.eccentricity=eold;
    }

    stardata->common.m_circumbinary_disk += stardata->model.dt*mdotCB;

    /* printf("CIRCUMB t=%g edot=%g e=%g a=%g mdotCB=%g mCB=%g\n",
       stardata->model.time,
       edot,
       stardata->common.orbit.eccentricity,
       stardata->common.orbit.separation,
       mdotCB,
       stardata->common.m_circumbinary_disk
       );
    */

    /*printf("CIRCUMBDATA %g %g %g %g %g %g %g %g %g %g %g %g %g\n",
      stardata->model.time, // 1
      stardata->common.orbit.separation,//2
      stardata->common.orbit.period,//3
      stardata->common.orbit.eccentricity,//4
      M_system,//5
      mdot_system,//6
      mdotCB,//7
      stardata->common.m_circumbinary_disk,//8
      stardata->star[0].mass,//9
      stardata->star[1].mass,//10
      stardata->common.orbit.angular_momentum,//11
      Jorbdot,//12
      edot//13

      );*/

    // CF Tyl
    /*FILE *file;

      file = fopen("test.dat","a+");
      if(file != NULL) {
      fprintf(file,"%18.12e %g %g %g %g\n",
      stardata->model.time,
      log10(stardata->common.m_circumbinary_disk),
      stardata->common.orbit.separation,
      -Jorbdot
      /stardata->common.orbit.angular_frequency/
      stardata->common.orbit.separation/stardata->common.orbit.separation/
      mdot_system,
      -Jorbdot/(sqrt(3.94e8*M_system*stardata->common.orbit.separation))/
      mdot_system
      ); //writes
      fclose(file); //done!
      } else {
      printf("Could not open file");
      }*/

    if(stardata->common.m_circumbinary_disk<1e-8)
    {
        stardata->common.m_circumbinary_disk =0.0;
    }
}

double circumbinary_disk_massloss_timescale(struct stardata_t  * stardata)
{
    // circumbinary disk mass-loss timescale in years
    return(3e4);
}


#endif//CIRCUMBINARY_DISK_DERMINE
