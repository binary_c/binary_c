#include "../binary_c.h"
No_empty_translation_unit_warning;


double non_conservative_gamma(struct stardata_t * Restrict const stardata,
                              struct star_t * const donor,
                              struct star_t * const accretor
    )
{
    double gamma = 0.0;

    /*
     * When mass cannot be accreted by a star, assume 
     * it is lost from the system with orbital 
     * angular momentum Jlost = gamma * Mlost * Lorb
     * where Lorb is the specific orbital angular momentum.
     *
     * This routine gives you gamma.
     */
     if(Fequal(stardata->preferences->nonconservative_angmom_gamma,
               RLOF_NONCONSERVATIVE_GAMMA_ISOTROPIC))
     {
         /*
          * For super-Eddington mass transfer rates, for gamma = -2.0, for novae,
          * and the Hachisu disk wind, assume that material is lost from the system
          * as if a wind from the accretor.
          */
         gamma = donor->mass / accretor->mass;
     }
     else if(Fequal(stardata->preferences->nonconservative_angmom_gamma,
                    RLOF_NONCONSERVATIVE_GAMMA_DONOR))
     {
         /*
          * If gamma = -1.0 then assume the lost material carries with it the 
          * specific angular momentum of the donor star
          */
         gamma = accretor->mass / donor->mass;
     }
     else if(stardata->preferences->nonconservative_angmom_gamma>=0.0)
     {
         /*
          * for all gamma > 0.0 assume 
          * that it takes away a fraction gamma of the orbital angular momentum.
          */
         gamma = stardata->preferences->nonconservative_angmom_gamma;
     }
     else
     {
         gamma = 0.0; // prevents compiler warning
         Exit_binary_c(BINARY_C_WRONG_ARGUMENT,
                       "Gamma (RLOF ang. mom. loss factor) must be -1, -2 or >0");
     }
     
     return gamma;
}
