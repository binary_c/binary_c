#include "../binary_c.h"
No_empty_translation_unit_warning;

void mean_HRD(const struct stardata_t * const stardata,
              double * const logTeff,
              double * const lg,
              double * const logL)
{
    /*
     * Compute luminosity-weighted mean HRD location.
     *
     * If no stars contribute to the HRD, set
     *
     * logTeff = logg = logL = -100.0
     *
     * which is physically unrealistic.
     */
    double Teff_tot = 0.0;
    double g_tot = 0.0;
    double L_tot = 0.0;

    Foreach_star(star)
    {
        if(star->stellar_type != MASSLESS_REMNANT)
        {
            Teff_tot += star->luminosity * Teff(star->starnum);
            g_tot += star->luminosity * logg(star);
            L_tot += star->luminosity;
        }
    }

    if(L_tot > 0.0)
    {
        *logTeff = log10(Teff_tot) / L_tot;
        *lg = log10(g_tot) / L_tot;
        *logL = log10(L_tot);
    }
    else
    {
        *logTeff = -100.0;
        *lg = -100.0;
        *logL = -100.0;
    }
}
