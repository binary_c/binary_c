#include "../binary_c.h"
No_empty_translation_unit_warning;


/* enable this to get more warnings to stderr */
#define RLOF_STUFFED_WARNING
//#define RLOF_STUFFED_FAIL

/*
 * When the maximum number of iterations is reached, return
 * MAX_INTPOL_RETURN, which should be either EVOLUTION_ROCHE_OVERFLOW
 * (just continue with RLOF) or STOP_LOOPING_RESULT (stop the code).
 */
#define MAX_INTPOL_RETURN EVOLUTION_ROCHE_OVERFLOW

int interpolate_R_to_RL(struct stardata_t * Restrict const stardata)
{
    /*
     * Use BSE's method of taking a negative timestep
     * and interpolating to R=RL.
     * This uses fewer timesteps than binary_c's method,
     * but means sometimes you have negative timesteps.
     * Many algorithms assume a positive timestep, e.g.
     * nucleosynthesis.
     */

    RLOF_stars;
    struct model_t *model=&(stardata->model);
    double dr,dtmwas=model->dtm;
    double roche_radius = effective_Roche_radius(stardata,donor);

#if (DEBUG==1)
#ifdef NUCSYN
    const double mcno3dup = donor->core_mass_no_3dup;
    const double ntp = donor->num_thermal_pulses;
#else
    const double mcno3dup = 666.0;
    const double ntp = 666.0;
#endif // NUCSYN

    Dprint("in interpolate_R_to_RL star %d, t=%12.12e r=%12.12g, roche_radius=%g (r/roche_radius=%12.12g), m=%g, m0=%g, m00=%g, mc=%g, mcnodup=%g, ntp=%g, l=%g, intpol=%d (max=%d), RLOF=%d, kw=%d dtm=%g rdot=%g\n",
           donor->starnum,
           stardata->model.time,
           donor->radius,
           roche_radius,
           donor->radius/roche_radius,
           donor->mass,donor->phase_start_mass,donor->mass_at_RLOF_start,
           Outermost_core_mass(donor),
           mcno3dup,
           ntp,
           donor->luminosity,
           model->intpol,
           MAX_INTPOL,
           donor->radius > roche_radius,
           donor->stellar_type,
           model->dtm,
           donor->rdot
        );

    Dprint("Companion star %d mt=%g mass0=%g mass_at_RLOF_start=%g\n",
           accretor->starnum,
           accretor->mass,
           accretor->phase_start_mass,
           accretor->mass_at_RLOF_start);
#endif

    Dprint("Check for RLOF : if R=%g > RL=%g\n",donor->radius,
           RLOF_OVERFLOW_THRESHOLD*roche_radius);

    /* radius shrinks at the end of the TPAGB : oops! */
    const double menv = envelope_mass(donor);

    Dprint("R=%g vs ROL=%g*%g and menv = %g\n",
           donor->radius,
           RLOF_OVERFLOW_THRESHOLD,
           roche_radius,
           menv
           );

    if(
        /*
         * Roche lobe must be filled for RLOF
         */
        (donor->radius > RLOF_OVERFLOW_THRESHOLD*roche_radius)
        &&
        /*
         * ... but ignore RLOF in giants with low-mass envelopes which
         * will just lose them quickly.
         */
        (!GIANT_LIKE_STAR(donor->stellar_type) ||
         menv > MINIMUM_ENVELOPE_MASS_FOR_RLOF)
        )
    {
        /*
         * Interpolate back until the primary is just filling its Roche lobe.
         */
        Dprint("Interpolate t=%25.12e: r=%g vs ROCHE_OVERFLOW_THRESHOLD(%g)*roche_radius=%g\n",
               stardata->model.time,
               donor->radius,RLOF_OVERFLOW_THRESHOLD,RLOF_OVERFLOW_THRESHOLD*roche_radius);

        if(More_or_equal(donor->radius,RLOF_ENTRY_THRESHOLD*roche_radius))
        {
            if(model->intpol==0)
            {
                /*
                 * start interpolating:
                 * tphys00 is the time from which we start interpolating
                 */
                Dprint("First interpolation : set tphys00=%g (tphys0=%g) rdot=%g drdt=%g\n",
                       model->time,model->tphys0,donor->rdot,donor->drdt
                    );

                model->tphys00 = model->time;

                Dprint("Set mass_at_RLOF_start = %g,%g\n",stardata->star[0].phase_start_mass,
                       stardata->star[1].phase_start_mass);

                stardata->star[0].mass_at_RLOF_start = stardata->star[0].phase_start_mass;
                stardata->star[1].mass_at_RLOF_start = stardata->star[1].phase_start_mass;
            }

            /* up the iteration counter */
            model->intpol++;

            Dprint("intpol count = %d\n",model->intpol);

            if(model->iter==0 || model->inttry==TRUE)
            {
                Dprint("Return EVOLUTION_ROCHE_OVERFLOW\n");
                return EVOLUTION_ROCHE_OVERFLOW;
            }

            if(model->intpol>=MAX_INTPOL) return MAX_INTPOL_RETURN;

            /* calculate excess radius */
            dr = donor->radius - RLOF_OVERFILL * roche_radius;

            Dprint("Calc execss radius dr=%g\n",dr);

            /*
             * Check for pathological case where going backwards in time
             * does NOT cause the radius to increase. This case happen at
             * the end of the AGB, for example, and the old code gets
             * confused. We check for this and try going further back
             * in time to fix it.
             *
             * This could lead to problems, so we don't allow the timestep
             * to get too large.
             *
             * There is, of course, no check that the stellar type
             * does not change, or something else happen that is crazy.
             */

#ifdef PATHOLOGICAL_RLOF_CHECK
            if((model->intpol>=NEARLY_MAX_INTPOL)&&
               (dr > 0.0)&&(model->dtm < 0.0))
            {
                Dprint("Pathological RLOF: DR > 0 and dtm < 0 - star should shrink! (intpol=%d)\n",model->intpol);

                /* leave dtm (more) negative and keep shrinking ? */
                model->dtm *= 1.2;

                /* but don't make it too large! */
                model->dtm=Max(-0.025,model->dtm);
            }
            else
#endif // PATHOLOGICAL_RLOF_CHECK


            {
                if(stardata->common.orbit.separation <
                   MINIMUM_RLOF_INTERPOLATION_SEPARATION)
                {
                    printf("Return ROCHE_UNSTABLE_MERGER should be contact_system\n");
                    return EVOLUTION_ROCHE_UNSTABLE_MERGER;
                }
                else if(model->prec==TRUE
                        ||
                        fabs(donor->rdot)<TINY)
                {
                    Dprint("Precision exceeded! (prec=%d fabs(rdot)=%g<TINY?)",model->prec,fabs(donor->rdot));
                    return EVOLUTION_ROCHE_OVERFLOW;
                }

                model->dtm = -dr/fabs(donor->rdot);

#if (DEBUG==1)
                Dprint("At dtmwas=%g (dr=%g rdot=%g) t=%g now dtm=%g (mc=%g) which takes us to t=%g (cf start time %g)\n",
                       dtmwas,dr,donor->rdot,
                       model->time,model->dtm,
                       Outermost_core_mass(donor),model->time+model->dtm,model->tphys0);
#endif

                /*
                 * Don't go back in time past the point where we started
                 */
                double tchange = model->tphys0 - model->time;
                Dprint("tchange=%g\n",tchange);
                if(fabs(tchange)>TINY)
                {
                    Dprint("Enforce tchange : dtm=Max(dtm=%g,tchange=%g)\n",model->dtm,tchange);
                    model->dtm = Max(model->dtm,tchange);
                }
            }

            /*
             * Sometimes we are unlucky and the stellar type changes
             * when the time goes backward. This is bad if it changes
             * from e.g. HeMS to CHeB because the initial mass is
             * simply wrong. So do not allow the stellar type to
             * change for the *small* dtm we go backward. Hopefully this
             * is ok.
             */
#ifdef RLOF_NO_STELLAR_TYPE_CHANGE_IF_DTM_NEGATIVE
            if(model->dtm > 0.0)
#endif
            {
                Star_number k;
                Starloop(k)
                {
                    if(stardata->star[k].stellar_type !=
                       stardata->star[k].detached_stellar_type)
                    {
                        Dprint("Stellar type change (star 1) was %d now %d\n",
                               stardata->star[k].stellar_type,stardata->star[k].detached_stellar_type);

                        stardata->star[k].stellar_type = stardata->star[k].detached_stellar_type;
                        stardata->star[k].phase_start_mass = stardata->star[k].mass_at_RLOF_start;
                        stardata->star[k].epoch = model->time - stardata->star[k].aj0;
                        Dprint("Set epoch (interpolate R to RL) %d = %g\n",
                               k,
                               stardata->star[k].epoch);

                        Dprint("Set initial mass to %g from mass_at_RLOF_start, epoch to %g\n",
                               stardata->star[k].phase_start_mass,
                               stardata->star[k].epoch);
                    }
                }
            }

            Dprint("test for roche dtm=%g\n",model->dtm);
        }
        else
        {
            /*
             * Enter Roche lobe overflow
             */
            if(More_or_equal(model->time,
                             model->max_evolution_time))
            {
                Dprint("Time exhausted during RLOF\n");
                return EVOLUTION_STOP_LOOPING_RESULT;
            }
            else
            {
                Dprint("Enter RLOF at t=%25.12e (would have been %25.12e : delta t = %g)\n",
                       model->time,
                       model->tphys00,
                       model->time - model->tphys00);

                /*
                 * Set intpol to zero to show that we're no longer interpolating
                 */
                model->intpol=0;

                return EVOLUTION_ROCHE_OVERFLOW;
            }
        }
    }
    else
    {
        /*
         * Check if already interpolating.
         */
        if(model->intpol>0)
        {
            model->intpol++;

            /* on MAX_INTPOL, just return and hope we can clear up */
            if(model->intpol>=MAX_INTPOL) return MAX_INTPOL_RETURN;

            dr = donor->radius - RLOF_OVERFILL *roche_radius;

            Dprint("Already interpolating intpol=%d\n",model->intpol);


#ifdef PATHOLOGICAL_RLOF_CHECK
            if((model->intpol>=NEARLY_MAX_INTPOL)&&
               (dr > 0.0)&&(model->dtm < 0.0))
            {
                Dprint("Pathological RLOF: DR > 0 and dtm < 0 - star should shrink! (intpol=%d)\n",model->intpol);

                /* leave dtm (more) negative and keep shrinking ? */
                model->dtm *= 1.2;

                /* but don't make it too large! */
                model->dtm=Max(-0.025,model->dtm);
            }
            else
#endif // PATHOLOGICAL_RLOF_CHECK
                /* For some reason the tidal version does not do this... ? */
                if(model->intpol>=MAX_INTPOL)
                {
                    /* reached max number of interpolation iterations */
                    Dprint("binary_star_functions/test_for_roche_lobe_overflow: Interpolation stuffed! intpol=%d t=%g dtm=%g\n",model->intpol,stardata->model.time,model->dtm);

#ifdef RLOF_STUFFED_FAIL
                    Exit_binary_c(BINARY_C_RLOF_EXIT,
                                  "RLOF STUFFED FAIL :  intpol=%d t=%g dtm=%g\n",
                                  model->intpol,
                                  stardata->model.time,
                                  model->dtm);
#endif

                    model->inttry=TRUE;
                    if(model->time > model->tphys00)
                    {
                        /* now... if we've really tried to interpolate
                         * and for some reason the radius function won't let us,
                         * just evolve normally.
                         *
                         * This can happen with low mass AGB stars which have a thin
                         * envelope which actually contracts with time. The corrections
                         * given below for a "type 4" star do not work well enough.
                         */
                        model->dtm=0; // set timestep to zero (perfectly valid!)
                        model->intpol=0; // turn off interpolation
                        Dprint("RLOF stuffed : set dtm=intpol=0\n");
#ifdef RLOF_STUFFED_WARNING
                        fprintf(stderr,"Warning: RLOF stuffed. r=%g r/roche_radius=%g m=%g menv=%g mc=%g type=%d l=%g",
                                donor->radius,
                                donor->radius/roche_radius,
                                donor->mass,
                                envelope_mass(donor),
                                Outermost_core_mass(donor),
                                donor->stellar_type,
                                donor->luminosity);
                        if(donor->radius/roche_radius < 1.001)
                        {
                            fprintf(stderr," but R<1.001*ROL so probably all is ok");
                        }
                        fprintf(stderr,"\n");
#endif
                    }
                    /*
                     * Otherwise, we haven't got to the time we'd like to get to.
                     *
                     * However, normally we get close to the desired result,
                     * so just let the normal evolution code take it from here.
                     */
                    Dprint("return sort of failed");
                    return EVOLUTION_ROCHE_OVERFLOW_NONE;
                }
                else if(model->intpol>=NEARLY_MAX_INTPOL)
                {
                    Dprint("Warning: intpol=%d >= NEARLY_MAX_INTPOL=%d : set inttry=TRUE\n",
                           model->intpol,NEARLY_MAX_INTPOL);
                    model->inttry = TRUE;
                }

            if(fabs(donor->rdot)<TINY)
            {
                Dprint("Exceeded precision!\n");
                model->prec = TRUE;
                model->dtm = 1.0E-07 * model->time;
            }
            else
            {
                model->dtm = RLOF_CONVERGENCE_FACTOR1 *
                    -dr/fabs(donor->rdot);

                Dprint("Set dtm=%g from dr=%g/fabs(rdot=%g)=%g\n",
                       model->dtm,dr,donor->rdot,fabs(donor->rdot));
#ifdef NUCSYN
                if(donor->stellar_type==TPAGB)
                {
                    /* limit the negative step to prevent pathological cases */
                    model->dtm = Max(-0.011,model->dtm);
                    Dprint("Limit negative timestep to %g\n",model->dtm);
                }
#endif
            }

            /*
             * time is the time now
             *
             * next_time is the time after the next (RLOF) timestep
             *
             * tphys0 is the previous time this loop was called
             *
             * tphys00 is the time we originally started at before
             *                            we started interpolating
             *
             * dtm is the timestep while interpolating (in Myr)
             *
             * dt is the timestep while NOT interpolating (in years, not used here)
             */
            double next_time = model->time + model->dtm;

            Dprint("Check finished RLOF: t=%16.16g dtm=%g [prev_loop tphys0=%16.16g, aim tphys00=%16.16g, next = %16.16g ... ",
                   model->time,model->dtm,
                   model->tphys0,
                   model->tphys00,
                   next_time);

            if(next_time > model->tphys00)
            {

                /*
                 * If this occurs then most likely the star is a high mass type 4
                 * where the radius can change very sharply or possibly there is a
                 * discontinuity in the radius as a function of time and HRDIAG
                 * needs to be checked!
                 */

                /*
                 * Note: this can also happen if there is wind mass loss which is
                 * not so well resolved with the old timestep. In which case, the
                 * wind reduces the mass so RLOF does not occur and we can never find
                 * a solution where it "just about" occurs again.
                 */
                model->dtm = 0.5*(model->tphys00 - model->tphys0);
                if(fabs(model->dtm)<1e-10) model->dtm=1e-10 ; // prevent dt=0 errors
                model->prec = TRUE;

                /* RLOF should be finished? */
                Dprint("RLOF finished: set dtm=%g (tphys00=%g tphys0=%g) \n",model->dtm,model->tphys00,model->tphys0);

            }
#if (DEBUG==1)
            else
            {
                Dprint("RLOF still going\n");
            }
#endif

#ifdef RLOF_LIMIT_TO_TWENTY_PERCENT_CHANGE
            {
                /* limit change to 20% to prevent crazy numerical failure */
                double signdtm = Sign(model->dtm);
                model->dtm = fabs(model->dtm);
                Clamp(model->dtm,0.8*fabs(dtmwas),1.2*fabs(dtmwas));
                model->dtm *= signdtm;
            }
#endif
            model->tphys0 = model->time;
        }
    }

    /*
     * Go back for the next step or interpolation.
     */

#ifdef ADAPTIVE_RLOF
    {
        int k;
        Starloop(k)
        {
            stardata->star[k].nth=0.0;
            stardata->star[k].prev_dm=0.0;
        }
    }


#ifdef VISCOUS_RLOF
    if(stardata->common.prevt_mdot_RLOF > .99e-14)
    {
        printf("FORCE RLOF was mdot=%g\n",stardata->common.prevt_mdot_RLOF);
        return EVOLUTION_ROCHE_OVERFLOW;
    }
#endif // VISCOUS_RLOF
    stardata->common.prevt_mdot_RLOF=0.0;
#endif // ADAPTIVE_RLOF

    Dprint("return: No RLOF\n");
    return EVOLUTION_ROCHE_OVERFLOW_NONE;
}
