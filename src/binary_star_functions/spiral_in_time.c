#include "../binary_c.h"
No_empty_translation_unit_warning;



double spiral_in_time(struct stardata_t * Restrict const stardata)
{
    /*
     * Given the current rate of angular momentum loss,
     * estimate the time to spiral in to stellar merger,
     * in Myr. We do this by considering
     *
     * 1) J / (dJ/dt)
     * 2) the gravitational wave spiral down timescale
     *
     * and then take the minimum of the two
     */

    const double tj = fabs(stardata->common.orbit.angular_momentum) /
        Max(1e-100,
            fabs(stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM]));
    const double tgrav =
        use_gravitational_wave_radiation(stardata) ?
        Peters_grav_wave_merger_time(
            stardata,
            stardata->star[0].mass,
            stardata->star[1].mass,
            stardata->common.orbit.separation,
            stardata->common.orbit.eccentricity
            ) :
        LONG_TIME_REMAINING;
    const double t = Min(tj, tgrav) * 1e-6;
    Dprint("spiral in time = Min(tj=%g, tgrav=%g) = %g Myr (tj = |%g/%g|) \n",
           tj,
           tgrav,
           t,
           stardata->common.orbit.angular_momentum,
           stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM]
        );
    return t;
}
