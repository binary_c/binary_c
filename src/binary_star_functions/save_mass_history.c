#include "../binary_c.h"
No_empty_translation_unit_warning;

#ifdef SAVE_MASS_HISTORY

static int __error_handler_function(void * p,
                                    const int error_number,
                                    const char * format,
                                    va_list args);
static int __error_handler_function(void * p Maybe_unused,
                                    const int error_number Maybe_unused,
                                    const char * format Maybe_unused,
                                    va_list args Maybe_unused)
{
    return 0;
}

void save_mass_history(struct stardata_t * Restrict const stardata)
{
    if(stardata->preferences->save_mass_history_n_thermal>TINY)
    {
        /*
         * Save mass-loss history
         */
        Boolean vb = TRUE;
        if(vb)
        {
            printf("Save mass history\n");
            fflush(NULL);
        }
        if(stardata->model.mass_history == NULL)
        {
            CDict_new_from(stardata->model.mass_history);
            stardata->model.mass_history->error_handler = __error_handler_function;
        }

        Foreach_evolving_star(star)
        {
            if(star->stellar_type < HeWD)
            {
                const double dm = Mdot_net(star) * stardata->model.dt;
                CDict_nest(
                    stardata->model.mass_history, /* cdict */
                    stardata->model.time, dm, /* key,value pair */
                    "star",star->starnum /* nest location */
                    );
            }
        }

        /*
         * Remove material > thermal timescale ago
         *
         * then alter the stellar radius because of mass changes
         */
        Foreach_evolving_star(star)
        {
            const double tkh = 1e-6 * kelvin_helmholtz_time(star); /* Myr */
            const double last_thermal = Max(0.0,
                                            stardata->model.time - stardata->preferences->save_mass_history_n_thermal * tkh);
            if(vb)
            {
                printf("Star %d M=%g L=%g R=%g tkh=%g Myr, st %d, now %g, now-n*thermal=%g, mdot=%g (mdot_th=%g) dm=%g\n",
                       star->starnum,
                       star->mass,
                       star->luminosity,
                       star->radius,
                       tkh,
                       star->stellar_type,
                       stardata->model.time,
                       last_thermal,
                       Mdot_net(star),
                       star->mass / star->tkh, // Msun/y
                       Mdot_net(star) * stardata->model.dtm
                    );
                fflush(NULL);
            }
            struct cdict_t * const * const p =
                (struct cdict_t *const * const)
                CDict_nest_get_data_pointer(stardata->model.mass_history,
                                            "star",star->starnum);
            if(p != NULL &&
               *p != NULL)
            {
                CDict_loop(*p,entry)
                {
                    const double time = entry->key.key.double_data;
                    if(time < last_thermal)
                    {
                        /* remove : > save_mass_history_n_thermal*tkh ago */
                        CDict_del_and_contents(*p,
                                               entry,
                                               TRUE);
                    }
                }
            }
        }
    }
}


#endif // SAVE_MASS_HISTORY
