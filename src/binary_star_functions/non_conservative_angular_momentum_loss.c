#include "../binary_c.h"
No_empty_translation_unit_warning;


void non_conservative_angular_momentum_loss(struct stardata_t * Restrict const stardata,
                                            const Boolean RLOF Maybe_unused)
{
    /*
     * Calculate the angular momentum that is lost
     * when mass transfer is non-conservative.
     *
     * This can be because an accretion limit has been
     * hit or because of non-conservative RLOF.
     */

    double Lorb = Angular_momentum_from_stardata / Total_mass;
    Star_number k;
    Starloop(k)
    {
        struct star_t * accretor = &stardata->star[k];
        struct star_t * donor = &stardata->star[Other_star(k)];

        /*
         * Star: the nonconservative mass loss rate is
         * calculated in limit_accretion_rates, and takes
         * into account *all* processes (RLOF, winds, etc.).
         *
         * Assume that material is lost from the star with
         * the equatorial angular momentum.
         *
         * This needs improving!
         */
        accretor->derivative[DERIVATIVE_STELLAR_ANGMOM_NONCONSERVATIVE_LOSS] +=
            accretor->derivative[DERIVATIVE_STELLAR_MASS_NONCONSERVATIVE_LOSS]
            * accretor->omega * Pow2(accretor->radius);

        /*
         * Orbit : use gamma as parameter to determine
         * how much angular momentum is lost.
         */
        //if(Is_not_zero(accretor->derivative[DERIVATIVE_STELLAR_MASS_NONCONSERVATIVE_LOSS]))
        {
            double gamma = non_conservative_gamma(stardata,
                                                  donor,
                                                  accretor);
            stardata->model.derivative[DERIVATIVE_ORBIT_ANGMOM_NONCONSERVATIVE_LOSS] +=
                gamma * Lorb * accretor->derivative[DERIVATIVE_STELLAR_MASS_NONCONSERVATIVE_LOSS];
        }

        // djorb = gamma * Lorb * dm
        //       = gamma * Jorb * dm / TOTAL_MASS
        //       = gamma * a^2 * sqrt(1-e^2) * 2pi/P * M1 M2 * dm / TOTAL_MASS
        // but gamma = M2/M1 so (M2 = donor)
        // djorb = gamma * a^2 * sqrt(1-e^2) * 2pi/P * M2^2 * dm / TOTAL_MASS
        //
        // CF Joke's expression which has M1^2 !
    }

}
