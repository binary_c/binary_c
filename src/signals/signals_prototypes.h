#pragma once
#ifndef SIGNALS_PROTOTYPES_H
#define SIGNALS_PROTOTYPES_H

char * setup_segfaults(void);
void catch_signal(int signo);
void catch_timeout(int signo) No_return;
void catch_timeouts(void);
void uncatch_timeouts(void);

#endif // SIGNALS_PROTOTYPES_H
