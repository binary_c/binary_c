#include "../binary_c.h"
No_empty_translation_unit_warning;

#include <sys/resource.h>

void No_return catch_timeout(int signo)
{
    struct itimerval value;
    getitimer(ITIMER_VIRTUAL,
              &value);

    /* 
     * show when we timed out
     */
    time_t t = time(NULL);
    char timestring[100];
    if(!strftime(timestring,sizeof(timestring)," %F %T :   ",localtime(&t)))
    {
        timestring[0] = 0;
    }

    /*
     * Get CPU usage
     */ 
    int who = RUSAGE_SELF;
    struct rusage usage;
    getrusage(who,&usage);

    struct timespec tp;
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID,&tp);

    float tCPU = (float)(1.0 * usage.ru_utime.tv_sec + 
                         1e-6 * usage.ru_utime.tv_usec);
    float tSYS = (float)(1.0 * usage.ru_stime.tv_sec + 
                         1e-6 * usage.ru_stime.tv_usec);

    /*
     * Show backtrace
     */
    Backtrace;

    /*
     * Exit with statement
     */
    Exit_binary_c_no_stardata(
        BINARY_C_TIMED_OUT,
        "At %s the virtual timer has expired with signal %d after %ld seconds. Most likely we're in an infinite loop of doom :( Times: CPU = %g, SYS = %g\n",
        timestring,
        signo,
        (long int)value.it_interval.tv_sec,
        tCPU,
        tSYS
        );
}


