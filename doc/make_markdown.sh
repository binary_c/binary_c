#!/bin/bash

# convert binary_c2.lyx to binary_c2.md
# in markdown format
#
# requires pandoc and installation of pandoc filters
# see e.g. https://scientificallysound.org/2021/12/28/cross-referencing-in-pandoc-and-markdown-with-x/

#
# also, you need to have pandoc-xnos installed: currently this is
# incompatible with the latest pandoc (3.1 at the time of writing) but
# you can download the pandoc-xnos source, alter the regex in its core.py
# file as shown at https://github.com/tomduck/pandoc-xnos/issues/28
# and reinstall from the source.

wd=$(pwd)
echo "Working directory $wd"

# make temporary directory
tmp=$(mktemp -d)
#tmp=/tmp/mdtmp
echo "Temporary directory $tmp"

TEXFILE="$tmp/binary_c2.tex"
FILTERED_TEXFILE="$tmp/binary_c2.pre_md.tex"
MARKDOWN_FILE="$wd/binary_c2.md"

# convert lyx to latex
lyx --export-to latex $TEXFILE "$wd/binary_c2.lyx"

# filter with Rob's perl script
cat $TEXFILE | perl "$wd/markdown_filter.pl" $wd $tmp > $FILTERED_TEXFILE

# use pandoc to convert latex to markdown
# note: --number-sections does nothing in markdown, but keep
# it here for future references, and use pandoc_secnums.lua for
# markdown instead
TEXINPUTS='' pandoc --verbose --bibliography $wd/references.bib --number-sections --wrap=none --listings --filter pandoc-xnos --lua-filter pandoc_secnums.lua -f latex -t markdown -o $MARKDOWN_FILE $FILTERED_TEXFILE

echo "The raw LaTeX file is at $tmp/binary_c2.tex"
echo "The filtered LaTeX file is at $tmp/binary_c2.pre_md.tex"
echo "The markdown file is at $wd/binary_c2.md"

cd $wd
