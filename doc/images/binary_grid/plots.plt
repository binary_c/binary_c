set terminal postscript colour solid enhanced "Helvetica" 18
# plots are NLINES-NSTARS

############################################################

set output "sleep-dt=0-klaipeda.ps"
set multiplot layout 2,1 scale 1,1.1
set lmargin 8

# runtimes
x=3;y=4;set xlabel ""; set ylabel "Runtime" offset 0; set xtics format "" 
plot "sleep/dt=0/sleepmode-klaipeda-10000-100.dat" u x:y w lp ps 1.5 title "{/Symbol d}t=0, N_{stars}=100, N_{lines}=10000", "sleep/dt=0/sleepmode-klaipeda-10000-1000.dat" u x:y w lp ps 1.5 title "{/Symbol d}t=0, N_{stars}=1000, N_{lines}=10000", "sleep/dt=0/sleepmode-klaipeda-100000-100.dat" u x:y w lp ps 1.5 title "{/Symbol d}t=0, N_{stars}=100, N_{lines}=100000"

# scores
x=3;y=8;set ylabel "Score {/=10 (1=perfect, larger=worse)}"; set xlabel "N_{threads}"; set xtics format "%g"; 

plot  "sleep/dt=0/sleepmode-klaipeda-10000-100.dat" u x:y  w lp ps 1.5 lt 1 notitle, "sleep/dt=0/sleepmode-klaipeda-10000-1000.dat" u x:y  w lp ps 1.5 lt 2 notitle, "sleep/dt=0/sleepmode-klaipeda-100000-100.dat" u x:y  w lp ps 1.5 lt 3 notitle 

unset multiplot

############################################################
set output "sleep-dt=0-aibn36.ps"
set multiplot layout 2,1 scale 1,1.1
set lmargin 8

# runtimes
x=3;y=4;set xlabel ""; set ylabel "Runtime" offset 0; set xtics format "" 
plot "sleep/dt=0/sleepmode-aibn36-10000-100.dat" u x:y w lp ps 1.5 title "{/Symbol d}t=0, N_{stars}=100, N_{lines}=10000", "sleep/dt=0/sleepmode-aibn36-10000-1000.dat" u x:y w lp ps 1.5 title "{/Symbol d}t=0, N_{stars}=1000, N_{lines}=10000", "sleep/dt=0/sleepmode-aibn36-100000-100.dat" u x:y w lp ps 1.5 title "{/Symbol d}t=0, N_{stars}=100, N_{lines}=100000"

# scores
x=3;y=8;set ylabel "Score {/=10 (1=perfect, larger=worse)}"; set xlabel "N_{threads}"; set xtics format "%g"; 

plot  "sleep/dt=0/sleepmode-aibn36-10000-100.dat" u x:y  w lp ps 1.5 lt 1 notitle, "sleep/dt=0/sleepmode-aibn36-10000-1000.dat" u x:y  w lp ps 1.5 lt 2 notitle, "sleep/dt=0/sleepmode-aibn36-100000-100.dat" u x:y  w lp ps 1.5 lt 3 notitle

unset multiplot


############################################################
set output "sleep-dt=1-klaipeda.ps"
set multiplot layout 2,1 scale 1,1.1
set lmargin 8

# runtimes
x=3;y=4;set xlabel ""; set ylabel "Runtime" offset 2; set xtics format "" ; set logscale y
plot "sleep/dt=1/sleepmode-klaipeda-100-100.dat" u x:y w lp ps 1.5 title "{/Symbol d}t=0, N_{stars}=100, N_{lines}=100", "sleep/dt=1/sleepmode-klaipeda-1000-100.dat" u x:y w lp ps 1.5 title "{/Symbol d}t=0, N_{stars}=100, N_{lines}=1000", "sleep/dt=1/sleepmode-klaipeda-1000-1000.dat" u x:y w lp ps 1.5 title "{/Symbol d}t=0, N_{stars}=1000, N_{lines}=1000"

# scores
x=3;y=8;set ylabel "Score {/=10 (1=perfect, larger=worse)}"; set xlabel "N_{threads}"; set xtics format "%g"; unset logscale y

plot  "sleep/dt=1/sleepmode-klaipeda-100-100.dat" u x:y  w lp ps 1.5 lt 1 notitle, "sleep/dt=1/sleepmode-klaipeda-1000-100.dat" u x:y  w lp ps 1.5 lt 2 notitle, "sleep/dt=1/sleepmode-klaipeda-1000-1000.dat" u x:y  w lp ps 1.5 lt 3 notitle

unset multiplot


############################################################
set output "spin-dt=1-klaipeda-noIO.ps"
set multiplot layout 2,1 scale 1,1.1

# runtimes
x=3;y=4;set xlabel ""; set ylabel "Runtime" offset 2; set xtics format "" ; set logscale y
plot "spin/dt=1/spinmode-klaipeda-0-10.dat" u x:y w lp ps 1.5 title "{/Symbol d}t=1, N_{stars}=10, N_{lines}=0", "spin/dt=1/spinmode-klaipeda-0-100.dat" u x:y w lp ps 1.5 title "{/Symbol d}t=1, N_{stars}=100, N_{lines}=0", "spin/dt=1/spinmode-klaipeda-0-1000.dat" u x:y w lp ps 1.5 title "{/Symbol d}t=1, N_{stars}=1000, N_{lines}=0"

# scores
x=3;y=8;set ylabel "Score {/=10 (1=perfect, larger=worse)}"; set xlabel "N_{threads}"; set xtics format "%g"; unset logscale y

plot  "spin/dt=1/spinmode-klaipeda-0-10.dat" u x:y  w lp ps 1.5 lt 1 notitle, "spin/dt=1/spinmode-klaipeda-0-100.dat" u x:y  w lp ps 1.5 lt 2 notitle, "spin/dt=1/spinmode-klaipeda-0-1000.dat" u x:y  w lp ps 1.5 lt 3 notitle

unset multiplot

############################################################
set output "spin-dt=1-klaipeda-Nlines10000.ps"
set multiplot layout 2,1 scale 1,1.1
set lmargin 8

# runtimes
x=3;y=4;set xlabel ""; set ylabel "Runtime" offset 2; set xtics format "" ; set logscale y
plot "spin/dt=1/spinmode-klaipeda-10000-10.dat" u x:y w lp ps 1.5 title "{/Symbol d}t=1, N_{stars}=10, N_{lines}=10000", "spin/dt=1/spinmode-klaipeda-10000-100.dat" u x:y w lp ps 1.5 title "{/Symbol d}t=1, N_{stars}=100, N_{lines}=10000", "spin/dt=1/spinmode-klaipeda-10000-1000.dat" u x:y w lp ps 1.5 title "{/Symbol d}t=1, N_{stars}=1000, N_{lines}=10000"

# scores
x=3;y=8;set ylabel "Score {/=10 (1=perfect, larger=worse)}"; set xlabel "N_{threads}"; set xtics format "%g"; unset logscale y

plot  "spin/dt=1/spinmode-klaipeda-10000-10.dat" u x:y  w lp ps 1.5 lt 1 notitle, "spin/dt=1/spinmode-klaipeda-10000-100.dat" u x:y  w lp ps 1.5 lt 2 notitle, "spin/dt=1/spinmode-klaipeda-10000-1000.dat" u x:y  w lp ps 1.5 lt 3 notitle

unset multiplot

