#!/usr/bin/env perl
use strict;
use 5.16.0;

# Amdahl's law for binary_grid runtimes

my ($filter,$file) = @ARGV;

my $dtinit;
my %dt;
open(my $fp,'<',$file)||die("cannot open $file");
while(my $l = <$fp>)
{
    chomp $l;
    $l=~s/^\s+//;
    my @data = split(/\s+/,$l);
    my @timespan = ($data[3]=~/(\S+)\:(\S+)/);
    $dt{$data[0]} = $timespan[0]*60.0 + $timespan[1];
}
close $fp;

# find max speedup
my $maxspeedup = 0.0;
foreach my $n (sort {$a<=>$b} keys %dt)
{
    my $speedup = $dt{1}/$dt{$n};
    if($speedup > $maxspeedup)
    {
        $maxspeedup = $speedup;
    }
}

foreach my $n (sort {$a<=>$b} keys %dt)
{
    my $speedup = $dt{1}/$dt{$n};
    last if ($speedup > $filter * $maxspeedup);
    printf "%4d %10f %10f\n",
        $n,
        $dt{$n},
        $speedup;
}
