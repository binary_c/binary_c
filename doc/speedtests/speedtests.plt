
############################################################
# plot speedup data when running multicore
#
# you should change *only* the files="" to add your files
#
############################################################

# data files
files = "capc85_gaiaHRD_12022020 capc85_gaiaHRD_jemalloc_12022020 capc85_gaiaHRD_tcmalloc_12022020 astro2_gaiaHRD_12022020 astro2_gaiaHRD_jemalloc_12022020 astro2_gaiaHRD_tcmalloc_12022020"

############################################################

set xlabel "{/Italic N}_{threads}"
set ylabel "Speedup vs. one thread"

set terminal pdfcairo enhanced color font "Helvetica" fontscale 0.5 linewidth 1 rounded dashlength 1 size 16cm,16cm
set output "speedtests.pdf"
set key noenhanced

# "modern" fit to Amdahl's law based on
# https://research.cs.wisc.edu/multifacet/papers/ieeecomputer08_amdahl_multicore.pdf
# n = number of CPUs
# r = resource fraction devoted to multicore
# performance function
perf(r)=(r**0.5)

# symmetric
speedup(f,r,n)=1.0/((1.0-f)/perf(r) + (f*r/(perf(r)*n)))
speedupfit(n)=1.0/((1.0-f)/perf(r) + (f*r/(perf(r)*n)))

# asymmetric
#speedup(f,r,n)=1.0/((1.0-f)/perf(r) + (f/(perf(r)+n-r)))
#speedupfit(n)=1.0/((1.0-f)/perf(r) + (f/(perf(r)+n-r)))

set fit nolog quiet maxiter 100 limit 1e-6
set logscale x

# make fits
nplot=1
plotstring = "plot "
plotcomma = ""
do for [file in files] {
   f=0.9 # assume 90% parallelizable
   r=1.0 # assume all the core can 
   fit speedupfit(x) "< filter.pl 0.95 ".file u 1:3 via f,r
   plotstring =  \
      plotstring.\
      sprintf("%s\"< filter.pl 1.0 %s \" u 1:3 lt %d title \"%s f=%g r=%g\"",plotcomma,file,nplot,file,f,r).\
      sprintf(", speedup(%g,%g,x) notitle w l lt %d ",f,r,nplot)
   nplot = nplot + 1
   plotcomma = ","
}

# make plot
eval(plotstring)

############################################################
# runtimes

set ylabel "Runtime / s"

# plot raw runtimes
nplot=1
plotstring = "plot "
plotcomma = ""
do for [file in files] {
   plotstring =  \
      plotstring.\
      sprintf("%s\"< filter.pl 1.0 %s \" u 1:2 lt %d title \"%s\"",plotcomma,file,nplot,file)
   nplot = nplot + 1
   plotcomma = ","
}

eval(plotstring)

############################################################

files = "capc85_gaiaHRD_25022020 capc85_gaiaHRD_2602202-nojoin astro1_gaiaHRD_25022020 astro2_gaiaHRD_jemalloc_26022020 astro2_gaiaHRD_2602202-nojoin"


set xlabel "{/Italic N}_{threads}"
set ylabel "Speedup vs. one thread"

#set terminal pdfcairo enhanced color font "Helvetica" fontscale 0.5 linewidth 1 rounded dashlength 1 size 16cm,16cm
#set output "speedtests.pdf"
set key noenhanced

# "modern" fit to Amdahl's law based on
# https://research.cs.wisc.edu/multifacet/papers/ieeecomputer08_amdahl_multicore.pdf
# n = number of CPUs
# r = resource fraction devoted to multicore
# performance function
perf(r)=(r**0.5)

# symmetric
speedup(f,r,n)=1.0/((1.0-f)/perf(r) + (f*r/(perf(r)*n)))
speedupfit(n)=1.0/((1.0-f)/perf(r) + (f*r/(perf(r)*n)))

# asymmetric
#speedup(f,r,n)=1.0/((1.0-f)/perf(r) + (f/(perf(r)+n-r)))
#speedupfit(n)=1.0/((1.0-f)/perf(r) + (f/(perf(r)+n-r)))

set fit nolog quiet maxiter 100 limit 1e-6
set logscale x

# make fits
nplot=1
plotstring = "plot "
plotcomma = ""
do for [file in files] {
   f=0.9 # assume 90% parallelizable
   r=1.0 # assume all the core can 
   fit speedupfit(x) "< filter.pl 0.95 ".file u 1:3 via f,r
   plotstring =  \
      plotstring.\
      sprintf("%s\"< filter.pl 1.0 %s \" u 1:3 lt %d title \"%s f=%g r=%g\"",plotcomma,file,nplot,file,f,r).\
      sprintf(", speedup(%g,%g,x) notitle w l lt %d ",f,r,nplot)
   nplot = nplot + 1
   plotcomma = ","
}

# make plot
eval(plotstring)

# plot raw runtimes
nplot=1
plotstring = "plot "
plotcomma = ""
do for [file in files] {
   plotstring =  \
      plotstring.\
      sprintf("%s\"< filter.pl 1.0 %s \" u 1:2 lt %d title \"%s\"",plotcomma,file,nplot,file)
   nplot = nplot + 1
   plotcomma = ","
}

eval(plotstring)