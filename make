#!/usr/bin/perl
use strict;


############################################################
# MAKE IS DEPRECATED
#
# please use meson instead
#
############################################################


print "make is deprecated : please use meson and ninja instead (see docs for details)\n";
exit if("@ARGV"!~/force/);

use Term::ANSIColor;
use Time::HiRes qw/tv_interval gettimeofday/;

# wrapper script to build binary_c and look nice at the same time

# start time
my $t0 = [gettimeofday];

# save arguments
my $args = "@ARGV";
my $exit_on_error = $args=~s/exit_on_error// ? 1: 0;

# change to src directory
chdir 'src';

# use all CPUs if we can
my $ncpus = ncpus() || ($args=~/\-j\s*(\d+)/)[0] || 1;
print "Make on $ncpus CPUs\n";

# get number of source files
my $nsrc = n_to_build();

# do the build, pass in the arguments given to this script
my $cmd = "/usr/bin/make -j$ncpus -f Makefile $args 2>\&1 | cat |";
open(MAKE,$cmd)||die ("cannot run $cmd");
my $n=1;

# colours (used often)
my $green=color('bold green');
my $magenta=color('bold magenta');
my $red=color('bold red');
my $reset=color('reset');
my %errors;
my $retval = 0;
my @make_stack = ();

while($_ = next_make_line())
{
    chomp;
    next if m!/usr/bin/make/!;
    next if m!up to date!;
    if(s/^(?:gcc|icc|suncc|cc|clang|$ENV{CC}|$ENV{GCC}|$ENV{ICC}).*?\s+(\S+\.[ch])\s.*/BUILD $1/
       || 
       s/.*binary_c$/LINK $green binary_c $reset/)
    {
        if($exit_on_error==2)
        {
            close MAKE;
            last;
        }
        else
        {           
            my $cfile = $1;
            if($nsrc)
            {
                my $nn=sprintf'% 3d',$n;
                s/BUILD/BUILD [$nn\/$nsrc]/;
                $n++ if($cfile=~/c$/);
            }
            print $_,"\n";
        }
    }
    elsif(m!/usr/bin/make! || /Leaving directory/ || /Entering directory/
	)
    {
	next;
    }  
    elsif(m!remark \#177!)
    {
	# skip spurious intel compiler messages
	<MAKE>;<MAKE>;<MAKE>;next;
    }
    elsif(m!unrecogni[sz]ed option '-Wl,-soname,libbinary_c.so'!)
    {
	# oops we're using ld as the linker when we should use gcc
	print "\n\n$magenta Linking error $_ $reset\n\nTo fix this, try\n\nexport LD=gcc\n\nor\n\nexport LD=/usr/bin/gcc\n\nto use the compiler as a linker, then retry the build.\n\n\n";
    }
    else
    {
        next if(suppress_warning($_));
	my $s = (/warning/o) ? $magenta : $red;
        $retval ||= /error/o;
	
	while(/(.*)\:(\d+)\:(\d+)\: error\:(.*)/g)
	{
	    my $er="Line $2, column $3 : $4\n";
	    $errors{$1} //= $1.":\n";
	    $errors{$1} .= '        '.$er;
	}

	print $s,,$_,$reset,"\n";
        if($exit_on_error)
        {
            $exit_on_error = 2;
        }        
    }
}

close MAKE;
print "\n\n";
if(scalar %errors)
{
    print "\n\nError summary\n\n";
    map
    {
	print 'src/'.$_.' : '.$errors{$_},"\n\n";
    }sort keys %errors;
}
printf "Done (return $retval) took %.2f s\n",tv_interval($t0);
exit($retval);

############################################################

sub n_to_build
{
    # simulate a make to count the number of files that 
    # will be compiled
    return 0 if($args=~/clean/);
    my $vb=0;
    my $cmd = "env -i MAKE=\"false\" CC=\"true\" LD=\"false\" /usr/bin/make -j -f ./Makefile $args @_ 2>\&1;";
    print "CMD $cmd\n"if($vb);
    my $failcount=0;
    my $nsrc=0;
    open(MAKE,$cmd." |");
    while(<MAKE>)
    {
        print "LINE $_\n"if($vb);
        if(/^true -c/)
        {
            # 'compilation' step : add to count
            $nsrc++;
        }
        elsif($failcount++>3)
        {
            # 'link' (i.e. stop) or some
            # other error (for which we should also stop)
            close MAKE;
            print "CLOSE MAKE\n"if($vb);
        }
    }
    print "FOUND $nsrc\n"if($vb);
    close MAKE;
    return $nsrc;
}

sub ncpus
{
    # count number of cpus
    open(CPU_COUNT,'</proc/cpuinfo')|| return undef;
    my $n=0;
    while(<CPU_COUNT>)
    {
	$n++ if(/processor/o);
    }
    close CPU_COUNT;
    return $n;
}

sub suppress_warning
{
    my $w = shift;
    my $suppress;
    # if $w is a warning that we want to suppress, return 1
    #
    # otherwise 0
    if($w=~/\S*-fassociative-math\S* disabled; other options take precedence/)
    {
        $suppress = 1;
        my $l0 = <MAKE>;
        my $l1 = <MAKE>;
        if($l1 !~ /^\s*\^/)
        {
            push(@make_stack,$l0,$l1);
        }
    }
    else
    {
        $suppress = 0;
    }
    return $suppress;
}

sub next_make_line
{
    # return the next line from make
    my $l;
    if(scalar @make_stack)
    {
        $l = shift @make_stack;
    }
    else
    {
        $l = <MAKE>;
    }
    return $l;
}
