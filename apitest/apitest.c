#include "../src/API/binary_c_API.h"
#include <time.h>
#include <sys/timeb.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#undef exit
/*
 * apitest
 *
 * Short test program to throw random binary systems at binary_c's
 * library via its API.
 *
 * Note that it looks more complicated than it is because I have included
 * code to capture binary_c's stdout stream and output it here.
 * (You may not care about the output.)
 *
 * Use "make" to build apitest.
 *
 * You can use apitest.sh to run apitest, or set LD_LIBRARY_PATH
 * manually to point to a directory containing libbinary_c.so
 *
 * Choose the APITEST_OUTPUT_STREAM to be either
 * "/dev/null"   : in which case there is no output
 * "/dev/stdout" : output is sent to stdout
 */

#define  APITEST_OUTPUT_STREAM "/dev/stderr"

 /*
 * Output lines (if not APITEST_OUTPUT_STREAM is not /dev/null):
 *
 * APITEST ....  is information about what this code is doing.
 * STATUS  ....  is information about the binary system.
 * BINARY_C .... is output from binary_c (see iterate_logging.c etc.)
 *               which would have gone to stdout
 *
 * If you define the NO_OUTPUT macro, there will be no output except
 * the memory allocation and test system information. This is useful for speed tests,
 * but note that you may end up in a race condition where the pipe which replaces
 * stdout's buffer fills and hence the code stops.
 *
 * Note:
 * I have tested this with gcc 4.7.2 (Ubuntu 12.10) and gcc 7.2.0
 * (Ubuntu 17.10) only.
 */


/*
 * Other test options
 */
#define NUMBER_OF_SYSTEMS 1
#define SHOW_BUFFER TRUE
#define MAX_EVOLUTION_TIME 15000.0
#define TIMESTEP 10.0

/* local function prototypes */
static double randd(double min, double max);
static void APIprintf(char * format,...);

// #define _CAPTURE
#ifdef _CAPTURE
static void show_stdout(void);
static void capture_stdout(void);
#endif
static size_t getTotalSystemMemory(void);

/* C macros */
#define BINARY_C_APITEST_VERSION 2.1
#define APIprint(...) APIprintf(__VA_ARGS__);
#define NO_OUTPUT

/* global variables */
int out_pipe[2];
int stdoutwas;



int main ( int argc, char * *  argv )
{
    printf("binary_c apitest version %f (binary_c version %s)\n",
           BINARY_C_APITEST_VERSION,BINARY_C_VERSION);

    const long int N = NUMBER_OF_SYSTEMS; /* number of systems */
    double maxt = MAX_EVOLUTION_TIME; /* maximum time */
    double dt = TIMESTEP; /* timestep */
    double t=0.0; /* time */
    int i; /* counter */

    /* memory for N binary systems */
    struct libbinary_c_stardata_t *stardata[N];
    struct libbinary_c_store_t * store = NULL;
    char * argstring;
    for(i=0;i<N;i++)
    {
        double m1 = exp(randd(log(0.1),log(100.0)));

        int n_writ = asprintf(&argstring,
                              "binary_c M_1 %g M_2 %g eccentricity %g metallicity %g max_evolution_time %g orbital_period %g",
                              m1, // m2
                              randd(0.1,m1), // 0.1 < m2 < m1
                              randd(0.0,1.0), // ecc
                              randd(1e-4,0.03), // Z
                              maxt, // max time (Myr)
                              randd(1.0,1e6)
            );
        if(n_writ<0)
        {
            fprintf(stderr,
                    "snprintf into argstring failed with error %d\n",
                    n_writ);
            exit(BINARY_C_WRITE_FAILED);
        }

        printf("argstring : %s\n",argstring);
        fflush(stdout);

        stardata[i] = NULL;
        binary_c_new_system(stardata + i,
                            NULL,
                            NULL,
                            &store,
                            NULL,
                            &argstring,
                            -1);

        //binary_c_help(stardata[0],"M_1");

        fprintf(stderr,
                "stardata[%d] = %p; preferences %p\n",
                i,
                (void*)stardata[i],
                (void*)stardata[i]->preferences);


        /* log to APITEST_OUTPUT_STREAM */
        strlcpy(stardata[i]->preferences->log_filename,
                (APITEST_OUTPUT_STREAM),
                STRING_LENGTH-1);

        /* API log for debugging, usually /dev/null to disable */
        strlcpy(stardata[i]->preferences->api_log_filename_prefix,
                "/dev/null",
                STRING_LENGTH-1);

        stardata[i]->preferences->internal_buffering = INTERNAL_BUFFERING_STORE;
        stardata[i]->preferences->batchmode = BATCHMODE_LIBRARY;
        free(argstring);
    }


    /* find available system RAM (bytes) */
    long int RAM_available=(long int)getTotalSystemMemory();

    printf("Memory : stardata (x%ld) %8.5f MB prefs %8.5f MB (could alloc %ld stars in %g MB of RAM)\n",
       N,
       N*sizeof(struct stardata_t)/(1024.0*1024.0),
       sizeof(struct preferences_t)/(1024.0*1024.0),
       RAM_available/sizeof(struct stardata_t),
       RAM_available/(1024.0*1024.0)
    );

    /* Stellar evolution time loop */
    while(t<maxt+TINY)
    {
        APIprint("At time t=%g evolve for %g\n",t,dt);
        for(i=0;i<N;i++)
        {
            APIprint("Call API to Evolve system %d from t=%g to %g\n",i,t,t+dt);

            /* evolve binary_c for time dt for system i */
            binary_c_evolve_for_dt(stardata[i],dt);

            APIprint("API evolved (stardata time now %g)\n",stardata[i]->model.time);
#ifndef NO_OUTPUT
            fprintf(stderr,"STATUS  : System %d : t=%g M1=%g M2=%g a=%g\n",
                    i,
                    stardata[i]->model.time,
                    stardata[i]->star[0].mass,
                    stardata[i]->star[1].mass,
                    stardata[i]->common.separation);
#endif
        }
        t += dt; // update the time
    }


    /* show logs */
    for(i=0;i<N;i++)
    {
        char * buffer = NULL;
        size_t nbytes = 0;
        binary_c_buffer_info(*(stardata+i),&buffer,&nbytes);
        printf("Buffer %d contains %zu bytes\n",
               i,
               nbytes);
        printf("Buffer %d is\n************************************************************\n%s\n************************************************************\n",
               i,
               buffer);

        /*
         * empty the buffer
         */
        binary_c_buffer_empty_buffer(*(stardata+i));
        binary_c_buffer_info(*(stardata+i),&buffer,&nbytes);
        printf("Buffer %d now contains %zu bytes (should be empty, i.e. 0)\n",
               i,
               nbytes);
    }

    /* free memory */
    for(i=0;i<N;i++)
    {
        binary_c_free_memory(stardata+i,TRUE,TRUE,FALSE,TRUE,TRUE);
    }
    /* free store only as a final act */
    binary_c_free_store(&store);
    APIprint("API test finished\n");

    return 0;
}

/************************************************************/
#ifdef _CAPTURE

static void capture_stdout(void)
{
    /* capture stdout to a pipe */
    fflush(stdout);
    setvbuf(stdout,NULL,_IONBF,0); // remove stdout buffering
    setvbuf(stderr,NULL,_IONBF,0); // remove stdout buffering

    /* save for later reset */
    stdoutwas = dup(STDOUT_FILENO);

    if(pipe(out_pipe)!=0)
    {
    fprintf(stderr,"pipe creation failure\n");
    Exit_binary_c_no_stardata(PIPE_FAILURE,"pipe creation failed");
    }

    /* non-blocking stdout */
    long flags = fcntl(out_pipe[0], F_GETFL);
    flags |= O_NONBLOCK;
    fcntl(out_pipe[0], F_SETFL, flags);

    /* never write to the new pipe */
    dup2(out_pipe[1],STDOUT_FILENO);
    close(out_pipe[1]);
}


static void show_stdout(void)
{
    APIprint("show_stdout:\n");

    /* show what would have been binary_c's stdout */
    char buffer;
    fflush(stderr);
    fflush(stdout);
#ifndef NO_OUTPUT
    int stat=1;
#endif
    while(read(out_pipe[0],&buffer,1)>0)
    {
#ifndef NO_OUTPUT
    if(stat==1) fprintf(stderr,"BINARY_C: ");
    fprintf(stderr,"%c",buffer);
    stat = buffer == '\n' ? 1 : 0;
#endif
    }
}
#endif // _CAPTURE

static double randd(double min, double max)
{
    // random number between min and max
    return min + (max-min) * ((double)rand()/(double)RAND_MAX);
}

static void APIprintf(char * format,...)
{
    /* API print function */
#ifndef NO_OUTPUT
    va_list args;
    va_start(args,format);

    /* s contains the message */
    char * s;
    asprintf(&s,format,args);
    chomp(s); // from binary_c
    fprintf(stderr,"APITEST : %s\n",s);
    fflush(stderr);
    va_end(args);
    free(s);
#endif
}

static size_t getTotalSystemMemory(void)
{
    /* use sysconf to get available memory */
    long pages = sysconf(_SC_PHYS_PAGES);
    long page_size = sysconf(_SC_PAGE_SIZE);
    return pages * page_size;
}
