!
! Demo code to run binary_c from FORTRAN
!
! Tested on Ubuntu 16.10 with gcc and gfortran 5.4.1
!
! See README and the Make_fortran script,
! and the src/api/fortran.c file which contains
! the interface functions that are built into the API
! shared library.
!
! See also
! http://www-h.eng.cam.ac.uk/help/tpl/languages/mixinglanguages.html
!

program apitestf

  use iso_c_binding
  implicit none

  ! These are passed to binary_c to set up the stars
  ! and/or returned by binary_c
  double precision m1,m2,mc1,mc2,per,ecc,metallicity
  double precision t,newt,maxt,dt,binary_c_time
  double precision omega1,omega2,r1,r2,vrot1,vrot2
  double precision logg1,logg2,lum1,lum2,teff1,teff2
  double precision xh_1,xhe_1,xc_1,xn_1,xo_1,xfe_1
  double precision xh_2,xhe_2,xc_2,xn_2,xo_2,xfe_2
  double precision timediff
  integer stellar_type1, stellar_type2, av,ac
  character (len=1024) :: argstring
  character (len=1024) :: format_string

  ! dummy variable
  double precision dummy

  !
  ! internal memory space : stardata is the structure
  ! used by binary_c to store all its useful information
  !
  ! but then fortran requires a pointer to this structure
  ! in order to pass it around
  !
  ! NB the structure should never be accessed directly from
  ! Fortran : this would require us to set up a (very) complicated
  ! common block. Instead, defined a C subroutine to return the value
  ! you want to print out.
  !
  type(c_ptr),pointer :: stardata_pointer,store_pointer,persistent_data_pointer
  type(c_ptr),target :: stardata,store,persistent_data
  stardata_pointer => stardata
  store_pointer => store
  persistent_data_pointer => persistent_data



  ! our pointers must be NULL when first sent to binary_c_fortran_api_new_system()
  stardata_pointer = C_NULL_PTR
  store_pointer = C_NULL_PTR
  persistent_data_pointer = C_NULL_PTR

  ! make argstring to define our system
  m1 = 1.0d0
  m2 = 0.5d0
  per = 1d6
  ecc = 0.d0
  metallicity = 0.02
  maxt = 15000d0
  format_string = '("binary_c reset_stars M_1 ",E10.3," M_2 ",E10.3," metallicity ",E10.3," separation 0 orbital_period ",E10.3," eccentricity ",E10.3," max_evolution_time ",E10.3,A)'
  write(argstring,format_string) m1,m2,metallicity,per,ecc,maxt,char(0)

  ! create new stardata structure
  call binary_c_fortran_api_new_system(stardata_pointer, &
       C_NULL_PTR,              &
       C_NULL_PTR,              &
       store_pointer,           &
       persistent_data_pointer, &
       argstring)

  ! loop from time 0 to maxt in dt = 10 Myr steps
  dt = 10.0d0
  t = 0.d0
  do while (t .lt. maxt)
     write (*,*) 'Evolve at time ',t

     ! do evolution for this dt
     call binary_c_fortran_api_evolve_for_dt(dt,stardata_pointer)

     ! get information from binary_c
     call binary_c_fortran_api_stardata_info(&
          m1,mc1,r1,omega1,&
          logg1,lum1,teff1,&
          xh_1,xhe_1,xc_1,xn_1,xo_1,xfe_1,&
          m2,mc2,stellar_type2,r2,omega2,&
          logg2,lum2,teff2,&
          xh_2,xhe_2,xc_2,xn_2,xo_2,xfe_2,&
          per,ecc,&
          metallicity,&
          binary_c_time,dummy,&
          stellar_type1,stardata_pointer)

     ! up our age counter
     t = t + dt

     write(*,*)'At time ',t,binary_c_time,&
          ' M1=',m1,' Mc1=',Mc1,&
          'R=',r1,'omega=',omega1,&
          'logg=',logg1,'lum=',lum1,'teff1=',teff1,&
          ' stellar_type=',stellar_type1,&
          'H=',xh_1,'He=',xhe_1,'C=',xc_1,'N=',xn_1,'O=',xo_1,'Fe=',xfe_1
  end do

  ! free C memory
  call binary_c_fortran_api_free_memory(stardata_pointer,1,1,1)

end program apitestf
